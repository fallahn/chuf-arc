/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Sample Browser Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#ifndef SB_ALPHA_MASK_HPP_
#define SB_ALPHA_MASK_HPP_

#include <Sample.hpp>

#include <chuf2/Scene.hpp>
#include <chuf2/FrameBuffer.hpp>
#include <chuf2/SpriteBatch.hpp>

class SampleAlphaMask final : public Sample
{
public:
    explicit SampleAlphaMask(MessageBus&);
    ~SampleAlphaMask() = default;

    bool update(float) override;
    void draw() override;

    bool handleKeyEvent(const chuf::Event::KeyEvent&) override;
    bool handleMouseEvent(const chuf::Event::MouseEvent&) override;
    void handleMessage(const Message&) override;

private:

    chuf::Scene::Ptr m_scene;
    chuf::FrameBuffer* m_fboMask;
    chuf::FrameBuffer* m_fboNormal;

    chuf::SpriteBatch::Ptr m_brushBatch;
    chuf::SpriteBatch::Ptr m_normalMapBatch;
    void markAlphaBuffer(bool remove);
};

#endif //SB_ALPHA_MASK_HPP_