/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Sample Browser Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//example app to test framework features

#ifndef SB_BSP_HPP_
#define SB_BSP_HPP_

#include <chuf2/App.hpp>

#include <chuf2/Scene.hpp>
#include <chuf2/Model.hpp>
#include <chuf2/SpriteBatch.hpp>
#include <chuf2/Sprite.hpp>

#include <chuf2/Text.hpp>
#include <chuf2/FrameBuffer.hpp>

#include <Sample.hpp>

namespace chuf
{
    class BspMap;
}
class SampleBsp final : public Sample
{
public:
    explicit SampleBsp(MessageBus&);
    ~SampleBsp() = default;

    bool update(float dt) override;
    void draw() override;

    bool handleKeyEvent(const chuf::Event::KeyEvent& evt) override;
    bool handleMouseEvent(const chuf::Event::MouseEvent& evt) override;
    void handleMessage(const Message&) override;

private:
    chuf::Scene::Ptr m_scene;
    chuf::Node* m_modelNode;
    chuf::Node* m_cameraNode;
    glm::vec3 m_cameraAcceleration;

    chuf::BspMap* m_bspMap;

    enum Button
    {
        Forward = (1 << 0),
        Back    = (1 << 1),
        Left    = (1 << 2),
        Right   = (1 << 3)
    };
    chuf::UInt16 m_buttonMask;

    std::unique_ptr<chuf::Text> m_text;

    chuf::SpriteBatch::Ptr m_sbReflectPreview;
    chuf::SpriteBatch::Ptr m_sbRefractPreview;
    chuf::Sprite::Ptr m_sprite;
};


#endif //SB_BSP_HPP_