/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Sample Browser Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#ifndef SB_MESSAGE_BUS_HPP_
#define SB_MESSAGE_BUS_HPP_

#include <SampleIds.hpp>

struct Message final
{
    enum class Type
    {
        UI = 1
    }type;

    struct UIEvent
    {
        enum Type
        {
            RequestStateChange

        }type;
        SampleId sampleId;
    };

    union
    {
        UIEvent ui;
    };
};

#include <queue>

class MessageBus final
{
public:
    MessageBus() = default;
    ~MessageBus() = default;
    MessageBus(const MessageBus&) = delete;
    MessageBus& operator = (const MessageBus&) = delete;

    Message poll()
    {
        auto m = m_processingMessages.front();
        m_processingMessages.pop();
        return m;
    }
    void post(const Message& msg)
    {
        m_pendingMessages.push(msg);
    }
    bool empty()
    {
        if (m_processingMessages.empty())
        {
            m_processingMessages.swap(m_pendingMessages);
            return true;
        }
        return false;
    }

private:
    std::queue<Message> m_processingMessages;
    std::queue<Message> m_pendingMessages;
};

#endif //SB_MESSGE_BUS_HPP_