/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Sample Browser Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#ifndef SB_SAMPLE_BROWSER_HPP_
#define SB_SAMPLE_BROWSER_HPP_

#include <chuf2/App.hpp>

#include <SampleIds.hpp>
#include <Sample.hpp>
#include <SampleStack.hpp>
#include <MessageBus.hpp>

class SampleBrowser final : public chuf::App
{
public:
    SampleBrowser();
    ~SampleBrowser() = default;

private:
    MessageBus m_messageBus;
    SampleStack m_stack;
    SampleId m_currentId;

    void start() override;
    void update(float dt) override;
    void draw() override;
    void finish() override;

    void handleKeyEvent(const chuf::Event::KeyEvent& evt) override;
    void handleMouseEvent(const chuf::Event::MouseEvent& evt) override;
    void handleMessage(const Message&);
};


#endif //SB_SAMPLE_BROWSER_HPP_