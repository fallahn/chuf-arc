/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Sample Browser Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#ifndef SB_SAMPLE_STACK_HPP_
#define SB_SAMPLE_STACK_HPP

#include <SampleIds.hpp>
#include <Sample.hpp>

#include <map>
#include <functional>
#include <vector>

class SampleStack final
{
public:
    enum class Action
    {
        Push,
        Pop,
        Clear
    };

    explicit SampleStack(MessageBus& mb) : m_messageBus(mb){}
    ~SampleStack() = default;

    SampleStack(const SampleStack&) = delete;
    SampleStack& operator = (const SampleStack&) = delete;


    template <typename T>
    void registerSample(SampleId id)
    {
        m_sampleFactories[id] = [this]()
        {
            return std::make_unique<T>(m_messageBus);
        };
    }

    void update(float dt);
    void draw();

    void handleKeyEvent(const chuf::Event::KeyEvent& evt);
    void handleMouseEvent(const chuf::Event::MouseEvent& evt);
    void handleMessage(const Message&);

    void pushSample(SampleId id);
    void popSample();
    void clearSamples();

    bool empty() const;
   
private:

    struct Pendingchange
    {
        explicit Pendingchange(Action a, SampleId i = SampleId::None) : action(a), id(i){}
        Action action;
        SampleId id;
    };

    std::vector<Sample::Ptr> m_stack;
    std::vector<Pendingchange> m_pendingChanges;
    std::map<SampleId, std::function<Sample::Ptr()>> m_sampleFactories;
    MessageBus& m_messageBus;

    Sample::Ptr createSample(SampleId id);
    void applyPendingChanges();
};

#endif //SB_SAMPLE_STACK_HPP_