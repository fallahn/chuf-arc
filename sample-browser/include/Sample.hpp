/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Sample Browser Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//base class for samples in sample browser

#ifndef SB_SAMPLE_HPP_
#define SB_SAMPLE_HPP_

#include <chuf2/App.hpp>
#include <chuf2/Cache.hpp>
#include <chuf2/Reports.hpp>

#include <MessageBus.hpp>

#include <memory>

class Sample
{
public:
    using Ptr = std::unique_ptr<Sample>;

    explicit Sample(MessageBus& mb) : m_messageBus(mb)
    {
        //chuf::BaseCache::flushAll();    
    }
    virtual ~Sample() = default;

    Sample(const Sample&) = delete;
    Sample& operator = (const Sample&) = delete;

    virtual bool update(float dt) = 0;
    virtual void draw() = 0;

    virtual bool handleKeyEvent(const chuf::Event::KeyEvent& evt)  = 0;
    virtual bool handleMouseEvent(const chuf::Event::MouseEvent& evt)  = 0;
    virtual void handleMessage(const Message&) = 0;

protected:
    chuf::App& getApp(){ return chuf::App::getInstance(); }
    void postMessage(const Message& msg){ m_messageBus.post(msg); }

private:
    MessageBus& m_messageBus;
};


#endif //SB_SAMPLE_HPP_