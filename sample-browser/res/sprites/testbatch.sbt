spritebatch
{
	image = "res/images/sprite.png"
	
	sprite one
	{
		rotation = 30
		position = 120, 23
		size = 130, 150
	}
	
	sprite two
	{
		position = 240, 400
		colour = 0, 0.4, 1, 1
	}
	
	sprite 3
	{
		//scale = 1, 1.2
		//subrect = 0, 0, 100, 150
		colour = 1,0.8, 0.4, 0.9
	}
	
	shader
	{
		vertShader = "res/shaders/Unlit.vert"
		fragShader = "res/shaders/Unlit.frag"
		defines = "#define VERTEX_COLOUR"
	}
}