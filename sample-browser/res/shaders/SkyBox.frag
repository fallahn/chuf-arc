in vec4 v_position;

uniform samplerCube u_cubeTexture;

out vec4 outColour;

void main()
{
	vec4 position = v_position;
#if defined(REFLECTION)
	position.y = -position.y;
#endif

	outColour = texture(u_cubeTexture, position.xyz);
}