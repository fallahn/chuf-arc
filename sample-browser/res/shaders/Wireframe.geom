layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;

uniform vec2 u_screenSize = vec2(1024.0, 768.0);

out noperspective vec3 v_distance;

void main()
{
	vec2 p0 = u_screenSize * gl_in[0].gl_Position.xy / gl_in[0].gl_Position.w;
	vec2 p1 = u_screenSize * gl_in[1].gl_Position.xy / gl_in[1].gl_Position.w;
	vec2 p2 = u_screenSize * gl_in[2].gl_Position.xy / gl_in[2].gl_Position.w;

	vec2 v0 = p2 - p1;
	vec2 v1 = p2 - p0;
	vec2 v2 = p1 - p0;
	float area = abs(v1.x * v2.y - v1.y * v2.x);

	v_distance = vec3(area / length(v0), 0.0, 0.0);
	EmitVertex();

	v_distance = vec3(0.0, area / length(v1), 0.0);
	EmitVertex();

	v_distance = vec3(0.0, 0.0, area / length(v2));
	EmitVertex();
	EndPrimitive();
}