uniform sampler2D u_diffuseTexture;
uniform sampler2D u_lightmapTexture;
uniform samplerCube u_cubemapTexture;

#if defined(BUMP)
uniform sampler2D u_normalTexture;
uniform sampler2D u_radNormTextures;
#endif //BUMP

#if defined(MASK)
uniform sampler2D u_maskTexture;
#endif //MASK

in vec2 v_texCoord0;
in vec2 v_texCoord1;

#if defined(BUMP)
const vec3 bumpBasis[3] = vec3[]
(
	vec3(sqrt(2.0) / sqrt(3.0), 0.0,             1.0 / sqrt(3.0)),
	vec3(-1.0 / sqrt(6.0),      1.0 / sqrt(2.0), 1.0 / sqrt(3.0)),
	vec3(-1.0 / sqrt(6.0),     -1.0 / sqrt(2.0), 1.0 / sqrt(3.0))
);

const float normalOffset = 1.0 / 3.0;
#endif //BUMP

out vec4 outColour;

void main()
{
	vec3 lightmapColour = vec3(0.0);
#if defined(BUMP)
	vec3 normalVector = normalize(texture(u_normalTexture, v_texCoord0).rgb * 2.0 - 1.0);
	
	vec3 basis = vec3(0.0);
	basis.x = clamp(dot(normalVector, bumpBasis[0]), 0.0, 1.0);
	basis.y = clamp(dot(normalVector, bumpBasis[1]), 0.0, 1.0);
	basis.z = clamp(dot(normalVector, bumpBasis[2]), 0.0, 1.0);
	basis *= basis;
	
	float sum = dot(basis, vec3(1.0));
	
	//3 normal maps are all part of a single texture
	float vScale = v_texCoord1.y / 3.0;
	lightmapColour = basis.x * texture(u_radNormTextures, vec2(v_texCoord1.x, vScale)).rgb
			+ basis.y * texture(u_radNormTextures, vec2(v_texCoord1.x, vScale + normalOffset)).rgb
			+ basis.z * texture(u_radNormTextures, vec2(v_texCoord1.x, vScale + (normalOffset * 2.0))).rgb;
			
	lightmapColour /= sum;
#else
	lightmapColour = texture(u_lightmapTexture, v_texCoord1).rgb;
#endif //BUMP

	vec4 diffuseColour = texture(u_diffuseTexture, v_texCoord0);
	diffuseColour.rgb *= lightmapColour;
	
	outColour = vec4(diffuseColour.rgb, diffuseColour.a);
}