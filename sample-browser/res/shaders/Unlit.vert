
in vec4 a_position;
in vec2 a_texCoord0;

#if defined(VERTEX_COLOUR)
in vec4 a_colour;
#endif //VERTEX_COLOUR

#if defined(REFLECTION)
uniform mat4 u_reflectionWorldViewProjectionMatrix;
#else
uniform mat4 u_worldViewProjectionMatrix;
#endif //REFLECTION

#if defined(CLIP_PLANE)
uniform mat4 u_worldMatrix;
uniform vec4 u_clipPlane;
#endif //CLIP_PLANE

out vec2 v_texCoord;

#if defined(VERTEX_COLOUR)
out vec4 v_colour;
#endif //VERTEX_COLOUR

void main()
{
#if defined(REFLECTION)	
	gl_Position = u_reflectionWorldViewProjectionMatrix * a_position;
#else
	gl_Position = u_worldViewProjectionMatrix * a_position;
#endif //REFLECTION
	
#if defined(CLIP_PLANE)
	gl_ClipDistance[0] = dot(u_worldMatrix * a_position, u_clipPlane);
#endif //CLIP_PLANE

	v_texCoord = a_texCoord0;
	
#if defined(VERTEX_COLOUR)
	v_colour = a_colour;
#endif //VERTEX_COLOUR	
}