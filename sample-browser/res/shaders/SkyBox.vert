
in vec4 a_position;

uniform mat4 u_worldViewProjectionMatrix;

#if defined(CLIP_PLANE)
uniform mat4 u_worldMatrix;
uniform vec4 u_clipPlane = vec4(0.0, 1.0, 0.0, 22.0);
#endif //CLIP_PLANE

out vec4 v_position;

void main()
{
	gl_Position = (u_worldViewProjectionMatrix * a_position).xyww;

#if defined(CLIP_PLANE)
	gl_ClipDistance[0] = dot(u_worldMatrix * a_position, u_clipPlane);
#endif //CLIP_PLANE
	
	v_position = a_position;
}