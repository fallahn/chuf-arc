in vec4 a_position;
in vec3 a_normal;
in vec2 a_texCoord0;

#if defined(BUMP)
in vec3 a_tangent;
in vec3 a_bitangent;
#endif //BUMP

uniform mat4 u_inverseTransposeWorldViewMatrix;
uniform vec3 u_lightDirection;

#if defined(BUMP) || defined(FLATSHADE)
uniform mat4 u_worldViewMatrix;
#endif

#if defined(BUMP)
uniform mat4 u_viewMatrix;
uniform vec3 u_cameraWorldPosition;
#endif //BUMP

#if defined(CLIP_PLANE)
uniform mat4 u_worldMatrix;
uniform mat4 u_inverseTransposeWorldMatrix;
uniform vec4 u_clipPlane;
#endif //CLIP_PLANE

#if defined(REFLECTION)
uniform mat4 u_reflectionWorldViewProjectionMatrix;
#else
uniform mat4 u_worldViewProjectionMatrix;
#endif //REFLECTION

#if defined(FLATSHADE)
out vec3 v_worldPosition;
#endif //FLATSHADE

#if !defined(BUMP)
out vec3 v_normalVector;
#endif //!BUMP
out vec2 v_texCoord0;
out vec3 v_lightDirection;

#if defined(BUMP)
out vec3 v_cameraPosition;
#endif //BUMP

//out float v_distance;
#if defined(LAYER_COUNT)
out vec2 v_texCoordLayer0;
#if LAYER_COUNT > 1
out vec2 v_texCoordLayer1;
#endif //1
#if LAYER_COUNT > 2
out vec2 v_texCoordLayer2;
#endif //2
#if LAYER_COUNT > 3
out vec2 v_texCoordLayer3;
#endif //3
#endif //LAYER_COUNT


void main()
{
#if defined(REFLECTION)	
	gl_Position = u_reflectionWorldViewProjectionMatrix * a_position;
#else
	gl_Position = u_worldViewProjectionMatrix * a_position;
#endif //REFLECTION

	mat3 inverseTransposeWorldViewMatrix = mat3(u_inverseTransposeWorldViewMatrix[0].xyz, u_inverseTransposeWorldViewMatrix[1].xyz, u_inverseTransposeWorldViewMatrix[2].xyz);
	vec3 normalVector = inverseTransposeWorldViewMatrix * a_normal;
	
#if defined(BUMP)
	vec3 tangent = normalize(inverseTransposeWorldViewMatrix * a_tangent); 
	vec3 bitangent = normalize(inverseTransposeWorldViewMatrix * a_bitangent);
	mat3 tangentSpaceTransformMatrix = mat3(tangent.x, bitangent.x, normalVector.x, tangent.y, bitangent.y, normalVector.y, tangent.z, bitangent.z, normalVector.z);
	
	v_lightDirection = tangentSpaceTransformMatrix * u_lightDirection;
	
	vec4 worldViewPosition = u_worldViewMatrix * a_position;
	v_cameraPosition = tangentSpaceTransformMatrix * (u_worldViewMatrix * vec4(u_cameraWorldPosition, 1.0) - worldViewPosition).xyz;
	
#else
	v_lightDirection = u_lightDirection;
	v_normalVector = normalVector;
#endif //BUMP

#if defined(FLATSHADE)
	v_worldPosition = (u_worldViewMatrix * a_position).xyz;
#endif //FLATSHADE

	//calc the depth of the vert in screen space so we can scale the texture according to distance
	//v_distance = clamp(1.0 / (-(u_worldViewMatrix * a_position).z / 100.0), 0.4, 1.0);

	v_texCoord0 = a_texCoord0;

#if defined(LAYER_COUNT)
	v_texCoordLayer0 = a_texCoord0 * LAYER_REPEAT0;
#if LAYER_COUNT > 1
	v_texCoordLayer1 = a_texCoord0 * LAYER_REPEAT1;
#endif //1
#if LAYER_COUNT > 2
	v_texCoordLayer2 = a_texCoord0 * LAYER_REPEAT2;
#endif //2
#if LAYER_COUNT > 3
	v_texCoordLayer3 = a_texCoord0 * LAYER_REPEAT3;
#endif //3
#endif //LAYER_COUNT	

#if defined(CLIP_PLANE)
	vec4 clipPlane = u_clipPlane;
	clipPlane.w -= 0.26; //reduces black lines where water suface meets
	gl_ClipDistance[0] = dot(u_worldMatrix * a_position, u_clipPlane);
#endif //CLIP_PLANE	
}
