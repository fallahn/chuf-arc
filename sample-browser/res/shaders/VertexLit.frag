uniform sampler2D u_diffuseTexture;

#if defined(BUMP)
uniform sampler2D u_normalTexture;
#if defined(MASK)
uniform sampler2D u_maskTexture;
#endif //MASK
#endif //BUMP

#if defined(LIGHTMAPPED)
uniform sampler2D u_lightmapTexture;
#endif //LIGHTMAPPED

uniform vec3 u_lightColour = vec3(1.0, 1.0, 1.0);
uniform vec3 u_ambientColour = vec3(0.2, 0.2, 0.1);

#if !defined(BUMP)
in vec3 v_normalVector;
#endif //BUMP

in vec2 v_texCoord0;
#if defined(LIGHTMAPPED)
in vec2 v_texCoord1;
#endif //LIGHTMAPPED
in vec3 v_lightDirection;

#if defined(BUMP)
in vec3 v_cameraPosition;
#endif //BUMP

#if defined(WIREFRAME)
in noperspective vec3 v_distance;
#endif //WIREFRAME

out vec4 outColour;

void main()
{
	vec4 diffuseColour = texture(u_diffuseTexture, v_texCoord0);
	vec3 baseColour = diffuseColour.rgb;
	
#if defined (BUMP)
	vec3 normalVector = normalize(texture(u_normalTexture, v_texCoord0).rgb * 2.0 - 1.0);
#else
	vec3 normalVector = v_normalVector;
#endif //BUMP

#if defined(LIGHTMAPPED)
	
	baseColour *= texture(u_lightmapTexture, v_texCoord1).rgb;
#endif //LIGHTMAPPED

	vec3 lightDirection = normalize(v_lightDirection * 2.0);
	vec3 blendedColour = baseColour * u_ambientColour;
	float diffuseAmount = max(dot(normalVector, -lightDirection), 0.0);

	//half lambert term to reduce contrast (raise pow for more contrast)
	diffuseAmount = pow((diffuseAmount * 0.5) + 0.5, 2.0);

	blendedColour += u_lightColour * baseColour * diffuseAmount;
	
#if defined(BUMP)
	//calc specular
	vec3 eyeVec = normalize(v_cameraPosition);
	vec3 specularAngle = reflect(lightDirection, normalVector);//normalize(normalVector * diffuseAmount * 2.0 - lightDirection);
	
#if defined(MASK)
	vec4 mask = texture(u_maskTexture, v_texCoord0);
	float exp = 255.0 * mask.r;
	float specAmount = mask.g;
#else
	const float exp = 255.0;
	const float specAmount = 0.5;
	
#endif //MASK
	vec3 specularColour = vec3(pow(clamp(dot(specularAngle, eyeVec), 0.0, 1.0), exp));
	blendedColour += (specularColour * specAmount);
#endif //BUMP

#if defined(WIREFRAME)
	float nearest = min(min(v_distance.x, v_distance.y), v_distance.z);
	float intensity = exp2(-1.0 * nearest * nearest);
	blendedColour = mix(blendedColour, vec4(1.0), intensity);
#endif //WIREFRAME
	outColour = vec4(blendedColour, diffuseColour.a);
}