attribute vec4 a_position;

uniform mat4 u_worldViewProjectionMatrix;

varying vec4 v_position;

void main()
{
	gl_Position = u_worldViewProjectionMatrix * a_position;
	v_position = a_position;
}