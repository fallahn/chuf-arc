#if defined(LAYER_COUNT)
uniform sampler2DArray u_layerDiffuseMaps;
uniform sampler2D u_blendMap;
#if defined(BUMP)
uniform sampler2DArray u_layerNormalMaps;
#if defined(MASK)
uniform sampler2DArray u_layerMaskMaps;
#endif //MASK
#endif //BUMP
#else
uniform sampler2D u_diffuseTexture;

#if defined(BUMP)
uniform sampler2D u_normalTexture;
#if defined(MASK)
uniform sampler2D u_maskTexture;
#endif //MASK
#endif //BUMP
#endif //LAYER_COUNT

uniform vec3 u_lightColour = vec3(0.9, 0.95, 1.0);
uniform vec3 u_ambientColour = vec3(0.5, 0.5, 0.5);

#if !defined(BUMP)
in vec3 v_normalVector;
#endif //BUMP

in vec2 v_texCoord0;
in vec3 v_lightDirection;

#if defined(BUMP)
in vec3 v_cameraPosition;
#endif //BUMP

#if defined(FLATSHADE)
in vec3 v_worldPosition;
#endif //FLATSHADE

//in float v_distance;
#if defined(LAYER_COUNT)
in vec2 v_texCoordLayer0;
#if LAYER_COUNT > 1
in vec2 v_texCoordLayer1;
#endif //1
#if LAYER_COUNT > 2
in vec2 v_texCoordLayer2;
#endif //2
#if LAYER_COUNT > 3
in vec2 v_texCoordLayer3;
#endif //3
#endif //LAYER_COUNT

out vec4 outColour;

void main()
{
#if defined(LAYER_COUNT)
	vec4 diffuseColour = texture(u_layerDiffuseMaps, vec3(v_texCoordLayer0, 0.0));
#if LAYER_COUNT > 1	
	vec4 blendVals = texture(u_blendMap, v_texCoord0); //TODO should we have a base layer then up to 4 layers on top?
	diffuseColour.rgb = mix(diffuseColour.rgb, texture(u_layerDiffuseMaps, vec3(v_texCoordLayer1, 1.0)).rgb, blendVals.r);
#endif //1
#if LAYER_COUNT > 2
	diffuseColour.rgb = mix(diffuseColour.rgb, texture(u_layerDiffuseMaps, vec3(v_texCoordLayer2, 2.0)).rgb, blendVals.g);
#endif //2
#if LAYER_COUNT > 3
	diffuseColour.rgb = mix(diffuseColour.rgb, texture(u_layerDiffuseMaps, vec3(v_texCoordLayer3, 3.0)).rgb, blendVals.b);
#endif //3
#else
	vec4 diffuseColour = texture(u_diffuseTexture, v_texCoord0);
#endif //LAYER_COUNT
	vec3 baseColour = diffuseColour.rgb;

	
#if defined(BUMP)
#if defined(LAYER_COUNT) //blend layers into one normal map
	vec3 normalVector = texture(u_layerNormalMaps, vec3(v_texCoordLayer0, 0.0)).rgb;
#if LAYER_COUNT > 1	
	normalVector = mix(normalVector, texture(u_layerNormalMaps, vec3(v_texCoordLayer1, 1.0)).rgb, blendVals.r);
#endif //1
#if LAYER_COUNT > 2
	normalVector = mix(normalVector, texture(u_layerNormalMaps, vec3(v_texCoordLayer2, 2.0)).rgb, blendVals.g);
#endif //2
#if LAYER_COUNT > 3
	normalVector = mix(normalVector, texture(u_layerNormalMaps, vec3(v_texCoordLayer3, 3.0)).rgb, blendVals.b);
#endif //3
	normalVector = normalize(normalVector * 2.0 - 1.0);
#else
	vec3 normalVector = normalize(texture(u_normalTexture, v_texCoord0).rgb * 2.0 - 1.0);
#endif //LAYER_COUNT
#elif defined(FLATSHADE)
    vec3 x = dFdx(v_worldPosition);
    vec3 y = dFdy(v_worldPosition);
    vec3 normalVector = normalize(cross(x, y));
#else
	vec3 normalVector = v_normalVector;
#endif //BUMP

	vec3 lightDirection = normalize(v_lightDirection * 2.0);
	vec3 blendedColour = baseColour * u_ambientColour;
	float diffuseAmount = max(dot(normalVector, -lightDirection), 0.0);
	//half lambert term to reduce contrast (raise pow for more contrast)
	diffuseAmount = pow((diffuseAmount * 0.5) + 0.5, 3.0);

	blendedColour += u_lightColour * baseColour * diffuseAmount;
	
#if defined(BUMP)
	//calc specular
	vec3 eyeVec = normalize(v_cameraPosition);
	vec3 specularAngle = reflect(lightDirection, normalVector);//normalize(normalVector * diffuseAmount * 2.0 - lightDirection);

#if defined(MASK)
#if defined(LAYER_COUNT)
	vec4 mask = texture(u_layerMaskMaps, vec3(v_texCoordLayer0, 0.0));
#if LAYER_COUNT > 1
	mask = mix(mask, texture(u_layerMaskMaps, vec3(v_texCoordLayer1, 1.0)), blendVals.r);
#endif //1
#if LAYER_COUNT > 2
	mask = mix(mask, texture(u_layerMaskMaps, vec3(v_texCoordLayer2, 2.0)), blendVals.g);
#endif //2
#if LAYER_COUNT > 3
	mask = mix(mask, texture(u_layerMaskMaps, vec3(v_texCoordLayer3, 3.0)), blendVals.b);
#endif //3
#else
	vec4 mask = texture(u_maskTexture, v_texCoord0);
#endif //LAYER_COUNT	
	float exp = 255.0 * mask.r;
	float specAmount = mask.g;
#else
	const float exp = 5.0;
	const float specAmount = 0.25;	
#endif //MASK
	vec3 specularColour = vec3(pow(clamp(dot(specularAngle, eyeVec), 0.0, 1.0), exp));
	blendedColour += (specularColour * specAmount);
#endif //BUMP	
	outColour = vec4(blendedColour, diffuseColour.a);
	
	//outColour = vec4(vec3(v_distance), 1.0);
}