uniform sampler2D u_refractionTexture;
uniform sampler2D u_reflectionTexture;

#if defined(BUMP)
uniform sampler2D u_normalTexture;
uniform float u_time = 0.0;
#endif //BUMP

#if defined(LIGHTMAPPED)
uniform sampler2D u_lightmapTexture;
#endif //LIGHTMAPPED

uniform vec4 u_waterColour = vec4(0.36, 0.32, 0.2, 1.0);

uniform float u_textureRepeat = 1.0;

in vec4 v_vertexRefractionPosition;
in vec4 v_vertexReflectionPosition;

in vec2 v_texCoord0;
#if defined(LIGHTMAPPED)
in vec2 v_texCoord1;
#endif //LIGHTMAPPED
in vec3 v_eyeDirection;
in vec3 v_lightDirection;

const float distortAmount = 0.035;
const float specularAmount = 0.3;

const vec4 tangent = vec4(1.0, 0.0, 0.0, 0.0);
const vec4 viewNormal = vec4(0.0, 1.0, 0.0, 0.0);
const vec4 bitangent = vec4(0.0, 0.0, 1.0, 0.0);

out vec4 outColour;

vec2 fromClipSpace(vec4 position)
{
	return position.xy / position.w / 2.0 + 0.5;
}

void main()
{
	vec4 normal = vec4(0.0, 0.0, 1.0, 1.0);
	vec2 dudv = vec2(0.0);
	
#if defined(BUMP)
	vec2 coord = v_texCoord0 * u_textureRepeat;
#if defined (ANIMATED) //we have an animated normal map
	normal = texture(u_normalTexture, coord);
#else
	normal = texture(u_normalTexture, coord + u_time);
	coord *= 0.9;
	float time = u_time / 2.0;
	coord.x -= time;
	normal += texture(u_normalTexture, coord);
	normal /= 2.0;
#endif //ANIMATED
	normal = normalize(normal * 2.0 - 1.0);
	dudv = normal.rg * distortAmount;
#endif //BUMP
		
	//refraction sample
	vec2 textureCoord = fromClipSpace(v_vertexRefractionPosition) + dudv;
	textureCoord = clamp(textureCoord, 0.001, 0.999);
	vec4 refractionColour = texture(u_refractionTexture, textureCoord) * u_waterColour;
	
	//calc fog distance
	//----version 1 (exponential)----//
	//float z = gl_FragCoord.z / gl_FragCoord.w;
	//const float fogDensity = 0.0005;
	//float fogAmount = exp2(-fogDensity * fogDensity * z * z * 1.442695);
	//fogAmount = clamp(fogAmount, 0.0, 0.7);
	
	//----version 2 (linear)----//
	float z = (gl_FragCoord.z / gl_FragCoord.w) / 300.0; //const is max fog distance
	const float fogDensity = 6.0;
	float fogAmount = z * fogDensity;	
	fogAmount = clamp(fogAmount, 0.0, 1.0);
	
	refractionColour = mix(refractionColour, u_waterColour, fogAmount);

	//reflection sample
	textureCoord = fromClipSpace(v_vertexReflectionPosition) + dudv;
	textureCoord = clamp(textureCoord, 0.001, 0.999);	
	vec4 reflectionColour = texture(u_reflectionTexture, textureCoord);

	//use eye pos to work out blend amount
	//put view in tangent space - why don't we do this in vert shader?
	vec4 viewDir = normalize(vec4(v_eyeDirection, 1.0));
	vec4 viewTanSpace = normalize(vec4(dot(viewDir, tangent), dot(viewDir, bitangent), dot(viewDir, viewNormal), 1.0));	
	vec4 viewReflection = normalize(reflect(-1.0 * viewTanSpace, normal));
	float fresnel = 1.0 - dot(normal, viewReflection);

	//vec4 lightDir = normalize(vec4(v_lightDirection, 1.0));
	//vec4 lightTanSpace = normalize(vec4(dot(lightDir, tangent), dot(lightDir, bitangent), dot(lightDir, viewNormal), 1.0));
	//lightTanSpace = reflect(lightDir, normal);
	
	const float exponent = 240.0;
	vec3 specular = vec3(clamp(pow(dot(viewTanSpace, normal), exponent), 0.0, 1.0)) * specularAmount;

	vec3 colour = mix(refractionColour.rgb, reflectionColour.rgb, fresnel) + specular;
	//colour += max(dot(normal, -lightDir), 0.0);
	
#if defined(LIGHTMAPPED)	
	colour *= texture(u_lightmapTexture, v_texCoord1).rgb;
#endif //LIGHTMAPPED	

	outColour = vec4(colour, 1.0);
}