//renders a heightmap texture as a normal map. Will be incorrect at
//edges so will not work well with tiled heightmaps.

uniform sampler2D u_diffuseTexture;
uniform float u_amplitude = 40.0;

in vec2 v_texCoord;

out vec4 outColour;

float fetchOffset(float x, float y)
{
	return texture(u_diffuseTexture, v_texCoord + vec2(dFdx(v_texCoord.x) * x, dFdy(v_texCoord.y) * y)).a; //assumes we use a single channel heightmap
}

void main()
{
	vec3 normal;
	normal.x = -0.5 * (fetchOffset(1.0, 0.0) - fetchOffset(-1.0, 0.0));
	normal.y = -0.5 * (fetchOffset(0.0, 1.0) - fetchOffset(0.0, -1.0));
	normal.z = 1.0 / u_amplitude;
	normal = normalize(normal);
	outColour = vec4(normal, 1.0);
}