
in vec4 a_position;
in vec3 a_normal;
in vec2 a_texCoord0;

const float extrusion = 60.0;
uniform sampler2D u_alphaTexture;
uniform mat4 u_inverseTransposeWorldViewMatrix;
#define CLIP_PLANE

#if defined(REFLECTION)
uniform mat4 u_reflectionWorldViewProjectionMatrix;
#else
uniform mat4 u_worldViewProjectionMatrix;
#endif //REFLECTION

#if defined(CLIP_PLANE)
uniform mat4 u_worldMatrix;
uniform vec4 u_clipPlane = vec4(0.0, 0.0, 1.0, -2.0);
#endif //CLIP_PLANE

out vec2 v_texCoord;
out vec3 v_normal;
out vec3 v_worldPosition;

void main()
{
#if defined(REFLECTION)	
	gl_Position = u_reflectionWorldViewProjectionMatrix * a_position;
#else
	vec3 position = a_position.xyz + (a_normal * texture(u_alphaTexture, a_texCoord0).a * extrusion);
	v_worldPosition = (u_worldMatrix * vec4(position, 1.0)).xyz;
	gl_Position = u_worldViewProjectionMatrix * vec4(position, 1.0);
#endif //REFLECTION
	
	mat3 inverseTransposeWorldViewMatrix = mat3(u_inverseTransposeWorldViewMatrix[0].xyz, u_inverseTransposeWorldViewMatrix[1].xyz, u_inverseTransposeWorldViewMatrix[2].xyz);	
	v_normal = normalize(inverseTransposeWorldViewMatrix * a_normal);

#if defined(CLIP_PLANE)
	gl_ClipDistance[0] = dot(vec4(v_worldPosition, 1.0), u_clipPlane);
#endif //CLIP_PLANE

	v_texCoord = a_texCoord0;	
}