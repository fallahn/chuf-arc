in vec4 a_position;
in vec3 a_normal;
in vec2 a_texCoord0;

#if defined(LIGHTMAPPED)
in vec2 a_texCoord1;
#endif //LIGHTMAPPED

#if defined(BUMP)
in vec3 a_tangent;
in vec3 a_bitangent;
#endif //BUMP

#if defined(SKINNING)
in vec4 a_boneIndices;
in vec4 a_boneWeights;
#endif //SKINNING

#if defined(CLIP_PLANE)
uniform mat4 u_worldMatrix;
uniform mat4 u_inverseTransposeWorldMatrix;
uniform vec4 u_clipPlane;
#endif //CLIP_PLANE

#if defined(REFLECTION)
uniform mat4 u_reflectionWorldViewProjectionMatrix;
#else
uniform mat4 u_worldViewProjectionMatrix;
#endif //REFLECTION
uniform mat4 u_inverseTransposeWorldViewMatrix;

uniform vec3 u_lightDirection;

#if defined(BUMP)
uniform mat4 u_worldViewMatrix;
uniform mat4 u_viewMatrix;
uniform vec3 u_cameraWorldPosition;
#endif //BUMP

#if defined(SKINNING)
uniform mat4[80] u_boneMatrices; //TODO set size appropriately?
#endif //SKINNING

#if !defined(BUMP)
out vec3 v_normalVector;
#endif //BUMP

out vec2 v_texCoord0;
#if defined(LIGHTMAPPED)
out vec2 v_texCoord1;
#endif //LIGHTMAPPED
out vec3 v_lightDirection;

#if defined(BUMP)
out vec3 v_cameraPosition;
#endif //BUMP

void main()
{
	vec4 position = a_position;

#if defined(SKINNING)
	mat4 skinMatrix = u_boneMatrices[int(a_boneIndices.x)] * a_boneWeights.x;
	skinMatrix += u_boneMatrices[int(a_boneIndices.y)] * a_boneWeights.y;
	skinMatrix += u_boneMatrices[int(a_boneIndices.z)] * a_boneWeights.z;
	skinMatrix += u_boneMatrices[int(a_boneIndices.w)] * a_boneWeights.w;
	position = vec4((skinMatrix * position).xyz, a_position.w);
#endif //SKINNING

#if defined(REFLECTION)	
	gl_Position = u_reflectionWorldViewProjectionMatrix * position;
#else
	gl_Position = u_worldViewProjectionMatrix * position;
#endif //REFLECTION
	v_texCoord0 = a_texCoord0;
#if defined(LIGHTMAPPED)
	v_texCoord1 = a_texCoord1;
#endif //LIGHTMAPPED
	
	mat3 inverseTransposeWorldViewMatrix = mat3(u_inverseTransposeWorldViewMatrix[0].xyz, u_inverseTransposeWorldViewMatrix[1].xyz, u_inverseTransposeWorldViewMatrix[2].xyz);
	vec3 normalVector = normalize(inverseTransposeWorldViewMatrix * a_normal);
	
	
#if defined(BUMP)
	//TODO all positions need to be pre-multiplied with skinning matrix if needed
	vec3 tangent = normalize(inverseTransposeWorldViewMatrix * a_tangent); 
	vec3 bitangent = normalize(inverseTransposeWorldViewMatrix * a_bitangent);
	mat3 tangentSpaceTransformMatrix = mat3(tangent.x, bitangent.x, normalVector.x, tangent.y, bitangent.y, normalVector.y, tangent.z, bitangent.z, normalVector.z);
	
	v_lightDirection = tangentSpaceTransformMatrix * u_lightDirection;//(u_viewMatrix * vec4(u_lightDirection, 1.0)).xyz;
	
	vec4 worldViewPosition = u_worldViewMatrix * a_position;
	v_cameraPosition = tangentSpaceTransformMatrix * (u_worldViewMatrix * vec4(u_cameraWorldPosition, 1.0) - worldViewPosition).xyz;
	
#else
	v_lightDirection = u_lightDirection;
	v_normalVector = normalVector;
#endif //BUMP
	
#if defined(CLIP_PLANE)
	gl_ClipDistance[0] = dot(u_worldMatrix * a_position, u_clipPlane);
#endif //CLIP_PLANE	
	
}