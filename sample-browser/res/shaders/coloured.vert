in vec3 a_position;
in vec3 a_normal;
//in vec2 a_texCoord0;

uniform mat4 u_worldViewProjectionMatrix;
uniform mat4 u_inverseTransposeWorldViewMatrix;

out vec3 v_normalVector;
//out vec2 v_texCoord;

void main()
{
	gl_Position = u_worldViewProjectionMatrix * vec4(a_position, 1.0);
	mat3 inverseTransposeWorldViewMatrix = mat3(u_inverseTransposeWorldViewMatrix[0].xyz, u_inverseTransposeWorldViewMatrix[1].xyz, u_inverseTransposeWorldViewMatrix[2].xyz);
	v_normalVector = inverseTransposeWorldViewMatrix * a_normal;
	//v_texCoord = a_texCoord0;
}