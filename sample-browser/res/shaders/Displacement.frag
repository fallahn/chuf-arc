
in vec2 v_texCoord;
in vec3 v_normal;
in vec3 v_worldPosition;

uniform sampler2D u_diffuseTexture;
uniform sampler2D u_normalTexture;

out vec4 outColour;

const vec3 lightDir = vec3(0.5, -0.5, -0.5);

vec3 getBlend(vec3 normal)
{
	vec3 blending = abs(normal);
	blending = normalize(max(blending, 0.00001)); // Force weights to sum to 1.0
	float b = (blending.x + blending.y + blending.z);
	return blending /= vec3(b);
}

void main()
{
	vec3 x = dFdx(v_worldPosition);
    vec3 y = dFdy(v_worldPosition);
    vec3 normal = normalize(cross(x, y)); 

    //vec3 normal = texture(u_normalTexture, vec2(v_texCoord.x, 1.0 - v_texCoord.y)).rgb;

    //vec3 normal = normalize(v_normal);

    vec3 blending = getBlend(normal);
    vec4 xaxis = texture(u_diffuseTexture, v_worldPosition.yz * 0.01);
	vec4 yaxis = texture(u_diffuseTexture, v_worldPosition.xz * 0.01);
	vec4 zaxis = texture(u_diffuseTexture, v_worldPosition.xy * 0.01);

	vec3 colour = (xaxis * blending.x + yaxis * blending.y + zaxis * blending.z).rgb;

	//vec3 colour = texture(u_diffuseTexture, v_texCoord * 10.0).rgb;
	vec3 baseColour = colour * 0.4;
	float diffuseAmount = max(dot(normal, -lightDir), 0.0);
	baseColour += colour * diffuseAmount;
	outColour.rgb = baseColour;
	outColour.a = 1.0;
}