in vec4 a_position;
in vec2 a_texCoord0;
in vec2 a_texCoord1;

#if defined(CLIP_PLANE)
uniform mat4 u_worldMatrix;
uniform mat4 u_inverseTransposeWorldMatrix;
uniform vec4 u_clipPlane;
#endif //CLIP_PLANE

#if defined(REFLECTION)
uniform mat4 u_reflectionWorldViewProjectionMatrix;
#else
uniform mat4 u_worldViewProjectionMatrix;
#endif //REFLECTION

out vec2 v_texCoord0;
out vec2 v_texCoord1;

void main()
{
#if defined(REFLECTION)	
	gl_Position = u_reflectionWorldViewProjectionMatrix * a_position;
#else
	gl_Position = u_worldViewProjectionMatrix * a_position;
#endif //REFLECTION
	
#if defined(CLIP_PLANE)
	gl_ClipDistance[0] = dot(u_worldMatrix * a_position, u_clipPlane);
#endif //CLIP_PLANE

	v_texCoord0 = a_texCoord0;
	v_texCoord1 = a_texCoord1;
}