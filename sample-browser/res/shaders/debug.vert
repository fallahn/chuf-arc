attribute vec3 a_position;
attribute vec3 a_colour;

uniform mat4 u_worldViewProjectionMatrix;

varying vec3 v_colour;

void main()
{
	gl_Position = u_worldViewProjectionMatrix * vec4(a_position, 1.0);
	v_colour = a_colour;
}