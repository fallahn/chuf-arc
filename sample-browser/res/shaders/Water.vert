in vec4 a_position;
in vec2 a_texCoord0;

#if defined(LIGHTMAPPED)
in vec2 a_texCoord1;
#endif //LIGHTMAPPED

uniform mat4 u_worldMatrix;
uniform mat4 u_worldViewProjectionMatrix;
uniform mat4 u_reflectionWorldViewProjectionMatrix;
uniform vec3 u_lightDirection;
uniform vec3 u_cameraWorldPosition;

out vec4 v_vertexRefractionPosition;
out vec4 v_vertexReflectionPosition;

out vec2 v_texCoord0;
#if defined(LIGHTMAPPED)
out vec2 v_texCoord1;
#endif //LIGHTMAPPED
out vec3 v_eyeDirection;
out vec3 v_lightDirection;

void main()
{
	v_vertexRefractionPosition = u_worldViewProjectionMatrix * a_position;
	v_vertexReflectionPosition = u_reflectionWorldViewProjectionMatrix * a_position;
	
	gl_Position = v_vertexRefractionPosition;
	
	v_texCoord0 = a_texCoord0;
#if defined(LIGHTMAPPED)
	v_texCoord1 = a_texCoord1;
#endif //LIGHTMAPPED	
	
	v_eyeDirection = u_cameraWorldPosition - (u_worldMatrix * a_position).xyz;
	v_lightDirection = u_lightDirection;
}