
in vec2 v_texCoord;

uniform sampler2D u_diffuseTexture;
uniform sampler2D u_alphaTexture;

out vec4 outColour;

void main()
{
	outColour.rgb = texture(u_diffuseTexture, v_texCoord * 10.0).rgb;
	outColour.a = texture(u_alphaTexture, v_texCoord).a;
}