#version 330

varying vec3 v_normalVector;
varying vec2 v_texCoord;

uniform sampler2D u_diffuseTexture;

uniform vec3 u_lightDirection;// = normalize(vec3(0.8, -1.0, 0.2));
uniform vec3 u_lightColour = vec3(1.0, 1.0, 1.0);
//const vec3 baseColour = vec3(1.0, 0.5, 0.0);

void main()
{
	vec3 baseColour = texture(u_diffuseTexture, v_texCoord).rgb;
	vec3 normalVector = normalize(v_normalVector);
	vec3 blendedColour = baseColour * 0.25; //reduce to ambient colour
	float diffuseAmount = max(dot(normalVector, -u_lightDirection), 0.0);
	blendedColour += u_lightColour * baseColour * diffuseAmount;
	gl_FragColor = vec4(blendedColour, 1.0);
}