
in vec2 v_texCoord;

#if defined(VERTEX_COLOUR)
in vec4 v_colour;
#endif //VERTEX_COLOUR

uniform sampler2D u_diffuseTexture;

out vec4 outColour;

void main()
{
#if defined(TEXT)
	float alpha = texture(u_diffuseTexture, v_texCoord).a;
#else
	outColour = texture(u_diffuseTexture, v_texCoord);
#endif //TEXT
	
#if defined(VERTEX_COLOUR)
#if defined(TEXT)
	outColour = vec4(v_colour.rgb, alpha);
#else
	outColour *= v_colour;
#endif //TEXT
#else
	outColour *= vec4(1.0);
#endif //VERTEX_COLOUR	
}