/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Sample Browser Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <SampleBrowser.hpp>
#include <SampleTerrain.hpp>
#include <SampleBsp.hpp>
#include <SampleUI.hpp>
#include <SampleAlphaMask.hpp>

#include <chuf2/Image.hpp>
#include <chuf2/Reports.hpp>
#include <chuf2/Timer.hpp>
#include <chuf2/Font.hpp>

namespace
{
    //our app instance
    SampleBrowser sb;

    chuf::Timer fpsTimer;

    bool showUI = true;
}

SampleBrowser::SampleBrowser()
    : m_messageBus  (),
    m_stack         (m_messageBus),
    m_currentId     (SampleId::None)
{

}

//private
void SampleBrowser::start()
{
    //TODO register names with UI
    m_stack.registerSample<SampleTerrain>(SampleId::Terrain);
    m_stack.registerSample<SampleBsp>(SampleId::BSP);
    m_stack.registerSample<SampleUI>(SampleId::UI);
    m_stack.registerSample<SampleAlphaMask>(SampleId::AlphaMask);

    //m_stack.pushSample(SampleId::Terrain);
    m_stack.pushSample(SampleId::UI);
}

void SampleBrowser::update(float dt)
{
    while (!m_messageBus.empty())
    {
        auto msg = m_messageBus.poll();
        handleMessage(msg);
        m_stack.handleMessage(msg);
    }
    
    m_stack.update(dt);

    if (fpsTimer.elapsed().asSeconds() > 0.15f)
    {
        chuf::StatsReporter::reporter.report("FPS", std::to_string((int)getFramerate()));
        fpsTimer.restart();
    }
}

void SampleBrowser::draw()
{ 
    clear(chuf::Colour::black, ClearFlags::COLOUR_DEPTH);
    m_stack.draw();
}

void SampleBrowser::finish()
{
}

void SampleBrowser::handleKeyEvent(const chuf::Event::KeyEvent& evt)
{
    switch (evt.key.type)
    {
    case chuf::Event::KEY_PRESSED:
        switch (evt.key.keysym.sym)
        {
        case SDLK_ESCAPE:
            close();
            break;
        default: break;
        }
        break;
    case chuf::Event::KEY_RELEASED:
        switch (evt.key.keysym.sym)
        {
        case SDLK_F5:
        {
            auto img = getScreenshot();
            img->save("img_" + getDateTimeString() + ".png");
            break;
        }
        case SDLK_p:
            setVSyncEnabled(!vSyncEnabled());
            break;
        case SDLK_BACKSPACE:
            showUI = !showUI;
            if (showUI)
            {
                m_stack.pushSample(SampleId::UI);
            }
            else
            {
                m_stack.popSample();
            }
            break;
        default: break;
        }
        break;
    default:break;
    }
    m_stack.handleKeyEvent(evt);
}

void SampleBrowser::handleMouseEvent(const chuf::Event::MouseEvent& evt)
{
    m_stack.handleMouseEvent(evt);
}

void SampleBrowser::handleMessage(const Message& msg)
{
    if (msg.type == Message::Type::UI)
    {
        switch (msg.ui.type)
        {
        case Message::UIEvent::RequestStateChange:
            if (m_currentId == msg.ui.sampleId) break;

            m_stack.clearSamples();
            m_stack.pushSample(msg.ui.sampleId);
            m_currentId = msg.ui.sampleId;
            showUI = false;
            break;
        default: break;
        }
    }
}