/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Sample Browser Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <SampleBsp.hpp>

#include <chuf2/Log.hpp>
#include <chuf2/Util.hpp>

#include <chuf2/Shader.hpp>
#include <chuf2/Colour.hpp>
#include <chuf2/Light.hpp>
#include <chuf2/Material.hpp>
#include <chuf2/MaterialProperty.hpp>
#include <chuf2/Mesh.hpp>
#include <chuf2/SubMesh.hpp>
#include <chuf2/Model.hpp>
#include <chuf2/Image.hpp>
#include <chuf2/Texture.hpp>
#include <chuf2/ConfigFile.hpp>
#include <chuf2/Font.hpp>
#include <chuf2/Plane.hpp>
#include <chuf2/Reports.hpp>

#include <chuf2/shaders/GeomVis.hpp>

#include <chuf2/loaders/Iqm.hpp>
#include <chuf2/loaders/Bsp.hpp>

#include <iostream>

namespace
{
    chuf::Colour lightColour(1.f, 1.f, 0.9f);
    glm::vec3 getColour()
    {
        return glm::vec3(lightColour.r, lightColour.g, lightColour.b);
    }

    void printChildren(const chuf::ConfigObject::Ptr& co)
    {
        std::cout << co->getName() << ", " << co->getId() << std::endl;
        for (const auto& p : co->getProperties())
            std::cout << p->getName() << ", " << p->valueAsString() << std::endl;

        for (const auto& o : co->getObjects())
            printChildren(o);
    }

    const float mouseAcceleration = 0.1f;
    const float cameraMass = 1.8f;
    const float cameraFriction = 0.9f;
    const float cameraSpeed = 3.5f;

    float elapsedTime = 0.f;
    float getTime()
    {
        return elapsedTime / 10.f;
    }
}

SampleBsp::SampleBsp(MessageBus& mb)
    : Sample        (mb),
    m_modelNode     (nullptr),
    m_cameraNode    (nullptr),
    m_bspMap        (nullptr),
    m_buttonMask    (0u)
{
    chuf::StatsReporter::reporter.clear();

    m_scene = chuf::Scene::create("testScene");
    m_scene->enableReflections();

    auto camNode = chuf::Node::create("camNode");
    auto camera = chuf::Camera::createPerspective(75.f, 0.1f, 400.f, static_cast<float>(getApp().getVideoContext().width) / getApp().getVideoContext().height);//Camera::createOrthographic(8.f, 6.f, 0.1f, 120.f, 4.f/3.f);//

    camera->setReflectionPlane({ 0.f, 1.f, 0.f, 0.f });
    camera->setSkybox("res/materials/skybox.cmf");
    //camNode->setCamera(camera);
    auto camPtr = camNode->addComponent<chuf::Camera>(camera);

    auto camParent = chuf::Node::create("camParent");
    camParent->translate(0.f, 4.f, 14.f);
    camParent->addChild(camNode);
    m_scene->addNode(camParent);
    m_scene->setActiveCamera(camPtr);

    std::function<const glm::vec4()> e = std::bind(&chuf::Camera::getReflectionPlane, m_scene->getActiveCamera());

    auto lightNode = chuf::Node::create("lightNode");
    auto light = chuf::Light::createDirectional(chuf::Colour(0.7f, 0.7f, 0.68f));
    lightNode->addComponent<chuf::Light>(light);
    lightNode->rotate(chuf::Transform::Axis::Y, 15.f);
    lightNode->rotate(chuf::Transform::Axis::X, -30.f);

    std::function<glm::vec3()> f = std::bind(&chuf::Node::getForwardVectorView, lightNode.get());
    std::function<glm::vec3()> g = std::bind(&getColour);
    m_scene->addNode(lightNode);


    auto model = chuf::IqmModel::load("res/models/mrfixit.iqm", m_scene->getRenderer()/*, true*/);
    for (auto i = 0u; i < model->getSubMeshCount(); ++i)
    {
        model->getMaterial(i)->getProperty("u_clipPlane")->bindValue(e);
        model->getMaterial(i)->getProperty("u_lightDirection")->bindValue(f);
        model->getMaterial(i)->getProperty("u_lightColour")->bindValue(g);
        //model->getMaterial(i)->getProperty("u_boneMatrices")->setValue(model->getSkin()->getAnimation(0).getCurrentFrame(), 80);
    }

    auto modelNode = chuf::Node::create("modelNode");
    modelNode->addComponent<chuf::Model>(model);
    modelNode->translate({ 60.f, -25.f, 0.f });
    m_scene->addNode(modelNode);


    m_modelNode = m_scene->findNode("modelNode");
    assert(m_modelNode);
    m_cameraNode = m_scene->findNode("camNode");
    assert(m_cameraNode);

    getApp().setMouseCaptured(true);

    auto font = chuf::Font::create("res/fonts/VeraMono.ttf");
    m_text = std::make_unique<chuf::Text>(font);
    m_text->setPosition({ 30.f, 690.f });
    m_text->setHorizontalSpacing(4.f);
    m_text->setColour(chuf::Colour::red);
    m_text->setScale(0.5f);
    //m_text->setRotation(-30.f);

    m_sbReflectPreview = chuf::SpriteBatch::create(m_scene->getFrameBuffer(chuf::Scene::FBO::Reflection)->getRenderTexture(0)->getTexture());
    auto sprite = m_sbReflectPreview->addSprite({ 20.f, 300.f });
    sprite->setScale({ 0.5f, 0.5f });

    m_sbRefractPreview = chuf::SpriteBatch::create(m_scene->getFrameBuffer(chuf::Scene::FBO::Refraction)->getRenderTexture(0)->getTexture());
    sprite = m_sbRefractPreview->addSprite({ 20.f, 20.f });
    sprite->setScale({ 0.5f, 0.5f });

    std::function<float()> d = std::bind(&getTime);

    auto bspMap = chuf::BspMap::create("res/maps/trest_bsp.bsp", &m_scene->getRenderer(), chuf::BspMap::Type::Source);
    //auto bspMap = BspMap::create("res/maps/Level.bsp", &m_scene->getRenderer(), BspMap::Type::QuakeThree);
    //auto bspMap = BspMap::create("res/maps/ep1_c17_02.bsp", &m_scene->getRenderer(), BspMap::Type::Source);
    //auto bspMap = BspMap::create("res/maps/vq2dm1.bsp", &m_scene->getRenderer(), BspMap::Type::QuakeThree);
    bspMap->bindValueToMaterials("u_time", d);
    bspMap->bindValueToMaterials("u_clipPlane", e);
    bspMap->bindValueToMaterials("u_lightDirection", f);
    bspMap->bindValueToMaterials("u_lightColour", g);
    bspMap->setActiveCamera(m_scene->getActiveCamera());

    auto bspNode = chuf::Node::create("bspNode");
    m_bspMap = bspNode->addComponent<chuf::BspMap>(bspMap);
    bspNode->setScale(0.1f);
    bspNode->translate({ 0.f, -30.f, 0.f });
    //bspNode->rotate(Transform::Axis::Y, 50.f);
    m_scene->addNode(bspNode);
}

bool SampleBsp::update(float dt)
{
    m_scene->update(dt);
    
    //m_modelNode->rotate(Transform::Axis::Y, 35.5f * dt);
    //m_sprite->rotate(35.f * dt);

    elapsedTime += dt;

    auto model = m_modelNode->getComponent<chuf::Model>();
    model->getSkin()->getAnimation(0).update(dt);
    auto mat = model->getSkin()->getAnimation(0).getCurrentFrame();
    for (auto i = 0u; i < model->getSubMeshCount(); ++i)
    {
        model->getMaterial(i)->getProperty("u_boneMatrices")->setValue(mat, 80);
    }

    glm::vec3 force;
    if (m_buttonMask & Button::Forward)
        force += m_cameraNode->getForwardVectorWorld();
    if (m_buttonMask & Button::Back)
        force -= m_cameraNode->getForwardVectorWorld();
    if (m_buttonMask & Button::Left)
        force -= m_cameraNode->getRightVectorWorld();
    if (m_buttonMask & Button::Right)
        force += m_cameraNode->getRightVectorWorld();

    if (chuf::Util::Vector::lengthSquared(force) > 1.f) glm::normalize(force);

    m_cameraAcceleration += force / cameraMass;
    m_cameraAcceleration *= cameraFriction;
    if (chuf::Util::Vector::lengthSquared(m_cameraAcceleration) < 0.01f) m_cameraAcceleration = glm::vec3();

    m_cameraNode->getParent().translate(m_cameraAcceleration * cameraSpeed * dt);
    m_text->setString(chuf::StatsReporter::reporter.getString());
    return true;
}

void SampleBsp::draw()
{
    m_scene->getRenderer().draw();
    m_sbReflectPreview->draw();
    m_sbRefractPreview->draw();
    m_text->draw();
}

//-----event handlers-----//
bool SampleBsp::handleKeyEvent(const chuf::Event::KeyEvent& evt)
{
    switch (evt.key.type)
    {
    case chuf::Event::KEY_PRESSED:
        switch (evt.key.keysym.sym)
        {
        case SDLK_a:
            m_buttonMask |= Button::Left;
            break;
        case SDLK_s:
            m_buttonMask |= Button::Back;
            break;
        case SDLK_d:
            m_buttonMask |= Button::Right;
            break;
        case SDLK_w:
            m_buttonMask |= Button::Forward;
            break;
        default: break;
        }
        break;
    case chuf::Event::KEY_RELEASED:
        switch (evt.key.keysym.sym)
        {
        case SDLK_a:
            m_buttonMask &= ~Button::Left;
            break;
        case SDLK_s:
            m_buttonMask &= ~Button::Back;
            break;
        case SDLK_d:
            m_buttonMask &= ~Button::Right;
            break;
        case SDLK_w:
            m_buttonMask &= ~Button::Forward;
            break;
        default: break;
        }
        break;
    default: break;
    }
    return true;
}

bool SampleBsp::handleMouseEvent(const chuf::Event::MouseEvent& evt)
{
    switch (evt.type)
    {
    case chuf::Event::MOUSE_BUTTON_PRESSED:
        //m_clearColour = Colour::green();
        (evt.button.button == 1) ? m_buttonMask |= Button::Forward : m_buttonMask |= Button::Back;
        break;
    case chuf::Event::MOUSE_BUTTON_RELEASED:
        //m_clearColour = Colour::white();
        (evt.button.button == 1) ? m_buttonMask &= ~Button::Forward : m_buttonMask &= ~Button::Back;
        break;

    case chuf::Event::MOUSE_MOVE:
    {
        float xMovement = -evt.motion.xrel * mouseAcceleration;
        float yMovement = -evt.motion.yrel * mouseAcceleration;
        m_cameraNode->getParent().rotate(chuf::Transform::Axis::Y, xMovement);
        m_cameraNode->rotate(chuf::Transform::Axis::X, yMovement);
    }
        break;
    default: break;
    }

    return true;
}

void SampleBsp::handleMessage(const Message& msg)
{

}