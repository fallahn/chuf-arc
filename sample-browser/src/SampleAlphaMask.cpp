/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Sample Browser Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <SampleAlphaMask.hpp>
#include <MessageBus.hpp>

#include <chuf2/Node.hpp>
#include <chuf2/Camera.hpp>
#include <chuf2/Mesh.hpp>
#include <chuf2/Model.hpp>
#include <chuf2/Material.hpp>
#include <chuf2/MaterialProperty.hpp>
#include <chuf2/Image.hpp>
#include <chuf2/Texture.hpp>
#include <chuf2/Sprite.hpp>


namespace
{
    chuf::Sprite::Ptr sprite;
    const float divisor = 2.f;
}

SampleAlphaMask::SampleAlphaMask(MessageBus& mb)
    : Sample    (mb),
    m_fboMask   (nullptr),
    m_fboNormal (nullptr)
{  
    m_scene = chuf::Scene::create({});

    float sceneWidth = static_cast<float>(chuf::App::getInstance().getVideoContext().width);
    float sceneHeight = static_cast<float>(chuf::App::getInstance().getVideoContext().height);

    auto image = chuf::Image::create(static_cast<chuf::UInt32>(sceneWidth), static_cast<chuf::UInt32>(sceneHeight), chuf::Image::Format::A, chuf::Colour::blue);
    auto alphaTexture = chuf::Texture::create(image);
    auto rt = chuf::RenderTexture::create("alpha mask", alphaTexture);
    m_fboMask = chuf::FrameBuffer::create("alpha mask");    
    m_fboMask->setRenderTexture(rt);

    m_normalMapBatch = chuf::SpriteBatch::create(alphaTexture, chuf::Shader::create("res/shaders/Unlit.vert", "res/shaders/HeightToNormal.frag"));
    auto normalSprite = m_normalMapBatch->addSprite(); //add at least one full sized sprite
    //normalSprite->setScale({ 1.f, -1.f });
    //normalSprite->move({ 0.f, -sceneHeight });
    //normalSprite->setSubRect({ 0.f, sceneHeight, sceneWidth, -sceneHeight });

    image = chuf::Image::create(static_cast<chuf::UInt32>(sceneWidth), static_cast<chuf::UInt32>(sceneHeight), chuf::Image::Format::RGB, chuf::Colour::white);
    auto texture = chuf::Texture::create(image);
    rt = chuf::RenderTexture::create("normal map", texture);
    m_fboNormal = chuf::FrameBuffer::create("normal");
    m_fboNormal->setRenderTexture(rt);

    //auto camera = chuf::Camera::createOrthographic(sceneWidth, sceneHeight, 0.1f, 10.f, sceneWidth / sceneHeight);
    auto camera = chuf::Camera::createPerspective(60.f, 0.1f, 1000.f, sceneWidth / sceneHeight);
    const float angle = std::tan(camera->getFov() / 2.f);
    const float zDistance = (sceneHeight / 2.f) / angle;
    camera->setFarPlane(zDistance + 2.f);

    auto camNode = chuf::Node::create("camNode");
    camNode->addComponent<chuf::Camera>(camera);
    camNode->setTranslation({ 0.f, 0.f, zDistance });
    m_scene->addNode(camNode);

    auto model = chuf::Model::create(
        chuf::Mesh::createGridMesh({ sceneWidth, sceneHeight }, { 1, 1 }, true),
        &m_scene->getRenderer());

    auto material = chuf::Material::create("res/shaders/Unlit.vert", "res/shaders/AlphaMask.frag", chuf::RenderPass::Standard | chuf::RenderPass::AlphaBlend);
    material->getProperty("u_diffuseTexture")->setValue("res/images/cobbles.png"/*chuf::Texture::Sampler::create(texture)*/);
    material->getProperty("u_alphaTexture")->setValue(chuf::Texture::Sampler::create(alphaTexture));
    material->getStateBlock()->setBlend(true);
    material->getStateBlock()->setBlendDest(chuf::RenderState::BlendFunc::ONE_MINUS_SRC_ALPHA);
    material->getStateBlock()->setBlendSrc(chuf::RenderState::BlendFunc::SRC_ALPHA);
    material->getStateBlock()->setDepthTest(true);
    model->setMaterial(material);
    auto wallNode = chuf::Node::create("");
    wallNode->addComponent<chuf::Model>(model);
    m_scene->addNode(wallNode);

    model = chuf::Model::create(
        chuf::Mesh::createGridMesh({ sceneWidth, sceneHeight }, { 1, 1 }, true),
        &m_scene->getRenderer());

    material = chuf::Material::create("res/shaders/Unlit.vert", "res/shaders/Unlit.frag");
    material->getProperty("u_diffuseTexture")->setValue("res/images/background.jpg");
    material->getStateBlock()->setDepthTest(true);
    model->setMaterial(material);
    auto backNode = chuf::Node::create("wibble");
    backNode->addComponent<chuf::Model>(model);
    backNode->translate({ 0.f, 0.f, -1.f });
    m_scene->addNode(backNode);


    model = chuf::Model::create(
        chuf::Mesh::createGridMesh({ sceneWidth, sceneHeight }, { 200, 150 }, true),
        &m_scene->getRenderer());

    material = chuf::Material::create("res/shaders/Displacement.vert", "res/shaders/Displacement.frag");
    material->getProperty("u_diffuseTexture")->setValue("res/images/cobbles.png");
    material->getProperty("u_alphaTexture")->setValue(chuf::Texture::Sampler::create(alphaTexture));
    material->getProperty("u_normalTexture")->setValue(chuf::Texture::Sampler::create(texture));
    material->getStateBlock()->setDepthTest(true);
    material->getStateBlock()->setClip(true);
    model->setMaterial(material);
    auto dNode = chuf::Node::create("wubble");
    dNode->addComponent<chuf::Model>(model);
    dNode->translate({ 0.f, 0.f, 1.f }); //TODO move behind background and extrude forward
    m_scene->addNode(dNode);

    //TODO enable back face culling on materials


    m_brushBatch = chuf::SpriteBatch::create(chuf::Texture::create("res/images/circle.png"));
    sprite = m_brushBatch->addSprite();
    sprite->setOrigin(sprite->getSize() / 2.f);

    chuf::App::getInstance().setMouseCaptured(false);
}

bool SampleAlphaMask::update(float dt)
{   
    sprite->setPosition(chuf::App::getInstance().getView().mapPixelToView(chuf::App::getInstance().getMousePosition()));
    return false;
}

void SampleAlphaMask::draw()
{
    m_scene->getRenderer().draw();
    m_brushBatch->draw();
}

bool SampleAlphaMask::handleKeyEvent(const chuf::Event::KeyEvent&)
{
    //TODO switch between 2D/3D
    return false;
}

bool SampleAlphaMask::handleMouseEvent(const chuf::Event::MouseEvent& evt)
{
    if (evt.type == chuf::Event::MOUSE_BUTTON_PRESSED)
    {
        if (evt.button.button == SDL_BUTTON_LEFT)
        {
            markAlphaBuffer(true);
        }
        else if (evt.button.button == SDL_BUTTON_RIGHT)
        {
            markAlphaBuffer(false);
        }
    }

    return false;
}

void SampleAlphaMask::handleMessage(const Message&)
{

}

//private
void SampleAlphaMask::markAlphaBuffer(bool remove)
{
    auto ob = m_fboMask->bind();
    //TODO make this part of the state block of the material of the graphic being drawn
    if (remove) glBlendEquation(GL_FUNC_REVERSE_SUBTRACT);
    m_brushBatch->draw();
    glBlendEquation(GL_FUNC_ADD);
       
    m_fboNormal->bind();
    m_fboNormal->clear(chuf::Colour::transparent);
    m_normalMapBatch->draw();
    
    ob->bind();
}