/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Sample Browser Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <SampleStack.hpp>

#include <cassert>

void SampleStack::update(float dt)
{
    for (auto i = m_stack.rbegin(); i != m_stack.rend(); ++i)
    {
        if (!(*i)->update(dt)) break;
    }
    applyPendingChanges();
}

void SampleStack::draw()
{
    for (auto& s : m_stack) s->draw();
}

void SampleStack::handleKeyEvent(const chuf::Event::KeyEvent& evt)
{
    for (auto i = m_stack.rbegin(); i != m_stack.rend(); ++i)
    {
        if (!(*i)->handleKeyEvent(evt)) break;
    }
    applyPendingChanges();
}

void SampleStack::handleMouseEvent(const chuf::Event::MouseEvent& evt)
{
    for (auto i = m_stack.rbegin(); i != m_stack.rend(); ++i)
    {
        if (!(*i)->handleMouseEvent(evt)) break;
    }
    applyPendingChanges();
}

void SampleStack::handleMessage(const Message& msg)
{
    for (auto& s : m_stack)
    {
        s->handleMessage(msg);
    }
}

void SampleStack::pushSample(SampleId id)
{
    m_pendingChanges.emplace_back(Action::Push, id);
}

void SampleStack::popSample()
{
    m_pendingChanges.emplace_back(Action::Pop);
}

void SampleStack::clearSamples()
{
    m_pendingChanges.emplace_back(Action::Clear);
}

bool SampleStack::empty() const
{
    return m_stack.empty();
}

//private
Sample::Ptr SampleStack::createSample(SampleId id)
{
    auto result = m_sampleFactories.find(id);
    assert(result != m_sampleFactories.end());

    return result->second();
}

void SampleStack::applyPendingChanges()
{
    for (auto& change : m_pendingChanges)
    {
        switch (change.action)
        {
        case Action::Push:
            m_stack.emplace_back(createSample(change.id));
            break;
        case Action::Pop:
            m_stack.pop_back();
            break;
        case Action::Clear:
            m_stack.clear();
            break;
        default: break;
        }
    }
    m_pendingChanges.clear();
}