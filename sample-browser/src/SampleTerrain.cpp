/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Sample Browser license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <SampleTerrain.hpp>

#include <chuf2/Light.hpp>
#include <chuf2/Model.hpp>
#include <chuf2/Mesh.hpp>
#include <chuf2/Font.hpp>
#include <chuf2/Image.hpp>
#include <chuf2/SpriteBatch.hpp>
#include <chuf2/Terrain.hpp>
#include <chuf2/Reports.hpp>
#include <chuf2/MaterialProperty.hpp>
#include <chuf2/FrameBuffer.hpp>
#include <chuf2/Sprite.hpp>

#include <chuf2/Util.hpp>
#include <chuf2/glm.hpp>

#include <cassert>

#include <chuf2/stbi/stb_image.h>
#include <fstream>

#include <Catlib.hpp>

namespace
{
    const float mouseAcceleration = 0.1f;
    const float cameraMass = 1.8f;
    const float cameraFriction = 0.9f;
    const float cameraSpeed = 3.5f;
    
    chuf::Colour lightColour(0.88f, 0.85f, 0.88f);
    glm::vec3 getColour()
    {
        return glm::vec3(lightColour.r, lightColour.g, lightColour.b);
    }

    chuf::Terrain* terrain = nullptr;
    chuf::Timer timer;
    float getTime()
    {
        return timer.elapsed().asSeconds() * 0.07f;
    }

    std::vector<std::string> testImages = 
    {
        "res/images/terrain_test/water/Image00001.png",
        "res/images/terrain_test/water/Image00002.png",
        "res/images/terrain_test/water/Image00003.png",
        "res/images/terrain_test/water/Image00004.png",
        "res/images/terrain_test/water/Image00005.png",
        "res/images/terrain_test/water/Image00006.png",
        "res/images/terrain_test/water/Image00007.png",
        "res/images/terrain_test/water/Image00008.png",
        "res/images/terrain_test/water/Image00009.png",
        "res/images/terrain_test/water/Image00010.png",
        "res/images/terrain_test/water/Image00011.png",
        "res/images/terrain_test/water/Image00012.png",
        "res/images/terrain_test/water/Image00013.png",
        "res/images/terrain_test/water/Image00014.png",
        "res/images/terrain_test/water/Image00015.png",
        "res/images/terrain_test/water/Image00016.png",
        "res/images/terrain_test/water/Image00017.png",
        "res/images/terrain_test/water/Image00018.png",
        "res/images/terrain_test/water/Image00019.png",
        "res/images/terrain_test/water/Image00020.png",
        "res/images/terrain_test/water/Image00021.png",
        "res/images/terrain_test/water/Image00022.png",
        "res/images/terrain_test/water/Image00023.png",
        "res/images/terrain_test/water/Image00024.png",
        "res/images/terrain_test/water/Image00025.png",
        "res/images/terrain_test/water/Image00026.png",
        "res/images/terrain_test/water/Image00027.png",
        "res/images/terrain_test/water/Image00028.png",
        "res/images/terrain_test/water/Image00029.png"
    };

    std::vector<std::string> moreImages = 
    {
        "res/images/button_glow/01.png",
        "res/images/button_glow/02.png",
        "res/images/button_glow/03.png",
        "res/images/button_glow/04.png",
        "res/images/button_glow/05.png",
        "res/images/button_glow/06.png",
        "res/images/button_glow/07.png",
        "res/images/button_glow/08.png",
        "res/images/button_glow/09.png",
        "res/images/button_glow/10.png",
        "res/images/button_glow/11.png",
        "res/images/button_glow/12.png",
        "res/images/button_glow/13.png",
        "res/images/button_glow/14.png"
    };
}

SampleTerrain::SampleTerrain(MessageBus& mb)
    : Sample        (mb),
    m_cameraNode    (nullptr),
    m_buttonMask    (0u)
{       
    //Catlib::write("button.cat", moreImages, 16.f);
    chuf::StatsReporter::reporter.clear();

    m_scene = chuf::Scene::create("testScene");
    m_scene->enableReflections();

    auto camNode = chuf::Node::create("camNode");
    auto camera = chuf::Camera::createPerspective(75.f, 0.1f, 400.f, static_cast<float>(getApp().getVideoContext().width) / getApp().getVideoContext().height);

    auto camPtr = camNode->addComponent<chuf::Camera>(camera);
    camPtr->setSkybox("res/materials/skybox_night.cmf");

    auto camParent = chuf::Node::create("camParent");
    camParent->translate(0.f, 4.f, 14.f);
    camParent->addChild(camNode);
    m_scene->addNode(camParent);
    m_scene->setActiveCamera(camPtr);

    auto lightNode = chuf::Node::create("lightNode");
    auto light = chuf::Light::createDirectional(chuf::Colour(0.7f, 0.7f, 0.68f));
    lightNode->addComponent<chuf::Light>(light);
    lightNode->rotate(chuf::Transform::Axis::Y, 10.f);
    lightNode->rotate(chuf::Transform::Axis::X, -30.f);

    std::function<const glm::vec4()> e = std::bind(&chuf::Camera::getReflectionPlane, m_scene->getActiveCamera());
    std::function<glm::vec3()> f = std::bind(&chuf::Node::getForwardVectorView, lightNode.get());
    std::function<glm::vec3()> g = std::bind(&getColour);
    m_scene->addNode(lightNode);

    m_cameraNode = m_scene->findNode("camNode");
    assert(m_cameraNode);

    getApp().setMouseCaptured(true);

    auto font = chuf::Font::create("res/fonts/VeraMono.ttf");
    m_text = std::make_unique<chuf::Text>(font);
    m_text->setPosition({ 30.f, 680.f });
    m_text->setHorizontalSpacing(4.f);
    m_text->setColour(chuf::Colour::red);
    m_text->setScale(0.5f);

    auto heightData = chuf::HeightData::create("res/images/height_test.png", 0.f, 50.f);
    //auto heightData = chuf::HeightData::create("res/images/epic_heightmap.jpg", 0.f, 50.f);
    //auto heightData = chuf::HeightData::createFromRaw("res/images/height_test_8bit.raw", 64u, 64u);
    auto ter = chuf::Terrain::create(&m_scene->getRenderer(), heightData, glm::vec3(1), 64u, 3u, 1.f);
    auto tNode = chuf::Node::create("terrain");
    //tNode->translate({ 0.f, -15.f, 0.f });
    terrain = tNode->addComponent<chuf::Terrain>(ter);
    m_scene->addNode(tNode);

    terrain->getMaterial()->getProperty("u_clipPlane")->bindValue(e);
    terrain->getMaterial()->getProperty("u_lightDirection")->bindValue(f);
    terrain->getMaterial()->getProperty("u_lightColour")->bindValue(g);
    terrain->getMaterial()->getProperty("u_ambientColour")->setValue(glm::vec3(0.1f, 0.1f, 0.14f));
    m_scene->getActiveCamera()->addListener(*terrain);

    auto cube = chuf::Model::create(chuf::Mesh::createCube(), &m_scene->getRenderer());
    cube->setMaterial(chuf::Material::createFromFile("res/materials/crate.cmf"));
    cube->getMaterial()->getProperty("u_clipPlane")->bindValue(e);
    cube->getMaterial()->getProperty("u_lightDirection")->bindValue(f);
    cube->getMaterial()->getProperty("u_lightColour")->bindValue(g);
    auto cubeNode = chuf::Node::create("cube");
    cubeNode->addComponent<chuf::Model>(cube);
    cubeNode->setTranslation({ 0.f, 1.f, 10. });
    m_scene->addNode(cubeNode);

    auto reflectTexture = m_scene->getFrameBuffer(chuf::Scene::FBO::Reflection)->getRenderTexture(0)->getTexture();
    auto refractTexture = m_scene->getFrameBuffer(chuf::Scene::FBO::Refraction)->getRenderTexture(0)->getTexture();

    auto quad = chuf::Model::create(chuf::Mesh::createQuad({ -200.f, 0.f, -200.f }, { -200.f, 0.f, 200.f }, { 200.f, 0.f, -200.f }, { 200.f, 0.f, 200.f }), &m_scene->getRenderer());
    quad->setMaterial(chuf::Material::createFromFile("res/materials/water.cmf"));
    quad->getMaterial()->getProperty("u_lightDirection")->bindValue(f);
    quad->getMaterial()->getProperty("u_lightColour")->bindValue(g);
    quad->getMaterial()->getProperty("u_reflectionTexture")->setSampler(chuf::Texture::Sampler::create(reflectTexture));
    quad->getMaterial()->getProperty("u_reflectionTexture")->getSampler()->setFilter(chuf::Texture::Filter::Linear, chuf::Texture::Filter::Linear);
    quad->getMaterial()->getProperty("u_refractionTexture")->setSampler(chuf::Texture::Sampler::create(refractTexture));

    std::function<float()> h = std::bind(&getTime);
    quad->getMaterial()->getProperty("u_time")->bindValue(h);

    auto quadNode = chuf::Node::create("quad");
    quadNode->addComponent<chuf::Model>(quad);
    quadNode->translate({ 0.f, 1.f, 0.f });
    m_scene->addNode(quadNode);

    m_scene->getActiveCamera()->setReflectionPlane({ 0.f, 1.f, 0.f, -1.f });

    m_sbReflectPreview = chuf::SpriteBatch::create(reflectTexture);
    auto sprite = m_sbReflectPreview->addSprite({ 20.f, 20.f });
    sprite->setScale({ 0.5f, 0.5f });

    m_sbRefractPreview = chuf::SpriteBatch::create(refractTexture);
    sprite = m_sbRefractPreview->addSprite({ 20.f, 340.f });
    sprite->setScale({ 0.5f, 0.5f });
}

bool SampleTerrain::update(float dt)
{  
    m_scene->update(dt);

    glm::vec3 force;
    if (m_buttonMask & Button::Forward)
        force += m_cameraNode->getForwardVectorWorld();
    if (m_buttonMask & Button::Back)
        force -= m_cameraNode->getForwardVectorWorld();
    if (m_buttonMask & Button::Left)
        force -= m_cameraNode->getRightVectorWorld();
    if (m_buttonMask & Button::Right)
        force += m_cameraNode->getRightVectorWorld();

    if (chuf::Util::Vector::lengthSquared(force) > 1.f) glm::normalize(force);

    m_cameraAcceleration += force / cameraMass;
    m_cameraAcceleration *= cameraFriction;
    if (chuf::Util::Vector::lengthSquared(m_cameraAcceleration) > 0.01f)// m_cameraAcceleration = glm::vec3();    
        m_cameraNode->getParent().translate(m_cameraAcceleration * cameraSpeed * dt);

    m_text->setString(chuf::StatsReporter::reporter.getString());
    //m_text->setString("abcd\nefgh\nijkl");
    return true;
}

void SampleTerrain::draw()
{
    m_scene->getRenderer().draw();

    //m_sbReflectPreview->draw();
    //m_sbRefractPreview->draw();
    m_text->draw();
}

bool SampleTerrain::handleKeyEvent(const chuf::Event::KeyEvent& evt)
{
    switch (evt.key.type)
    {
    case chuf::Event::KEY_PRESSED:
        switch (evt.key.keysym.sym)
        {
        case SDLK_a:
            m_buttonMask |= Button::Left;
            break;
        case SDLK_s:
            m_buttonMask |= Button::Back;
            break;
        case SDLK_d:
            m_buttonMask |= Button::Right;
            break;
        case SDLK_w:
            m_buttonMask |= Button::Forward;
            break;
        default: break;
        }
        break;
    case chuf::Event::KEY_RELEASED:
        switch (evt.key.keysym.sym)
        {
        case SDLK_a:
            m_buttonMask &= ~Button::Left;
            break;
        case SDLK_s:
            m_buttonMask &= ~Button::Back;
            break;
        case SDLK_d:
            m_buttonMask &= ~Button::Right;
            break;
        case SDLK_w:
            m_buttonMask &= ~Button::Forward;
            break;
        default: break;
        }
        break;
    default: break;
    }
    return true;
}

bool SampleTerrain::handleMouseEvent(const chuf::Event::MouseEvent& evt)
{
    switch (evt.type)
    {
    case chuf::Event::MOUSE_BUTTON_PRESSED:
        //m_clearColour = Colour::green();
        (evt.button.button == 1) ? m_buttonMask |= Button::Forward : m_buttonMask |= Button::Back;
        break;
    case chuf::Event::MOUSE_BUTTON_RELEASED:
        //m_clearColour = Colour::white();
        (evt.button.button == 1) ? m_buttonMask &= ~Button::Forward : m_buttonMask &= ~Button::Back;
        break;

    case chuf::Event::MOUSE_MOVE:
    {
        float xMovement = -evt.motion.xrel * mouseAcceleration;
        float yMovement = -evt.motion.yrel * mouseAcceleration;
        m_cameraNode->getParent().rotate(chuf::Transform::Axis::Y, xMovement);
        m_cameraNode->rotate(chuf::Transform::Axis::X, yMovement);
    }
    break;
    default: break;
    }
    return true;
}

void SampleTerrain::handleMessage(const Message& msg)
{

}