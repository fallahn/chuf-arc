/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Sample Browser Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <SampleUI.hpp>

#include <chuf2/ui/Container.hpp>
#include <chuf2/ui/Button.hpp>
#include <chuf2/Texture.hpp>
#include <chuf2/Font.hpp>
#include <chuf2/Reports.hpp>

SampleUI::SampleUI(MessageBus& mb)
    : Sample(mb)
{
    auto font = chuf::Font::create("res/fonts/VeraMono.ttf");
    //m_desktop.setView({ { 80.f, 0.f, 800.f, 600.f } });
    
    auto& container = m_desktop.addControl<chuf::ui::Container>();
    auto& button = m_desktop.addControl<chuf::ui::Button>();
    button.setTexture(chuf::Texture::create("res/images/button_test.png", true, false), { 0.f, 0.f, 180.f, 47.f });
    button.setTranslation(20.f, 40.f);
    button.addCallback([this](chuf::ui::Control*)
    {
        Message msg;
        msg.type = Message::Type::UI;
        msg.ui.type = Message::UIEvent::RequestStateChange;
        msg.ui.sampleId = SampleId::BSP;
        postMessage(msg);

    }, chuf::ui::Button::Event::Activated);
    //button.setTogglable(true);
    button.setText("BSP", font);
    container.addControl(button);

    auto& button2 = m_desktop.addControl<chuf::ui::Button>();
    button2.setTexture(chuf::Texture::create("res/images/button_test.png", true, false), { 0.f, 47.f, 180.f, 47.f });
    button2.setTranslation(220.f, 40.f);
    button2.addCallback([this](chuf::ui::Control*)
    {
        Message msg;
        msg.type = Message::Type::UI;
        msg.ui.type = Message::UIEvent::RequestStateChange;
        msg.ui.sampleId = SampleId::Terrain;
        postMessage(msg);
    }, chuf::ui::Button::Event::Activated);
    button2.setText("Terrain", font);
    container.addControl(button2);

    auto& button3 = m_desktop.addControl<chuf::ui::Button>();
    button3.setTexture(chuf::Texture::create("res/images/button_test.png", true, false), { 180.f, 47.f, 180.f, 47.f });
    button3.setTranslation(440.f, 40.f);
    button3.setScale(2.f, 1.f);
    button3.addCallback([this](chuf::ui::Control*)
    {
        Message msg;
        msg.type = Message::Type::UI;
        msg.ui.type = Message::UIEvent::RequestStateChange;
        msg.ui.sampleId = SampleId::AlphaMask;
        postMessage(msg);
    }, chuf::ui::Button::Event::Activated);
    button3.setText("Alpha Masking", font);
    container.addControl(button3);

    m_desktop.addContainer(container);

    //TODO make a text / label control
    m_text = std::make_unique<chuf::Text>(font);
    m_text->setPosition({ 10.f, 10.f });
    m_text->setHorizontalSpacing(4.f);
    m_text->setColour(chuf::Colour::white);
    m_text->setScale(0.5f);
    m_text->setString("Backspace: Show / Hide UI    P: Enable / Disable VSync     F5: Screenshot    Escape: Quit");


    chuf::App::getInstance().setMouseCaptured(false);
}

SampleUI::~SampleUI()
{
    chuf::App::getInstance().setMouseCaptured(true);
}

//public
bool SampleUI::update(float dt)
{

    return true;
}

void SampleUI::draw()
{
    m_desktop.draw();
    m_text->draw();
}

bool SampleUI::handleKeyEvent(const chuf::Event::KeyEvent& evt)
{
    m_desktop.handleKeyEvent(evt);
    return false;
}

bool SampleUI::handleMouseEvent(const chuf::Event::MouseEvent& evt)
{
    m_desktop.handleMouseEvent(evt);
    return false;
}

void SampleUI::handleMessage(const Message& msg)
{

}