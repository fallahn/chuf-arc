CHUF 2.0 Sample Browser
-----------------------

Contains a selection of examples built with the CHUF 2.0 library.
To build enable 'build samples' in the CHUFlib CMake configuration.