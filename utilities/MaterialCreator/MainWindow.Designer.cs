﻿namespace MaterialCreator
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.shaderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cubeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sphereToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setGameDirectoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chooseClearColourToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chooseEnvironmentMapToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.glControl1 = new OpenTK.GLControl();
            this.checkBoxShowNormals = new System.Windows.Forms.CheckBox();
            this.trackBarNormalLength = new System.Windows.Forms.TrackBar();
            this.trackBarRoughness = new System.Windows.Forms.TrackBar();
            this.label12 = new System.Windows.Forms.Label();
            this.numericUpDownRepeatY = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownRepeatX = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonClearMask = new System.Windows.Forms.Button();
            this.buttonClearNormal = new System.Windows.Forms.Button();
            this.buttonClearDiffuse = new System.Windows.Forms.Button();
            this.panelColourPicker = new System.Windows.Forms.Panel();
            this.comboBoxColourTexture = new System.Windows.Forms.ComboBox();
            this.trackBarReflect = new System.Windows.Forms.TrackBar();
            this.trackBarSpecAmount = new System.Windows.Forms.TrackBar();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.trackBarSpecIntens = new System.Windows.Forms.TrackBar();
            this.checkBoxUseTransparency = new System.Windows.Forms.CheckBox();
            this.buttonBrowseMask = new System.Windows.Forms.Button();
            this.buttonBrowseNormal = new System.Windows.Forms.Button();
            this.buttonBrowseDiffuse = new System.Windows.Forms.Button();
            this.textBoxMaskPath = new System.Windows.Forms.TextBox();
            this.textBoxNormalPath = new System.Windows.Forms.TextBox();
            this.textBoxDiffusePath = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labelFps = new System.Windows.Forms.Label();
            this.textBoxLog = new System.Windows.Forms.TextBox();
            this.progressBarBatch = new System.Windows.Forms.ProgressBar();
            this.buttonBatchGo = new System.Windows.Forms.Button();
            this.buttonBrowseBatchOutput = new System.Windows.Forms.Button();
            this.buttonBrowseBatchInput = new System.Windows.Forms.Button();
            this.textBoxBatchOutput = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxBatchInput = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarNormalLength)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarRoughness)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRepeatY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRepeatX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarReflect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarSpecAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarSpecIntens)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.shaderToolStripMenuItem,
            this.viewToolStripMenuItem,
            this.optionsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(895, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.newToolStripMenuItem.Text = "New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.saveAsToolStripMenuItem.Text = "Save As...";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // shaderToolStripMenuItem
            // 
            this.shaderToolStripMenuItem.Name = "shaderToolStripMenuItem";
            this.shaderToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.shaderToolStripMenuItem.Text = "Material";
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cubeToolStripMenuItem,
            this.sphereToolStripMenuItem});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.viewToolStripMenuItem.Text = "Shape";
            // 
            // cubeToolStripMenuItem
            // 
            this.cubeToolStripMenuItem.Checked = true;
            this.cubeToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cubeToolStripMenuItem.Name = "cubeToolStripMenuItem";
            this.cubeToolStripMenuItem.Size = new System.Drawing.Size(108, 22);
            this.cubeToolStripMenuItem.Text = "Cube";
            this.cubeToolStripMenuItem.Click += new System.EventHandler(this.shapeToolStripMenuItem_Click);
            // 
            // sphereToolStripMenuItem
            // 
            this.sphereToolStripMenuItem.Name = "sphereToolStripMenuItem";
            this.sphereToolStripMenuItem.Size = new System.Drawing.Size(108, 22);
            this.sphereToolStripMenuItem.Text = "Sphere";
            this.sphereToolStripMenuItem.Click += new System.EventHandler(this.shapeToolStripMenuItem_Click);
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setGameDirectoryToolStripMenuItem,
            this.chooseClearColourToolStripMenuItem,
            this.chooseEnvironmentMapToolStripMenuItem});
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.optionsToolStripMenuItem.Text = "Options";
            // 
            // setGameDirectoryToolStripMenuItem
            // 
            this.setGameDirectoryToolStripMenuItem.Name = "setGameDirectoryToolStripMenuItem";
            this.setGameDirectoryToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.setGameDirectoryToolStripMenuItem.Text = "Set Game Directory";
            this.setGameDirectoryToolStripMenuItem.Click += new System.EventHandler(this.setGameDirectoryToolStripMenuItem_Click);
            // 
            // chooseClearColourToolStripMenuItem
            // 
            this.chooseClearColourToolStripMenuItem.Name = "chooseClearColourToolStripMenuItem";
            this.chooseClearColourToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.chooseClearColourToolStripMenuItem.Text = "Choose Background Colour";
            this.chooseClearColourToolStripMenuItem.Click += new System.EventHandler(this.chooseClearColourToolStripMenuItem_Click);
            // 
            // chooseEnvironmentMapToolStripMenuItem
            // 
            this.chooseEnvironmentMapToolStripMenuItem.Name = "chooseEnvironmentMapToolStripMenuItem";
            this.chooseEnvironmentMapToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.chooseEnvironmentMapToolStripMenuItem.Text = "Choose Environment Map";
            this.chooseEnvironmentMapToolStripMenuItem.Click += new System.EventHandler(this.chooseEnvironmentMapToolStripMenuItem_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 24);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            this.splitContainer1.Panel1MinSize = 550;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.labelFps);
            this.splitContainer1.Panel2.Controls.Add(this.textBoxLog);
            this.splitContainer1.Panel2.Controls.Add(this.progressBarBatch);
            this.splitContainer1.Panel2.Controls.Add(this.buttonBatchGo);
            this.splitContainer1.Panel2.Controls.Add(this.buttonBrowseBatchOutput);
            this.splitContainer1.Panel2.Controls.Add(this.buttonBrowseBatchInput);
            this.splitContainer1.Panel2.Controls.Add(this.textBoxBatchOutput);
            this.splitContainer1.Panel2.Controls.Add(this.label4);
            this.splitContainer1.Panel2.Controls.Add(this.label3);
            this.splitContainer1.Panel2.Controls.Add(this.textBoxBatchInput);
            this.splitContainer1.Panel2.Controls.Add(this.label2);
            this.splitContainer1.Panel2.Resize += new System.EventHandler(this.splitContainer1_Panel2_Resize);
            this.splitContainer1.Panel2MinSize = 175;
            this.splitContainer1.Size = new System.Drawing.Size(895, 512);
            this.splitContainer1.SplitterDistance = 650;
            this.splitContainer1.TabIndex = 1;
            // 
            // splitContainer2
            // 
            this.splitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.glControl1);
            this.splitContainer2.Panel1MinSize = 300;
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.checkBoxShowNormals);
            this.splitContainer2.Panel2.Controls.Add(this.trackBarNormalLength);
            this.splitContainer2.Panel2.Controls.Add(this.trackBarRoughness);
            this.splitContainer2.Panel2.Controls.Add(this.label12);
            this.splitContainer2.Panel2.Controls.Add(this.numericUpDownRepeatY);
            this.splitContainer2.Panel2.Controls.Add(this.numericUpDownRepeatX);
            this.splitContainer2.Panel2.Controls.Add(this.label11);
            this.splitContainer2.Panel2.Controls.Add(this.label1);
            this.splitContainer2.Panel2.Controls.Add(this.buttonClearMask);
            this.splitContainer2.Panel2.Controls.Add(this.buttonClearNormal);
            this.splitContainer2.Panel2.Controls.Add(this.buttonClearDiffuse);
            this.splitContainer2.Panel2.Controls.Add(this.panelColourPicker);
            this.splitContainer2.Panel2.Controls.Add(this.comboBoxColourTexture);
            this.splitContainer2.Panel2.Controls.Add(this.trackBarReflect);
            this.splitContainer2.Panel2.Controls.Add(this.trackBarSpecAmount);
            this.splitContainer2.Panel2.Controls.Add(this.label10);
            this.splitContainer2.Panel2.Controls.Add(this.label9);
            this.splitContainer2.Panel2.Controls.Add(this.label8);
            this.splitContainer2.Panel2.Controls.Add(this.trackBarSpecIntens);
            this.splitContainer2.Panel2.Controls.Add(this.checkBoxUseTransparency);
            this.splitContainer2.Panel2.Controls.Add(this.buttonBrowseMask);
            this.splitContainer2.Panel2.Controls.Add(this.buttonBrowseNormal);
            this.splitContainer2.Panel2.Controls.Add(this.buttonBrowseDiffuse);
            this.splitContainer2.Panel2.Controls.Add(this.textBoxMaskPath);
            this.splitContainer2.Panel2.Controls.Add(this.textBoxNormalPath);
            this.splitContainer2.Panel2.Controls.Add(this.textBoxDiffusePath);
            this.splitContainer2.Panel2.Controls.Add(this.label7);
            this.splitContainer2.Panel2.Controls.Add(this.label6);
            this.splitContainer2.Panel2.Controls.Add(this.label5);
            this.splitContainer2.Panel2MinSize = 140;
            this.splitContainer2.Size = new System.Drawing.Size(650, 512);
            this.splitContainer2.SplitterDistance = 319;
            this.splitContainer2.TabIndex = 1;
            // 
            // glControl1
            // 
            this.glControl1.BackColor = System.Drawing.Color.Black;
            this.glControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.glControl1.Location = new System.Drawing.Point(0, 0);
            this.glControl1.Name = "glControl1";
            this.glControl1.Size = new System.Drawing.Size(648, 317);
            this.glControl1.TabIndex = 0;
            this.glControl1.VSync = false;
            this.glControl1.MouseEnter += new System.EventHandler(this.glControl1_MouseEnter);
            this.glControl1.MouseLeave += new System.EventHandler(this.glControl1_MouseLeave);
            this.glControl1.Resize += new System.EventHandler(this.glControl1_Resize);
            // 
            // checkBoxShowNormals
            // 
            this.checkBoxShowNormals.AutoSize = true;
            this.checkBoxShowNormals.Location = new System.Drawing.Point(288, 156);
            this.checkBoxShowNormals.Name = "checkBoxShowNormals";
            this.checkBoxShowNormals.Size = new System.Drawing.Size(94, 17);
            this.checkBoxShowNormals.TabIndex = 18;
            this.checkBoxShowNormals.Text = "Show Normals";
            this.checkBoxShowNormals.UseVisualStyleBackColor = true;
            this.checkBoxShowNormals.CheckedChanged += new System.EventHandler(this.checkBoxShowNormals_CheckedChanged);
            // 
            // trackBarNormalLength
            // 
            this.trackBarNormalLength.Location = new System.Drawing.Point(382, 154);
            this.trackBarNormalLength.Minimum = 1;
            this.trackBarNormalLength.Name = "trackBarNormalLength";
            this.trackBarNormalLength.Size = new System.Drawing.Size(89, 45);
            this.trackBarNormalLength.TabIndex = 22;
            this.trackBarNormalLength.Value = 2;
            this.trackBarNormalLength.Scroll += new System.EventHandler(this.trackBarNormalLength_Scroll);
            // 
            // trackBarRoughness
            // 
            this.trackBarRoughness.Location = new System.Drawing.Point(288, 114);
            this.trackBarRoughness.Name = "trackBarRoughness";
            this.trackBarRoughness.Size = new System.Drawing.Size(183, 45);
            this.trackBarRoughness.TabIndex = 27;
            this.trackBarRoughness.Scroll += new System.EventHandler(this.trackBarRoughness_Scroll);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(477, 123);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(61, 13);
            this.label12.TabIndex = 28;
            this.label12.Text = "Roughness";
            // 
            // numericUpDownRepeatY
            // 
            this.numericUpDownRepeatY.Location = new System.Drawing.Point(105, 121);
            this.numericUpDownRepeatY.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numericUpDownRepeatY.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownRepeatY.Name = "numericUpDownRepeatY";
            this.numericUpDownRepeatY.Size = new System.Drawing.Size(31, 20);
            this.numericUpDownRepeatY.TabIndex = 26;
            this.numericUpDownRepeatY.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownRepeatY.ValueChanged += new System.EventHandler(this.numericUpDownRepeat_ValueChanged);
            // 
            // numericUpDownRepeatX
            // 
            this.numericUpDownRepeatX.Location = new System.Drawing.Point(69, 121);
            this.numericUpDownRepeatX.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numericUpDownRepeatX.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownRepeatX.Name = "numericUpDownRepeatX";
            this.numericUpDownRepeatX.Size = new System.Drawing.Size(30, 20);
            this.numericUpDownRepeatX.TabIndex = 25;
            this.numericUpDownRepeatX.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownRepeatX.ValueChanged += new System.EventHandler(this.numericUpDownRepeat_ValueChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(18, 123);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(45, 13);
            this.label11.TabIndex = 24;
            this.label11.Text = "Repeat:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(477, 157);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 23;
            this.label1.Text = "Length";
            // 
            // buttonClearMask
            // 
            this.buttonClearMask.Image = global::MaterialCreator.Properties.Resources.DeleteHS;
            this.buttonClearMask.Location = new System.Drawing.Point(239, 80);
            this.buttonClearMask.Name = "buttonClearMask";
            this.buttonClearMask.Size = new System.Drawing.Size(30, 23);
            this.buttonClearMask.TabIndex = 21;
            this.buttonClearMask.UseVisualStyleBackColor = true;
            this.buttonClearMask.Click += new System.EventHandler(this.buttonClearTexture_Click);
            // 
            // buttonClearNormal
            // 
            this.buttonClearNormal.Image = global::MaterialCreator.Properties.Resources.DeleteHS;
            this.buttonClearNormal.Location = new System.Drawing.Point(239, 41);
            this.buttonClearNormal.Name = "buttonClearNormal";
            this.buttonClearNormal.Size = new System.Drawing.Size(30, 23);
            this.buttonClearNormal.TabIndex = 20;
            this.buttonClearNormal.UseVisualStyleBackColor = true;
            this.buttonClearNormal.Click += new System.EventHandler(this.buttonClearTexture_Click);
            // 
            // buttonClearDiffuse
            // 
            this.buttonClearDiffuse.Image = global::MaterialCreator.Properties.Resources.DeleteHS;
            this.buttonClearDiffuse.Location = new System.Drawing.Point(239, 5);
            this.buttonClearDiffuse.Name = "buttonClearDiffuse";
            this.buttonClearDiffuse.Size = new System.Drawing.Size(30, 23);
            this.buttonClearDiffuse.TabIndex = 19;
            this.buttonClearDiffuse.UseVisualStyleBackColor = true;
            this.buttonClearDiffuse.Click += new System.EventHandler(this.buttonClearTexture_Click);
            // 
            // panelColourPicker
            // 
            this.panelColourPicker.BackColor = System.Drawing.Color.Fuchsia;
            this.panelColourPicker.Location = new System.Drawing.Point(239, 154);
            this.panelColourPicker.Name = "panelColourPicker";
            this.panelColourPicker.Size = new System.Drawing.Size(30, 21);
            this.panelColourPicker.TabIndex = 17;
            this.panelColourPicker.Click += new System.EventHandler(this.panelColourPicker_Click);
            // 
            // comboBoxColourTexture
            // 
            this.comboBoxColourTexture.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxColourTexture.FormattingEnabled = true;
            this.comboBoxColourTexture.Items.AddRange(new object[] {
            "Coloured",
            "Textured"});
            this.comboBoxColourTexture.Location = new System.Drawing.Point(129, 154);
            this.comboBoxColourTexture.Name = "comboBoxColourTexture";
            this.comboBoxColourTexture.Size = new System.Drawing.Size(104, 21);
            this.comboBoxColourTexture.TabIndex = 16;
            this.comboBoxColourTexture.SelectedIndexChanged += new System.EventHandler(this.comboBoxColourTexture_SelectedIndexChanged);
            // 
            // trackBarReflect
            // 
            this.trackBarReflect.Location = new System.Drawing.Point(288, 82);
            this.trackBarReflect.Name = "trackBarReflect";
            this.trackBarReflect.Size = new System.Drawing.Size(183, 45);
            this.trackBarReflect.TabIndex = 15;
            this.trackBarReflect.Value = 5;
            this.trackBarReflect.Scroll += new System.EventHandler(this.trackBarReflect_Scroll);
            // 
            // trackBarSpecAmount
            // 
            this.trackBarSpecAmount.Location = new System.Drawing.Point(288, 43);
            this.trackBarSpecAmount.Maximum = 20;
            this.trackBarSpecAmount.Name = "trackBarSpecAmount";
            this.trackBarSpecAmount.Size = new System.Drawing.Size(183, 45);
            this.trackBarSpecAmount.TabIndex = 14;
            this.trackBarSpecAmount.Value = 10;
            this.trackBarSpecAmount.Scroll += new System.EventHandler(this.trackBarSpecAmount_Scroll);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(477, 10);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(74, 13);
            this.label10.TabIndex = 13;
            this.label10.Text = "Spec Intensity";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(477, 46);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "Spec Amount";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(477, 85);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(55, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "Reflection";
            // 
            // trackBarSpecIntens
            // 
            this.trackBarSpecIntens.Location = new System.Drawing.Point(288, 7);
            this.trackBarSpecIntens.Maximum = 19;
            this.trackBarSpecIntens.Minimum = 1;
            this.trackBarSpecIntens.Name = "trackBarSpecIntens";
            this.trackBarSpecIntens.Size = new System.Drawing.Size(183, 45);
            this.trackBarSpecIntens.TabIndex = 10;
            this.trackBarSpecIntens.Value = 10;
            this.trackBarSpecIntens.Scroll += new System.EventHandler(this.trackBarSpecIntens_Scroll);
            // 
            // checkBoxUseTransparency
            // 
            this.checkBoxUseTransparency.AutoSize = true;
            this.checkBoxUseTransparency.Location = new System.Drawing.Point(21, 156);
            this.checkBoxUseTransparency.Name = "checkBoxUseTransparency";
            this.checkBoxUseTransparency.Size = new System.Drawing.Size(91, 17);
            this.checkBoxUseTransparency.TabIndex = 9;
            this.checkBoxUseTransparency.Text = "Transparency";
            this.checkBoxUseTransparency.UseVisualStyleBackColor = true;
            this.checkBoxUseTransparency.CheckedChanged += new System.EventHandler(this.checkBoxUseTransparency_CheckedChanged);
            // 
            // buttonBrowseMask
            // 
            this.buttonBrowseMask.Location = new System.Drawing.Point(203, 80);
            this.buttonBrowseMask.Name = "buttonBrowseMask";
            this.buttonBrowseMask.Size = new System.Drawing.Size(30, 23);
            this.buttonBrowseMask.TabIndex = 8;
            this.buttonBrowseMask.Text = "...";
            this.buttonBrowseMask.UseVisualStyleBackColor = true;
            this.buttonBrowseMask.Click += new System.EventHandler(this.buttonBrowseTexture_Click);
            // 
            // buttonBrowseNormal
            // 
            this.buttonBrowseNormal.Location = new System.Drawing.Point(203, 41);
            this.buttonBrowseNormal.Name = "buttonBrowseNormal";
            this.buttonBrowseNormal.Size = new System.Drawing.Size(30, 23);
            this.buttonBrowseNormal.TabIndex = 7;
            this.buttonBrowseNormal.Text = "...";
            this.buttonBrowseNormal.UseVisualStyleBackColor = true;
            this.buttonBrowseNormal.Click += new System.EventHandler(this.buttonBrowseTexture_Click);
            // 
            // buttonBrowseDiffuse
            // 
            this.buttonBrowseDiffuse.Location = new System.Drawing.Point(203, 5);
            this.buttonBrowseDiffuse.Name = "buttonBrowseDiffuse";
            this.buttonBrowseDiffuse.Size = new System.Drawing.Size(30, 23);
            this.buttonBrowseDiffuse.TabIndex = 6;
            this.buttonBrowseDiffuse.Text = "...";
            this.buttonBrowseDiffuse.UseVisualStyleBackColor = true;
            this.buttonBrowseDiffuse.Click += new System.EventHandler(this.buttonBrowseTexture_Click);
            // 
            // textBoxMaskPath
            // 
            this.textBoxMaskPath.Location = new System.Drawing.Point(67, 82);
            this.textBoxMaskPath.Name = "textBoxMaskPath";
            this.textBoxMaskPath.ReadOnly = true;
            this.textBoxMaskPath.Size = new System.Drawing.Size(130, 20);
            this.textBoxMaskPath.TabIndex = 5;
            // 
            // textBoxNormalPath
            // 
            this.textBoxNormalPath.Location = new System.Drawing.Point(67, 43);
            this.textBoxNormalPath.Name = "textBoxNormalPath";
            this.textBoxNormalPath.ReadOnly = true;
            this.textBoxNormalPath.Size = new System.Drawing.Size(130, 20);
            this.textBoxNormalPath.TabIndex = 4;
            // 
            // textBoxDiffusePath
            // 
            this.textBoxDiffusePath.Location = new System.Drawing.Point(67, 7);
            this.textBoxDiffusePath.Name = "textBoxDiffusePath";
            this.textBoxDiffusePath.ReadOnly = true;
            this.textBoxDiffusePath.Size = new System.Drawing.Size(130, 20);
            this.textBoxDiffusePath.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(18, 85);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(33, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Mask";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(18, 46);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Normal:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Diffuse:";
            // 
            // labelFps
            // 
            this.labelFps.AutoSize = true;
            this.labelFps.Location = new System.Drawing.Point(15, 374);
            this.labelFps.Name = "labelFps";
            this.labelFps.Size = new System.Drawing.Size(58, 13);
            this.labelFps.TabIndex = 10;
            this.labelFps.Text = "FPS: 60ish";
            // 
            // textBoxLog
            // 
            this.textBoxLog.BackColor = System.Drawing.SystemColors.ControlDark;
            this.textBoxLog.Location = new System.Drawing.Point(18, 180);
            this.textBoxLog.Multiline = true;
            this.textBoxLog.Name = "textBoxLog";
            this.textBoxLog.ReadOnly = true;
            this.textBoxLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxLog.Size = new System.Drawing.Size(182, 183);
            this.textBoxLog.TabIndex = 9;
            // 
            // progressBarBatch
            // 
            this.progressBarBatch.Location = new System.Drawing.Point(100, 132);
            this.progressBarBatch.Name = "progressBarBatch";
            this.progressBarBatch.Size = new System.Drawing.Size(100, 22);
            this.progressBarBatch.Step = 1;
            this.progressBarBatch.TabIndex = 8;
            // 
            // buttonBatchGo
            // 
            this.buttonBatchGo.Location = new System.Drawing.Point(18, 132);
            this.buttonBatchGo.Name = "buttonBatchGo";
            this.buttonBatchGo.Size = new System.Drawing.Size(75, 23);
            this.buttonBatchGo.TabIndex = 7;
            this.buttonBatchGo.Text = "Go!";
            this.buttonBatchGo.UseVisualStyleBackColor = true;
            this.buttonBatchGo.Click += new System.EventHandler(this.buttonBatchGo_Click);
            // 
            // buttonBrowseBatchOutput
            // 
            this.buttonBrowseBatchOutput.Location = new System.Drawing.Point(170, 90);
            this.buttonBrowseBatchOutput.Name = "buttonBrowseBatchOutput";
            this.buttonBrowseBatchOutput.Size = new System.Drawing.Size(25, 23);
            this.buttonBrowseBatchOutput.TabIndex = 6;
            this.buttonBrowseBatchOutput.Text = "...";
            this.buttonBrowseBatchOutput.UseVisualStyleBackColor = true;
            this.buttonBrowseBatchOutput.Click += new System.EventHandler(this.buttonBrowseBatchOutput_Click);
            // 
            // buttonBrowseBatchInput
            // 
            this.buttonBrowseBatchInput.Location = new System.Drawing.Point(170, 59);
            this.buttonBrowseBatchInput.Name = "buttonBrowseBatchInput";
            this.buttonBrowseBatchInput.Size = new System.Drawing.Size(25, 23);
            this.buttonBrowseBatchInput.TabIndex = 5;
            this.buttonBrowseBatchInput.Text = "...";
            this.buttonBrowseBatchInput.UseVisualStyleBackColor = true;
            this.buttonBrowseBatchInput.Click += new System.EventHandler(this.buttonBrowseBatchInput_Click);
            // 
            // textBoxBatchOutput
            // 
            this.textBoxBatchOutput.Location = new System.Drawing.Point(64, 93);
            this.textBoxBatchOutput.Name = "textBoxBatchOutput";
            this.textBoxBatchOutput.Size = new System.Drawing.Size(100, 20);
            this.textBoxBatchOutput.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 96);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Output:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Input:";
            // 
            // textBoxBatchInput
            // 
            this.textBoxBatchInput.Location = new System.Drawing.Point(64, 62);
            this.textBoxBatchInput.Name = "textBoxBatchInput";
            this.textBoxBatchInput.Size = new System.Drawing.Size(100, 20);
            this.textBoxBatchInput.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(15, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Batch:";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(895, 536);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(760, 520);
            this.Name = "MainWindow";
            this.Text = "Material Editor - VertexLit";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainWindow_FormClosing);
            this.Load += new System.EventHandler(this.MainWindow_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.trackBarNormalLength)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarRoughness)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRepeatY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRepeatX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarReflect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarSpecAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarSpecIntens)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem shaderToolStripMenuItem;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonBrowseBatchOutput;
        private System.Windows.Forms.Button buttonBrowseBatchInput;
        private System.Windows.Forms.TextBox textBoxBatchOutput;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxBatchInput;
        private System.Windows.Forms.Button buttonBatchGo;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setGameDirectoryToolStripMenuItem;
        private System.Windows.Forms.ProgressBar progressBarBatch;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.Button buttonBrowseMask;
        private System.Windows.Forms.Button buttonBrowseNormal;
        private System.Windows.Forms.Button buttonBrowseDiffuse;
        private System.Windows.Forms.TextBox textBoxMaskPath;
        private System.Windows.Forms.TextBox textBoxNormalPath;
        private System.Windows.Forms.TextBox textBoxDiffusePath;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TrackBar trackBarSpecIntens;
        private System.Windows.Forms.CheckBox checkBoxUseTransparency;
        private System.Windows.Forms.TrackBar trackBarReflect;
        private System.Windows.Forms.TrackBar trackBarSpecAmount;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private OpenTK.GLControl glControl1;
        private System.Windows.Forms.ToolStripMenuItem chooseClearColourToolStripMenuItem;
        private System.Windows.Forms.TextBox textBoxLog;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cubeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sphereToolStripMenuItem;
        private System.Windows.Forms.Label labelFps;
        private System.Windows.Forms.ComboBox comboBoxColourTexture;
        private System.Windows.Forms.Panel panelColourPicker;
        private System.Windows.Forms.CheckBox checkBoxShowNormals;
        private System.Windows.Forms.Button buttonClearDiffuse;
        private System.Windows.Forms.Button buttonClearMask;
        private System.Windows.Forms.Button buttonClearNormal;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TrackBar trackBarNormalLength;
        private System.Windows.Forms.NumericUpDown numericUpDownRepeatY;
        private System.Windows.Forms.NumericUpDown numericUpDownRepeatX;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TrackBar trackBarRoughness;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ToolStripMenuItem chooseEnvironmentMapToolStripMenuItem;
    }
}

