﻿/*********************************************************************
Matt Marchant 2015
http://trederia.blogspot.com

CHUF 2.0 Material Editor - Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//functions for handling OpenGL events in the main form

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using OpenTK;
using OpenTK.Input;
using OpenTK.Graphics.OpenGL4;

using Chuf;
using Chuf.Shaders;

namespace MaterialCreator
{
    public partial class MainWindow : Form
    {
        private Color m_clearColour = Color.CornflowerBlue;
        private Scene m_uiScene = new Scene();
        private Scene m_3dScene = new Scene();

        private Texture m_diffuseTexture = new Texture();
        private Texture m_normalTexture = new Texture();
        private Texture m_maskTexture = new Texture();

        private Texture m_skyboxTexture = new Texture();
        private Texture m_noiseTexture = new Texture();

        private const int m_viewHeight = 340;

        private Drawable m_cubeDrawable = null;
        private Drawable m_sphereDrawable = null;


        //---------------------------------------------------

        public void GlInitialise()
        {
            if(!Shader.Supported)
            {
                MessageBox.Show("Minimum Required OpenGL Version is 4.0\nWhich Is Not Supported By Current Hardware", "Error");
                this.Close();
            }
            
            glControl1.VSync = true;

            GLCheck.call(()=>GL.ClearColor(m_clearColour));           

            GLCheck.call(()=>GL.Enable(EnableCap.DepthTest));
            GLCheck.call(()=>GL.DepthFunc(DepthFunction.Less));

            GLCheck.call(()=>GL.Enable(EnableCap.CullFace));
            GLCheck.call(()=>GL.CullFace(CullFaceMode.Back));

            GLCheck.call(()=>GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha));

            loadShaders();
            buildScenes();
            updateAmbientColour();
        }

        public void GlUpdate()
        {
            //called repeatedly for message pump
            //dispatch draw and input handing events from here
            HandleEvents();

            Draw();
        }

        private void HandleEvents()
        {
            HandleMouse();
            //HandleKeyboard();
        }

        private MouseState m_currentMouseState, m_previousMouseState;
        private const float m_rotationMultiplier = 0.25f;
        private float m_fov = 75f;
        private const float m_maxfov = 175f;
        private const float m_minfov = 10f;
        private void HandleMouse()
        {
            m_currentMouseState = Mouse.GetState();
            if(m_currentMouseState != m_previousMouseState)
            {
                if (m_mouseInGlControl && m_currentMouseState[MouseButton.Left])
                {
                    var yRotation = (float)(m_currentMouseState.X - m_previousMouseState.X) * m_rotationMultiplier;
                    var xRotation = (float)(m_currentMouseState.Y - m_previousMouseState.Y) * m_rotationMultiplier;
                    m_cubeDrawable.rotate(Vector3.UnitY, yRotation);
                    m_cubeDrawable.rotate(Vector3.UnitX, xRotation);
                    m_sphereDrawable.rotate(Vector3.UnitY, yRotation);
                    m_sphereDrawable.rotate(Vector3.UnitX, xRotation);
                }

                if (m_mouseInGlControl)
                {
                    var scroll = (float)(m_currentMouseState.Wheel - m_previousMouseState.Wheel) * 5f;
                    m_fov = MathHelper.Clamp(m_fov - scroll, m_minfov, m_maxfov);
                    glControl1_Resize(null, EventArgs.Empty);
                }
            }
            m_previousMouseState = m_currentMouseState;
        }

        private void Draw()
        {
            if(this.Visible)
            { 
                GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
                m_3dScene.draw();
                m_uiScene.draw();
                glControl1.SwapBuffers();
            }
        }

        private void loadShaders() 
        {
       
        }

        private void buildScenes()
        {
            Drawable diffuseQuad = new Drawable(Drawable.Mesh.Quad);
            diffuseQuad.addPass("unlit", Shader.create(Unlit.Vert, Unlit.Frag, "#define DIFFUSE_MAP\n"));
            diffuseQuad.Translation = new Vector3(0.6f, 2.8f, 0f);
            diffuseQuad.setUniformValue("u_diffuseMap", m_diffuseTexture);

            Drawable normalQuad = new Drawable(Drawable.Mesh.Quad);
            normalQuad.addPass("unlit", Shader.create(Unlit.Vert, Unlit.Frag, "#define DIFFUSE_MAP\n"));
            normalQuad.Translation = new Vector3(0f, -1.1f, 0f);
            normalQuad.setUniformValue("u_diffuseMap", m_normalTexture);
            diffuseQuad.addChild(normalQuad);

            Drawable maskQuad = new Drawable(Drawable.Mesh.Quad);
            maskQuad.addPass("unlit", Shader.create(Unlit.Vert, Unlit.Frag, "#define DIFFUSE_MAP\n"));
            maskQuad.Translation = normalQuad.Translation;
            maskQuad.setUniformValue("u_diffuseMap", m_maskTexture);
            normalQuad.addChild(maskQuad);

            m_uiScene.addNode(diffuseQuad);

            m_cubeDrawable = new Drawable(Drawable.Mesh.Cube);
            m_cubeDrawable.addPass("unlitColour", Shader.create(Unlit.Vert, Unlit.Frag));
            m_cubeDrawable.addPass("unlitTexture", Shader.create(Unlit.Vert, Unlit.Frag, "#define DIFFUSE_MAP\n"), false);
            m_cubeDrawable.addPass("vertlitColour", Shader.create(Vertexlit.Vert, Vertexlit.Frag), false);
            m_cubeDrawable.addPass("vertlitTexture", Shader.create(Vertexlit.Vert, Vertexlit.Frag, "#define DIFFUSE_MAP\n"), false);
            m_cubeDrawable.addPass("vertlitBumped", Shader.create(Vertexlit.Vert, Vertexlit.Frag, "#define DIFFUSE_MAP\n#define BUMP\n"), false);
            m_cubeDrawable.addPass("vertlitMasked", Shader.create(Vertexlit.Vert, Vertexlit.Frag, "#define DIFFUSE_MAP\n#define BUMP\n#define MASK\n"), false);
            m_cubeDrawable.addPass("drawNormals", Shader.create(NormalVis.Vert, NormalVis.Geom, NormalVis.Frag, "#define BUMP\n"), false);
            m_cubeDrawable.setUniformValue("u_diffuseMap", m_diffuseTexture);
            m_cubeDrawable.setUniformValue("u_normalMap", m_normalTexture);
            m_cubeDrawable.setUniformValue("u_maskMap", m_maskTexture);
            m_cubeDrawable.scale(1.5f);

            m_sphereDrawable = new Drawable(Drawable.Mesh.Sphere);
            m_sphereDrawable.addPass("unlitColour", Shader.create(Unlit.Vert, Unlit.Frag));
            m_sphereDrawable.addPass("unlitTexture", Shader.create(Unlit.Vert, Unlit.Frag, "#define DIFFUSE_MAP\n"), false);
            m_sphereDrawable.addPass("vertlitColour", Shader.create(Vertexlit.Vert, Vertexlit.Frag), false);
            m_sphereDrawable.addPass("vertlitTexture", Shader.create(Vertexlit.Vert, Vertexlit.Frag, "#define DIFFUSE_MAP\n"), false);
            m_sphereDrawable.addPass("vertlitBumped", Shader.create(Vertexlit.Vert, Vertexlit.Frag, "#define DIFFUSE_MAP\n#define BUMP\n"), false);
            m_sphereDrawable.addPass("vertlitMasked", Shader.create(Vertexlit.Vert, Vertexlit.Frag, "#define DIFFUSE_MAP\n#define BUMP\n#define MASK\n"), false);
            m_sphereDrawable.addPass("drawNormals", Shader.create(NormalVis.Vert, NormalVis.Geom, NormalVis.Frag, "#define BUMP\n"), false);
            m_sphereDrawable.setUniformValue("u_diffuseMap", m_diffuseTexture);
            m_sphereDrawable.setUniformValue("u_normalMap", m_normalTexture);
            m_sphereDrawable.setUniformValue("u_maskMap", m_maskTexture);
            m_sphereDrawable.scale(2f);
            m_sphereDrawable.Hidden = true;

            m_3dScene.addNode(m_cubeDrawable);
            m_3dScene.addNode(m_sphereDrawable);
            m_3dScene.ActiveCamera.Translation = new Vector3(0f, 0f, 2f);

            m_skyboxTexture.load(m_gameDirectory + "\\res\\images\\cubemap.jpg", TextureTarget.TextureCubeMap);
            m_cubeDrawable.setUniformValue("u_skyMap", m_skyboxTexture);
            m_sphereDrawable.setUniformValue("u_skyMap", m_skyboxTexture);

            m_noiseTexture.load(Chuf.Image.createNoiseMap(512, 512));
            m_cubeDrawable.setUniformValue("u_noiseMap", m_noiseTexture);
            m_sphereDrawable.setUniformValue("u_noiseMap", m_noiseTexture);

        }

        private void updateAmbientColour()
        {
            Vector4 colour = new Vector4((float)m_clearColour.R / 255f, (float)m_clearColour.G / 255f, (float)m_clearColour.B / 255f, (float) m_clearColour.A / 255f);
            colour *= 0.2f; //TODO make this a variable in the UI

            m_cubeDrawable.setUniformValue("u_ambientColour", colour);
        }

        ///-----gl control event handlers---///
        private void glControl1_Resize(object sender, EventArgs e)
        {
            float ratio = (float)glControl1.Width / (float)glControl1.Height;
            float width = (float)m_viewHeight * ratio;
            
            m_uiScene.ActiveCamera.ProjectionMatrix = Matrix4.CreateOrthographicOffCenter(0f, width / 100f, 0f, (float)m_viewHeight / 100f, -0.1f, 1f);
            m_uiScene.ActiveCamera.ViewPort = new Rectangle(0, 0, glControl1.Width, glControl1.Height);

            m_3dScene.ActiveCamera.ProjectionMatrix = Matrix4.CreatePerspectiveFieldOfView(MathHelper.DegreesToRadians(m_fov), ratio, 0.1f, 12f);
            m_3dScene.ActiveCamera.ViewPort = m_uiScene.ActiveCamera.ViewPort;
        }

        private bool m_mouseInGlControl = false;
        private void glControl1_MouseEnter(object sender, EventArgs e)
        {
            m_mouseInGlControl = true;
        }

        private void glControl1_MouseLeave(object sender, EventArgs e)
        {
            m_mouseInGlControl = false;
        }
    }
}