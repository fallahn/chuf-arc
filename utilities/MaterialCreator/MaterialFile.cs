﻿/*********************************************************************
Matt Marchant 2015
http://trederia.blogspot.com

Crush Map Editor - Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Chuf;

namespace MaterialCreator
{
    sealed public class MaterialFile
    {
        public string Name { get { return m_name; } }
        private string m_name;
        public string DiffuseMap { get; set; }
        public string NormalMap { get; set; }
        public string MaskMap { get; set; }
        public bool Transparent { get; set; }
        public bool Debug { get; set; }
        private MaterialType m_type;
        public MaterialType Type { get { return m_type; } set { m_type = value; } }
        private string m_lastPath;

        public MaterialFile(MaterialType type, string name)
        {
            m_type = type;
            m_name = name;
        }

        public void save(string path = "")
        {
            if(string.IsNullOrEmpty(path))
            {
                if(!string.IsNullOrEmpty(m_lastPath))
                {
                    path = m_lastPath;
                }
                else
                {
                    Logger.Log("MaterialFile: Invalid save path");
                    return;
                }
            }

            if(!Directory.Exists(path))
            {
                Logger.Log("MaterialFile: " + path + " path does not exist.");
                return;
            }

            //TODO this is no longer true as we can replace with colour
            if(DiffuseMap == null || DiffuseMap == string.Empty)
            {
                MessageBox.Show("Material " + m_name + " must have at least a Diffuse Map", "Error");
                return;
            }

            StreamWriter file = new StreamWriter(path + "\\" + m_name + ".cmf");
            switch(m_type)
            {
                case MaterialType.Lightmapped:
                    file.WriteLine("material LightMapped");
                    break;
                case MaterialType.VertexLit:
                    file.WriteLine("material VertexLit");
                    break;
                case MaterialType.Unlit:
                default:
                    file.WriteLine("material Unlit");
                    break;
            }
            file.WriteLine("{");

            file.WriteLine("\tdiffuse = \"" + DiffuseMap.Replace('\\', '/') + "\"");
            if (m_type != MaterialType.Unlit)
            {
                if (NormalMap == null || NormalMap == string.Empty)
                    file.WriteLine("\t//normal = \"\"");
                else
                    file.WriteLine("\tnormal = \"" + NormalMap.Replace('\\', '/') + "\"");

                if (MaskMap == null || MaskMap == string.Empty)
                    file.WriteLine("\t//mask = \"\"");
                else
                    file.WriteLine("\tmask = \"" + MaskMap.Replace('\\', '/') + "\"");
            }
            file.WriteLine("}");
            file.Close();

            m_lastPath = path;
        }

        public static MaterialFile load(string path)
        {
            try
            {
                MaterialFile retVal = null;
                string name = Path.GetFileNameWithoutExtension(path);

                StreamReader file = new StreamReader(path);
                char[] delimiters = { ' ' };
                bool validFile = false;
                while(!file.EndOfStream)
                {
                    var words = file.ReadLine().Split(delimiters);
                    if (words.Length == 0) continue;

                    if(!validFile)
                    {
                        validFile = (words[0].ToLower() == "material");
                        if(validFile && words.Length > 1)
                        {
                            switch(words[1].ToLower())
                            {
                                case "unlit":
                                    retVal = new MaterialFile(MaterialType.Unlit, name);
                                    continue;
                                case "vertexlit":
                                    retVal = new MaterialFile(MaterialType.VertexLit, name);
                                    continue;
                                case "lightmapped":
                                    retVal = new MaterialFile(MaterialType.Lightmapped, name);
                                    continue;
                                case "water":
                                    retVal = new MaterialFile(MaterialType.Water, name);
                                    continue;
                                default:
                                    Logger.Log("Invalid material type found");
                                    validFile = false;
                                    continue;
                            }
                        }
                        else
                        {
                            validFile = false;
                        }
                        continue;
                    }
                    else //we have a valid file so parse properties
                    {
                        if(words.Length == 3)
                        {
                            //look for specific properties so that comments are automatically skipped
                            switch(words[0].ToLower().Replace("\t", string.Empty))
                            {
                                case "diffuse":
                                    retVal.DiffuseMap = words[2].Replace("\"", string.Empty);
                                    continue;
                                case "normal":
                                    retVal.NormalMap = words[2].Replace("\"", string.Empty);
                                    continue;
                                case "mask":
                                    retVal.MaskMap = words[2].Replace("\"", string.Empty);
                                    continue;
                                case "debug":
                                    retVal.Debug = (words[2].Replace("\"", string.Empty).ToLower() == "true");
                                    continue;
                                case "transparency":
                                    retVal.Transparent = (words[2].Replace("\"", string.Empty).ToLower() == "true");
                                    continue;
                                default: continue;
                            }
                        }
                    }
                }

                if(!validFile)
                {
                    Logger.Log(path + " not a valid material file.");
                    return null;
                }

                retVal.m_lastPath = path.Replace(Path.GetFileName(path), string.Empty);
                return retVal;
            }
            catch(Exception e)
            {
                Logger.Log(e.Message);
            }

            return null;
        }
    }
}
