﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MaterialCreator
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new MainWindow());

            //manual loop so we can despatch updates to GL control
            MainWindow mw = new MainWindow();
            mw.Show();
            mw.GlInitialise();
            while(mw.Visible)
            {
                Application.DoEvents();
                mw.GlUpdate();
            }
        }
    }
}
