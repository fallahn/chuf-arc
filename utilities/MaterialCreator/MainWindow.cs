﻿/*********************************************************************
Matt Marchant 2015
http://trederia.blogspot.com

CHUF 2.0 Material Editor - Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using OpenTK;
using OpenTK.Graphics.OpenGL4;

using Chuf;

namespace MaterialCreator
{
    public enum MaterialType
    {
        Unlit = 0,
        VertexLit,
        Lightmapped,
        Water
    }    
    
    public partial class MainWindow : Form
    {
        //global consts
        private const int controlPadding = 10;
        private string windowTitle = "Material Editor - ";
        private string REG_gameDirectory = "game_directory";
        private string REG_clearColour = "clear_colour";
        private const int maxConsoleLines = 50;
        
        //private members
        private string m_gameDirectory;       
        private MaterialType m_materialType = MaterialType.Unlit;
        private MaterialFile m_currentMaterialFile = null;


        //-----Main Window Initialisation----//
        public MainWindow()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
        }
        private void MainWindow_Load(object sender, EventArgs e)
        {
            ///----------------------------------------------------------------------------///
            ///NOTE: OpenGL context creation is not complete until this event is finished  ///
            ///DO NOT try calling anything on and GL controls from here - perform any      ///
            ///GL setup in GLInitialise()                                                  ///
            ///----------------------------------------------------------------------------///           
            
            //creates shader menu from enum
            var names = Enum.GetNames(typeof(MaterialType));
            foreach(var name in names)
            {
                ToolStripMenuItem item = new ToolStripMenuItem(name);
                item.Click += shaderToolStripMenuItem_Click;
                item.Tag = Enum.Parse(typeof(MaterialType), name);
                shaderToolStripMenuItem.DropDownItems.Add(item);
            }
            
            //call all resize events to set default layout
            shaderToolStripMenuItem_Click(shaderToolStripMenuItem.DropDownItems[0], EventArgs.Empty);
            splitContainer1_Panel2_Resize(splitContainer1.Panel2, EventArgs.Empty);


            //check registry for working directory
            m_gameDirectory = RegKey.read(REG_gameDirectory);
            if (m_gameDirectory == null)
                setGameDirectoryToolStripMenuItem_Click(null, EventArgs.Empty);

            string clearColour = RegKey.read(REG_clearColour);
            if (clearColour != null)
            {
                m_clearColour = ColorTranslator.FromHtml(clearColour);
            }


            log("Messages:");
            log("-----------------");
            Logger.Log = log;

            comboBoxColourTexture.SelectedIndex = 0;
        }

        private void MainWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Save current material first?", "Confirmation", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                if (m_currentMaterialFile == null)
                {
                    saveAsToolStripMenuItem_Click(null, EventArgs.Empty);
                }
                else
                {
                    saveToolStripMenuItem_Click(null, EventArgs.Empty);
                }
            }
        }

        //-----------events----------//
        /// <summary>
        /// Batch Process Panel Resize
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void splitContainer1_Panel2_Resize(object sender, EventArgs e)
        {
            var panel = (SplitterPanel)sender;
            buttonBrowseBatchInput.Location = new Point(panel.Width - (controlPadding + buttonBrowseBatchInput.Width), buttonBrowseBatchInput.Location.Y);
            buttonBrowseBatchOutput.Left = buttonBrowseBatchInput.Left;

            textBoxBatchInput.Width = buttonBrowseBatchInput.Left - textBoxBatchInput.Left - controlPadding;
            textBoxBatchOutput.Width = textBoxBatchInput.Width;

            progressBarBatch.Width = (buttonBrowseBatchInput.Left + buttonBrowseBatchInput.Width) - progressBarBatch.Left;

            textBoxLog.Width = textBoxLog.Left + buttonBatchGo.Width + progressBarBatch.Width - controlPadding;
        }
        /// <summary>
        /// Batch Input Button Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBrowseBatchInput_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            if (m_gameDirectory != null) fd.SelectedPath = m_gameDirectory;
            if (Directory.Exists(textBoxBatchInput.Text)) fd.SelectedPath = textBoxBatchInput.Text;
            if(fd.ShowDialog() == DialogResult.OK)
            {
                textBoxBatchInput.Text = fd.SelectedPath;
            }
        }
        /// <summary>
        /// Batch Output Button Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBrowseBatchOutput_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            if (m_gameDirectory != null) fd.SelectedPath = m_gameDirectory;
            if (Directory.Exists(textBoxBatchOutput.Text)) fd.SelectedPath = textBoxBatchOutput.Text;
            if (fd.ShowDialog() == DialogResult.OK)
            {
                textBoxBatchOutput.Text = fd.SelectedPath;
            }
        }
        /// <summary>
        /// Starts batch Creation Of Material Files
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonBatchGo_Click(object sender, EventArgs e)
        {
            if(Directory.Exists(textBoxBatchInput.Text)
                && Directory.Exists(textBoxBatchOutput.Text))
            {
                var files = Directory.GetFiles(textBoxBatchInput.Text);

                if (MessageBox.Show("Write " + files.Length.ToString() + " materials as " + Enum.GetName(typeof(MaterialType), m_materialType) + "?",
                    "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    progressBarBatch.Maximum = files.Length;
                    progressBarBatch.Value = 0;

                    foreach (var file in files)
                    {
                        if (validExtension(file))
                        {
                            var name = Path.GetFileNameWithoutExtension(file);
                            MaterialFile mf = new MaterialFile(m_materialType, name);
                            var dir = Path.GetFileName(m_gameDirectory);
                            var idx = file.IndexOf(dir) + dir.Length;
                            mf.DiffuseMap = file.Substring(idx);
                            mf.save(textBoxBatchOutput.Text);

                            progressBarBatch.PerformStep();
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Sets The Current Active Shader
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void shaderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach(ToolStripMenuItem item in shaderToolStripMenuItem.DropDownItems)
            {
                item.Checked = false;
            }

            var currentItem = (ToolStripMenuItem)sender;
            currentItem.Checked = true;

            this.Text = windowTitle + currentItem.Text;
            this.m_materialType = (MaterialType)currentItem.Tag;

            //we need to call this, but not on initial layout
            if(e != EventArgs.Empty)
                comboBoxColourTexture_SelectedIndexChanged(null, EventArgs.Empty);
        }
        /// <summary>
        /// Choose the current shape in preview window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void shapeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (ToolStripMenuItem item in viewToolStripMenuItem.DropDownItems)
            {
                item.Checked = false;
            }
            var currentItem = (ToolStripMenuItem)sender;
            currentItem.Checked = true;

            //switch active drawable
            if(currentItem.Text == "Cube")
            {
                m_sphereDrawable.Hidden = true;
                m_cubeDrawable.Hidden = false;
            }
            else
            {
                m_cubeDrawable.Hidden = true;
                m_sphereDrawable.Hidden = false;
            }
        }
        /// <summary>
        /// Sets Working Directory For Game Assets
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void setGameDirectoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.Description = "Select Game Directory";
            if (m_gameDirectory != null) fd.SelectedPath = m_gameDirectory;
            if(fd.ShowDialog() == DialogResult.OK)
            {
                m_gameDirectory = fd.SelectedPath;
                RegKey.write(REG_gameDirectory, m_gameDirectory);
            }
        }
        /// <summary>
        /// Chooses the Clear Colour for the GL Control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chooseClearColourToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ColorDialog cd = new ColorDialog();
            cd.Color = m_clearColour;
            cd.AllowFullOpen = true;
            cd.AnyColor = true;
            cd.FullOpen = true;

            if(cd.ShowDialog() == DialogResult.OK)
            {
                m_clearColour = cd.Color;
                GL.ClearColor(m_clearColour);
                RegKey.write(REG_clearColour, ColorTranslator.ToHtml(m_clearColour));

                updateAmbientColour();
            }
        }


        private void chooseEnvironmentMapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog od = new OpenFileDialog();
            od.Filter = "Supported Files|*.png;*.jpg;*.bmp";
            if (od.ShowDialog() == DialogResult.OK)
            {
                m_skyboxTexture.load(od.FileName, TextureTarget.TextureCubeMap);
            }
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //ask to save modified changes
            if(MessageBox.Show("Save current material first?", "Confirmation", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                if(m_currentMaterialFile == null)
                {
                    saveAsToolStripMenuItem_Click(null, EventArgs.Empty);
                }
                else
                {
                    saveToolStripMenuItem_Click(null, EventArgs.Empty);
                }
            }
            
            m_currentMaterialFile = null;
            m_diffuseTexture.clear();
            m_normalTexture.clear();
            m_maskTexture.clear();

            textBoxDiffusePath.Text = string.Empty;
            textBoxMaskPath.Text = string.Empty;
            textBoxNormalPath.Text = string.Empty;

            trackBarNormalLength.Value = 2;
            trackBarNormalLength_Scroll(null, EventArgs.Empty);
            trackBarReflect.Value = 5;
            trackBarReflect_Scroll(null, EventArgs.Empty);
            trackBarRoughness.Value = 0;
            trackBarRoughness_Scroll(null, EventArgs.Empty);
            trackBarSpecAmount.Value = 10;
            trackBarSpecAmount_Scroll(null, EventArgs.Empty);
            trackBarSpecIntens.Value = 10;
            trackBarSpecIntens_Scroll(null, EventArgs.Empty);

            comboBoxColourTexture.SelectedIndex = 0;
            checkBoxShowNormals.Checked = false;
            checkBoxUseTransparency.Checked = false;

            updateSelectedShader();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //ask to save modified changes
            if (MessageBox.Show("Save current material first?", "Confirmation", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                if (m_currentMaterialFile == null)
                {
                    saveAsToolStripMenuItem_Click(null, EventArgs.Empty);
                }
                else
                {
                    saveToolStripMenuItem_Click(null, EventArgs.Empty);
                }
            }
            
            OpenFileDialog od = new OpenFileDialog();
            od.Filter = "CHUF Material|*.cmf";
            if (od.ShowDialog() == DialogResult.OK)
            {
                m_currentMaterialFile = MaterialFile.load(od.FileName);

                //load textures
                if (!string.IsNullOrEmpty(m_currentMaterialFile.DiffuseMap))
                {
                    m_diffuseTexture.load(m_gameDirectory + "/" + m_currentMaterialFile.DiffuseMap);
                    comboBoxColourTexture.SelectedIndex = 1;
                    textBoxDiffusePath.Text = m_currentMaterialFile.DiffuseMap;
                }
                else
                {
                    m_diffuseTexture.clear();
                    comboBoxColourTexture.SelectedIndex = 0;
                    textBoxDiffusePath.Text = string.Empty;
                }
                if(!string.IsNullOrEmpty(m_currentMaterialFile.NormalMap))
                {
                    m_normalTexture.load(m_gameDirectory + "/" + m_currentMaterialFile.NormalMap);
                    textBoxNormalPath.Text = m_currentMaterialFile.NormalMap;
                }
                else
                {
                    m_normalTexture.clear();
                    textBoxNormalPath.Text = string.Empty;
                }
                if(!string.IsNullOrEmpty(m_currentMaterialFile.MaskMap))
                {
                    m_maskTexture.load(m_gameDirectory + "/" + m_currentMaterialFile.MaskMap);
                    textBoxMaskPath.Text = m_currentMaterialFile.MaskMap;
                }
                else
                {
                    m_maskTexture.clear();
                    textBoxMaskPath.Text = string.Empty;
                }

                //set checkboxes
                checkBoxShowNormals.Checked = m_currentMaterialFile.Debug;
                checkBoxUseTransparency.Checked = m_currentMaterialFile.Transparent;

                //select type in menu
                m_materialType = m_currentMaterialFile.Type;
                foreach (ToolStripMenuItem item in shaderToolStripMenuItem.DropDownItems)
                {
                    var type = (MaterialType)item.Tag;
                    if (type == m_materialType)
                    {
                        shaderToolStripMenuItem_Click(item, EventArgs.Empty);
                        break;
                    }
                }

                updateSelectedShader();
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(m_currentMaterialFile != null)
            {
                updateCurrentMaterialFile();
                m_currentMaterialFile.save();
            }
            else
            {
                Logger.Log("Current MaterialFile is null... someting went wrong.");
            }
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "CHUF Material|*.cmf";
            if(sfd.ShowDialog() == DialogResult.OK)
            {
                m_currentMaterialFile = new MaterialFile(m_materialType, Path.GetFileNameWithoutExtension(sfd.FileName));
                updateCurrentMaterialFile();
                m_currentMaterialFile.save(sfd.FileName.Replace(Path.GetFileName(sfd.FileName), string.Empty));
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //----------texture browse buttons--------//
        private void buttonBrowseTexture_Click(object sender, EventArgs e)
        {
            OpenFileDialog od = new OpenFileDialog();
            od.Filter = "Supported Files|*.png;*.jpg;*.bmp";
            if(od.ShowDialog() == DialogResult.OK)
            {
                var button = (Button)sender;
                switch (button.Name)
                {
                    case "buttonBrowseDiffuse":
                    m_diffuseTexture.load(od.FileName);
                    textBoxDiffusePath.Text = od.FileName.Replace(m_gameDirectory, string.Empty);
                    break;
                    case "buttonBrowseNormal":
                    m_normalTexture.load(od.FileName);
                    textBoxNormalPath.Text = od.FileName.Replace(m_gameDirectory, string.Empty);
                    break;
                    case "buttonBrowseMask":
                    m_maskTexture.load(od.FileName);
                    textBoxMaskPath.Text = od.FileName.Replace(m_gameDirectory, string.Empty);
                    break;
                    default: break;
                }
                comboBoxColourTexture_SelectedIndexChanged(null, EventArgs.Empty);
            }
        }

        private void buttonClearTexture_Click(object sender, EventArgs e)
        {
            var button = (Button)sender;
            switch (button.Name)
            {
                case "buttonClearDiffuse":
                    m_diffuseTexture.clear();
                    textBoxDiffusePath.Text = string.Empty;
                    break;
                case "buttonClearNormal":
                    m_normalTexture.clear();
                    textBoxNormalPath.Text = string.Empty;
                    break;
                case "buttonClearMask":
                    m_maskTexture.clear();
                    textBoxMaskPath.Text = string.Empty;
                    break;
                default: break;
            }
            comboBoxColourTexture_SelectedIndexChanged(null, EventArgs.Empty);
        }

        //switch between coloured and textured
        private void comboBoxColourTexture_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (m_cubeDrawable == null || m_sphereDrawable == null) return;

            updateSelectedShader();

            checkBoxShowNormals_CheckedChanged(null, EventArgs.Empty);
        }

        private void panelColourPicker_Click(object sender, EventArgs e)
        {
            ColorDialog cd = new ColorDialog();
            cd.Color = panelColourPicker.BackColor;
            cd.AllowFullOpen = true;
            cd.FullOpen = true;
            if(cd.ShowDialog() == DialogResult.OK)
            {
                m_sphereDrawable.setUniformValue("u_diffuseColour", cd.Color);
                m_cubeDrawable.setUniformValue("u_diffuseColour", cd.Color);
                panelColourPicker.BackColor = cd.Color;
            } 
        }
        
        private void checkBoxShowNormals_CheckedChanged(object sender, EventArgs e)
        {
            m_cubeDrawable.setPassEnabled("drawNormals", checkBoxShowNormals.Checked);
            m_sphereDrawable.setPassEnabled("drawNormals", checkBoxShowNormals.Checked);
        }

        private void trackBarSpecIntens_Scroll(object sender, EventArgs e)
        {
            m_cubeDrawable.setUniformValue("u_specularIntensity", (float)trackBarSpecIntens.Value / 10f);
            m_sphereDrawable.setUniformValue("u_specularIntensity", (float)trackBarSpecIntens.Value / 10f);
        }

        private void trackBarSpecAmount_Scroll(object sender, EventArgs e)
        {
            m_cubeDrawable.setUniformValue("u_specularAmount", (float)trackBarSpecAmount.Value / 10f);
            m_sphereDrawable.setUniformValue("u_specularAmount", (float)trackBarSpecAmount.Value / 10f);
        }

        private void trackBarRoughness_Scroll(object sender, EventArgs e)
        {
            m_cubeDrawable.setUniformValue("u_roughness", trackBarRoughness.Value / 100f);
            m_sphereDrawable.setUniformValue("u_roughness", trackBarRoughness.Value / 100f);
        }

        private void checkBoxUseTransparency_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxUseTransparency.Checked)
            {
                GLCheck.call(() => GL.Enable(EnableCap.Blend));
                GLCheck.call(() => GL.Disable(EnableCap.CullFace));
            }
            else
            {
                GLCheck.call(() => GL.Disable(EnableCap.Blend));
                GLCheck.call(() => GL.Enable(EnableCap.CullFace));
            }
        }

        private void trackBarNormalLength_Scroll(object sender, EventArgs e)
        {
            m_cubeDrawable.setUniformValue("u_lineLength", (float)trackBarNormalLength.Value / 100f);
            m_sphereDrawable.setUniformValue("u_lineLength", (float)trackBarNormalLength.Value / 100f);
        }

        private void trackBarReflect_Scroll(object sender, EventArgs e)
        {
            m_cubeDrawable.setUniformValue("u_reflectionAmount", trackBarReflect.Value / 10f);
            m_sphereDrawable.setUniformValue("u_reflectionAmount", trackBarReflect.Value / 10f);
        }

        private void numericUpDownRepeat_ValueChanged(object sender, EventArgs e)
        {
            m_cubeDrawable.setUniformValue("u_textureRepeat", new Vector2((float)numericUpDownRepeatX.Value, (float)numericUpDownRepeatY.Value));
            m_sphereDrawable.setUniformValue("u_textureRepeat", new Vector2((float)numericUpDownRepeatX.Value, (float)numericUpDownRepeatY.Value));
        }

        //--------------misc functions----------------//
        private List<string> imageExtensions = new List<string>{".png", ".tga", ".bmp", ".jpg"};
        private bool validExtension(string path)
        {
            var ext = Path.GetExtension(path);
            return (imageExtensions.Find(x => x == ext) != String.Empty);
        }

        private void log(string message)
        {
            textBoxLog.AppendText(message + Environment.NewLine);
            var lineCount = textBoxLog.Lines.Length;
            if(lineCount > maxConsoleLines)
            {
                var lines = textBoxLog.Lines;
                var newLines = lines.Skip(lineCount - maxConsoleLines);
                textBoxLog.Lines = newLines.ToArray();
            }

            Debug.WriteLine(message);
        }

        private void updateSelectedShader()
        {
            m_cubeDrawable.disableAllPasses();
            m_sphereDrawable.disableAllPasses();

            switch (m_materialType)
            {
                default:
                case MaterialType.Unlit:
                    if (comboBoxColourTexture.SelectedIndex == 0) //coloured
                    {
                        m_cubeDrawable.setPassEnabled("unlitColour", true);
                        m_sphereDrawable.setPassEnabled("unlitColour", true);
                    }
                    else //textured
                    {
                        m_cubeDrawable.setPassEnabled("unlitTexture", true);
                        m_sphereDrawable.setPassEnabled("unlitTexture", true);
                    }
                    break;
                case MaterialType.VertexLit:
                    if (comboBoxColourTexture.SelectedIndex == 0) //colour
                    {
                        m_cubeDrawable.setPassEnabled("vertlitColour", true);
                        m_sphereDrawable.setPassEnabled("vertlitColour", true);
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(textBoxDiffusePath.Text)
                            && !string.IsNullOrEmpty(textBoxNormalPath.Text)
                            && string.IsNullOrEmpty(textBoxMaskPath.Text))
                        {
                            //we only have normal and diffuse
                            m_cubeDrawable.setPassEnabled("vertlitBumped", true);
                            m_sphereDrawable.setPassEnabled("vertlitBumped", true);
                        }
                        else if (!string.IsNullOrEmpty(textBoxDiffusePath.Text)
                            && !string.IsNullOrEmpty(textBoxNormalPath.Text)
                            && !string.IsNullOrEmpty(textBoxMaskPath.Text))
                        {
                            //we have all three maps
                            m_cubeDrawable.setPassEnabled("vertlitMasked", true);
                            m_sphereDrawable.setPassEnabled("vertlitMasked", true);
                        }
                        else
                        {
                            //just use the diffuse map
                            m_cubeDrawable.setPassEnabled("vertlitTexture", true);
                            m_sphereDrawable.setPassEnabled("vertlitTexture", true);
                        }
                    }
                    break;
            }
        }

        private void updateCurrentMaterialFile()
        {
            m_currentMaterialFile.Type = m_materialType;
            m_currentMaterialFile.DiffuseMap = textBoxDiffusePath.Text;
            m_currentMaterialFile.NormalMap = textBoxNormalPath.Text;
            m_currentMaterialFile.MaskMap = textBoxMaskPath.Text;
            m_currentMaterialFile.Transparent = checkBoxUseTransparency.Checked;
            m_currentMaterialFile.Debug = checkBoxShowNormals.Checked;
            //TODO other slider options / colour
        }
    }
}
