CHUF Material Editor
--------------------

CME is a simple GUI editor used to create and edit cmf fles used with
CHUF 2.0. The material editor uses winforms and OpenTK for OpenGL support
and also depends on ChufLib which is used for GL specific classes.

CME is tested to work on both Windows and Linux under mono. When using mono
you will need to create /etc/mono/registry and execute chmod ugo+rw to
enable the registry settings used by CHUF Material Editor.

CHUF Material Editor is released under the zlib license.