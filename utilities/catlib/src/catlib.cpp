/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

catlib Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#ifndef STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION
#define STBI_ASSERT(x)
//#define STB_IMAGE_STATIC
#endif //STB_IMAGE_IMPLEMENTAION

#include <stb_image.h>
#include <catlib.hpp>

#include <exception>
#include <fstream>
#include <cstring>
#include <cmath>

namespace
{
    const std::size_t maxFrames = 255u;
    const unsigned int curIdent = 0x544e4143;
    const unsigned int curVersion = 1u;
}

void Catlib::write(const std::string& outfile, const std::vector<std::string>& infiles, float framerate)
{
    framerate = std::abs(framerate);

    if (infiles.size() > maxFrames)
    {
        throw("too many frames - maximum is 255");
    }

    int w, h, bpp;
    Properties properties;
    //check the first image by loading it. if it's valid store properties and check rest of the images
    auto data = stbi_load(infiles[0].c_str(), &w, &h, &bpp, 0);
    if (!data)
    {
        std::string msg = "unable to validate frame 0, reason: " + std::string(stbi_failure_reason());
        throw std::runtime_error(msg.c_str());
    }
    stbi_image_free(data);

    properties.width = w;
    properties.height = h;
    properties.depth = bpp;
    properties.framerate = framerate;


    //validate rest of frames - TODO can we do this faster without having to load every single image?
    for (auto i = 1u; i < infiles.size(); ++i)
    {
        data = stbi_load(infiles[i].c_str(), &w, &h, &bpp, 0);
        if (!data)
        {
            std::string msg = "unable to validate frame " + std::to_string(i) + ", reason: " + std::string(stbi_failure_reason());
            throw std::runtime_error(msg.c_str());
        }
        stbi_image_free(data);

        if (properties.width != w || properties.height != h || properties.depth != bpp)
        {
            std::string msg = "frame " + std::to_string(1) + " does not have the same properties as frame 0.";
            throw std::runtime_error(msg.c_str());
        }
    }

    //try writing out the compressed data for each file
    std::ofstream file(outfile, std::ios::out | std::ios::binary);
    if (file.good())
    {
        Header header;
        header.framerate = framerate;
        header.count = infiles.size();
        file.write((char*)&header, sizeof(header));

        std::vector<unsigned int> sizes;
        std::vector<std::unique_ptr<std::ifstream>> tempData;

        for (const auto& path : infiles)
        {
            //assume the file is good because we checked it with stbi
            tempData.emplace_back(new std::ifstream(path, std::ios::in | std::ios::binary));
            tempData.back()->seekg(0, std::ios::end);
            sizes.push_back(static_cast<unsigned int>(tempData.back()->tellg()));
            tempData.back()->seekg(0, std::ios::beg);            
        }

        file.write((char*)sizes.data(), sizes.size() * sizeof(unsigned int));

        for (auto i = 0u; i < tempData.size(); ++i)
        {
            std::vector<char> temp(sizes[i]);
            tempData[i]->read(temp.data(), sizes[i]);
            file.write(temp.data(), sizes[i]);
        }

        file.close();
    }
    else
    {
        throw std::runtime_error("unable to open output file for writing");
    }
}

void Catlib::loadFromFile(const std::string& path)
{
    std::ifstream file(path, std::ios::in | std::ios::binary);
    if (file.good())
    {
        file.seekg(0, std::ios::end);
        std::size_t size = static_cast<std::size_t>(file.tellg());
        file.seekg(0, std::ios::beg);

        if (size > 0)
        {
            std::vector<char> data(size);
            file.read(data.data(), size);
            file.close();

            //read in header
            Header header;
            char* cur = data.data();
            std::memcpy(&header, cur, sizeof(header));
            cur += sizeof(header);

            if (header.ident != curIdent || header.version != curVersion)
            {
                throw("out of date or invalid file");
            }

            //read array of sizes
            std::vector<unsigned int> sizes(header.count);
            std::memcpy(sizes.data(), cur, sizes.size() * sizeof(unsigned int));
            cur += sizes.size() * sizeof(unsigned int);

            //read each image, uncompress with stb_image
            int w, h, bpp;
            for (auto i : sizes)
            {
                std::vector<unsigned char> imgData(i);
                std::memcpy(imgData.data(), cur, i);
                cur += i;

                auto img = stbi_load_from_memory(imgData.data(), imgData.size(), &w, &h, &bpp, 0);
                if (!img)
                {
                    m_data.clear();
                    throw("failed to parse animation frame from cat file");
                }
                //if (w != ) //TODO check every frame is the same size
                auto imgSize = w * h * bpp;
                m_data.emplace_back(new unsigned char[imgSize]);
                std::memcpy(m_data.back().get(), img, imgSize);
                stbi_image_free(img);
            }

            m_properties.width = w;
            m_properties.height = h;
            m_properties.depth = bpp;
            m_properties.framerate = header.framerate;
        }
        else
        {
            throw std::runtime_error("invalid cat file");
        }
    }
}

bool Catlib::loaded() const
{
    return !m_data.empty();
}

const Catlib::Properties& Catlib::getProperties() const
{
    return m_properties;
}

const std::vector<std::unique_ptr<unsigned char[]>>& Catlib::getData() const
{
    return m_data;
}
