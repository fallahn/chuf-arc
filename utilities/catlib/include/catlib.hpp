/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

catlib Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//library for reading / writing CHUF animated texture (.cat) files
//catlib uses stb_image to read image files, and defines STB_IMAGE_IMPLEMENTATION
//if you use stbi_image elsewhere in your application do not define 
//STB_IMAGE_IMPLEMENTATION to prevent an error with multiply defined symbols.

#ifndef CAT_HEADER_HPP_
#define CAT_HEADER_HPP_

#include <string>
#include <vector>
#include <memory>

class Catlib final
{
public:
    //contains property data for the currently loaded .cat file
    struct Properties
    {
        //width in pixels
        int width = 0;
        //height in pixels
        int height = 0;
        //depth in bytes per pixel
        int depth = 0;
        //framerate in frames per second
        float framerate = 0.f;
    };

    Catlib() = default;
    ~Catlib() = default;

    Catlib(const Catlib&) = delete;
    Catlib operator = (const Catlib&) = delete;

    //attempts to write listed files as frames to a .cat file. Throws an exception if one or more file fail
    //to load, or if a succeeding frame is not the same resolution as the first. framerate is in frames per
    //second, and maximum frame count is 255
    static void write(const std::string& outfile, const std::vector<std::string>& infiles, float framerate);

    //attempts to read .cat file from given path
    void loadFromFile(const std::string& path);

    //returns true if a .cat file is currently loaded
    bool loaded() const;

    //returns a properties struct containing data about the currently loaded .cat file
    const Properties& getProperties() const;

    //returns a vector containing vectors of raw image data for each frame of the animation
    //use the getProperties() function to determine the width, height and depth (in bytes per pixel)
    //of the frame data, which can be processed with your library of choice
    const std::vector<std::unique_ptr<unsigned char[]>>& getData() const;

private:

    /*
    .cat files start with a fixed sized header, containing the identity, version and
    basic animation data; the frame count and frame rate. The header is followed by
    an array of unsigned int, with Header::Count values. These values represent the
    size in bytes of each image that makes up a frame. The rest of the file is the
    compressed image data. When .cat files are loaded the frames are decompressed with
    stbi - therefore .cat files support all image types supported by stbi_image.
    See https://github.com/nothings/stb for details on stbi_image
    */

    struct Header
    {
        unsigned int ident = 0x544e4143;
        unsigned short version = 1u;
        unsigned char count = 0u;
        float framerate = 0.f;
    };

    Properties m_properties;

    //we use a unique_ptr here because it means when the vector resizes
    //it only reallocates space for a pointer, not all the memory currently
    //used by the stored images.
    std::vector<std::unique_ptr<unsigned char[]>> m_data;
};

#endif //CAT_HEADER_HPP_