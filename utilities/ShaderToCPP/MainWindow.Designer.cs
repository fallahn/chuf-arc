﻿namespace ShaderToCPP
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxVertPath = new System.Windows.Forms.TextBox();
            this.textBoxGeomPath = new System.Windows.Forms.TextBox();
            this.textBoxFragPath = new System.Windows.Forms.TextBox();
            this.buttonVertPath = new System.Windows.Forms.Button();
            this.buttonGeomPath = new System.Windows.Forms.Button();
            this.buttonFragPath = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Vert:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Geom:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Frag:";
            // 
            // textBoxVertPath
            // 
            this.textBoxVertPath.Location = new System.Drawing.Point(62, 10);
            this.textBoxVertPath.Name = "textBoxVertPath";
            this.textBoxVertPath.ReadOnly = true;
            this.textBoxVertPath.Size = new System.Drawing.Size(250, 20);
            this.textBoxVertPath.TabIndex = 3;
            // 
            // textBoxGeomPath
            // 
            this.textBoxGeomPath.Location = new System.Drawing.Point(62, 36);
            this.textBoxGeomPath.Name = "textBoxGeomPath";
            this.textBoxGeomPath.ReadOnly = true;
            this.textBoxGeomPath.Size = new System.Drawing.Size(250, 20);
            this.textBoxGeomPath.TabIndex = 4;
            // 
            // textBoxFragPath
            // 
            this.textBoxFragPath.Location = new System.Drawing.Point(62, 62);
            this.textBoxFragPath.Name = "textBoxFragPath";
            this.textBoxFragPath.ReadOnly = true;
            this.textBoxFragPath.Size = new System.Drawing.Size(250, 20);
            this.textBoxFragPath.TabIndex = 5;
            // 
            // buttonVertPath
            // 
            this.buttonVertPath.Location = new System.Drawing.Point(318, 8);
            this.buttonVertPath.Name = "buttonVertPath";
            this.buttonVertPath.Size = new System.Drawing.Size(27, 23);
            this.buttonVertPath.TabIndex = 6;
            this.buttonVertPath.Text = "...";
            this.buttonVertPath.UseVisualStyleBackColor = true;
            this.buttonVertPath.Click += new System.EventHandler(this.buttonVertPath_Click);
            // 
            // buttonGeomPath
            // 
            this.buttonGeomPath.Location = new System.Drawing.Point(318, 34);
            this.buttonGeomPath.Name = "buttonGeomPath";
            this.buttonGeomPath.Size = new System.Drawing.Size(27, 23);
            this.buttonGeomPath.TabIndex = 7;
            this.buttonGeomPath.Text = "...";
            this.buttonGeomPath.UseVisualStyleBackColor = true;
            this.buttonGeomPath.Click += new System.EventHandler(this.buttonGeomPath_Click);
            // 
            // buttonFragPath
            // 
            this.buttonFragPath.Location = new System.Drawing.Point(318, 60);
            this.buttonFragPath.Name = "buttonFragPath";
            this.buttonFragPath.Size = new System.Drawing.Size(27, 23);
            this.buttonFragPath.TabIndex = 8;
            this.buttonFragPath.Text = "...";
            this.buttonFragPath.UseVisualStyleBackColor = true;
            this.buttonFragPath.Click += new System.EventHandler(this.buttonFragPath_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(270, 89);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(75, 23);
            this.buttonSave.TabIndex = 9;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(358, 119);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.buttonFragPath);
            this.Controls.Add(this.buttonGeomPath);
            this.Controls.Add(this.buttonVertPath);
            this.Controls.Add(this.textBoxFragPath);
            this.Controls.Add(this.textBoxGeomPath);
            this.Controls.Add(this.textBoxVertPath);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(374, 158);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(374, 158);
            this.Name = "MainWindow";
            this.Text = "Shader to CPP";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxVertPath;
        private System.Windows.Forms.TextBox textBoxGeomPath;
        private System.Windows.Forms.TextBox textBoxFragPath;
        private System.Windows.Forms.Button buttonVertPath;
        private System.Windows.Forms.Button buttonGeomPath;
        private System.Windows.Forms.Button buttonFragPath;
        private System.Windows.Forms.Button buttonSave;
    }
}

