﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShaderToCPP
{
    public partial class MainWindow : Form
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private enum ShaderType
        {
            Vertex,
            Geometry,
            Tesselation,
            Fragment
        }

        //--------------event handlers--------------//
        private void buttonVertPath_Click(object sender, EventArgs e)
        {
            OpenFileDialog od = new OpenFileDialog();
            od.Filter = "Vertex Shader Files|*.vert";
            if(od.ShowDialog() == DialogResult.OK)
            {
                textBoxVertPath.Text = od.FileName;
            }
        }

        private void buttonGeomPath_Click(object sender, EventArgs e)
        {
            OpenFileDialog od = new OpenFileDialog();
            od.Filter = "Geometry Shader Files|*.geom";
            if (od.ShowDialog() == DialogResult.OK)
            {
                textBoxGeomPath.Text = od.FileName;
            }
        }

        private void buttonFragPath_Click(object sender, EventArgs e)
        {
            OpenFileDialog od = new OpenFileDialog();
            od.Filter = "Fragment Shader Files|*.frag";
            if (od.ShowDialog() == DialogResult.OK)
            {
                textBoxFragPath.Text = od.FileName;
            }
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            SaveFileDialog sd = new SaveFileDialog();
            sd.Filter = "Header files|*.hpp";
            if(sd.ShowDialog() == DialogResult.OK)
            {
                saveFile(sd.FileName);
            }
        }


        //-----------------parsing-------------------//
        private void saveFile(string path)
        {
            //create / open file
            StreamWriter file;
            try
            {
                file = new StreamWriter(path);
            }
            catch(Exception)
            {
                MessageBox.Show("Unable to open file:\n" + path, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //write header
            writeHeader(file, path);

            //parse each file
            parseFile(textBoxVertPath.Text, file, ShaderType.Vertex);
            parseFile(textBoxGeomPath.Text, file, ShaderType.Geometry);
            parseFile(textBoxFragPath.Text, file, ShaderType.Fragment);

            //write footer
            writeFooter(file);

            //close file
            file.Close();

            MessageBox.Show("Done.");
        }

        private void writeHeader(StreamWriter file, string fileName)
        {
            string license = 
                "/*********************************************************************\n"+
                "Matt Marchant 2014 - 2015\n"+
                "http://trederia.blogspot.com\n"+
                "\n"+
                "CHUF 2.0 Zlib license.\n"+
                "\n"+
                "This software is provided 'as-is', without any express or\n"+
                "implied warranty. In no event will the authors be held\n"+
                "liable for any damages arising from the use of this software.\n"+
                "\n"+
                "Permission is granted to anyone to use this software for any purpose,\n"+
                "including commercial applications, and to alter it and redistribute\n"+
                "it freely, subject to the following restrictions:\n"+
                "\n"+
                "1. The origin of this software must not be misrepresented;\n"+
                "you must not claim that you wrote the original software.\n"+
                "If you use this software in a product, an acknowledgment\n"+
                "in the product documentation would be appreciated but\n"+
                "is not required.\n"+
                "\n"+
                "2. Altered source versions must be plainly marked as such,\n"+
                "and must not be misrepresented as being the original software.\n"+
                "\n"+
                "3. This notice may not be removed or altered from any\n"+
                "source distribution.\n"+
                "*********************************************************************/\n";
            file.WriteLine(license);

            //trim file name to create header guard / namespace name
            string ns = Path.GetFileNameWithoutExtension(fileName);
            string header = "CHUF_SHADER_" + ns.ToUpper() + "_HPP_";
            file.WriteLine("#ifndef " + header);
            file.WriteLine("#define " + header);
            file.WriteLine("\n");
            file.WriteLine("#include <string>");
            file.WriteLine("\nnamespace Shaders\n{");
            file.WriteLine("\tnamespace " + ns + "\n\t{\n");

        }

        private void parseFile(string inputPath, StreamWriter file, ShaderType type)
        {
            if (inputPath == String.Empty) return;

            string[] inFile;
            try 
            {
                inFile = File.ReadAllLines(inputPath);
            }
            catch(Exception)
            {
                MessageBox.Show("Unable to open file:\n" + inputPath, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            string strName = "";
            switch(type)
            {
                case ShaderType.Fragment:
                    strName = "fragment";
                    break;
                case ShaderType.Geometry:
                    strName = "geometry";
                    break;
                case ShaderType.Tesselation:
                    strName = "tesselation";
                    break;
                case ShaderType.Vertex:
                    strName = "vertex";
                    break;
            }

            file.WriteLine("\t\tstatic const std::string " + strName + " =");

            for(int i = 0; i < inFile.Length - 1; ++i)
            {
                file.WriteLine("\t\t\t\"" + inFile[i] + "\\n\" \\");
            }
            file.WriteLine("\t\t\t\"" + inFile[inFile.Length - 1] + "\\n\";\n\n");
        }

        private void writeFooter(StreamWriter file)
        {
            file.WriteLine("\t}\n}\n");
            file.WriteLine("#endif //CHUF_SHADER_HPP_");
        }
    }
}
