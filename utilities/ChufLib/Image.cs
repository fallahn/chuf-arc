﻿/*********************************************************************
Matt Marchant 2015
http://trederia.blogspot.com

ChufLib - Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

using System;
using System.Drawing;
using System.Drawing.Imaging;

namespace Chuf
{
    unsafe sealed public class Image
    {
        static public Bitmap createNoiseMap(int width, int height)
        {
            Bitmap bmp = new Bitmap(width, height, PixelFormat.Format32bppArgb);

            var imgData = bmp.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, bmp.PixelFormat);
            var dataPtr = (Byte*)imgData.Scan0.ToPointer();

            Random rand = new Random();

            var imgSize = width * height * 4;
            for (var i = 0; i < imgSize; ++i)
            {
                dataPtr[i] = (Byte)rand.Next(0, 256);
            }

            bmp.UnlockBits(imgData);

            return bmp;
        }
    }
}