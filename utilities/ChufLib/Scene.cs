/*********************************************************************
Matt Marchant 2015
http://trederia.blogspot.com

ChufLib - Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//manages lighting, cameras and a scene graph

using System;
using System.Collections.Generic;

using OpenTK;
using OpenTK.Graphics.OpenGL4;

namespace Chuf
{
    /// <summary>
    /// Manages lighting, cameras and a scene graph used for rendering
    /// a scene.
    /// </summary>
    sealed public class Scene
    {
        sealed private class RootNode : Node
        {
            public override NodeType Type
            {
                get { return NodeType.Root; }
            }
        }

        private RootNode m_scene = new RootNode();

        private Camera m_activeCamera = new Camera();
        /// <summary>
        /// Gets or Sets the current active camera used when drawing this scene
        /// </summary>
        public Camera ActiveCamera
        {
            get { return m_activeCamera; }
            set { m_activeCamera = value; }
        }

        private List<Light> m_lights = new List<Light>();

        /// <summary>
        /// Adds a node to this scene's scene graph
        /// </summary>
        /// <param name="n">Node derived object to add</param>
        public void addNode(Node n)
        {
            n.setScene(this);
            m_scene.addChild(n);
            if(n.Type == Node.NodeType.Light)
            {
                m_lights.Add((Light)n);
            }
        }

        /// <summary>
        /// Draws this scene by traversing the scene graph
        /// </summary>
        public void draw()
        {
            //TODO associate UBO / uniforms for lighting with shaders
            GLCheck.call(() => GL.Viewport(m_activeCamera.ViewPort));
            m_scene.draw(m_activeCamera.ViewProjectionMatrix);
        }
    }
}