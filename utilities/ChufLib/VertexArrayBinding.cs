﻿/*********************************************************************
Matt Marchant 2015
http://trederia.blogspot.com

ChufLib - Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//binds a drawables vertex array properties to shader attributes

using System;
using System.Collections.Generic;

using OpenTK.Graphics.OpenGL4;

namespace Chuf
{
    sealed public class VertexAttribBinding
    {
        private int m_id = -1;
        static private int maxVertexAttribs = 0;
        static private int currentBuffer = 0;

        public VertexAttribBinding(Shader shader, uint VBOid, VertexLayout vertLayout)
        {
            //check we have VAOs available
            if(maxVertexAttribs == 0)
            {
                maxVertexAttribs = GLCheck.call<int>(() => GL.GetInteger(GetPName.MaxVertexAttribs));
                if(maxVertexAttribs <= 0)
                {
                    Logger.Log("VAOs are unavailable on current hardware");
                    return;
                }
            }
            
            //delete existing buffers first
            if(m_id > 0) GLCheck.call(()=>GL.DeleteVertexArray(m_id));

            //make sure any other buffers are unbound first
            GLCheck.call(() => GL.BindBuffer(BufferTarget.ArrayBuffer, 0));
            GLCheck.call(() => GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0));

            //create new VAO
            m_id = GLCheck.call<int>(() => GL.GenVertexArray());

            if(m_id == 0)
            {
                Logger.Log("Failed creating VAO buffer.");
                return;
            }

            GLCheck.call(() => GL.BindVertexArray(m_id));
            GLCheck.call(() => GL.BindBuffer(BufferTarget.ArrayBuffer, VBOid));

            //get vertex attributes from shader and bind to VBO layout
            int offset = 0;
            foreach(var element in vertLayout.Elements)
            {
                int attribId = -1;
                switch(element.type)
                {
                    case VertexLayout.Type.Position:
                        attribId = shader.getAttributeId("a_position");
                        break;
                    case VertexLayout.Type.UV0:
                        attribId = shader.getAttributeId("a_texCoord0");
                        break;
                    case VertexLayout.Type.Normal:
                        attribId = shader.getAttributeId("a_normal");
                        break;
                    case VertexLayout.Type.Tangent:
                        attribId = shader.getAttributeId("a_tangent");
                        break;
                    case VertexLayout.Type.Bitangent:
                        attribId = shader.getAttributeId("a_bitangent");
                        break;
                    default: break;
                }

                if(attribId != -1)
                {
                    GLCheck.call(() => GL.VertexAttribPointer(attribId, element.size, VertexAttribPointerType.Float, false, vertLayout.Size, offset));
                    GLCheck.call(() => GL.EnableVertexAttribArray(attribId));
                }
                offset += element.size * sizeof(float);
            }

            unbind();
        }

        public void bind()
        {
            if(m_id > 0 && currentBuffer != m_id)
            {
                GLCheck.call(() => GL.BindVertexArray(m_id));
                currentBuffer = m_id;
            }
        }

        public static void unbind()
        {
            if (currentBuffer != 0)
            {
                GLCheck.call(() => GL.BindVertexArray(0));
                currentBuffer = 0;
            }
        }
    }
}