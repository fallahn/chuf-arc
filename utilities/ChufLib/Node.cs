﻿/*********************************************************************
Matt Marchant 2015
http://trederia.blogspot.com

ChufLib - Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//transformable scene graph node

using System;
using System.Collections.Generic;

using OpenTK;

namespace Chuf
{    
    /// <summary>
    /// Abstract class used to create transformable nodes within a scene graph
    /// </summary>
    abstract public class Node
    {
        [Flags]
        private enum DirtyFlags
        {
            None = 0,
            Translation = 1,
            Rotation = 2,
            Scale = 4,
            All = Translation | Rotation | Scale
        }
        DirtyFlags m_dirtyFlags = DirtyFlags.All;

        private Vector3 m_translation = new Vector3();
        /// <summary>
        /// This node's current translation relative to its parent
        /// </summary>
        public Vector3 Translation
        {
            get { return m_translation; }
            set
            {
                m_translation = value;
                m_dirtyFlags |= DirtyFlags.Translation;
                transformChanged();
            }
        }
        
        private Quaternion m_rotation = Quaternion.Identity;
        /// <summary>
        /// This node's current rotation in the world
        /// </summary>
        public Quaternion Rotation
        {
            get { return m_rotation; }
            set
            {
                m_rotation = value;
                m_dirtyFlags |= DirtyFlags.Rotation;
                transformChanged();
            }
        }
        
        private Vector3 m_scale = Vector3.One;
        /// <summary>
        /// This node's current scale
        /// </summary>
        public Vector3 Scale
        {
            get { return m_scale; }
            set
            {
                m_scale = value;
                m_dirtyFlags |= DirtyFlags.Scale;
                transformChanged();
            }
        }

        private Matrix4 m_transform = Matrix4.Identity;
        /// <summary>
        /// This node's current world matrix representing its total world transform
        /// including those of its parents if it has any
        /// </summary>
        public Matrix4 WorldMatrix
        {
            get 
            {
                //opentk uses row major matrices, so multiply in reverse
                m_transform = Matrix4.Identity;

                if ((m_dirtyFlags & DirtyFlags.Scale) != DirtyFlags.None
                    || m_scale != Vector3.One)
                {
                    m_transform = Matrix4.CreateScale(m_scale);
                    m_dirtyFlags &= DirtyFlags.Scale;
                }

                if ((m_dirtyFlags & DirtyFlags.Rotation) != DirtyFlags.None
                    || m_rotation != Quaternion.Identity)
                {
                    m_transform *= Matrix4.CreateFromQuaternion(m_rotation);
                    m_dirtyFlags &= ~DirtyFlags.Rotation;
                }

                if ((m_dirtyFlags & DirtyFlags.Translation) != DirtyFlags.None
                    || m_translation != Vector3.Zero)
                {
                    m_transform *= Matrix4.CreateTranslation(m_translation);
                    m_dirtyFlags &= ~DirtyFlags.Translation;
                }

                if (m_parent != null) return m_transform * m_parent.WorldMatrix;

                return m_transform;
            }
        }

        /// <summary>
        /// Returns this nodes WorldViewMatrix based on the active camera of the node's
        /// current scene if it exists, else returns the node's WorldMatrix
        /// </summary>
        public Matrix4 WorldViewMatrix
        {
            get
            {
                Matrix4 retval = WorldMatrix;
                if(m_scene != null)
                {
                    try
                    {
                        retval *= m_scene.ActiveCamera.ViewMatrix;
                    }
                    catch
                    {
                        throw new Exception("Scene camera null or not found");
                    }
                }
                return retval;
            }
        }

        /// <summary>
        /// Returns the nodes InverseTransposeWorldView matrix, normally used by the vertex
        /// shader to put vertex normals into eye space
        /// </summary>
        public Matrix4 InverseTransposeWorldViewMatrix
        {
            get
            {
                Matrix4 retVal;
                if (m_scene != null)
                {
                    try
                    {
                        retVal = WorldMatrix * m_scene.ActiveCamera.ViewMatrix;
                        retVal.Invert();
                        retVal.Transpose();
                    }
                    catch
                    {
                        throw new Exception("Failed to get active camera from scene");
                    }
                }
                else
                {
                    retVal = Matrix4.Invert(WorldMatrix);
                    retVal.Transpose();
                }
                return retVal;
            }
        }

        public enum NodeType
        {
            Drawable,
            Camera,
            Light,
            Root
        }
        /// <summary>
        /// Returns the node type, implemented by any class inheriting this
        /// </summary>
        public abstract NodeType Type { get; }

        public Node()
        {
            
        }

        /// <summary>
        /// Moves this node relative to is parent by adding the given translation
        /// to its current translation.
        /// </summary>
        /// <param name="translation">Vector to move the node by</param>
        public void translate(Vector3 translation)
        {
            m_translation += translation;
            m_dirtyFlags |= DirtyFlags.Translation;
            transformChanged();
        }

        /// <summary>
        /// Moves this node relative to is parent by adding the given translation
        /// to its current translation. 
        /// </summary>
        /// <param name="x">Distance to move the node in the X axis</param>
        /// <param name="y">Distance to move the node in the Y axis</param>
        /// <param name="z">Distance to move the node in the Z axis</param>
        public void translate(float x, float y, float  z)
        {
            m_translation.X += x;
            m_translation.Y += y;
            m_translation.Z += z;
            m_dirtyFlags |= DirtyFlags.Translation;
            transformChanged();
        }

        /// <summary>
        /// Rotates this node about a given axis
        /// </summary>
        /// <param name="axis">Vector3 representing the axis about which to rotate</param>
        /// <param name="angle">The angle in degrees to rotate this node by</param>
        public void rotate(Vector3 axis, float angle)
        {
            var rotation = Quaternion.FromAxisAngle(axis, MathHelper.DegreesToRadians(angle));
            m_rotation = rotation * m_rotation;
            m_dirtyFlags |= DirtyFlags.Rotation;
            transformChanged();
        }

        /// <summary>
        /// Scales this node by multiplying the current scale by the given value
        /// </summary>
        /// <param name="scale">Vector3 containing the scale values for each axis to be scaled by</param>
        public void scale(Vector3 scale)
        {
            m_scale *= scale;
            m_dirtyFlags |= DirtyFlags.Scale;
            transformChanged();
        }

        /// <summary>
        /// Scales this node by multiplying all axes by the given value
        /// </summary>
        /// <param name="scale">Amount to scale each of this node's axes by</param>
        public void scale(float scale)
        {
            m_scale *= scale;
            m_dirtyFlags |= DirtyFlags.Scale;
            transformChanged();
        }

        private Node m_parent = null;
        protected List<Node> m_children = new List<Node>();
        
        /// <summary>
        /// Adds a node to this node's current set of children. Currently nodes can only be
        /// added to a parent once TODO fix this.
        /// </summary>
        /// <param name="n">Node derived object to add</param>
        public void addChild(Node n)
        {
            if (n == this) return;
            if(n.m_parent != null)
            {
                Logger.Log("Node already parented elsewhere, adding child failed");
                return; //TODO we should just remove the node from its existing parent first
            }
            
            n.m_parent = this;
            m_children.Add(n);
        }

        protected Scene m_scene = null;
        /// <summary>
        /// Sets the reference to the scene to which this node belongs
        /// </summary>
        /// <param name="scene">Scene to set this node to</param>
        public void setScene(Scene scene)
        {
            m_scene = scene;
            foreach (var n in m_children)
                n.setScene(scene);
        }

        /// <summary>
        /// Callback for nodes which inherit this class. Notifies the implementation 
        /// when the transform of this node has been changed, so that any values in a derived
        /// class which may depend on the transform can be updated.
        /// </summary>
        virtual protected void transformChanged(){}

        /// <summary>
        /// Allows passing draws calls down the scene graph. Overriding this is only necessary
        /// when creating a node type which needs to be drawn in the scene.
        /// </summary>
        /// <param name="viewProjection">The view projection matrix of the scene graph's active camera</param>
        virtual public void draw(Matrix4 viewProjection)
        {
            foreach (var n in m_children)
                n.draw(viewProjection);
        }
    }
}