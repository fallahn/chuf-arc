﻿/*********************************************************************
Matt Marchant 2015
http://trederia.blogspot.com

ChufLib - Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//wrapper for a shader program

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL4;

namespace Chuf
{
    /// <summary>
    /// A single shader program which can be used by multiple Drawables / RenderPasses
    /// Each new shader program is automatically cached, and cached versions are returned
    /// if it already exists, rather than creating new versions of the same program
    /// </summary>
   sealed public class Shader
   {
       private static Dictionary<int, Shader> m_shaderCache = new Dictionary<int,Shader>();
       
       private struct Uniform
       {
           //string name;
           public int location;
           public int samplerIndex;

           public Uniform(int loc = -1, int samplerIdx = -1)
           {
               location = loc;
               samplerIndex = samplerIdx;
           }
       }

       private int m_id = 0;
       private Dictionary<string, Uniform> m_uniforms = new Dictionary<string, Uniform>();
       private Dictionary<string, int> m_attributes = new Dictionary<string, int>();

       private static int currentShaderId = 0;

       /// <summary>
       /// Used to test if the current hardware supports the use of shaders
       /// </summary>
       public static bool Supported
       {
           get
           {
               //OpenGL 3.3 is required minimum
               return (new Version(GL.GetString(StringName.Version).Substring(0, 3)) >= new Version(3, 3) ? true : false);
           }
       }

       private Shader()
       {

       }

       ~Shader()
       {
           if(m_id > 0)
           {
               //GLCheck.call(() => GL.DeleteProgram(m_id));
           }
       }

       /// <summary>
       /// Creates a shader program if it doesn't yet exist.
       /// </summary>
       /// <param name="vert">Vertex shader source</param>
       /// <param name="frag">Fragment shader source</param>
       /// <param name="defines">Newline delimited list of defines (optional)</param>
       /// <returns>Shader if successful</returns>
       public static Shader create(string vert, string frag, string defines = "")
       {
           return create(vert, "", frag, defines);
       }

       public static Shader create(string vert, string geom, string frag, string defines)
       {
           if (!Supported)
           {
               Logger.Log("Current Hardware Doesn't Have Sufficient Shader Support");
               throw new Exception();
           }

           if(string.IsNullOrEmpty(vert))
           {
               Logger.Log("Invalid vertex shader source string. Shader not created.");
               throw new Exception();
           }

           if(string.IsNullOrEmpty(frag))
           {
               Logger.Log("Invalid fragment shader source string. Shader not created.");
               throw new Exception();
           }

           vert = "#version 400\n" + defines + vert;
           frag = "#version 400\n" + defines + frag;

           //ok ok, we shouldn't do this but there's unlikely to be enough variations in this use case to cause a problem
           var uid = vert.GetHashCode() + frag.GetHashCode();
           if (m_shaderCache.ContainsKey(uid)) return m_shaderCache[uid];

           Shader s = new Shader();

           //compile into program
           if(s.compileProgram(vert, geom, frag, defines))
           {
               //fetch uniforms
               s.getUniforms();

               //fetch attrib locations
               s.getAttributes();

               m_shaderCache.Add(uid, s);

               return s;
           }
           throw new Exception("Failed to compile shader program");
       }

       /// <summary>
       /// Binds this shader for drawing
       /// </summary>
       public void bind()
       {
           if (currentShaderId == m_id) return;
           GLCheck.call(() => GL.UseProgram(m_id));
           currentShaderId = m_id;
       }

       /// <summary>
       /// Unbinds the currently active shader (ie binds NULL)
       /// </summary>
       public static void unbind()
       {
           if (currentShaderId == 0) return;
           GLCheck.call(()=>GL.UseProgram(0));
           currentShaderId = 0;
       }

       /// <summary>
       /// Sets a texture uniform variable
       /// </summary>
       /// <param name="name">Name of the uniform variable to set</param>
       /// <param name="texture">Reference to texture to use</param>
       public void setParameter(string name, Texture texture)
       {
           if (m_id > 0 && m_uniforms.ContainsKey(name))
           {
               this.bind();
               var uniform = m_uniforms[name];
               var index = TextureUnit.Texture0 + uniform.samplerIndex;

               GLCheck.call(() => GL.ActiveTexture(index));
               GLCheck.call(() => GL.BindTexture(texture.Target, texture.ID));
               GLCheck.call(() => GL.Uniform1(uniform.location, uniform.samplerIndex));
           }
       }

       /// <summary>
       /// Sets a 4x4 matrix variable
       /// </summary>
       /// <param name="name">Name of the uniform variable to set</param>
       /// <param name="mat4">Reference to the 4x4 matrix to use</param>
       public void setParameter(string name, Matrix4 mat4)
       {
           if(m_id > 0)
           {
               this.bind();
               var location = getUniformLocation(name);
               if(location > -1)
               {
                   GLCheck.call(() => GL.UniformMatrix4(location, false, ref mat4));
               }
           }
       }

       /// <summary>
       /// Sets a 2 component vector variable
       /// </summary>
       /// <param name="name">Name of the uniform variable to set</param>
       /// <param name="f">Value to set variable to</param>
       public void setParameter(string name, Vector2 vec2)
       {
           if (m_id > 0)
           {
               this.bind();
               var location = getUniformLocation(name);
               if (location > -1)
               {
                   GLCheck.call(() => GL.Uniform2(location, ref vec2));
               }
           }
       }

       /// <summary>
       /// Sets a 3 component vector variable
       /// </summary>
       /// <param name="name">Name of the uniform variable to set</param>
       /// <param name="f">Value to set variable to</param>
       public void setParameter(string name, Vector3 vec3)
       {
           if (m_id > 0)
           {
               this.bind();
               var location = getUniformLocation(name);
               if (location > -1)
               {
                   GLCheck.call(() => GL.Uniform3(location, ref vec3));
               }
           }
       }

       /// <summary>
       /// Sets a 4 component vector variable
       /// </summary>
       /// <param name="name">Name of the uniform variable to set</param>
       /// <param name="f">Value to set variable to</param>
       public void setParameter(string name, Vector4 vec4)
       {
           if (m_id > 0)
           {
               this.bind();
               var location = getUniformLocation(name);
               if (location > -1)
               {
                   GLCheck.call(() => GL.Uniform4(location, ref vec4));
               }
           }
       }

       /// <summary>
       /// Sets a floating point value variable
       /// </summary>
       /// <param name="name">Name of the uniform variable to set</param>
       /// <param name="f">Value to set variable to</param>
       public void setParameter(string name, float f)
       {
           if(m_id > 0)
           {
               this.bind();
               var location = getUniformLocation(name);
               if(location > -1)
               {
                   GLCheck.call(() => GL.Uniform1(location, f));
               }
           }
       }

       /// <summary>
       /// Sets a colour variable as RGBA
       /// </summary>
       /// <param name="name">Name of the uniform variable to set</param>
       /// <param name="colour">System.Drawing.Color value to set variable to</param>
       public void setParameter(string name, Color colour)
       {
           if(m_id > 0)
           {
               this.bind();
               var location = getUniformLocation(name);
               if(location > -1)
               {
                   GLCheck.call(() => GL.Uniform4(location, (float)colour.R / 255f, (float)colour.G / 255f, (float)colour.B / 255f, (float)colour.A / 255f));
               }
           }
       }

       /// <summary>
       /// Fetches an OpenGL vertex attribute ID
       /// </summary>
       /// <param name="name">Attribute to fetch</param>
       /// <returns>OpenGL ID of the attribute if it exists in the program, or -1 if not</returns>
       public int getAttributeId(string name)
       {
           if (m_attributes.ContainsKey(name))
               return m_attributes[name];

           return -1;
       }

       //private functions
       private bool compileProgram(string vertSource, string geomSource, string fragSource, string defines)
       {           
           //clear existing programs and create new
           if (m_id > 0) GLCheck.call(() => GL.DeleteProgram(m_id));
           m_uniforms.Clear();

           m_id = GLCheck.call(() => GL.CreateProgram());

           //compile shaders
           if (!compileShader(vertSource, ShaderType.VertexShader)) return false;
           if (!compileShader(fragSource, ShaderType.FragmentShader)) return false;
           if (!string.IsNullOrEmpty(geomSource))
           {
               geomSource = "#version 400\n" + defines + geomSource;
               if (!compileShader(geomSource, ShaderType.GeometryShader)) return false;
           }

           //link program
           string info = "";
           int statusCode = -1;
           GLCheck.call(() => GL.LinkProgram(m_id));
           GLCheck.call(() => GL.GetProgramInfoLog(m_id, out info));
           GLCheck.call(() => GL.GetProgram(m_id, GetProgramParameterName.LinkStatus, out statusCode));

           if(statusCode != 1)
           {
               Logger.Log("Failed to link program: "
                   + Environment.NewLine + info + Environment.NewLine + "Status Code: " + statusCode.ToString());

               //tidy up
               GLCheck.call(() => GL.DeleteProgram(m_id));
               m_id = 0;

               return false;
           }

           return true;
       }

       private bool compileShader(string source, ShaderType shaderType)
       {
           string infoLog = "";
           int resultStatus = -1;

           int id = GLCheck.call<int>(() => GL.CreateShader(shaderType));
           GLCheck.call(() => GL.ShaderSource(id, source));
           GLCheck.call(() => GL.CompileShader(id));
           GLCheck.call(() => GL.GetShaderInfoLog(id, out infoLog));
           GLCheck.call(() => GL.GetShader(id, ShaderParameter.CompileStatus, out resultStatus));

           if (resultStatus != 1)
           {
               Logger.Log("Failed to compile shader: "
                   + Environment.NewLine + infoLog + Environment.NewLine + "Status Code: " + resultStatus.ToString());

               //tidy up
               GLCheck.call(() => GL.DeleteShader(id));
               GLCheck.call(() => GL.DeleteProgram(m_id));
               m_id = 0;

               return false;
           }
           GLCheck.call(() => GL.AttachShader(m_id, id));
           GLCheck.call(() => GL.DeleteShader(id));

           return true;
       }

       private void getUniforms()
       {
           int uniformCount = -1;
           GLCheck.call(() => GL.GetProgram(m_id, GetProgramParameterName.ActiveUniforms, out uniformCount));

           if(uniformCount > 0)
           {
               int uniformMaxLength = -1;
               GLCheck.call(() => GL.GetProgram(m_id, GetProgramParameterName.ActiveUniformMaxLength, out uniformMaxLength));

               if(uniformMaxLength > 0)
               {
                   int uniformSize = 0;
                   ActiveUniformType aut = ActiveUniformType.Bool; //because we can't use null value
                   int uniformLocation = -1;
                   StringBuilder name = new StringBuilder(uniformMaxLength);
                   int length = 0;

                   int samplerIndex = 0;

                   for(var i = 0; i < uniformCount; ++i)
                   {
                       GLCheck.call(() => GL.GetActiveUniform(m_id, i, uniformMaxLength, out length, out uniformSize, out aut, name));
                       uniformLocation = GLCheck.call<int>(() => GL.GetUniformLocation(m_id, name.ToString()));

                       Uniform u = new Uniform();
                       u.location = uniformLocation;

                       if (aut == ActiveUniformType.Sampler2D || aut == ActiveUniformType.SamplerCube)
                       {
                           u.samplerIndex = samplerIndex;
                           samplerIndex += uniformSize;
                       }                       

                       m_uniforms.Add(name.ToString(), u);
                   }
               }
           }
       }

       private void getAttributes()
       {
           int attribCount = -1;
           GLCheck.call(() => GL.GetProgram(m_id, GetProgramParameterName.ActiveAttributes, out attribCount));

           if(attribCount > 0)
           {
               int maxAttribLength = -1;
               GLCheck.call(() => GL.GetProgram(m_id, GetProgramParameterName.ActiveAttributeMaxLength, out maxAttribLength));

               if(maxAttribLength > 0)
               {
                   int attribSize;
                   ActiveAttribType attribType;
                   int attribLocation;
                   StringBuilder name = new StringBuilder(maxAttribLength);
                   int length;

                   for(var i = 0; i < attribCount; ++i)
                   {
                       GLCheck.call(() => GL.GetActiveAttrib(m_id, i, maxAttribLength, out length, out attribSize, out attribType, name));
                       attribLocation = GLCheck.call<int>(() => GL.GetAttribLocation(m_id, name.ToString()));
                       m_attributes.Add(name.ToString(), attribLocation);
                   }
               }
           }
       }

       private int getUniformLocation(string name)
       {
           if (m_uniforms.ContainsKey(name))
               return m_uniforms[name].location;
           
           //try querying the program for the uniform, but these should already be 
           //stored in the dictionary anyway
           int location = GLCheck.call<int>(() => GL.GetUniformLocation(m_id, name));
           if (location != -1) m_uniforms.Add(name, new Uniform(location));
           //TODO warn about missing uniforms - currently surpressed to prevent spewing
           //loads of data to the console
           return location;
       }
   }
}