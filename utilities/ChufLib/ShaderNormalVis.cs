﻿/*********************************************************************
Matt Marchant 2015
http://trederia.blogspot.com

ChufLib - Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

namespace Chuf
{
    namespace Shaders
    {
        static public class NormalVis
        {
            public static string Vert =
                "in vec4 a_position;\n" +
                "in vec3 a_normal;\n" +
                "#if defined(BUMP)\n" +
                "in vec3 a_tangent;\n" +
                "in vec3 a_bitangent;\n" +
                "#endif\n" +
                "out VS_OUT\n" +
                "{\n" +
                "    vec3 normal;\n" +
                "    vec3 tangent;\n" +
                "    vec3 bitangent;\n" +
                "} vs_out;\n" +
                
                "void main()\n" +
                "{\n" +
                "    gl_Position = a_position;\n" +
                "    vs_out.normal = normalize(a_normal);\n" +
                "#if defined(BUMP)\n" +
                "    vs_out.tangent = normalize(a_tangent);\n" +
                "    vs_out.bitangent = normalize(a_bitangent);\n" +
                "#endif\n" +
                "}\n";

            public static string Geom =
                "layout (triangles) in;\n" +
                "layout (line_strip, max_vertices = 6) out;\n" +
                
                "in VS_OUT\n" +
                "{\n" +
                "    vec3 normal;\n" +
                "    vec3 tangent;\n" +
                "    vec3 bitangent;\n" +
                "}gs_in[];\n" +

                "uniform mat4 u_worldViewProjectionMatrix;\n" +
                "uniform float u_lineLength = 0.1;\n" +

                "out vec3 vertColour;\n" +

                "void createLines(int index)\n" +
                "{\n" +
                "    gl_Position = u_worldViewProjectionMatrix * gl_in[index].gl_Position;\n" +
                "    vertColour = vec3(0.0, 0.0, 1.0);\n" +
                "    EmitVertex();\n" +
                "    gl_Position = u_worldViewProjectionMatrix * vec4(gl_in[index].gl_Position.xyz + gs_in[index].normal * u_lineLength, 1.0);\n" +
                "    EmitVertex();\n" +
                "#if defined(BUMP)\n" +
                "    gl_Position = u_worldViewProjectionMatrix * gl_in[index].gl_Position;\n" +
                "    vertColour = vec3(1.0, 0.0, 0.0);\n" +
                "    EmitVertex();\n" +
                "    gl_Position = u_worldViewProjectionMatrix * vec4(gl_in[index].gl_Position.xyz + gs_in[index].tangent * u_lineLength, 1.0);\n" +
                "    EmitVertex();\n" +
                "    gl_Position = u_worldViewProjectionMatrix * gl_in[index].gl_Position;\n" +
                "    vertColour = vec3(0.0, 1.0, 0.0);\n" +
                "    EmitVertex();\n" +
                "    gl_Position = u_worldViewProjectionMatrix * vec4(gl_in[index].gl_Position.xyz + gs_in[index].bitangent * u_lineLength, 1.0);\n" +
                "    EmitVertex();\n" +
                "#endif\n" +
                "    EndPrimitive();\n" +
                "}\n" +

                "void main()\n" +
                "{\n" +
                "    createLines(0);\n" +
                "    createLines(1);\n" +
                "    createLines(2);\n" +
                "}\n";

            public static string Frag = 
                "in vec3 vertColour;\n" +
                "out vec4 colour;\n" +
                "void main()\n" +
                "{\n" +
                "    colour = vec4(vertColour.r, vertColour.g, vertColour.b, 1.0);\n" +
                "}\n";
        }
    }
}