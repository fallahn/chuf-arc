﻿/*********************************************************************
Matt Marchant 2015
http://trederia.blogspot.com

ChufLib - Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

namespace Chuf
{
    namespace Shaders
    {
        static public class Vertexlit
        {
            public static string Vert = 
                "in vec4 a_position;\n" +
                "in vec3 a_normal;\n" +
                "#if defined(BUMP)\n" +
                "in vec3 a_tangent;\n" +
                "in vec3 a_bitangent;\n" +
                "#endif\n" +
                "in vec2 a_texCoord0;\n" +

                "uniform mat4 u_worldViewProjectionMatrix;\n" +
                "uniform mat4 u_worldViewMatrix;\n" +
                "uniform mat4 u_inverseTransposeWorldViewMatrix;\n" +
                "uniform vec3 u_lightDirection = vec3(0.0, -1.0, -1.0);\n" +
                "uniform vec3 u_cameraWorldViewPosition;\n" +

                "#if !defined(BUMP)\n" +
                "out vec3 v_normalVector;\n" +
                "#else\n" +
                "out mat3 v_tanToEyeMatrix;\n" +
                "#endif\n" +
                "out vec2 v_texCoord0;\n" +
                "out vec3 v_lightDirection;\n" +
                "out vec3 v_eyePosition;\n" +

                "void main()\n" +
                "{\n" +
                "    vec4 position = a_position;\n" +
                "    v_texCoord0 = a_texCoord0;\n" +
                "    gl_Position = u_worldViewProjectionMatrix * position;\n" +
                "    mat3 inverseTransposeWorldViewMatrix = mat3(u_inverseTransposeWorldViewMatrix[0].xyz,\n" +
                "                                                u_inverseTransposeWorldViewMatrix[1].xyz,\n" +
                "                                                u_inverseTransposeWorldViewMatrix[2].xyz);\n" +
                "    vec3 normalVector = normalize(inverseTransposeWorldViewMatrix * a_normal);\n" +
                "    vec3 eyePosition = u_cameraWorldViewPosition - (u_worldViewMatrix * position).xyz;\n" + 
                "#if defined(BUMP)\n" + 
                "    vec3 tangent = normalize(inverseTransposeWorldViewMatrix * a_tangent);\n" +
                "    vec3 bitangent = normalize(inverseTransposeWorldViewMatrix * a_bitangent);\n" +
                "    mat3 tangentSpaceTransformMatrix = mat3(tangent.x, bitangent.x, normalVector.x, tangent.y, bitangent.y, normalVector.y, tangent.z, bitangent.z, normalVector.z);\n" +
                //we'll bring normals into eyespace rather than light/eye into tan space
                //then we can use them for sampling reflections
                "    v_tanToEyeMatrix = inverse(transpose(tangentSpaceTransformMatrix));\n" +
                "#else\n" +               
                "    v_normalVector = normalVector;\n" + 
                "#endif\n" +
                "    v_lightDirection = u_lightDirection;\n" +
                "    v_eyePosition = eyePosition;\n" +
                "}\n";

            public static string Frag =
                "#if !defined(BUMP)\n" +
                "in vec3 v_normalVector;\n" +
                "#else\n" +
                "in mat3 v_tanToEyeMatrix;\n" +
                "#endif\n" +
                "in vec3 v_lightDirection;\n" +
                "in vec3 v_eyePosition;\n" +
                "in vec2 v_texCoord0;\n" +                
                "#if defined(DIFFUSE_MAP)\n" +

                "uniform vec2 u_textureRepeat = vec2(1.0);" +
                "uniform sampler2D u_diffuseMap;\n" +
                "#else\n" +
                "uniform vec4 u_diffuseColour = vec4(1.0, 0.0, 1.0, 1.0);\n" +
                "#endif\n" +
                "#if defined(BUMP)\n" +
                "uniform sampler2D u_normalMap;\n" +
                "#endif\n" +
                "#if defined(MASK)\n" +
                "uniform sampler2D u_maskMap;\n" +
                "#endif\n" +
                "uniform vec4 u_ambientColour = vec4(0.2, 0.2, 0.2, 1.0);\n" +
                "uniform vec4 u_lightColour = vec4(1.0);\n" +
                "uniform float u_specularIntensity = 1.0;" +
                "uniform float u_specularAmount = 1.0;" +
                "uniform samplerCube u_skyMap;\n" +
                "uniform float u_reflectionAmount = 0.5;\n" +
                "uniform sampler2D u_noiseMap;\n" +
                "uniform float u_roughness = 0.0;\n" +

                "out vec4 finalColour;\n" +

                "void main()\n" +
                "{\n" +
                "#if defined(DIFFUSE_MAP)\n" +
                "    vec4 baseColour = texture2D(u_diffuseMap, v_texCoord0 * u_textureRepeat);\n" +
                "#else\n" +
                "    vec4 baseColour = u_diffuseColour;\n" +
                "#endif\n" +
                "#if defined(BUMP)\n" +
                "    vec3 normalVector = normalize(texture2D(u_normalMap, v_texCoord0 * u_textureRepeat).rgb * 2.0 - 1.0);\n" +
                "    normalVector = normalize(normalVector * v_tanToEyeMatrix);\n" +
                "#else\n" +
                "    vec3 normalVector = normalize(v_normalVector);\n" +
                "#endif\n" +
                "    vec3 lightDirection = normalize(v_lightDirection);\n" +
                //ambience
                "    vec4 blendedColour = baseColour * u_ambientColour;\n" +
                //diffuse
                "    float diffuseAmount = max(dot(normalVector, -lightDirection), 0.0);\n" +
                "    blendedColour += u_lightColour * baseColour * diffuseAmount;\n" +

                //specular
                "    vec3 eyeVec = normalize(v_eyePosition);\n" +
                "    vec3 specularAngle = reflect(lightDirection, normalVector);\n" +

                "#if defined(MASK)\n" +
                "    vec4 maskValues = texture2D(u_maskMap, v_texCoord0 * u_textureRepeat);\n" +
                "    float specularIntensity = clamp(255.0 * maskValues.r * u_specularIntensity, 0.0, 255.0);\n" +
                "    float specularAmount = clamp(maskValues.g * u_specularAmount, 0.0, 1.0);\n" +
                "    float reflectionAmount = maskValues.b;\n" +
                "#else\n" +
                "    float specularIntensity = clamp(255.0 * u_specularIntensity, 0.0, 255.0);\n" +
                "    float specularAmount = clamp(u_specularAmount, 0.0 , 1.0);\n" +
                "    float reflectionAmount = u_reflectionAmount;\n" +
                "#endif\n" +
                "    vec3 specularColour = vec3(pow(clamp(dot(specularAngle, eyeVec), 0.0, 1.0), specularIntensity));\n" +
                //reflections
                "    vec3 reflectionDirection = reflect(eyeVec, normalVector);\n" +
                "    reflectionDirection += texture2D(u_noiseMap, v_texCoord0).rgb * u_roughness;\n" +
                "    vec4 reflectionColour = textureCube(u_skyMap, reflectionDirection);\n" +
                "    blendedColour = mix(blendedColour, reflectionColour, reflectionAmount);\n" +                
                "    blendedColour += vec4(specularColour * specularAmount, 1.0);\n" +

                "    blendedColour.a = baseColour.a;\n" +
                "    finalColour = blendedColour;\n " +
                "}\n";
        }
    }
}