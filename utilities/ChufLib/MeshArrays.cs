﻿/*********************************************************************
Matt Marchant 2015
http://trederia.blogspot.com

ChufLib - Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//arrays of mesh data used for creating basic shapes

using System;
using System.Collections.Generic;
using System.Diagnostics;

using OpenTK;
using OpenTK.Graphics.OpenGL4;

namespace Chuf
{
    /// <summary>
    /// Represents a single element of vertex data
    /// </summary>
    public struct VertexElement
    {
        public VertexLayout.Type type;
        public int size;
        public VertexElement(VertexLayout.Type t, int s)
        {
            type = t;
            size = s;
        }
    }

    /// <summary>
    /// Vertex layout description, which holds and array of element types
    /// which make up the data for single vertex
    /// </summary>
    sealed public class VertexLayout
    {
        public enum Type
        {
            Position,
            UV0,
            Normal,
            Tangent,
            Bitangent
        };

        public VertexLayout(VertexElement[] elements)
        {
            Elements = elements;
        }

        /// <summary>
        /// The element decriptors which make up this vertex layout
        /// </summary>
        public VertexElement[] Elements {get; set;}

        /// <summary>
        /// Total size in bytes for a vertex using this layout
        /// </summary>
        public int Size
        {
            get
            {
                int sum = 0;
                foreach (var element in Elements)
                {
                    sum += element.size;
                }
                return sum * sizeof(float);
            }
        }
    }

    /// <summary>
    /// Base class for storing arrays of vertex data used in a single mesh
    /// </summary>
    public class MeshArray
    {
        public VertexLayout vertexLayout { get; set; }
        public float vertexCount { get; set; }
        public float[] data { get; set; }
        public UInt16[] indices { get; set; }
        public BeginMode beginMode { get; set; }
    }

    /// <summary>
    /// Creates the mesh data needed to draw a textured quad
    /// </summary>
    sealed public class Quad : MeshArray
    {       
        public Quad()
        {
            VertexElement[] elements = 
            {
                new VertexElement(VertexLayout.Type.Position, 3),
                new VertexElement(VertexLayout.Type.UV0, 2),
                new VertexElement(VertexLayout.Type.Normal, 3),
                new VertexElement(VertexLayout.Type.Tangent, 3),
                new VertexElement(VertexLayout.Type.Bitangent, 3)
            };
            vertexLayout = new VertexLayout(elements);

            vertexCount = 4f;
            data = new float[]
            {
                -0.5f, -0.5f, 0f,  0f, 0f,  0f, 0f, 1f,  1f, 0f, 0f,  0f, 1f, 0f,
                -0.5f, 0.5f,  0f,  0f, 1f,  0f, 0f, 1f,  1f, 0f, 0f,  0f, 1f, 0f,
                0.5f,  -0.5f, 0f,  1f, 0f,  0f, 0f, 1f,  1f, 0f, 0f,  0f, 1f, 0f,
                0.5f,  0.5f,  0f,  1f, 1f,  0f, 0f, 1f,  1f, 0f, 0f,  0f, 1f, 0f
            };

            indices = new ushort[]
            {
                1, 0, 3, 2
            };

            beginMode = BeginMode.TriangleStrip;
        }
    }

    /// <summary>
    /// Creates the mesh data needed to draw a textured cube
    /// </summary>
    sealed public class Cube : MeshArray
    {      
        public Cube()
        { 
            VertexElement[] elements = 
            {
                new VertexElement(VertexLayout.Type.Position, 3),
                new VertexElement(VertexLayout.Type.Normal, 3),
                new VertexElement(VertexLayout.Type.Tangent, 3),
                new VertexElement(VertexLayout.Type.Bitangent, 3),
                new VertexElement(VertexLayout.Type.UV0, 2)
            };
            vertexLayout = new VertexLayout(elements);
            vertexCount = 24f;

            float halfWidth = 0.5f;
            data = new float[]
            {
                //front
                -halfWidth, -halfWidth, halfWidth,
                0f, 0f, 1f, //normal
                0f, 1f, 0f, //tan
                1f, 0f, 0f, //bitan
                0f, 0f,     //uv
                halfWidth, -halfWidth, halfWidth,
                0f, 0f, 1f,
                0f, 1f, 0f,
                1f, 0f, 0f,
                1f, 0f,
                halfWidth, halfWidth, halfWidth,
                0f, 0f, 1f,
                0f, 1f, 0f,
                1f, 0f, 0f,
                1f, 1f,
                -halfWidth, halfWidth, halfWidth,
                0f, 0f, 1f,
                0f, 1f, 0f,
                1f, 0f, 0f,
                0f, 1f,
                //back
                -halfWidth, -halfWidth, -halfWidth,
                0f, 0f, -1f,
                0f, 1f, 0f,
                -1f, 0f, 0f,
                1f, 0f,
                -halfWidth, halfWidth, -halfWidth,
                0f, 0f, -1f,
                0f, 1f, 0f,
                -1f, 0f, 0f,
                1f, 1f,
                halfWidth, halfWidth, -halfWidth,
                0f, 0f, -1f,
                0f, 1f, 0f,
                -1f, 0f, 0f,
                0f, 1f,
                halfWidth, -halfWidth, -halfWidth,
                0f, 0f, -1f,
                0f, 1f, 0f,
                -1f, 0f, 0f,
                0f, 0f,
                //top
                -halfWidth, halfWidth, -halfWidth,
                0f, 1f, 0f,
                0f, 0f, -1f,
                1f, 0f, 0f,
                0f, 1f,
                -halfWidth, halfWidth, halfWidth,
                0f, 1f, 0f,
                0f, 0f, -1f,
                1f, 0f, 0f,
                0f, 0f,
                halfWidth, halfWidth, halfWidth,
                0f, 1f, 0f,
                0f, 0f, -1f,
                1f, 0f, 0f,
                1f, 0f,
                halfWidth, halfWidth, -halfWidth,
                0f, 1f, 0f,
                0f, 0f, -1f,
                1f, 0f, 0f,
                1f, 1f,
                //bottom
                -halfWidth, -halfWidth, -halfWidth,
                0f, -1f, 0f,
                0f, 0f, 1f,
                1f, 0f, 0f,
                1f, 1f,
                halfWidth, -halfWidth, -halfWidth,
                0f, -1f, 0f,
                0f, 0f, 1f,
                1f, 0f, 0f,
                0f, 1f,
                halfWidth, -halfWidth, halfWidth,
                0f, -1f, 0f,
                0f, 0f, 1f,
                1f, 0f, 0f,
                0f, 0f,
                -halfWidth, -halfWidth, halfWidth,
                0f, -1f, 0f,
                0f, 0f, 1f,
                1f, 0f, 0f,
                1f, 0f,
                //right
                halfWidth, -halfWidth, -halfWidth,
                1f, 0f, 0f,
                0f, 1f, 0f,
                0f, 0f, -1f,
                1f, 0f,
                halfWidth, halfWidth, -halfWidth,
                1f, 0f, 0f,
                0f, 1f, 0f,
                0f, 0f, -1f,
                1f, 1f,
                halfWidth, halfWidth, halfWidth,
                1f, 0f, 0f,
                0f, 1f, 0f,
                0f, 0f, -1f,
                0f, 1f,
                halfWidth, -halfWidth, halfWidth,
                1f, 0f, 0f,
                0f, 1f, 0f,
                0f, 0f, -1f,
                0f, 0f,
                //left
                -halfWidth, -halfWidth, -halfWidth,
                -1f, 0f, 0f,
                0f, 1f, 0f,
                0f, 0f, 1f,
                0f, 0f,
                -halfWidth, -halfWidth, halfWidth,
                -1f, 0f, 0f,
                0f, 1f, 0f,
                0f, 0f, 1f,
                1f, 0f,
                -halfWidth, halfWidth, halfWidth,
                -1f, 0f, 0f,
                0f, 1f, 0f,
                0f, 0f, 1f,
                1f, 1f,
                -halfWidth, halfWidth, -halfWidth,
                -1f, 0f, 0f,
                0f, 1f, 0f,
                0f, 0f, 1f,
                0f, 1f
            };

            indices = new ushort[]
            {
                0,  1,  2,  0,  2,  3,
                4,  5,  6,  4,  6,  7,
                8,  9,  10, 8,  10, 11,
                12, 13, 14, 12, 14, 15,
                16, 17, 18, 16, 18, 19,
                20, 21, 22, 20, 22, 23
            };

            beginMode = BeginMode.Triangles;
        }
    }

    /// <summary>
    /// Creates the mesh data rewuired to draw a textured sphere
    /// </summary>
    sealed public class Sphere : MeshArray
    {
        public Sphere()
        {
            VertexElement[] elements = 
            {
                new VertexElement(VertexLayout.Type.Position, 3),
                new VertexElement(VertexLayout.Type.UV0, 2),
                new VertexElement(VertexLayout.Type.Normal, 3),
                new VertexElement(VertexLayout.Type.Tangent, 3),
                new VertexElement(VertexLayout.Type.Bitangent, 3)
            };
            vertexLayout = new VertexLayout(elements);

            calcVertices();

            beginMode = BeginMode.Triangles;
        }

        private const float m_radius = 0.5f;
        private const float m_height = m_radius;
        private const byte m_segments = 39;
        private const byte m_rings = m_segments;
        private struct Vertex
        {
            public Vector3 position;
            public Vector2 texCoord;
            public Vector3 normal;
            public Vector3 tangent;
            public Vector3 bitangent;

            public Vertex(Vector3 p, Vector2 uv, Vector3 n)
            {
                position = p;
                texCoord = uv;
                normal = n;
                tangent = new Vector3();
                bitangent = new Vector3();
            }
        }
        
        private void calcVertices()
        {
            var vertData = new Vertex[m_rings * m_segments];
            vertexCount = vertData.Length;

            int i = 0;

            for(double y = 0; y < m_rings; ++y)
            {
                double phi = (y / (m_rings - 1)) * Math.PI;
                for(double x = 0; x < m_segments; ++x)
                {
                    double theta = (x / (m_segments - 1)) * 2 * Math.PI;

                    Vector3 p = new Vector3
                    (
                        (float)(m_radius * Math.Sin(phi) * Math.Cos(theta)),
                        (float)(m_height * Math.Cos(phi)),
                        (float)(m_radius * Math.Sin(phi) * Math.Sin(theta))
                    );
                    Vector3 n = Vector3.Normalize(p);

                    Vector2 uv = new Vector2
                    (
                        1f - (float)x / (m_segments - 1),
                        1f - (float)y / (m_rings - 1)
                    );

                    vertData[i++] = new Vertex(p, uv, n);
                }
            }

            calcIndices();
            calcBitan(ref vertData);

            data = new float[vertData.Length * vertexLayout.Size];
            i = 0;
            foreach(var vert in vertData)
            {
                //HAX. for some reason the bitans aren't calculated correctly
                //from the UVs. Should figure that out one day.
                Vector3 bitan = -Vector3.Cross(vert.normal, vert.tangent);
                data[i++] = vert.position.X;
                data[i++] = vert.position.Y;
                data[i++] = vert.position.Z;
                data[i++] = vert.texCoord.X;
                data[i++] = vert.texCoord.Y;
                data[i++] = vert.normal.X;
                data[i++] = vert.normal.Y;
                data[i++] = vert.normal.Z;
                data[i++] = vert.tangent.X;
                data[i++] = vert.tangent.Y;
                data[i++] = vert.tangent.Z;
                data[i++] = bitan.X;
                data[i++] = bitan.Y;
                data[i++] = bitan.Z;
            }
        }

        private void calcIndices()
        {
            indices = new ushort[(int)vertexCount * 6];

            UInt16 i = 0;

            for (byte y = 0; y < m_rings - 1; ++y)
            {
                for(byte x = 0; x < m_segments - 1; ++x)
                {                   
                    indices[i++] = (UInt16)((y + 1) * m_segments + x + 1);
                    indices[i++] = (UInt16)((y + 1) * m_segments + x);                    
                    indices[i++] = (UInt16)(y * m_segments + x);

                    indices[i++] = (UInt16)(y * m_segments + x);
                    indices[i++] = (UInt16)(y * m_segments + x + 1);                   
                    indices[i++] = (UInt16)((y + 1) * m_segments + x + 1);                    
                }
            }

#if DEBUG
            foreach(var idx in indices)
            {
                Debug.Assert(idx < vertexCount);
            }
#endif 
        }

        private void calcBitan(ref Vertex[] vertData)
        {
            for(var i = 0; i < indices.Length; i += 3)
            {
                Vector3 deltaPos1 = vertData[indices[i + 1]].position - vertData[indices[i]].position;
                Vector3 deltaPos2 = vertData[indices[i + 2]].position - vertData[indices[i]].position;

                Vector2 deltaUV1 = vertData[indices[i + 1]].texCoord - vertData[indices[i]].texCoord;
                Vector2 deltaUV2 = vertData[indices[i + 2]].texCoord - vertData[indices[i]].texCoord;

                float sign = 1f / (deltaUV1.X * deltaUV2.Y - deltaUV1.Y * deltaUV2.X);
                Vector3 faceTan = -(deltaPos1 * deltaUV2.Y - deltaPos2 * deltaUV1.Y) * sign;
                Vector3 faceBitan = -(deltaPos2 * deltaUV1.X - deltaPos1 * deltaUV2.X) * sign;

                for(var j = 0; j < 3; ++j)
                {
                    vertData[indices[i + j]].tangent += faceTan;
                    vertData[indices[i + j]].bitangent += faceBitan; //the result of this is actually wrong. see above kludge
                }
            }

            foreach (var vert in vertData)
            {
                vert.tangent.Normalize();
                vert.bitangent.Normalize();
            }
        }
    }
}