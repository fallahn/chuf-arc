﻿/*********************************************************************
Matt Marchant 2015
http://trederia.blogspot.com

ChufLib - Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//functions to aid debug / error checking

using System;
using System.Diagnostics;

using OpenTK.Graphics.OpenGL4;

namespace Chuf
{
    public delegate void LogDelegate(string msg);
    /// <summary>
    /// Use this delegate to update a Winforms control for example
    /// </summary>
    sealed public class Logger
    {
        public static LogDelegate Log;
    }
    
    /// <summary>
    /// Used to perform error checking and message printing
    /// when using OpenGL functions
    /// </summary>
    sealed public class GLCheck
    {
        /// <summary>
        /// Checks the OpenGL call for errors
        /// </summary>
        /// <param name="glCall">OpenGL function to check</param>
        public static void call(Action glCall)
        {
            glCall();
#if DEBUG
            check();
#endif
        }

        /// <summary>
        /// Use this to check OpenGL functions which return a value
        /// </summary>
        /// <typeparam name="T">The OpenGL function's return type</typeparam>
        /// <param name="glCall">The OpenGL function call to check</param>
        /// <returns>Result of OpenGL function call</returns>
        public static T call<T>(Func<T> glCall)
        {
            T result = glCall();
#if DEBUG
            check();
#endif
            return result;
        }

        private static void check()
        {
            ErrorCode errorCode = GL.GetError();

            if (errorCode == ErrorCode.NoError)
                return;

            string error = "unknown error";
            string description = "no description";
            string fileName = "unknown file";
            string lineNumber = "unknown line number";

            StackTrace st = new StackTrace(true);
            var frame = st.GetFrame(2);
            fileName = frame.GetFileName();
            lineNumber = frame.GetFileLineNumber().ToString();

            //decode the error code
            switch (errorCode)
            {
            case ErrorCode.InvalidEnum:
            {
                error = "GL_INVALID_ENUM";
                description = "an unacceptable value has been specified for an enumerated argument";
                break;
            }

            case ErrorCode.InvalidValue:
            {
                error = "GL_INVALID_VALUE";
                description = "a numeric argument is out of range";
                break;
            }

            case ErrorCode.InvalidOperation:
            {
                error = "GL_INVALID_OPERATION";
                description = "the specified operation is not allowed in the current state";
                break;
            }

            case ErrorCode.OutOfMemory:
            {
                error = "GL_OUT_OF_MEMORY";
                description = "there is not enough memory left to execute the command";
                break;
            }

            case ErrorCode.InvalidFramebufferOperationExt:
            {
                error = "GL_INVALID_FRAMEBUFFER_OPERATION_EXT";
                description = "the object bound to FRAMEBUFFER_BINDING_EXT is not \"framebuffer complete\"";
                break;
            }
            default:
            {
                error = errorCode.ToString();
                description = "";
                break;
            }
            }

            var errorOutput = "OpenGL call failed with: " + error + ": " + description + " on line " + lineNumber + " in " + fileName;
            Logger.Log(errorOutput);
            Debug.WriteLine(errorOutput);
        }
    }
}