﻿/*********************************************************************
Matt Marchant 2015
http://trederia.blogspot.com

ChufLib - Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//wrapper for opengl texture

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;

using OpenTK.Graphics.OpenGL4;

namespace Chuf
{
    sealed public class Texture
    {
        private int m_id = 0;
        /// <summary>
        /// OpenGL texture ID, or 0 if no texture loaded
        /// </summary>
        public int ID { get { return m_id; } }

        private System.Drawing.Imaging.PixelFormat m_format = System.Drawing.Imaging.PixelFormat.DontCare;
        public System.Drawing.Imaging.PixelFormat Format { get { return m_format; } }

        private int m_width = 0;
        /// <summary>
        /// Width of texture in pixels
        /// </summary>
        public int Width { get { return m_width; } }

        private int m_height = 0;
        /// <summary>
        /// Height of texture in pixels
        /// </summary>
        public int Height{ get { return m_height; } }

        private TextureTarget m_target = TextureTarget.Texture2D;
        /// <summary>
        /// Type of texture (2d / cubemap etc)
        /// </summary>
        public TextureTarget Target { get { return m_target; } }

        public Texture()
        {

        }

        ~Texture()
        {
            if(m_id > 0)
            {
                //GLCheck.call(()=>GL.DeleteTexture(m_id));
            }
        }

        /// <summary>
        /// Load an image from disk into a texture
        /// </summary>
        /// <param name="path">Path to file on disk</param>
        /// <param name="flipVertically">If true image if flipped vertically before loading into texture</param>
        /// <returns>true on success else false if there is a failure. Errors are logged to console</returns>
        public bool load(string path, TextureTarget textureTarget = TextureTarget.Texture2D, bool flipVertically = true)
        {
            if (string.IsNullOrEmpty(path)) return false;

            //TODO we could cache textures by path / GL id

            Bitmap sourceImage;
            try
            {
                sourceImage = new Bitmap(path);
                if (flipVertically)
                    sourceImage.RotateFlip(RotateFlipType.RotateNoneFlipY);

                m_width = sourceImage.Width;
                m_height = sourceImage.Height;
            }
            catch (Exception e)
            {
                Logger.Log(e.Message);
                return false;
            }

            if (textureTarget == TextureTarget.Texture2D)
                return createTexture2D(sourceImage);

            else if (textureTarget == TextureTarget.TextureCubeMap)
                return createCubemap(sourceImage);

            Logger.Log("Currently only texture 2D and cubemaps supported. " + path + " not loaded.");
            return false;
        }

        /// <summary>
        /// Load an image from memory into a texture
        /// </summary>
        /// <param name="bmp">Bitmap image in memory</param>
        /// <returns>true on success else false if there is a failure. Errors are logged to console</returns>
        public bool load(Bitmap bmp, TextureTarget textureTarget = TextureTarget.Texture2D)
        {
            if (textureTarget == TextureTarget.Texture2D)
                return createTexture2D(bmp);

            else if (textureTarget == TextureTarget.TextureCubeMap)
                return createCubemap(bmp);

            Logger.Log("Currently only texture 2D and cubemaps supported.");
            return false;
        }
    
        /// <summary>
        /// Forcefully deletes the OpenGL texture associated with this object, and sets it to null
        /// </summary>
        public void clear()
        {
            if (m_id > 0)
            {
                GLCheck.call(()=>GL.DeleteTexture(m_id));
                m_id = 0;
            }
        }

        private bool createTexture2D(Bitmap sourceImage)
        {
            if (m_id > 0) GLCheck.call(()=>GL.DeleteTexture(m_id));
            m_id = GLCheck.call<int>(()=>GL.GenTexture());
            GLCheck.call(()=>GL.BindTexture(TextureTarget.Texture2D, m_id));

            GLCheck.call(()=>GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear));
            GLCheck.call(()=>GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear));

            BitmapData imgData = sourceImage.LockBits(new Rectangle(0, 0, sourceImage.Width, sourceImage.Height),
                ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            GLCheck.call(()=>GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, imgData.Width, imgData.Height, 0,
                OpenTK.Graphics.OpenGL4.PixelFormat.Bgra, PixelType.UnsignedByte, imgData.Scan0));

            sourceImage.UnlockBits(imgData);

            GLCheck.call(()=>GL.BindTexture(TextureTarget.Texture2D, 0));

            m_target = TextureTarget.Texture2D;
            return true;
        }

        private bool createCubemap(Bitmap sourceImage)
        {
            try
            {
                //split source image
                var subWidth = sourceImage.Width / 4;
                var subHeight = sourceImage.Height / 3;
                var size = new Size(subWidth, subHeight);

                Rectangle[] subRects = 
                {
                    new Rectangle(new Point(subWidth * 2, subHeight), size), //posX
                    new Rectangle(new Point(0, subHeight), size),            //negX
                    new Rectangle(new Point(subWidth, 0), size),             //posY
                    new Rectangle(new Point(subWidth, subHeight * 2), size), //negY
                    new Rectangle(new Point(subWidth, subHeight), size),     //posZ
                    new Rectangle(new Point(subWidth * 3, subHeight), size)  //negZ
                };

                //load images into texture
                if (m_id > 0) GLCheck.call(() => GL.DeleteTexture(m_id));
                m_id = GLCheck.call<int>(() => GL.GenTexture());
                GLCheck.call(() => GL.BindTexture(TextureTarget.TextureCubeMap, m_id));

                GLCheck.call(() => GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear));
                GLCheck.call(() => GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.NearestMipmapLinear));
                GLCheck.call(() => GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToEdge));
                GLCheck.call(() => GL.TexParameter(TextureTarget.TextureCubeMap, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToEdge));

                for (var i = 0; i < 6; ++i)
                {
                    BitmapData imgData = sourceImage.LockBits(subRects[i], ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
                    GLCheck.call(() => GL.TexImage2D(TextureTarget.TextureCubeMapPositiveX + i, 0,
                                                        PixelInternalFormat.Rgba, imgData.Width, imgData.Height, 0,
                                                        OpenTK.Graphics.OpenGL4.PixelFormat.Bgra, PixelType.UnsignedByte, imgData.Scan0));
                    sourceImage.UnlockBits(imgData);
                }

                GLCheck.call(() => GL.GenerateMipmap(GenerateMipmapTarget.TextureCubeMap));
                GLCheck.call(() => GL.BindTexture(TextureTarget.TextureCubeMap, 0));

                m_target = TextureTarget.TextureCubeMap;
            }
            catch
            {
                //Logger.Log(path + " image missing or not found.");
                return false;
            }

            return false;
        }
    }
}