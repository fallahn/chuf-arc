﻿/*********************************************************************
Matt Marchant 2015
http://trederia.blogspot.com

ChufLib - Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//classes used to represent world lights

using System;
using System.Drawing;
using System.Diagnostics;

using OpenTK;

namespace Chuf
{
    abstract public class Light : Node
    {
        public override NodeType Type
        {
            get { return NodeType.Light; }
        }

        public Color Colour { get; set; }

        public enum LightVariety
        {
            Directional,
            Point,
            Spot
        }
        abstract public LightVariety Variety { get; }
    }

    /// <summary>
    /// Light node with a colour and direction
    /// </summary>
    public class DirectionalLight : Light
    {
        public DirectionalLight(Color colour)
        {
            Colour = colour;
        }

        /// <summary>
        /// The direction of this light in world coordinate. Default is straight down
        /// </summary>
        public Vector3 Direction
        {
            get 
            {
                //returns the down vector of this node - TODO bitmask this?
                return new Vector3(-WorldMatrix[1,0], -WorldMatrix[1,1], -WorldMatrix[1,2]);
            }
        }

        public override Light.LightVariety Variety
        {
            get { return LightVariety.Directional; }
        }
    }

    public class PointLight : Light
    {
        private float m_range = 0f;
        /// <summary>
        /// Maximum range in world units before the light's brightness reaches zero
        /// </summary>
        public float Range
        {
            get { return m_range; }
            set 
            {
                Debug.Assert(value > 0f);
                m_range = value;
                m_inverseRange = 1f / value;
            }
        }

        private float m_inverseRange = 1f;
        /// <summary>
        /// The inverse of this light's range
        /// </summary>
        public float InverseRange { get { return m_inverseRange; } }

        public PointLight(Color colour, float range)
        {
            Range = range;
            Colour = colour;
        }

        public override LightVariety Variety
        {
            get { return LightVariety.Point; }
        }
    }

    public class SpotLight : Light
    {
        private float m_range = 0f;
        /// <summary>
        /// Maximum range in world units before the light's brightness reaches zero
        /// </summary>
        public float Range
        {
            get { return m_range; }
            set
            {
                Debug.Assert(value > 0f);
                m_range = value;
                m_inverseRange = 1f / value;
            }
        }

        private float m_inverseRange = 1f;
        /// <summary>
        /// The inverse of this light'a range
        /// </summary>
        public float InverseRange
        { 
            get
            {
                return m_inverseRange;
            }
        }

        private float m_innerAngle = MathHelper.DegreesToRadians(10f);
        /// <summary>
        /// The inner angle of this light's brightness cone in radians
        /// </summary>
        public float InnerAngle
        {
            get { return m_innerAngle; }
            set
            {
                Debug.Assert(value > 0f);
                Debug.Assert(value < MathHelper.DegreesToRadians(180f));
                m_innerAngle = value;
                m_innerAngleCos = (float)Math.Cos(m_innerAngle);
            }
        }
        private float m_innerAngleCos = 0f;
        /// <summary>
        /// The cosine of this lights inner angle in radians
        /// </summary>
        public float InnerAngleCos
        {
            get { return m_innerAngleCos; }
        }

        private float m_outerAngle = MathHelper.DegreesToRadians(45f);
        /// <summary>
        /// The outer angle of this light's cone in radians
        /// </summary>
        public float OuterAngle
        {
            get { return m_outerAngle; }
            set
            {
                Debug.Assert(value > 0f);
                Debug.Assert(value < MathHelper.DegreesToRadians(180f));
                m_outerAngle = value;
                m_outerAngleCos = (float)Math.Cos(m_outerAngle);
            }
        }

        private float m_outerAngleCos = 0f;
        /// <summary>
        /// The cosine of this light's outer light cone in radians
        /// </summary>
        public float OuterAngleCos
        {
            get { return m_outerAngleCos; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="colour"></param>
        /// <param name="range"></param>
        /// <param name="innerAngle">Angle in radians</param>
        /// <param name="outerAngle">Angle in radians</param>
        public SpotLight(Color colour, float range, float innerAngle, float outerAngle)
        {
            Colour = colour;
            Range = range;
            InnerAngle = innerAngle;
            OuterAngle = outerAngle;
        }

        public override LightVariety Variety
        {
            get { return LightVariety.Spot; }
        }
    }
}