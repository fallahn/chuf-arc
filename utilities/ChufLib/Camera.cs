﻿/*********************************************************************
Matt Marchant 2015
http://trederia.blogspot.com

ChufLib - Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//creates a moveable camera attachable to a scene graph

using System;
using System.Drawing;

using OpenTK;
using OpenTK.Graphics.OpenGL4;

namespace Chuf
{
    /// <summary>
    /// Represents a transformable camera in 3D space.
    /// Inherits Node.
    /// </summary>
    sealed public class Camera : Node
    {
        public override Node.NodeType Type
        {
            get { return Node.NodeType.Camera; }
        }

        protected override void transformChanged()
        {
            m_dirtyFlags |= DirtyFlags.ViewMatrix | DirtyFlags.ViewProjectionMatrix;
        }
        
        [Flags]
        private enum DirtyFlags
        {
            None = 0,
            ViewMatrix = 1,
            ViewProjectionMatrix = 2,
            All = ViewMatrix | ViewProjectionMatrix
        }
        private DirtyFlags m_dirtyFlags = DirtyFlags.All;
        
        private Matrix4 m_projectionMatrix = Matrix4.CreatePerspectiveFieldOfView(75f * (float)Math.PI / 180f, 16f/9f, 0.1f, 12f);
        /// <summary>
        /// Get the camera's projection matrix
        /// </summary>
        public Matrix4 ProjectionMatrix
        { 
            get { return m_projectionMatrix; }
            set
            {
                m_projectionMatrix = value;
                m_dirtyFlags |= DirtyFlags.ViewProjectionMatrix;
            }
        }

        private Matrix4 m_viewMatrix = new Matrix4();
        /// <summary>
        /// Get the camera's view matrix based on its current transform
        /// </summary>
        public Matrix4 ViewMatrix
        {
            get
            {
                if((m_dirtyFlags & DirtyFlags.ViewMatrix) != DirtyFlags.None)
                {
                    m_viewMatrix = Matrix4.Invert(WorldMatrix);
                    m_dirtyFlags &= ~DirtyFlags.ViewMatrix;
                    m_dirtyFlags |= DirtyFlags.ViewProjectionMatrix;
                }
                return m_viewMatrix;
            }
        }

        private Matrix4 m_viewProjectionMatrix = new Matrix4();
        /// <summary>
        /// Get the camera's combined view / projection matrix
        /// </summary>
        public Matrix4 ViewProjectionMatrix
        {
            get
            {
                if ((m_dirtyFlags & DirtyFlags.ViewProjectionMatrix) != DirtyFlags.None)
                {
                    m_viewProjectionMatrix = ViewMatrix* ProjectionMatrix;
                    m_dirtyFlags &= ~DirtyFlags.ViewProjectionMatrix;
                }
                return m_viewProjectionMatrix;
            }
        }
        
        /// <summary>
        /// Returns the camera's position in eye space
        /// </summary>
        public Vector3 WorldViewPosition
        {
            get
            {
                var position = new Vector4(this.Translation);
                position = Vector4.Transform(position, ViewMatrix);
                return new Vector3(position);
            }
        }
        
        /// <summary>
        /// The OpenGL viewport this camera should use
        /// </summary>
        public Rectangle ViewPort { get; set; }
    }
}