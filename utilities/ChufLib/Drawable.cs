﻿/*********************************************************************
Matt Marchant 2015
http://trederia.blogspot.com

ChufLib - Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//drawable node

using System;
using System.Drawing;
using System.Collections.Generic;

using OpenTK;
using OpenTK.Graphics.OpenGL4;

namespace Chuf
{   
    /// <summary>
    /// Represents a drawable mesh in the scene. Currently only supports
    /// primitve types: Quad, Cube and Sphere
    /// </summary>
    sealed public class Drawable : Node
    {
        private static Dictionary<Mesh, MeshArray> m_meshCache = new Dictionary<Mesh, MeshArray>();
        
        sealed private class UniformValue
        {
            public enum Type
            {
                Float,
                Colour,
                Matrix4,
                Texture,
                Vec2,
                Vec3,
                Vec4,
                None
            }
            private Type m_type = Type.None;
            public Type type {get {return m_type;} }
            //in an ideal world this would be a union. ah well...
            public struct Data
            {
                public float f;
                public Color c;
                public Matrix4 m;
                public Texture t;
                public Vector2 v2;
                public Vector3 v3;
                public Vector4 v4;
            }
            private Data m_data;
            public Data Value { get { return m_data; } }

            public UniformValue(float value)
            {
                m_data.f = value;
                m_type = Type.Float;
            }

            public UniformValue(Color colour)
            {
                m_data.c = colour;
                m_type = Type.Colour;
            }

            public UniformValue(Matrix4 matrix)
            {
                m_data.m = matrix;
                m_type = Type.Matrix4;
            }

            public UniformValue(Texture texture)
            {
                m_data.t = texture;
                m_type = Type.Texture;
            }

            public UniformValue(Vector2 vec2)
            {
                m_data.v2 = vec2;
                m_type = Type.Vec2;
            }

            public UniformValue(Vector3 vec3)
            {
                m_data.v3 = vec3;
                m_type = Type.Vec3;
            }

            public UniformValue(Vector4 vec4)
            {
                m_data.v4 = vec4;
                m_type = Type.Vec4;
            }
        }

        private Dictionary<string, UniformValue> m_uniforms = new Dictionary<string,UniformValue>();
        private readonly Dictionary<string, RenderPass> m_renderPasses = new Dictionary<string, RenderPass>();

        public enum Mesh
        {
            Quad,
            Cube,
            Sphere
        }
        
        /// <summary>
        /// Returns this node's type.
        /// </summary>
        public override NodeType Type
        {
            get { return NodeType.Drawable; }
        }

        private uint[] m_buffers = new uint[2];
        private MeshArray m_meshArray;

        private bool m_hidden = false;
        /// <summary>
        /// Returns whether or not this node will be drawn in the scene when
        /// draw() is called on it.
        /// </summary>
        public bool Hidden { get { return m_hidden; } set { m_hidden = value; } }

        public Drawable(Mesh shape)
        {
            GLCheck.call(() => GL.GenBuffers(2, m_buffers));
            GLCheck.call(() => GL.BindBuffer(BufferTarget.ArrayBuffer, m_buffers[0]));
            
            //get vertex array and indices based on shape
            switch(shape)
            {
                case Mesh.Quad:
                default:
                    if (!m_meshCache.ContainsKey(shape))
                        m_meshCache.Add(shape, new Quad());
                    m_meshArray = m_meshCache[shape];
                    break;
                case Mesh.Cube:
                    if (!m_meshCache.ContainsKey(shape))
                        m_meshCache.Add(shape, new Cube());
                    m_meshArray = m_meshCache[shape];
                    break;
                case Mesh.Sphere:
                    if (!m_meshCache.ContainsKey(shape))
                        m_meshCache.Add(shape, new Sphere());
                    m_meshArray = m_meshCache[shape];
                    break;
            }
            GLCheck.call(() => GL.BufferData(BufferTarget.ArrayBuffer, (IntPtr)(m_meshArray.vertexCount * m_meshArray.vertexLayout.Size), m_meshArray.data, BufferUsageHint.StaticDraw));
            GLCheck.call(() => GL.BindBuffer(BufferTarget.ElementArrayBuffer, m_buffers[1]));
            GLCheck.call(() => GL.BufferData(BufferTarget.ElementArrayBuffer, (IntPtr)(m_meshArray.indices.Length * sizeof(UInt16)), m_meshArray.indices, BufferUsageHint.StaticDraw));
        }

        ~Drawable()
        {
            //GLCheck.call(() => GL.DeleteBuffers(2, m_buffers));
        }

        /// <summary>
        /// Adds a Render Pass to this drawable. Each pass contains a shader and set of
        /// states used to draw the mesh held by this node. Every enabled pass is used in the order
        /// in which they were added when this node is drawn.
        /// </summary>
        /// <param name="name">Name of the pass to be added. Names are unique and existing passes with
        /// the same name will be overwritten by a new RenderPass</param>
        /// <param name="shader">Shader to be used by the new pass</param>
        /// <param name="enabled">Sets whether or not t his pass is initially enabled</param>
        /// <returns>Newly created pass</returns>
        public RenderPass addPass(string name, Shader shader, bool enabled = true)
        {
            VertexAttribBinding vao = new VertexAttribBinding(shader, m_buffers[0], m_meshArray.vertexLayout);
            RenderPass rp = new RenderPass(shader, vao);
            rp.Enabled = enabled;

            if(m_renderPasses.ContainsKey(name))
            {
                m_renderPasses[name] = rp;
            }
            else
            {
                m_renderPasses.Add(name, rp);
            }

            return rp;
        }

        /// <summary>
        /// Enables or disables a pass for drawing. Does nothing if pass does not exist.
        /// </summary>
        /// <param name="name">Name of pass to enable / disable</param>
        /// <param name="enabled">Whether to enable or disable the pass if found</param>
        public void setPassEnabled(string name, bool enabled)
        {
            if (m_renderPasses.ContainsKey(name))
                m_renderPasses[name].Enabled = enabled;
        }

        /// <summary>
        /// Disables all render passes (nothing will be drawn!)
        /// </summary>
        public void disableAllPasses()
        {
            foreach (var r in m_renderPasses)
                r.Value.Enabled = false;
        }

        /// <summary>
        /// Draws this node using the current set of active passes, if the drawable is not hidden.
        /// </summary>
        /// <param name="viewProjectionMat">The view projection matrix of the active camera to use when drawing</param>
        public override void draw(Matrix4 viewProjectionMat)
        {
            if(!Hidden)
            {
                foreach(var pass in m_renderPasses)
                {
                    if (pass.Value.Enabled)
                    {
                        pass.Value.Shader.setParameter("u_worldViewProjectionMatrix", WorldMatrix * viewProjectionMat);
                        pass.Value.Shader.setParameter("u_worldViewMatrix", WorldViewMatrix);
                        pass.Value.Shader.setParameter("u_worldMatrix", WorldMatrix);
                        pass.Value.Shader.setParameter("u_inverseTransposeWorldViewMatrix", InverseTransposeWorldViewMatrix);

                        if(m_scene != null)
                        {
                            try
                            {
                                pass.Value.Shader.setParameter("u_cameraWorldViewPosition", m_scene.ActiveCamera.WorldViewPosition);
                            }
                            catch
                            {
                                throw new Exception("Scene camera is null or not valid");
                            }
                        }

                        foreach (var uniform in m_uniforms)
                        {
                            switch (uniform.Value.type)
                            {
                                case UniformValue.Type.Float:
                                    pass.Value.Shader.setParameter(uniform.Key, uniform.Value.Value.f);
                                    break;
                                case UniformValue.Type.Colour:
                                    pass.Value.Shader.setParameter(uniform.Key, uniform.Value.Value.c);
                                    break;
                                case UniformValue.Type.Matrix4:
                                    pass.Value.Shader.setParameter(uniform.Key, uniform.Value.Value.m);
                                    break;
                                case UniformValue.Type.Texture:
                                    pass.Value.Shader.setParameter(uniform.Key, uniform.Value.Value.t);
                                    break;
                                case UniformValue.Type.Vec2:
                                    pass.Value.Shader.setParameter(uniform.Key, uniform.Value.Value.v2);
                                    break;
                                case UniformValue.Type.Vec3:
                                    pass.Value.Shader.setParameter(uniform.Key, uniform.Value.Value.v3);
                                    break;
                                case UniformValue.Type.Vec4:
                                    pass.Value.Shader.setParameter(uniform.Key, uniform.Value.Value.v4);
                                    break;
                                case UniformValue.Type.None:
                                default: break;
                            }
                        }

                        pass.Value.bind();

                        //draw index arrays
                        GLCheck.call(() => GL.BindBuffer(BufferTarget.ElementArrayBuffer, m_buffers[1]));
                        GLCheck.call(() => GL.DrawElements(m_meshArray.beginMode, m_meshArray.indices.Length, DrawElementsType.UnsignedShort, 0));
                    }
                }
                foreach(var n in m_children)
                {
                    n.draw(viewProjectionMat);
                }
            }
        }

        /// <summary>
        /// Sets a name / value pair for a shader uniform used by one or more of this drawable's active render passes
        /// </summary>
        /// <param name="name">Name of uniform as it appears in the targetted shader</param>
        /// <param name="f">floating point value to be used with this uniform</param>
        public void setUniformValue(string name, float f)
        {
            if(m_uniforms.ContainsKey(name))
            {
                m_uniforms[name] = new UniformValue(f);
            }
            else
            {
                m_uniforms.Add(name, new UniformValue(f));
            }
        }

        /// <summary>
        /// Sets a name / value pair for a shader uniform used by one or more of this drawable's active render passes
        /// </summary>
        /// <param name="name">Name of uniform as it appears in the targetted shader</param>
        /// <param name="c">A System.Drawing.Color to set this uniform to</param>
        public void setUniformValue(string name, Color c)
        {
            if (m_uniforms.ContainsKey(name))
            {
                m_uniforms[name] = new UniformValue(c);
            }
            else
            {
                m_uniforms.Add(name, new UniformValue(c));
            }
        }

        /// <summary>
        /// Sets a name / value pair for a shader uniform used by one or more of this drawable's active render passes
        /// </summary>
        /// <param name="name">Name of uniform as it appears in the targetted shader</param>
        /// <param name="mat">OpenTK.Matrix4 4x4 matrix to set this uniform to</param>
        public void setUniformValue(string name, Matrix4 mat)
        {
            if (m_uniforms.ContainsKey(name))
            {
                m_uniforms[name] = new UniformValue(mat);
            }
            else
            {
                m_uniforms.Add(name, new UniformValue(mat));
            }
        }

        /// <summary>
        /// Sets a name / value pair for a shader uniform used by one or more of this drawable's active render passes
        /// </summary>
        /// <param name="name">Name of uniform as it appears in the targetted shader</param>
        /// <param name="t">A texture object to set this uniform to</param>
        public void setUniformValue(string name, Texture t)
        {
            if (m_uniforms.ContainsKey(name))
            {
                m_uniforms[name] = new UniformValue(t);
            }
            else
            {
                m_uniforms.Add(name, new UniformValue(t));
            }
        }

        /// <summary>
        /// Sets a name / value pair for a shader uniform used by one or more of this drawable's active render passes
        /// </summary>
        /// <param name="name">Name of uniform as it appears in the targetted shader</param>
        /// <param name="t">A Vector2 value to set this uniform to</param>
        public void setUniformValue(string name, Vector2 vec2)
        {
            if (m_uniforms.ContainsKey(name))
            {
                m_uniforms[name] = new UniformValue(vec2);
            }
            else
            {
                m_uniforms.Add(name, new UniformValue(vec2));
            }
        }

        /// <summary>
        /// Sets a name / value pair for a shader uniform used by one or more of this drawable's active render passes
        /// </summary>
        /// <param name="name">Name of uniform as it appears in the targetted shader</param>
        /// <param name="t">A Vector3 value to set this uniform to</param>
        public void setUniformValue(string name, Vector3 vec3)
        {
            if (m_uniforms.ContainsKey(name))
            {
                m_uniforms[name] = new UniformValue(vec3);
            }
            else
            {
                m_uniforms.Add(name, new UniformValue(vec3));
            }
        }

        /// <summary>
        /// Sets a name / value pair for a shader uniform used by one or more of this drawable's active render passes
        /// </summary>
        /// <param name="name">Name of uniform as it appears in the targetted shader</param>
        /// <param name="t">A Vector4 value to set this uniform to</param>
        public void setUniformValue(string name, Vector4 vec4)
        {
            if (m_uniforms.ContainsKey(name))
            {
                m_uniforms[name] = new UniformValue(vec4);
            }
            else
            {
                m_uniforms.Add(name, new UniformValue(vec4));
            }
        }
    }
}