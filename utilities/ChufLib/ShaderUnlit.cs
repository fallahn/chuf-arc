﻿/*********************************************************************
Matt Marchant 2015
http://trederia.blogspot.com

ChufLib - Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

namespace Chuf
{
    namespace Shaders
    {
        /// <summary>
        /// Source code for shaders used by unlit materials
        /// </summary>
        static public class Unlit
        {
            public static string Vert =
                "in vec4 a_position;\n" +
                "#if defined(DIFFUSE_MAP)\n" +
                "in vec2 a_texCoord0;\n" +
                "#endif\n" +

                "uniform mat4 u_worldViewProjectionMatrix;\n" +

                "#if defined(DIFFUSE_MAP)\n" +
                "out vec2 v_texCoord0;\n" +
                "#endif\n" +

                "void main()\n" +
                "{\n" +
                "    gl_Position = u_worldViewProjectionMatrix * a_position;\n" +
                "#if defined(DIFFUSE_MAP)\n" +
                "    v_texCoord0 = a_texCoord0;\n" +
                "#endif\n" +
                "}\n";

            public static string Frag =
                "#if defined(DIFFUSE_MAP)\n" +
                "in vec2 v_texCoord0;\n" +
                "uniform vec2 u_textureRepeat = vec2(1.0);" +
                "uniform sampler2D u_diffuseMap;\n" +
                "#else\n" +
                "uniform vec4 u_diffuseColour = vec4(1.0, 0.0, 1.0, 1.0);\n" +
                "#endif\n" +
                "out vec4 finalColour;\n" +

                "void main()\n" +
                "{\n" +
                "#if defined(DIFFUSE_MAP)\n" +
                "    finalColour = texture2D(u_diffuseMap, v_texCoord0 * u_textureRepeat);\n" +
                "#else\n" +
                "    finalColour = u_diffuseColour;\n" +
                "#endif\n" +
                "}\n";
        }
    }
}