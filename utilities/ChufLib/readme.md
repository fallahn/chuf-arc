ChufLib
-------

A small library of classes for .net which provides CHUF-like behaviour such as scene graphs,
basic mesh rendering and OpenGL resource management (such as textures / shaders). ChufLib
is used for the OpenGL parts of the CHUF Material Editor and is dependent on OpenTK. As such
ChufLib should be cross platform and compatible with both Winforms and GTK (untested).

ChufLib is Licensed under the zlib license.