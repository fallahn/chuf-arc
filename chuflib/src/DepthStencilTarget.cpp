/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/DepthStencilTarget.hpp>
#include <chuf2/Util.hpp>
#include <chuf2/Cache.hpp>

#include <cassert>

using namespace chuf;

namespace
{
    Cache<DepthStencilTarget> dstCache;
}

DepthStencilTarget::DepthStencilTarget(const std::string& name, Format f, UInt16 width, UInt16 height, const DepthStencilTarget::Key& k)
    : m_name    (name),
    m_format    (f),
    m_depthID   (0),
    m_stencilID (0),
    m_width     (width),
    m_height    (height),
    m_packed    (false){}

DepthStencilTarget::~DepthStencilTarget()
{
    if (m_depthID)
        glCheck(glDeleteRenderbuffers(1, &m_depthID));

    if (m_stencilID)
        glCheck(glDeleteRenderbuffers(1, &m_stencilID));
}

//public
DepthStencilTarget* DepthStencilTarget::create(const std::string& name, Format f, UInt16 width, UInt16 height)
{
    assert(!name.empty());
    //search cache for existing
    auto exists = dstCache.find(name);
    if (exists)
    {
        if (exists->getWidth() != width ||
            exists->getHeight() != height)
        {
            Logger::Log("DepthStencilTarget " + name + " already exists and has different diemnsions.", Logger::Type::Warning);
        }
        return exists;
    }

    assert(width && height);
    auto dst = std::make_unique<DepthStencilTarget>(name, f, width, height, Key());
    //by default we try creating a single packed depth / stencil buffer
    //but if for some reason it fails on certain hardware try creating
    //separate buffers

    glCheck(glGenRenderbuffers(1, &dst->m_depthID));
    glCheck(glBindRenderbuffer(GL_RENDERBUFFER, dst->m_depthID));

    //by ommiting the check macro we can handle any failure directly
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);

    auto glError = glGetError();
    if (glError != GL_NO_ERROR)
    {
        //try creating alternate buffer type
        glCheck(glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, width, height));
        
        //create a stencil buffer only if requested
        if (f == Format::DepthStencil)
        {
            glCheck(glGenRenderbuffers(1, &dst->m_stencilID));
            glCheck(glBindRenderbuffer(GL_RENDERBUFFER, dst->m_stencilID));
            glCheck(glRenderbufferStorage(GL_RENDERBUFFER, GL_STENCIL_INDEX8, width, height));
        }
    }
    else
    {
        dst->m_packed = true;
    }
  
    return dstCache.insert(name, dst);
}

DepthStencilTarget* DepthStencilTarget::getTarget(const std::string& name)
{
    assert(!name.empty());
    return dstCache.find(name);
}

const std::string& DepthStencilTarget::getName() const
{
    return m_name;
}

DepthStencilTarget::Format DepthStencilTarget::getFormat() const
{
    return m_format;
}

UInt16 DepthStencilTarget::getWidth() const
{
    return m_width;
}

UInt16 DepthStencilTarget::getHeight() const
{
    return m_height;
}

bool DepthStencilTarget::packed() const
{
    return m_packed;
}