/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/BoundingSphere.hpp>
#include <chuf2/BoundingBox.hpp>
#include <chuf2/Ray.hpp>
#include <chuf2/Util.hpp>

#include <algorithm>

using namespace chuf;

BoundingSphere::BoundingSphere()
    : m_radius(0.f){}

BoundingSphere::BoundingSphere(const glm::vec3& centre, float radius)
    : m_centre(centre), m_radius(radius){}

BoundingSphere::BoundingSphere(const BoundingSphere& copy)
    : m_centre(copy.m_centre), m_radius(copy.m_radius){}

//public
float BoundingSphere::getRadius() const
{
    return m_radius;
}

const glm::vec3& BoundingSphere::getCentre() const
{
    return m_centre;
}

void BoundingSphere::setRadius(float radius)
{
    m_radius = radius;
}

void BoundingSphere::setCentre(const glm::vec3& pos)
{
    m_centre = pos;
}

bool BoundingSphere::intersects(const BoundingSphere& bs) const
{
    glm::vec3 pos = bs.m_centre - m_centre;
    float distance = bs.m_radius + m_radius;
    return(Util::Vector::lengthSquared(pos) <= (distance * distance));
}

bool BoundingSphere::intersects(const BoundingBox& bb) const
{
    //find closest point and check to see if its distance
    //is less than our radius

    glm::vec3 closestPoint = m_centre;
    const auto& min = bb.getMinPoint();
    const auto& max = bb.getMaxPoint();

    if (m_centre.x < min.x)
    {
        closestPoint.x = min.x;
    }
    else if (m_centre.x > max.x)
    {
        closestPoint.x = max.x;
    }

    if (m_centre.y < min.y)
    {
        closestPoint.y = min.y;
    }
    else if (m_centre.y > max.y)
    {
        closestPoint.y = max.y;
    }

    if (m_centre.z < min.z)
    {
        closestPoint.z = min.z;
    }
    else if (m_centre.z > max.z)
    {
        closestPoint.z = max.z;
    }

    closestPoint -= m_centre;

    return (Util::Vector::lengthSquared(closestPoint) <= (m_radius * m_radius));
}

bool BoundingSphere::intersects(const Frustum& f) const
{
    return (intersects(f.getBottom()) != Plane::Intersection::Back &&
        intersects(f.getFar()) != Plane::Intersection::Back &&
        intersects(f.getLeft()) != Plane::Intersection::Back &&
        intersects(f.getNear()) != Plane::Intersection::Back &&
        intersects(f.getRight()) != Plane::Intersection::Back &&
        intersects(f.getTop()) != Plane::Intersection::Back);
}

Plane::Intersection BoundingSphere::intersects(const Plane& p) const
{
    float dist = p.distance(m_centre);

    if (std::abs(dist) <= m_radius)
    {
        return Plane::Intersection::Intersecting;
    }
    else if (dist > 0.f)
    {
        return Plane::Intersection::Front;
    }
    else
    {
        return Plane::Intersection::Back;
    }
}

float BoundingSphere::intersects(const Ray& ray) const
{
    const auto& origin = ray.getOrigin();
    const auto& direction = ray.getDirection();

    auto dist = origin - m_centre;
    float distSquared = glm::length2(dist);

    //this relies o nthe fact that ray distances ae always normalised.
    dist *= direction;
    float B = 2.f * (dist.x + dist.y + dist.z);
    float C = distSquared - (m_radius * m_radius);
    float disc = B * B - 4.f * C;

    if (disc < 0)
    {
        return -1.f;
    }
    else
    {
        float sqrDisc = std::sqrt(disc);
        float t0 = (-B - sqrDisc) * 0.5f;
        float t1 = (-B + sqrDisc) * 0.5f;
        return (t0 > 0 && t0 < t1) ? t0 : t1;
    }
}

bool BoundingSphere::empty() const
{
    return (m_radius == 0.f);
}

void BoundingSphere::merge(const BoundingSphere& bs)
{
    if (bs.empty()) return;

    auto dv = m_centre - bs.m_centre;
    float distance = static_cast<float>(dv.length());

    //test is one is inside the other already
    if (distance <= (bs.m_radius - m_radius))
    {
        m_centre = bs.m_centre;
        m_radius = bs.m_radius;
        return;
    }
    else
    {
        return;
    }

    //technically distance should never be 0
    //because one sphere would be contained in the other
    dv *= (1.f / distance);

    //new radius
    m_radius = (m_radius + bs.m_radius + distance) / 2.f;

    //new centre
    float scale = (m_radius - bs.m_radius);
    m_centre = dv * scale + bs.m_centre;
}

void BoundingSphere::merge(const BoundingBox& bb)
{
    if (bb.empty()) return;

    const auto& min = bb.getMinPoint();
    const auto& max = bb.getMaxPoint();

    //find farthest corner
    auto vec1 = min - m_centre;
    auto vec2 = max - m_centre;
    auto farthest = min;

    if (vec2.x > vec1.x) farthest.x = max.x;
    if (vec2.y > vec1.y) farthest.y = max.y;
    if (vec2.z > vec1.z) farthest.z = max.z;

    vec1 = m_centre - farthest;
    float distance = static_cast<float>(farthest.length());

    //check box to see if it is inside sphere
    if (distance <= m_radius) return;

    //distance should never be 0 here
    vec1 *= (1.f / distance);

    m_radius = (m_radius + distance) / 2.f;
    m_centre = vec1 * m_radius + farthest;
}

void BoundingSphere::set(const glm::vec3& centre, float radius)
{
    m_centre = centre;
    m_radius = radius;
}

void BoundingSphere::set(const BoundingSphere& other)
{
    m_centre = other.m_centre;
    m_radius = other.m_radius;
}

void BoundingSphere::set(const BoundingBox& bb)
{
    m_centre = bb.getCentre();
    m_radius = static_cast<float>((bb.getMaxPoint() - m_centre).length());
}

void BoundingSphere::transform(const glm::mat4& matrix)
{
    auto t = matrix * glm::vec4(m_centre.x, m_centre.y, m_centre.z, 1.f);
    m_centre = { t.x, t.y, t.z };

    glm::vec3 scale = Util::Matrix::getScale(matrix);
    float radius = m_radius * scale.x;

    radius = radius > (m_radius * scale.y) ? radius : (m_radius * scale.y);
    radius = radius > (m_radius * scale.z) ? radius : (m_radius * scale.z);

    m_radius = radius;
}

BoundingSphere& BoundingSphere::operator *= (const glm::mat4& matrix)
{
    transform(matrix);
    return *this;
}

BoundingSphere operator * (const glm::mat4& matrix, const BoundingSphere& bs)
{
    BoundingSphere sphere(bs);
    sphere.transform(matrix);
    return sphere;
}

//private
float BoundingSphere::distanceSquared(const BoundingSphere& bs, const glm::vec3& point)
{
    return Util::Vector::lengthSquared(point - bs.m_centre);
}

bool BoundingSphere::contains(const BoundingSphere& bs, const std::vector<glm::vec3>& points)
{
    for (const auto& p : points)
    {
        if (distanceSquared(bs, p) > (bs.m_radius * m_radius))
        {
            return false;
        }
    }
    return true;
}
