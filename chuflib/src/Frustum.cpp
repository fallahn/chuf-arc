/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/Frustum.hpp>
#include <chuf2/BoundingBox.hpp>
#include <chuf2/BoundingSphere.hpp>
#include <chuf2/Util.hpp>

#include <chuf2/glm/gtc/matrix_access.hpp>
#include <chuf2/glm/gtc/matrix_transform.hpp>

using namespace chuf;

Frustum::Frustum()
{
    set(glm::mat4());
}

Frustum::Frustum(const glm::mat4& matrix)
{
    set(matrix);
}

Frustum::Frustum(const Frustum& copy)
{
    set(copy);
}

const Plane& Frustum::getNear() const
{
    return m_near;
}

const Plane& Frustum::getFar() const
{
    return m_far;
}

const Plane& Frustum::getTop() const
{
    return m_top;
}

const Plane& Frustum::getBottom() const
{
    return m_bottom;
}

const Plane& Frustum::getLeft() const
{
    return m_left;
}

const Plane& Frustum::getRight() const
{
    return m_right;
}

std::vector<glm::vec3> Frustum::getCorners() const
{
    return
    {
        Plane::intersection(m_near, m_bottom, m_right),
        Plane::intersection(m_near, m_bottom, m_left),
        Plane::intersection(m_near, m_top, m_left),
        Plane::intersection(m_near, m_top, m_right),
        Plane::intersection(m_far, m_top, m_right),
        Plane::intersection(m_far, m_top, m_left),
        Plane::intersection(m_far, m_bottom, m_left),
        Plane::intersection(m_far, m_bottom, m_right)
    };
}

std::vector<Plane> Frustum::getPlanes() const
{
    return
    {
        m_top,
        m_bottom,
        m_left,
        m_right,
        m_near,
        m_far
    };
}

const glm::mat4& Frustum::getProjectionMatrix()const
{
    return m_projectionMatrix;
}

void Frustum::set(const glm::mat4& matrix)
{
    m_projectionMatrix = matrix;
    updatePlanes();
}

void Frustum::set(const Frustum& copy)
{
    m_near = copy.m_near;
    m_far = copy.m_far;
    m_top = copy.m_top;
    m_bottom = copy.m_bottom;
    m_left = copy.m_left;
    m_right = copy.m_right;
    set(copy.m_projectionMatrix);
}

bool Frustum::intersects(const BoundingBox& bb) const
{
    return bb.intersects(*this);
}

bool Frustum::intersects(const BoundingSphere& sphere) const
{
    return sphere.intersects(*this);
}

bool Frustum::intersects(const glm::vec3& point) const
{
    return  !(m_far.distance(point) <= 0.f
        || m_near.distance(point) <= 0.f
        || m_left.distance(point) <= 0.f
        || m_right.distance(point) <= 0.f
        || m_top.distance(point) <= 0.f
        || m_bottom.distance(point) <= 0.f);
}

Plane::Intersection Frustum::intersects(const Plane& plane) const
{
    return plane.intersects(*this);
}

//private
void Frustum::updatePlanes()
{
    m_left.set(
        glm::vec3(
        m_projectionMatrix[0][3] + m_projectionMatrix[0][0],
        m_projectionMatrix[1][3] + m_projectionMatrix[1][0],
        m_projectionMatrix[2][3] + m_projectionMatrix[2][0]),
        m_projectionMatrix[3][3] + m_projectionMatrix[3][0]
        );

    m_right.set(
        glm::vec3(
        m_projectionMatrix[0][3] - m_projectionMatrix[0][0],
        m_projectionMatrix[1][3] - m_projectionMatrix[1][0],
        m_projectionMatrix[2][3] - m_projectionMatrix[2][0]),
        m_projectionMatrix[3][3] - m_projectionMatrix[3][0]
        );

    m_bottom.set(
        glm::vec3(
        m_projectionMatrix[0][3] + m_projectionMatrix[0][1],
        m_projectionMatrix[1][3] + m_projectionMatrix[1][1],
        m_projectionMatrix[2][3] + m_projectionMatrix[2][1]),
        m_projectionMatrix[3][3] + m_projectionMatrix[3][1]
        );

    m_top.set(
        glm::vec3(
        m_projectionMatrix[0][3] - m_projectionMatrix[0][1],
        m_projectionMatrix[1][3] - m_projectionMatrix[1][1],
        m_projectionMatrix[2][3] - m_projectionMatrix[2][1]),
        m_projectionMatrix[3][3] - m_projectionMatrix[3][1]
        );

    m_near.set(
        glm::vec3(
        m_projectionMatrix[0][3] + m_projectionMatrix[0][2],
        m_projectionMatrix[1][3] + m_projectionMatrix[1][2],
        m_projectionMatrix[2][3] + m_projectionMatrix[2][2]),
        m_projectionMatrix[3][3] + m_projectionMatrix[3][2]
        );

    m_far.set(
        glm::vec3(
        m_projectionMatrix[0][3] - m_projectionMatrix[0][2],
        m_projectionMatrix[1][3] - m_projectionMatrix[1][2],
        m_projectionMatrix[2][3] - m_projectionMatrix[2][2]),
        m_projectionMatrix[3][3] - m_projectionMatrix[3][2]
        );
}