/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/physics/PhysWorld.hpp>
#include <chuf2/Ray.hpp>
#include <chuf2/Log.hpp>

using namespace chuf;

namespace
{
    const Int32 DIRTY      = 1u;
    const Int32 COLLISION  = 2u;
    const Int32 REGISTERED = 4u;
    const Int32 REMOVE     = 8u;

    const Int32 updateResolution = 10; // max number of updates to do per frame
}

PhysicsWorld::PhysicsWorld()
    : m_updating            (false),
    m_status                (Listener::Event::Deactivated),
    m_gravity               (0.f, -9.8f, 0.f),
    m_collisionCallback     (this)
{
    //create phys world
    m_collisionConfig = std::make_unique<btDefaultCollisionConfiguration>();
    m_collisionDespatcher = std::make_unique<btCollisionDispatcher>(m_collisionConfig.get());
    m_overlappingPairCache = std::make_unique<btDbvtBroadphase>();
    m_constraintSolver = std::make_unique<btSequentialImpulseConstraintSolver>();

    m_btPhysWorld = std::make_unique<btDiscreteDynamicsWorld>(m_collisionDespatcher.get(), m_overlappingPairCache.get(),
                                                                m_constraintSolver.get(), m_collisionConfig.get());
    m_btPhysWorld->setGravity({ m_gravity.x, m_gravity.y, m_gravity.z });

    //ghostpair callback so we can register ghost collisions
    m_ghostpairCallback = std::make_unique<btGhostPairCallback>();
    m_btPhysWorld->getPairCache()->setInternalGhostPairCallback(m_ghostpairCallback.get());
    m_btPhysWorld->getDispatchInfo().m_allowedCcdPenetration = 0.0001f;
}

void PhysicsWorld::update(float dt)
{
    assert(m_btPhysWorld);

    m_updating = true;

    //update phys world
    m_btPhysWorld->stepSimulation(dt, updateResolution);

    //update listeners if we have any
    if (!m_listeners.empty())
    {
        auto objCount = m_btPhysWorld->getNumCollisionObjects();
        Listener::Event lastStatus = m_status;

        if (m_status == Listener::Event::Deactivated)
        {
            for (auto i = 0; i < objCount; ++i)
            {
                if (m_btPhysWorld->getCollisionObjectArray()[i]->isActive())
                {
                    m_status = Listener::Event::Activated;
                    break;
                }
            }
        }
        else
        {
            bool allInactive = true;
            for (auto i = 0; i < objCount; ++i)
            {
                if (m_btPhysWorld->getCollisionObjectArray()[i]->isActive())
                {
                    allInactive = false;
                    break;
                }
            }
            if (allInactive)
            {
                m_status = Listener::Event::Deactivated;
            }
        }

        if (lastStatus != m_status)
        {
            //status changed, raise event
            for (auto& l : m_listeners)
            {
                l->statusEvent(m_status);
            }
        }
    }


    //check all collisions pairs
    //first remove any marked for removal (and raise not colliding event)
    for (auto cs = m_collisionStatus.begin(); cs != m_collisionStatus.end();)
    {
        if (cs->second.status & REMOVE)
        {
            if ((cs->second.status & COLLISION) && cs->first.objB)
            {
                for (auto& l : cs->second.listeners)
                {
                    PhysicsCollisionComponent::CollisionPair cp(cs->first.objA, nullptr);
                    l->collisionEvent(PhysicsCollisionComponent::Listener::Type::NotColliding, cp);
                }
            }
            auto erase = cs++;
            m_collisionStatus.erase(erase);
        }
        else
        {
            cs->second.status |= DIRTY;
            cs++;
        }
    }

    //perform tests on remaining collision pairs
    for (auto& cs : m_collisionStatus)
    {
        if ((cs.second.status & REGISTERED) && !(cs.second.status & REMOVE))
        {
            if (cs.first.objB)
            {
                m_btPhysWorld->contactPairTest(cs.first.objA->getCollisionObject(), cs.first.objB->getCollisionObject(), m_collisionCallback);
            }
            else
            {
                m_btPhysWorld->contactTest(cs.first.objA->getCollisionObject(), m_collisionCallback);
            }
        }
    }

    //finally update the status entries
    for (auto& cs : m_collisionStatus)
    {
        if (cs.second.status & DIRTY)
        {
            if ((cs.second.status & COLLISION) && cs.first.objB)
            {
                for (auto& l : cs.second.listeners)
                {
                    l->collisionEvent(PhysicsCollisionComponent::Listener::Type::NotColliding, cs.first);
                }
            }
            cs.second.status &= ~COLLISION;
        }
    }

    m_updating = false; //we use this to prevent trying to remove a physics update during an upate (via a collision callback for example)
}

void PhysicsWorld::addListener(PhysicsWorld::Listener* listener)
{
    assert(listener);

    m_listeners.push_back(listener);
}

void PhysicsWorld::removeListener(PhysicsWorld::Listener* listener)
{
    assert(listener);

    m_listeners.erase(std::remove_if(m_listeners.begin(), m_listeners.end(),
        [listener](const Listener* l)
    {
        return l == listener;
    }), m_listeners.end());
}

const glm::vec3& PhysicsWorld::getGravity() const
{
    return m_gravity;
}

void PhysicsWorld::setGravity(const glm::vec3& g)
{
    m_gravity = g;
    m_btPhysWorld->setGravity({ g.x, g.y, g.z });
}

bool PhysicsWorld::rayTest(const Ray& ray, float distance, PhysicsWorld::Manifold* manifold, PhysicsWorld::ManifoldFilter* filter)
{
    assert(m_btPhysWorld);

    const auto& origin = ray.getOrigin();
    btVector3 rayFromWorld = { origin.x, origin.y, origin.z };

    auto direction = ray.getDirection();
    direction += origin;
    btVector3 rayToWorld = { direction.x, direction.y, direction.z };

    RayTestCallback rtc(rayFromWorld, rayToWorld, filter);
    m_btPhysWorld->rayTest(rayFromWorld, rayToWorld, rtc);

    if (rtc.hasHit())
    {
        if (manifold)
        {
            manifold->component = getcollisionComponent(rtc.m_collisionObject);
            manifold->distance = rtc.m_closestHitFraction;
            manifold->normal = { rtc.m_hitNormalWorld.x(), rtc.m_hitNormalWorld.y(), rtc.m_hitNormalWorld.z() };
            manifold->point = { rtc.m_hitPointWorld.x(), rtc.m_hitPointWorld.y(), rtc.m_hitPointWorld.z() };
        }
        return true;
    }
    
    return false;
}

bool PhysicsWorld::sweepTest(PhysicsCollisionComponent*, const glm::vec3& end, PhysicsWorld::Manifold*, PhysicsWorld::ManifoldFilter*)
{
    return false;
}

//TODO factory funcs

void PhysicsWorld::drawDebug(const glm::mat4& viewProjection)
{

}

//private
void PhysicsWorld::addListener(PhysicsCollisionComponent::Listener* l, PhysicsCollisionComponent* a, PhysicsCollisionComponent* b)
{
    assert(l);
    assert(a || b);

    PhysicsCollisionComponent::CollisionPair cp(a, b);
    auto& info = m_collisionStatus[cp];
    info.listeners.push_back(l);
    info.status |= REGISTERED;
}

void PhysicsWorld::removeListener(PhysicsCollisionComponent::Listener* l, PhysicsCollisionComponent* a, PhysicsCollisionComponent* b)
{
    assert(a || b);

    PhysicsCollisionComponent::CollisionPair cp(a, b);

    if (m_collisionStatus.count(cp) > 0)
    {
        m_collisionStatus[cp].status |= REMOVE;
    }
}

void PhysicsWorld::addCollisionComponent(PhysicsCollisionComponent* pc)
{
    assert(pc);

    //physics components are matched up with thier bullet counterparts using
    //bullets option to supply user data to physics objects
    pc->getCollisionObject()->setUserPointer(pc);

    auto group = static_cast<short>(pc->m_collisionGroup);
    auto mask = static_cast<short>(pc->m_collisionMask);

    switch (pc->getType())
    {
    case PhysicsCollisionComponent::Type::Body:
        m_btPhysWorld->addRigidBody(static_cast<btRigidBody*>(pc->getCollisionObject()), group, mask);
        break;
    case PhysicsCollisionComponent::Type::Character:
    case PhysicsCollisionComponent::Type::Ghost:
        m_btPhysWorld->addCollisionObject(pc->getCollisionObject(), group, mask);
        break;
    default:
        Logger::Log("Tried adding invalid Physics Type: " + std::to_string(static_cast<int>(pc->getType())), Logger::Type::Error);
        break;
    }
}

void PhysicsWorld::removeCollisionComponent(PhysicsCollisionComponent* pc, bool removeListeners)
{
    assert(pc);
    assert(!m_updating); //got an assertion here? You shouldn't be trying to remove phys components during an update

    if (pc->getCollisionObject())
    {
        switch (pc->getType())
        {
        case PhysicsCollisionComponent::Type::Body:
            m_btPhysWorld->removeRigidBody(static_cast<btRigidBody*>(pc->getCollisionObject()));
            break;
        case PhysicsCollisionComponent::Type::Character:
        case PhysicsCollisionComponent::Type::Ghost:
            m_btPhysWorld->removeCollisionObject(pc->getCollisionObject());
            break;
        default:
            Logger::Log("Tried to remove invalid Physics Component Type from physics world", Logger::Type::Error);
            break;
        }
    }

    if (removeListeners)
    {
        for (auto& cs : m_collisionStatus)
        {
            if (cs.first.objA == pc || cs.first.objB == pc)
            {
                cs.second.status |= REMOVE;
            }
        }
    }
}

PhysicsCollisionComponent* PhysicsWorld::getcollisionComponent(const btCollisionObject* object) const
{ 
    assert(object);
    return reinterpret_cast<PhysicsCollisionComponent*>(object->getUserPointer());
}

void PhysicsWorld::destroyShape(PhysicsCollisionShape* shape)
{
    //TODO this doesn't really destroy the shape, the vector should be of unique_ptrs
    m_shapes.erase(std::remove_if(m_shapes.begin(), m_shapes.end(),
        [shape](const PhysicsCollisionShape* p)
    {
        return (p == shape);
    }), m_shapes.end());
}



//------Manifold filter------//
bool PhysicsWorld::ManifoldFilter::filter(PhysicsCollisionComponent*)
{
    return false;
}

bool PhysicsWorld::ManifoldFilter::hit(const Manifold&)
{
    return true;
}

//----callback wrapper-----//
btScalar PhysicsWorld::CollisionCallback::addSingleResult(btManifoldPoint& mp, const btCollisionObjectWrapper* a, Int32 partIdA, Int32 indexA,
                                                            const btCollisionObjectWrapper* b, Int32 partIdB, Int32 indexB)
{
    assert(m_physWorld);

    auto objA = m_physWorld->getcollisionComponent(a->getCollisionObject());
    auto objB = m_physWorld->getcollisionComponent(b->getCollisionObject());

    PhysicsCollisionComponent::CollisionPair cp(objA, objB);
    CollisionData* cd = nullptr;
    if (m_physWorld->m_collisionStatus.count(cp) > 0)
    {
        cd = &m_physWorld->m_collisionStatus[cp];
    }
    else
    {
        cd = &m_physWorld->m_collisionStatus[cp];

        PhysicsCollisionComponent::CollisionPair p1(cp.objA, nullptr);
        if (m_physWorld->m_collisionStatus.count(p1) > 0)
        {
            const auto& colData = m_physWorld->m_collisionStatus[p1];
            for (auto& l : colData.listeners)
            {
                assert(l);
                cd->listeners.push_back(l);
            }
        }

        PhysicsCollisionComponent::CollisionPair p2(cp.objB, nullptr);
        if (m_physWorld->m_collisionStatus.count(p2) > 0)
        {
            const auto& colData = m_physWorld->m_collisionStatus[p2];
            for (auto& l : colData.listeners)
            {
                assert(l);
                cd->listeners.push_back(l);
            }
        }
    }

    //raise collision event
    if (!(cd->status & COLLISION)
        && !(cd->status & REMOVE))
    {
        for (auto& l : cd->listeners)
        {
            auto a = mp.getPositionWorldOnA();
            auto b = mp.getPositionWorldOnB();
            
            l->collisionEvent(PhysicsCollisionComponent::Listener::Type::Colliding, cp, { a.getX(), a.getY(), a.getZ() }, { b.getX(), b.getY(), b.getZ() });
        }
    }


    cd->status &= ~DIRTY;
    cd->status |= COLLISION;

    return 0.f;
}


//-------ray test callback wrapper--------//
PhysicsWorld::RayTestCallback::RayTestCallback(const btVector3& rayFromWorld, const btVector3& rayToWorld, ManifoldFilter* filter)
    : btCollisionWorld::ClosestRayResultCallback(rayFromWorld, rayToWorld), m_filter(filter){}

bool PhysicsWorld::RayTestCallback::needsCollision(btBroadphaseProxy* proxy) const
{
    if (!btCollisionWorld::ClosestRayResultCallback::needsCollision(proxy)) return false;

    btCollisionObject* object = reinterpret_cast<btCollisionObject*>(proxy->m_clientObject);
    PhysicsCollisionComponent* pc = reinterpret_cast<PhysicsCollisionComponent*>(object->getUserPointer());

    if (pc == nullptr) return false;

    return (m_filter) ? !m_filter->filter(pc) : true;
}

btScalar PhysicsWorld::RayTestCallback::addSingleResult(btCollisionWorld::LocalRayResult& result, bool normalInWorldSpace)
{
    assert(result.m_collisionObject);

    PhysicsCollisionComponent* pc = reinterpret_cast<PhysicsCollisionComponent*>(result.m_collisionObject->getUserPointer());
    if (pc == nullptr) return 1.f;

    float retVal = btCollisionWorld::ClosestRayResultCallback::addSingleResult(result, normalInWorldSpace);

    m_manifold.component = pc;
    m_manifold.point = { m_hitPointWorld.x(), m_hitPointWorld.y(), m_hitPointWorld.z() };
    m_manifold.distance = m_closestHitFraction;
    m_manifold.normal = { m_hitNormalWorld.x(), m_hitNormalWorld.y(), m_hitNormalWorld.z() };

    if (m_filter && !m_filter->hit(m_manifold)) return -1.f;

    return retVal;
}