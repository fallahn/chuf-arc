/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/Material.hpp>
#include <chuf2/Mesh.hpp>
#include <chuf2/Skin.hpp>
#include <chuf2/Model.hpp>
#include <chuf2/Node.hpp>
#include <chuf2/Pass.hpp>
#include <chuf2/SubMesh.hpp>
#include <chuf2/Technique.hpp>

#include <chuf2/Util.hpp>

#include <iostream>

using namespace chuf;

namespace
{
    bool drawWireframe(const Mesh::Ptr& mesh)
    {
        auto vertCount = mesh->getVertexCount();
        switch (mesh->getPrimitiveType())
        {
        case Mesh::PrimitiveType::Triangles:
            for (auto i = 0u; i < vertCount; i += 3)
            {
                glCheck(glDrawArrays(GL_LINE_LOOP, i, 3));
            }
            return true;
        case Mesh::PrimitiveType::TriangleStrip:
            for (auto i = 2u; i < vertCount; ++i)
            {
                glCheck(glDrawArrays(GL_LINE_LOOP, i - 2, 3));
            }
            return true;
        default: return false;
        }
    }

    bool drawWireframe(const SubMesh& subMesh)
    {
        auto indexCount = subMesh.getIndexCount();
        auto indexSize = 0u;
        switch (subMesh.getIndexFormat())
        {
        case Mesh::IndexFormat::I8:
            indexSize = 1u;
            break;
        case Mesh::IndexFormat::I16:
            indexSize = 2u;
            break;
        case Mesh::IndexFormat::I32:
            indexSize = 4u;
            break;
        default: return false;
        }

        switch (subMesh.getPrimitiveType())
        {
        case Mesh::PrimitiveType::Triangles:
            for (auto i = 0u; i < indexCount; i += 3u)
            {
                glCheck(glDrawElements(GL_LINE_LOOP, 3, static_cast<GLenum>(subMesh.getIndexFormat()), ((const GLvoid*)(indexSize * i))));
            }
            return true;
        case Mesh::PrimitiveType::TriangleStrip:
            for (auto i = 2u; i < indexCount; ++i)
            {
                glCheck(glDrawElements(GL_LINE_LOOP, 3, static_cast<GLenum>(subMesh.getIndexFormat()), ((const GLvoid*)(indexSize * (i - 2)))))
            }
            return true;
        default: return false;
        }
    }
}

Model::Model(const Mesh::Ptr& mesh, SceneRenderer* sr, const Model::Key& k)
    : Drawable      (sr),
    m_mesh		    (mesh),
    m_subMeshCount	(0u),
    m_bbDirty       (true)
{
    assert(mesh);
    m_subMeshCount = mesh->getSubMeshCount();
}

//public
Model::Ptr Model::create(const Mesh::Ptr& mesh, SceneRenderer* sr)
{
    assert(mesh);
    return std::make_unique<Model>(mesh, sr, Key());
}

Mesh::Ptr Model::getMesh() const
{
    return m_mesh;
}

UInt32 Model::getSubMeshCount() const
{
    return m_subMeshCount;
}

Material::Ptr Model::getMaterial(Int32 subMeshIndex) const
{
    assert(subMeshIndex == -1 || subMeshIndex >= 0);

    if (subMeshIndex == -1) return m_material;
    if (subMeshIndex >= m_subMeshCount) return nullptr;

    return m_subMeshMaterials[subMeshIndex];
}

void Model::setMaterial(const Material::Ptr& m, Int32 subMeshIndex)
{
    assert(subMeshIndex == -1 || (subMeshIndex >= 0 && subMeshIndex < m_subMeshCount));

    Material::Ptr oldMat;

    if (subMeshIndex == -1)
    {
        oldMat = m_material;
        if (m)
        {
            m_material = m;
            if (m_subMeshCount > 0)
            {
                m_subMeshMaterials.clear();
                for (auto i = 0u; i < m_subMeshCount; ++i)
                {
                    m_subMeshMaterials.push_back(m);
                }
            }
        }
    }
    else if (subMeshIndex >= 0 && subMeshIndex < m_subMeshCount)
    {
        verifySubMeshCount();

        if (m_subMeshMaterials.size() > subMeshIndex)
        {
            oldMat = m_subMeshMaterials[subMeshIndex];
            if (m) m_subMeshMaterials[subMeshIndex] = m;
        }
        else if (subMeshIndex == m_subMeshMaterials.size())
        {
            m_subMeshMaterials.push_back(m);
        }
    }

    if (oldMat)
    {
        auto tCount = oldMat->getTechniqueCount();
        for (auto i = 0u; i < tCount; ++i)
        {
            auto t = oldMat->getTechnique(i);
            assert(t);

            auto passCount = t->getPassCount();
            for (auto j = 0u; j < passCount; ++j)
            {
                auto p = t->getPass(j);
                p->setVertexAttribBinding(nullptr);
                m_vaoBinds.erase(p->getShader());
            }
        }
    }
    if (m)
    {
        auto tCount = m->getTechniqueCount();
        for (auto i = 0u; i < tCount; ++i)
        {
            auto t = m->getTechnique(i);
            assert(t);

            auto pCount = t->getPassCount();
            for (auto j = 0u; j < pCount; ++j)
            {
                auto p = t->getPass(j);
                assert(p);

                auto vb = VertexAttribBinding::create(m_mesh, p->getShader());
                p->setVertexAttribBinding(vb);
                m_vaoBinds.insert(std::make_pair(p->getShader(), vb));
            }
        }
    }

    if (getNode())
    {
        bindNodeToMaterial(m);
    }
}

Material::Ptr Model::setMaterial(const std::string& vertShader, const std::string& fragShader, Int32 subMeshIndex)
{
    auto material = Material::create(vertShader, fragShader);
    if (!material)
    {
        Logger::Log("Failed to create material for model", Logger::Type::Error);
        return nullptr;
    }
    setMaterial(material, subMeshIndex);
    return material;
}

Material::Ptr Model::setMaterial(const std::string& path, Int32 subMeshIndex)
{
    auto material = Material::createFromFile(path);
    if (!material)
    {
        Logger::Log("Failed to create material for model", Logger::Type::Error);
        return nullptr;
    }
    setMaterial(material, subMeshIndex);
    return material;
}

bool Model::hasMaterial(Int32 index)
{
    return(index < m_subMeshCount && m_subMeshMaterials[index]);
}

void Model::setSkin(const Skin::Ptr& skin)
{
    m_skin = skin;
}

Skin::Ptr Model::getSkin() const
{
    return (m_skin) ? m_skin : nullptr;
}

Node* Model::getNode() const
{
    return const_cast<Node*>(Component::getNode());
}

void Model::setNode(Node* node)
{
    Component::setNode(node);

    if (node)
    {
        if (m_material)
        {
            bindNodeToMaterial(m_material);
        }
        for (const auto& m : m_subMeshMaterials)
        {
            bindNodeToMaterial(m);
        }
    }
}

const BoundingBox& Model::getBoundingBox() const
{
    if (m_bbDirty)
    {
        m_bbDirty = false;
        m_bb = m_mesh->getBoundingBox();
        m_bb.transform(getNode()->getMatrix());
    }
    
    return m_bb;
}

UInt32 Model::draw(UInt32 passFlags, bool wireframe) const
{
    assert(m_mesh);
    auto subMeshCount = m_mesh->getSubMeshCount();
    Uint32 drawCount = 0u;
    if (subMeshCount == 0)
    {
        if (m_material && m_material->hasFlag(passFlags))
        {
            glCheck(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0));

            auto t = m_material->getTechnique();
            assert(t);

            auto passCount = t->getPassCount();
            for (auto i = 0u; i < passCount; ++i)
            {
                auto p = t->getPass(i);
                assert(p);

                if (p->flags() == passFlags)
                {
                    //if a material is shared it needs the vao resetting for the current mesh
                    p->setVertexAttribBinding(m_vaoBinds.find(p->getShader())->second);
                    p->bind();
                    if (!wireframe || !drawWireframe(m_mesh))
                    {
                        glCheck(glDrawArrays(static_cast<GLenum>(m_mesh->getPrimitiveType()), 0, m_mesh->getVertexCount()));
                    }
                    p->unbind();
                    drawCount = 1u;
                }
            }
        }
    }
    else
    {
        //draw all sub meshes
        for (auto i = 0u; i < subMeshCount; ++i)
        {
            const auto& subMesh = m_mesh->getSubMesh(i);
            //assert(subMesh);

            auto material = getMaterial(i);
            if (material && material->hasFlag(passFlags))
            {
                auto t = material->getTechnique();
                assert(t);

                auto passCount = t->getPassCount();
                for (auto j = 0u; j < passCount; ++j)
                {
                    auto p = t->getPass(j);
                    assert(p);

                    if (p->flags() == passFlags)
                    {
                        p->setVertexAttribBinding(m_vaoBinds.find(p->getShader())->second);
                        p->bind();
                        glCheck(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, subMesh.getIndexBuffer()));

                        if (!wireframe || !drawWireframe(subMesh))
                        {
                            glCheck(glDrawElements(static_cast<GLenum>(subMesh.getPrimitiveType()), subMesh.getIndexCount(), static_cast<GLenum>(subMesh.getIndexFormat()), 0));
                        }
                        p->unbind();
                        drawCount++;
                    }
                }
            }
        }
    }

    return drawCount;
}

void Model::transformChanged(Transform& t)
{
    m_bbDirty = true;
}

//private
void Model::bindNodeToMaterial(const Material::Ptr& m)
{
    assert(m);
    if (getNode())
    {
        m->bindNode(getNode());
    }
}

void Model::verifySubMeshCount()
{
    assert(m_mesh);
    m_subMeshCount = m_mesh->getSubMeshCount();
}