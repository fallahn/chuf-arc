/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/Scene.hpp>
#include <chuf2/Log.hpp>
#include <chuf2/FrameBuffer.hpp>
#include <chuf2/Texture.hpp>

using namespace chuf;

Scene::Scene(const std::string& name, const Key& key)
    : m_name	    (name),
    m_ambientColour (0.2f, 0.2f, 0.2f),
    m_renderer      (this)
{
    for (auto i = 0; i < static_cast<Int32>(FBO::Count); ++i)
    {
        m_frameBuffers.push_back(nullptr);
    }
}

Scene::~Scene()
{
    removeAllNodes();
    //LOG("Successfully Destroyed Scene");
}

//public
Scene::Ptr Scene::create(const std::string& name)
{
    return std::make_unique<Scene>(name, Key());
}

const std::string& Scene::getName() const
{
    return m_name;
}

void Scene::setName(const std::string& name)
{
    m_name = name;
}

Node* Scene::findNode(const std::string& name, bool recursive)
{
    for (const auto& n : m_nodes)
    {
        if (n->getName() == name) return n.get();
    }

    if (recursive)
    {
        for (const auto& n : m_nodes)
        {
            auto result = n->findNode(name, true);
            if (result) return result;
        }
    }
    return nullptr;
}

UInt32 Scene::findNodes(const std::string& name, std::vector<Node*>& dest, bool recursive)
{
    auto count = 0u;
    for (const auto& n : m_nodes)
    {
        if (n->getName() == name)
        {
            count++;
            dest.push_back(n.get());
        }
        if (recursive)
        {
            count += n->findNodes(name, dest, true);
        }
    }

    return count;
}

Node& Scene::addNode(const std::string& name)
{
    auto node = Node::create(name);
    auto& retVal = *node;
    addNode(node);
    return retVal;
}

void Scene::addNode(Node::Ptr& node)
{
    if (node->getScene() == this) return;

    node->setScene(this);

    if (!m_activeCamera)
    {
        if (auto c = node->getComponent<Camera>())
        {
            setActiveCamera(c);
        }
    }
    m_nodes.push_back(std::move(node));
}

Node::Ptr Scene::removeNode(Node* node)
{
    if (node->getScene() != this) return nullptr;

    node->remove();
    node->setScene(nullptr);

    auto result = std::find_if(m_nodes.begin(), m_nodes.end(), [&](Node::Ptr& p){return (p.get() == node); });
    assert(result != m_nodes.end());

    Node::Ptr p = std::move(*result);
    p->m_parent = nullptr;
    p->m_parentIndex = -1;
    m_nodes.erase(result);
    return p;
}

void Scene::removeAllNodes()
{
    while (m_nodes.size())
    {
        removeNode(m_nodes[0].get());
    }
}

UInt32 Scene::getNodeCount() const
{
    return m_nodes.size();
}

Node* Scene::getFirstNode() const
{
    if (m_nodes.size()) return m_nodes[0].get();
    return nullptr;
}

const Colour& Scene::getAmbientColour() const
{
    return m_ambientColour;
}

void Scene::setAmbientColour(const Colour& colour)
{
    m_ambientColour = colour;
}

Camera* Scene::getActiveCamera() const
{
    return m_activeCamera;
}

void Scene::setActiveCamera(Camera* camera)
{
    if (m_activeCamera)
    {
        m_activeCamera->setActive(false);
    }
    m_activeCamera = camera;
    if (m_activeCamera)
    {
        m_activeCamera->setActive(true);
    }
}

void Scene::traverse(Scene::Callback func, void* userData)
{
    for (auto& n : m_nodes)
    {
        traverseNode(*n, func, userData);
    }
}

FrameBuffer* Scene::getFrameBuffer(Scene::FBO id) const
{
    return m_frameBuffers[static_cast<int>(id)];
}

SceneRenderer& Scene::getRenderer()
{
    return m_renderer;
}

void Scene::update(float dt)
{
    m_nodes.erase(std::remove_if(m_nodes.begin(), m_nodes.end(), 
        [](const Node::Ptr& n)
    {
        return n->destroyed();
    }), m_nodes.end());

    for (auto& n : m_nodes)
    {
        n->update(dt);
    }
}

void Scene::enableReflections()
{
    auto fbo = FrameBuffer::create("reflection", 320u, 240u);
    fbo->setDepthStencilTarget(DepthStencilTarget::create("reflection", DepthStencilTarget::Format::Depth, 320u, 240u));
    m_frameBuffers[static_cast<int>(Scene::FBO::Reflection)] = fbo;

    fbo = FrameBuffer::create("refraction", 640u, 480u);
    fbo->setDepthStencilTarget(DepthStencilTarget::create("refraction", DepthStencilTarget::Format::Depth, 640u, 480u));
    m_frameBuffers[static_cast<int>(Scene::FBO::Refraction)] = fbo;

    std::vector<RenderPass> passes = { RenderPass::Reflection, RenderPass::Refraction, RenderPass::Standard };
    m_renderer.buildRenderPath(passes);
}

//private
void Scene::traverseNode(Node& node, Scene::Callback func, void* userData)
{
    if(!func(node, userData)) return;
    auto& children = node.getChildren();
    for (auto& c : children)
    {
        traverseNode(*c, func, userData);
    }
}
