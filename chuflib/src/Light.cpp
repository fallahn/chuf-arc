/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/Light.hpp>
#include <chuf2/Log.hpp>
#include <chuf2/Node.hpp>

using namespace chuf;

Light::Light(const Colour& colour, float range, const Light::Key& key)
    : m_type	(Type::Point),
    m_node		(nullptr)
{
    m_light = std::make_unique<Point>(colour, range);
}

Light::Light(const Colour& colour, float range, float innerAngle, float outerAngle, const Light::Key& key)
    : m_type	(Type::Spot),
    m_node		(nullptr)
{
    m_light = std::make_unique<Spot>(colour, range, innerAngle, outerAngle);
}

Light::Light(const Colour& colour, const Light::Key& key)
    : m_type	(Type::Directional),
    m_node		(nullptr)
{
    m_light = std::make_unique<Directional>(colour);
}


//public
Light::Type Light::getType() const
{
    return m_type;
}

const Colour& Light::getColour() const
{
    return m_light->colour;
}

void Light::setColour(const Colour& colour)
{
    m_light->colour = colour;
}

Node* Light::getNode() const
{
    return m_node;
}

float Light::getRange() const
{
    switch (m_type)
    {
    case Type::Point:
        return static_cast<Point*>(m_light.get())->range;
    case Type::Spot:
        return static_cast<Spot*>(m_light.get())->range;
    case Type::Directional:
    default:
        Logger::Log("Light of this type has no range parameter", Logger::Type::Info);
        return 0.f;
    }
}

void Light::setRange(float range)
{
    switch (m_type)
    {
    case Type::Point:
    {
        auto l = static_cast<Point*>(m_light.get());
        l->range = range;
        l->rangeInverse = 1.f / range;
    }
        break;
    case Type::Spot:
    {
        auto l = static_cast<Spot*>(m_light.get());
        l->range = range;
        l->rangeInverse = 1.f / range;
    }
        break;
    case Type::Directional:
    default:
        Logger::Log("Light of this type has no range parameter", Logger::Type::Info);
        break;
    }

    if (m_node) m_node->setBoundsDirty();
}

float Light::getRangeInverse() const
{
    switch (m_type)
    {
    case Type::Point:
        return static_cast<Point*>(m_light.get())->rangeInverse;
    case Type::Spot:
        return static_cast<Spot*>(m_light.get())->rangeInverse;
    case Type::Directional:
    default:
        Logger::Log("Light of this type has no range parameter", Logger::Type::Info);
        return 0.f;
    }
}

float Light::getInnerAngle() const
{
    assert(m_type == Type::Spot);
    return static_cast<Spot*>(m_light.get())->innerAngle;
}

void Light::setInnerAngle(float angle)
{
    assert(m_type == Type::Spot);
    auto l = static_cast<Spot*>(m_light.get());
    l->innerAngle = angle;
    l->innerAngleCos = std::cos(angle);
}

float Light::getOuterAngle() const
{
    assert(m_type == Type::Spot);
    return static_cast<Spot*>(m_light.get())->outerAngle;
}

void Light::setOuterAngle(float angle)
{
    assert(m_type == Type::Spot);
    auto l = static_cast<Spot*>(m_light.get());
    l->outerAngle = angle;
    l->outerAngleCos = std::cos(angle);

    if (m_node) m_node->setBoundsDirty();
}

float Light::getInnerAngleCos() const
{
    assert(m_type == Type::Spot);
    return static_cast<Spot*>(m_light.get())->innerAngleCos;
}

float Light::getOuterAngleCos() const
{
    assert(m_type == Type::Spot);
    return static_cast<Spot*>(m_light.get())->outerAngleCos;
}

Light::Ptr Light::createPoint(const Colour& colour, float range)
{
    return std::make_unique<Light>(colour, range, Light::Key());
}

Light::Ptr Light::createSpot(const Colour& colour, float range, float innerAngle, float outerAngle)
{
    return std::make_unique<Light>(colour, range, innerAngle, outerAngle, Light::Key());
}

Light::Ptr Light::createDirectional(const Colour& colour)
{
    return std::make_unique<Light>(colour, Light::Key());
}

//private
Light::BaseLight::BaseLight(const Colour& c)
    : colour	(c)
{

}

Light::Point::Point(const Colour& colour, float r)
    : BaseLight	(colour),
    range		(r),
    rangeInverse(1.f / r)
{

}

Light::Spot::Spot(const Colour& colour, float r, float i, float o)
    : BaseLight	(colour),
    range			(r),
    rangeInverse    (1.f / r),
    innerAngle		(i),
    innerAngleCos   (std::cos(i)),
    outerAngle		(o),
    outerAngleCos	(std::cos(o))
{

}

Light::Directional::Directional(const Colour& colour)
    : BaseLight	(colour)
{

}
