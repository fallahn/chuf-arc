/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/Font.hpp>
#include <chuf2/Image.hpp>
#include <chuf2/Texture.hpp>
#include <chuf2/Log.hpp>
#include <chuf2/Util.hpp>

#include <array>
#include <cassert>

using namespace chuf;

namespace
{
    const FloatRect defaultRect(0.f, 0.f, 0.f, 0.f);
    const UInt16 maxGlyphHeight = 40u; //maximum char size in pixels
    const UInt16 pointsPerPixel = 64u;
    const UInt16 charCountX = 16u;
    const UInt16 charCountY = 16u;
    const UInt16 charCount = charCountX * charCountY; //just while we use extended ascii

    //rounds n to nearest pow2 value for padding images
    unsigned pow2(unsigned n)
    {
        assert(n);
        n--;
        for (auto i = 1u; i < 32u; i *= 2u)
            n |= (n >> i);
        n++;

        return n;
    }
}

std::vector<Font::Ptr> Font::fontCache;

Font::Font(const Font::Key& key)
    : m_texture (nullptr)
{

}

Font* Font::create(const std::string& path)
{
    std::string name; //TODO make this a bit more elegant
    auto pos = path.find_last_of('/');
    if (pos == std::string::npos)
    {
        pos = path.find_last_of('\\');
    }
    if (pos != std::string::npos)
    {
        name = path.substr(pos);
        Util::String::removeChar(name, '\\');
        Util::String::removeChar(name, '/');
    }
    else
    {
        Logger::Log("FONT: " + path + " not a valid font path.", Logger::Type::Error);
        return nullptr;
    }

    //check font cache and return that
    auto result = std::find_if(fontCache.begin(), fontCache.end(),
        [&name](const Font::Ptr& p)
    {
        return (p->getName() == name);
    });
    if (result != fontCache.end()) return result->get();


    //create a ft library for each font, for better destruction handling
    //and because ft requires an instance for each thread it runs in
    FT_RAII ft;
    auto error = FT_Init_FreeType(&ft.flib);
    if (error)
    {
        Logger::Log("Failed to init freetype error: " + std::to_string(error), Logger::Type::Error);
        return nullptr;
    }

    //load the first font face - TODO check available faces and try requesting bold, italic etc see FT_Open_Face()
    error = FT_New_Face(ft.flib, path.c_str(), 0, &ft.face);
    if (error)
    {
        Logger::Log("Failed to load font \'" + path + "\' error: " + std::to_string(error), Logger::Type::Error);
        return nullptr;
    }

    //set face maxmimum size
    error = FT_Set_Pixel_Sizes(ft.face, 0u, maxGlyphHeight);
    if (error)
    {
        Logger::Log("Failed to set font max size' error: " + std::to_string(error), Logger::Type::Error);
        return nullptr;
    }

    //width and height of image for each glyph in atlas
    UInt32 celWidth = 0u;
    UInt32 celHeight = 0u;
    Int32 maxHeight = 0u; //max height of char above baseline
    Int32 minDrop = 0u; //max drop of char below baseline
    std::array<FT_Glyph_Metrics, charCount> metrics; //create the ascii set for now, worry about unicode when it actually works
    std::array<GlyphData, charCount> glyphData;

    for (auto i = 0u; i < charCount; ++i)
    {
        error = FT_Load_Char(ft.face, i, FT_LOAD_RENDER);
        if (!error)
        {
            metrics[i] = ft.face->glyph->metrics;
            
            //load 8 bit data into image
            glyphData[i].width = ft.face->glyph->bitmap.width;
            glyphData[i].height = ft.face->glyph->bitmap.rows;
            auto dataSize = glyphData[i].width * glyphData[i].height;
            glyphData[i].data = std::unique_ptr<UInt8[]>(new UInt8[dataSize]);
            std::memcpy(&glyphData[i].data[0], ft.face->glyph->bitmap.buffer, dataSize);

            
            //calc size from metrics
            if (metrics[i].horiBearingY > maxHeight)
            {
                maxHeight = metrics[i].horiBearingY;
            }
            if (metrics[i].width > celWidth)
            {
                celWidth = metrics[i].width;
            }
            auto hang = metrics[i].horiBearingY - metrics[i].height;
            if (hang < minDrop)
            {
                minDrop = hang;
            }
        }
        else
        {
            Logger::Log("failed to load glyph at " + std::to_string(i) + " skipping...", Logger::Type::Warning);
        }
    }
    //convert size to pixels
    maxHeight /= pointsPerPixel;
    celWidth /= pointsPerPixel;
    minDrop /= pointsPerPixel;
    celHeight = maxHeight - minDrop;

    //copy glyphs to image atlas and convert to texture
    auto imgWidth = pow2(celWidth * charCountX);
    auto glyphCountX = imgWidth / celWidth;
    auto glyphCountY = (charCount / glyphCountX) + 1;  //add row for remainder
    auto imgHeight = pow2(celHeight * glyphCountY);
    auto imgSize = imgWidth * imgHeight;

    std::vector<UInt8> imgData(imgSize);
    std::memset(&imgData[0], 0u, imgSize); //make sure alpha is zero by default
    FloatRect subRect = { 0.f, 0.f, static_cast<float>(celWidth), static_cast<float>(celHeight) };
    UInt16 i = 0u;

    auto font = std::make_unique<Font>(Font::Key());
    font->m_name = name;

    for (auto y = 0u; y < glyphCountY; ++y)
    {
        for (auto x = 0u; x < glyphCountX && i < charCount; ++x, ++i)
        {
            auto cx = celWidth * x;
            auto cy = celHeight * y;
            
            subRect.left = static_cast<float>(cx);
            subRect.top = static_cast<float>(cy);
            subRect.width = static_cast<float>(metrics[i].width / pointsPerPixel);

            font->m_subRectangles.insert(std::make_pair(static_cast<char>(i), subRect)); //TODO is there a signed / unsigned issue here?

            //copy textures to array
            cy += (maxHeight - (metrics[i].horiBearingY / pointsPerPixel));
            for (auto j = 0u; j < glyphData[i].height; ++j)
            {
                std::memcpy(&imgData[(j + cy) * imgWidth + cx], &glyphData[i].data[j * glyphData[i].width], glyphData[i].width);
            }
        }
    }

    auto img = Image::create(imgWidth, imgHeight, Image::Format::A, &imgData[0]);
    assert(img);
    font->m_texture = Texture::create(img, false);
    assert(font->m_texture);

    auto fp = font.get();
    fontCache.push_back(std::move(font));

    return fp;
}

Texture* Font::getAtlas() const
{
    return m_texture;
}

const std::string& Font::getName() const
{
    return m_name;
}

const FloatRect& Font::getChar(char c) const
{
    auto r = m_subRectangles.find(c);
    if (r != m_subRectangles.end())
        return r->second;

    return defaultRect;
}