/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/QuadTree.hpp>
#include <chuf2/Util.hpp>
#include <chuf2/Log.hpp>

#include <cassert>

using namespace chuf;

QuadTreeNode::QuadTreeNode()
    : m_parent              (nullptr),
    m_quadTree              (nullptr),
    m_hasChildren           (false),
    m_level                 (0),
    m_numComponentsBelow    (0){}

QuadTreeNode::QuadTreeNode(const FloatRect& area, Int32 level, QuadTreeNode* parent, QuadTree* quadTree)
    : m_parent              (parent),
    m_quadTree              (quadTree),
    m_hasChildren           (false),
    m_area                  (area),
    m_level                 (level),
    m_numComponentsBelow    (0){}

//public
void QuadTreeNode::create(const FloatRect& area, Int32 level, QuadTreeNode* parent, QuadTree* quadTree)
{
    m_parent = parent;
    m_quadTree = quadTree;
    m_area = area;
    m_level = level;
}

QuadTree* QuadTreeNode::getTree() const
{
    return m_quadTree;
}

void QuadTreeNode::add(QuadTreeComponent* qc)
{
    assert(qc);

    //add to children if fits
    if (m_hasChildren)
    {
        if (addToChildren(qc)) return;
    }
    else //try adding to new partition
    {
        if (m_components.size() >= m_quadTree->maxNodeComponents()
            && m_level < m_quadTree->maxLevels())
        {
            split();
        
        if (addToChildren(qc)) return;
        }
    }

    //else add here as we are at our recursive limit
    addToThis(qc);
}

const FloatRect& QuadTreeNode::getArea() const
{
    return m_area;
}

Int32 QuadTreeNode::getNumComponentsBelow() const
{
    return m_numComponentsBelow;
}

void QuadTreeNode::update(QuadTreeComponent* qc)
{
    if (qc)
    {
        //remove from here
        if (!m_components.empty()) m_components.erase(qc);
        
        //move upwards looking for room
        QuadTreeNode* currentNode = this;
        while (currentNode)
        {
            currentNode->m_numComponentsBelow--;
            if (currentNode->m_area.contains(qc->getGlobalQuadTreeBounds())) break;

            currentNode = currentNode->m_parent;
        }

        //if no node found add to quad tree outside root set
        if (!currentNode)
        {
            assert(m_quadTree);

            auto& outsideSet = m_quadTree->getOutsideRootSet();
            if (outsideSet.find(qc) != outsideSet.end()) return; //already added

            outsideSet.insert(qc);
            qc->setQuadTreeNode(nullptr);
        }
        else //add to found node
        {
            currentNode->add(qc);
        }
    }
}

void QuadTreeNode::remove(QuadTreeComponent* qc)
{
    assert(!m_components.empty());

    m_components.erase(qc);

    if (!qc) return;

    QuadTreeNode* currentNode = this;
    while (currentNode)
    {
        currentNode->m_numComponentsBelow--;

        if (currentNode->m_numComponentsBelow >= m_quadTree->minNodeComponents())
        {
            join();
            break;
        }
        currentNode = currentNode->m_parent;
    }
}

const QuadTreeNode::Set& QuadTreeNode::getComponents() const
{
    return m_components;
}

bool QuadTreeNode::hasChildren() const
{
    return m_hasChildren;
}

const std::array<QuadTreeNode::Ptr, 4u>& QuadTreeNode::getChildren() const
{
    return m_children;
}

//private
void QuadTreeNode::getSubComponents()
{
    //get all components stored in children and move them to this node
    std::vector<QuadTreeNode*> nodeList;
    nodeList.push_back(this);

    while (!nodeList.empty())
    {
        auto currentNode = nodeList.back();
        nodeList.pop_back();

        for (auto qc : currentNode->m_components)
        {
            if (qc != nullptr)
            {
                qc->setQuadTreeNode(this);
                m_components.insert(qc);
            }
        }

        if (currentNode->m_hasChildren)
        {
            for (const auto& c : m_children)
            {
                nodeList.push_back(c.get());
            }
        }
    }
}

glm::uvec2 QuadTreeNode::getPossiblePosition(QuadTreeComponent* qc)
{
    auto componentCentre = qc->getGlobalQuadTreeBounds().centre();
    auto areaCentre = m_area.centre();

    return{ componentCentre.x > areaCentre.x ? 1 : 0, componentCentre.y > areaCentre.y ? 1 : 0 };
}

void QuadTreeNode::addToThis(QuadTreeComponent* qc)
{
    qc->setQuadTreeNode(this);
    if (m_components.find(qc) == m_components.end())
        m_components.insert(qc);
}

bool QuadTreeNode::addToChildren(QuadTreeComponent* qc)
{
    assert(m_hasChildren);

    auto position = getPossiblePosition(qc);

    auto child = m_children[position.x + position.y * 2].get();

    if (child->m_area.contains(qc->getGlobalQuadTreeBounds()))
    {
        child->add(qc);
        m_numComponentsBelow++;
        return true;
    }
    return false;
}

void QuadTreeNode::destroyChildren()
{
    for (auto& c : m_children) c.reset();
    m_hasChildren = false;
}

void QuadTreeNode::split()
{
    assert(!m_hasChildren);

    glm::vec2 areaHalfDims(m_area.width / 2.f, m_area.height / 2.f);
    glm::vec2 areaPosition(m_area.left, m_area.top);
    glm::vec2 areaCentre = m_area.centre();

    Int32 nextLevel = m_level - 1;
    for (auto x = 0; x < 2; ++x)
    {
        for (auto y = 0; y < 2; ++y)
        {
            glm::vec2 offset(x * areaHalfDims.x, y * areaHalfDims.y);
            FloatRect newBounds = Util::Rectangle::fromBounds(areaPosition + offset, areaCentre + offset);

            glm::vec2 newHalfDims(newBounds.width / 2.f, newBounds.height / 2.f);
            glm::vec2 newCentre = newBounds.centre();
            newBounds = Util::Rectangle::fromBounds(newCentre - newHalfDims, newCentre + newHalfDims);

            m_children[x + y * 2] = std::make_unique<QuadTreeNode>(newBounds, nextLevel, this, m_quadTree);
        }
    }
    m_hasChildren = true;
}

void QuadTreeNode::join()
{
    if (m_hasChildren)
    {
        getSubComponents();
        destroyChildren();
    }
}

void QuadTreeNode::clearDestroyed()
{
    for (auto it = m_components.begin(); it != m_components.end();)
    {
        ((*it) == nullptr) ? it++ : it = m_components.erase(it);
    }

    if (m_hasChildren)
    {
        for (auto& c : m_children)
        {
            c->clearDestroyed();
        }
    }
}