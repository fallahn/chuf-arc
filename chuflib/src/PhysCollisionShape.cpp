/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/physics/PhysCollisionShape.hpp>

#include <cassert>

using namespace chuf;

PhysicsCollisionShape::PhysicsCollisionShape(Type type, btCollisionShape* cs, btStridingMeshInterface* smi)
    : m_type            (type),
    m_shape             (cs),
    m_stridingInterface (smi)
{
    std::memset(&m_shapeData, 0, sizeof(m_shapeData));
}

//public
PhysicsCollisionShape::Type PhysicsCollisionShape::getType() const
{
    return m_type;
}

btCollisionShape* PhysicsCollisionShape::getShape() const
{
    return m_shape;
}

PhysicsCollisionShape::Definition PhysicsCollisionShape::box()
{
    Definition def;
    def.m_type = PhysicsCollisionShape::Type::Box;
    def.m_explicit = false;
    def.m_centreAbsolute = false;
    return def;
}

PhysicsCollisionShape::Definition PhysicsCollisionShape::box(const glm::vec3& extents, const glm::vec3& centre, bool absolute)
{
    Definition def;
    def.m_type = PhysicsCollisionShape::Type::Box;
    def.m_explicit = true;
    def.m_centreAbsolute = absolute;

    def.m_data.box.centre[0] = centre.x;
    def.m_data.box.centre[1] = centre.y;
    def.m_data.box.centre[2] = centre.z;

    def.m_data.box.extents[0] = extents.x;
    def.m_data.box.extents[1] = extents.y;
    def.m_data.box.extents[2] = extents.z;
    return def;
}

PhysicsCollisionShape::Definition PhysicsCollisionShape::Sphere()
{
    Definition def;
    def.m_type = PhysicsCollisionShape::Type::Sphere;
    def.m_explicit = false;
    def.m_centreAbsolute = false;
    return def;
}

PhysicsCollisionShape::Definition PhysicsCollisionShape::Sphere(float radius, const glm::vec3& centre, bool absolute)
{
    Definition def;
    def.m_type = PhysicsCollisionShape::Type::Sphere;
    def.m_data.sphere.radius = radius;
    def.m_explicit = true;
    def.m_centreAbsolute = absolute;

    def.m_data.sphere.centre[0] = centre.x;
    def.m_data.sphere.centre[1] = centre.y;
    def.m_data.sphere.centre[2] = centre.z;
    return def;
}

PhysicsCollisionShape::Definition PhysicsCollisionShape::capsule()
{
    Definition def;
    def.m_type = PhysicsCollisionShape::Type::Capsule;
    def.m_explicit = false;
    def.m_centreAbsolute = false;
    return def;
}

PhysicsCollisionShape::Definition PhysicsCollisionShape::capsule(float radius, float length, const glm::vec3& centre, bool absolute)
{
    Definition def;
    def.m_type = PhysicsCollisionShape::Type::Capsule;
    def.m_explicit = true;
    def.m_centreAbsolute = absolute;
    def.m_data.capsule.radius = radius;
    def.m_data.capsule.height = length;

    def.m_data.capsule.centre[0] = centre.x;
    def.m_data.capsule.centre[1] = centre.y;
    def.m_data.capsule.centre[2] = centre.z;
    return def;
}

PhysicsCollisionShape::Definition PhysicsCollisionShape::cylinder()
{
    Definition def;
    def.m_type = PhysicsCollisionShape::Type::Cylinder;
    def.m_explicit = false;
    def.m_centreAbsolute = false;
    return def;
}

PhysicsCollisionShape::Definition PhysicsCollisionShape::cylinder(float radius, float length, const glm::vec3& centre, bool absolute)
{
    Definition def;
    def.m_type = PhysicsCollisionShape::Type::Cylinder;
    def.m_explicit = true;
    def.m_centreAbsolute = absolute;
    def.m_data.capsule.radius = radius;
    def.m_data.capsule.height = length;

    def.m_data.capsule.centre[0] = centre.x;
    def.m_data.capsule.centre[1] = centre.y;
    def.m_data.capsule.centre[2] = centre.z;
    return def;
}

PhysicsCollisionShape::Definition PhysicsCollisionShape::heightfield()
{
    Definition def;
    def.m_type = PhysicsCollisionShape::Type::Heightfield;
    def.m_explicit = false;
    def.m_centreAbsolute = false;
    return def;
}

PhysicsCollisionShape::Definition PhysicsCollisionShape::heightfield(HeightData* hd)
{
    assert(hd);

    Definition def;
    def.m_type = PhysicsCollisionShape::Type::Heightfield;
    def.m_data.heightdata = hd;
    def.m_explicit = true;
    def.m_centreAbsolute = false;
    return def;
}

PhysicsCollisionShape::Definition PhysicsCollisionShape::mesh(Mesh* m)
{
    assert(m);

    Definition def;
    def.m_type = PhysicsCollisionShape::Type::Mesh;
    def.m_data.mesh = m;
    def.m_centreAbsolute = false;
    def.m_explicit = true;
    return def;
}