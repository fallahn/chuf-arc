/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/loaders/SourceBspImpl.hpp>

#include <chuf2/Log.hpp>
#include <chuf2/Util.hpp>
#include <chuf2/SubMesh.hpp>
#include <chuf2/Texture.hpp>
#include <chuf2/Image.hpp>
#include <chuf2/Scene.hpp>
#include <chuf2/Reports.hpp>

#include <cassert>
#include <fstream>
#include <bitset>

using namespace chuf;
using namespace Bsp;

namespace
{
    glm::vec3 toGlm(const Bsp::Source::Vector& v)
    {
        return { v.x, v.y, v.z };
    }

    const chuf::Int32 IDENT = 0x50534256;
    const chuf::UInt16 maxLuxels = 20u; //max size of individual lightmap. max source size is actually 32, but so rare most texture space is wasted.
    const chuf::Int16 maxLightmapWidth = 256;
    const chuf::Int16 maxLightmapHeight = 256;
    const chuf::UInt8 lightmapPadding = 2u;
    const chuf::UInt16 lightmapsPerPage = (maxLightmapWidth / (maxLuxels + lightmapPadding)) * (maxLightmapHeight / (maxLuxels + lightmapPadding));

    const chuf::Int32 maxLeaves = 65536;
    const float epsilon = 0.f;// 1e-12;

    template <typename T>
    void parseLump(std::vector<T>& dest, const char* source, Source::Lumps type, const Source::Header& header)
    {
        UInt32 count = header.lumps[type].fileLength / sizeof(T);
        dest.resize(count);
        std::memcpy(dest.data(), source + header.lumps[type].fileOffset, header.lumps[type].fileLength);
    }

    //index material properties by name more easily
    enum PropertyIndex
    {
        LightMap = 0,
        RNMap = 1,
        ReflectionMatrix = 2
    };

    //utility functions used for adjusting gamma of lightmaps
#include <chuf2/loaders/ConversionTables.hpp>
    std::array<float, 4096> linearToVertex;

    void buildGammaTable(float gamma, chuf::Int32 overbright)
    {
        float f;
        float overbrightFactor = 1.0f;

        if (overbright == 2)
        {
            overbrightFactor = 0.5;
        }
        else if (overbright == 4)
        {
            overbrightFactor = 0.25;
        }

        for (auto i = 0u; i < linearToVertex.size(); i++)
        {
            f = pow(i / 1024.f, 1.f / gamma);

            linearToVertex[i] = f * overbrightFactor;
            if (linearToVertex[i] > 1) linearToVertex[i] = 1.f;
        }
    }

    float textlightToLinear(chuf::UInt8 c, chuf::Int8 exp)
    {
        assert(exp > -128 && exp <= 127);
        return static_cast<float>(c)* power2_n[exp + 128];
    }

    float linearToVertexLight(float f)
    {
        Int32 i = static_cast<Int32>((f * 1024.f + 0.5f));

        if (i >= linearToVertex.size())
        {
            if (i < 0) i = 0;
            else i = linearToVertex.size() - 1;
        }
        return linearToVertex[i];
    }

    UInt8 floatToByte(float f)
    {
        return static_cast<UInt8>(f + 0.5f);
    }

    void clampColour(glm::vec3& colour)
    {
        float maxColour = std::fmax(colour.r, std::fmax(colour.g, colour.b));
        if (maxColour > 1.f)
        {
            float oMax = 1.f / maxColour;
            colour *= oMax;
        }

        if (colour.r < 0) colour.r = 0.f;
        if (colour.g < 0) colour.g = 0.f;
        if (colour.b < 0) colour.b = 0.f;
    }

    glm::vec3 colourShiftPixel(const Source::ColourRGB32Exp& colour)
    {
        glm::vec3 linearColour =
        {
            textlightToLinear(colour.r, colour.exponent),
            textlightToLinear(colour.g, colour.exponent),
            textlightToLinear(colour.b, colour.exponent)
        };
        return
        {
            linearToVertexLight(linearColour.r),
            linearToVertexLight(linearColour.g),
            linearToVertexLight(linearColour.b)
        };
    }

    void colourShiftLightmap(const Source::ColourRGB32Exp& src, UInt8* dst, const glm::vec3& avgColour)
    {
        glm::vec3 vertColour = colourShiftPixel(src);
        glm::vec3 addColour = vertColour + avgColour;

        clampColour(addColour);
        auto i = 0u;
        dst[i++] = floatToByte(vertColour.r * 255.f);
        dst[i++] = floatToByte(vertColour.g * 255.f);
        dst[i] = floatToByte(vertColour.b * 255.f);
    }
}

SourceBspImpl::SourceBspImpl(const std::string& path)
{
    buildGammaTable(2.2f, 1); //overbright could be 2?
    assert(loadMap(path));
}

//public
void SourceBspImpl::draw(UInt32 passFlags)
{
    if ((passFlags & RenderPass::Reflection) && flagDirty(DirtyBits::FRUSTUM_REFLECTION))
    {
        auto camera = getCamera();
        const auto& reflectionFrustum = camera->getReflectionViewFrustum();
        const auto camPos = camera->getNode()->getTranslationWorld();
        updateVisibility(camPos, reflectionFrustum);
    }
    else if (flagDirty(DirtyBits::FRUSTUM))
    {
        auto camera = getCamera();
        const auto& frustum = camera->getViewFrustum();
        const auto camPos = camera->getNode()->getTranslationWorld();
        updateVisibility(camPos, frustum);
    }
    
    if (passFlags & RenderPass::AlphaBlend)
    {
        auto& indices = getTransparentFaces();
        for (auto i : indices)
        {
            drawFace(i, passFlags);
        }
        indices.clear();
        return;
    }

    //TODO stencil reflective/refractive pass

    //else just draw everything
    const auto& visibleFaces = getVisibleFaces();
    for (auto i : visibleFaces)
        drawFace(i, passFlags);
}

void SourceBspImpl::transformChanged(Transform& t)
{
    transformLeafBoundingBoxes();
}

void SourceBspImpl::setNode(Node* n)
{
    BspImpl::setNode(n);
    if (n) transformLeafBoundingBoxes();
}

//private
void SourceBspImpl::updateVisibility(const glm::vec3& position, const Frustum& frustum)
{
    auto& visibleFaces = getVisibleFaces();
    visibleFaces.clear();

    //remember to put position in bsp space
    auto currentLeaf = findLeaf(glm::vec3(glm::vec4(position, 1.f) * getNode()->getInverseTransposeWorldMatrix()));
    auto currentCluster = m_leaves[currentLeaf].cluster;

    int clustersSkipped = 0;
    int leavesCulled = 0;
    int facesDrawn = 0;

    std::bitset<100000> bitset;

    Int32 i = m_leaves.size();
    while (i--)
    {
        //skip clusters which aren't in current PVS
        const auto& leaf = m_leaves[i];
        if (!clusterVisible(currentCluster, leaf.cluster))
        {
            clustersSkipped++;
            continue;
        }

        //remove leaves which don't intersect view frustum
        const BoundingBox& bb = m_boundingBoxes[i].second;
        if (!frustum.intersects(bb))
        {
            leavesCulled++;
            continue;
        }

        //if the leaf contains water set the camera's reflection plane
        if (leaf.leafWaterId > -1)
        {
            const auto node = getNode(); //remember to put the plane into world space
            node->getScene()->getActiveCamera()->setReflectionPlane(node->getInverseTransposeWorldMatrix() * glm::vec4(0.f, 1.f, 0.f,
                -m_waterData[leaf.leafWaterId].surfaceZ));

            //TODO store MVP in array //TODO store only once for leaves with same water level
            //TODO store plane so it's easier to flip between buffers?
        }

        //group and sort all the visible faces
        Int32 faceCount = leaf.leafFaceCount;
        while (faceCount--)
        {
            Int32 faceIndex = m_leafFaces[leaf.firstLeafFace + faceCount];
            if (!bitset.test(faceIndex))
            {
                bitset.set(faceIndex);
                visibleFaces.push_back(faceIndex);
                facesDrawn++;

                //TODO if face in water leaf store index of MVP array (make visible face array a pair of values, set -1 if no matrix)
            }
        }
    }

    //minimise state switching by sorting by material
    std::sort(visibleFaces.begin(), visibleFaces.end(), [&](Int32 left, Int32 right)
    {
        return (m_textureData[m_textureInfo[m_faces[left].textureInfo].textureData].nameStringIndex <
            m_textureData[m_textureInfo[m_faces[right].textureInfo].textureData].nameStringIndex);
    });

    REPORT("Current Leaf", std::to_string(currentLeaf));
    REPORT("Clusters Skipped", std::to_string(clustersSkipped));
    REPORT("Leaves Culled", std::to_string(leavesCulled));
    REPORT("Faces Drawn", std::to_string(facesDrawn));
}

bool SourceBspImpl::loadMap(const std::string& map)
{
    std::fstream file;
    file.open(map, std::ios::in | std::ios::binary);

    if (file.fail())
    {
        Logger::Log("Failed to open file " + map, Logger::Type::Error);
        return false;
    }

    file.seekg(0, std::ios::end);
    UInt32 fileSize = static_cast<UInt32>(file.tellg());
    file.seekg(0, std::ios::beg);

    if (fileSize == 0u)
    {
        Logger::Log("BSP file size 0", Logger::Type::Error);
        return false;
    }

    //copy file into memory and do all ops from there
    std::vector<char> bspData(fileSize);
    file.read((char*)bspData.data(), fileSize);
    file.close();

    //---------------------------------------------------------------

    Bsp::Source::Header header;

    char* dataPtr = bspData.data();
    std::memcpy(&header, dataPtr, sizeof(Bsp::Source::Header));

    //check header ident is correct for source BSP
    if (header.id != IDENT)
    {
        Logger::Log("BSP id: " + std::to_string(header.id) + " not a Source BSP file.", Logger::Type::Error);
        return false;
    }

    //check header version 20 - 21
    if (header.version < 20 || header.version > 21)
    {
        Logger::Log("Source BSP version: " + std::to_string(header.version) + " not supported. Must be 20 or 21.", Logger::Type::Error);
        return false;
    }

    //load vertices
    std::vector<Source::Vertex> vertices;
    parseLump<Source::Vertex>(vertices, dataPtr, Source::Lumps::Vertices, header);

    //load faces
    parseLump<Source::Face>(m_faces, dataPtr, Source::Lumps::Faces, header);

    //load texture info
    parseLump<Source::TexInfo>(m_textureInfo, dataPtr, Source::Lumps::TextureInfo, header);
    parseLump<Source::TexData>(m_textureData, dataPtr, Source::Lumps::TextureData, header);
    std::vector<Int32> stringTable;
    parseLump<Int32>(stringTable, dataPtr, Source::Lumps::TextureDataStringTable, header);
    std::vector<char> stringData;
    parseLump<char>(stringData, dataPtr, Source::Lumps::TextureDataStringData, header);

    //load materials
    loadMaterials(stringTable, stringData);

    //load lightmaps
    std::vector<Source::ColourRGB32Exp> lightSamples;
    parseLump<Source::ColourRGB32Exp>(lightSamples, dataPtr, Source::Lumps::Lighting, header);
    loadLightmaps(lightSamples);

    //load edges
    std::vector<Source::Edge> edges;
    parseLump<Source::Edge>(edges, dataPtr, Source::Lumps::Edges, header);
    std::vector<Int32> surfEdges;
    parseLump<Int32>(surfEdges, dataPtr, Source::Lumps::SurfEdges, header);

    createMesh(vertices, edges, surfEdges); //this must be done after materials are loaded so attributes are properly bound

    //load visibility data
    parseLump<Source::Node>(m_nodes, dataPtr, Source::Nodes, header);
    parseLump<Source::Leaf>(m_leaves, dataPtr, Source::Leaves, header);

    //calc bounding boxes for culling
    for (const auto& l : m_leaves)
    {
        BoundingBox bb({ l.bbMin[0], l.bbMin[2], -l.bbMin[1] }, { l.bbMax[0], l.bbMax[2], -l.bbMax[1] });
        m_boundingBoxes.emplace_back(std::make_pair(bb, bb));
    }

    parseLump<Int16>(m_leafFaces, dataPtr, Source::LeafFaces, header);
    parseLump<Source::Plane>(m_planes, dataPtr, Source::Planes, header);
    for(auto& p : m_planes) //swap coords
    {
        float temp = p.normal.y;
        p.normal.y = p.normal.z;
        p.normal.z = -temp;
    }

    if (header.lumps[Source::Visibility].fileLength > 0)
    {
        auto lumpStart = dataPtr + header.lumps[Source::Visibility].fileOffset;
        Int32 clusterCount;
        std::memcpy(&clusterCount, lumpStart, sizeof(Int32));

        std::vector<std::array<UInt32, 2>> offsets(clusterCount);
        std::memcpy(offsets.data(), lumpStart + sizeof(Int32), sizeof(Int32[2]) * clusterCount);

        //decompress PVS sets
        decompressPVS(clusterCount, offsets, lumpStart);
    }


    //load lighting data
    std::vector<Source::WorldLight> lights;
    parseLump<Source::WorldLight>(lights, dataPtr, Source::WorldLights, header);

    //load water info
    parseLump<Source::LeafWaterInfo>(m_waterData, dataPtr, Source::LeafWaterData, header);

    return true;
}

void SourceBspImpl::loadMaterials(const std::vector<Int32>& lookupTable, const std::vector<char>& stringData)
{
    auto& materials = getMaterials();
    for (auto l : lookupTable)
    {
        std::string name(&stringData[l]); //TODO bsps seem to store names as all caps :S
        auto material = Material::createFromFile("res/materials/" + name + ".cmf"/*"res/materials/cobbles.cmf"*/);
        if (!material) material = Material::getDefault(Material::Type::LightMapped);
        materials.push_back(
            std::make_pair(material,
            std::vector<MaterialProperty*>({
            material->getProperty("u_lightmapTexture"),
            material->getProperty("u_radNormTextures"),
            material->getProperty("u_reflectionWorldViewProjectionMatrix")
        })));
    }

    //TODO go through faces and set mip data on materials
}

void SourceBspImpl::loadLightmaps(std::vector<Source::ColourRGB32Exp>& samples)
{
    auto& lightmaps = getLightmaps();
    const UInt8 atlasCount = 4u;

    std::vector<Image::Ptr> atlases;
    for (auto i = 0u; i < atlasCount; ++i)
    {
        atlases.emplace_back(Image::create(maxLightmapWidth, maxLightmapHeight, Image::Format::RGB));
    }

    UInt32 currentX = 0u;
    UInt32 currentY = 0u;
    UInt32 nextY = 0u;

    for (auto f = 0u; f < m_faces.size(); ++f)
    {
        //check we fit on current page else create a new one
        UInt16 currentPage = f / lightmapsPerPage;
        if (currentPage > 0 && lightmaps.size() < currentPage)
        {
            storeAtlases(atlases);

            //reset all atlases
            for (auto& a : atlases) a = std::move(Image::create(maxLightmapWidth, maxLightmapHeight, Image::Format::RGB));
            currentX = currentY = nextY = 0u;
        }

        const auto& face = m_faces[f];
        auto width = face.lightmapTextureSizeInLuxels[0] + 1;
        auto height = face.lightmapTextureSizeInLuxels[1] + 1;
        auto luxelCount = width * height;
        if (width <= 0 || height <= 0)
        {
            //this face has no lightmap
            //need to pack out the array accordingly
            m_scaleOffsets.emplace_back(std::make_pair(glm::vec2(), glm::vec2()));
            continue;
        }

        //up to 4 styles of lightmap for example a flickering light
        //or a light which changes colour. We only load the first one for now
        Int32 styleCount = 0;
        for ( ; styleCount < 4; ++styleCount)
        {
            if (face.styles[styleCount] == 255u) break;
        }
        if (styleCount == 0 || face.lightOffset < 0)
        {
            //pad array
            m_scaleOffsets.emplace_back(std::make_pair(glm::vec2(), glm::vec2()));
            continue;
        }

        const UInt8 bpp = 3u;
        std::vector<std::vector<UInt8>> lightmapData =
        {
            std::vector<UInt8>(luxelCount * bpp),
            std::vector<UInt8>(luxelCount * bpp),
            std::vector<UInt8>(luxelCount * bpp),
            std::vector<UInt8>(luxelCount * bpp)
        };

        for (auto& v : lightmapData) std::memset(v.data(), 127u, v.size()); //set lightmap to default to grey

        //TODO go backwards for each one of styleCount
        auto avgColour = colourShiftPixel(samples[(face.lightOffset / sizeof(Source::ColourRGB32Exp)) - 1]);

        for (auto i = 0; i < luxelCount; ++i)
        {
            auto inPos = (face.lightOffset / sizeof(Source::ColourRGB32Exp)) + i;
            auto outPos = i * bpp;
            colourShiftLightmap(samples[inPos], &lightmapData[0][outPos], avgColour);
            //grab the rad bump maps if surface is bumped
            if (m_textureInfo[face.textureInfo].mipFlags & Source::SurfBumpLight)
            {
                inPos += luxelCount;
                colourShiftLightmap(samples[inPos], &lightmapData[1][outPos], avgColour);

                inPos += luxelCount;
                colourShiftLightmap(samples[inPos], &lightmapData[2][outPos], avgColour);

                inPos += luxelCount;
                colourShiftLightmap(samples[inPos], &lightmapData[3][outPos], avgColour);
            }
        }

        //check we fit first and start a new line if necessary
        if (currentX + width > maxLightmapWidth)
        {
            currentX = 0u;
            currentY = nextY;
            nextY = 0u;
        }

        //add the face lightmaps to the atlases - TODO use a less wasteful packing algorithm
        for (auto i = 0u; i < atlasCount; ++i)
        {
            atlases[i]->setSubrect({ currentX, currentY }, *Image::create(width, height, Image::Format::RGB, lightmapData[i].data()));
        }
        //to update UVs
        glm::vec2 scale(static_cast<float>(width - 1) / maxLightmapWidth, static_cast<float>(height - 1) / maxLightmapHeight);
        glm::vec2 offset(static_cast<float>(currentX + 1) / maxLightmapWidth, static_cast<float>(currentY + 1) / maxLightmapHeight);
        m_scaleOffsets.emplace_back(std::make_pair(scale, offset));

        currentX += (width + lightmapPadding);
        if (currentY + height >= nextY) nextY = currentY + height + lightmapPadding;
    }

    storeAtlases(atlases);
}

void SourceBspImpl::calcTexCoords(const std::vector<glm::vec3>& vertices, std::vector<glm::vec2>&uvCoords0, std::vector<glm::vec2>&uvCoords1, const std::vector<Int32>& indices, UInt32 faceIndex)
{
    const auto& face = m_faces[faceIndex];
    const auto& texInf = m_textureInfo[face.textureInfo];
    const auto& texVecs = texInf.textureVectorsTexelsPerWorldUnit;
    const auto& lightVecs = texInf.lightmapVectorsLuxelsPerWorldUnit;

    //glm::vec2 texSize = { m_textureData[texInf.textureData].width, m_textureData[texInf.textureData].width };
    //this covers when materials use textures with a resolution different to that stored in the bsp data
    glm::vec2 texSize = getMaterials()[m_textureData[texInf.textureData].nameStringIndex].first->getTagVec2("dimensions");
    assert(texSize.x > 0 && texSize.y > 0);

    for (auto i : indices)
    {
        const auto& vert = vertices[i];
        uvCoords0[i] =
        {
            (texVecs[0][0] * vert.x + texVecs[0][1] * vert.y + texVecs[0][2] * vert.z + texVecs[0][3]) / texSize.x,
            (texVecs[1][0] * vert.x + texVecs[1][1] * vert.y + texVecs[1][2] * vert.z + texVecs[1][3]) / texSize.y
        };

        uvCoords1[i] =
        {
            (lightVecs[0][0] * vert.x + lightVecs[0][1] * vert.y + lightVecs[0][2] * vert.z + lightVecs[0][3]
                - static_cast<float>(face.lightmapTextureMinsInLuxels[0])) / static_cast<float>(face.lightmapTextureSizeInLuxels[0] + 1),

            (lightVecs[1][0] * vert.x + lightVecs[1][1] * vert.y + lightVecs[1][2] * vert.z + lightVecs[1][3]
                - static_cast<float>(face.lightmapTextureMinsInLuxels[1])) / static_cast<float>(face.lightmapTextureSizeInLuxels[1] + 1)
        };

        //update to packed texture coords
        uvCoords1[i] = (uvCoords1[i] * m_scaleOffsets[faceIndex].first) + m_scaleOffsets[faceIndex].second;
    }
}

void SourceBspImpl::calcNormals(const std::vector<glm::vec3>& vertices, std::vector<glm::vec2>&uvCoords0,
    std::vector<glm::vec3>& normals, std::vector<glm::vec3>& tangents, std::vector<glm::vec3>& bitangents, const std::vector<Int32>& indices)
{
    UInt32 index0 = 0;
    UInt32 index1 = 1;// indices.size() - 2;
    UInt32 index2 = 2;// indices.size() - 1;

    std::vector<glm::vec3> facePositions =
    {
        vertices[indices[index0]],
        vertices[indices[index1]],
        vertices[indices[index2]]
    };


    //------------------------------------------------version 1------------------------------------------

    glm::vec3 normal, deltaPos1, deltaPos2;

    while (glm::length(normal) == 0 && (index0 + 1) < indices.size())
    {
        //calc normal from face
        deltaPos1 = facePositions[1] - facePositions[0];
        deltaPos2 = facePositions[2] - facePositions[0];
        normal = glm::cross(deltaPos1, deltaPos2);

        //try as many combinations as possible to get a valid face
        if (glm::length(normal) == 0)
        {
            index0++;
            /*if (index0 == indices.size())
            {
                index0 = 0;
                index1++;

                if (index1 == indices.size())
                {
                    index1 = 0;
                    index2++;
                    facePositions[2] = vertices[index2];
                }
                facePositions[1] = vertices[index1];
            }*/
            facePositions[0] = vertices[index0];
        }
    }

    if (glm::length(normal) == 0)
    {
        Logger::Log("Normal generation failed on face", Logger::Type::Warning);
        //for (auto i : indices)
        {
            uvCoords0[indices[index0]] = { 0, 0 };
            uvCoords0[indices[index1]] = { 0, 0 };
            uvCoords0[indices[index2]] = { 0, 0 };
        }
    }

    //use UV coords to calc tangents
    std::vector<glm::vec2> faceUVs =
    {
        uvCoords0[indices[index0]],
        uvCoords0[indices[index1]],
        uvCoords0[indices[index2]]
    };

    auto deltaUV1 = faceUVs[1] - faceUVs[0];
    auto deltaUV2 = faceUVs[2] - faceUVs[0];

    const float sign = 1.f / (deltaUV1.x * deltaUV2.y - deltaUV1.y * deltaUV2.x);
    glm::vec3 tangent = -(deltaPos1 * deltaUV2.y - deltaPos2 * deltaUV1.y) * sign;
    glm::vec3 bitangent = -(deltaPos2 * deltaUV1.x - deltaPos1 * deltaUV2.x) * sign;

    //----------------------------------------version 2------------------------------------------------------------------

    /*glm::vec3 tangent;
    glm::vec3 bitangent;

    glm::vec3 deltaOne(facePositions[1].x - facePositions[0].x, faceUVs[1].x - faceUVs[0].x, faceUVs[1].y - faceUVs[0].y);
    glm::vec3 deltaTwo(facePositions[2].x - facePositions[0].x, faceUVs[2].x - faceUVs[0].x, faceUVs[2].y - faceUVs[0].y);

    auto cross = glm::cross(deltaOne, deltaTwo);
    if (std::fabs(cross.x) > epsilon)
    {
        tangent.x += -cross.y / cross.x;
        bitangent.x += -cross.z / cross.x;
    }

    deltaOne = { facePositions[1].y - facePositions[0].y, faceUVs[1].x - faceUVs[0].x, faceUVs[1].y - faceUVs[0].y };
    deltaTwo = { facePositions[2].y - facePositions[0].y, faceUVs[2].x - faceUVs[0].x, faceUVs[2].y - faceUVs[0].y };

    cross = glm::cross(deltaOne, deltaTwo);
    if (std::fabs(cross.x) > epsilon)
    {
        tangent.y += -cross.y / cross.x;
        bitangent.y += -cross.z / cross.x;
    }

    deltaOne = { facePositions[1].z - facePositions[0].z, faceUVs[1].x - faceUVs[0].x, faceUVs[1].y - faceUVs[0].y };
    deltaTwo = { facePositions[2].z - facePositions[0].z, faceUVs[2].x - faceUVs[0].x, faceUVs[2].y - faceUVs[0].y };

    cross = glm::cross(deltaOne, deltaTwo);
    if (std::fabs(cross.x) > epsilon)
    {
        tangent.z += -cross.y / cross.x;
        bitangent.z += -cross.z / cross.x;
    }

    auto normal = glm::cross(tangent, bitangent);

    if (glm::length(normal) == 0)
    {
        Logger::Log("Normal generation failed on face", Logger::Type::Warning);
    }*/

    //-----------------------------------------------------------------------------------------------------------------

    for (auto i : indices)
    {
        normals[i] += normal;
        tangents[i] += tangent;
        bitangents[i] += bitangent;
    }
}

std::vector<Int32> SourceBspImpl::getFirstVerts(const std::vector<Source::Edge>& edges, const std::vector<Int32>& surfEdges, UInt32 faceIndex)
{
    const auto& face = m_faces[faceIndex];
    auto index = surfEdges[face.firstEdge];
    auto reverse = (index < 0);
    auto edge = edges[std::abs(index)];

    std::vector<Int32> retVal = {edge.vertices[reverse ? 1 : 0], edge.vertices[reverse ? 0 : 1]};

    index = surfEdges[face.firstEdge + 1];
    reverse = (index < 0);
    edge = edges[std::abs(index)];

    retVal.push_back(edge.vertices[reverse ? 0 : 1]);
    return retVal;
}

void SourceBspImpl::createMesh(const std::vector<Bsp::Source::Vertex>& vertices, const std::vector<Bsp::Source::Edge>& edges, const std::vector<Int32>& surfEdges)
{
    //calc updated vert array from face / edge data
    std::vector<glm::vec3> positions;
    std::vector<std::vector<Int32>> faceIndices;

    //calc sub mesh for each face - what we want to do is duplicate vertices shared by faces, but index vertices shared by
    //triangles within a face
    for (const auto& face : m_faces)
    {
        std::vector<Int32> indices;
        Int32 baseRoot, rootVert, vertA, vertB;
        std::map<Int32, Int32> cachedLookup;

        for (auto i = 0; i < face.edgeCount; ++i)
        {
            auto index = surfEdges[face.firstEdge + i];
            auto edge = edges[std::abs(index)];
            auto reverse = (index < 0);

            if (i == 0)
            {
                baseRoot = rootVert = edge.vertices[reverse ? 0 : 1];

                //copy vert position
                positions.push_back(toGlm(vertices[rootVert].position));
                cachedLookup.insert(std::make_pair(rootVert, positions.size() - 1));
                rootVert = cachedLookup[rootVert];

                vertB = edge.vertices[reverse ? 1 : 0];
                positions.push_back(toGlm(vertices[vertB].position));
                cachedLookup.insert(std::make_pair(vertB, positions.size() - 1));
            }
            else
            {
                vertA = edge.vertices[reverse ? 0 : 1];
                if (vertA == baseRoot) continue;
                else if (cachedLookup.find(vertA) == cachedLookup.end())
                {
                    positions.push_back(toGlm(vertices[vertA].position));
                    cachedLookup.insert(std::make_pair(vertA, positions.size() - 1));
                }
                vertA = cachedLookup[vertA];

                vertB = edge.vertices[reverse ? 1 : 0];
                if (vertB == baseRoot) continue;
                else if (cachedLookup.find(vertB) == cachedLookup.end())
                {
                    positions.push_back(toGlm(vertices[vertB].position));
                    cachedLookup.insert(std::make_pair(vertB, positions.size() - 1));
                }
                vertB = cachedLookup[vertB];

                indices.push_back(rootVert);
                indices.push_back(vertA);
                indices.push_back(vertB);

                //std::cout << rootVert << ", " << vertA << ", " << vertB << std::endl;
            }
        }
        faceIndices.push_back(indices);
    }

    //for each face calc tex coords and normal
    auto vertexCount = positions.size();
    std::vector<glm::vec3> normals(vertexCount);
    std::vector<glm::vec3> tangents(vertexCount);
    std::vector<glm::vec3> bitangents(vertexCount);
    std::vector<glm::vec2> uv0(vertexCount);
    std::vector<glm::vec2> uv1(vertexCount);

    for (auto i = 0u; i < m_faces.size(); ++i)
    {
        calcTexCoords(positions, uv0, uv1, faceIndices[i], i);
        calcNormals(positions, uv0, normals, tangents, bitangents, /*getFirstVerts(edges, surfEdges, i)*/faceIndices[i]);
    }

    //normalise summed vectors
    for (auto& n : normals) n = glm::normalize(n);
    for (auto& t : tangents) t = glm::normalize(t);
    for (auto& b : bitangents) b = glm::normalize(b);

    std::vector<float> vertexData;
    for (auto i = 0u; i < vertexCount; ++i)
    {
        vertexData.push_back(positions[i].x);
        vertexData.push_back(positions[i].z); //swap space
        vertexData.push_back(-positions[i].y);
        vertexData.push_back(uv0[i].x);
        vertexData.push_back(1.f - uv0[i].y);
        vertexData.push_back(uv1[i].x);
        vertexData.push_back(uv1[i].y);
        vertexData.push_back(normals[i].x);
        vertexData.push_back(normals[i].z);
        vertexData.push_back(-normals[i].y);
        vertexData.push_back(tangents[i].x);
        vertexData.push_back(tangents[i].z);
        vertexData.push_back(-tangents[i].y);
        vertexData.push_back(bitangents[i].x);
        vertexData.push_back(bitangents[i].z);
        vertexData.push_back(-bitangents[i].y);
    }

    //create mesh / VBO
    std::vector<VertexLayout::Element> elements =
    {
        VertexLayout::Element(VertexLayout::Type::POSITION, 3u),
        VertexLayout::Element(VertexLayout::Type::UV0, 2u),
        VertexLayout::Element(VertexLayout::Type::UV1, 2u),
        VertexLayout::Element(VertexLayout::Type::NORMAL, 3u),
        VertexLayout::Element(VertexLayout::Type::TANGENT, 3u),
        VertexLayout::Element(VertexLayout::Type::BITANGENT, 3u)
    };

    auto mesh = Mesh::createMesh(VertexLayout(elements), vertexCount);
    mesh->setVertexData(vertexData.data(), vertexCount);

    //add submeshes
    for (const auto& indices : faceIndices)
    {
        auto& subMesh = mesh->addSubMesh(Mesh::PrimitiveType::Triangles, Mesh::IndexFormat::I32, indices.size());
        subMesh.setIndexData(indices.data(), 0u, indices.size());
    }

    //create vert attrib bindings for mesh/materials
    auto& materials = getMaterials();
    for (auto& m : materials)
    {
        auto tCount = m.first->getTechniqueCount();
        for (auto i = 0u; i < tCount; ++i)
        {
            auto t = m.first->getTechnique(i);
            assert(t);

            auto pCount = t->getPassCount();
            for (auto j = 0u; j < pCount; ++j)
            {
                auto p = t->getPass(j);
                assert(p);

                auto vb = VertexAttribBinding::create(mesh, p->getShader());
                p->setVertexAttribBinding(vb);
            }
        }
    }

    setMesh(mesh);
}

bool SourceBspImpl::clusterVisible(Int32 current, Int32 test)
{
    if (current < 0 || test < 0) return true;
    return ((m_clusterBitsets[current][test >> 3] & (1 << (test & 7))) != 0);
}

void SourceBspImpl::decompressPVS(UInt32 clusterCount, const std::vector<std::array<UInt32, 2>>& offsets, const char* lumpStart)
{
    m_clusterBitsets.resize(clusterCount);
    for (auto i = 0u; i < clusterCount; ++i)
    {
        m_clusterBitsets[i].resize(maxLeaves >> 3);
        auto in = lumpStart + offsets[i][0];
        auto out = m_clusterBitsets[i].data();

        do
        {
            if (*in) //we have data so copy
            {
                *out++ = *in++;
                continue;
            }

            //we hit a 0 so next byte tells us how many 0's to decompress
            UInt8 c = in[1];
            in += 2; //skip the count byte
            // add c amount of 0's
            while (c--)
            {
                *out++ = 0;
            }
        } while (out - m_clusterBitsets[i].data() < (clusterCount + 7) >> 3);
    }
}

Int32 SourceBspImpl::findLeaf(const glm::vec3& position)
{
    Int32 i = 0;
    float distance = 0.f;

    //walk the bsp to find the index of the leaf which contains our position
    while (i >= 0)
    {
        const auto& node = m_nodes[i];
        const auto& plane = m_planes[node.planeIndex];

        //check which side of the plane the position is on so we know which direction to go
        distance = plane.normal.x * position.x + plane.normal.y * position.y + plane.normal.z * position.z - plane.distance;
        i = (distance >= 0) ? node.children[0] : node.children[1];
    }
    return ~i;
}

void SourceBspImpl::transformLeafBoundingBoxes()
{
    assert(getNode());
    for (auto& p : m_boundingBoxes)
    {
        BoundingBox b(p.first);
        b.transform(getNode()->getWorldMatrix());
        p.second = b;
    }
}

void SourceBspImpl::drawFace(UInt32 index, UInt32 passFlags)
{
    //check face material flags
    if (m_textureInfo[m_faces[index].textureInfo].mipFlags & (Source::TexFlags::SurfNoDraw | Source::TexFlags::SurfTrigger)) return; //hacky stop nodraws

    auto& material = getMaterials()[m_textureData[m_textureInfo[m_faces[index].textureInfo].textureData].nameStringIndex];

    if (!(passFlags & RenderPass::AlphaBlend) && material.first->hasFlag(RenderPass::AlphaBlend))
    {
        //store face id for next pass
        addTransparentFace(index);
        return;
    }

    UInt16 lightmapPage = index / lightmapsPerPage;
    material.second[PropertyIndex::LightMap]->setSampler(getLightmaps()[lightmapPage]);
    material.second[PropertyIndex::RNMap]->setSampler(m_radiosityNormalMaps[lightmapPage]);

    //TODO if final pass send parameter to lookup MVP and set it in material (-1 is no MVP)

    auto t = material.first->getTechnique();
    assert(t);

    const auto& subMesh = getMesh()->getSubMesh(index);
    auto passCount = t->getPassCount();
    for (auto i = 0u; i < passCount; ++i)
    {
        auto p = t->getPass(i);
        assert(p);

        if (p->flags() == passFlags)
        {
            p->bind();
            glCheck(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, subMesh.getIndexBuffer()));
            glCheck(glDrawElements(static_cast<GLenum>(subMesh.getPrimitiveType()), subMesh.getIndexCount(), static_cast<GLenum>(subMesh.getIndexFormat()), 0));
            p->unbind();
        }
    }
}

void SourceBspImpl::storeAtlases(const std::vector<Image::Ptr>& atlases)
{
    //for (const auto& a : atlases) a->save(a->getUid() + ".png");

    auto& lightmaps = getLightmaps();

    lightmaps.emplace_back(Texture::Sampler::create(Texture::create(std::move(atlases[0]), false)));

    auto rnm = Image::create(maxLightmapWidth, maxLightmapHeight * 3, Image::Format::RGB);
    rnm->setSubrect({}, *atlases[1]);
    rnm->setSubrect({ 0, maxLightmapHeight }, *atlases[2]);
    rnm->setSubrect({ 0, maxLightmapHeight * 2 }, *atlases[3]);
    //rnm->save(rnm->getUid() + ".png");

    m_radiosityNormalMaps.emplace_back(Texture::Sampler::create(Texture::create(std::move(rnm), false)));
}
