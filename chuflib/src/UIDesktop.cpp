/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/ui/Desktop.hpp>
#include <chuf2/Sprite.hpp>
#include <chuf2/Texture.hpp>
#include <chuf2/App.hpp>

#include <cassert>

using namespace chuf;
using namespace chuf::ui;

namespace
{
    
}


Desktop::Desktop()
    : m_view(App::getInstance().getView())
{

}

//public
Sprite::Ptr Desktop::getSprite(Texture* texture, const FloatRect& subrect)
{  
    Sprite::Ptr sprite;
    if (m_spriteBatches.find(texture->getHandle()) != m_spriteBatches.end())
    {
        sprite = m_spriteBatches[texture->getHandle()]->addSprite();
    }
    else
    {
        auto spriteBatch = chuf::SpriteBatch::create(texture);

        //set spritebatch matrix for desktop view
        auto newMat = glm::ortho<float>(m_view.viewport.left, m_view.viewport.left + m_view.viewport.width,
                                        m_view.viewport.top, m_view.viewport.top + m_view.viewport.height);
        spriteBatch->setProjectionMatrix(newMat);

        sprite = spriteBatch->addSprite();
        m_spriteBatches.insert(std::make_pair(texture->getHandle(), std::move(spriteBatch)));
    }
    if (subrect.width != 0 && subrect.height != 0) sprite->setSubRect(subrect);   
    return sprite;
}

Text* Desktop::addText(Font* font)
{
    m_texts.emplace_back(std::make_unique<Text>(font));
    return m_texts.back().get();
}

void Desktop::removeText(Text* text)
{
    m_texts.erase(std::remove_if(m_texts.begin(), m_texts.end(),
        [text](const Text::Ptr& p)
    {
        return p.get() == text;
    }), m_texts.end());
}

void Desktop::addContainer(chuf::ui::Container& c)
{
    //assert(c);
    m_containers.push_back(&c);
}

glm::vec2 Desktop::getMousePosition() const
{
    return m_view.mapPixelToView(App::getInstance().getMousePosition());
}

void Desktop::setView(const View& v)
{
    m_view = v;

    auto newMat = glm::ortho<float>(v.viewport.left, v.viewport.left + v.viewport.width, v.viewport.top, v.viewport.top + v.viewport.height);
    for (auto& sb : m_spriteBatches) sb.second->setProjectionMatrix(newMat);
}

void Desktop::draw()
{
    for (const auto& sb : m_spriteBatches)
    {
        sb.second->draw();
    }
    for (const auto& t : m_texts)
    {
        t->draw();
    }
}

void Desktop::handleKeyEvent(const chuf::Event::KeyEvent& evt)
{
    for (auto& c : m_containers)
    {
        c->handleKeyEvent(evt);
    }
}

void Desktop::handleMouseEvent(const chuf::Event::MouseEvent& evt)
{
    auto pos = getMousePosition();
    for (auto& c : m_containers)
    {
        c->handleMouseEvent(evt, pos);
    }
}