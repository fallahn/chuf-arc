/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/ui/Button.hpp>
#include <chuf2/ui/Desktop.hpp>
#include <chuf2/Sprite.hpp>
#include <chuf2/SpriteBatch.hpp>
#include <chuf2/Texture.hpp>
#include <chuf2/Font.hpp>

using namespace chuf;
using namespace chuf::ui;

namespace
{
    chuf::Colour normalColour = chuf::Colour::white;
    chuf::Colour selectedColour = chuf::Colour(0.6f, 0.6f, 0.6f);
    chuf::Colour activeColour = chuf::Colour::blue;
}

Button::Button(Desktop& d)
    : Control   (d),
    m_text      (nullptr),
    m_togglable (false)
{}

Button::~Button()
{
    if (m_sprite)
    {
        m_sprite->setTransform(nullptr);
    }
}

//public
bool Button::selectable() const
{
    return true;
}

void Button::select()
{
    Control::select();
    for (auto& cb : m_callbacks[Event::Selected])
    {
        cb(this);
    }

    if (!active())
    {
        //TODO use subrects to update sprite texture
        if (m_sprite)
        {
            m_sprite->setColour(selectedColour);
        }
    }
}

void Button::deselect()
{
    Control::deselect();    
    for (auto& cb : m_callbacks[Event::Deselected])
    {
        cb(this);
    }

    if (!active())
    {

        if (m_sprite)
        {
            m_sprite->setColour(normalColour);
        }
    }
}

void Button::activate()
{
    if (!active())
    {
        Control::activate();
        if (m_sprite)
        {
            m_sprite->setColour(activeColour);
        }
        for (const auto & cb : m_callbacks[Event::Activated])
        {
            cb(this/*, Event::ButtonPressed*/);
        }

        if (!m_togglable)
        {
            deactivate(); //TODO this should be done on mouse up / mouse exit event
        }
    }
    else
    {
        deactivate();
    }
}

void Button::deactivate()
{
    Control::deactivate();
    if (m_sprite)
    {
        if (selected())
        {
            m_sprite->setColour(selectedColour);
        }
        else
        {
            m_sprite->setColour(normalColour);
        }
    }
    for (auto& cb : m_callbacks[Event::Deactivated])
    {
        cb(this);
    }
}

void Button::handleKeyEvent(const chuf::Event::KeyEvent& evt)
{

}

void Button::handleMouseEvent(const chuf::Event::MouseEvent& evt, const glm::vec2& mousePos)
{
    //TODO raise enter / exit events
}

void Button::setAlignment(Alignment){}

bool Button::contains(const glm::vec2& mousePos) const
{
    return (m_sprite) ? m_sprite->getGlobalBounds().contains(mousePos) : false;
}

void Button::addCallback(Callback c, Event e)
{
    m_callbacks[e].push_back(c);
}

void Button::setText(const std::string& str, Font* font)
{
    if (!font)
    {
        Logger::Log("missing font when updating button", Logger::Type::Warning);
        return;
    }

    if (m_text)
    {
        if (font == m_text->getFont())
        {
            m_text->setString(str);
        }
        else
        {
            getDesktop().removeText(m_text);
            m_text = getDesktop().addText(font);
            m_text->setString(str);
            m_text->setColour(Colour::white);
            //TODO pop into function of its own
            //TODO some sort of padding / positioning
            auto pos = getTranslation();
            m_text->setPosition({ pos.x + 15.f, pos.y + 3.f });
        }
    }
    else
    {
        m_text = getDesktop().addText(font);
        m_text->setString(str);
        m_text->setColour(Colour::white);
        //TODO some sort of padding / positioning
        auto pos = getTranslation();
        m_text->setPosition({ pos.x + 15.f, pos.y + 3.f });
    }
}

void Button::setTexture(Texture* tex, const FloatRect& subrect)
{
    if (m_sprite)
    {
        m_sprite->setTransform(nullptr);
    }
    m_sprite = getDesktop().getSprite(tex, subrect);
    if (m_sprite)
    {
        m_sprite->setTransform(this);
    }
}

void Button::setTogglable(bool b)
{
    m_togglable = b;
}