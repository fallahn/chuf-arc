/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/Ray.hpp>
#include <chuf2/Plane.hpp>
#include <chuf2/Frustum.hpp>
#include <chuf2/BoundingBox.hpp>
#include <chuf2/BoundingSphere.hpp>

#include <assert.h>

using namespace chuf;

namespace
{
    const float NoIntersect = -1.f;
    const float epsilon = 0.0001f;
}

//ctor
Ray::Ray()
    : m_origin  (),
    m_direction (0.f, 0.f, 1.f)
{
    normalise();
}

Ray::Ray(const glm::vec3& origin, const glm::vec3& direction)
    : m_origin  (origin),
    m_direction (direction)
{
    normalise();
}

Ray::Ray(const Ray& other)
{
    m_direction = other.m_direction;
    m_origin = other.m_origin;
    normalise();
}

//operators
Ray& Ray::operator = (const Ray& other)
{
    if (&other != this)
    {
        m_direction = other.m_direction;
        m_origin = other.m_origin;
        normalise();
    }
    return *this;
}

Ray& Ray::operator *= (const glm::mat4& tform)
{
    transform(tform);
    return *this;
}

const Ray operator * (const glm::mat4& tform, const Ray& ray)
{
    Ray r(ray);
    r.transform(tform);
    return r;
}

//public
const glm::vec3& Ray::getOrigin() const
{
    return m_origin;
}

void Ray::setOrigin(const glm::vec3& o)
{
    m_origin = o;
}


const glm::vec3& Ray::getDirection() const
{
    return m_direction;
}

void Ray::setDirection(const glm::vec3& dir)
{
    m_direction = dir;
    normalise();
}

float Ray::intersects(const BoundingBox& box) const
{
    return box.intersects(*this);
}

float Ray::intersects(const BoundingSphere& sphere) const
{
    return sphere.intersects(*this);
}

float Ray::intersects(const Plane& plane) const
{
    const auto& normal = plane.getNormal();

    //if origin is on plane the distance is 0
    const float alpha = glm::dot(normal, m_origin) + plane.getDistance();
    if (std::abs(alpha) < epsilon)
    {
        return 0.f;
    }

    //check if direction is parallel to plane
    const float dot = glm::dot(normal, m_direction);
    if (dot == 0) return NoIntersect;
    
    //check if plane is behind ray (distance will be negative
    const float dist = -alpha / dot;
    if (dist < 0) return NoIntersect;

    return dist;
}

float Ray::intersects(const Frustum& frustum) const
{
    auto near = frustum.getNear();
    float nearDist = intersects(near);
    float nearOriginDist = near.distance(m_origin);
    
    auto far = frustum.getFar();
    float farDist = intersects(far);
    float farOriginDist = far.distance(m_origin);

    auto left = frustum.getLeft();
    float leftDist = intersects(left);
    float leftOriginDist = left.distance(m_origin);

    auto right = frustum.getRight();
    float rightDist = intersects(right);
    float rightOriginDist = right.distance(m_origin);

    auto top = frustum.getTop();
    float topDist = intersects(top);
    float topOriginDist = top.distance(m_origin);

    auto bottom = frustum.getBottom();
    auto bottomDist = intersects(bottom);
    float bottomOriginDist = bottom.distance(m_origin);

    //if the distance is negative to a plane and doesn't intersect
    //then the ray must be outside, facing away from frustum
    if ((nearDist < 0 && nearOriginDist < 0) ||
        (farDist < 0 && farOriginDist < 0) ||
        (leftDist < 0 && leftOriginDist < 0) ||
        (rightDist < 0 && rightOriginDist < 0) ||
        (topDist < 0 && topOriginDist < 0) ||
        (bottomDist < 0 && bottomOriginDist < 0))
    {
        return NoIntersect;
    }

    //else value is minimum positive distance
    float distance = (nearDist > 0.0f) ? nearDist : 0.0f;
    distance = (farDist > 0.0f) ? ((distance == 0.0f) ? farDist : std::min(farDist, distance)) : distance;
    distance = (leftDist > 0.0f) ? ((distance == 0.0f) ? leftDist : std::min(leftDist, distance)) : distance;
    distance = (rightDist > 0.0f) ? ((distance == 0.0f) ? rightDist : std::min(rightDist, distance)) : distance;
    distance = (topDist > 0.0f) ? ((distance == 0.0f) ? bottomDist : std::min(bottomDist, distance)) : distance;
    distance = (bottomDist > 0.0f) ? ((distance == 0.0f) ? topDist : std::min(topDist, distance)) : distance;

    return distance;
}

void Ray::transform(const glm::mat4& tform)
{
    glm::vec4 temp(m_origin.x, m_origin.y, m_origin.z, 1.f);
    temp = tform * temp;
    m_origin = { temp.x, temp.y, temp.z };

    temp = { m_direction.x, m_direction.y, m_direction.z, 0.f };
    temp = tform * temp;
    m_direction = glm::normalize(glm::vec3(temp.x, temp.y, temp.z));
}

//private
void Ray::normalise()
{
    assert(glm::length2(m_direction) != 0);

    m_direction = glm::normalize(m_direction);
}