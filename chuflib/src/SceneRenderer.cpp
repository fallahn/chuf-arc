/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/SceneRenderer.hpp>
#include <chuf2/Scene.hpp>
#include <chuf2/FrameBuffer.hpp>
#include <chuf2/App.hpp>
#include <chuf2/Drawable.hpp>
#include <chuf2/Reports.hpp>

#include <algorithm>

using namespace chuf;
using namespace std::placeholders;

std::map<RenderPass, SceneRenderer::ScenePass> SceneRenderer::passFunctions;

SceneRenderer::SceneRenderer(Scene* scene)
    : m_scene(scene)
{
    passFunctions.insert(std::make_pair(RenderPass::Reflection, std::bind(&SceneRenderer::reflectionPass, this, _1, _2)));
    passFunctions.insert(std::make_pair(RenderPass::Refraction, std::bind(&SceneRenderer::refractionPass, this, _1, _2)));
    passFunctions.insert(std::make_pair(RenderPass::Debug, std::bind(&SceneRenderer::debugPass, this, _1, _2)));
    //passFunctions.insert(std::make_pair(RenderPass::Wireframe, std::bind(&SceneRenderer::reflectionPass, this, _1, _2)));
    
    m_scenePasses.push_back(std::bind(&SceneRenderer::standardPass, this, _1, _2));
}

//public
void SceneRenderer::addDrawable(const Drawable* d)
{
    m_drawables.push_back(std::make_pair(d, false));
}

void SceneRenderer::removeDrawable(const Drawable* d)
{
    m_drawables.erase(std::remove_if(m_drawables.begin(), m_drawables.end(), 
        [d](const std::pair<const Drawable*, bool>& p)
    {
        return p.first == d;
    }), m_drawables.end());
}

void SceneRenderer::buildRenderPath(std::vector<RenderPass>& passes)
{
    //make sure we don't try including alpha passes (they are already part of the functions
    passes.erase(std::remove_if(passes.begin(), passes.end(), 
        [](RenderPass rp)
    {
        return ((rp & RenderPass::AlphaBlend) != 0);
    }), passes.end());

    m_scenePasses.clear();
    m_scenePasses.resize(passes.size());
    for (auto i = 0u; i < passes.size(); ++i)
    {
        if (passes[i] == RenderPass::Standard)
        {
            if (passes.size() > 1)
            {
                //use frustum test caching from previous passes
                m_scenePasses[i] = std::bind(&SceneRenderer::standardPassCached, this, _1, _2);
            }
            else
            {
                m_scenePasses[i] = std::bind(&SceneRenderer::standardPass, this, _1, _2);
            }
        }
        else
        {
            m_scenePasses[i] = passFunctions[passes[i]];
        }
    }
}

void SceneRenderer::draw()
{
    //TODO replace m_drawables with results from querying BVH and pass to functions
    //TODO create a new test scene (or maybe use terrain) with large model numbers
    auto& app = App::getInstance();
    auto camera = m_scene->getActiveCamera();

    for (const auto& pass : m_scenePasses)
    {
        pass(camera, app);
    }
}
//private
void SceneRenderer::reflectionPass(Camera* camera, App& app)
{
    const auto& reflectMat = camera->getReflectionMatrix();
    const auto& reflectionFrustum = camera->getReflectionViewFrustum();

    auto ob = m_scene->getFrameBuffer(Scene::FBO::Reflection)->bind();
    auto ov = app.setView(m_scene->getFrameBuffer(Scene::FBO::Reflection)->getView());
    m_scene->getFrameBuffer(Scene::FBO::Reflection)->clear(m_clearColour);

    for (auto& d : m_drawables)
    {
        auto bb = d.first->getBoundingBox();
        bb.transform(reflectMat);
        if (d.second = bb.intersects(reflectionFrustum))
        {
            d.first->draw(RenderPass::Reflection);
        }
    }

    //alpha blended pass on FBO
    for (const auto& d : m_drawables)
    {
        if (d.second)
        {
            d.first->draw(RenderPass::Reflection | RenderPass::AlphaBlend);
        }
    }
    ob->bind();
    app.setView(ov);
}

void SceneRenderer::refractionPass(Camera* camera, App& app)
{
    const auto& frustum(camera->getViewFrustum());
    auto ob = m_scene->getFrameBuffer(Scene::FBO::Refraction)->bind();
    auto ov = app.setView(m_scene->getFrameBuffer(Scene::FBO::Refraction)->getView());
    m_scene->getFrameBuffer(Scene::FBO::Refraction)->clear(m_clearColour);
    camera->invertReflectionPlane();

    //opaque pass
    for (auto& d : m_drawables)
    {
        const auto& bb = d.first->getBoundingBox();
        if (d.second = frustum.intersects(bb))
        {
            d.first->draw(RenderPass::Refraction);
        }
    }

    //transparent pass
    for (const auto& d : m_drawables)
    {
        //uses cached frustum result
        if (d.second)
        {
            d.first->draw(RenderPass::Refraction | RenderPass::AlphaBlend);
        }
    }
    camera->invertReflectionPlane(); //flip this back

    ob->bind();
    app.setView(ov);
}

void SceneRenderer::standardPass(Camera* camera, App& app)
{
    //this relies on the App instance already having cleared the buffer before drawing
    //and that previous passes have restored the main buffer
    //opaque pass
    const auto& frustum(camera->getViewFrustum());
    UInt32 count = 0u;
    for (auto& d : m_drawables)
    {
        const auto& bb = d.first->getBoundingBox();
        if (d.second = bb.intersects(frustum))
        {
            count += d.first->draw(RenderPass::Standard/*, true*/);
        }
    }
    REPORT("Opaque pass drawn", std::to_string(count));

    //transparent pass
    count = 0u;
    for (const auto& d : m_drawables)
    {
        if (d.second)
        {
            count += d.first->draw(RenderPass::Standard | RenderPass::AlphaBlend);
        }
    }
    REPORT("Transparent pass drawn", std::to_string(count));
}

void SceneRenderer::standardPassCached(Camera* camera, App& app)
{
    //opaque pass
    UInt32 count = 0u;
    for (const auto& d : m_drawables)
    {
        if (d.second)
        {
            count += d.first->draw(RenderPass::Standard);
        }
    }
    REPORT("Opaque pass drawn", std::to_string(count));

    //transparent pass
    count = 0u;
    for (const auto& d : m_drawables)
    {
        if (d.second)
        {
            count += d.first->draw(RenderPass::Standard | RenderPass::AlphaBlend);
        }
    }
    REPORT("Transparent pass drawn", std::to_string(count));
}

void SceneRenderer::debugPass(Camera* camera, App& app)
{
    //debug pass
    //for (const auto& d : m_drawables)
    //{
    //    d->draw(RenderPass::Debug);
    //}
}