/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/ui/Container.hpp>
#include <chuf2/ui/Desktop.hpp>

#include <assert.h>

using namespace chuf::ui;

Container::Container(Desktop& d)
    : Control       (d),
    m_selectedIndex (0)
{
    
}

//public
void Container::addControl(Control& c)
{
    //assert(c);
    m_controls.push_back(&c);

    if (!hasSelection() && c.selectable())
    {
        select(m_controls.size() - 1);
    }
}

bool Container::selectable() const
{
    return false;
}

void Container::update(float dt)
{
    for (auto& c : m_controls) c->update(dt);
}

void Container::handleKeyEvent(const chuf::Event::KeyEvent& evt)
{
    if (hasSelection() && m_controls[m_selectedIndex]->active())
    {
        m_controls[m_selectedIndex]->handleKeyEvent(evt);
    }
    else if (evt.key.type == chuf::Event::KEY_RELEASED)
    {
        if (evt.key.keysym.sym == SDLK_UP
            || evt.key.keysym.sym == SDLK_LEFT)
        {
            selectPrevious();
        }
        else if (evt.key.keysym.sym == SDLK_DOWN
            || evt.key.keysym.sym == SDLK_RIGHT)
        {
            selectNext();
        }
        else if (evt.key.keysym.sym == SDLK_RETURN)
        {
            if (hasSelection())
            {
                m_controls[m_selectedIndex]->activate();
            }
        }
    }
}

void Container::handleMouseEvent(const chuf::Event::MouseEvent& evt, const glm::vec2& mousePos)
{
    if (evt.type == chuf::Event::MOUSE_MOVE)
    {
        for (auto i = 0u; i < m_controls.size(); ++i)
        {
            if (m_controls[i]->contains(mousePos))
            {
                m_controls[i]->select();
                m_selectedIndex = i;
            }
            else
            {
                m_controls[i]->deselect();
            }
        }
    }
    else if (evt.type == chuf::Event::MOUSE_BUTTON_PRESSED
        && evt.button.button == SDL_BUTTON_LEFT)
    {
        if (hasSelection() &&
            m_controls[m_selectedIndex]->contains(mousePos))
        {
            m_controls[m_selectedIndex]->activate();
        }
    }
}

void Container::setAlignment(Alignment)
{

}

//private
bool Container::hasSelection() const
{
    return m_selectedIndex >= 0; //uhhh - this is unsigned so always true? :S
}

void Container::select(chuf::UInt16 index)
{
    if (m_controls[index]->selectable())
    {
        if (hasSelection())
            m_controls[m_selectedIndex]->deselect();

        m_controls[index]->select();
        m_selectedIndex = index;
    }
}

void Container::selectNext()
{
    if (!hasSelection()) return;

    auto next = m_selectedIndex;

    do
    {
        next = (next + 1) % m_controls.size();
    } while (!m_controls[next]->selectable());

    select(next);
}

void Container::selectPrevious()
{
    if (!hasSelection()) return;

    auto prev = m_selectedIndex;

    do
    {
        prev = (prev + m_controls.size() - 1) % m_controls.size();
    } while (!m_controls[prev]->selectable());

    select(prev);
}
