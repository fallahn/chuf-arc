/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/Mesh.hpp>
#include <chuf2/SubMesh.hpp>
#include <chuf2/Skin.hpp>
#include <chuf2/Util.hpp>

#include <array>
#include <cassert>

using namespace chuf;

Mesh::Ptr Mesh::createMesh(const VertexLayout& v, UInt32 count, bool dynamic)
{
    GLuint vbo;
    glCheck(glGenBuffers(1, &vbo));
    glCheck(glBindBuffer(GL_ARRAY_BUFFER, vbo));
    glCheck(glBufferData(GL_ARRAY_BUFFER, v.getVertexSize() * count, NULL, dynamic ? GL_DYNAMIC_DRAW : GL_STATIC_DRAW));

    auto mesh = std::make_shared<Mesh>(v, Key());
    mesh->m_dynamic = dynamic;
    mesh->m_vertBuffer = vbo;
    mesh->m_vertCount = count;

    return mesh;
}

Mesh::Ptr Mesh::createQuad(const glm::vec3& p1, const glm::vec3& p2, const glm::vec3& p3, const glm::vec3& p4)
{
    //assuming the UVs are aligned with the quad (which they are)
    //tan and bitan will run along the edges
    glm::vec3 t = glm::normalize(p1 - p2);
    glm::vec3 b = glm::normalize(p4 - p2);
    glm::vec3 n = glm::cross(t, b);
    n = -glm::normalize(n);

    std::array<float, 56u> vertArray =
    {
        p1.x, p1.y, p1.z, n.x, n.y, n.z, t.x, t.y, t.z, b.x, b.y, b.z, 0.f, 1.f,
        p2.x, p2.y, p2.z, n.x, n.y, n.z, t.x, t.y, t.z, b.x, b.y, b.z, 0.f, 0.f,
        p3.x, p3.y, p3.z, n.x, n.y, n.z, t.x, t.y, t.z, b.x, b.y, b.z, 1.f, 1.f,
        p4.x, p4.y, p4.z, n.x, n.y, n.z, t.x, t.y, t.z, b.x, b.y, b.z, 1.f, 0.f
    };

    std::vector<VertexLayout::Element> elements =
    {
        VertexLayout::Element(VertexLayout::Type::POSITION, 3u),
        VertexLayout::Element(VertexLayout::Type::NORMAL, 3u),
        VertexLayout::Element(VertexLayout::Type::TANGENT, 3u),
        VertexLayout::Element(VertexLayout::Type::BITANGENT, 3u),
        VertexLayout::Element(VertexLayout::Type::UV0, 2u)
    };

    auto mesh = createMesh(VertexLayout(elements), 4u, false);
    mesh->m_primitiveType = PrimitiveType::TriangleStrip;
    mesh->setVertexData(&vertArray[0], 4u);
    mesh->setBoundingBox({ p1, p4 });
    return mesh;
}

Mesh::Ptr Mesh::createLines(const std::vector<glm::vec3>& points)
{
    assert(points.size());

    std::unique_ptr<float[]> pointData(new float[points.size() * 3u]);
    UInt32 j = 0;
    for (auto& p : points)
    {
        pointData[j++] = p.x;
        pointData[j++] = p.y;
        pointData[j++] = p.z;
    }

    std::vector<VertexLayout::Element> elements =
    {
        VertexLayout::Element(VertexLayout::Type::POSITION, 3u)
    };

    auto mesh = createMesh(VertexLayout(elements), points.size(), false);
    mesh->m_primitiveType = PrimitiveType::LineStrip;
    mesh->setVertexData(&pointData[0], points.size());

    return mesh;
}

Mesh::Ptr Mesh::createCube()
{
    std::vector<VertexLayout::Element> elements =
    {
        VertexLayout::Element(VertexLayout::Type::POSITION, 3u),
        VertexLayout::Element(VertexLayout::Type::NORMAL, 3u),
        VertexLayout::Element(VertexLayout::Type::TANGENT, 3u),
        VertexLayout::Element(VertexLayout::Type::BITANGENT, 3u),
        VertexLayout::Element(VertexLayout::Type::UV0, 2u)
    };

    const float halfWidth = 0.5f;
    std::array<float, 360u> vertData =
    {
        //front
        -halfWidth, -halfWidth, halfWidth,
        0.f, 0.f, 1.f, //normal
        0.f, 1.f, 0.f, //tan
        1.f, 0.f, 0.f, //bitan
        0.f, 0.f,      //uv
        halfWidth, -halfWidth, halfWidth,
        0.f, 0.f, 1.f,
        0.f, 1.f, 0.f,
        1.f, 0.f, 0.f,
        1.f, 0.f,
        halfWidth, halfWidth, halfWidth,
        0.f, 0.f, 1.f,
        0.f, 1.f, 0.f,
        1.f, 0.f, 0.f,
        1.f, 1.f,
        -halfWidth, halfWidth, halfWidth,
        0.f, 0.f, 1.f,
        0.f, 1.f, 0.f,
        1.f, 0.f, 0.f,
        0.f, 1.f,
        //back
        -halfWidth, -halfWidth, -halfWidth,
        0.f, 0.f, -1.f,
        0.f, 1.f, 0.f,
        -1.f, 0.f, 0.f,
        1.f, 0.f,
        -halfWidth, halfWidth, -halfWidth,
        0.f, 0.f, -1.f,
        0.f, 1.f, 0.f,
        -1.f, 0.f, 0.f,
        1.f, 1.f,
        halfWidth, halfWidth, -halfWidth,
        0.f, 0.f, -1.f,
        0.f, 1.f, 0.f,
        -1.f, 0.f, 0.f,
        0.f, 1.f,
        halfWidth, -halfWidth, -halfWidth,
        0.f, 0.f, -1.f,
        0.f, 1.f, 0.f,
        -1.f, 0.f, 0.f,
        0.f, 0.f,
        //top
        -halfWidth, halfWidth, -halfWidth,
        0.f, 1.f, 0.f,
        0.f, 0.f, -1.f,
        1.f, 0.f, 0.f,
        0.f, 1.f,
        -halfWidth, halfWidth, halfWidth,
        0.f, 1.f, 0.f,
        0.f, 0.f, -1.f,
        1.f, 0.f, 0.f,
        0.f, 0.f,
        halfWidth, halfWidth, halfWidth,
        0.f, 1.f, 0.f,
        0.f, 0.f, -1.f,
        1.f, 0.f, 0.f,
        1.f, 0.f,
        halfWidth, halfWidth, -halfWidth,
        0.f, 1.f, 0.f,
        0.f, 0.f, -1.f,
        1.f, 0.f, 0.f,
        1.f, 1.f,
        //bottom
        -halfWidth, -halfWidth, -halfWidth,
        0.f, -1.f, 0.f,
        0.f, 0.f, 1.f,
        1.f, 0.f, 0.f,
        1.f, 1.f,
        halfWidth, -halfWidth, -halfWidth,
        0.f, -1.f, 0.f,
        0.f, 0.f, 1.f,
        1.f, 0.f, 0.f,
        0.f, 1.f,
        halfWidth, -halfWidth, halfWidth,
        0.f, -1.f, 0.f,
        0.f, 0.f, 1.f,
        1.f, 0.f, 0.f,
        0.f, 0.f,
        -halfWidth, -halfWidth, halfWidth,
        0.f, -1.f, 0.f,
        0.f, 0.f, 1.f,
        1.f, 0.f, 0.f,
        1.f, 0.f,
        //right
        halfWidth, -halfWidth, -halfWidth,
        1.f, 0.f, 0.f,
        0.f, 1.f, 0.f,
        0.f, 0.f, -1.f,
        1.f, 0.f,
        halfWidth, halfWidth, -halfWidth,
        1.f, 0.f, 0.f,
        0.f, 1.f, 0.f,
        0.f, 0.f, -1.f,
        1.f, 1.f,
        halfWidth, halfWidth, halfWidth,
        1.f, 0.f, 0.f,
        0.f, 1.f, 0.f,
        0.f, 0.f, -1.f,
        0.f, 1.f,
        halfWidth, -halfWidth, halfWidth,
        1.f, 0.f, 0.f,
        0.f, 1.f, 0.f,
        0.f, 0.f, -1.f,
        0.f, 0.f,
        //left
        -halfWidth, -halfWidth, -halfWidth,
        -1.f, 0.f, 0.f,
        0.f, 1.f, 0.f,
        0.f, 0.f, 1.f,
        0.f, 0.f,
        -halfWidth, -halfWidth, halfWidth,
        -1.f, 0.f, 0.f,
        0.f, 1.f, 0.f,
        0.f, 0.f, 1.f,
        1.f, 0.f,
        -halfWidth, halfWidth, halfWidth,
        -1.f, 0.f, 0.f,
        0.f, 1.f, 0.f,
        0.f, 0.f, 1.f,
        1.f, 1.f,
        -halfWidth, halfWidth, -halfWidth,
        -1.f, 0.f, 0.f,
        0.f, 1.f, 0.f,
        0.f, 0.f, 1.f,
        0.f, 1.f
    };

    auto mesh = createMesh(VertexLayout(elements), 24u);
    mesh->setVertexData(vertData.data(), 24u);
    mesh->setBoundingBox({ { -0.5f, -0.5f, -0.5f }, { 0.5f, 0.5f, 0.5f } });
    mesh->setBoundingSphere({ {}, { halfWidth } });

    std::array<UInt16, 36u> indices =
    {
        0u, 1u, 2u, 0u, 2u, 3u,
        4u, 5u, 6u, 4u, 6u, 7u,
        8u, 9u, 10u, 8u, 10u, 11u,
        12u, 13u, 14u, 12u, 14u, 15u,
        16u, 17u, 18u, 16u, 18u, 19u,
        20u, 21u, 22u, 20u, 22u, 23u
    };


    auto& part = mesh->addSubMesh(Mesh::PrimitiveType::Triangles, Mesh::IndexFormat::I16, indices.size());
    part.setIndexData(&indices[0], 0u, indices.size());

    return mesh;
}

Mesh::Ptr Mesh::createFromBoundingBox(const BoundingBox& bb, const Colour& colour)
{
    //TODO optimise with index array
    const auto corners = bb.getCorners();
    std::vector<float> verts =
    {
        corners[7].x, corners[7].y, corners[7].z,
        colour.r, colour.g, colour.b,
        corners[6].x, corners[6].y, corners[6].z,
        colour.r, colour.g, colour.b,
        corners[1].x, corners[1].y, corners[1].z,
        colour.r, colour.g, colour.b,
        corners[0].x, corners[0].y, corners[0].z,
        colour.r, colour.g, colour.b,
        corners[7].x, corners[7].y, corners[7].z,
        colour.r, colour.g, colour.b,
        corners[4].x, corners[4].y, corners[4].z,
        colour.r, colour.g, colour.b,
        corners[3].x, corners[3].y, corners[3].z,
        colour.r, colour.g, colour.b,
        corners[0].x, corners[0].y, corners[0].z,
        colour.r, colour.g, colour.b,
        corners[0].x, corners[0].y, corners[0].z,
        colour.r, colour.g, colour.b,
        corners[1].x, corners[1].y, corners[1].z,
        colour.r, colour.g, colour.b,
        corners[2].x, corners[2].y, corners[2].z,
        colour.r, colour.g, colour.b,
        corners[3].x, corners[3].y, corners[3].z,
        colour.r, colour.g, colour.b,
        corners[4].x, corners[4].y, corners[4].z,
        colour.r, colour.g, colour.b,
        corners[5].x, corners[5].y, corners[5].z,
        colour.r, colour.g, colour.b,
        corners[2].x, corners[2].y, corners[2].z,
        colour.r, colour.g, colour.b,
        corners[1].x, corners[1].y, corners[1].z,
        colour.r, colour.g, colour.b,
        corners[6].x, corners[6].y, corners[6].z,
        colour.r, colour.g, colour.b,
        corners[5].x, corners[5].y, corners[5].z,
        colour.r, colour.g, colour.b
    };

    std::vector<VertexLayout::Element> elements =
    {
        VertexLayout::Element(VertexLayout::Type::POSITION, 3u),
        VertexLayout::Element(VertexLayout::Type::COLOUR, 3u)
    };

    auto mesh = createMesh(VertexLayout(elements), 18, false);
    mesh->setPrimitveType(PrimitiveType::LineStrip);
    mesh->setVertexData(verts.data(), 18);
    mesh->setBoundingBox(bb);
    BoundingSphere bs;
    bs.set(bb);
    mesh->setBoundingSphere(bs);
    return mesh;
}

Mesh::Ptr Mesh::createFromBoundingSphere(const BoundingSphere& bs, const Colour& colour)
{
    float radius = bs.getRadius();
    UInt8 bandCount = 6u;
    UInt8 stripCount = bandCount;

    std::vector<float> verts;
    for (auto band = 0u; band <= bandCount; ++band)
    {
        float theta = static_cast<float>(band)* PI / bandCount;
        float sinTheta = std::sin(theta);
        float cosTheta = std::cos(theta);

        for (auto strip = 0u; strip <= stripCount; ++strip)
        {
            float phi = static_cast<float>(strip)* 2.f * PI / stripCount;
            float sinPhi = std::sin(phi);
            float cosPhi = std::cos(phi);

            float x = sinPhi * sinTheta;
            float y = cosTheta;
            float z = cosPhi * sinTheta;

            verts.push_back(x * radius);
            verts.push_back(y * radius);
            verts.push_back(z * radius);
            verts.push_back(colour.r);
            verts.push_back(colour.g);
            verts.push_back(colour.b);
        }
    }

    UInt16 vertCount = verts.size() / 6u;
    std::vector<VertexLayout::Element> elements =
    {
        VertexLayout::Element(VertexLayout::Type::POSITION, 3u),
        VertexLayout::Element(VertexLayout::Type::COLOUR, 3u)
    };
    auto mesh = createMesh(VertexLayout(elements), vertCount);
    mesh->setVertexData(&verts[0], vertCount);
    mesh->setBoundingSphere(bs);
    mesh->setBoundingBox({ { -radius, -radius, -radius }, { radius, radius, radius } });

    std::vector<UInt16> indices;
    for (auto band = 0u; band < bandCount; band++)
    {
        for (auto strip = 0u; strip < stripCount; ++strip)
        {
            auto first = (band * (band + 1)) + strip;
            auto second = first + bandCount + 1;
            auto last = bandCount * (band + 1) + band;

            //anti clockwise winding
            indices.push_back(first);
            indices.push_back(second);
            indices.push_back(first + 1u);

            indices.push_back(second);
            indices.push_back(second + 1u);
            indices.push_back(first + 1u);

            //TODO make sure final edge is shared with first
            //TODO fix this
            if (band > 0u && ((first + 1) == last))
            {
                indices.push_back(last);
                indices.push_back(last - stripCount);
            }
        }
    }

    auto& sm = mesh->addSubMesh(Mesh::PrimitiveType::LineStrip, Mesh::IndexFormat::I16, indices.size());
    sm.setIndexData(&indices[0], 0u, indices.size());

    return mesh;
}

Mesh::Ptr Mesh::createFromFrustum(const Frustum& f, const Colour& colour)
{
    //TODO creat sub function as this is basically the same as fromBoundingbox
    const auto corners = f.getCorners();
    std::vector<float> verts =
    {
        corners[7].x, corners[7].y, corners[7].z,
        colour.r, colour.g, colour.b,
        corners[6].x, corners[6].y, corners[6].z,
        colour.r, colour.g, colour.b,
        corners[1].x, corners[1].y, corners[1].z,
        colour.r, colour.g, colour.b,
        corners[0].x, corners[0].y, corners[0].z,
        colour.r, colour.g, colour.b,
        corners[7].x, corners[7].y, corners[7].z,
        colour.r, colour.g, colour.b,
        corners[4].x, corners[4].y, corners[4].z,
        colour.r, colour.g, colour.b,
        corners[3].x, corners[3].y, corners[3].z,
        colour.r, colour.g, colour.b,
        corners[0].x, corners[0].y, corners[0].z,
        colour.r, colour.g, colour.b,
        corners[0].x, corners[0].y, corners[0].z,
        colour.r, colour.g, colour.b,
        corners[1].x, corners[1].y, corners[1].z,
        colour.r, colour.g, colour.b,
        corners[2].x, corners[2].y, corners[2].z,
        colour.r, colour.g, colour.b,
        corners[3].x, corners[3].y, corners[3].z,
        colour.r, colour.g, colour.b,
        corners[4].x, corners[4].y, corners[4].z,
        colour.r, colour.g, colour.b,
        corners[5].x, corners[5].y, corners[5].z,
        colour.r, colour.g, colour.b,
        corners[2].x, corners[2].y, corners[2].z,
        colour.r, colour.g, colour.b,
        corners[1].x, corners[1].y, corners[1].z,
        colour.r, colour.g, colour.b,
        corners[6].x, corners[6].y, corners[6].z,
        colour.r, colour.g, colour.b,
        corners[5].x, corners[5].y, corners[5].z,
        colour.r, colour.g, colour.b
    };

    std::vector<VertexLayout::Element> elements =
    {
        VertexLayout::Element(VertexLayout::Type::POSITION, 3u),
        VertexLayout::Element(VertexLayout::Type::COLOUR, 3u)
    };

    auto mesh = createMesh(VertexLayout(elements), 18, false);
    mesh->setPrimitveType(PrimitiveType::Lines);
    mesh->setVertexData(verts.data(), 18);
    return mesh;
}

Mesh::Ptr Mesh::createNormalMesh(const Mesh::Ptr& mesh, float scale)
{
    //find out where the data is stored
    auto& vertData = mesh->getVertexLayout();
    std::map<VertexLayout::Type, UInt32> dataPositions;
    for (auto i = 0u; i < vertData.getElementCount(); ++i)
    {
        auto type = vertData.getElement(i).type;
        switch (type)
        {
        case VertexLayout::Type::BITANGENT:
        case VertexLayout::Type::NORMAL:
        case VertexLayout::Type::POSITION:
        case VertexLayout::Type::TANGENT:
            dataPositions.insert(std::make_pair(type, i));
            break;
        default: break;
        }
    }

    //this assumes we have at least position data - what happens if we only have some kind of normal data? (other than being an invalid mesh)
    if (dataPositions.size() > 1)
    {
        void* data = nullptr;
        glCheck(glBindBuffer(GL_ARRAY_BUFFER, mesh->getVertexBuffer()));
        glCheck(data = glMapBuffer(GL_ARRAY_BUFFER, GL_READ_ONLY));

        UInt32 size = vertData.getVertexSize() * mesh->getVertexCount();
        std::vector<float> vboData(size / sizeof(float));
        std::memcpy(&vboData[0], data, size);

        glCheck(glUnmapBuffer(GL_ARRAY_BUFFER));

        std::vector<VertexLayout::Element> elements =
        {
            VertexLayout::Element(VertexLayout::Type::POSITION, 3u),
            VertexLayout::Element(VertexLayout::Type::COLOUR, 3u)
        };


        //copy relevant data
        std::vector<float> posData;
        if (dataPositions.find(VertexLayout::Type::POSITION) != dataPositions.end())
        {
            UInt32 start = 0u;
            for (auto i = 0u; i < dataPositions[VertexLayout::Type::POSITION]; ++i)
            {
                start += vertData.getElement(i).size;
            }

            const UInt32 stride = vertData.getVertexSize() / sizeof(float);
            const UInt32 end = vboData.size();

            for (auto i = start; i < end; i += stride)
            {
                //std::memcpy(&posData[j], &vboData[i], 3u);
                posData.push_back(vboData[i]);
                posData.push_back(vboData[i + 1]);
                posData.push_back(vboData[i + 2]);
            }
        }
        else
        {
            return nullptr;
        }

        std::vector<float> newData;
        //normal
        if (dataPositions.find(VertexLayout::Type::NORMAL) != dataPositions.end())
        {
            updateNormalMeshArray(dataPositions[VertexLayout::Type::NORMAL], vertData, posData, vboData, newData, scale, Colour::blue);
        }

        //tan 
        if (dataPositions.find(VertexLayout::Type::TANGENT) != dataPositions.end())
        {
            updateNormalMeshArray(dataPositions[VertexLayout::Type::TANGENT], vertData, posData, vboData, newData, scale, Colour::red);
        }

        //bitan
        if (dataPositions.find(VertexLayout::Type::BITANGENT) != dataPositions.end())
        {
            updateNormalMeshArray(dataPositions[VertexLayout::Type::BITANGENT], vertData, posData, vboData, newData, scale, Colour::green);
        }


        //create new mesh and return
        VertexLayout vertLayout(elements);
        UInt32 vertCount = newData.size() / (vertLayout.getVertexSize() / sizeof(float));
        auto newMesh = createMesh(vertLayout, vertCount);
        newMesh->setVertexData(newData.data(), vertCount);
        newMesh->setPrimitveType(PrimitiveType::Lines);

        return newMesh;
    }

    return nullptr;
}

Mesh::Ptr Mesh::createBoneMesh(const Skin::Ptr& skin, Int32 index)
{
    /*const auto& bones = (index > -1) ?  skin->getFrame(index) : skin->getBindPose();
    const auto& bindPose = skin->getBindPose();
    std::vector<float> data;

    for (const auto& b : bones)
    {
    if (b.getParentId() >= 0)
    {
    glm::vec3 position(b.getBindPoseMatrix()[3]);
    data.push_back(position.x);
    data.push_back(position.y);
    data.push_back(position.z);
    data.push_back(1.f);
    data.push_back(1.f);
    data.push_back(1.f);

    position = glm::vec3(bones[b.getParentId()].getBindPoseMatrix()[3]);
    data.push_back(position.x);
    data.push_back(position.y);
    data.push_back(position.z);
    data.push_back(1.f);
    data.push_back(1.f);
    data.push_back(1.f);
    }
    }

    std::vector<VertexLayout::Element> elements =
    {
    VertexLayout::Element(VertexLayout::Type::POSITION, 3u),
    VertexLayout::Element(VertexLayout::Type::COLOUR, 3u)
    };

    VertexLayout vertLayout(elements);
    UInt32 vertCount = data.size() / (vertLayout.getVertexSize() / sizeof(float));
    auto newMesh = createMesh(vertLayout, vertCount);
    newMesh->setVertexData(data.data(), vertCount);
    newMesh->setPrimitveType(PrimitiveType::Lines);

    return newMesh;*/
    return nullptr;
}

Mesh::Ptr Mesh::createGridMesh(const glm::vec2& size, const glm::ivec2& triCount, bool createUVs)
{
    assert(triCount.x > 0 && triCount.y > 0);

    auto xVertCount = triCount.x + 1;
    auto yVertCount = triCount.y + 1;

    float xSpacing = size.x / triCount.x;
    float ySpacing = size.y / triCount.y;

    auto arraySize = xVertCount * yVertCount * 3; //positions
    arraySize *= 2; //normals
    if (createUVs)
    {
        auto uvCount = xVertCount * yVertCount * 2;
        arraySize += uvCount;
        //TODO we could create tan/bitan too if creating UVs
    }

    //create verts
    std::vector<float> vertData(arraySize);
    for (auto y = 0, i= 0; y < yVertCount; ++y)
    {
        for (auto x = 0; x < xVertCount; ++x)
        {
            //position
            vertData[i++] = xSpacing * x - (size.x / 2.f);
            vertData[i++] = ySpacing * y - (size.y / 2.f);
            vertData[i++] = 0.f;

            //normal
            vertData[i++] = 0.f;
            vertData[i++] = 0.f;
            vertData[i++] = 1.f;

            if (createUVs)
            {
                vertData[i++] = (xSpacing * x) / size.x;
                vertData[i++] = (ySpacing * y) / size.y;
            }
        }
    }

    //create index array
    auto indexCount = 
        (xVertCount * 2u) * //indices per row
        (yVertCount - 1u) + //rows of triangles
        (yVertCount - 2u) * 2u; //and degenerate tris

    assert(indexCount < USHRT_MAX);

    std::vector<Int16> indices(indexCount);
    for (auto row = 0, i = 0; row < yVertCount - 1; ++row)
    {
        auto i1 = row * xVertCount;
        auto i2 = (row + 1) * xVertCount;

        if (row % 2 == 0) //even row
        {
            if (row > 0)
            {
                //wrap
                indices[i] = indices[i - 1];
                i++;
                indices[i++] = i1;
            }

            //add strip
            for (auto x = 0u; x < xVertCount; ++x)
            {
                indices[i++] = i1 + x;
                indices[i++] = i2 + x;
            }
        }
        else //odd row
        {
            if (row > 0)
            {
                indices[i] = indices[i - 1];
                i++;
                indices[i++] = i2 + (static_cast<Int32>(xVertCount)-1);
            }

            for (auto x = static_cast<Int32>(xVertCount)-1; x >= 0; --x)
            {
                indices[i++] = i2 + x;
                indices[i++] = i1 + x;
            }
        }
    }

    //create vbo
    std::vector<VertexLayout::Element> elements =
    {
        { VertexLayout::Type::POSITION, 3 },
        { VertexLayout::Type::NORMAL, 3 },
    };

    if (createUVs)
    {
        elements.emplace_back(VertexLayout::Type::UV0, 2);
        //TODO tan / bitan
    }

    auto mesh = Mesh::createMesh(VertexLayout(elements), xVertCount * yVertCount);
    mesh->setVertexData(vertData.data());
    //TODO we can calc bounds, but using vertex displacement will make this inaccurate

    auto& subMesh = mesh->addSubMesh(Mesh::PrimitiveType::TriangleStrip, Mesh::IndexFormat::I16, indexCount);
    subMesh.setIndexData(indices.data(), 0, indexCount);

    return mesh;
}
