/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/Mesh.hpp>
#include <chuf2/Shader.hpp>
#include <chuf2/VertexAttribBinding.hpp>

#include <chuf2/Util.hpp>

#include <iostream>

using namespace chuf;

namespace
{
    GLuint maxVertAttribs = 0u;
    std::vector<VertexAttribBinding::Ptr> attribCache;
}

VertexAttribBinding::VertexAttribBinding(const VertexAttribBinding::Key& k)
    : m_handle(0), m_shader(nullptr){}

VertexAttribBinding::~VertexAttribBinding()
{
    if (m_handle)
    {
        glCheck(glDeleteVertexArrays(1, &m_handle));
        m_handle = 0;
        //LOG("Successfully Destroyed AttribBinding");
    }
}

//public
VertexAttribBinding* VertexAttribBinding::create(const Mesh::Ptr& mesh, Shader* shader)
{
    assert(mesh);
    return create(mesh, mesh->getVertexLayout(), nullptr, shader);
}

VertexAttribBinding* VertexAttribBinding::create(const VertexLayout& layout, void* ptr, Shader* shader)
{
    return create(nullptr, layout, ptr, shader);
}

void VertexAttribBinding::bind()
{
    if (m_handle)
    {
        glCheck(glBindVertexArray(m_handle));
    }
    else
    {
        if (m_mesh)
        {
            glCheck(glBindBuffer(GL_ARRAY_BUFFER, m_mesh->getVertexBuffer()));
        }
        else
        {
            glCheck(glBindBuffer(GL_ARRAY_BUFFER, 0));
        }

        auto i = 0u;
        for (const auto& a : m_attributes)
        {
            if (a.enabled)
            {
                glCheck(glVertexAttribPointer(i, a.size, a.type, a.normalised, a.stride, a.ptr));
                glCheck(glEnableVertexAttribArray(i));
            }
            ++i;
        }
    }
}

void VertexAttribBinding::unbind()
{
    if (m_handle)
    {
        glCheck(glBindVertexArray(0));
    }
    else
    {
        if (m_mesh)
        {
            glCheck(glBindBuffer(GL_ARRAY_BUFFER, 0));
        }
        auto size = m_attributes.size();
        for(auto i = 0u; i < size; ++i)
        {
            if (m_attributes[i].enabled)
            {
                glCheck(glDisableVertexAttribArray(i));
            }
        }
    }
}

//private
VertexAttribBinding* VertexAttribBinding::create(const Mesh::Ptr& mesh, const VertexLayout& layout, void* ptr, Shader* shader)
{
    assert(shader);

    auto result = std::find_if(attribCache.begin(), attribCache.end(), [&mesh, shader](const VertexAttribBinding::Ptr& p)
    {
        return (p->m_mesh == mesh && p->m_shader == shader);
    });
    if (result != attribCache.end()) return result->get();


    if (maxVertAttribs == 0)
    {
        //query the context for max vertex attribs
        GLint count = 0;
        glCheck(glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &count));

        if (count <= 0)
        {
            Logger::Log("Current device supports 0 or less vertex attributes", Logger::Type::Warning);
            return nullptr;
        }
        maxVertAttribs = count;
    }

    auto vb = std::make_unique<VertexAttribBinding>(Key());
    //create VAO if supported
    if (mesh && glGenVertexArrays)
    {
        glCheck(glBindBuffer(GL_ARRAY_BUFFER, 0));
        glCheck(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0));

        glCheck(glGenVertexArrays(1, &vb->m_handle));

        if (vb->m_handle == 0)
        {
            Logger::Log("Failed to create VAO buffer", Logger::Type::Error);
            return nullptr;
        }

        glCheck(glBindVertexArray(vb->m_handle));
        glCheck(glBindBuffer(GL_ARRAY_BUFFER, mesh->getVertexBuffer()));
    }
    else //create a CPU side representation
    {
        vb->m_attributes = std::vector<VertexAttrib>(maxVertAttribs);
    }

    if (mesh) vb->m_mesh = mesh;
    vb->m_shader = shader;

    auto offset = 0;
    auto count = layout.getElementCount();
    for (auto i = 0u; i < count; ++i)
    {
        const VertexLayout::Element& e = layout.getElement(i);
        VertexAttributeID attribID;

        switch (e.type)
        {
        case VertexLayout::Type::BITANGENT:
            attribID = shader->getVertexAttribute(vertexAttribBitangent);
            break;
        case VertexLayout::Type::TANGENT:
            attribID = shader->getVertexAttribute(vertexAttribTangent);
            break;
        case VertexLayout::Type::POSITION:
            attribID = shader->getVertexAttribute(vertexAttribPosition);
            break;
        case VertexLayout::Type::COLOUR:
            attribID = shader->getVertexAttribute(vertexAttribColour);
            break;
        case VertexLayout::Type::NORMAL:
            attribID = shader->getVertexAttribute(vertexAttribNormal);
            break;
        case VertexLayout::Type::UV0:
            attribID = shader->getVertexAttribute(vertexAttribUV0);
            break;
        case VertexLayout::Type::UV1:
            attribID = shader->getVertexAttribute(vertexAttribUV1);
            break;
        case VertexLayout::Type::BLENDINDICES:
            attribID = shader->getVertexAttribute(vertexAttribBlendIndices);
            break;
        case VertexLayout::Type::BLENDWEIGHTS:
            attribID = shader->getVertexAttribute(vertexAttribBlendWeights);
            break;
        default: attribID = -1;
            break;
        }

        if (attribID != -1)
        {
            void* pointer = ptr ? static_cast<void*>(static_cast<UInt8*>(ptr) + offset) : (void*)offset;
            vb->setVertAttribPointer(attribID, static_cast<GLint>(e.size), GL_FLOAT, GL_FALSE, static_cast<GLsizei>(layout.getVertexSize()), pointer);
        }

        offset += e.size * sizeof(float);
    }

    if (vb->m_handle)
    {
        glCheck(glBindVertexArray(0));
    }

    auto vbPtr = vb.get();
    attribCache.push_back(std::move(vb));

    return vbPtr;
}

void VertexAttribBinding::setVertAttribPointer(GLuint index, GLint size, GLenum type, GLboolean normalise, GLsizei stride, void* ptr)
{
    assert(index < maxVertAttribs);

    if (m_handle)
    {
        //using VAOs
        glCheck(glVertexAttribPointer(index, size, type, normalise, stride, ptr));
        glCheck(glEnableVertexAttribArray(index));
    }
    else
    {
        //using client side
        assert(m_attributes.size() > index);
        m_attributes[index].enabled = true;
        m_attributes[index].size = size;
        m_attributes[index].type = type;
        m_attributes[index].normalised = normalise;
        m_attributes[index].stride = stride;
        m_attributes[index].ptr = ptr;
    }
}