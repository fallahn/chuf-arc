/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/physics/PhysWorld.hpp>

#include <chuf2/Shader.hpp>
#include <chuf2/Material.hpp>
#include <chuf2/MaterialProperty.hpp>
#include <chuf2/Mesh.hpp>
#include <chuf2/Log.hpp>

#include <string>

namespace
{
    const Uint16 maxLines = 2048u; //hard limit for our model's buffer
}

using namespace chuf;

PhysicsWorld::DebugDrawer::DebugDrawer()
    : m_mode        (btIDebugDraw::DBG_DrawAabb | btIDebugDraw::DBG_DrawWireframe),
    m_lineCount     (0),
    m_needsUpdate   (true)
{
    std::string vert = 
    {
        "#version 330\n"
        "uniform mat4 u_viewProjectionMatrix;\n"
        "in vec4 a_position;\n"
        "in vec4 a_colour;\n"
        "out vec4 v_colour;\n"
        "void main(void)\n"
        "{\n"
        "    v_colour = a_colour;\n"
        "    gl_Position = u_viewProjectionMatrix * a_position;\n"
        "}"
    };

    std::string frag =
    {
        "#version 330\n"
        "in vec4 v_colour;\n"
        "void main(void)\n"
        "{\n"
        "   gl_FragColor = v_colour;\n"
        "}"
    };

    Shader::SourceList list;
    list.insert(std::make_pair(Shader::Type::Vertex, vert));
    list.insert(std::make_pair(Shader::Type::Fragment, frag));

    auto shader = Shader::createFromSource(list);
    auto material = Material::create(shader);
    material->getStateBlock()->setDepthTest(true);
    material->getStateBlock()->setDepthFunc(RenderState::DepthFunc::LEQUAL);

    std::vector<VertexLayout::Element> layout =
    {
        { VertexLayout::Type::POSITION, 3 },
        { VertexLayout::Type::COLOUR, 4 }
    };

    m_mesh = Mesh::createMesh(VertexLayout(layout), maxLines*2u, true);
    m_mesh->setPrimitveType(Mesh::PrimitiveType::Lines);

    m_model = Model::create(m_mesh, nullptr);
    m_model->setMaterial(material);

    m_vertexData.reserve(maxLines * 2); //2 verts per line
}

//public
void PhysicsWorld::DebugDrawer::draw(const glm::mat4& vp)
{
    m_mesh->setVertexData((float*)(m_vertexData.data()), m_vertexData.size() * 7);//ARGG so hacky!
    m_model->getMaterial()->getProperty("u_viewProjectionMatrix")->setValue(vp);
    m_model->draw(RenderPass::Standard);//TODO allow materials to draw with no node
    m_lineCount = 0;
    m_vertexData.clear();
}

void PhysicsWorld::DebugDrawer::drawLine(const btVector3& from, const btVector3& to, const btVector3& fromColour, const btVector3& toColour)
{
    if (m_lineCount++ < maxLines)
    {
        m_vertexData.emplace_back(from.getX(), from.getY(), from.getZ(), fromColour.getX(), fromColour.getY(), fromColour.getZ());
        m_vertexData.emplace_back(to.getX(), to.getY(), to.getZ(), toColour.getX(), toColour.getY(), toColour.getZ());
    }
}

void PhysicsWorld::DebugDrawer::drawLine(const btVector3& from, const btVector3& to, const btVector3& colour)
{
    drawLine(from, to, colour, colour);
}

void PhysicsWorld::DebugDrawer::drawContactPoint(const btVector3& pointOnB, const btVector3& normalOnB, btScalar distance, int lifeTime, const btVector3& colour)
{
    drawLine(pointOnB, pointOnB + normalOnB, colour);
}

void PhysicsWorld::DebugDrawer::reportErrorWarning(const char* str)
{
    Logger::Log(std::string(str), Logger::Type::Warning);
}

void PhysicsWorld::DebugDrawer::draw3dText(const btVector3& location, const char* textString)
{
    //NOT SUPPORTED (too lazy.)
}

void PhysicsWorld::DebugDrawer::setDebugMode(Int32 mode)
{
    m_mode = mode;
}

Int32 PhysicsWorld::DebugDrawer::getDebugMode() const
{
    return m_mode;
}