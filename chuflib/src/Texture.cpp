/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/Texture.hpp>
#include <chuf2/Image.hpp>
#include <chuf2/Cache.hpp>
#include <chuf2/Util.hpp>

#include <catlib.hpp>

using namespace chuf;

namespace
{
    TextureID currentTextureID = 0;
    Texture::Type currentType = Texture::Type::Texture2D;
    Cache<Texture> textureCache;
}

Texture::Texture(const Texture::Key& key)
    : m_textureID		(0),
    m_format			(Invalid),
    m_type				(Texture2D),
    m_width				(0u),
    m_height			(0u),
    m_mipmaps			(false),
    m_flippedVertically	(false),
    m_wrapS				(Clamp),
    m_wrapT				(Clamp),
    m_wrapR				(Clamp),
    m_minFilter			(Nearest),
    m_magFilter			(Nearest),
    m_mode              (Mode::Static),
    m_framerate         (0.f),
    m_currentFrame      (0u)
{

}

Texture::~Texture()
{
    if (m_textureID)
    {
        glCheck(glDeleteTextures(1, &m_textureID));
        m_textureID = 0;
        //LOG("Destroyed texture " + m_path);
    }
}

//public
Texture* Texture::create(const std::string& path, bool createMipmaps, bool flipVertically)
{
    //make sure we're not trying to load an animated texture
    auto ext = Util::String::toLower(Util::String::getExtension(path));
    if (ext == ".cat")
    {
        return createAnimated(path);
    }
    
    //check cache first
    auto result = textureCache.find(path);
    if (result)
    {
        assert(result->m_type == Texture2D);
        if (createMipmaps) result->createMipmaps();
        return result;
    }

    //else create a new texture
    auto image = Image::create(path);
    if (image)
    {
        if (!image->isPowerTwo())
        {
            Logger::Log("Image \'" + path + "\' has non power-of-two dimensions. This may cause problems on some hardware.", Logger::Type::Warning);
        }
        if (flipVertically) image->flipVertically();
        Format f = (image->getFormat() == Image::Format::RGB) ? Format::RGB : Format::RGBA;
        return create(f, image->getWidth(), image->getHeight(), image->getData(), image->getUid(), createMipmaps, image->flippedVertically());
    }
    else //find the fall back texture if creation didn't work
    {
        result = textureCache.find("defaultTex"); //TODO make this string a const
        if (result)
        {
            return result;
        }
        else
        {
            image = Image::createChequer();
            assert(image);
            return create(Format::RGBA, image->getWidth(), image->getHeight(), image->getData(), "defaultTex", true, true);
        }
    }
}

Texture* Texture::create(const Image::Ptr& image, bool createMipmaps)
{
    assert(image->getFormat() != Image::Format::Invalid);
    
    //check cache first
    auto result = textureCache.find(image->getUid());
    if (result)
    {
        assert(result->m_type == Texture2D);
        if (createMipmaps) result->createMipmaps();
        return result;
    }
    
    if (!image->isPowerTwo())
    {
        Logger::Log("Image \'" + image->getUid() + "\' has non power-of-two dimensions. This may cause problems on some hardware.", Logger::Type::Warning);
    }

    switch (image->getFormat())
    {
    case Image::Format::A:
        return create(Texture::ALPHA, image->getWidth(), image->getHeight(), image->getData(), image->getUid(), createMipmaps, image->flippedVertically());
    case Image::Format::RGB:
        return create(Texture::RGB, image->getWidth(), image->getHeight(), image->getData(), image->getUid(), createMipmaps, image->flippedVertically());
    case Image::Format::RGBA:
        return create(Texture::RGBA, image->getWidth(), image->getHeight(), image->getData(), image->getUid(), createMipmaps, image->flippedVertically());
    case Image::Format::Invalid:
    default: return nullptr;
    }
}

Texture* Texture::createCubemap(const std::string& path)
{
    auto result = textureCache.find(path);
    if (result)
    {
        assert(result->m_type == TextureCube);
        return result;
    }
    
    auto images = Image::createCubemap(path);
    checkCubemapImages(images);

    std::string uid(path);
    if (images.size() != 6u)
    {
        result = textureCache.find("defaultCube");
        if (result)
        {
            return result;
        }
        else
        {
            images.clear();
            for (auto i = 0u; i < 6u; ++i)
            {
                images.push_back(std::move(Image::createChequer()));
            }
            uid = "defaultCube";
        }
    }

    return createCubemapFromImages(images, uid);
}

Texture* Texture::createCubemap(const std::vector<Image::Ptr>& images)
{
    assert(images.size() == 6u);

    auto uid = images[0]->getUid();
    auto result = textureCache.find(uid);
    if (result)
    {
        assert(result->m_type == TextureCube);
        return result;
    }

    checkCubemapImages(images);
    return createCubemapFromImages(images, uid);
}

Texture* Texture::createAnimated(const std::string& path)
{
    //check cache first
    auto result = textureCache.find(path);
    if (result)
    {
        assert(result->m_type == Texture2D);
        return result;
    }
    
    //else try parse cat file
    Texture::Ptr newTexture;
    try
    {
        newTexture = std::make_unique<Texture>(Key());
        newTexture->m_mode = Mode::Animated;

        Catlib catFile;
        catFile.loadFromFile(path);

        const auto& p = catFile.getProperties();
        const auto& data = catFile.getData();

        for (const auto& i : data)
        {
            //TODO - do we want to save memory by preventing mipmaps on animated textures?
            auto frame = Texture::create(Image::create(p.width, p.height, static_cast<Image::Format>(p.depth), i.get()), false);
            newTexture->m_frames.push_back(frame);
        }
        assert(p.framerate > 0.f);
        newTexture->m_framerate = 1.f / p.framerate;
    }
    catch (...)
    {
        LOG("Error creating animated texture: " + path + ". Caught catlib exception");
        return nullptr;
    }
    return textureCache.insert(path, newTexture);
}

Texture* Texture::createArray(const std::vector<std::unique_ptr<Image>>& images)
{
    assert(images.size() > 1); //otherwise we just want a normal texture. TODO do this automatically?
    //validate no images are nullptr
    if (std::any_of(images.begin(), images.end(), [](const std::unique_ptr<Image>& i)
    {
        return (i == nullptr);
    }))
    {
        LOG("image list contained null image");
        return nullptr;
    }

    //validate images are all same dimension and depth
    auto width = images[0]->getWidth();
    auto height = images[0]->getHeight();
    auto depth = images[0]->getFormat();

    if (!std::all_of(images.begin() + 1, images.end(),
        [width, height, depth](const std::unique_ptr<Image>& i)
    {
        return (i->getWidth() == width && i->getHeight() == height && i->getFormat() == depth);
    }))
    {
        LOG("Not all images in image array have the same properties");
        return nullptr;
    };

    //create single uid
    Uint32 temp = 0;
    for (const auto& img : images)
    {
        const auto& str = img->getUid();
        for (auto i = 0u; i < str.size(); ++i)
        {
            temp += i * static_cast<Uint8>(str[i]);
        }
    }
    std::string uid = std::to_string(temp);

    Format format;
    switch (depth)
    {
    case Image::Format::A:
        format = Format::ALPHA;
        break;
    case Image::Format::RGB:
        format = Format::RGB;
        break;
    case Image::Format::RGBA:
        format = Format::RGBA;
        break;
    case Image::Format::Invalid:
    default:
        return nullptr;
    }

    return createArray(format, width, height, images, uid, images[0]->flippedVertically());
}

const std::string& Texture::getPath() const
{
    return m_path;
}

Texture::Format Texture::getFormat() const
{
    return m_format;
}

Texture::Type Texture::getType() const
{
    return m_type;
}

UInt32 Texture::getWidth() const
{
    return m_width;
}

UInt32 Texture::getHeight() const
{
    return m_height;
}

TextureID Texture::getHandle() const
{
    return (m_mode == Mode::Static) ? m_textureID : getCurrentFrame();
}

void Texture::createMipmaps()
{
    if (!m_mipmaps)
    {
        glCheck(glBindTexture(m_type, m_textureID));
        if (glGenerateMipmap)
            glCheck(glGenerateMipmap(m_type));

        m_mipmaps = true;
    }
}

bool Texture::hasMipmaps() const
{
    return m_mipmaps;
}

bool Texture::flippedVertically() const
{
    return m_flippedVertically;
}


//private
TextureID Texture::getCurrentFrame() const
{
    assert(m_framerate > 0.f && m_frames.size() > 0);
    UInt32 frame = static_cast<Uint32>(m_frameTimer.elapsed().asSeconds() / m_framerate);
    m_currentFrame = frame % m_frames.size();
    return m_frames[m_currentFrame]->m_textureID;
}

Texture* Texture::create(Format format, UInt32 width, UInt32 height, const UInt8* data, const std::string& uid, bool createMipmaps, bool flippedVertically)
{	
    GLuint texID;
    glCheck(glGenTextures(1, &texID));
    glCheck(glBindTexture(GL_TEXTURE_2D, texID));
    glCheck(glPixelStorei(GL_UNPACK_ALIGNMENT, 1));

    glCheck(glTexImage2D(GL_TEXTURE_2D, 0, static_cast<GLenum>(format), width, height, 0, static_cast<GLenum>(format), GL_UNSIGNED_BYTE, data));

    auto minFilter = createMipmaps ? NearestMipmapLinear : Linear;
    glCheck(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, minFilter));

    auto texture = std::make_unique<Texture>(Key());
    texture->m_textureID = texID;
    texture->m_format = format;
    texture->m_type = Texture2D;
    texture->m_width = width;
    texture->m_height = height;
    texture->m_minFilter = minFilter;
    texture->m_path = uid;
    texture->m_flippedVertically = flippedVertically;
    if (createMipmaps)
        texture->createMipmaps();

    glCheck(glBindTexture(currentType, currentTextureID));

    return textureCache.insert(uid, texture);
}

Texture* Texture::createCubemapFromImages(const std::vector<Image::Ptr>& images, const std::string& uid)
{
    assert(images.size() == 6u);

    GLuint texID;
    glCheck(glGenTextures(1, &texID));
    glCheck(glBindTexture(GL_TEXTURE_CUBE_MAP, texID));
    glCheck(glPixelStorei(GL_UNPACK_ALIGNMENT, 1));

    Format format = (images[0]->getFormat() == Image::Format::RGB) ? RGB : RGBA;
    for (auto i = 0u; i < 6u; ++i)
    {
        glCheck(glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0,
                            static_cast<GLenum>(format),
                            images[i]->getWidth(), images[i]->getHeight(),
                            0, static_cast<GLenum>(format), GL_UNSIGNED_BYTE, images[i]->getData()));
    }

    glCheck(glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR));
    glCheck(glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR));
    glCheck(glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
    glCheck(glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));


    Texture::Ptr texture = std::make_unique<Texture>(Texture::Key());
    texture->m_textureID = texID;
    texture->m_format = format;
    texture->m_width = images[0]->getWidth();
    texture->m_height = images[0]->getHeight();
    texture->m_minFilter = Linear;
    texture->m_wrapS = Clamp;
    texture->m_wrapT = Clamp;
    texture->m_path = uid;
    texture->m_type = TextureCube;
    texture->createMipmaps(); //cubemaps require mipmaps

    glCheck(glBindTexture(currentType, currentTextureID));

    return textureCache.insert(images[0]->getUid(), texture);
}

Texture* Texture::createArray(Format format, UInt32 width, UInt32 height, const std::vector<std::unique_ptr<Image>>& images, const std::string& uid, bool flippedVertically)
{
    GLuint texID;
    glCheck(glGenTextures(1, &texID));
    glCheck(glBindTexture(GL_TEXTURE_2D_ARRAY, texID));
    //glCheck(glTexStorage3D(GL_TEXTURE_2D_ARRAY, 1, static_cast<GLenum>(format), width, height, images.size())); //requires > GL 4.3
    glCheck(glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, static_cast<GLenum>(format), width, height, images.size(), 0, static_cast<GLenum>(format), GL_UNSIGNED_BYTE, nullptr));

    //mip level, x offset, y offset, index
    for (Uint32 i = 0u; i < images.size(); ++i)
    {
        glCheck(glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, i, width, height, 1, static_cast<GLenum>(format), GL_UNSIGNED_BYTE, images[i]->getData()));
    }
    glCheck(glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR));
    glCheck(glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR));
    glCheck(glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_REPEAT));
    glCheck(glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_REPEAT));

    auto texture = std::make_unique<Texture>(Key());
    texture->m_textureID = texID;
    texture->m_format = format;
    texture->m_type = Texture2DArray;
    texture->m_width = width;
    texture->m_height = height;
    texture->m_minFilter = Linear;
    texture->m_path = uid;
    texture->m_flippedVertically = flippedVertically;
    texture->createMipmaps();

    glCheck(glBindTexture(currentType, currentTextureID));

    return textureCache.insert(uid, texture);
}

void Texture::checkCubemapImages(const std::vector<Image::Ptr>& images)
{
    bool powTwo = true;
    for (const auto& i : images)
        powTwo = i->isPowerTwo();

    if (!powTwo)
    {
        Logger::Log("One or more cubemap images have non power-of-two dimensions. This may cause problems on some hardware.", Logger::Type::Warning);
    }
}