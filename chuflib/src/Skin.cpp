/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/Skin.hpp>

#include <cassert>

using namespace chuf;

Skin::Skin(const BoneHierarchy bones, const Keyframes& keyframes, const Key& key)
    : m_frames      (keyframes),
    m_boneIndices   (bones)
{

}

//public
Skin::Ptr Skin::create(const BoneHierarchy bones, const Keyframes& keyframes)
{
    return std::make_shared<Skin>(bones, keyframes, Key());
}

const Frame& Skin::getFrame(UInt32 index) const
{
    assert(index < m_frames.size());
    return m_frames[index];
}

void Skin::addAnimation(const Skin::Animation& animation)
{
    m_animations.push_back(animation);
    m_animations.back().setSkin(this);
}

void Skin::setAnimations(const Skin::Animations& animations)
{
    m_animations = animations;
    for (auto& a : m_animations)
        a.setSkin(this);
}

Skin::Animation& Skin::getAnimation(UInt32 index)
{
    return m_animations[index];
}

//private