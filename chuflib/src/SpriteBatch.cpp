/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/App.hpp>
#include <chuf2/ConfigFile.hpp>
#include <chuf2/Log.hpp>
#include <chuf2/Material.hpp>
#include <chuf2/MaterialProperty.hpp>
#include <chuf2/Mesh.hpp>
#include <chuf2/SpriteBatch.hpp>
#include <chuf2/Sprite.hpp>
#include <chuf2/Util.hpp>

#include <chuf2/glm/gtc/matrix_transform.hpp>

#include <functional>
#include <array>

using namespace chuf;

namespace
{
    const UInt32 maxVerts = 1024u; //barrier before resizing buffer
    const UInt8 vertsPerQuad = 6u; //2 extra verts to create degenerate triangles between quads
}


SpriteBatch::SpriteBatch(const SpriteBatch::Key& key)
    : m_vertCount	(0u),
    m_bufferSize    (maxVerts),
    m_dirtyBits     (ALL){}

SpriteBatch::Ptr SpriteBatch::create(const std::string& path, Shader* shader)
{
    auto texture = Texture::create(path);
    if (!texture) return nullptr;
    return std::move(create(texture, shader));
}

SpriteBatch::Ptr SpriteBatch::create(Texture* texture, Shader* shader)
{
    Material::Ptr material;

    assert(texture);
    if (!shader)shader = Shader::create("res/shaders/Unlit.vert", "res/shaders/Unlit.frag", "#define VERTEX_COLOUR\n");
    assert(shader);
    material = Material::create(shader);
    material->getProperty("u_diffuseTexture")->setSampler(Texture::Sampler::create(texture));
    auto state = RenderState::StateBlock::create();
    state->setCullFace(true);
    state->setBlend(true);
    state->setBlendDest(RenderState::BlendFunc::ONE_MINUS_SRC_ALPHA);
    state->setBlendSrc(RenderState::BlendFunc::SRC_ALPHA);
    material->setStateBlock(state);

    Ptr sb = std::make_unique<SpriteBatch>(Key());
    auto size = App::getInstance().getView().viewport;
    sb->m_projectionMatrix = glm::ortho<float>(0.f, size.width, 0.f, size.height, -0.1f, 10.f);

    std::function<const glm::mat4&()> f = std::bind(&SpriteBatch::getViewProjectionMatrix, sb.get());
    material->getProperty("u_worldViewProjectionMatrix")->bindValue(f);
    sb->setMaterial(material); //use proper setter to update vert attrib bindings
    sb->m_textureSize = {static_cast<float>(texture->getWidth()), static_cast<float>(texture->getHeight())};

    return std::move(sb);
}

SpriteBatch::Ptr SpriteBatch::createFromFile(const std::string& path)
{
    ConfigFile::Ptr cf = ConfigFile::create(path);
    if (cf->getName() != "spritebatch")
    {
        Logger::Log(path + ": no valid spritebatch found.", Logger::Type::Error);
        return nullptr;
    }
    //check for image path
    auto img = cf->findProperty("image");
    if (!img)
    {
        Logger::Log(path + ": no valid image path found.", Logger::Type::Error);
        return nullptr;
    }
    //try loading texture from path
    Texture* t = Texture::create(img->valueAsString());
    if (!t)
    {
        Logger::Log(path + ": failed to create texture from: " + img->valueAsString(), Logger::Type::Error);
        return nullptr;
    }

    //check for optional shader parameter
    auto s = cf->findObjectWithName("shader");
    Shader* shader = nullptr;
    if (s)
    {
        //try creating shader from config
        std::string vert, frag, defines;
        if (s->findProperty("vertShader")) vert = s->findProperty("vertShader")->valueAsString();
        if (s->findProperty("fragShader")) frag = s->findProperty("fragShader")->valueAsString();
        if (s->findProperty("defines")) defines = s->findProperty("defines")->valueAsString();

        if (!defines.empty())
        {
            //string::removeChar(defines, ' ');
            Util::String::replace(defines, ',', '\n');
            defines += '\n';
        }

        if (!vert.empty() && !frag.empty())
        {
            shader = Shader::create(vert, frag, defines);
        }

        if (!shader)
        {
            Logger::Log("failed to create spritebatch shader from \'" + vert + "\' and \'" + frag + "\'. Attempting to create default spritebatch shader...", Logger::Type::Warning);
        }
    }
    if (!shader)
    {
        //create default shader
        shader = Shader::create("res/shaders/Unlit.vert", "res/shaders/Unlit.frag", "#define VERTEX_COLOUR\n");
    }
    assert(shader);

    //create spritebatch from texture shader combo
    auto spriteBatch = create(t, shader);

    //add any sprites from the config
    auto& objects = cf->getObjects();
    for (const auto& o : objects)
    {
        if (o->getName() == "sprite")
        {
            auto sprite = spriteBatch->addSprite({ 0.f, 0.f }, o->getId());
            auto& properties = o->getProperties();
            for (const auto& p : properties)
            {
                if (p->getName() == "position")
                {
                    sprite->setPosition(p->valueAsVec2());
                }
                else if (p->getName() == "colour")
                {
                    glm::vec4 c = p->valueAsVec4();
                    sprite->setColour({ c.r, c.g, c.b, c.a });
                }
                else if (p->getName() == "rotation")
                {
                    sprite->setRotation(p->valueAsFloat());
                }
                else if (p->getName() == "subrect")
                {
                    glm::vec4 sr = p->valueAsVec4();
                    sprite->setSubRect({ sr.x, sr.y, sr.z, sr.w });
                }
                else if (p->getName() == "scale")
                {
                    sprite->setScale(p->valueAsVec2());
                }
                else if (p->getName() == "size")
                {
                    sprite->setSize(p->valueAsVec2());
                }
                else if (p->getName() == "origin")
                {
                    sprite->setOrigin(p->valueAsVec2());
                }
            }
        }
    }

    return spriteBatch;
}

Sprite::Ptr SpriteBatch::addSprite(const glm::vec2& position, const std::string& name)
{
    if (m_vertCount > m_bufferSize - vertsPerQuad) //resize the buffer
    {
        m_bufferSize += maxVerts;
        setMaterial(m_material);
        Logger::Log("resized spritebatch to " + std::to_string(m_bufferSize));
    }

    Sprite::Key key; // must be l-value
    Sprite::Ptr sprite = std::make_shared<Sprite>(name, m_mesh->getVertexLayout(), key);
    sprite->m_parentBatch = this;
    sprite->m_subRect = { 0.f, 0.f, m_textureSize.x, m_textureSize.y };

    sprite->m_arrayOffset = m_vertexArray.size();

    //set sprite texture size
    sprite->m_textureSize = m_textureSize;
    sprite->setSize(m_textureSize);

    //push back empty vert data ready for sprite to update
    auto arrayCount = (m_mesh->getVertexSize() / sizeof(float)) * vertsPerQuad;
    for (auto i = 0u; i < arrayCount; ++i)
        m_vertexArray.push_back(0.f);

    m_vertCount += vertsPerQuad;

    sprite->setDirty(Sprite::ALL);
    sprite->m_index = m_sprites.size();
    sprite->setPosition(position);

    m_sprites.push_back(sprite);

    m_dirtyBits |= LOCAL_BOUNDS; //automatically updates global

    return sprite;
}

Sprite::Ptr SpriteBatch::findSprite(const std::string& name)
{
    auto result = std::find_if(m_sprites.begin(), m_sprites.end(),
        [&name](const Sprite::Ptr& p)
    {
        return (p->getName() == name);
    });

    if (result != m_sprites.end()) return *result;
    return nullptr;
}

void SpriteBatch::removeSprite(const Sprite::Ptr& sprite)
{
    //TODO can we defer this for when deleting multiple sprites at once?
    //would only work with contiguous sprites. Only do this if it becomes
    //a geniune bottleneck somewhere

    auto result = std::find_if(m_sprites.begin(), m_sprites.end(),
        [sprite](const Sprite::Ptr& p)
    {
        return (p == sprite);
    });

    if (result != m_sprites.end())
    {
        //erase vertices
        auto spriteIndex = sprite->m_arrayOffset;
        auto dataSize = (m_mesh->getVertexSize() / sizeof(float)) * vertsPerQuad;
        auto start = m_vertexArray.begin() + spriteIndex;
        m_vertexArray.erase(start, start + dataSize);
        m_vertCount -= vertsPerQuad;
        m_mesh->setVertexData(&m_vertexArray[0], m_vertCount);

        //update indices of sprites remaining after removal
        for (auto& s : m_sprites)
        {
            if (s->m_arrayOffset > spriteIndex)
            {
                s->m_arrayOffset -= dataSize;
                s->m_index--;
            }
        }

        //erase sprite
        m_sprites.erase(result);

        m_dirtyBits |= LOCAL_BOUNDS;
    }
}

const std::vector<std::shared_ptr<Sprite>>& SpriteBatch::getSprites() const
{
    return m_sprites;
}

void SpriteBatch::setTextureRepeat(bool repeat)
{
    auto sampler = m_material->getProperty("u_diffuseTexture")->getSampler();
    if (repeat)
    {
        sampler->setWrap(Texture::Wrap::Repeat, Texture::Wrap::Repeat);
    }
    else
    {
        sampler->setWrap(Texture::Wrap::Clamp, Texture::Wrap::Clamp);
    }
}

void SpriteBatch::setProjectionMatrix(const glm::mat4& matrix)
{
    m_projectionMatrix = matrix;
    m_dirtyBits |= TRANSFORM | GLOBAL_BOUNDS;
}

void SpriteBatch::setViewMatrix(const glm::mat4& matrix)
{
    m_viewMatrix = matrix;
    m_dirtyBits |= TRANSFORM | GLOBAL_BOUNDS;
}

const FloatRect& SpriteBatch::getLocalBounds() const
{
    if (m_dirtyBits & LOCAL_BOUNDS)
    {
        m_dirtyBits &= ~LOCAL_BOUNDS;

        m_localBounds.width = 0.f;
        m_localBounds.height = 0.f;
        for (const auto& s : m_sprites)
        {
            //TODO shouldn't this take into account sprite's local transform?
            auto pos = s->getPosition();
            auto w = pos.x + s->m_subRect.width;
            if (w > m_localBounds.width) m_localBounds.width = w;
            auto h = pos.y + s->m_subRect.height;
            if (h > m_localBounds.height) m_localBounds.height = h;
            m_localBounds.top = m_localBounds.height;
        }

        //need to update global bounds
        m_dirtyBits |= GLOBAL_BOUNDS;
    }

    return m_localBounds;
}

const FloatRect& SpriteBatch::getGlobalBounds() const
{
    if (m_dirtyBits & GLOBAL_BOUNDS)
    {
        m_dirtyBits &= ~GLOBAL_BOUNDS;

        //convert localbounds to world space
        std::array<glm::vec4, 2u> worldPoints
        {
            {
                glm::vec4(m_localBounds.width, m_localBounds.top, 0.f, 1.f) ,
                glm::vec4(0.f, 0.f, 0.f, 1.f)
            }
        };

        //multiply by view
        for (auto& p : worldPoints)
            p = m_viewMatrix * p;

        std::array<glm::vec2, 2u> finalPoints;
        if (worldPoints[0].x > worldPoints[1].x)
        {
            finalPoints[1].x = worldPoints[0].x;
            finalPoints[0].x = worldPoints[1].x;
        }
        else
        {
            finalPoints[0].x = worldPoints[0].x;
            finalPoints[1].x = worldPoints[1].x;
        }
        if (worldPoints[0].y > worldPoints[0].y)
        {
            finalPoints[1].y = worldPoints[0].y;
            finalPoints[0].y = worldPoints[1].y;
        }
        else
        {
            finalPoints[0].y = worldPoints[0].y;
            finalPoints[1].y = worldPoints[1].y;
        }

        m_globalBounds = { finalPoints[0].x, finalPoints[1].y, finalPoints[1].x - finalPoints[0].x, finalPoints[1].y - finalPoints[0].y };
    }
    return m_globalBounds;
}

void SpriteBatch::draw()
{
    if (m_dirtySprites.size() > 0)
    {
        for (auto& s : m_dirtySprites)
            s->update();

        m_dirtySprites.clear();
        m_mesh->setVertexData(&m_vertexArray[0], m_vertCount);
    }

    //draw vert array
    if (m_vertCount == 0) return;

    glCheck(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0));

    assert(m_material);

    auto t = m_material->getTechnique();
    assert(t);
    auto pCount = t->getPassCount();
    for (auto i = 0u; i < pCount; ++i)
    {
        auto p = t->getPass(i);
        assert(p);
        p->bind();

        glCheck(glDrawArrays(static_cast<GLenum>(Mesh::PrimitiveType::TriangleStrip), 0, m_vertCount));
        p->unbind();
    }
}

//private
const glm::mat4& SpriteBatch::getViewProjectionMatrix()
{
    if (m_dirtyBits & TRANSFORM)
    {
        m_dirtyBits &= ~TRANSFORM;
        m_viewProjectionMatrix = m_projectionMatrix * m_viewMatrix;
    }
    return m_viewProjectionMatrix;
}

void SpriteBatch::setMaterial(const Material::Ptr& material)
{
    m_material = material;

    VertexLayout vertexLayout
    ({
        { VertexLayout::Type::POSITION, 2u },
        { VertexLayout::Type::UV0, 2u },
        { VertexLayout::Type::COLOUR, 4u }
    });

    m_mesh = Mesh::createMesh(vertexLayout, m_bufferSize, true);
    m_vertexArray.reserve(m_bufferSize * (vertexLayout.getVertexSize() / sizeof(float)));

    assert(m_material);
    //update vertex attrib bindings with array pointer
    //TODO because attrib bindings are cached, old ones don't get destroyed when replaced
    auto tCount = m_material->getTechniqueCount();
    for (auto i = 0u; i < tCount; ++i)
    {
        auto t = m_material->getTechnique(i);
        assert(t);
        auto pCount = t->getPassCount();
        for (auto j = 0u; j < pCount; ++j)
        {
            auto p = t->getPass(j);
            assert(p);
            auto vb = VertexAttribBinding::create(m_mesh, p->getShader());
            p->setVertexAttribBinding(vb);
        }
    }
}
