/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/BoundingBox.hpp>
#include <chuf2/BoundingSphere.hpp>
#include <chuf2/Ray.hpp>

using namespace chuf;

namespace
{
    void updateMinMax(const glm::vec4& point, glm::vec4& min, glm::vec4& max)
    {
        if (point.x < min.x) min.x = point.x;
        if (point.x > max.x) max.x = point.x;
        if (point.y < min.y) min.y = point.y;
        if (point.y > max.y) max.y = point.y;
        if (point.z < min.z) min.z = point.z;
        if (point.z > max.z) max.z = point.z;
    }
}

BoundingBox::BoundingBox(const glm::vec3& minPoint, const glm::vec3& maxPoint)
    : m_minPoint    (minPoint),
    m_maxPoint      (maxPoint){}

BoundingBox::BoundingBox(const BoundingBox& other)
{
    set(other);
}

//public
glm::vec3 BoundingBox::getCentre() const
{
    auto c = m_maxPoint - m_minPoint;
    c /= 2.f;
    c += m_minPoint;
    return c;
}

const glm::vec3& BoundingBox::getMinPoint() const
{
    return m_minPoint;
}

const glm::vec3& BoundingBox::getMaxPoint() const
{
    return m_maxPoint;
}

void BoundingBox::setMinPoint(const glm::vec3& minPoint)
{
    m_minPoint = minPoint;
}

void BoundingBox::setMaxPoint(const glm::vec3& maxPoint)
{
    m_maxPoint = maxPoint;
}

bool BoundingBox::intersects(const BoundingBox& bb) const
{
    return ((m_minPoint.x >= bb.m_minPoint.x && m_minPoint.x <= bb.m_maxPoint.x) || (bb.m_minPoint.x >= m_minPoint.x && bb.m_minPoint.x <= m_maxPoint.x)) &&
        ((m_minPoint.y >= bb.m_minPoint.y && m_minPoint.y <= bb.m_maxPoint.y) || (bb.m_minPoint.y >= m_minPoint.y && bb.m_minPoint.y <= m_maxPoint.y)) &&
        ((m_minPoint.z >= bb.m_minPoint.z && m_minPoint.z <= bb.m_maxPoint.z) || (bb.m_minPoint.z >= m_minPoint.z && bb.m_minPoint.z <= m_maxPoint.z));
}

bool BoundingBox::intersects(const BoundingSphere& bs) const
{
    return bs.intersects(*this);
}

bool BoundingBox::intersects(const Frustum& f) const
{
    return (intersects(f.getNear()) != Plane::Intersection::Back &&
        intersects(f.getFar()) != Plane::Intersection::Back &&
        intersects(f.getLeft()) != Plane::Intersection::Back &&
        intersects(f.getRight()) != Plane::Intersection::Back &&
        intersects(f.getBottom()) != Plane::Intersection::Back &&
        intersects(f.getTop()) != Plane::Intersection::Back);

    //const auto planes = f.getPlanes();
    //std::vector<glm::vec3> corners =
    //{
    //    //near face, anti clockwise facing the front
    //    { m_minPoint.x, m_maxPoint.y, m_maxPoint.z },
    //    { m_minPoint.x, m_minPoint.y, m_maxPoint.z },
    //    { m_maxPoint.x, m_minPoint.y, m_maxPoint.z },
    //    { m_maxPoint.x, m_maxPoint.y, m_maxPoint.z },

    //    //far face, anti clockwise facing the rear
    //    { m_maxPoint.x, m_maxPoint.y, m_minPoint.z },
    //    { m_maxPoint.x, m_minPoint.y, m_minPoint.z },
    //    { m_minPoint.x, m_minPoint.y, m_minPoint.z },
    //    { m_minPoint.x, m_maxPoint.y, m_minPoint.z }
    //};

    ////check all corners agains each plane - if all of them are behind at least one
    ////plane then the box is outside the frustum
    //for (const auto& p : planes)
    //{
    //    if (p.distance(corners[0]) > 0) continue;
    //    if (p.distance(corners[1]) > 0) continue;
    //    if (p.distance(corners[2]) > 0) continue;
    //    if (p.distance(corners[3]) > 0) continue;
    //    if (p.distance(corners[4]) > 0) continue;
    //    if (p.distance(corners[5]) > 0) continue;
    //    if (p.distance(corners[6]) > 0) continue;
    //    if (p.distance(corners[7]) > 0) continue;
    //    return false;
    //}
    //return true;
}

Plane::Intersection BoundingBox::intersects(const Plane& p)const
{
    glm::vec3 centre((m_minPoint + m_maxPoint) * 0.5f);
    float distance = p.distance(centre);

    glm::vec3 extents((m_maxPoint - m_minPoint) * 0.5f);
    extents *= p.getNormal();
    if (std::fabsf(distance) <= (std::fabsf(extents.x) + std::fabsf(extents.y) + std::fabsf(extents.z)))
    {
        return Plane::Intersection::Intersecting;
    }

    return (distance > 0.f) ? Plane::Intersection::Front : Plane::Intersection::Back;
}

float BoundingBox::intersects(const Ray& ray) const
{
    float dnear = 0.f;
    float dfar = 0.f;
    float tmin = 0.f;
    float tmax = 0.f;

    const auto& origin = ray.getOrigin();
    const auto& direction = ray.getDirection();

    //X direction
    float div = 1.f / direction.x;
    if (div >= 0.f)
    {
        tmin = (m_minPoint.x - origin.x) * div;
        tmax = (m_maxPoint.x - origin.x) * div;
    }
    else
    {
        tmin = (m_maxPoint.x - origin.x) * div;
        tmax = (m_minPoint.x - origin.x) * div;
    }
    dnear = tmin;
    dfar = tmax;

    //check if the ray misses the box.
    if (dnear > dfar || dfar < 0.0f)
    {
        return -1.f;
    }

    //Y direction.
    div = 1.f / direction.y;
    if (div >= 0.0f)
    {
        tmin = (m_minPoint.y - origin.y) * div;
        tmax = (m_maxPoint.y - origin.y) * div;
    }
    else
    {
        tmin = (m_maxPoint.y - origin.y) * div;
        tmax = (m_minPoint.y - origin.y) * div;
    }



    if (tmin > dnear)
    {
        dnear = tmin;
    }
    if (tmax < dfar)
    {
        dfar = tmax;
    }
    // Check if the ray misses the box.
    if (dnear > dfar || dfar < 0.0f)
    {
        -1.f;
    }

    // Z direction.
    div = 1.f / direction.z;
    if (div >= 0.f)
    {
        tmin = (m_minPoint.z - origin.z) * div;
        tmax = (m_maxPoint.z - origin.z) * div;
    }
    else
    {
        tmin = (m_maxPoint.z - origin.z) * div;
        tmax = (m_minPoint.z - origin.z) * div;
    }

    if (tmin > dnear)
    {
        dnear = tmin;
    }
    if (tmax < dfar)
    {
        dfar = tmax;
    }

    //check if the ray misses the box.
    if (dnear > dfar || dfar < 0.0f)
    {
        return -1.f;
    }
    //the ray intersects the box (and since the direction of a Ray is normalized, dnear is the distance to the ray).
    return dnear;
}

bool BoundingBox::empty() const
{
    return (m_minPoint == m_maxPoint);
}

void BoundingBox::merge(const BoundingBox& bb)
{
    m_minPoint.x = std::min(m_minPoint.x, bb.m_minPoint.x);
    m_minPoint.y = std::min(m_minPoint.y, bb.m_minPoint.y);
    m_minPoint.z = std::min(m_minPoint.z, bb.m_minPoint.z);

    m_maxPoint.x = std::max(m_maxPoint.x, bb.m_maxPoint.x);
    m_maxPoint.y = std::max(m_maxPoint.y, bb.m_maxPoint.y);
    m_maxPoint.z = std::max(m_maxPoint.z, bb.m_maxPoint.z);
}

void BoundingBox::merge(const BoundingSphere& bs)
{
    const auto& centre = bs.getCentre();
    float radius = bs.getRadius();

    m_minPoint.x = std::min(m_minPoint.x, centre.x - radius);
    m_minPoint.y = std::min(m_minPoint.y, centre.y - radius);
    m_minPoint.z = std::min(m_minPoint.z, centre.z - radius);

    m_maxPoint.x = std::max(m_maxPoint.x, centre.x + radius);
    m_maxPoint.y = std::max(m_maxPoint.y, centre.y + radius);
    m_maxPoint.z = std::max(m_maxPoint.z, centre.z + radius);
}

void BoundingBox::set(const glm::vec3& minPoint, const glm::vec3& maxPoint)
{
    m_minPoint = minPoint;
    m_maxPoint = maxPoint;
}

void BoundingBox::set(const BoundingBox& other)
{
    m_minPoint = other.m_minPoint;
    m_maxPoint = other.m_maxPoint;
}

std::vector<glm::vec4> BoundingBox::getCorners() const
{
    return
    {
        //near face, anti clockwise facing the front
        { m_minPoint.x, m_maxPoint.y, m_maxPoint.z, 1.f },
        { m_minPoint.x, m_minPoint.y, m_maxPoint.z, 1.f },
        { m_maxPoint.x, m_minPoint.y, m_maxPoint.z, 1.f },
        { m_maxPoint.x, m_maxPoint.y, m_maxPoint.z, 1.f },

        //far face, anti clockwise facing the rear
        { m_maxPoint.x, m_maxPoint.y, m_minPoint.z, 1.f },
        { m_maxPoint.x, m_minPoint.y, m_minPoint.z, 1.f },
        { m_minPoint.x, m_minPoint.y, m_minPoint.z, 1.f },
        { m_minPoint.x, m_maxPoint.y, m_minPoint.z, 1.f }
    };
}

void BoundingBox::transform(const glm::mat4& matrix)
{
    auto corners = getCorners();

    corners[0] = matrix * corners[0];
    glm::vec4 newMin = corners[0];
    glm::vec4 newMax = corners[0];

    for (auto& v : corners)
    {
        v = matrix * v;
        updateMinMax(v, newMin, newMax);
    }

    m_minPoint = { newMin.x, newMin.y, newMin.z };
    m_maxPoint = { newMax.x, newMax.y, newMax.z };
}

BoundingBox& BoundingBox::operator *= (const glm::mat4& matrix)
{
    transform(matrix);
    return *this;
}

BoundingBox operator * (const glm::mat4& matrix, const BoundingBox& bb)
{
    BoundingBox b(bb);
    b.transform(matrix);
    return b;
}