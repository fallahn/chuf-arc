/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/SubMesh.hpp>

#include <chuf2/Util.hpp>

using namespace chuf;

SubMesh::SubMesh(const SubMesh::Key& key)
    : m_mesh		(nullptr),
    m_meshIndex		(0u),
    m_primitiveType	(Mesh::PrimitiveType::TriangleStrip),
    m_indexFormat	(Mesh::IndexFormat::I8),
    m_count			(0u),
    m_indexBuffer	(0),
    m_dynamic		(false)
{}

SubMesh::~SubMesh()
{
    if (m_indexBuffer)
    {
        glCheck(glDeleteBuffers(1, &m_indexBuffer));
        //LOG("Successfully Destroyed SubMesh");
    }
}
//public
UInt32 SubMesh::getMeshIndex() const
{
    return m_meshIndex;
}

Mesh::PrimitiveType SubMesh::getPrimitiveType() const
{
    return m_primitiveType;
}

UInt32 SubMesh::getIndexCount() const
{
    return m_count;
}

Mesh::IndexFormat SubMesh::getIndexFormat() const
{
    return m_indexFormat;
}

IndexBufferID SubMesh::getIndexBuffer() const
{
    return m_indexBuffer;
}

bool SubMesh::dynamic()const
{
    return m_dynamic;
}

void SubMesh::setIndexData(const void* data, UInt32 start, UInt32 count)
{
    glCheck(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indexBuffer));

    UInt32 indexSize = 0u;
    switch (m_indexFormat)
    {
    case Mesh::IndexFormat::I8:
        indexSize = 1u;
        break;
    case Mesh::IndexFormat::I16:
        indexSize = 2u;
        break;
    case Mesh::IndexFormat::I32:
        indexSize = 4u;
        break;
    }

    if (start == 0 && count == 0)
    {
        glCheck(glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexSize * m_count, data, m_dynamic ? GL_DYNAMIC_DRAW : GL_STATIC_DRAW));
    }
    else
    {
        if (count == 0)
        {
            count = m_count - start;
        }
        glCheck(glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, start * indexSize, count * indexSize, data));
    }
}

//private
SubMesh::Ptr SubMesh::create(Mesh* mesh, UInt32 meshIndex, Mesh::PrimitiveType type, Mesh::IndexFormat format, UInt32 count, bool dynamic)
{
    assert(mesh);

    GLuint vbo;
    glCheck(glGenBuffers(1, &vbo));
    glCheck(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo));

    UInt32 indexSize = 0u;
    switch (format)
    {
    case Mesh::IndexFormat::I8:
        indexSize = 1u;
        break;
    case Mesh::IndexFormat::I16:
        indexSize = 2u;
        break;
    case Mesh::IndexFormat::I32:
        indexSize = 4u;
        break;
    }

    glCheck(glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexSize * count, NULL, dynamic ? GL_DYNAMIC_DRAW : GL_STATIC_DRAW));

    auto subMesh = std::make_unique<SubMesh>(Key());
    subMesh->m_mesh = mesh;
    subMesh->m_meshIndex = meshIndex;
    subMesh->m_primitiveType = type;
    subMesh->m_indexFormat = format;
    subMesh->m_count = count;
    subMesh->m_indexBuffer = vbo;
    subMesh->m_dynamic = dynamic;

    return std::move(subMesh);
}
