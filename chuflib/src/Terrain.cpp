/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/Terrain.hpp>
#include <chuf2/Node.hpp>
#include <chuf2/Util.hpp>
#include <chuf2/Reports.hpp>
#include <chuf2/Scene.hpp>
#include <chuf2/Camera.hpp>

#include <cassert>

namespace
{

}

using namespace chuf;

Terrain::Terrain(SceneRenderer* sr, const Key&)
    : Drawable  (sr),
    m_flags     (DirtyBits::TRANSFORM | DirtyBits::CAMERA){}

//public
Terrain::Ptr Terrain::create(SceneRenderer* sr, HeightData::Ptr& heightData, const glm::vec3& scale, UInt32 patchSize, UInt32 detailLevels, float skirtSize)
{
    assert(heightData);

    Uint32 width = heightData->columnCount();
    UInt32 height = heightData->rowCount();

    auto terrain = std::make_unique<Terrain>(sr, Key());
    terrain->m_heightData = std::move(heightData);
    terrain->m_scale = scale;    
    terrain->m_material = Material::createFromFile("res/materials/terrain.cmf");
    terrain->m_patchSize = glm::vec2(static_cast<float>(patchSize) * scale.x, static_cast<float>(patchSize) * scale.y);

    auto& bounds = terrain->m_boundingBox;
    terrain->m_quadTree.create({bounds.getMaxPoint().x, bounds.getMinPoint().z,
        bounds.getMaxPoint().x - bounds.getMinPoint().x, bounds.getMaxPoint().z - bounds.getMinPoint().z});

    float halfWidth = static_cast<float>(width - 1) * 0.5f;
    float halfHeight = static_cast<float>(height - 1) * 0.5f;

    //calc the largest number of verts to skip on the lowest LOD
    UInt32 maxSkip = static_cast<UInt32>(std::pow(2.0, static_cast<double>(detailLevels - 1)));

    //and create the patches
    UInt32 x1 = 0u, x2 = 0u, z1 = 0u, z2 = 0u;
    UInt32 row = 0u, col = 0u;
    for (auto z = 0u; z < height - 1; z = z2, ++row)
    {
        z1 = z;
        z2 = std::min(z1 + patchSize, height - 1);

        for (auto x = 0u; x < width - 1; x = x2, ++col)
        {
            x1 = x;
            x2 = std::min(x1 + patchSize, width - 1);

            auto patch = TerrainPatch::create(*terrain, terrain->m_patches.size(),
                                            row, col, terrain->m_heightData->getData(),
                                            width, height, x1, z1, x2, z2,
                                            -halfWidth, -halfHeight, maxSkip, skirtSize);

            bounds.merge(patch->getBoundingBox());
            terrain->m_patches.push_back(std::move(patch));
        }
    }

    terrain->m_patchDims = { col / row, row };
    terrain->m_halfSize = { halfWidth, halfHeight };

    //update patches with this terrain's material
    for (auto& p : terrain->m_patches)
    {
        p->updateMaterial();
        terrain->m_quadTree.add(p.get());
    }
    REPORT("Patch Count", std::to_string(terrain->m_patches.size()));

    return std::move(terrain);
}

UInt32 Terrain::getPatchCount() const
{
    return m_patches.size();
}

const TerrainPatch& Terrain::getPatch(UInt32 index) const
{
    return *m_patches[index];
}

const BoundingBox& Terrain::getBoundingBox() const
{
    return m_boundingBox;
}

float Terrain::getHeight(float x, float z) const
{
    auto rows = m_heightData->rowCount();
    auto cols = m_heightData->columnCount();

    glm::vec4 localPos = getInverseWorldMatrix() * glm::vec4(x, 0.f, z, 1.f);
    x = localPos.x + static_cast<float>(cols - 1) * 0.5f;
    z = localPos.z + static_cast<float>(rows - 1) * 0.5f;

    float height = m_heightData->getValue({ x, z });
    if (getNode()) //scale by node size if it is attached
    {
        height *= Util::Matrix::getScale(const_cast<Node*>(getNode())->getWorldMatrix()).y;
    }

    height *= m_scale.y;
    return height;
}

void Terrain::setNode(Node* n)
{
    Component::setNode(n);

    for (auto& p : m_patches)
    {
        p->updateNodeBindings();
    }
}

UInt32 Terrain::draw(UInt32 passflags, bool wireframe) const
{
    bool updateVis = ((m_flags & DirtyBits::CAMERA) != 0);
    if (updateVis)
    {
        m_visibilitySet = m_quadTree.queryArea(getNode()->getScene()->getActiveCamera()->getFrustumBounds2D());
        if(!(passflags & RenderPass::Reflection)) m_flags &= ~DirtyBits::CAMERA; //don't reset on reflection as we'll need to test again with different frustum

        for (auto& p : m_visibilitySet) //we only need update current subset rather than all patches on camera change
            dynamic_cast<TerrainPatch*>(p)->cameraChanged();

        REPORT("Patches After Broad Phase", std::to_string(m_visibilitySet.size()));
    }

    auto count = 0u;
    for (const auto& p : m_visibilitySet)
        count += dynamic_cast<TerrainPatch*>(p)->draw(passflags, updateVis, wireframe);

    if(updateVis) REPORT("Patches Drawn", std::to_string(count));
    return count;
}

Material::Ptr Terrain::getMaterial() const
{
    return m_material;
}

void Terrain::cameraChanged(Camera*)
{
    m_flags |= DirtyBits::CAMERA;
    //static int i = 0;
    //REPORT("cam move count", std::to_string(i++));
}

//private
void Terrain::transformChanged(Transform& t)
{
    m_flags |= DirtyBits::TRANSFORM;

    for (auto& p : m_patches)
    {
        p->m_flags |= TerrainPatch::DirtyBits::BOUNDS;
    }
}

const glm::mat4& Terrain::getInverseWorldMatrix() const
{
    if (m_flags & DirtyBits::TRANSFORM)
    {
        m_flags &= ~DirtyBits::TRANSFORM;

        if (getNode()) m_inverseWorldMatrix - const_cast<Node*>(getNode())->getWorldMatrix();
        else m_inverseWorldMatrix = glm::mat4();

        m_inverseWorldMatrix = glm::scale(m_inverseWorldMatrix, m_scale);
        m_inverseWorldMatrix = glm::inverse(m_inverseWorldMatrix);
    }
    return m_inverseWorldMatrix;
}

std::vector<TerrainPatch*> Terrain::getPatches(const FloatRect& testArea) const
{
    Int32 x = std::min(std::max(static_cast<Int32>(std::floor((testArea.left + m_halfSize.x) / m_patchSize.x)), 0), static_cast<Int32>(m_patchDims.x));
    Int32 y = std::min(std::max(static_cast<Int32>(std::floor((testArea.top + m_halfSize.y) / m_patchSize.y)), 0), static_cast<Int32>(m_patchDims.y));

    Int32 start = y * m_patchDims.x + x;
    Int32 width = std::min(static_cast<Int32>(std::ceil(testArea.width / m_patchSize.x)), static_cast<Int32>(m_patchDims.x));
    Int32 height = std::min(static_cast<Int32>(std::ceil(testArea.height / m_patchSize.y)) + 1, static_cast<Int32>(m_patchDims.y));
    Int32 i = 0;

    std::vector<TerrainPatch*> retVal;
    for (start, i; i < height; start += m_patchDims.x, ++i)
    {
        Int32 maxWidth = start + width;
        for (Int32 j = start; j < maxWidth && j < m_patches.size(); ++j)
        {
            retVal.push_back(m_patches[j].get());
        }
    }

    return retVal;
}