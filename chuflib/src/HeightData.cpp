/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/HeightData.hpp>
#include <chuf2/Image.hpp>
#include <chuf2/Log.hpp>

#include <cassert>
#include <fstream>

using namespace chuf;

HeightData::HeightData(UInt32 width, UInt32 height, const Key& key)
    : m_data    (width * height, 0.f),
    m_dimensions(width, height)
{

}

//public
HeightData::Ptr HeightData::create(UInt32 width, UInt32 height)
{
    assert(width > 1 && height > 1);
    return std::make_unique<HeightData>(width, height, HeightData::Key());
}

HeightData::Ptr HeightData::create(const std::string& path, float minScale, float maxScale)
{
    assert(minScale < maxScale);

    auto img = Image::create(path);
    img->flipVertically(); //might have unextected results when trying to line up with textures
    auto hd = std::make_unique<HeightData>(img->getWidth(), img->getHeight(), HeightData::Key());

    if (img->getFormat() != Image::Format::Invalid)
    {
        UInt32 stride = static_cast<UInt32>(img->getFormat());
        UInt32 size = img->getWidth() * img->getHeight();
        const float heightScale = maxScale - minScale;

        auto data = img->getData();
        UInt32 index = 0;
        for (Int32 y = img->getHeight() - 1, i = 0; y >= 0; --y)
        {
            for (UInt32 x = 0u, w = img->getWidth(); x < w; ++x)
            {
                index = (y * w + x) * stride;
                float value = static_cast<float>(data[index]);
                if (stride > 1)
                {
                    value += static_cast<float>(data[index + 1]);
                    value += static_cast<float>(data[index + 2]);
                    value /= 3.f;
                }

                value = minScale + ((value / 255.f) * heightScale);
                hd->m_data[i++] = value;
            }
        }
        ////This does a row for row copy - ideally we want this if
        ////we are going to do real time modifcations to it
        //for (auto i = 0u; i < size; ++i)
        //{
        //    float value = static_cast<float>(data[i]);
        //    if (stride > 1)
        //    {
        //        value += static_cast<float>(data[i + 1]);
        //        value += static_cast<float>(data[i + 2]);
        //        value /= 3.f;
        //    }

        //    value = minScale + ((value / 255.f) * heightScale);
        //    hd->m_data[i] = value;
        //}

        return std::move(hd);
    }
    return nullptr;
}

HeightData::Ptr HeightData::createFromRaw(const std::string& path, UInt32 width, UInt32 height, float minScale, float maxScale)
{
    assert(width > 1 && height > 1 && minScale < maxScale);
    
    //open file
    std::ifstream file(path, std::ios::binary | std::ios::in);
    if (!file.good() || !file.is_open() || file.fail())
    {
        Logger::Log("failed to open raw data for heightmap: " + path, Logger::Type::Warning, Logger::Output::All);
        file.close();
        return nullptr;
    }

    //validate size
    file.seekg(0, std::ios::end);
    std::size_t fileSize = static_cast<std::size_t>(file.tellg());
    file.seekg(0, std::ios::beg);

    if (fileSize < 4)
    {
        Logger::Log("raw data invalid file size: " + path, Logger::Type::Warning, Logger::Output::All);
        file.close();
        return nullptr;
    }

    //read file into memory
    std::vector<char> fileData(fileSize);
    file.read(fileData.data(), fileSize);

    UInt32 bitCount = (fileSize / (width * height)) * 8;
    if (bitCount != 8 && bitCount != 16)
    {
        Logger::Log("raw data invalid bit count: " + path, Logger::Type::Warning, Logger::Output::All);
        return nullptr;
    }

    const float heightScale = maxScale - minScale;
    auto hd = std::make_unique<HeightData>(width, height, HeightData::Key());

    auto size = width * height;
    //assert(size == fileSize);
    if (bitCount == 8)
    {
        LOG("8 bit raw image data found");
        for (UInt32 y = 0, i = 0; y < height; ++y)
        {
            for (UInt32 x = 0; x < width; ++x, ++i)
            {
                hd->m_data[i] = minScale + std::abs(static_cast<float>(fileData[y * width + x])/* / 255.f*/) * heightScale;
            }
        }
    }
    else
    {
        LOG("16 bit raw image data found");
        UInt32 idx = 0u;
        for (Uint32 i = 0, y = 0; y < height; ++y)
        {
            for (UInt32 x = 0; x < width; ++x, ++i)
            {
                idx = (y * width + x) << 1;
                hd->m_data[i] = minScale + std::abs(static_cast<float>(fileData[idx] | static_cast<int>(fileData[idx + 1] << 8))/* / 65535.f*/) * heightScale;
            }
        }
        //for (auto i = 0u; i < size; ++i)
        //{
        //    idx = i << 1;
        //    hd->m_data[i] = minScale + ((fileData[idx] | static_cast<int>(fileData[idx + 1] << 8)) / 65535.f) * heightScale;
        //}
    }

    return std::move(hd);
}

HeightData::Ptr HeightData::create(const float* data, UInt32 width, UInt32 height)
{
    assert(width > 1 && height > 1);
    
    auto hd = std::make_unique<HeightData>(width, height, HeightData::Key());
    std::memcpy(hd->m_data.data(), data, width * height);

    return std::move(hd);
}

const std::vector<float>& HeightData::getData() const
{
    return m_data;
}

float HeightData::getValue(const glm::vec2& position) const
{
    assert(position.x >= 0 && position.y >= 0);
    
    auto indexX = std::min(static_cast<UInt32>(position.x), m_dimensions.x);
    auto indexY = std::min(static_cast<UInt32>(position.y), m_dimensions.y);

    float lowerVal = m_data[indexY * m_dimensions.x + indexX];
    float diff = 0.f;
    bool doAverage = false;
    if (indexX < m_dimensions.x - 1)
    {
        float upperValX = m_data[indexY * m_dimensions.x + (indexX + 1)];
        diff += (position.x - static_cast<float>(indexX)* (upperValX - lowerVal));
        doAverage = true;
    }

    if (indexY < m_dimensions.y - 1)
    {
        float upperValY = m_data[(indexY + 1) * m_dimensions.x + indexX];
        diff += (position.y - static_cast<float>(indexY)* (upperValY - lowerVal));
        if (doAverage)
        {
            diff /= 2.f;
        }
    }

    return lowerVal + diff;
}

const glm::uvec2& HeightData::getSize() const
{
    return m_dimensions;
}

UInt32 HeightData::rowCount() const
{
    return m_dimensions.y;
}

UInt32 HeightData::columnCount() const
{
    return m_dimensions.x;
}

//private