/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/AudioListener.hpp>
#include <chuf2/Util.hpp>
#include <chuf2/Log.hpp>
#include <chuf2/Node.hpp>

#include <memory>
#include <array>

using namespace chuf;

namespace
{
    float a_volume = 100.f;
    glm::vec3 a_position;
    glm::vec3 a_direction(0.f, 0.f, -1.f);
    glm::vec3 a_upVector(0.f, 1.f, 0.f);

    ALCdevice* a_device = nullptr;
    ALCcontext* a_context = nullptr;
}

AudioListener::AudioListener()
{
    a_device = alcOpenDevice(nullptr);

    if (a_device)
    {
        a_context = alcCreateContext(a_device, nullptr);

        if (a_context)
        {
            alcMakeContextCurrent(a_context);

            orientate();

            alCheck(alListenerf(AL_GAIN, a_volume * 0.01f));
            alCheck(alListener3f(AL_POSITION, a_position.x, a_position.y, a_position.z));
        }
        else
        {
            Logger::Log("failed to create openAL context", Logger::Type::Error);
        }
    }
    else
    {
        Logger::Log("failed to create openAL device", Logger::Type::Error);
    }
}

AudioListener::~AudioListener()
{
    alcMakeContextCurrent(nullptr);
    if (a_context) alcDestroyContext(a_context);
    if (a_device) alcCloseDevice(a_device);
}

//public
void AudioListener::setPosition(const glm::vec3& position)
{
    a_position = position;    
    if (a_context)
        alCheck(alListener3f(AL_POSITION, position.x, position.y, position.z));
}

glm::vec3 AudioListener::getPosition()
{
    return a_position;
}

void AudioListener::setUpVector(const glm::vec3& upVec)
{
    a_upVector = upVec;
    if(a_context) orientate();
}

glm::vec3 AudioListener::getUpVector()
{
    return a_upVector;
}

void AudioListener::setDirection(const glm::vec3& direction)
{
    a_direction = direction;
    if (a_context) orientate();
}

glm::vec3 AudioListener::getDirection()
{
    return a_direction;
}

void AudioListener::setVolume(float vol)
{
    a_volume = vol;
    if (a_context)
        alCheck(alListenerf(AL_GAIN, a_volume * 0.01f));
}

float AudioListener::getVolume()
{
    return a_volume;
}

int AudioListener::getFormatFromChannelCount(unsigned channelCount)
{
    //just makes sure there is a valid context if it does not exist
    std::unique_ptr<AudioListener> listener;
    if (!a_device) listener = std::make_unique<AudioListener>();

    int format = 0;
    switch (channelCount)
    {
    case 1:
        format = AL_FORMAT_MONO16;
        break;
    case 2:
        format = AL_FORMAT_STEREO16;
        break;
    case 4:
        format = alGetEnumValue("AL_FORMAT_QUAD16");
        break;
    case 6:
        format = alGetEnumValue("AL_FORMAT_51CHN16");
        break;
    case 7:
        format = alGetEnumValue("AL_FORMAT_61CHN16");
        break;
    case 8:
        format = alGetEnumValue("AL_FORMAT_71CHN16");
        break;
        break;
    default:
        format = 0;
        break;
    }
    return format;
}

//private
void AudioListener::orientate()
{
    std::array<float, 6u> o = { a_direction.x, a_direction.y, a_direction.z, a_upVector.x, a_upVector.y, a_upVector.z };
    alCheck(alListenerfv(AL_ORIENTATION, &o[0]));
}



///----------------------------------------------------------///
void AudioListener::CameraListener::cameraChanged(Camera* c)
{
    auto node = c->getNode();
    if (c->getNode() && a_device)
    {
        AudioListener::setPosition(node->getTranslationWorld());
        AudioListener::setDirection(node->getForwardVectorWorld());
        AudioListener::setUpVector(node->getUpVectorWorld());
    }
}