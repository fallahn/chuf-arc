/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/Material.hpp>
#include <chuf2/MaterialProperty.hpp>
#include <chuf2/Pass.hpp>
#include <chuf2/Log.hpp>
#include <chuf2/ConfigFile.hpp>
#include <chuf2/Image.hpp>
#include <chuf2/Texture.hpp>
#include <chuf2/Util.hpp>

#include <chuf2/shaders/GeomVis.hpp>

#include <iostream>

using namespace chuf;

namespace
{
    std::map<Material::Type, Material::Ptr> fallbackMaterials;
    const chuf::UInt8 maxLayers = 4u; //TODO increase to 5?

    void checkForDiffuse(ConfigObject* matProp, const RenderState::StateBlock::Ptr& sb, const std::string& path)
    {
        auto diffuse = matProp->findProperty("diffuse");
        if (!diffuse)
        {
            Logger::Log(path + " has no diffuse map property", Logger::Type::Warning);
            //TODO what if we just want to use vertex colours?
            matProp->addProperty("diffuse", "missing");
            sb->setCullFace(false); //we don't know what's missing from the material, so we have to resort to rendering both sides
        }
    }
    void createDiffuseSampler(Material::Ptr& material, const ConfigProperty::Ptr& p)
    {
        auto ts = Texture::Sampler::create(p->valueAsString());
        if (ts)
        {
            material->addTag("dimensions", std::to_string(ts->getTexture()->getHeight()) + "," + std::to_string(ts->getTexture()->getHeight()));
            material->getProperty("u_diffuseTexture")->setSampler(ts);
        }
        else
        {
            Logger::Log("Failed to create texture sampler for " + p->valueAsString(), Logger::Type::Error);
        }
    }
    void createNormalSampler(Material::Ptr& material, const ConfigProperty::Ptr& p, std::string& defines)
    {
        auto ts = Texture::Sampler::create(p->valueAsString());
        if (ts)
        {
            material->getProperty("u_normalTexture")->setSampler(ts);
            defines += "#define BUMP\n";
        }
        else
        {
            Logger::Log("Failed to create texture sampler for " + p->valueAsString(), Logger::Type::Error);
        }
    }
    void createMaskSampler(Material::Ptr& material, const ConfigProperty::Ptr& p, std::string& defines)
    {
        auto ts = Texture::Sampler::create(p->valueAsString());
        if (ts)
        {
            material->getProperty("u_maskTexture")->setSampler(ts);
            defines += "#define MASK\n";
        }
        else
        {
            Logger::Log("Failed to create texture sampler for " + p->valueAsString(), Logger::Type::Error);
        }
    }
    void setCullFace(const ConfigProperty::Ptr& p, const RenderState::StateBlock::Ptr& sb)
    {
        auto value = p->valueAsString();
        if (value == "FRONT")
        {
            sb->setCullFaceSide(RenderState::CullFace::FRONT);
        }
        else if (value == "BACK")
        {
            sb->setCullFaceSide(RenderState::CullFace::BACK);
        }
    }

    void checkImageArray(std::vector<Image::Ptr>& images, Colour colour)
    {
        UInt32 width = 0u, height = 0u;
        Image::Format format = Image::Format::Invalid;
        for (const auto& i : images)
        {
            if (i != nullptr)
            {
                width = i->getWidth();
                height = i->getHeight();
                format = i->getFormat();
                break;
            }
        }

        if (width == 0) width = 2u;
        if (height == 0) height = 2u;
        if (format == Image::Format::Invalid) format = Image::Format::RGB;

        for (auto& i : images)
        {
            if (i == nullptr ||
                i->getWidth() != width ||
                i->getHeight() != height ||
                i->getFormat() != format)
            {
                i = Image::create(width, height, format, colour);
                LOG("Image Array skipping non-matching size");
            }
            else if (!i->flippedVertically())
            {
                i->flipVertically();
            }
        }
    }
}

Material::Material(const Material::Key& key) : m_type(Type::Custom), m_flags(RenderPass::Standard){}

Material::~Material()
{
    //LOG("Successfully Destroyed Material");
}

Material::Ptr Material::create(Shader* shader, Uint32 defaultPassflags)
{
    assert(shader);
    auto material = std::make_shared<Material>(Material::Key());
    auto technique = std::make_unique<Technique>("", material);
    auto pass = std::make_unique<Pass>("", technique.get());
    pass->m_shader = shader;
    pass->m_flags = defaultPassflags;
    technique->m_passes.push_back(std::move(pass));
    material->m_currentTechnique = technique.get();
    material->m_techniques.push_back(std::move(technique));
    material->m_flags = defaultPassflags;
    return material;
}

Material::Ptr Material::create(const std::string& vertShader, const std::string& fragShader, UInt32 defaultPassflags)
{
    auto material = std::make_shared<Material>(Material::Key());
    auto technique = std::make_unique<Technique>("", material);

    auto pass = std::make_unique<Pass>("", technique.get());
    if (!pass->init(vertShader, fragShader))
    {
        Logger::Log("Failed creating Pass for material", Logger::Type::Warning);
        return nullptr;
    }
    pass->m_flags = defaultPassflags;

    technique->m_passes.push_back(std::move(pass));
    material->m_currentTechnique = technique.get();
    material->m_techniques.push_back(std::move(technique));
    material->m_flags = defaultPassflags;
    return material;
}

Material::Ptr Material::createFromFile(const std::string& path)
{
    ConfigFile::Ptr cfgFile = ConfigFile::create(path);
    ConfigObject* matProp = nullptr;
    bool createFallback = false;
    if (cfgFile)
    {
        matProp = (Util::String::toLower(cfgFile->getName()) == "material") ? cfgFile.get() : cfgFile->findObjectWithName("material");
    }
    else
    {
        return nullptr;
    }
    if (matProp)
    {
        auto material = std::make_shared<Material>(Material::Key());
        auto technique = std::make_unique<Technique>("", material);

        std::string materialType = Util::String::toLower(matProp->getId());

        //create shader / samplers based on material type
        if (materialType == "unlit")
        {
            createUnlit(material, technique, path, matProp);
        }
        else if (materialType == "vertexlit")
        {
            createVertexLit(material, technique, path, matProp);
        }
        else if (materialType == "lightmapped")
        {
            createLightMapped(material, technique, path, matProp);
        }
        else if (materialType == "skybox")
        {
            createSkybox(material, technique, path, matProp);
        }
        else if (materialType == "water")
        {
            createWater(material, technique, path, matProp);
        }
        else if (materialType == "terrain")
        {
            createTerrain(material, technique, path, matProp);
        }
        else
        {
            Logger::Log(materialType + ": unrecognised material type in " + path + ". Valid types are: Unlit, LightMapped, VertexLit, SkyBox and Water.", Logger::Type::Error);
            return nullptr;
        }

        material->m_currentTechnique = technique.get();
        material->m_techniques.push_back(std::move(technique));

        return material;
    }
    Logger::Log("No valid material data found in \'" + path + "\'", Logger::Type::Error);
    return nullptr;
}

Material::Ptr Material::getDefault(Type type)
{
    if (fallbackMaterials.find(type) != fallbackMaterials.end()) return fallbackMaterials[type];

    auto material = std::make_shared<Material>(Material::Key());
    auto technique = std::make_unique<Technique>("", material);
    switch (type)
    {
    default:
    case Type::Unlit: 
    {
        ConfigObject configObject("material", "Unlit");
        createUnlit(material, technique, "default", &configObject);
    } break;
    case Type::LightMapped:
    {
        ConfigObject configObject("material", "LightMapped");
        createLightMapped(material, technique, "default", &configObject);
    } break;

    }
    material->m_currentTechnique = technique.get();
    material->m_techniques.push_back(std::move(technique));
    material->setName("default");
    fallbackMaterials.insert(std::make_pair(type, material));

    return material;
}

UInt32 Material::getTechniqueCount() const
{
    return m_techniques.size();
}

Technique* Material::getTechnique(UInt32 index)
{
    assert(index < m_techniques.size());
    return m_techniques[index].get();
}

Technique* Material::getTechnique(const std::string& name) const
{
    for (const auto& t : m_techniques)
        if (t->getName() == name) return t.get();

    return nullptr;
}

Technique* Material::getTechnique()
{
    return m_currentTechnique;
}

void Material::setTechnique(const std::string& name)
{
    auto t = getTechnique(name);
    if (t) m_currentTechnique = t;
    else Logger::Log("Failed to find technique \'" + name + "\'", Logger::Type::Error);
}

void Material::bindNode(Node* node)
{
    std::hash<std::string> hashFunc;

    RenderState::bindNode(node);
    for (auto& t : m_techniques)
    {
        t->bindNode(node);
        //bind default matrices
        for (auto& p : t->m_passes)
        {
            auto u = p->getShader()->getUniform("u_worldMatrix", hashFunc("u_worldMatrix"));
            if (u)
            {
                setPropertyBinding("u_worldMatrix", RenderState::PropertyBinding::WorldMatrix);
            }
            //
            u = p->getShader()->getUniform("u_viewMatrix", hashFunc("u_viewMatrix"));
            if (u)
            {
                setPropertyBinding("u_viewMatrix", RenderState::PropertyBinding::ViewMatrix);
            }
            //
            u = p->getShader()->getUniform("u_reflectionViewMatrix", hashFunc("u_reflectionViewMatrix"));
            if (u)
            {
                setPropertyBinding("u_reflectionViewMatrix", RenderState::PropertyBinding::ReflectionViewMatrix);
            }
            //
            u = p->getShader()->getUniform("u_projectionMatrix", hashFunc("u_projectionMatrix"));
            if (u)
            {
                setPropertyBinding("u_projectionMatrix", RenderState::PropertyBinding::ProjectionMatrix);
            }
            //
            u = p->getShader()->getUniform("u_worldViewMatrix", hashFunc("u_worldViewMatrix"));
            if (u)
            {
                setPropertyBinding("u_worldViewMatrix", RenderState::PropertyBinding::WorldViewMatrix);
            }
            //
            u = p->getShader()->getUniform("u_reflectionWorldViewMatrix", hashFunc("u_reflectionWorldViewMatrix"));
            if (u)
            {
                setPropertyBinding("u_reflectionWorldViewMatrix", RenderState::PropertyBinding::ReflectionWorldViewMatrix);
            }
            //
            u = p->getShader()->getUniform("u_viewProjectionMatrix", hashFunc("u_viewProjectionMatrix"));
            if (u)
            {
                setPropertyBinding("u_viewProjectionMatrix", RenderState::PropertyBinding::ViewProjectionMatrix);
            }
            //
            u = p->getShader()->getUniform("u_reflectionViewProjectionMatrix", hashFunc("u_reflectionViewProjectionMatrix"));
            if (u)
            {
                setPropertyBinding("u_reflectionViewProjectionMatrix", RenderState::PropertyBinding::ReflectionViewProjectionMatrix);
            }
            //
            u = p->getShader()->getUniform("u_worldViewProjectionMatrix", hashFunc("u_worldViewProjectionMatrix"));
            if (u)
            {
                setPropertyBinding("u_worldViewProjectionMatrix", RenderState::PropertyBinding::WorldViewProjectionMatrix);
            }
            //
            u = p->getShader()->getUniform("u_reflectionWorldViewProjectionMatrix", hashFunc("u_reflectionWorldViewProjectionMatrix"));
            if (u)
            {
                setPropertyBinding("u_reflectionWorldViewProjectionMatrix", RenderState::PropertyBinding::ReflectionWorldViewProjectionMatrix);
            }
            //
            u = p->getShader()->getUniform("u_inverseTransposeWorldMatrix", hashFunc("u_inverseTransposeWorldMatrix"));
            if (u)
            {
                setPropertyBinding("u_inverseTransposeWorldMatrix", RenderState::PropertyBinding::InverseTransposeWorldMatrix);
            }
            u = p->getShader()->getUniform("u_inverseTransposeWorldViewMatrix", hashFunc("u_inverseTransposeWorldViewMatrix"));
            if (u)
            {
                setPropertyBinding("u_inverseTransposeWorldViewMatrix", RenderState::PropertyBinding::InverseTransposeWorldViewMatrix);
            }
            u = p->getShader()->getUniform("u_ambientColour", hashFunc("u_ambientColour"));
            if (u)
            {
                setPropertyBinding("u_ambientColour", RenderState::PropertyBinding::AmbientColour);
            }
            u = p->getShader()->getUniform("u_cameraWorldPosition", hashFunc("u_cameraWorldPosition"));
            if (u)
            {
                setPropertyBinding("u_cameraWorldPosition", RenderState::PropertyBinding::CameraWorldPosition);
            }
            u = p->getShader()->getUniform("u_cameraViewPosition", hashFunc("u_cameraViewPosition"));
            if (u)
            {
                setPropertyBinding("u_cameraViewPosition", RenderState::PropertyBinding::CameraViewPosition);
            }
        }
    }
}

const Material::Type& Material::getType() const
{
    return m_type;
}

void Material::setName(const std::string& name)
{
    m_name = name;
}

const std::string& Material::getName() const
{
    return m_name;
}

bool Material::hasFlag(UInt32 flags) const
{
    return ((m_flags & flags) != 0);
}

//private
void Material::setBlend(Material::Ptr& material, const ConfigProperty::Ptr& p, const RenderState::StateBlock::Ptr& sb)
{
    bool useBlend = p->valueAsBool();
    sb->setBlend(useBlend);
    sb->setBlendDest(RenderState::BlendFunc::ONE_MINUS_SRC_ALPHA);
    sb->setBlendSrc(RenderState::BlendFunc::SRC_ALPHA);
    if (useBlend)
    {
        material->m_flags |= RenderPass::AlphaBlend;
    }
    else
    {
        material->m_flags &= ~RenderPass::AlphaBlend;
    }
}

void Material::createDebugPass(const Technique::Ptr& technique, const std::string& defines)
{
    Shader::SourceList sourceList;
    sourceList.insert(std::make_pair(Shader::Type::Vertex, geomVisVert));
    sourceList.insert(std::make_pair(Shader::Type::Geometry, geomVisGeom));
    sourceList.insert(std::make_pair(Shader::Type::Fragment, geomVisFrag));
    auto visShader = Shader::createFromSource(sourceList, defines);
    assert(visShader);
    auto visPass = std::make_unique<Pass>("VisPass", technique.get());
    visPass->m_shader = visShader;
    visPass->m_flags |= RenderPass::Debug;
    technique->m_passes.push_back(std::move(visPass));
}

void Material::createUnlit(Material::Ptr& material, Technique::Ptr& technique, const std::string& path, ConfigObject* matProp)
{
    material->m_type = Type::Unlit;
    //set any state block parameters first
    //so material properties can override if necessary
    auto sb = StateBlock::create();
    sb->setDepthTest(true);
    sb->setCullFace(true);

    //check we have at least a diffuse property, and warn if not
    checkForDiffuse(matProp, sb, path);

    //create samplers for any textures found
    const auto& properties = matProp->getProperties();
    for (const auto& p : properties)
    {
        auto name = Util::String::toLower(p->getName());
        if (name == "diffuse")
        {
            createDiffuseSampler(material, p);
        }
        else if (name == "cull_face")
        {
            setCullFace(p, sb);
        }
        else if (name == "transparency")
        {
            setBlend(material, p, sb);
        }
    }

    auto sbRefraction = sb->clone();
    auto sbReflection = sb->clone();

    auto pass = std::make_unique<Pass>("UnlitPass0", technique.get());
    if (!pass->init("res/shaders/Unlit.vert", "res/shaders/Unlit.frag"))
    {
        Logger::Log("Failed creating Pass for " + path, Logger::Type::Warning);
    }
    pass->m_flags = material->m_flags;
    pass->setStateBlock(sb);
    technique->m_passes.push_back(std::move(pass));

    //add refraction pass
    std::string defines("#define CLIP_PLANE\n");
    pass = std::make_unique<Pass>("UnlitPass1", technique.get());
    if (!pass->init("res/shaders/Unlit.vert", "res/shaders/Unlit.frag", defines))
    {
        Logger::Log("Failed creating Pass 1 for " + path, Logger::Type::Warning);
    }
    material->m_flags |= RenderPass::Refraction;
    pass->m_flags = material->m_flags;
    pass->m_flags &= ~RenderPass::Standard;
    sbRefraction->setClip(true);
    pass->setStateBlock(sbRefraction);
    technique->m_passes.push_back(std::move(pass));

    //add reflection pass
    pass = std::make_unique<Pass>("UnlitPass2", technique.get());
    if (!pass->init("res/shaders/Unlit.vert", "res/shaders/Unlit.frag", defines + "#define REFLECTION\n"))
    {
        Logger::Log("Failed creating Pass 2 for " + path, Logger::Type::Warning);
    }
    material->m_flags |= RenderPass::Reflection;
    pass->m_flags = material->m_flags;
    pass->m_flags &= ~RenderPass::Standard;
    pass->m_flags &= ~RenderPass::Refraction;
    sbReflection->setClip(true);
    sbReflection->setFrontFace(Winding::CLOCKWISE);
    pass->setStateBlock(sbReflection);
    technique->m_passes.push_back(std::move(pass));
}

void Material::createVertexLit(Material::Ptr& material, Technique::Ptr& technique, const std::string& path, ConfigObject* matProp)
{
    material->m_type = Type::VertexLit;

    auto sb = StateBlock::create();
    sb->setDepthTest(true);
    sb->setCullFace(true);

    //check we have at least a diffuse property, and warn if not
    checkForDiffuse(matProp, sb, path);

    bool drawDebug = false;
    std::string defines;

    //create samplers for any textures found
    //and set any state block parameters
    const auto& properties = matProp->getProperties();
    for (const auto& p : properties)
    {
        auto name = Util::String::toLower(p->getName());
        if (name == "diffuse")
        {
            createDiffuseSampler(material, p);
        }
        else if (name == "normal")
        {
            createNormalSampler(material, p, defines);
        }
        else if (name == "mask")
        {
            createMaskSampler(material, p, defines);
        }
        else if (name == "cull_face")
        {
            setCullFace(p, sb);
        }
        else if (name == "debug")
        {
            drawDebug = p->valueAsBool();
        }
        else if (name == "skinning")
        {
            if (p->valueAsBool())
            {
                defines += "#define SKINNING\n";
            }
        }
        else if (name == "lightmapped")
        {
            if (p->valueAsBool())
            {
                defines += "#define LIGHTMAPPED\n";
            }
        }
        else if (name == "transparency")
        {
            setBlend(material, p, sb);
        }
    }

    auto sbRefraction = sb->clone();
    auto sbReflection = sb->clone();

    auto pass = std::make_unique<Pass>("VertexLitPass0", technique.get());
    if (!pass->init("res/shaders/VertexLit.vert", "res/shaders/VertexLit.frag", defines))
    {
        Logger::Log("Failed creating Pass 0 for " + path, Logger::Type::Warning);
    }
    pass->setStateBlock(sb);
    pass->m_flags = material->m_flags;
    technique->m_passes.push_back(std::move(pass));

    //add refraction pass
    defines += "#define CLIP_PLANE\n";
    pass = std::make_unique<Pass>("LightMappedPass1", technique.get());
    if (!pass->init("res/shaders/VertexLit.vert", "res/shaders/VertexLit.frag", defines))
    {
        Logger::Log("Failed creating Pass 1 for " + path, Logger::Type::Warning);
    }
    material->m_flags |= RenderPass::Refraction;
    pass->m_flags = material->m_flags;
    pass->m_flags &= ~RenderPass::Standard;
    sbRefraction->setClip(true);
    pass->setStateBlock(sbRefraction);
    technique->m_passes.push_back(std::move(pass));

    //add reflection pass
    defines += "#define REFLECTION\n";
    pass = std::make_unique<Pass>("VertexLitPass2", technique.get());
    if (!pass->init("res/shaders/VertexLit.vert", "res/shaders/VertexLit.frag", defines))
    {
        Logger::Log("Failed creating Pass 2 for " + path, Logger::Type::Warning);
    }
    material->m_flags |= RenderPass::Reflection;
    pass->m_flags = material->m_flags;
    pass->m_flags &= ~RenderPass::Standard;
    pass->m_flags &= ~RenderPass::Refraction;
    sbReflection->setClip(true);
    sbReflection->setFrontFace(Winding::CLOCKWISE);
    pass->setStateBlock(sbReflection);
    technique->m_passes.push_back(std::move(pass));


    if (drawDebug) //add a pass with debug shader
    {
        material->m_flags |= RenderPass::Debug;
        createDebugPass(technique, defines);
    }
}

void Material::createLightMapped(Ptr& material, Technique::Ptr& technique, const std::string& path, ConfigObject* matProp)
{
    material->m_type = Type::LightMapped;
    //set any state block parameters first
    //so material properties can override if necessary
    auto sb = StateBlock::create();
    sb->setDepthTest(true);
    sb->setCullFace(true);

    //check we have at least a diffuse property, and warn if not
    checkForDiffuse(matProp, sb, path);

    std::string defines;
    bool drawDebug = false;
    //create samplers for any textures found
    const auto& properties = matProp->getProperties();
    for (const auto& p : properties)
    {
        auto name = Util::String::toLower(p->getName());
        if (name == "diffuse")
        {
            createDiffuseSampler(material, p);
        }
        else if (name == "normal")
        {
            createNormalSampler(material, p, defines);
        }
        else if (name == "mask")
        {
            createMaskSampler(material, p, defines);
        }
        else if (name == "cull_face")
        {
            setCullFace(p, sb);
        }
        else if (name == "debug")
        {
            drawDebug = p->valueAsBool();
        }
        else if (name == "transparency")
        {
            setBlend(material, p, sb);
        }
    }

    auto refractSb = sb->clone(); //use this as basis for reflection pass

    auto pass = std::make_unique<Pass>("LightMappedPass0", technique.get());
    if (!pass->init("res/shaders/LightMapped.vert", "res/shaders/LightMapped.frag", defines))
    {
        Logger::Log("Failed creating Pass 0 for " + path, Logger::Type::Warning);
    }
    pass->m_flags = material->m_flags;
    pass->setStateBlock(sb);
    technique->m_passes.push_back(std::move(pass));

    //add refraction pass
    auto reflectSb = refractSb->clone();
    defines += "#define CLIP_PLANE\n";
    pass = std::make_unique<Pass>("LightMappedPass1", technique.get());
    if (!pass->init("res/shaders/LightMapped.vert", "res/shaders/LightMapped.frag", defines))
    {
        Logger::Log("Failed creating Pass 1 for " + path, Logger::Type::Warning);
    }
    material->m_flags |= RenderPass::Refraction;
    pass->m_flags = material->m_flags;
    pass->m_flags &= ~RenderPass::Standard;
    refractSb->setClip(true);
    pass->setStateBlock(refractSb);
    technique->m_passes.push_back(std::move(pass));

    //add pass for reflection
    defines += "#define REFLECTION\n";
    pass = std::make_unique<Pass>("LightMappedPass2", technique.get());
    if (!pass->init("res/shaders/LightMapped.vert", "res/shaders/LightMapped.frag", defines))
    {
        Logger::Log("Failed creating Pass 2 for " + path, Logger::Type::Warning);
    }
    material->m_flags |= RenderPass::Reflection;
    pass->m_flags = material->m_flags;
    pass->m_flags &= ~RenderPass::Standard;
    pass->m_flags &= ~RenderPass::Refraction;
    reflectSb->setClip(true);
    reflectSb->setFrontFace(Winding::CLOCKWISE);
    pass->setStateBlock(reflectSb);
    technique->m_passes.push_back(std::move(pass));

    if (drawDebug) //add a pass with debug shader
    {
        createDebugPass(technique, defines);
    }
}

void Material::createSkybox(Material::Ptr& material, Technique::Ptr& technique, const std::string& path, ConfigObject* matProp)
{
    material->m_type = Type::SkyBox;

    //invert cull face as we ought to be *inside*
    auto sb = StateBlock::create();
    sb->setCullFace(true);
    sb->setCullFaceSide(RenderState::CullFace::FRONT);
    checkForDiffuse(matProp, sb, path);

    //create samplers
    const auto& properties = matProp->getProperties();
    for (const auto& p : properties)
    {
        if (Util::String::toLower(p->getName()) == "diffuse")
        {
            auto ts = Texture::Sampler::create(Texture::createCubemap(p->valueAsString()));
            if (ts)
            {
                material->getProperty("u_cubeTexture")->setSampler(ts);
            }
            else
            {
                Logger::Log("Failed to create texture sampler for " + p->valueAsString(), Logger::Type::Error);
            }
        }
    }

    material->m_flags |= RenderPass::Standard | RenderPass::Reflection;

    auto sbClone = sb->clone();

    auto pass = std::make_unique<Pass>("SkyBoxPass0", technique.get());
    if (!pass->init("res/shaders/SkyBox.vert", "res/shaders/SkyBox.frag"))
    {
        Logger::Log("Failed creating Pass 0 for " + path, Logger::Type::Warning);
    }
    pass->setStateBlock(sb);
    pass->m_flags |= RenderPass::Standard;
    technique->m_passes.push_back(std::move(pass));

    pass = std::make_unique<Pass>("SkyboxPass1", technique.get());
    if (!pass->init("res/shaders/SkyBox.vert", "res/shaders/SkyBox.frag", "#define REFLECTION\n"))
    {
        Logger::Log("Failed creating Pass 1 for " + path, Logger::Type::Warning);
    }

    //sbClone->setClip(true);
    pass->m_flags |= RenderPass::Reflection;
    pass->setStateBlock(sbClone);
    technique->m_passes.push_back(std::move(pass));
}

void Material::createWater(Material::Ptr& material, Technique::Ptr& technique, const std::string& path, ConfigObject* matProp)
{
    material->m_type = Type::Water;

    auto sb = StateBlock::create();
    sb->setCullFace(true);
    sb->setDepthTest(true);
    sb->setBlend(true);
    sb->setBlendDest(RenderState::BlendFunc::ONE_MINUS_SRC_ALPHA);
    sb->setBlendSrc(RenderState::BlendFunc::SRC_ALPHA);

    //create samplers
    std::string defines;
    const auto& properties = matProp->getProperties();
    for (const auto& p : properties)
    {
        auto name = Util::String::toLower(p->getName());
        if (name == "normal")
        {
            createNormalSampler(material, p, defines);
            const auto& t = material->getProperty("u_normalTexture")->getSampler()->getTexture();
            material->addTag("dimensions", std::to_string(t->getHeight()) + "," + std::to_string(t->getHeight()));
        }
        else if (name == "lightmapped")
        {
            if (p->valueAsBool())
            {
                defines += "#define LIGHTMAPPED\n";
            }
        }
        else if (name == "tint")
        {
            material->getProperty("u_waterColour")->setValue(p->valueAsVec4());
        }
        else if (name == "animated")
        {
            if (p->valueAsBool())
            {
                defines += "#define ANIMATED\n";
            }
        }
        else if (name == "repeat")
        {
            material->getProperty("u_textureRepeat")->setValue(std::max(0.5f, p->valueAsFloat()));
        }
    }
    if (defines.empty())
    {
        //source bsp barfs if we don't have tex dimensions
        material->addTag("dimensions", std::to_string(2) + "," + std::to_string(2));
    }

    material->m_flags |= RenderPass::Standard;// | RenderPass::AlphaBlend; //assume we don't need to render this in reflection passes

    auto pass = std::make_unique<Pass>("WaterPass0", technique.get());
    if (!pass->init("res/shaders/Water.vert", "res/shaders/Water.frag", defines))
    {
        Logger::Log("Failed creating Pass 0 for " + path, Logger::Type::Warning);
    }
    pass->setStateBlock(sb);
    pass->m_flags = material->m_flags;
    technique->m_passes.push_back(std::move(pass));
}

void Material::createTerrain(Material::Ptr& material, Technique::Ptr& technique, const std::string& path, ConfigObject* matProp)
{
    material->m_type = Type::Terrain;

    auto sb = StateBlock::create();
    sb->setDepthTest(true);
    sb->setCullFace(true);

    std::string defines;

    //check for any layers - do this first so can prevent the default
    //diffuse texture being created if we have layers, and replace with solid colour
    std::vector<TerrainLayer> layers;
    const auto& objects = matProp->getObjects();
    for (const auto& o : objects)
    {
        if (Util::String::toLower(o->getName()) == "layer")
        {
            TerrainLayer layer;
            const auto& properties = o->getProperties();
            for (const auto& p : properties)
            {
                const auto name = Util::String::toLower(p->getName());
                if (name == "diffuse")
                {
                    layer.diffusePath = p->valueAsString();
                }
                else if (name == "normal")
                {
                    layer.normalPath = p->valueAsString();
                }
                else if (name == "mask")
                {
                    layer.maskPath = p->valueAsString();
                }
                else if (name == "repeat")
                {
                    layer.repeat = p->valueAsFloat();
                    if (layer.repeat <= 0) layer.repeat = 1.f;
                }
            }

            if (!layer.diffusePath.empty())
            {
                layers.push_back(layer);
            }
        }
        if (layers.size() == maxLayers) break;
    }

    bool bumped = false;
    if (layers.size() > 0) //create material with layers
    {
        //check blend map exists - else whhhyy?
        auto blend = matProp->findProperty("blend");
        Texture::Sampler::Ptr ts;
        if (blend)
        {
            ts = Texture::Sampler::create(blend->valueAsString());
            if (!ts) ts = Texture::Sampler::create(Texture::create("default"));  
        }
        else
        {
            //add fallback
            LOG("blend map parameter missing from terrain material file");
            ts = Texture::Sampler::create(Texture::create("default"));
        }
        material->getProperty("u_blendMap")->setSampler(ts);
        
        bumped = createLayerSamplers(material, defines, layers);
    }
    else //create material normally
    {
        //check we have at least a diffuse property, and warn if not
        checkForDiffuse(matProp, sb, path);
    }
    bool drawDebug = false;
    
    //create samplers for any textures found
    //and set any state block parameters
    const auto& properties = matProp->getProperties();
    for (const auto& p : properties)
    {
        auto name = Util::String::toLower(p->getName());

        if (name == "diffuse" && layers.empty()) //skip certain properties if we have layers
        {
            createDiffuseSampler(material, p);
        }
        else if (name == "normal" && layers.empty())
        {
            createNormalSampler(material, p, defines);
            bumped = true;
        }
        else if (name == "mask" && layers.empty())
        {
            createMaskSampler(material, p, defines);
        }
        else if (name == "cull_face")
        {
            setCullFace(p, sb);
        }
        else if (name == "debug")
        {
            drawDebug = p->valueAsBool();
        }
        else if (name == "flatshaded")
        {
            if (bumped)
            {
                Logger::Log("flat shading will not work with bump mapped terrains", Logger::Type::Warning);
            }
            else
            {
                defines += "#define FLATSHADE\n";
            }
        }
    }
    

    auto sbRefraction = sb->clone();
    auto sbReflection = sb->clone();

    auto pass = std::make_unique<Pass>("TerrainPass0", technique.get());
    if (!pass->init("res/shaders/Terrain.vert", "res/shaders/Terrain.frag", defines))
    {
        Logger::Log("Failed creating Pass 0 for " + path, Logger::Type::Warning);
    }
    pass->setStateBlock(sb);
    pass->m_flags = material->m_flags;
    technique->m_passes.push_back(std::move(pass));

    //add refraction pass
    defines += "#define CLIP_PLANE\n";
    pass = std::make_unique<Pass>("TerrainPass1", technique.get());
    if (!pass->init("res/shaders/Terrain.vert", "res/shaders/Terrain.frag", defines))
    {
        Logger::Log("Failed creating Pass 1 for " + path, Logger::Type::Warning);
    }
    material->m_flags |= RenderPass::Refraction;
    pass->m_flags = material->m_flags;
    pass->m_flags &= ~RenderPass::Standard;
    sbRefraction->setClip(true);
    pass->setStateBlock(sbRefraction);
    technique->m_passes.push_back(std::move(pass));

    //add reflection pass
    defines += "#define REFLECTION\n";
    pass = std::make_unique<Pass>("TerrainPass2", technique.get());
    if (!pass->init("res/shaders/Terrain.vert", "res/shaders/Terrain.frag", defines))
    {
        Logger::Log("Failed creating Pass 2 for " + path, Logger::Type::Warning);
    }
    material->m_flags |= RenderPass::Reflection;
    pass->m_flags = material->m_flags;
    pass->m_flags &= ~RenderPass::Standard;
    pass->m_flags &= ~RenderPass::Refraction;
    sbReflection->setClip(true);
    sbReflection->setFrontFace(Winding::CLOCKWISE);
    pass->setStateBlock(sbReflection);
    technique->m_passes.push_back(std::move(pass));


    if (drawDebug) //add a pass with debug shader
    {
        material->m_flags |= RenderPass::Debug;
        createDebugPass(technique, defines);
    }
}

bool Material::createLayerSamplers(Material::Ptr& material, std::string& defines, const std::vector<TerrainLayer>& layers)
{
    bool bumped = false;
    
    std::vector<Image::Ptr> diffuseImages;
    std::vector<Image::Ptr> normalImages;
    std::vector<Image::Ptr> maskImages;

    defines += "#define LAYER_COUNT " + std::to_string(layers.size()) + "\n";
    for (auto i = 0u; i < layers.size(); ++i)
    {
        defines += "#define LAYER_REPEAT" + std::to_string(i) + " " + std::to_string(layers[i].repeat) + "\n";

        //create images 
        diffuseImages.emplace_back(Image::create(layers[i].diffusePath));

        //normal maps
        if (!layers[i].normalPath.empty())
        {
            //check and align layers with no normal maps
            //so that they appear at the correct index
            if (normalImages.empty() && i > 0)
            {
                while (normalImages.size() < i)
                {
                    normalImages.push_back(nullptr);
                }
            }
            normalImages.emplace_back(Image::create(layers[i].normalPath));
        }
        else if (!normalImages.empty()) //we have an empty path which needs padding
        {
            normalImages.push_back(nullptr);
        }

        //mask maps
        if (!layers[i].maskPath.empty())
        {
            //check and align layers with no mask maps
            if (maskImages.empty() && i > 0)
            {
                while (maskImages.size() < i)
                {
                    maskImages.push_back(nullptr);
                }
            }
            maskImages.emplace_back(Image::create(layers[i].maskPath));
        }
        else if (!maskImages.empty())
        {
            maskImages.push_back(nullptr);
        }
    }
    //pad out vectors to size and assign to properties
    if (!normalImages.empty())
    {
        while (normalImages.size() < diffuseImages.size())
        {
            normalImages.push_back(nullptr);
        }

        bumped = true;
        defines += "#define BUMP\n";

        checkImageArray(normalImages, Colour(0.5f, 0.5f, 1.f));
        auto ts = Texture::Sampler::create(Texture::createArray(normalImages));
        ts->setWrap(Texture::Wrap::Repeat, Texture::Wrap::Repeat);
        ts->setFilter(Texture::Filter::LinearMipmapLinear, Texture::Filter::Linear);
        material->getProperty("u_layerNormalMaps")->setValue(ts);
    }

    if (!maskImages.empty())
    {
        while (maskImages.size() < diffuseImages.size())
        {
            maskImages.push_back(nullptr);
        }

        defines += "#define MASK\n";

        checkImageArray(maskImages, Colour(0.2f, 0.1f, 0.f));
        auto ts = Texture::Sampler::create(Texture::createArray(maskImages));
        ts->setWrap(Texture::Wrap::Repeat, Texture::Wrap::Repeat);
        ts->setFilter(Texture::Filter::LinearMipmapLinear, Texture::Filter::Linear);
        material->getProperty("u_layerMaskMaps")->setValue(ts);
    }
   
    checkImageArray(diffuseImages, Colour::magenta);

    auto ts = Texture::Sampler::create(Texture::createArray(diffuseImages));
    ts->setWrap(Texture::Wrap::Repeat, Texture::Wrap::Repeat);
    ts->setFilter(Texture::Filter::LinearMipmapLinear, Texture::Filter::Linear);
    material->getProperty("u_layerDiffuseMaps")->setValue(ts);

    return bumped;
}