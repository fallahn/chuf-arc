/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/FrameBuffer.hpp>
#include <chuf2/Util.hpp>
#include <chuf2/Image.hpp>
#include <chuf2/Texture.hpp>
#include <chuf2/Cache.hpp>
#include <chuf2/RenderState.hpp>

using namespace chuf;

namespace
{
    UInt16 maxRenderTextures = 0u;
    FrameBuffer::Ptr defaultFrameBuffer = nullptr;
    FrameBuffer* currentFrameBuffer = nullptr;
    Cache<FrameBuffer> fboCache;
}

FrameBuffer::FrameBuffer(const std::string& name, UInt16 width, UInt16 height, FrameBufferID fbo, const Key& key)
    : m_name            (name),
    m_fbo               (fbo),
    m_dst               (nullptr),
    m_renderTextures    (maxRenderTextures),
    m_clearFlags        (0)
{
    m_view.viewport.width = static_cast<float>(width);
    m_view.viewport.height = static_cast<float>(height);
}

FrameBuffer::~FrameBuffer()
{
    if (m_fbo)
        glCheck(glDeleteFramebuffers(1, &m_fbo));
}

//public
FrameBuffer* FrameBuffer::create(const std::string& name)
{
    assert(!name.empty());
    auto exists = fboCache.find(name);
    if (exists) return exists;
    return create(name, 0u, 0u);
}

FrameBuffer* FrameBuffer::create(const std::string& name, UInt16 width, UInt16 height)
{
    assert(!name.empty());
    auto exists = fboCache.find(name);
    if (exists) return exists;

    RenderTexture* rt = nullptr;
    if (width && height)
    {
        rt = RenderTexture::create(name, width, height);
        if (!rt)
        {
            Logger::Log("Failed to create render target for frame buffer " + name, Logger::Type::Error);
            return nullptr;
        }
    }

    GLuint fbo;
    glCheck(glGenFramebuffers(1, &fbo));
    auto fb = std::make_unique<FrameBuffer>(name, width, height, fbo, Key());

    if (rt)
    {
        fb->setRenderTexture(rt);
    }

    return fboCache.insert(name, fb);
}

FrameBuffer* FrameBuffer::getFrameBuffer(const std::string& name)
{
    return fboCache.find(name);
}

UInt16 FrameBuffer::getMaxRenderTextures()
{
    return maxRenderTextures;
}

FrameBuffer* FrameBuffer::bindDefault()
{
    glCheck(glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer->m_fbo));
    currentFrameBuffer = defaultFrameBuffer.get();

    return currentFrameBuffer;
}

FrameBuffer* FrameBuffer::getCurrent()
{
    return currentFrameBuffer;
}
//--------------------------------------

const std::string& FrameBuffer::getName() const
{
    return m_name;
}

UInt16 FrameBuffer::getWidth() const
{
    if (m_renderTextures.size() > 0u)
        return m_renderTextures[0]->getWidth();
    return 0u;
}

UInt16 FrameBuffer::getHeight() const
{
    if (m_renderTextures.size() > 0u)
        return m_renderTextures[0]->getHeight();
    return 0u;
}

View FrameBuffer::setView(const View& view)
{
    auto oldView = m_view;
    m_view = view;
    return oldView;
}

const View& FrameBuffer::getView() const
{
    return m_view;
}

void FrameBuffer::setRenderTexture(RenderTexture* rt, UInt16 index)
{
    assert(rt->getTexture());
    if (m_renderTextures.size() > index && m_renderTextures[index] == rt) return;

    //TODO if using a cubemap target we need to define which face to bind
    //which means explicitly stating a face here, rather than type
    setRenderTexture(rt, index, static_cast<GLenum>(rt->getTexture()->getType()));
}

RenderTexture* FrameBuffer::getRenderTexture(UInt16 index) const
{
    if (index < m_renderTextures.size()) return m_renderTextures[index];
    return nullptr;
}

UInt16 FrameBuffer::getRenderTextureCount() const
{
    return m_renderTextures.size();
}

void FrameBuffer::setDepthStencilTarget(DepthStencilTarget* dst)
{
    if (m_dst == dst) return;

    m_dst = dst;
    if (m_dst)
    {
        glCheck(glBindFramebuffer(GL_FRAMEBUFFER, m_fbo));

        //attach the dst to the fbo, and optionally set separate stencil buffer
        glCheck(glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_dst->m_depthID));
        m_clearFlags |= GL_DEPTH_BUFFER_BIT;
        if (m_dst->m_packed)
        {
            glCheck(glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_RENDERBUFFER, m_dst->m_depthID));
            m_clearFlags |= GL_STENCIL_BUFFER_BIT;
        }
        else if (m_dst->m_format == DepthStencilTarget::Format::DepthStencil)
        {
            glCheck(glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_RENDERBUFFER, m_dst->m_stencilID));
            m_clearFlags |= GL_STENCIL_BUFFER_BIT;
        }

        GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
        if (status != GL_FRAMEBUFFER_COMPLETE)
        {
            Logger::Log("Error attaching depth stencil target to fbo " + m_name, Logger::Type::Warning);
        }

        glCheck(glBindFramebuffer(GL_FRAMEBUFFER, currentFrameBuffer->m_fbo));
    }
}

DepthStencilTarget* FrameBuffer::getDepthStencilTarget() const
{
    return m_dst;
}

bool FrameBuffer::defaultBuffer() const
{
    return (this == defaultFrameBuffer.get());
}

FrameBuffer* FrameBuffer::bind()
{
    glCheck(glBindFramebuffer(GL_FRAMEBUFFER, m_fbo));
    //TODO we should really be applying out local view here too

    auto prevBuffer = currentFrameBuffer;
    currentFrameBuffer = this;

    return prevBuffer;
}

void FrameBuffer::clear(Colour colour)
{
    FrameBuffer* old = nullptr;
    if (currentFrameBuffer->m_fbo != m_fbo)
    {
        old = bind();
    }
    
    if (m_clearFlags & GL_DEPTH_BUFFER_BIT)
    {
        //restores depth writing if been switched off
        //by a previous state
        RenderState::StateBlock::enableDepthWrite();
        //TODO 
        glClearDepth(1.f);
    }

    if (m_clearFlags & GL_STENCIL_BUFFER_BIT)
    {
        //TODO 
        glClearStencil(0);
    }

    if (m_clearFlags & GL_COLOR_BUFFER_BIT)
        glClearColor(colour.r, colour.g, colour.b, colour.a);

    glClear(m_clearFlags);

    if (old)
    {
        old->bind();
    }
}

//private
void FrameBuffer::start()
{
    //initialises some base properties of the FBO class
    //such as the default frame buffer
    GLint fbo;
    glCheck(glGetIntegerv(GL_FRAMEBUFFER_BINDING, &fbo));
    defaultFrameBuffer = std::make_unique<FrameBuffer>("defaultFrameBuffer", 0u, 0u, static_cast<FrameBufferID>(fbo), Key());
    currentFrameBuffer = defaultFrameBuffer.get();

    //query the hardware for max colour attachments.
    //if implementing GLES at some point this will be fixed at 1 (for GLES2.x)
    GLint count;
    glCheck(glGetIntegerv(GL_MAX_COLOR_ATTACHMENTS, &count));
    maxRenderTextures = std::max(1, count);
}

void FrameBuffer::finish()
{
    //we can't rely on RAII here, so we force destruction
    //of anonymous variables
    defaultFrameBuffer.reset();
}

void FrameBuffer::setRenderTexture(RenderTexture* rt, UInt16 index, GLenum target)
{
    if (target == GL_TEXTURE_CUBE_MAP)
    {
        Logger::Log("Attaching cubemap faces to frame buffers not yet implemented. Attachment failed.", Logger::Type::Warning);
        return;
    }
    
    assert(index < maxRenderTextures);
    m_renderTextures[index] = rt;

    if (rt)
    {
        glCheck(glBindFramebuffer(GL_FRAMEBUFFER, m_fbo));
        GLenum attachPoint = GL_COLOR_ATTACHMENT0 + index;
        glCheck(glFramebufferTexture2D(GL_FRAMEBUFFER, attachPoint, target, m_renderTextures[index]->getTexture()->getHandle(), 0));
        GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
        if (status != GL_FRAMEBUFFER_COMPLETE)
        {
            Logger::Log("Error attaching texture " + rt->getTexture()->getPath() + " to fbo", Logger::Type::Warning);
        }

        glCheck(glBindFramebuffer(GL_FRAMEBUFFER, currentFrameBuffer->m_fbo));
    }
    m_clearFlags |= GL_COLOR_BUFFER_BIT;
}