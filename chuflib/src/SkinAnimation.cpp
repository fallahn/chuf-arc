/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/Skin.hpp>

#include <cassert>


using namespace chuf;

namespace
{
    //swaps from Z up to Y up
    const glm::mat4 swapMat(1.f, 0.f, 0.f, 0.f,
                            0.f, 0.f, 1.f, 0.f,
                            0.f, 1.f, 0.f, 0.f,
                            0.f, 0.f, 0.f, 1.f);
}

Skin::Animation::Animation(UInt32 frameCount, UInt32 startFrame, float frameRate, const std::string& name, bool looped)
    : m_frameCount      (frameCount),
    m_startFrame        (startFrame),
    m_lastFrame         (m_startFrame + frameCount),
    m_frameRate         (frameRate),
    m_name              (name),
    m_looped            (looped),
    m_parentSkin        (nullptr),
    m_currentTime       (0.f),
    m_frameTime         (1.f / frameRate),
    m_currentFrameIndex (0u),
    m_nextFrameIndex    (0u)
{

}

//public
const glm::mat4* Skin::Animation::getCurrentFrame() const
{
    return m_currentFrame.data();
}

void Skin::Animation::setSkin(Skin* skin)
{
    m_parentSkin = skin;
    buildFirstFrame();
}

void Skin::Animation::update(float dt)
{
    m_currentTime += dt;

    if (m_currentTime >= m_frameTime && (m_currentFrameIndex < m_lastFrame || m_looped))
    {
        m_currentFrameIndex++; //TODO account for start frame offset and loop to beginning
        if (m_currentFrameIndex == m_lastFrame) m_currentFrameIndex = 0u;
        m_nextFrameIndex = (m_currentFrameIndex + 1u) % m_frameCount;

        m_currentTime = 0.f;
    }
    interpolate(m_parentSkin->m_frames[m_currentFrameIndex], m_parentSkin->m_frames[m_nextFrameIndex], m_currentTime / m_frameTime);
}

//private
void Skin::Animation::buildFirstFrame()
{
    assert(m_parentSkin);
    UInt32 size = m_parentSkin->m_frames[m_startFrame].size();
    m_currentFrame.resize(size);
    
    for (auto i = 0; i < size; ++i)
    {
        const auto& mat = m_parentSkin->m_frames[m_startFrame][i];
        const auto& boneIndices = m_parentSkin->m_boneIndices;
        if (boneIndices[i] >= 0)
        {
            m_currentFrame[i] = m_currentFrame[boneIndices[i]] * mat;
        }
        else
        {
            m_currentFrame[i] = mat;
        }
    }
}

void Skin::Animation::interpolate(const Frame& a, const Frame& b, float time)
{
    UInt32 size = m_parentSkin->m_frames[m_startFrame].size();
    const auto& boneIndices = m_parentSkin->m_boneIndices;
    for (auto i = 0; i < size; ++i)
    {
        glm::mat4 mat(glm::mix(a[i], b[i], time));
        if (boneIndices[i] >= 0)
        {
            m_currentFrame[i] = m_currentFrame[boneIndices[i]] * mat;
        }
        else
        {
            m_currentFrame[i] = mat;
        }
    }

    //for (auto& m : m_currentFrame)
    //    m = swapMat *m;
}