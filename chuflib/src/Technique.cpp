/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/Material.hpp>
#include <chuf2/Technique.hpp>
#include <chuf2/Pass.hpp>
#include <chuf2/Node.hpp>

using namespace chuf;

Technique::Technique(const std::string& name, const Material::Ptr& material)
    : m_name	(name),
    m_material	(material)
{
    RenderState::m_parent = material.get();
}

//public
const std::string& Technique::getName() const
{
    return m_name;
}

UInt32 Technique::getPassCount() const
{
    return m_passes.size();
}

Pass* Technique::getPass(const std::string& name) const
{
    for (const auto& p : m_passes)
        if (p->getName() == name) return p.get();

    return nullptr;
}

Pass* Technique::getPass(UInt32 index)
{
    assert(index < m_passes.size());
    return m_passes[index].get();
}

void Technique::bindNode(Node* node)
{
    RenderState::bindNode(node);
    for (auto& p : m_passes)
        p->bindNode(node);
}

//private