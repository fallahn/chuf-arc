/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/VertexLayout.hpp>

#include <cassert>

using namespace chuf;

VertexLayout::VertexLayout(const std::vector<Element>& elements)
    : m_vertSize(0u)
{
    //loop over elements so we can pick out size as we go
    for (const auto& e : elements)
    {
        m_vertSize += sizeof(float) * e.size;
        m_elements.push_back(e);
    }
}

//public
const VertexLayout::Element& VertexLayout::getElement(UInt32 index) const
{
    assert(index < m_elements.size());
    return m_elements[index];
}

UInt32 VertexLayout::getElementCount() const
{
    return m_elements.size();
}

UInt32 VertexLayout::getVertexSize() const
{
    return m_vertSize;
}

Int8 VertexLayout::getElementIndex(VertexLayout::Type type)const
{
    for (auto i = 0; i < m_elements.size(); ++i)
    {
        if (m_elements[i].type == type) return i;
    }
    return -1;
}

Int32 VertexLayout::getElementOffset(VertexLayout::Type type) const
{
    auto offset = 0u;
    for (auto i = 0u; i < m_elements.size(); ++i)
    {
        if (m_elements[i].type == type) return offset;
        offset += m_elements[i].size;
    }
    return -1;
}

bool VertexLayout::operator == (const VertexLayout& v) const
{
    if (m_elements.size() != v.m_elements.size()) return false;

    for (auto i = 0u; i < m_elements.size(); ++i)
    {
        if (m_elements[i] != v.m_elements[i]) return false;
    }
    return true;
}

bool VertexLayout::operator != (const VertexLayout& v) const
{
    return !(*this == v);
}

//---------------------------------------------------------//

VertexLayout::Element::Element()
    : type	(Type::POSITION),
    size	(0u){}

VertexLayout::Element::Element(Type t, UInt32 s)
    : type	(t),
    size	(s){}

bool VertexLayout::Element::operator == (const VertexLayout::Element& e)const
{
    return (type == e.type && size == e.size);
}

bool VertexLayout::Element::operator != (const VertexLayout::Element& e) const
{
    return !(*this == e);
}