/*********************************************************************
Matt Marchant 2015 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/Taggable.hpp>
#include <chuf2/Log.hpp>

using namespace chuf;

void Taggable::addTag(const std::string& tag, const std::string& value)
{
    if (m_tags.find(tag) == m_tags.end())
    {
        m_tags.insert(std::make_pair(tag, value));
    }
    else
    {
        m_tags[tag] = value;
    }
}

void Taggable::removeTag(const std::string& tag)
{
    auto result = m_tags.find(tag);
    if (result != m_tags.end())
    {
        m_tags.erase(result);
    }
}

bool Taggable::hasTag(const std::string& tag) const
{
    return (m_tags.find(tag) != m_tags.end());
}

const std::string& Taggable::getTag(const std::string& tag)
{ 
    if (m_tags.find(tag) == m_tags.end())
    {
        m_tags.insert(std::make_pair(tag, "null"));
    }
    return m_tags[tag];
}

glm::vec2 Taggable::getTagVec2(const std::string& tag) const
{
    glm::vec2 retval;
    auto result = m_tags.find(tag);
    if (result != m_tags.end())
    {
        auto values = toFloatArray(result->second);
        for (auto i = 0u; i < values.size() && i < 3; ++i)
            retval[i] = values[i];
    }
    return retval;
}

glm::vec3 Taggable::getTagVec3(const std::string& tag) const
{
    glm::vec3 retval;
    auto result = m_tags.find(tag);
    if (result != m_tags.end())
    {
        auto values = toFloatArray(result->second);
        for (auto i = 0u; i < values.size() && i < 3; ++i)
            retval[i] = values[i];
    }
    return retval;
}

float Taggable::getTagFloat(const std::string& tag) const
{
    float retVal = 0.f;
    auto result = m_tags.find(tag);
    if (result != m_tags.end())
    {
        try
        {
            retVal = std::stof(result->second);
        }
        catch (...)
        {
            Logger::Log("Failed to convert value for " + tag + " to float", Logger::Type::Warning);
        }
    }
    return retVal;
}

Int32 Taggable::getTagInt(const std::string& tag) const
{
    Int32 retVal = 0;
    auto result = m_tags.find(tag);
    if (result != m_tags.end())
    {
        try
        {
            retVal = std::stoi(result->second);
        }
        catch (...)
        {
            Logger::Log("Failed to convert value for " + tag + " to integer", Logger::Type::Warning);
        }
    }
    return retVal;
}

//private
std::vector<float> Taggable::toFloatArray(const std::string& value) const
{
    assert(!value.empty());

    std::vector<float> retval;
    auto start = 0u;
    auto next = value.find_first_of(',');
    while (next != std::string::npos && start < value.length())
    {
        try
        {
            retval.push_back(std::stof(value.substr(start, next)));
        }
        catch (...)
        {
            retval.push_back(0.f);
        }
        start = ++next;
        next = value.find_first_of(',', start);
        if (next > value.length()) next = value.length();
    }
    return retval;
}