/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/Reports.hpp>

#include <cassert>
#include <algorithm>

namespace
{
    const std::string interweebl(": ");
}

using namespace chuf;

StatsReporter::StatsReporter()
    : m_rebuildString(true)
{

}

//public
void StatsReporter::report(const std::string& name, const std::string& value)
{
    assert(!name.empty() && !value.empty());
    m_data[name] = value;
    m_rebuildString = true;
}

void StatsReporter::remove(const std::string& name)
{
    m_data.erase(name);
    m_rebuildString = true;
}

const std::string& StatsReporter::getString()
{
    if (m_rebuildString)
    {
        m_rebuildString = false;

        m_string.clear();
        for (const auto& p : m_data)
        {
            m_string.append(p.first);
            m_string.append(interweebl);
            m_string.append(p.second);
            m_string.append("\n");
        }
    }

    return m_string;
}

void StatsReporter::clear()
{
    m_data.clear();
}

StatsReporter StatsReporter::reporter;