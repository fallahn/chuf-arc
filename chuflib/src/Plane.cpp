/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/Plane.hpp>
#include <chuf2/BoundingBox.hpp>
#include <chuf2/BoundingSphere.hpp>
#include <chuf2/Frustum.hpp>

using namespace chuf;

Plane::Plane()
    : m_normal	    (0.f, 1.f, 0.f),
    m_distance	    (0),
    m_fourComponent (m_normal, m_distance)
{}

Plane::Plane(const glm::vec3& normal, float distance)
    : m_normal	    (normal),
    m_distance	    (distance),
    m_fourComponent (normal, distance)
{
    normalise();
}

Plane& Plane::operator*=(const glm::mat4& matrix)
{
    transform(matrix);
    return *this;
}

const Plane operator * (const glm::mat4& matrix, const Plane& plane)
{
    Plane p(plane);
    p.transform(matrix);
    return p;
}

//public
const glm::vec3& Plane::getNormal() const
{
    return m_normal;
}

void Plane::setNormal(const glm::vec3& normal)
{
    m_normal = normal;
    m_fourComponent = glm::vec4(normal, m_distance);
    normalise();
}

float Plane::getDistance() const
{
    return m_distance;
}

void Plane::setDistance(float distance)
{
    m_distance = distance;
    m_fourComponent = glm::vec4(m_normal, distance);
    normalise();
}

float Plane::distance(const glm::vec3& point) const
{
    return glm::dot(m_normal, point) + m_distance;
}

glm::vec3 Plane::intersection(const Plane& p1, const Plane& p2, const Plane& p3)
{
    //calculate the determinant of the matrix (i.e | n1 n2 n3 |).
    float det = p1.m_normal.x * (p2.m_normal.y * p3.m_normal.z -
        p2.m_normal.z * p3.m_normal.y) - p2.m_normal.x *(p1.m_normal.y * p3.m_normal.z -
        p1.m_normal.z * p3.m_normal.y) + p3.m_normal.x * (p1.m_normal.y * p2.m_normal.z - p1.m_normal.z * p2.m_normal.y);
    
    //if the determinant is zero, then the planes do not all intersect.
    if (fabs(det) <= 0.000001f)	return glm::vec3();

    //create 3 points, one on each plane.
    //(we just pick the point on the plane directly along its normal from the origin).
    float p1x = -p1.m_normal.x * p1.m_distance;
    float p1y = -p1.m_normal.y * p1.m_distance;
    float p1z = -p1.m_normal.z * p1.m_distance;
    float p2x = -p2.m_normal.x * p2.m_distance;
    float p2y = -p2.m_normal.y * p2.m_distance;
    float p2z = -p2.m_normal.z * p2.m_distance;
    float p3x = -p3.m_normal.x * p3.m_distance;
    float p3y = -p3.m_normal.y * p3.m_distance;
    float p3z = -p3.m_normal.z * p3.m_distance;

    //calculate the cross products of the normals.
    float c1x = (p2.m_normal.y * p3.m_normal.z) - (p2.m_normal.z * p3.m_normal.y);
    float c1y = (p2.m_normal.z * p3.m_normal.x) - (p2.m_normal.x * p3.m_normal.z);
    float c1z = (p2.m_normal.x * p3.m_normal.y) - (p2.m_normal.y * p3.m_normal.x);
    float c2x = (p3.m_normal.y * p1.m_normal.z) - (p3.m_normal.z * p1.m_normal.y);
    float c2y = (p3.m_normal.z * p1.m_normal.x) - (p3.m_normal.x * p1.m_normal.z);
    float c2z = (p3.m_normal.x * p1.m_normal.y) - (p3.m_normal.y * p1.m_normal.x);
    float c3x = (p1.m_normal.y * p2.m_normal.z) - (p1.m_normal.z * p2.m_normal.y);
    float c3y = (p1.m_normal.z * p2.m_normal.x) - (p1.m_normal.x * p2.m_normal.z);
    float c3z = (p1.m_normal.x * p2.m_normal.y) - (p1.m_normal.y * p2.m_normal.x);

    //calculate the point of intersection using the formula:
    //x = (| n1 n2 n3 |)^-1 * [(x1 * n1)(n2 x n3) + (x2 * n2)(n3 x n1) + (x3 * n3)(n1 x n2)]
    float s1 = p1x * p1.m_normal.x + p1y * p1.m_normal.y + p1z * p1.m_normal.z;
    float s2 = p2x * p2.m_normal.x + p2y * p2.m_normal.y + p2z * p2.m_normal.z;
    float s3 = p3x * p3.m_normal.x + p3y * p3.m_normal.y + p3z * p3.m_normal.z;
    float detI = 1.f / det;

    return glm::vec3((s1 * c1x + s2 * c2x + s3 * c3x) * detI,
                    (s1 * c1y + s2 * c2y + s3 * c3y) * detI,
                    (s1 * c1z + s2 * c2z + s3 * c3z) * detI);
}

bool Plane::parallel(const Plane& p) const
{
    return (m_normal.y * p.m_normal.z) - (m_normal.z * p.m_normal.y) == 0.f &&
            (m_normal.z * p.m_normal.x) - (m_normal.x * p.m_normal.z) == 0.f &&
            (m_normal.x * p.m_normal.y) - (m_normal.y * p.m_normal.x) == 0.f;
}

void Plane::set(const glm::vec3& normal, float distance)
{
    m_normal = normal;
    m_distance = distance;
    m_fourComponent = glm::vec4(normal, distance);
    normalise();
}

void Plane::set(const glm::vec4& v)
{
    set(glm::vec3(v), v.w);
}

void Plane::set(const Plane& plane)
{
    m_normal = plane.m_normal;
    m_distance = plane.m_distance;
    m_fourComponent = plane.m_fourComponent;
}

void Plane::transform(const glm::mat4& matrix)
{
    glm::mat4 inverted = glm::inverse(matrix);

    //treat the plane as a four-tuple and multiply by the inverse transpose of the matrix to get the transformed plane.
    //then we normalise the plane by dividing both the normal and the distance by the length of the normal.
    float nx = m_normal.x * inverted[0][0] + m_normal.y * inverted[0][1] + m_normal.z * inverted[0][2] + m_distance * inverted[0][3];
    float ny = m_normal.x * inverted[1][0] + m_normal.y * inverted[1][1] + m_normal.z * inverted[1][2] + m_distance * inverted[1][3];
    float nz = m_normal.x * inverted[2][0] + m_normal.y * inverted[2][1] + m_normal.z * inverted[2][2] + m_distance * inverted[2][3];
    float  d = m_normal.x * inverted[3][0] + m_normal.y * inverted[3][1] + m_normal.z * inverted[3][2] + m_distance * inverted[3][3];
    
    float divisor = std::sqrt(nx * nx + ny * ny + nz * nz);
    float factor = 1.f / divisor;
    m_normal.x = nx * factor;
    m_normal.y = ny * factor;
    m_normal.z = nz * factor;
    m_distance = d * factor;

    m_fourComponent = glm::vec4(m_normal, m_distance);
}

void Plane::normalise()
{
    const float factor = 1.f / std::sqrtf(m_normal.x * m_normal.x + m_normal.y * m_normal.y + m_normal.z * m_normal.z);
    m_normal *= factor;
    m_distance *= factor;

    m_fourComponent = glm::vec4(m_normal, m_distance);
}

void Plane::invert()
{
    m_normal = -m_normal;
    m_distance = -m_distance;
    m_fourComponent = glm::vec4(m_normal, m_distance);
}

Plane::Intersection Plane::intersects(const BoundingBox& box) const
{
    return box.intersects(*this);
}

Plane::Intersection Plane::intersects(const BoundingSphere& sphere) const
{
    return sphere.intersects(*this);
}

Plane::Intersection Plane::intersects(const Plane& plane) const
{
    if (!parallel(plane)
        ||(m_normal.x == plane.m_normal.x * -plane.m_distance
        && m_normal.y == plane.m_normal.y * -plane.m_distance
        && m_normal.z == plane.m_normal.z * -plane.m_distance))
    {
        return Intersection::Intersecting;
    }

    glm::vec3 point(plane.m_normal * plane.m_distance);

    return (distance(point) < 0.f) ? Intersection::Back : Intersection::Front;
}

Plane::Intersection Plane::intersects(const Frustum& frustum) const
{
    const auto corners = frustum.getCorners();
    const auto dist = distance(corners[0]);

    if (dist > 0.f)
    {
        for (auto i = 1u; i < corners.size(); ++i)
        {
            if (distance(corners[i]) <= 0.f)
            {
                return Plane::Intersection::Intersecting;
            }
            return Plane::Intersection::Front;
        }
    }
    else if (dist < 0.f)
    {
        for (auto i = 1u; i < corners.size(); ++i)
        {
            if (distance(corners[i]) >= 0.f)
            {
                return Plane::Intersection::Intersecting;
            }
            return Plane::Intersection::Back;
        }
    }
    return Plane::Intersection::Intersecting;
}