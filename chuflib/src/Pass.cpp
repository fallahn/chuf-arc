/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/Pass.hpp>
#include <chuf2/Technique.hpp>
#include <chuf2/Log.hpp>

#include <iostream>

using namespace chuf;

Pass::Pass(const std::string& name, Technique* technique)
    : m_name			(name),
    m_technique			(technique),
    m_shader			(nullptr),
    m_vertAttribBinding	(nullptr),
    m_flags             (0u)
{
    RenderState::m_parent = dynamic_cast<RenderState*>(technique);
}

//public
const std::string& Pass::getName() const
{
    return m_name;
}

Shader* Pass::getShader()
{
    return m_shader;
}

void Pass::setVertexAttribBinding(VertexAttribBinding* vb)
{
    m_vertAttribBinding = vb;
}

VertexAttribBinding* Pass::getVertexAttribBinding() const
{
    return m_vertAttribBinding;
}

bool Pass::hasFlag(UInt32 flags) const
{
    return ((m_flags & flags) != 0);
}

UInt32 Pass::flags() const
{
    return m_flags;
}

void Pass::bind()
{
    assert(m_shader);
    m_shader->bind();

    RenderState::bind(*this);

    if (m_vertAttribBinding) m_vertAttribBinding->bind();
}

void Pass::unbind()
{
    if (m_vertAttribBinding) m_vertAttribBinding->unbind();
}

//private
bool Pass::init(const std::string& vertShader, const std::string& fragShader, const std::string& defines)
{
    m_shader = Shader::create(vertShader, fragShader, defines);
    if (!m_shader)
    {
        Logger::Log("Failed to create Shader for Pass", Logger::Type::Error);
        return false;
    }
    return true;
}

bool Pass::initFromSource(const std::string& vertShader, const std::string& fragShader, const std::string& defines)
{
    Shader::SourceList sourceList;
    sourceList.insert(std::make_pair(Shader::Type::Vertex, std::move(vertShader)));
    sourceList.insert(std::make_pair(Shader::Type::Fragment, std::move(fragShader)));

    m_shader = Shader::createFromSource(sourceList, defines);
    if (!m_shader)
    {
        Logger::Log("Failed to create Shader for Pass", Logger::Type::Error);
        return false;
    }
    return true;
}

