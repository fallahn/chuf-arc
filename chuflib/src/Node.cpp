/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/
#include <chuf2/Node.hpp>
#include <chuf2/Scene.hpp>
#include <chuf2/Model.hpp>
#include <chuf2/Light.hpp>
#include <chuf2/Log.hpp>
#include <chuf2/Util.hpp>
#include <chuf2/Sprite.hpp>
#include <chuf2/SpriteBatch.hpp>
#include <chuf2/loaders/Bsp.hpp>
#include <chuf2/Terrain.hpp>

using namespace chuf;

namespace
{
    const std::string emptyString;

    const glm::mat4 identityMat;
    glm::mat4 worldViewMat;
    glm::mat4 reflectedWorldViewMat;
    glm::mat4 invTransWorldMat;
    glm::mat4 invTransWorldViewMat;
    glm::mat4 worldViewProjectionMat;
    glm::mat4 reflectionWorldViewProjectionMat;
}


Node::Node(const std::string& name, const Key& key)
    : m_name			(name),
    m_parentIndex		(-1),
    m_active			(false),
    m_dirtybits			(DirtyBits::ALL),
    m_hierarchyChanged	(true),
    m_scene				(nullptr),
    m_destroyed         (false)
{}

Node::~Node()
{
    removeAllChildren();

    //LOG("Successfully Destroyed Node " + m_name);
}

//public
Node::Ptr Node::create(const std::string& name)
{
    return std::make_unique<Node>(name, Key());
}

const std::string& Node::getName() const
{
    return m_name;
}

void Node::addChild(Node::Ptr& child)
{
    if (child->m_parent == this) return;

    /*
    the only way to get a node ptr is to have created it, or
    already removed it from another hierachy - so we don't need to check here
    */

    child->m_parentIndex = m_children.size();
    child->m_parent = this;
    child->m_scene = m_scene;
    m_children.push_back(std::move(child));

    if (m_hierarchyChanged)
    {
        hierarchyChanged();
    }
}

Node::Ptr Node::removeChild(Node* child)
{
    child->remove();

    auto result = std::find_if(m_children.begin(), m_children.end(), [&](Ptr& p){return (p.get() == child); });
    assert(result != m_children.end());

    Ptr p = std::move(*result);
    p->m_parent = nullptr;
    p->m_parentIndex = -1;
    m_children.erase(result);
    return p;
}

void Node::removeAllChildren()
{
    m_hierarchyChanged = false;

    while (m_children.size())
        removeChild(m_children[0].get());

    m_hierarchyChanged = true;
    hierarchyChanged();
}

const std::vector<Node::Ptr>& Node::getChildren() const
{
    return m_children;
}

Node& Node::getParent()
{
    return *m_parent;
}

void Node::setActive(bool active)
{
    m_active = active;
}

bool Node::active() const
{
    return m_active;
}

bool Node::activeInHierarchy() const
{
    if (!m_active) return false;

    auto node = m_parent;
    while (node)
    {
        if (!node->m_active)
            return false;
        node = node->m_parent;
    }
    return true;
}

UInt32 Node::getChildCount() const
{
    return m_children.size();
}

Node* Node::findNode(const std::string& name, bool recursive) const
{
    for (const auto& c : m_children)
        if (c->m_name == name)
            return c.get();

    if (recursive)
    {
        for (const auto& c : m_children)
        {
            auto result = c->findNode(name, true);
            if (result) return result;
        }
    }
    return nullptr;
}

UInt32 Node::findNodes(const std::string& name, std::vector<Node*>& dest, bool recursive) const
{
    auto count = 0u;
    for (const auto& c : m_children)
    {
        if (c->m_name == name)
        {
            dest.push_back(c.get());
            count++;
        }

        if (recursive)
        {
            count += c->findNodes(name, dest, true);
        }
    }

    return count;
}

Node& Node::getRootNode()
{
    auto n = this;
    while (n->m_parent)
    {
        n = n->m_parent;
    }
    return *n;
}

Node* Node::getFirstChild() const
{
    if (m_children.size()) return m_children[0].get();
    return nullptr;
}

const BoundingSphere& Node::getBoundingSphere()
{
    if (m_dirtybits & BOUNDS)
    {
        m_dirtybits &= ~BOUNDS;

        const auto& worldMat = getWorldMatrix();
        bool empty = true;

        auto model = getComponent<Model>();
        if (model && model->getMesh())
        {
            m_boundingSphere.set(model->getMesh()->getBoundingSphere());
            empty = false;
        }
        auto light = getComponent<Light>();
        if (light && light->getType() == Light::Type::Point)
        {
            if (empty)
            {
                m_boundingSphere.set(glm::vec3(), light->getRange());
                empty = false;
            }
            else
            {
                m_boundingSphere.merge({ glm::vec3(), light->getRange() });
            }
        }

        //TODO bounds of other attachments, such as audio range
        if (empty)
        {
            const auto bsPos = m_boundingSphere.getCentre();
            auto pos = worldMat * glm::vec4(bsPos.x, bsPos.y, bsPos.z, 1.f);
            m_boundingSphere.set({ pos.x, pos.y, pos.z }, 0.f);
        }
        else
        {
            m_boundingSphere.transform(worldMat);
        }

        //merge bounds of child nodes
        for (const auto& c : m_children)
        {
            const auto& cs = c->getBoundingSphere();
            if (!cs.empty())
            {
                if (empty)
                {
                    m_boundingSphere.set(cs);
                }
                else
                {
                    m_boundingSphere.merge(cs);
                }
            }
        }
    }
    return m_boundingSphere;
}

const BoundingBox& Node::getBoundingBox()
{
    if (m_dirtybits & BOUNDS)
    {
        //recalc box
        m_dirtybits &= ~BOUNDS;

        const auto& worldMat = getWorldMatrix();
        bool empty = true;

        auto model = getComponent<Model>();
        if (model && model->getMesh())
        {
            m_boundingBox.set(model->getMesh()->getBoundingBox());
            empty = false;
        }
        //if (m_light && m_light->getType() == Light::Type::Point)
        //{
        //    if (empty)
        //    {
        //        m_boundingBox.set(glm::vec3(), m_light->getRange());
        //        empty = false;
        //    }
        //    else
        //    {
        //        m_boundingSphere.merge({ glm::vec3(), m_light->getRange() });
        //    }
        //}

        //TODO bounds of other attachments, such as audio range
        if (empty)
        {
            /*const auto bsPos = m_boundingSphere.getCentre();
            auto pos = worldMat * glm::vec4(bsPos.x, bsPos.y, bsPos.z, 1.f);
            m_boundingSphere.set({ pos.x, pos.y, pos.z }, 0.f);*/
        }
        else
        {
            m_boundingBox.transform(worldMat);
        }

        //merge bounds of child nodes
        for (const auto& c : m_children)
        {
            const auto& cb = c->getBoundingBox();
            if (!cb.empty())
            {
                if (empty)
                {
                    m_boundingBox.set(cb);
                }
                else
                {
                    m_boundingBox.merge(cb);
                }
            }
        }
    }

    return m_boundingBox;
}

const glm::mat4& Node::getWorldMatrix()
{
    if (m_dirtybits & WORLD)
    {
        m_dirtybits &= ~WORLD;

        auto parent = m_parent;
        if (parent)
        {
            m_worldMatrix = parent->getWorldMatrix() * getMatrix();
        }
        else
        {
            m_worldMatrix = getMatrix();
        }

        //because getMatrix() updates the current matrix, we need to 
        //make sure all child nodes are up to date too
        for (auto& c : m_children)
        {
            c->getWorldMatrix();
        }
    }

    return m_worldMatrix;
}

const glm::mat4& Node::getWorldViewMatrix()
{
    worldViewMat = getViewMatrix() * getWorldMatrix();
    return worldViewMat;
}

const glm::mat4& Node::getReflectionWorldViewMatrix()
{
    reflectedWorldViewMat = getReflectionViewMatrix() * getWorldMatrix();
    return reflectedWorldViewMat;
}

const glm::mat4& Node::getInverseTransposeWorldMatrix()
{
    invTransWorldMat = glm::transpose(glm::inverse(getWorldMatrix()));
    return invTransWorldMat;
}

const glm::mat4& Node::getInverseTransposeWorldViewMatrix()
{
    invTransWorldViewMat = glm::inverse(getViewMatrix() * getWorldMatrix());
    invTransWorldViewMat = glm::transpose(invTransWorldViewMat);
    return invTransWorldViewMat;
}

const glm::mat4& Node::getViewMatrix() const
{
    auto scene = getScene();
    auto cam = (scene) ? scene->getActiveCamera() : nullptr;
    if (cam) return cam->getViewMatrix();
    else return identityMat;
}

const glm::mat4& Node::getReflectionViewMatrix() const
{
    auto scene = getScene();
    auto cam = (scene) ? scene->getActiveCamera() : nullptr;
    if (cam) return cam->getReflectionViewMatrix();
    else return identityMat;
}

const glm::mat4& Node::getInverseViewMatrix() const
{
    auto scene = getScene();
    auto cam = (scene) ? scene->getActiveCamera() : nullptr;
    if (cam) return cam->getInverseViewMatrix();
    else return identityMat;
}

const glm::mat4& Node::getProjectionMatrix() const
{
    auto scene = getScene();
    auto cam = (scene) ? scene->getActiveCamera() : nullptr;
    if (cam) return cam->getProjectionMatrix();
    else return identityMat;
}

const glm::mat4& Node::getViewProjectionMatrix() const
{
    auto scene = getScene();
    auto cam = (scene) ? scene->getActiveCamera() : nullptr;
    if (cam) return cam->getViewProjectionMatrix();
    else return identityMat;
}

const glm::mat4& Node::getInverseViewProjectionMatrix() const
{
    auto scene = getScene();
    auto cam = (scene) ? scene->getActiveCamera() : nullptr;
    if (cam) return cam->getInverseViewProjectionMatrix();
    else return identityMat;
}

const glm::mat4& Node::getReflectionViewProjectionMatrix() const
{
    auto scene = getScene();
    auto cam = (scene) ? scene->getActiveCamera() : nullptr;
    if (cam) return cam->getReflectionViewProjectionMatrix();
    else return identityMat;
}

const glm::mat4& Node::getWorldViewProjectionMatrix()
{
    worldViewProjectionMat = getViewProjectionMatrix() * getWorldMatrix();
    return worldViewProjectionMat;
}

const glm::mat4& Node::getReflectionWorldViewProjectionMatrix()
{
    reflectionWorldViewProjectionMat = getReflectionViewProjectionMatrix() * getWorldMatrix();
    return reflectionWorldViewProjectionMat;
}

glm::vec3 Node::getTranslationWorld()
{
    glm::vec4 tr4(getTranslation(), 1.f);
    tr4 = getWorldMatrix() * tr4;
    return glm::vec3(tr4);
}

glm::vec3 Node::getTranslationView()
{
    glm::vec4 tr4(getTranslation(), 1.f);
    tr4 = getWorldViewMatrix() * tr4;
    return glm::vec3(tr4);
}

glm::vec3 Node::getForwardVectorWorld()
{
    return Util::Matrix::getForwardVector(getWorldMatrix());
}

glm::vec3 Node::getForwardVectorView()
{
    return Util::Matrix::getForwardVector(getWorldViewMatrix());
}

glm::vec3 Node::getUpVectorWorld()
{
    return Util::Matrix::getUpVector(getWorldMatrix());
}

glm::vec3 Node::getUpVectorView()
{
    return Util::Matrix::getUpVector(getViewMatrix());
}

glm::vec3 Node::getRightVectorWorld()
{
    return Util::Matrix::getRightVector(getWorldMatrix());
}

glm::vec3 Node::getRightVectorView()
{
    return Util::Matrix::getRightVector(getWorldViewMatrix());
}

glm::vec3 Node::getSceneCameraTranslationWorld() const
{
    if (m_scene)
    {
        auto cam = m_scene->getActiveCamera();
        if (cam)
        {
            auto camNode = cam->getNode(); 
            if (camNode)
            {
                return camNode->getTranslationWorld();
            }
        }
    }
    return glm::vec3();
}

glm::vec3 Node::getSceneCameraTranslationView() const
{
    if (m_scene)
    {
        auto cam = m_scene->getActiveCamera();
        if (cam)
        {
            auto camNode = cam->getNode();
            if (camNode)
            {
                return camNode->getTranslationView();
            }
        }
    }
    return glm::vec3();
}

Scene* Node::getScene() const
{
    return m_scene;
}

void Node::setScene(Scene* scene)
{
    //TODO what if this node belongs to another scene already?
    
    m_scene = scene;
    for (auto& c : m_children)
    {
        c->setScene(scene);
    }

    //set scene FBOs on renderables
    if (scene)
    {
        if (auto bsp = getComponent<BspMap>())
        {
            bsp->setFrameBuffer(Scene::FBO::Reflection, scene->getFrameBuffer(Scene::FBO::Reflection));
            bsp->setFrameBuffer(Scene::FBO::Refraction, scene->getFrameBuffer(Scene::FBO::Refraction));
        }

        //TODO other drawables
    }
}

void Node::update(float dt)
{
   //remove dead children
    m_children.erase(std::remove_if(m_children.begin(), m_children.end(),
        [](const Node::Ptr& n)
    {
        return n->destroyed();
    }), m_children.end());    
    
    //remove dead components
    for (auto it = m_components.begin(); it != m_components.end();)
    {
        if (it->second->destroyed())
        {
            m_components.erase(it++);
        }
        else 
        {
            ++it;
        }
    }

    //update what's left
    for (auto& n : m_children)
    {
        n->update(dt);
    }
}

void Node::destroy()
{
    m_destroyed = true;
}

bool Node::destroyed() const
{
    return m_destroyed;
}

//protected
void Node::remove()
{
    m_parent = nullptr;

    if (m_parent && m_parent->m_hierarchyChanged)
    {
        m_parent->hierarchyChanged();
    }
}

void Node::transformChanged()
{
    m_dirtybits |= ALL;

    for (auto& c : m_children)
    {
        if (Transform::transformUpdatePaused())
        {
            if (!c->isDirty(Transform::NOTIFY))
            {
                c->transformChanged();
                pauseTransformation(*c);
            }
        }
        else
        {
            c->transformChanged();
        }
    }
    Transform::transformChanged();
}

void Node::hierarchyChanged()
{
    transformChanged();
}

void Node::setBoundsDirty()
{
    m_dirtybits |= BOUNDS;
    if (m_parent) m_parent->setBoundsDirty();
}

//private
