/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/Transform.hpp>
#include <chuf2/Util.hpp>

#include <chuf2/glm/gtx/transform.hpp>
#include <chuf2/glm/gtx/quaternion.hpp>
#include <chuf2/glm/gtx/rotate_vector.hpp>

using namespace chuf;

namespace
{
    const glm::mat4 identityMatrix = glm::mat4();
    const glm::quat identityQuat = glm::quat();
    const glm::vec3 vec3Zero = glm::vec3(0.f);
    const glm::vec3 vec3One = glm::vec3(1.f);

    chuf::Int32 pauseTransform = 0;
    std::vector<Transform*> updatedTransforms;
}

//ctors
Transform::Transform()
    : m_dirtyBits	(0u),
    m_scale			(1.f)
{

}
    
Transform::Transform(const glm::vec3& translation, const glm::quat& rotation, const glm::vec3& scale)
    : m_dirtyBits	(0u),
    m_scale			(1.f)
{
    set(translation, rotation, scale);
}

Transform::Transform(const glm::vec3& translation, const glm::mat4& rotation, const glm::vec3& scale)
    : m_dirtyBits	(0u),
    m_scale			(1.f)
{
    set(translation, rotation, scale);
}

Transform::Transform(const Transform& copy)
    : m_dirtyBits	(0u),
    m_scale			(1.f)
{
    set(copy);
}

//public
glm::mat4 Transform::getMatrix()
{
    if (m_dirtyBits)
    {
        bool hasTranslation = !Util::Vector::zero(m_translation);
        bool hasRotation = !Util::Quaternion::identity(m_rotation);
        bool hasScale = !Util::Vector::one(m_scale);

        m_matrix = identityMatrix;

        //translate
        if (hasTranslation || (m_dirtyBits & TRANSLATION))
        {
            m_matrix = glm::translate(identityMatrix, m_translation);
        }
        //rotate
        if (hasRotation || (m_dirtyBits & ROTATION))
        {
            m_matrix *= glm::toMat4(m_rotation);
        }
        //scale
        if (hasScale || (m_dirtyBits & SCALE))
        {
            m_matrix *= glm::scale(identityMatrix, m_scale);
        }
        m_dirtyBits = 0u;
    }

    return m_matrix;
}

const glm::vec3& Transform::getTranslation() const
{
    return m_translation;
}

const glm::quat& Transform::getRotation() const
{
    return m_rotation;
}

void Transform::getRotation(glm::mat4& dest) const
{
    dest = glm::toMat4(m_rotation);
}

void Transform::getRotation(glm::vec3& dest) const
{
    auto rotMat = glm::toMat3(m_rotation);
    dest = rotMat * dest;
}

const glm::vec3& Transform::getScale() const
{
    return m_scale;
}

glm::vec3 Transform::getForwardVector() const
{
    return Util::Matrix::getForwardVector(m_matrix);
}

glm::vec3 Transform::getBackVector() const
{
    return Util::Matrix::getBackVector(m_matrix);
}

glm::vec3 Transform::getUpVector() const
{
    return Util::Matrix::getUpVector(m_matrix);
}

glm::vec3 Transform::getDownVector() const
{
    return Util::Matrix::getDownVector(m_matrix);
}

glm::vec3 Transform::getLeftVector() const
{
    return Util::Matrix::getLeftVector(m_matrix);
}

glm::vec3 Transform::getRightVector() const
{
    return Util::Matrix::getRightVector(m_matrix);
}

//------------------------//
void Transform::translate(float x, float y, float z)
{
    m_translation.x += x;
    m_translation.y += y;
    m_translation.z += z;
    dirty(TRANSLATION);
}

void Transform::translate(const glm::vec3& translation)
{
    m_translation += translation;
    dirty(TRANSLATION);
}

void Transform::translate(float x, float y)
{
    m_translation.x += x;
    m_translation.y += y;
    dirty(TRANSLATION);
}

void Transform::translate(const glm::vec2& translation)
{
    m_translation.x += translation.x;
    m_translation.y += translation.y;
    dirty(TRANSLATION);
}

void Transform::rotate(const glm::quat& rotation)
{
    m_rotation = rotation * m_rotation;
    dirty(ROTATION);
}

void Transform::rotate(Axis axis, float angle)
{
    angle = Util::degToRad(angle);
    glm::vec3 normal;
    switch (axis)
    {
    case Axis::X:
        normal.x = 1.f;
        break;
    case Axis::Y:
        normal.y = 1.f;
        break;
    case Axis::Z:
        normal.z = 1.f;
        break;
    default: break;
    }

    m_rotation = glm::rotate(m_rotation, angle, normal);
    dirty(ROTATION);
}

void Transform::rotate(const glm::mat4& rotation)
{
    auto rot = glm::toQuat(rotation);
    m_rotation = rot * m_rotation;
    dirty(ROTATION);
}

void Transform::scale(float scale)
{
    m_scale *= scale;
    dirty(SCALE);
}

void Transform::scale(const glm::vec2& scale)
{
    m_scale.x *= scale.x;
    m_scale.y *= scale.y;
    dirty(SCALE);
}

void Transform::scale(const glm::vec3& scale)
{
    m_scale *= scale;
    dirty(SCALE);
}

void Transform::set(const glm::vec3& translation, const glm::quat& rotation, const glm::vec3& scale)
{
    m_translation = translation;
    m_rotation = rotation;
    m_scale = scale;
    dirty(TRANSLATION | ROTATION | SCALE);
}

void Transform::set(const glm::vec3& translation, const glm::mat4& rotation, const glm::vec3& scale)
{
    m_translation = translation;
    m_rotation = glm::toQuat(rotation);
    m_scale = scale;
    dirty(TRANSLATION | ROTATION | SCALE);
}

void Transform::set(const glm::mat4& matrix)
{
    m_translation = glm::vec3(matrix[3]);
    m_rotation = glm::toQuat(matrix);

    glm::vec3 col1(matrix[0][0], matrix[0][1], matrix[0][2]);
    glm::vec3 col2(matrix[1][0], matrix[1][1], matrix[1][2]);
    glm::vec3 col3(matrix[2][0], matrix[2][1], matrix[2][2]);
    
    m_scale.x = glm::length(col1);
    m_scale.y = glm::length(col2);
    m_scale.z = glm::length(col3);

    dirty(TRANSLATION | ROTATION | SCALE);
}

void Transform::set(const Transform& transform)
{
    m_translation = transform.m_translation;
    m_rotation = transform.m_rotation;
    m_scale = transform.m_scale;

    dirty(TRANSLATION | ROTATION | SCALE);
}

void Transform::setIdentity()
{
    m_translation = vec3Zero;
    m_rotation = identityQuat;
    m_scale = vec3One;

    dirty(TRANSLATION | ROTATION | SCALE);
}

void Transform::setTranslation(float x, float y, float z)
{
    m_translation.x = x;
    m_translation.y = y;
    m_translation.z = z;

    dirty(TRANSLATION);
}

void Transform::setTranslation(const glm::vec3& translation)
{
    m_translation = translation;

    dirty(TRANSLATION);
}

void Transform::setTranslation(float x, float y)
{
    m_translation.x = x;
    m_translation.y = y;
    m_translation.z = 0.f;
    dirty(TRANSLATION);
}

void Transform::setTranslation(const glm::vec2& translation)
{
    m_translation.x = translation.x;
    m_translation.y = translation.y;
    m_translation.z = 0.f;
    dirty(TRANSLATION);
}

void Transform::setRotation(const glm::quat& rotation)
{
    m_rotation = rotation;

    dirty(ROTATION);
}

void Transform::setRotation(Axis axis, float angle)
{
    angle = Util::degToRad(angle);
    glm::vec3 normal;
    switch (axis)
    {
    case Axis::X:
        normal.x = 1.f;
        break;
    case Axis::Y:
        normal.y = 1.f;
        break;
    case Axis::Z:
        normal.z = 1.f;
        break;
    default: break;
    }
    m_rotation = glm::rotate(identityQuat, angle, normal);

    dirty(ROTATION);
}

void Transform::setRotation(const glm::mat4& rotation)
{
    m_rotation = glm::toQuat(rotation);

    dirty(ROTATION);
}

void Transform::setScale(float scale)
{
    m_scale = glm::vec3(scale);

    dirty(SCALE);
}

void Transform::setScale(float x, float y, float z)
{
    m_scale = glm::vec3(x, y, z);

    dirty(SCALE);
}

void Transform::setScale(const glm::vec3& scale)
{
    m_scale = scale;

    dirty(SCALE);
}

void Transform::setScale(float x, float y)
{
    m_scale.x = x;
    m_scale.y = y;
    m_scale.z = 1.f;

    dirty(SCALE);
}

void Transform::setScale(const glm::vec2& scale)
{
    m_scale.x = scale.x;
    m_scale.y = scale.y;
    m_scale.z = 1.f;

    dirty(SCALE);
}

glm::vec3 Transform::transformPoint(const glm::vec3& point)
{
    glm::vec4 tp = getMatrix() * glm::vec4(point.x, point.y, point.z , 1.f);
    return glm::vec3(tp.x, tp.y, tp.z);
}

//void Transform::transformPoint(glm::vec3& point)
//{
//    glm::vec4 tp = getMatrix() * glm::vec4(point.x, point.y, point.z, 1.f);
//    point = glm::vec3(tp.x, tp.y, tp.z);
//}

glm::vec2 Transform::transformPoint(const glm::vec2& point)
{
    glm::vec4 tp = getMatrix() * glm::vec4(point.x, point.y, 0.f, 1.f);
    return glm::vec2(tp.x, tp.y);
}

void Transform::addListener(Listener& l)
{
    m_listeners.push_back(&l);
}

void Transform::removeListener(Listener& l)
{
    m_listeners.remove_if([&](const Listener* lp)
    {
        return (lp == &l);
    });
}

//----static functions----//
void Transform::pauseTransformUpdate()
{
    pauseTransform++;
}

void Transform::resumeTransformUpdate()
{
    if (!pauseTransform) return;

    if (pauseTransform == 1)
    {
        //call transform changed on all transforms
        for (auto& t : updatedTransforms)
        {
            t->transformChanged();
        }
        
        //mark transforms for modification. do this again
        //because list could have potentially grown
        for (auto& t : updatedTransforms)
        {
            t->m_dirtyBits &= NOTIFY;
        }

        updatedTransforms.clear();
    }

    pauseTransform--;
}

bool Transform::transformUpdatePaused()
{
    return (pauseTransform > 0);
}



//protected
void Transform::dirty(UInt8 dirtyBits)
{
    m_dirtyBits |= dirtyBits;
    if (transformUpdatePaused())
    {
        if (!isDirty(NOTIFY))
        {
            pauseTransformation(*this);
        }
    }
    else
    {
        transformChanged();
    }
}

bool Transform::isDirty(UInt8 dirtyBits) const
{
    return (m_dirtyBits | dirtyBits) == dirtyBits;
}

void Transform::pauseTransformation(Transform& t)
{
    t.m_dirtyBits |= NOTIFY;
    updatedTransforms.push_back(&t);
}

void Transform::transformChanged()
{
    for (auto l : m_listeners)
    {
        l->transformChanged(*this);
    }
}

//private
