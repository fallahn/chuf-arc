/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/RenderState.hpp>
#include <chuf2/MaterialProperty.hpp>
#include <chuf2/Pass.hpp>
#include <chuf2/Node.hpp>
#include <chuf2/Scene.hpp>
#include <chuf2/Log.hpp>

using namespace chuf;

namespace
{
    std::unordered_map<std::string, std::string> propertyBindings;
    const glm::mat4 identityMat = glm::mat4();
    const glm::vec3 vec3Zero = glm::vec3();
    const glm::vec3 vec3One = glm::vec3(1.f);
    const glm::vec3 unitY = glm::vec3(0.f, 1.f, 0.f);

    const std::string AmbientColour("AMBIENT_COLOUR");
    const std::string CameraViewPosition("CAMERA_VIEW_POSITION");
    const std::string CameraWorldPosition("CAMERA_WORLD_POSITION");
    const std::string InverseTransposeWorldMatrix("INVERSE_TRANSPOSE_WORLD_MATRIX");
    const std::string InverseTransposeWorldViewMatrix("INVERSE_TRANSPOSE_WORLD_VIEW_MATRIX");
    const std::string ProjectionMatrix("PROJECTION_MATRIX");
    const std::string ViewMatrix("VIEW_MATRIX");
    const std::string ReflectionViewMatrix("REFLECTION_VIEW_MATRIX");
    const std::string WorldMatrix("WORLD_MATRIX");
    const std::string ViewProjectionMatrix("VIEW_PROJECTION_MATRIX");
    const std::string ReflectionViewProjectionMatrix("REFLECTION_VIEW_PROJECTION_MATRIX");
    const std::string WorldViewMatrix("WORLD_VIEW_MATRIX");
    const std::string ReflectionWorldViewMatrix("REFLECTION_WORLD_VIEW_MATRIX");
    const std::string WorldViewProjectionMatrix("WORLD_VIEW_PROJECTION_MATRIX");
    const std::string ReflectionWorldViewProjectionMatrix("REFLECTION_WORLD_VIEW_PROJECTION_MATRIX");
    //TODO add light colour and light direction
}

RenderState::RenderState(const Key& key) : RenderState(){}
RenderState::RenderState(): m_node(nullptr), m_parent(nullptr){}
RenderState::~RenderState(){}

void RenderState::start()
{
    if (!StateBlock::m_defaultState)
    {
        StateBlock::m_defaultState = StateBlock::create();
    }
}

void RenderState::finish()
{

}

MaterialProperty* RenderState::getProperty(const std::string& name) const
{
    for (auto& m : m_properties)
        if (m->getName() == name) return m.get();

    m_properties.push_back(std::make_unique<MaterialProperty>(name));
    return m_properties.back().get();
}

 MaterialProperty* RenderState::getProperty(UInt32 index) const
{
    assert(index < m_properties.size());
    return m_properties[index].get();
}

UInt32 RenderState::getPropertyCount() const
{
    return m_properties.size();
}

void RenderState::addProperty(MaterialProperty::Ptr& mp)
{
    m_properties.push_back(std::move(mp));
}

void RenderState::removeProperty(const std::string& name)
{
    auto result =  std::remove_if(m_properties.begin(), m_properties.end(),
        [&name](const MaterialProperty::Ptr& p)
        {
            return (p->getName() == name);
        });

    m_properties.erase(result, m_properties.end());
}

void RenderState::setPropertyBinding(const std::string& name, PropertyBinding b)
{
    std::string bindingName;
    switch (b)
    {
    case PropertyBinding::AmbientColour:
        bindingName = AmbientColour;
        break;
    case PropertyBinding::CameraViewPosition:
        bindingName = CameraViewPosition;
        break;
    case PropertyBinding::CameraWorldPosition:
        bindingName = CameraWorldPosition;
        break;
    case PropertyBinding::InverseTransposeWorldMatrix:
        bindingName = InverseTransposeWorldMatrix;
        break;
    case PropertyBinding::InverseTransposeWorldViewMatrix:
        bindingName = InverseTransposeWorldViewMatrix;
        break;
    case PropertyBinding::None:
    default:
        break;
    case PropertyBinding::ProjectionMatrix:
        bindingName = ProjectionMatrix;
        break;
    case PropertyBinding::ViewMatrix:
        bindingName = ViewMatrix;
        break;
    case PropertyBinding::ReflectionViewMatrix:
        bindingName = ReflectionViewMatrix;
        break;
    case PropertyBinding::ViewProjectionMatrix:
        bindingName = ViewProjectionMatrix;
        break;
    case PropertyBinding::ReflectionViewProjectionMatrix:
        bindingName = ReflectionViewProjectionMatrix;
        break;
    case PropertyBinding::WorldMatrix:
        bindingName = WorldMatrix;
        break;
    case PropertyBinding::WorldViewMatrix:
        bindingName = WorldViewMatrix;
        break;
    case PropertyBinding::ReflectionWorldViewMatrix:
        bindingName = ReflectionWorldViewMatrix;
        break;
    case PropertyBinding::WorldViewProjectionMatrix:
        bindingName = WorldViewProjectionMatrix;
        break;
    case PropertyBinding::ReflectionWorldViewProjectionMatrix:
        bindingName = ReflectionWorldViewProjectionMatrix;
        break;
    }
    setPropertyBinding(name, bindingName);
}

void RenderState::setPropertyBinding(const std::string& name, const std::string& bindingName)
{
    auto result = propertyBindings.find(name);
    if (bindingName.empty())
    {
        if (result != propertyBindings.end()) propertyBindings.erase(result);
    }
    else
    {
        if (result != propertyBindings.end())
        {
            propertyBindings[name] = bindingName;
        }
        else
        {
            propertyBindings.insert(std::make_pair(name, bindingName));
        }
    }

    if (m_node)
    {
        applyBinding(name, bindingName);
    }
}

void RenderState::setStateBlock(StateBlock::Ptr& sb)
{
    m_stateBlock = std::move(sb);
}

RenderState::StateBlock* RenderState::getStateBlock() const
{
    if (!m_stateBlock)
        m_stateBlock = std::move(StateBlock::create());
    return m_stateBlock.get();
}

void RenderState::bindNode(Node* node)
{
    m_node = node;
}

void RenderState::bind(Pass& pass)
{
    //assert(pass);
    UInt64 overrideBits = (m_stateBlock) ? m_stateBlock->m_overrideBits : 0u;

    auto rs = m_parent;
    while (rs)
    {
        if (rs->m_stateBlock)
        {
            overrideBits |= rs->m_stateBlock->m_overrideBits;
        }
        rs = rs->m_parent;
    }

    StateBlock::restore(overrideBits);

    rs = getTopState(rs);

    auto shader = pass.getShader();

    while (rs != nullptr)
    {
        auto& p = rs->m_properties;
        for (auto& mp : p)
            mp->bind(shader);

        if (rs->m_stateBlock)
            rs->m_stateBlock->bindNoRestore();

        rs = getTopState(rs);
    }

}

RenderState* RenderState::getTopState(RenderState* below)
{
    auto rs = this;
    if (rs == below) return nullptr;

    while (rs)
    {
        if (rs->m_parent == below || rs->m_parent == nullptr)
        {
            return rs;
        }
        rs = rs->m_parent;
    }

    return nullptr;
}

//protected
void RenderState::applyBinding(const std::string& uniformName, const std::string& binding)
{
    assert(m_node);
    auto matProp = getProperty(uniformName);
    assert(matProp);

    bool bound = true;
    //TODO call resolve on any listed auto bindings

    if (binding == WorldMatrix)
    {
        std::function<const glm::mat4&()> f = std::bind(&RenderState::bindingGetWorldMatrix, this);
        matProp->bindValue(f);
    }
    else if (binding == ViewMatrix)
    {
        std::function<const glm::mat4&()> f = std::bind(&RenderState::bindingGetViewMatrix, this);
        matProp->bindValue(f);
    }
    else if (binding == ReflectionViewMatrix)
    {
        std::function<const glm::mat4&()> f = std::bind(&RenderState::bindingGetReflectionViewMatrix, this);
        matProp->bindValue(f);
    }
    else if (binding == WorldViewMatrix)
    {
        std::function<const glm::mat4&()> f = std::bind(&RenderState::bindingGetWorldViewMatrix, this);
        matProp->bindValue(f);
    }
    else if (binding == ReflectionWorldViewMatrix)
    {
        std::function<const glm::mat4&()> f = std::bind(&RenderState::bindingGetReflectionWorldViewMatrix, this);
        matProp->bindValue(f);
    }
    else if (binding == ViewProjectionMatrix)
    {
        std::function<const glm::mat4&()> f = std::bind(&RenderState::bindingGetViewProjectionMatrix, this);
        matProp->bindValue(f);
    }
    else if (binding == ReflectionViewProjectionMatrix)
    {
        std::function<const glm::mat4&()> f = std::bind(&RenderState::bindingGetReflectionViewProjectionMatrix, this);
        matProp->bindValue(f);
    }
    else if (binding == WorldViewProjectionMatrix)
    {
        std::function<const glm::mat4&()> f = std::bind(&RenderState::bindingGetWorldViewProjectionMatrix, this);
        matProp->bindValue(f);
    }
    else if (binding == ReflectionWorldViewProjectionMatrix)
    {
        std::function<const glm::mat4&()> f = std::bind(&RenderState::bindingGetReflectionWorldViewProjectionMatrix, this);
        matProp->bindValue(f);
    }
    else if (binding == InverseTransposeWorldMatrix)
    {
        std::function<const glm::mat4&()> f = std::bind(&RenderState::bindingGetInverseTransposeWorldMatrix, this);
        matProp->bindValue(f);
    }
    else if (binding == InverseTransposeWorldViewMatrix)
    {
        std::function<const glm::mat4&()> f = std::bind(&RenderState::bindingGetInverseTransposeWorldViewMatrix, this);
        matProp->bindValue(f);
    }
    else if (binding == CameraWorldPosition)
    {
        std::function<glm::vec3()> f = std::bind(&RenderState::bindingGetCameraWorldPosition, this);
        matProp->bindValue(f);
    }
    else if (binding == CameraViewPosition)
    {
        std::function<glm::vec3()> f = std::bind(&RenderState::bindingGetCameraViewPosition, this);
        matProp->bindValue(f);
    }
    else if (binding == AmbientColour)
    {
        std::function<glm::vec3()> f = std::bind(&RenderState::bindingGetAmbientColour, this);
        matProp->bindValue(f);
    }
    //TODO light bindings (colour/direction)
    else
    {
        bound = false;
        Logger::Log("Unrecognised autobind: " + binding, Logger::Type::Warning);
    }

    //TODO mark property as being autobound
}

//private
const glm::mat4& RenderState::bindingGetWorldMatrix()
{
    return m_node ? m_node->getWorldMatrix() : identityMat;
}

const glm::mat4& RenderState::bindingGetViewMatrix()
{
    return m_node ? m_node->getViewMatrix() : identityMat;
}

const glm::mat4& RenderState::bindingGetReflectionViewMatrix()
{
    return m_node ? m_node->getReflectionViewMatrix() : identityMat;
}

const glm::mat4& RenderState::bindingGetProjectionMatrix()
{
    return m_node ? m_node->getProjectionMatrix() : identityMat;
}

const glm::mat4& RenderState::bindingGetWorldViewMatrix()
{
    return m_node ? m_node->getWorldViewMatrix() : identityMat;
}

const glm::mat4& RenderState::bindingGetReflectionWorldViewMatrix()
{
    return m_node ? m_node->getReflectionWorldViewMatrix() : identityMat;
}

const glm::mat4& RenderState::bindingGetViewProjectionMatrix()
{
    return m_node ? m_node->getViewProjectionMatrix() : identityMat;
}

const glm::mat4& RenderState::bindingGetReflectionViewProjectionMatrix()
{
    return m_node ? m_node->getReflectionViewProjectionMatrix() : identityMat;
}

const glm::mat4& RenderState::bindingGetWorldViewProjectionMatrix()
{
    return m_node ? m_node->getWorldViewProjectionMatrix() : identityMat;
}

const glm::mat4& RenderState::bindingGetReflectionWorldViewProjectionMatrix()
{
    return m_node ? m_node->getReflectionWorldViewProjectionMatrix() : identityMat;
}

const glm::mat4& RenderState::bindingGetInverseTransposeWorldMatrix()
{
    return m_node ? m_node->getInverseTransposeWorldMatrix() : identityMat;
}

const glm::mat4& RenderState::bindingGetInverseTransposeWorldViewMatrix()
{
    return m_node ? m_node->getInverseTransposeWorldViewMatrix() : identityMat;
}

glm::vec3 RenderState::bindingGetCameraWorldPosition()
{
    return m_node ? m_node->getSceneCameraTranslationWorld() : vec3Zero;
}

glm::vec3 RenderState::bindingGetCameraViewPosition()
{
    return m_node ? m_node->getSceneCameraTranslationView() : vec3Zero;
}

glm::vec3 RenderState::bindingGetAmbientColour()
{
    if (!m_node) return vec3Zero;

    auto scene = m_node->getScene();

    auto colour = scene ? scene->getAmbientColour() : Colour();
    return glm::vec3(colour.r, colour.g, colour.b);
}

const glm::vec3& RenderState::bindingGetLightColour()
{
    return vec3One;
}

glm::vec3 RenderState::bindingGetLightDirection()
{
    return m_node ? m_node->getForwardVector() : -unitY;
}
