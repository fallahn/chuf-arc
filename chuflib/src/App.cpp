/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/App.hpp>

#include <chuf2/Timer.hpp>
#include <chuf2/RenderState.hpp>
#include <chuf2/FrameBuffer.hpp>
#include <chuf2/Cache.hpp>
#include <chuf2/Image.hpp>
#include <chuf2/Util.hpp>

#include <sstream>
#include <cassert>

using namespace chuf;

namespace
{
    const float frameTime = 1.f / 60.f;
}

App::App(const VideoContext& videoContext)
    : m_window		(nullptr),
    m_glContext		(nullptr),
    m_videoContext	(videoContext),
    m_framerate		(0.f),
    m_running		(true)
{
    assert(videoContext.depthbits == 16 || videoContext.depthbits == 24 || videoContext.depthbits == 32);
    //TODO assert resolution settings valid
    
    if (!m_instance)
        m_instance = this;
    else
        throw "App instance already exists";
    
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        std::stringstream ss;
        ss << "Failed to init SDL with error: " << SDL_GetError() << std::endl;
        throw ss.str();
    }
    else
    {
        SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
        SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, videoContext.depthbits); //nvidia doesn't support 32 bit depth

        //SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
        //SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 2);

        //SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3); //requesting this version causes an error in texture class?
        //SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
                
        m_window = SDL_CreateWindow(videoContext.windowTitle.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                                    videoContext.width, videoContext.height, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN /*| SDL_WINDOW_FULLSCREEN*/);
        if (m_window)
        {
            m_glContext = SDL_GL_CreateContext(m_window);
            assert(m_glContext);

            //gl3w init MUST be after a valid context is created
            if (gl3wInit())
            {
                killSDL();
                throw "Failed to init gl3w";
            }

            if (!gl3wIsSupported(3, 3))
            {
                killSDL();
                throw "OpenGL 3.3 support not found";
            }

            auto vb = glGetString(GL_VERSION);
            auto vc = glGetString(GL_SHADING_LANGUAGE_VERSION);
            std::cout << "Found GL version: " << vb << "\nWith shader version: " << vc << " support." << std::endl;

            glClearColor(0.f, 0.f, 0.f, 1.f);
            glClear(GL_COLOR_BUFFER_BIT);

            //glEnable(GL_MULTISAMPLE);
        }
    }
}

App::~App()
{
    killSDL();
}

//public
App& App::getInstance()
{
    assert(m_instance);
    //got an assertion? did you forget to create an instance of your derived app?
    return *m_instance;
}

void App::run()
{
    m_view.viewport.width = static_cast<float>(m_videoContext.width);
    m_view.viewport.height = static_cast<float>(m_videoContext.height);
    setView(m_view);
    
    setVSyncEnabled(true);

    RenderState::start();
    FrameBuffer::start();
    start();

    Timer updateTimer;
    float timeSinceLastUpdate = frameTime;
    while (m_running)
    {
        float elapsed = updateTimer.restart().asSeconds();
        m_framerate = 1.f / elapsed;
        timeSinceLastUpdate += elapsed;
        while (timeSinceLastUpdate >= frameTime)
        {
            timeSinceLastUpdate -= frameTime;
            parseEvents();
            update(frameTime);
        }
        draw();
        SDL_GL_SwapWindow(m_window);
    }

    finish();

    FrameBuffer::finish();
    RenderState::finish();
    BaseCache::flushAll();
}

void App::close()
{
    m_running = false;
}

void App::setVSyncEnabled(bool enabled)
{
    assert(m_instance);
    if (enabled)
        SDL_GL_SetSwapInterval(1);
    else
        SDL_GL_SetSwapInterval(0);
}

bool App::vSyncEnabled()
{
    assert(m_instance);
    return SDL_GL_GetSwapInterval() == 1;
}

View App::setView(const chuf::View& viewport)
{
    View prevView(m_view);
    m_view = viewport;
    glViewport(static_cast<GLint>(m_view.viewport.left),
        static_cast<GLint>(m_view.viewport.top),
        static_cast<GLsizei>(m_view.viewport.width),
        static_cast<GLsizei>(m_view.viewport.height));

    return prevView;
}

const chuf::View& App::getView() const
{
    return m_view;
}

const VideoContext& App::getVideoContext() const
{
    return m_videoContext;
}

void App::setWindowTitle(const std::string& title)
{
    assert(m_window);
    SDL_SetWindowTitle(m_window, title.c_str());
}

float App::getFramerate() const
{
    return m_framerate;
}

void App::clear(Colour colour, ClearFlags flags)
{
    if (flags & ClearFlags::DEPTH)
    {
        //restores depth writing if been switched oof
        //by a previous state
        RenderState::StateBlock::enableDepthWrite();
        //TODO 
        glClearDepth(1.f);
    }

    if (flags & ClearFlags::STENCIL)
    {
        //TODO 
        glClearStencil(0);
    }

    if (flags & ClearFlags::COLOUR)
        glClearColor(colour.r, colour.g, colour.b, colour.a);

    assert(m_instance);	
    glClear(flags);
}

void App::setMouseCaptured(bool captured)
{
    SDL_SetRelativeMouseMode(captured ? SDL_bool::SDL_TRUE : SDL_bool::SDL_FALSE);
}

Image::Ptr App::getScreenshot() const
{
    auto image = Image::create(m_videoContext.width, m_videoContext.height, Image::Format::RGBA);
    glCheck(glReadPixels(0, 0, m_videoContext.width, m_videoContext.height, GL_RGBA, GL_UNSIGNED_BYTE, const_cast<UInt8*>(image->getData())));
    image->flipVertically();
    return std::move(image);
}

std::string App::getDateTimeString() const
{
    std::time_t time = std::time(NULL);
    struct tm* timeInfo;

    timeInfo = std::localtime(&time);

    char buffer[26];
    std::string retVal;

    strftime(buffer, 26, "%d_%m_%y_%H_%M_%S", timeInfo);

    retVal.assign(buffer);
    return retVal;
}

glm::ivec2 App::getMousePosition() const
{
    int x, y;
    SDL_GetMouseState(&x, &y);
    return { x, y };
}

//protected
void App::handleMouseEvent(const Event::MouseEvent& evt)
{
    //not implimented by default
    //override event handlers in derived class as needed
}

void App::handleKeyEvent(const Event::KeyEvent& evt)
{
    //not implimented by default
    //override event handlers in derived class as needed	
}

void App::handleJoyEvent(const Event::JoyEvent& evt)
{
    //not implimented by default
    //override event handlers in derived class as needed
}

void App::handleControllerEvent(const Event::ControllerEvent& evt)
{
    //not implimented by default
    //override event handlers in derived class as needed
}

void App::handleWindowEvent(const Event::WindowEvent& evt)
{
    //not implimented by default
    //override event handlers in derived class as needed
}

//private
App* App::m_instance = nullptr;

void App::killSDL()
{
    if (m_glContext)
        SDL_GL_DeleteContext(m_glContext);

    if (m_window)
        SDL_DestroyWindow(m_window);

    SDL_Quit();
}

void App::parseEvents()
{
    SDL_Event evt;
    while (SDL_PollEvent(&evt))
    {
        switch (evt.type)
        {
        case Event::WINDOW:
            switch (evt.window.event)
            {
            case SDL_WindowEventID::SDL_WINDOWEVENT_CLOSE:
                close();
                break;
            default:
                handleWindowEvent(evt);
                break;
            }
            break;
        case Event::MOUSE_BUTTON_PRESSED:
        case Event::MOUSE_BUTTON_RELEASED:
        case Event::MOUSE_MOVE:
        case Event::MOUSE_WHEEL_MOVED:
            handleMouseEvent(evt);
            break;
        case Event::KEY_PRESSED:
        case Event::KEY_RELEASED:
            handleKeyEvent(evt);
            break;
        case Event::JOY_AXIS_MOVED:
        case Event::JOY_BALL_MOVED:
        case Event::JOY_BUTTON_PRESSED:
        case Event::JOY_BUTTON_RELEASED:
        case Event::JOY_CONNECTED:
        case Event::JOY_DISCONNECTED:
        case Event::JOY_HAT_MOVED:
            handleJoyEvent(evt);
            break;
        case Event::CONTROLLER_AXIS_MOVED:
        case Event::CONTROLLER_BUTTON_PRESSED:
        case Event::CONTROLLER_BUTTON_RELEASED:
        case Event::CONTROLLER_CONNECTED:
        case Event::CONTROLLER_DISCONNECTED:
        case Event::CONTROLLER_REMAPPED:
            handleControllerEvent(evt);
            break;
        default: break;
        }
    }
}
