/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/Mesh.hpp>
#include <chuf2/SubMesh.hpp>
#include <chuf2/Skin.hpp>

#include <chuf2/Util.hpp>

#include <array>

using namespace chuf;

Mesh::Mesh(const VertexLayout& layout, const Mesh::Key& key)
    : m_vertLayout	(layout),
    m_vertCount		(0u),
    m_vertBuffer	(0),
    m_primitiveType	(PrimitiveType::TriangleStrip),
    m_dynamic		(false)
{}

Mesh::~Mesh()
{
    if (m_vertBuffer)
    {
        glCheck(glDeleteBuffers(1, &m_vertBuffer));
        m_vertBuffer = 0;
        //LOG("Successfully Destroyed Mesh");
    }
}

//public
const VertexLayout& Mesh::getVertexLayout() const
{
    return m_vertLayout;
}

UInt32 Mesh::getVertexCount() const
{
    return m_vertCount;
}

UInt32 Mesh::getVertexSize() const
{
    return m_vertLayout.getVertexSize();
}

VertexBufferID Mesh::getVertexBuffer() const
{
    return m_vertBuffer;
}

bool Mesh::dynamic() const
{
    return m_dynamic;
}

Mesh::PrimitiveType Mesh::getPrimitiveType() const
{
    return m_primitiveType;
}

void Mesh::setPrimitveType(Mesh::PrimitiveType type)
{
    m_primitiveType = type;
}

void Mesh::setVertexData(const float* data, UInt32 count, UInt32 start)
{
    assert(data);

    glCheck(glBindBuffer(GL_ARRAY_BUFFER, m_vertBuffer));
    if (count == 0 && start == 0)
    {
        glCheck(glBufferData(GL_ARRAY_BUFFER, m_vertLayout.getVertexSize() * m_vertCount, data, m_dynamic ? GL_DYNAMIC_DRAW : GL_STATIC_DRAW));
    }
    else
    {
        if (count == 0)
        {
            count = m_vertCount - start;
        }
        glCheck(glBufferSubData(GL_ARRAY_BUFFER, start * m_vertLayout.getVertexSize(), count * m_vertLayout.getVertexSize(), data));
    }
}

SubMesh& Mesh::addSubMesh(Mesh::PrimitiveType type, Mesh::IndexFormat format, UInt32 count, bool dynamic)
{
    m_subMeshes.push_back(std::move(SubMesh::create(this, m_subMeshes.size(), type, format, count, dynamic)));
    return *m_subMeshes[m_subMeshes.size() - 1];
}

UInt32 Mesh::getSubMeshCount() const
{
    return m_subMeshes.size();
}

SubMesh& Mesh::getSubMesh(UInt32 index) const
{
    assert(index < m_subMeshes.size());
    return *m_subMeshes[index];
}

const BoundingBox& Mesh::getBoundingBox() const
{
    return m_boundingBox;
}

void Mesh::setBoundingBox(const BoundingBox& bb)
{
    m_boundingBox = bb;
}

const BoundingSphere& Mesh::getBoundingSphere() const
{
    return m_boundingSphere;
}

void Mesh::setBoundingSphere(const BoundingSphere& bs)
{
    m_boundingSphere = bs;
}

//private
void Mesh::updateNormalMeshArray(UInt32 typeIndex, const VertexLayout& vertData, const std::vector<float>& posData, const std::vector<float>& vboData, std::vector<float>& newData, float scale, Colour colour)
{
    UInt32 posStart = 0u;
    UInt32 normStart = 0u;
    for (auto i = 0u; i < typeIndex; ++i)
    {
        normStart += vertData.getElement(i).size;
    }

    const UInt32 posStride = 3u;
    const UInt32 normStride = vertData.getVertexSize() / sizeof(float);
    const UInt32 end = posData.size();

    for (auto i = posStart, j = normStart; i < end; i += posStride, j += normStride)
    {
        //pos / colour
        newData.push_back(posData[i]);
        newData.push_back(posData[i + 1]);
        newData.push_back(posData[i + 2]);
        newData.push_back(colour.r);
        newData.push_back(colour.g);
        newData.push_back(colour.b);

        //pos + normal pos / colour
        newData.push_back(posData[i] + (vboData[j] * scale));
        newData.push_back(posData[i + 1] + (vboData[j + 1] * scale));
        newData.push_back(posData[i + 2] + (vboData[j + 2] * scale));
        newData.push_back(colour.r);
        newData.push_back(colour.g);
        newData.push_back(colour.b);
    }
}