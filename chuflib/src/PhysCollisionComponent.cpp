/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/physics/PhysCollisionComponent.hpp>
#include <chuf2/physics/PhysWorld.hpp>
#include <chuf2/glm.hpp>
#include <chuf2/Node.hpp>

#include <cassert>

using namespace chuf;

namespace
{
    struct CollisionCallback final : public btCollisionWorld::ContactResultCallback
    {
        btScalar addSingleResult(btManifoldPoint& cp, const btCollisionObjectWrapper* a, Int32 partIdA, Int32 indexA, 
                                const btCollisionObjectWrapper* b, Int32 partIdB, Int32 indexB) override
        {
            result = true;
            return 0.f;
        }
        bool result = false;
    };
}

PhysicsCollisionComponent::PhysicsCollisionComponent(Int32 group, Int32 mask)
    : m_enabled     (true),
    m_collisionShape(nullptr),
    m_collisionGroup(group),
    m_collisionMask (mask),
    m_physWorld     (nullptr)
{

}

PhysicsCollisionComponent::~PhysicsCollisionComponent()
{
    if (m_physWorld) m_physWorld->removeCollisionComponent(this, true);
}

//public
PhysicsCollisionShape* PhysicsCollisionComponent::getCollisionShape() const
{
    return m_collisionShape;
}

PhysicsCollisionShape::Type PhysicsCollisionComponent::getShapeType() const
{
    assert(m_collisionShape);
    return m_collisionShape->getType();
}

bool PhysicsCollisionComponent::kinematicBody() const
{
    switch (getType())
    {
    case Type::Character:
    case Type::Ghost:
        return true;
    default:
        assert(getCollisionObject());
        return getCollisionObject()->isKinematicObject();
    }
    
    return false;
}

bool PhysicsCollisionComponent::staticBody() const
{
    switch (getType())
    {
    case Type::Character:
    case Type::Ghost:
        return false;
    default:
        assert(getCollisionObject());
        return getCollisionObject()->isStaticObject();
    }
    return false;
}

bool PhysicsCollisionComponent::dynamicBody() const
{
    assert(getCollisionObject());
    return !getCollisionObject()->isStaticOrKinematicObject();
}

bool PhysicsCollisionComponent::enabled() const
{
    return m_enabled;
}

void PhysicsCollisionComponent::setEnabled(bool enabled)
{
    if (enabled)
    {
        if (!m_enabled)
        {
            if (m_physWorld) m_physWorld->addCollisionComponent(this);
            m_motionState->updateTransformFromNode();
            m_enabled = true;
        }
    }
    else
    {
        if (!m_enabled)
        {
            if (m_physWorld) m_physWorld->removeCollisionComponent(this, false);
            m_enabled = false;
        }
    }
}

void PhysicsCollisionComponent::addListener(Listener* l, PhysicsCollisionComponent* o)
{
    assert(m_physWorld);
    m_physWorld->addListener(l, this, o);
}

void PhysicsCollisionComponent::removeListener(Listener* l, PhysicsCollisionComponent* o)
{
    assert(m_physWorld);
    m_physWorld->removeListener(l, this, o);
}

bool PhysicsCollisionComponent::intersects(PhysicsCollisionComponent* component) const
{
    assert(m_physWorld);
    assert(component && component->getCollisionObject());
    assert(getCollisionObject());

    static CollisionCallback cb;

    cb.result = false;
    m_physWorld->m_btPhysWorld->contactPairTest(getCollisionObject(), component->getCollisionObject(), cb);
    return cb.result;
    
    return false;
}

//protected
void PhysicsCollisionComponent::setCollisionShape(PhysicsCollisionShape* s)
{
    m_collisionShape = s;
}

//----------collision pair----------//
PhysicsCollisionComponent::CollisionPair::CollisionPair(PhysicsCollisionComponent* a, PhysicsCollisionComponent* b)
    : objA(a), objB(b)
{
    assert(a && b);
}

bool PhysicsCollisionComponent::CollisionPair::operator < (const CollisionPair& cp) const
{
    if ((objA == cp.objA && objB == cp.objB)
        || (objA == cp.objB && objB == cp.objA))
    {
        return false;
    }

    if (objA < cp.objA)
    {
        return true;
    }

    if (objA == cp.objA)
    {
        return objB < cp.objB;
    }
    
    return false;
}

//--------motion state--------/
PhysicsCollisionComponent::MotionState::MotionState(Node* n, PhysicsCollisionComponent* pc, const glm::vec3* centreOfMassOffset)
    : m_node        (n),
    m_physComponent (pc),
    m_comOffset     (btTransform::getIdentity())
{
    if (centreOfMassOffset)
    {
        m_comOffset.setOrigin({ centreOfMassOffset->x, centreOfMassOffset->y, centreOfMassOffset->z });
    }
    updateTransformFromNode();
}


void PhysicsCollisionComponent::MotionState::getWorldTransform(btTransform& t) const
{
    assert(m_node);
    assert(m_physComponent);

    if (m_physComponent->kinematicBody())
    {
        updateTransformFromNode();
    }
    t = m_comOffset.inverse() * m_worldTransform;
}

void PhysicsCollisionComponent::MotionState::setWorldTransform(const btTransform& t)
{
    assert(m_node);
    m_worldTransform = t * m_comOffset;

    const auto& rot = m_worldTransform.getRotation();
    const auto& pos = m_worldTransform.getOrigin();

    m_node->setRotation({ rot.x(), rot.y(), rot.z(), rot.w() });
    m_node->setTranslation(pos.x(), pos.y(), pos.z());
}

void PhysicsCollisionComponent::MotionState::updateTransformFromNode() const
{
    assert(m_node);
    
    glm::quat rotation = m_node->getRotation();
    const auto& mat = m_node->getWorldMatrix();

    if (!m_comOffset.getOrigin().isZero())
    {
        btQuaternion bq(rotation.x, rotation.y, rotation.z, rotation.w);
        btTransform offset = btTransform(bq) * m_comOffset.inverse();

        btVector3 origin
        (
            mat[3][0] + m_comOffset.getOrigin().getX() + offset.getOrigin().getX(), 
            mat[3][1] + m_comOffset.getOrigin().getY() + offset.getOrigin().getY(),
            mat[3][2] + m_comOffset.getOrigin().getZ() + offset.getOrigin().getZ()
        );

        m_worldTransform = btTransform(bq, origin);
    }
    else
    {
        m_worldTransform = btTransform({ rotation.x, rotation.y, rotation.z, rotation.w }, { mat[3][0], mat[3][1], mat[3][2] });
    }
}

void PhysicsCollisionComponent::MotionState::setCentreOfMassOffset(const glm::vec3& o)
{
    m_comOffset.setOrigin({ o.x, o.y, o.z });
}