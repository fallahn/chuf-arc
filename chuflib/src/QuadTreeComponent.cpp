/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/QuadTreeComponent.hpp>
#include <chuf2/QuadTreeNode.hpp>

#include <cassert>

namespace
{
    
}

using namespace chuf;

QuadTreeComponent::QuadTreeComponent(const FloatRect& bounds)
    : m_localBounds     (bounds),
    m_quadTree          (nullptr),
    m_quadTreeNode      (nullptr)
{

}

QuadTreeComponent::~QuadTreeComponent()
{
    removeFromQuadTree();
}

//public
const FloatRect& QuadTreeComponent::getLocalQuadTreeBounds() const
{
    return m_localBounds;
}

const FloatRect& QuadTreeComponent::getGlobalQuadTreeBounds() const
{
    return m_globalBounds;
}

void QuadTreeComponent::updateQuadTree()
{
    if (m_quadTreeNode) m_quadTreeNode->update(this);
}

void QuadTreeComponent::removeFromQuadTree()
{
    if (m_quadTreeNode)
    {
        m_quadTreeNode->remove(this);
        m_quadTreeNode = nullptr;
        //LOG("SERVER Removing myself from quad tree", Logger::Type::Info);
    }
    //LOG("Tried to remove myself from quad tree, but node pointer is null!", Logger::Type::Info);
}

void QuadTreeComponent::setQuadTree(QuadTree* qt)
{
    m_quadTree = qt;
}

void QuadTreeComponent::setQuadTreeNode(QuadTreeNode* qn)
{
    m_quadTreeNode = qn;
}

//protected
void QuadTreeComponent::updateLocalQuadTreeBounds(const FloatRect& lb)
{
    m_localBounds = lb;
}

void QuadTreeComponent::updateGlobalQuadTreeBounds(const FloatRect& gb)
{
    m_globalBounds = gb;
}