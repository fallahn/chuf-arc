/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/AudioSource.hpp>
#include <chuf2/Util.hpp>

using namespace chuf;

AudioSource::AudioSource()
{
    alCheck(alGenSources(1, &m_alId));
    alCheck(alSourcei(m_alId, AL_BUFFER, 0));
}

AudioSource::AudioSource(const AudioSource& other)
{
    alCheck(alGenBuffers(1, &m_alId));
    alCheck(alSourcei(m_alId, AL_BUFFER, 0));

    setPitch(other.getPitch());
    setAttenuation(other.getAttenuation());
    setPosition(other.getPosition());
    setVolume(other.getVolume());
    setRelativeToListener(other.relativeToListener());
    setMinDistance(other.getMinDistance());
}

AudioSource::~AudioSource()
{
    alCheck(alSourcei(m_alId, AL_BUFFER, 0));
    alCheck(alDeleteSources(1, &m_alId));
}

//public
void AudioSource::setPitch(float pitch)
{
    alCheck(alSourcef(m_alId, AL_PITCH, pitch));
}

float AudioSource::getPitch() const
{
    ALfloat pitch = 0.f;
    alCheck(alGetSourcef(m_alId, AL_PITCH, &pitch));
    return pitch;
}

void AudioSource::setAttenuation(float att)
{
    alCheck(alSourcef(m_alId, AL_ROLLOFF_FACTOR, att));
}

float AudioSource::getAttenuation() const
{
    ALfloat att = 0.f;
    alCheck(alGetSourcef(m_alId, AL_ROLLOFF_FACTOR, &att));
    return att;
}

void AudioSource::setPosition(const glm::vec3& pos)
{
    alCheck(alSource3f(m_alId, AL_POSITION, pos.x, pos.y, pos.z));
}

glm::vec3 AudioSource::getPosition() const
{
    glm::vec3 pos;
    alCheck(alGetSource3f(m_alId, AL_POSITION, &pos.x, &pos.y, &pos.z));
    return pos;
}

void AudioSource::setVolume(float vol)
{
    alCheck(alSourcef(m_alId, AL_GAIN, vol * 0.01f));
}

float AudioSource::getVolume() const
{
    ALfloat vol = 0.f;
    alCheck(alGetSourcef(m_alId, AL_GAIN, &vol));
    return vol;
}

void AudioSource::setRelativeToListener(bool relative)
{
    alCheck(alSourcei(m_alId, AL_SOURCE_RELATIVE, relative));
}

bool AudioSource::relativeToListener() const
{
    ALint rel = 0;
    alCheck(alGetSourcei(m_alId, AL_SOURCE_RELATIVE, &rel));
    return (rel != 0);
}

void AudioSource::setMinDistance(float dist)
{
    alCheck(alSourcef(m_alId, AL_REFERENCE_DISTANCE, dist));
}

float AudioSource::getMinDistance() const
{
    ALfloat dist = 0.f;
    alCheck(alGetSourcef(m_alId, AL_REFERENCE_DISTANCE, &dist));
    return dist;
}

//protected
AudioSource::State AudioSource::getState() const
{
    ALint state = 0;
    alCheck(alGetSourcei(m_alId, AL_SOURCE_STATE, &state));

    switch (state)
    {
    case AL_PLAYING:
        return State::Playing;
    case AL_PAUSED:
        return State::Paused;
    case AL_INITIAL:
    case AL_STOPPED:
        return State::Stopped;
    }
    return State::Stopped;
}