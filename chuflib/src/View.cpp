/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/View.hpp>
#include <chuf2/App.hpp>

using namespace chuf;

View::View(){}

View::View(const FloatRect& vp) : viewport(vp){}

glm::vec2 View::mapPixelToView(const glm::ivec2& pixel) const
{
    const auto& videoContext = App::getInstance().getVideoContext();

    float ratioX = static_cast<float>(pixel.x) / videoContext.width;
    float ratioY = static_cast<float>(pixel.y) / videoContext.height;

    glm::vec2 retVal(viewport.width * ratioX, viewport.height - (viewport.height * ratioY)); //flip coords vertically
    retVal.x += viewport.left;
    retVal.y += viewport.top;
    return retVal;
}