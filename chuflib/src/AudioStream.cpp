/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/AudioStream.hpp>
#include <chuf2/AudioListener.hpp>
#include <chuf2/Util.hpp>
#include <chuf2/Log.hpp>

using namespace chuf;

namespace
{
    using Lock = std::lock_guard<std::mutex>;
}

AudioStream::AudioStream()
    : m_threadStartState(State::Stopped),
    m_streaming         (false),
    m_channelCount      (0u),
    m_sampleRate        (0u),
    m_bufferFormat      (0u),
    m_loop              (false),
    m_samplesProcessed  (0u)
{

}

AudioStream::~AudioStream()
{
    {
        Lock lock(m_mutex);
        m_streaming = false;
    }
    m_thread->join();
}

//public
void AudioStream::play()
{
    if (m_bufferFormat == 0)
    {
        Logger::Log("failed to play audio stream, parameters not yet initialised", Logger::Type::Error);
        return;
    }

    bool streaming = false;
    State state(State::Stopped);

    {
        Lock lock(m_mutex);
        streaming = m_streaming;
        state = m_threadStartState;
    }

    if (streaming && (state == State::Paused))
    {
        Lock lock(m_mutex);
        m_threadStartState = State::Playing;
        alCheck(alSourcePlay(m_alId));
        return;
    }
    else if (streaming && (state == State::Playing))
    {
        stop();
    }

    onSeek(0.f);

    m_samplesProcessed = 0u;
    m_streaming = true;
    m_threadStartState = State::Playing;
    m_thread = std::make_unique<std::thread>(std::bind(&AudioStream::streamData, this));
}


void AudioStream::pause()
{
    {
        Lock lock(m_mutex);

        if (!m_streaming) return;

        m_threadStartState = State::Paused;
    }

    alCheck(alSourcePause(m_alId));
}

void AudioStream::stop()
{
    {
        Lock lock(m_mutex);
        m_streaming = false;
    }

    m_thread->join();

    onSeek(0.f);
    m_samplesProcessed = 0u;
}

void AudioStream::setLooped(bool looped)
{
    m_loop = looped;
}

bool AudioStream::looped() const
{
    return m_loop;
}

AudioSource::State AudioStream::getState() const
{
    auto state = AudioSource::getState();

    if (state == State::Stopped)
    {
        Lock lock(m_mutex);
        if (m_streaming) state = m_threadStartState;
    }
    return state;
}

UInt32 AudioStream::getSampleRate() const
{
    return m_sampleRate;
}

UInt32 AudioStream::getChannelCount() const
{
    return m_channelCount;
}

void AudioStream::setPlayOffset(float offset)
{
    auto oldState = getState();

    stop();

    onSeek(offset);

    m_samplesProcessed = static_cast<UInt64>(offset * m_sampleRate * m_channelCount);

    if (oldState == State::Stopped) return;

    m_streaming = true;
    m_threadStartState = oldState;
    m_thread = std::make_unique<std::thread>(std::bind(&AudioStream::streamData, this));
}

float AudioStream::getPlayOffset() const
{
    if (m_sampleRate && m_channelCount)
    {
        ALfloat time = 0.f;
        alCheck(alGetSourcef(m_alId, AL_SEC_OFFSET, &time));
        return time + static_cast<float>(m_samplesProcessed) / m_sampleRate / m_channelCount;
    }
    return 0.f;
}

//protected
void AudioStream::init(UInt32 channelCount, UInt32 sampleRate)
{
    m_channelCount = channelCount;
    m_sampleRate = sampleRate;

    m_bufferFormat = AudioListener::getFormatFromChannelCount(channelCount);

    if (m_bufferFormat <= 0)
    {
        m_channelCount = 0u;
        m_sampleRate = 0u;
        Logger::Log("AudioStream init: invalid channel count", Logger::Type::Error);
    }
}


//private
bool AudioStream::fillQueue()
{
    bool stop = false;
    for (auto i = 0; (i < BufferCount) && !stop; ++i)
    {
        if (fillAndPushBuffer(i)) stop = true;
    }
    return stop;
}

void AudioStream::clearQueue()
{
    ALint queueCount = 0;
    alCheck(alGetSourcei(m_alId, AL_BUFFERS_QUEUED, &queueCount));

    ALuint buffer = 0;
    for (ALint i = 0; i < queueCount; ++i)
    {
        alCheck(alSourceUnqueueBuffers(m_alId, 1, &buffer));
    }
}

bool AudioStream::fillAndPushBuffer(UInt32 bufferId)
{
    bool stop = false;

    Chunk data = { nullptr, 0 };
    if (!onGetData(data))
    {
        m_endBuffers[bufferId] = true;

        if (m_loop)
        {
            onSeek(0.f);

            if (!data.data || (data.sampleCount == 0))
            {
                return fillAndPushBuffer(bufferId);
            }
        }
        else
        {
            stop = true;
        }
    }

    if (data.data && data.sampleCount)
    {
        UInt32 buffer = m_buffers[bufferId];

        ALsizei size = static_cast<ALsizei>(data.sampleCount) * sizeof(Int16);
        alCheck(alBufferData(buffer, m_bufferFormat, data.data, size, m_sampleRate));

        alCheck(alSourceQueueBuffers(m_alId, 1, &buffer));
    }

    return stop;
}

void AudioStream::streamData()
{
    bool stop = false;

    {
        Lock lock(m_mutex);

        if (m_threadStartState == State::Stopped)
        {
            m_streaming = false;
            return;
        }
    }

    alCheck(alGenBuffers(BufferCount, &m_buffers[0]));
    for (auto& b : m_endBuffers) b = false;

    stop = fillQueue();

    alCheck(alSourcePlay(m_alId));

    {
        Lock lock(m_mutex);
        if (m_threadStartState == State::Paused)
        {
            alCheck(alSourcePause(m_alId));
        }
    }

    for (;;)
    {
        {
            Lock lock(m_mutex);
            if (!m_streaming) break;
        }

        if (AudioSource::getState() == State::Stopped)
        {
            if (!stop)
            {
                alCheck(alSourcePlay(m_alId));
            }
            else
            {
                Lock lock(m_mutex);
                m_streaming = false;
            }
        }

        ALint processedCount = 0;
        alCheck(alGetSourcei(m_alId, AL_BUFFERS_PROCESSED, &processedCount));

        while (processedCount--)
        {
            ALuint buffer = 0;
            alCheck(alSourceUnqueueBuffers(m_alId, 1, &buffer));

            UInt32 buffNum = 0;
            for (auto i = 0; i < BufferCount; ++i)
            {
                if (m_buffers[i] == buffer)
                {
                    buffNum = i;
                    break;
                }
            }

            if (m_endBuffers[buffNum])
            {
                m_samplesProcessed = 0u;
                m_endBuffers[buffNum] = false;
            }
            else
            {
                ALint size = 0;
                ALint bits = 0;

                alCheck(alGetBufferi(buffer, AL_SIZE, &size));
                alCheck(alGetBufferi(buffer, AL_BITS, &bits));

                if (bits == 0)
                {
                    Lock lock(m_mutex);
                    m_streaming = false;
                    stop = true;
                    break;
                }
                else
                {
                    m_samplesProcessed += size / (bits / 8);
                }
            }

            if (!stop)
            {
                if (fillAndPushBuffer(buffNum)) stop = true;
            }
        }

        if (AudioSource::getState() != State::Stopped)
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }
    }

    alCheck(alSourceStop(m_alId));

    clearQueue();

    alCheck(alSourcei(m_alId, AL_BUFFER, 0));
    alCheck(alDeleteBuffers(BufferCount, &m_buffers[0]));
}
