/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/AudioFile.hpp>
#include <chuf2/Log.hpp>

#include <sstream>
#include <cctype>

using namespace chuf;

AudioFile::AudioFile()
    : m_file        (nullptr),
    m_sampleCount   (0u),
    m_sampleRate    (0u),
    m_channelCount  (0u)
{

}

AudioFile::~AudioFile()
{
    if (m_file)
        sf_close(m_file);
}

//public
bool AudioFile::open(const std::string& path)
{
    if (m_file) sf_close(m_file);

    SF_INFO fileInfo;
    fileInfo.format = 0;
    m_file = sf_open(path.c_str(), SFM_READ, &fileInfo);

    if (!m_file)
    {
        std::stringstream ss;
        ss << "failed to open " << path << ". reason: " << sf_strerror(m_file) << std::endl;
        Logger::Log(ss.str(), Logger::Type::Error);
        return false;
    }

    init(fileInfo);
    return true;
}

bool AudioFile::open(const void* data, UInt32 size)
{
    if (m_file) sf_close(m_file);

    SF_VIRTUAL_IO io;
    io.get_filelen = &Memory::getLength;
    io.read = &Memory::read;
    io.seek = &Memory::seek;
    io.tell = &Memory::tell;

    m_memory.begin = static_cast<const char*>(data);
    m_memory.current = m_memory.begin;
    m_memory.size = size;

    SF_INFO fileInfo;
    fileInfo.format = 0;

    m_file = sf_open_virtual(&io, SFM_READ, &fileInfo, &m_memory);
    if (!m_file)
    {
        std::stringstream ss;
        ss << "failed to open sound from memory: " << sf_strerror(m_file) << std::endl;
        Logger::Log(ss.str(), Logger::Type::Error);
        return false;
    }

    init(fileInfo);
    return true;
}

UInt32 AudioFile::read(Int16* data, UInt32 sampleCount)
{
    if (m_file && data && sampleCount)
        return static_cast<UInt32>(sf_read_short(m_file, data, sampleCount));
    return 0u;
}

void AudioFile::seek(float offset)
{
    if (m_file)
    {
        sf_count_t fileOffset = static_cast<sf_count_t>(offset * m_sampleRate);
        sf_seek(m_file, fileOffset, SEEK_SET);
    }
}

UInt32 AudioFile::getSampleCount() const
{
    return m_sampleCount;
}

UInt32 AudioFile::getSampleRate() const
{
    return m_sampleRate;
}

UInt32 AudioFile::getChannelCount() const
{
    return m_channelCount;
}

//private
void AudioFile::init(SF_INFO fileInfo)
{
    m_channelCount = fileInfo.channels;
    m_sampleRate = fileInfo.samplerate;
    m_sampleCount = static_cast<UInt32>(fileInfo.frames * fileInfo.channels);
}

Int32 AudioFile::getFormatFromExtension(const std::string& filename)
{
    std::string extension("wav");

    auto pos = filename.find_last_of(".");
    if (pos != std::string::npos)
    {
        extension = filename.substr(pos + 1);
        for (auto& c : extension)
        {
            c = std::tolower(c);
        }
    }

    if (extension == "wav") return SF_FORMAT_WAV;
    if (extension == "aif") return SF_FORMAT_AIFF;
    if (extension == "aiff") return SF_FORMAT_AIFF;
    if (extension == "au") return SF_FORMAT_AU;
    if (extension == "raw") return SF_FORMAT_RAW;
    if (extension == "flac") return SF_FORMAT_FLAC;
    if (extension == "ogg") return SF_FORMAT_OGG;

    return -1;
}


//----memory struct----//
sf_count_t AudioFile::Memory::getLength(void* usr)
{
    Memory* m = static_cast<Memory*>(usr);
    return m->size;
}

sf_count_t AudioFile::Memory::read(void* ptr, sf_count_t count, void* usr)
{
    Memory* m = static_cast<Memory*>(usr);

    sf_count_t pos = tell(usr);
    if (pos + count >= m->size)
    {
        count = m->size - pos;
    }

    std::memcpy(ptr, m->current, static_cast<std::size_t>(count));
    m->current += count;
    return count;
}

sf_count_t AudioFile::Memory::seek(sf_count_t offset, int when, void* usr)
{
    Memory* m = static_cast<Memory*>(usr);
    sf_count_t pos = 0;
    switch (when)
    {
    case SEEK_SET:
        pos = offset;
        break;
    case SEEK_CUR:
        pos = m->current - m->begin + offset;
        break;
    case SEEK_END:
        pos = m->size - offset;
        break;
    default:
        pos = 0;
        break;
    }

    if (pos >= m->size)
    {
        pos = m->size - 1;
    }
    else if (pos < 0)
    {
        pos = 0;
    }

    m->current = m->begin + pos;
    return pos;
}

sf_count_t AudioFile::Memory::tell(void* usr)
{
    Memory* m = static_cast<Memory*>(usr);
    return m->current - m->begin;
}