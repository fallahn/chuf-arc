/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/Texture.hpp>

#include <chuf2/Log.hpp>
#include <chuf2/Util.hpp>

#include <cassert>

using namespace chuf;

namespace
{
    TextureID currentID = 0;
}

Texture::Sampler::Sampler(Texture* texture, const Texture::Sampler::Key& key)
    : m_texture	(texture),
    m_wrapS		(Repeat),
    m_wrapT		(Repeat)
{
    assert(texture);
    m_minFilter = texture->m_minFilter;
    m_magFilter = texture->m_magFilter;
}

//public
Texture::Sampler::Ptr Texture::Sampler::create(Texture* texture)
{
    assert(texture);
    return std::make_shared<Sampler>(texture, Texture::Sampler::Key());
}

Texture::Sampler::Ptr Texture::Sampler::create(const std::string& path, bool createMipmap, bool flipVertically)
{
    auto t = Texture::create(path, createMipmap, flipVertically);
    return t ? std::make_shared<Sampler>(t, Texture::Sampler::Key()) : nullptr;
}

void Texture::Sampler::setWrap(Wrap wrapS, Wrap wrapT)
{
    m_wrapS = wrapS;
    m_wrapT = wrapT;
}

void Texture::Sampler::setFilter(Filter min, Filter mag)
{
    m_minFilter = min;
    m_magFilter = mag;
}

void Texture::Sampler::setTexture(Texture* t)
{
    assert(t);
    m_texture = t;
}

Texture* Texture::Sampler::getTexture() const
{
    return m_texture;
}

void Texture::Sampler::bind()
{
    assert(m_texture);

    //auto id = m_texture->getHandle();
    //if (currentID != id)
    //{
    //    glCheck(glBindTexture(m_texture->m_type, id));
    //    currentID = id;
    //}
    glCheck(glBindTexture(m_texture->m_type, m_texture->getHandle()));

    if (m_texture->m_wrapS != m_wrapS)
    {
        glCheck(glTexParameteri(m_texture->m_type, GL_TEXTURE_WRAP_S, static_cast<GLenum>(m_wrapS)));
        m_texture->m_wrapS = m_wrapS;
    }
    if (m_texture->m_wrapT != m_wrapT)
    {
        glCheck(glTexParameteri(m_texture->m_type, GL_TEXTURE_WRAP_T, static_cast<GLenum>(m_wrapT)));
        m_texture->m_wrapT = m_wrapT;
    }
    if (m_texture->m_minFilter != m_minFilter)
    {
        glCheck(glTexParameteri(m_texture->m_type, GL_TEXTURE_MIN_FILTER, static_cast<GLenum>(m_minFilter)));
        m_texture->m_minFilter = m_minFilter;
    }
    if (m_texture->m_magFilter != m_magFilter)
    {
        glCheck(glTexParameteri(m_texture->m_type, GL_TEXTURE_MAG_FILTER, static_cast<GLenum>(m_magFilter)));
        m_texture->m_magFilter = m_magFilter;
    }
}

//private