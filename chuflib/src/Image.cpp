/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/Image.hpp>
#include <chuf2/Log.hpp>
#include <chuf2/Util.hpp>
#include <chrono>

//use the lovely stb_image from
//http://nothings.org/
//#ifndef STB_IMAGE_IMPLEMENTATION
//#define STB_IMAGE_IMPLEMENTATION
//#define STBI_ASSERT(x)
#define STB_IMAGE_WRITE_IMPLEMENTATION
#define STB_IMAGE_RESIZE_IMPLEMENTATION
#define STBIR_DEFAULT_FILTER_UPSAMPLE    STBIR_FILTER_CUBICBSPLINE
//#endif //STB_IMAGE_IMPLEMENTAION
#include <chuf2/stbi/stb_image.h>
#include <chuf2/stbi/stb_image_write.h>
#include <chuf2/stbi/stb_image_resize.h>

#include <cassert>
#include <cctype>

using namespace chuf;

Image::Image(const Image::Key& key)
    : m_format			(Format::Invalid),
    m_width				(0u),
    m_height			(0u),
    m_flippedVertically	(false)
{

}

//public
Image::Ptr Image::create(const std::string& path)
{	
    Int32 w, h, bpp;
    UInt8* pxDat = stbi_load(path.c_str(), &w, &h, &bpp, /*STBI_rgb_alpha*/0);
    if (pxDat && w && h)
    {
        auto size = w * h * bpp;
        auto image = std::make_unique<Image>(Image::Key());
        image->m_width = w;
        image->m_height = h;
        image->m_format = (bpp == 3) ? Format::RGB : Format::RGBA;
        image->m_imageData.resize(size);
        std::memcpy(image->m_imageData.data(), pxDat, size);
        image->m_uid = path;
        stbi_image_free(pxDat);

        return std::move(image);
    }
    else
    {
        Logger::Log("Failed to load image " + path + "\nReason: " + stbi_failure_reason(), Logger::Type::Warning);
    }
return nullptr;
}

std::vector<Image::Ptr> Image::createCubemap(const std::string& path)
{
    std::vector<Image::Ptr> images;
    Int32 w, h, bpp;
    UInt8* pxDat = stbi_load(path.c_str(), &w, &h, &bpp, 0);
    if (pxDat && w && h)
    {
        assert(w > 3 && h > 2);
        Int32 subWidth = w / 4;
        Int32 subHeight = h / 3;

        std::vector<UIntRect> subRects =
        {
            UIntRect(subWidth * 2u, subHeight, subWidth, subHeight), //posX
            UIntRect(0u, subHeight, subWidth, subHeight), //negX
            UIntRect(subWidth, 0u, subWidth, subHeight), //posY
            UIntRect(subWidth, subHeight * 2u, subWidth, subHeight), //negY
            UIntRect(subWidth, subHeight, subWidth, subHeight), //posZ
            UIntRect(subWidth * 3u, subHeight, subWidth, subHeight) //negz
        };
        UIntRect srcRect(0u, 0u, w, h);

        Int32 size = subWidth * subHeight * bpp;
        for (auto& dstRect : subRects)
        {
            auto image = std::make_unique<Image>(Image::Key());
            image->m_width = subWidth;
            image->m_height = subHeight;
            image->m_format = (bpp == 3) ? Format::RGB : Format::RGBA;
            image->m_uid = path;
            image->m_imageData.resize(size);
            copySubrect(image->m_imageData.data(), pxDat, dstRect, srcRect, bpp);
            images.push_back(std::move(image));
        }
        stbi_image_free(pxDat);
    }
    else
    {
        Logger::Log("Failed to load \'" + path + "\'. Cubemap images not created", Logger::Type::Warning);
    }
    return images;
}

Image::Ptr Image::create(UInt32 width, UInt32 height, Image::Format format, UInt8* data)
{
    assert(width > 0 && height > 0);
    UInt8 bytesPerPixel = static_cast<UInt8>(format);
    UInt32 imageSize = width * height * bytesPerPixel;

    auto image = std::make_unique<Image>(Image::Key());
    image->m_format = format;
    image->m_width = width;
    image->m_height = height;
    image->m_imageData.resize(imageSize);
    if (data)
    {
        std::memcpy(image->m_imageData.data(), data, imageSize);	
    }
    else
    {
        std::memset(image->m_imageData.data(), 255u, imageSize);
    }

    image->calcUid(); //we don't have a path so create a unique number
    return std::move(image);
}

Image::Ptr Image::create(UInt32 width, UInt32 height, Image::Format format, Colour colour)
{
    if (format == Format::Invalid) return nullptr;
    
    auto image = create(width, height, format);
    auto stride = static_cast<UInt32>(format);
    auto size = width * height * stride;

    auto data = image->m_imageData.data();
    for (auto i = 0u; i < size; )
    {
        if (format == Format::A)
        {
            data[i++] = colour.alphaAsByte();
        }
        else
        {
            data[i++] = colour.redAsByte();
            data[i++] = colour.greenAsByte();
            data[i++] = colour.blueAsByte();

            if (stride > 3)
            {
                data[i++] = colour.alphaAsByte();
            }
        }
    }
    image->m_uid = std::to_string(colour.packed());
    return std::move(image);
}

Image::Ptr Image::createChequer()
{
    //creates a magenta / black chequer pattern for debugging
    
    UInt32 width = 64u;
    UInt32 height = width;
    UInt32 checkCount = 4u;
    UInt32 checkSize = width / checkCount;

    UInt32 i = 0u;
    bool pink = true;

    std::vector<UInt8> data(width * height * 4u);
    for (auto j = 0u; j < checkCount; ++j)
    {
        for (auto k = 0u; k < checkCount; ++k)
        {
            for (auto l = 0u; l < checkSize; ++l)
            {
                for (auto m = 0u; m < checkSize; ++m)
                {
                    if (pink)
                    {
                        data[i++] = 255u;
                        data[i++] = 0u;
                        data[i++] = 255u;
                        data[i++] = 255u;
                    }
                    else
                    {
                        data[i++] = 0u;
                        data[i++] = 0u;
                        data[i++] = 0u;
                        data[i++] = 255u;
                    }
                }
                pink = !pink;
            }
        }
        pink = !pink;
    }
    return std::move(create(width, height, Image::Format::RGBA, data.data()));
}

const UInt8* Image::getData() const
{
    return m_imageData.data();
}

Image::Format Image::getFormat() const
{
    return m_format;
}

UInt32 Image::getWidth() const
{
    return m_width;
}

UInt32 Image::getHeight() const
{
    return m_height;
}

const std::string& Image::getUid() const
{
    return m_uid;
}

void Image::flipVertically()
{
    assert(m_format != Format::Invalid);

    auto rowSize = m_width * static_cast<UInt8>(m_format);
    auto size = rowSize * m_height;
    std::vector<UInt8> newData(size);
    auto i = 0u;

    for (auto y = 1u; y <= m_height; ++y)
    {
        auto start = size - (y * rowSize);
        std::memcpy(&newData[i], &m_imageData[start], rowSize);
        i += rowSize;
    }

    m_imageData = std::move(newData);
    m_flippedVertically = !m_flippedVertically;
}

bool Image::flippedVertically() const
{
    return m_flippedVertically;
}

bool Image::isPowerTwo() const
{
    assert(m_width && m_height);
    return ((m_width & (m_width - 1)) == 0) && ((m_height & (m_height - 1)) == 0);
}

void Image::save(const std::string& path) const
{
    assert(path.length() > 3u);
    if (m_width > 0 && m_height > 0)
    {
        std::string ext = path.substr(path.length() - 3);
        for (auto& c : ext)
            c = std::tolower(c);

        std::string output = path;
        output.replace(path.length() - 3, 3, ext);

        if (ext == "bmp")
        {
            stbi_write_bmp(output.c_str(), m_width, m_height, static_cast<int>(m_format), &m_imageData[0]);
        }
        else if (ext == "tga")
        {
            stbi_write_tga(output.c_str(), m_width, m_height, static_cast<int>(m_format), &m_imageData[0]);
        }
        else if (ext == "png")
        {
            stbi_write_png(output.c_str(), m_width, m_height, static_cast<int>(m_format), &m_imageData[0], 0);
        }
        else
        {
            Logger::Log("\'" + ext + "\' invalid file extension, valid types are: bmp, tga or png. Image not written.", Logger::Type::Error);
        }
    }
}

void Image::setSubrect(const glm::vec2& dst, const Image& src)
{
    assert(dst.x + src.m_width <= m_width);
    assert(dst.y + src.m_height <= m_height);
    assert(src.m_format != Format::Invalid);

    auto byteCount = static_cast<UInt8>(src.m_format);
    auto start = m_width * static_cast<UInt32>(dst.y) * byteCount + (static_cast<UInt32>(dst.x) * byteCount);
    auto rowWidth = src.m_width * static_cast<Uint8>(src.m_format);
    for (auto i = 0u; i < src.m_height; ++i)
    {
        std::memcpy(m_imageData.data() + start, src.m_imageData.data() + (i * rowWidth), rowWidth);
        start += m_width * byteCount;
    }
}

void Image::resize(UInt32 width, UInt32 height)
{
    assert(width > 0 && height > 0);
    assert(!m_imageData.empty());

    UInt8 bpp = static_cast<UInt8>(m_format);
    std::vector<chuf::UInt8> newData(width * height * bpp);

    //---------------------------Nearest Neighbour---------------------------//
    //double scaleWidth = static_cast<double>(width) / static_cast<double>(m_width);
    //double scaleHeight = static_cast<double>(height) / static_cast<double>(m_height);

    //for (int cy = 0; cy < height; cy++)
    //{
    //    for (int cx = 0; cx < width; cx++)
    //    {
    //        int pixel = (cy * (width * bpp)) + (cx * bpp);
    //        int nearestMatch = ((static_cast<int>(cy / scaleHeight) * (m_width * bpp)) + (static_cast<int>(cx / scaleWidth) * bpp));

    //        newData[pixel] = m_imageData[nearestMatch];
    //        newData[pixel + 1] = m_imageData[nearestMatch + 1];
    //        newData[pixel + 2] = m_imageData[nearestMatch + 2];
    //    }
    //}
    //-----------------------------------------------------------------------//

    stbir_resize_uint8(m_imageData.data(), m_width, m_height, 0,
                        newData.data(), width, height, 0, bpp);

    m_imageData = std::move(newData);
    m_width = width;
    m_height = height;
}

void Image::adjustGamma(float amount)
{
    assert(m_format != Format::Invalid);

    for (auto i = 0u; i < m_imageData.size(); ++i)
    {
        if (m_format == Format::RGBA && (i % 4 == 0))
        {
            //skip the alpha channel
            continue;
        }
        float scale = 1.f;
        float temp = 0.f;

        float currentByte = static_cast<float>(m_imageData[i]);
        currentByte *= amount / 255.f;

        //clamp to max value
        if (currentByte > 1.f && (temp = (1.f / currentByte)) < scale) scale = temp;

        currentByte *= scale * 255.f;
        m_imageData[i] = static_cast<UInt8>(currentByte);
    }
}

void Image::blur(UInt16 radius)
{
    assert(radius < m_width / 2 && radius < m_height / 2);
    assert(m_format != Format::Invalid);

    //blur horizontal
    blurPass({ 1.f, 0.f }, radius);

    //blur vertical
    blurPass({ 0.f, 1.f }, radius);

}

//private
void Image::calcUid()
{
    auto time = std::chrono::system_clock::now();
    m_uid += std::to_string(time.time_since_epoch().count()) + std::to_string(Util::Random::value(10, 324532));
}

void Image::copySubrect(UInt8* dst, UInt8* src, const chuf::UIntRect& dstRect, const chuf::UIntRect& srcRect, Int32 bpp)
{
    assert(dstRect.left <= (srcRect.width - dstRect.width)
        && dstRect.top <= (srcRect.height - dstRect.height));
    
    auto i = 0;

    auto srcWidth = (srcRect.width * bpp);
    auto dstWidth = (dstRect.width * bpp);
    auto start = (srcWidth * dstRect.top) + (dstRect.left * bpp);
    
    //TODO this would be much better using memcpy a row at a time
    for (auto y = 0u; y < dstRect.height; ++y)
    {
        for (auto x = 0u; x < dstWidth; x += bpp)
        {
            auto j = start + (y * srcWidth) + x;
            for (auto k = 0; k < bpp; ++k)
            {
                //Logger::Log(std::to_string(i) + ", " + std::to_string(src[j]) + ", ");
                dst[i++] = src[j++];
            }
        }
        //Logger::Log("\n");
    }
}

UInt8* Image::getPixel(Int32 x, Int32 y)
{
    x = (x < 0) ? x += m_width : (x >= m_width) ? x -= m_width : x;
    y = (y < 0) ? y += m_height : (y >= m_height) ? y -= m_height : y;

    auto bpp = static_cast<UInt8>(m_format);
    auto index = (y * (m_width * bpp)) + (x * bpp);
    return &m_imageData[index];
}

void Image::setPixel(Int32 x, Int32 y, const std::vector<float>& normalisedPixel)
{
    auto bpp = static_cast<UInt8>(m_format);
    UInt8* dest = &m_imageData[(y * (m_width * bpp)) + (x * bpp)];
    for (auto i = 0u; i < normalisedPixel.size(); ++i)
    {
        dest[i] = static_cast<UInt8>(normalisedPixel[i] * 255.f);
    }
}

void Image::blurPass(const glm::vec2& dir, UInt16 radius)
{
    auto bpp = static_cast<UInt16>(m_format);

    for (auto y = 0u; y < m_height; ++y)
    {
        for (auto x = 0u; x < m_width; ++x)
        {
            std::vector<float> sum(bpp); //holds the resulting pixel
            std::memset(sum.data(), 0, sum.size());

            //make sure recieved px vals are normalised
            auto pixel = getPixel(x - 4 * radius * dir.x, y - 4 * radius * dir.y);
            for (auto i = 0u; i < bpp; ++i) sum[i] += static_cast<float>(pixel[i]) / 255.f * 0.0162162162f;
            //sum += texture2D(u_texture, vec2(x - 4*radius*dir.x, y - 4*radius*dir.y)) * 0.0162162162;
            pixel = getPixel(x - 3 * radius * dir.x, y - 3 * radius * dir.y);
            for (auto i = 0u; i < bpp; ++i) sum[i] += static_cast<float>(pixel[i]) / 255.f * 0.0540540541f;
            //sum += texture2D(u_texture, vec2(x - 3*radius*dir.x, y - 3*radius*dir.y)) * 0.0540540541;
            pixel = getPixel(x - 2 * radius * dir.x, y - 2 * radius * dir.y);
            for (auto i = 0u; i < bpp; ++i) sum[i] += static_cast<float>(pixel[i]) / 255.f * 0.1216216216f;
            //sum += texture2D(u_texture, vec2(x - 2*radius*dir.x, y - 2*radius*dir.y)) * 0.1216216216;
            pixel = getPixel(x - 1 * radius * dir.x, y - 1 * radius * dir.y);
            for (auto i = 0u; i < bpp; ++i) sum[i] += static_cast<float>(pixel[i]) / 255.f * 0.1945945946f;
            //sum += texture2D(u_texture, vec2(x - 1*radius*dir.x, y - 1*radius*dir.y)) * 0.1945945946;

            pixel = getPixel(x, y);
            for (auto i = 0u; i < bpp; ++i) sum[i] += static_cast<float>(pixel[i]) / 255.f * 0.2270270270f;
            //sum += texture2D(u_texture, vec2(x, y)) * 0.2270270270;

            pixel = getPixel(x + 1 * radius * dir.x, y + 1 * radius * dir.y);
            for (auto i = 0u; i < bpp; ++i) sum[i] += static_cast<float>(pixel[i]) / 255.f * 0.1945945946f;
            //sum += texture2D(u_texture, vec2(x + 1*radius*dir.x, y + 1*radius*dir.y)) * 0.1945945946;
            pixel = getPixel(x + 2 * radius * dir.x, y + 2 * radius * dir.y);
            for (auto i = 0u; i < bpp; ++i) sum[i] += static_cast<float>(pixel[i]) / 255.f * 0.1216216216f;
            //sum += texture2D(u_texture, vec2(x + 2*radius*dir.x, y + 2*radius*dir.y)) * 0.1216216216;
            pixel = getPixel(x + 3 * radius * dir.x, y + 3 * radius * dir.y);
            for (auto i = 0u; i < bpp; ++i) sum[i] += static_cast<float>(pixel[i]) / 255.f * 0.0540540541f;
            //sum += texture2D(u_texture, vec2(x + 3*radius*dir.x, y + 3*radius*dir.y)) * 0.0540540541;
            pixel = getPixel(x + 4 * radius * dir.x, y + 4 * radius * dir.y);
            for (auto i = 0u; i < bpp; ++i) sum[i] += static_cast<float>(pixel[i]) / 255.f * 0.0162162162f;
            //sum += texture2D(u_texture, vec2(x + 4*radius*dir.x, y + 4*radius*dir.y)) * 0.0162162162;

            setPixel(x, y, sum);
        }
    }
}