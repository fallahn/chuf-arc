/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/Shader.hpp>
#include <chuf2/Colour.hpp>
#include <chuf2/Cache.hpp>
#include <chuf2/Util.hpp>
#include <chuf2/Log.hpp>

#include <chuf2/glm/gtc/type_ptr.hpp>

#include <istream>
#include <array>

using namespace chuf;
namespace
{
    Cache<Shader> shaderCache;
    Shader* currentShader = nullptr;
    const UInt32 sourceArraySize = 3u;

    std::string createName(const std::string& vertShader, const std::string& fragShader)
    {
        const UInt16 minLength = 26u; //TODO what if a shader is less than 26 chars long?
        UInt16 length1 = vertShader.length();
        UInt16 length2 = fragShader.length();

        auto stride1 = length1 / minLength;
        auto stride2 = length2 / minLength;

        std::string name;
        for (auto i = 0u; i < length1; i += stride1)
            name.push_back(vertShader[i]);

        for (auto i = 0u; i < length2; i += stride2)
            name.push_back(fragShader[i]);

        Util::String::removeChar(name, '\n');
        Util::String::removeChar(name, '\r');
        return name;
    }

    std::string readFile(const std::string& path)
    {
        std::string str;
        std::ifstream file(path, std::ios_base::binary);
        if (file)
        {
            file.seekg(0, std::ios_base::end);
            auto size = file.tellg();
            if (size > 0)
            {
                file.seekg(0, std::ios_base::beg);
                str.resize(static_cast<unsigned>(size));
                file.read(&str[0], size);
            }
        }
        else
        {
            Logger::Log("Failed to open file: " + path, Logger::Type::Error, Logger::Output::All);
        }

        assert(!str.empty());
        return std::move(str);
    }
}

const Shader::Uniform::Ptr Shader::emptyUniform = std::make_unique<Shader::Uniform>(Shader::Key());


Shader::Shader(const Key& key)
    : m_programID(0u)
{}

Shader::~Shader()
{
    if (m_programID)
    {
        if (currentShader == this)
        {
            glCheck(glUseProgram(0));
            currentShader = nullptr;
        }
        glCheck(glDeleteProgram(m_programID));
        m_programID = 0u;
        //LOG("Successfully Destroyed Shader " + m_name);
    }
}

Shader* Shader::createFromSource(SourceList& sources, const std::string& defines)
{
    std::string vertShader;
    if (sources.find(Type::Vertex) != sources.end()
        && !sources[Type::Vertex].empty())
    {
        vertShader = std::move(sources[Type::Vertex]);
    }
    else
    {
        Logger::Log("Cannot create shader without vertex shader source", Logger::Type::Error);
        return nullptr;
    }
    
    std::string fragShader;
    if (sources.find(Type::Fragment) != sources.end()
        && !sources[Type::Fragment].empty())
    {
        fragShader = std::move(sources[Type::Fragment]);
    }
    else
    {
        Logger::Log("Cannot create shader without fragment shader source", Logger::Type::Error);
        return nullptr;
    }

    //make sure we didn't pass a path in by mistake - this doesn't really cover all cases, but is better than nothing
    if (vertShader.find(".vert") != std::string::npos)
    {
        Logger::Log("found vertex file extension while loading shader. Did you mean to use createFromFile()?", Logger::Type::Warning);
    }
    if (fragShader.find(".frag") != std::string::npos)
    {
        Logger::Log("found fragment file extension while loading shader. Did you mean to use createFromFile()?", Logger::Type::Warning);
    }
    
    //check cache to see if shader already exists
    std::string name = createName(defines + vertShader, defines + fragShader);
    auto result = shaderCache.find(name);
    if (result) return result;
    
    //make array of 3 strings. array[1] = list of defines, array[2] = source from file    
    const GLchar* sourceArray[sourceArraySize];
    sourceArray[0] = "#version 330\n";
    sourceArray[1] = defines.c_str();
    sourceArray[2] = vertShader.c_str();

    //compile vertex shader
    glCheck(GLuint vertShaderID = glCreateShader(GL_VERTEX_SHADER));
    if (!compileVertexShader(vertShaderID, sourceArray))
    {
        glCheck(glDeleteShader(vertShaderID));
        return nullptr;
    }

    //compile fragment shader
    sourceArray[2] = fragShader.c_str();
    glCheck(GLint fragShaderID = glCreateShader(GL_FRAGMENT_SHADER));

    if (!compileFragmentShader(fragShaderID, sourceArray))
    {
        glCheck(glDeleteShader(vertShaderID));
        glCheck(glDeleteShader(fragShaderID));
        return nullptr;
    }
    
    //check for tesselation / geometry shaders
    GLint geomShaderID = -1;
    if (sources.find(Type::Geometry) != sources.end()
        && !sources[Type::Geometry].empty())
    {
        sourceArray[2] = sources[Type::Geometry].c_str();
        glCheck(geomShaderID = glCreateShader(GL_GEOMETRY_SHADER));

        if (!compileGeometryShader(geomShaderID, sourceArray))
        {
            glCheck(glDeleteShader(vertShaderID));
            glCheck(glDeleteShader(fragShaderID));
            glCheck(glDeleteShader(geomShaderID));
            return nullptr;
        }
    }

    return linkProgram(name, vertShaderID, fragShaderID, geomShaderID);
}

Shader* Shader::create(const std::string& vertShader, const std::string& fragShader, const std::string& defines)
{
    std::string vertString = readFile(vertShader);
    std::string fragString = readFile(fragShader);

    //TODO string parsing on things such as includes

    SourceList sourceList;
    sourceList.insert(std::make_pair(Type::Vertex, std::move(vertString)));
    sourceList.insert(std::make_pair(Type::Fragment, std::move(fragString)));
    return createFromSource(sourceList, defines);
}

VertexAttributeID Shader::getVertexAttribute(const std::string& name) const
{
    auto result = m_vertexAttributes.find(name);
    return (result != m_vertexAttributes.end()) ? result->second : -1;
}

Shader::Uniform* Shader::getUniform(const std::string& name, std::size_t hash) const
{
    auto result = m_uniforms.find(hash);
    if (result != m_uniforms.end()) return result->second.get();

    GLint uniformLocation;
    glCheck(uniformLocation = glGetUniformLocation(m_programID, name.c_str()));

    if (uniformLocation > -1)
    {
        std::string arrayName = name;
        auto result = arrayName.find_first_of('[');
        if (result != std::string::npos)
        {
            hash = m_hashfunc(arrayName);
            auto u = m_uniforms.find(hash);
            if (u != m_uniforms.end())
            {
                const auto& aUniform = *u->second;
                Uniform::Ptr uniform = std::make_unique<Uniform>(Shader::Key());
                uniform->m_shader = const_cast<Shader*>(this);
                uniform->m_name = name;
                uniform->m_location = uniformLocation;
                uniform->m_index = 0u;
                uniform->m_type = aUniform.getType();
                auto retVal = uniform.get();
                m_uniforms.insert(std::make_pair(hash, std::move(uniform)));

                return retVal;
            }
        }
    }
    return nullptr;
}

Shader::Uniform* Shader::getUniform(UInt32 index) const
{
    UInt32 i = 0;
    for (auto& u : m_uniforms)
    {
        if (i == index) return u.second.get();
        ++i;
    }
    return nullptr;
}

UInt32 Shader::getUniformCount() const
{
    return m_uniforms.size();
}

void Shader::setUniform(const Shader::Uniform* u, float value)
{
    assert(u);
    glCheck(glUniform1f(u->m_location, value));
}

void Shader::setUniform(const Shader::Uniform* u, const float* data, UInt32 size, UInt32 stride)
{
    assert(u);
    assert(data);
    switch (stride)
    {
    case 1:
        glCheck(glUniform1fv(u->m_location, size, data));
        break;
    case 2:
        glCheck(glUniform2fv(u->m_location, size, data));
        break;
    case 3:
        glCheck(glUniform3fv(u->m_location, size, data));
        break;
    case 4:
        glCheck(glUniform4fv(u->m_location, size, data));
        break;
    case 9:
        glCheck(glUniformMatrix3fv(u->m_location, size, GL_FALSE, data));
        break;
    case 12:
        glCheck(glUniformMatrix3x4fv(u->m_location, size, GL_FALSE, data));
        break;
    case 16:
        glCheck(glUniformMatrix4fv(u->m_location, size, GL_FALSE, data));
        break;
    }
}

void Shader::setUniform(const Shader::Uniform* u, int value)
{
    assert(u);
    glCheck(glUniform1i(u->m_location, value));
}

void Shader::setUniform(const Shader::Uniform* u, const int* data, UInt32 size)
{
    assert(u);
    assert(data);
    glCheck(glUniform1iv(u->m_location, size, data));
}

void Shader::setUniform(const Shader::Uniform* u, const glm::vec2& value)
{
    assert(u);
    glCheck(glUniform2f(u->m_location, value.x, value.y));
}

void Shader::setUniform(const Shader::Uniform* u, const glm::vec2* data, UInt32 size)
{
    assert(u);
    assert(data);
    //TODO, there must be a better way to fetch the underlying data
    //UBOs might help / be better but aren't compatible with GLES
    const auto dataWidth = 2u;
    auto rawSize = size * dataWidth;
    std::unique_ptr<GLfloat[]> rawData(new GLfloat[rawSize]);
    UInt32 j = 0u;
    for (auto i = 0u; i < size; ++i)
    {
        rawData[j] = data[i].x;
        rawData[++j] = data[i].y;
        ++j;
    }

    glCheck(glUniform2fv(u->m_location, size, &rawData[0]));
}

void Shader::setUniform(const Shader::Uniform* u, const glm::vec3& value)
{
    assert(u);
    glCheck(glUniform3f(u->m_location, value.x, value.y, value.z));
}

void Shader::setUniform(const Shader::Uniform* u, const glm::vec3* data, UInt32 size)
{
    assert(u);
    assert(data);

    const auto dataWidth = 3u;
    auto rawSize = size * dataWidth;
    std::unique_ptr<GLfloat[]> rawData(new GLfloat[rawSize]);
    UInt32 j = 0u;
    for (auto i = 0u; i < size; ++i)
    {
        rawData[j] = data[i].x;
        rawData[++j] = data[i].y;
        rawData[++j] = data[i].z;
        ++j;
    }
    glCheck(glUniform3fv(u->m_location, size, &rawData[0]));
}

void Shader::setUniform(const Shader::Uniform* u, const glm::vec4& value)
{
    assert(u);
    glCheck(glUniform4f(u->m_location, value.x, value.y, value.z, value.w));
}

void Shader::setUniform(const Shader::Uniform* u, const glm::vec4* data, UInt32 size)
{
    assert(u);
    assert(data);

    const auto dataWidth = 4u;
    auto rawSize = size * dataWidth;
    std::unique_ptr<GLfloat[]> rawData(new GLfloat[rawSize]);
    UInt32 j = 0u;
    for (auto i = 0u; i < size; ++i)
    {
        rawData[j] = data[i].x;
        rawData[++j] = data[i].y;
        rawData[++j] = data[i].z;
        rawData[++j] = data[i].w;
        ++j;
    }
    glCheck(glUniform4fv(u->m_location, size, &rawData[0]));
}

void Shader::setUniform(const Shader::Uniform* u, const Colour& c)
{
    assert(u);
    glCheck(glUniform4f(u->m_location, c.r, c.g, c.b, c.a));
}

void Shader::setUniform(const Shader::Uniform* u, const Colour* data, UInt32 size)
{
    assert(u);
    assert(data);

    const auto dataWidth = 4u;
    auto rawSize = size * dataWidth;
    std::unique_ptr<GLfloat[]> rawData(new GLfloat[rawSize]);
    UInt32 j = 0u;
    for (auto i = 0u; i < size; ++i)
    {
        rawData[j] = data[i].r;
        rawData[++j] = data[i].g;
        rawData[++j] = data[i].b;
        rawData[++j] = data[i].a;
        ++j;
    }
    glCheck(glUniform4fv(u->m_location, size, &rawData[0]));
}

void Shader::setUniform(const Shader::Uniform* u, const glm::mat4& mat)
{
    assert(u);
    glCheck(glUniformMatrix4fv(u->m_location, 1, GL_FALSE, glm::value_ptr(mat)));
}

void Shader::setUniform(const Shader::Uniform* u, const glm::mat4* data, UInt32 size)
{
    assert(u);
    assert(data);

    const auto dataWidth = 16u;
    auto rawSize = dataWidth * size;
    //std::unique_ptr<GLfloat[]> rawData(new GLfloat[rawSize]);
    std::vector<GLfloat> rawData(rawSize);
    UInt32 j = 0u;
    for (auto i = 0u; i < size; ++i)
    {
        auto p = glm::value_ptr(data[i]);
        for (auto k = 0u; k < dataWidth; j++, k++)
        {
            rawData[j] = p[k];
        }
    }
    glCheck(glUniformMatrix4fv(u->m_location, size, GL_FALSE, &rawData[0]));
}

void Shader::setUniform(const Shader::Uniform* u, const Texture::Sampler::Ptr& sampler)
{
    assert(u);
    assert(u->m_type == GL_SAMPLER_2D || u->m_type == GL_SAMPLER_CUBE || u->m_type == GL_SAMPLER_2D_ARRAY);
    
    glCheck(glActiveTexture(GL_TEXTURE0 + u->m_index));
    sampler->bind();

    glCheck(glUniform1i(u->m_location, u->m_index));
}

void Shader::setUniform(const Shader::Uniform* u, const std::vector<Texture::Sampler::Ptr>& samplers)
{
    assert(u);
    assert(u->m_type == GL_SAMPLER_2D || u->m_type == GL_SAMPLER_CUBE);
    assert(samplers.size() > 0);

    GLint units[32];
    auto size = samplers.size();
    for (auto i = 0u; i < size; ++i)
    {
        assert((samplers[i]->getTexture()->getType() == Texture::Type::Texture2D && u->m_type == GL_SAMPLER_2D)
            ||(samplers[i]->getTexture()->getType() == Texture::Type::TextureCube && u->m_type == GL_SAMPLER_CUBE));

        glCheck(glActiveTexture(GL_TEXTURE0 + u->m_index + i));
        samplers[i]->bind();
        units[i] = u->m_index + i;
    }
    glCheck(glUniform1iv(u->m_location, size, units));
}

const std::string& Shader::getName() const
{
    return m_name;
}

void Shader::bind()
{
    if (currentShader != this)
    {
        glCheck(glUseProgram(m_programID));
        currentShader = this;
    }
}

const Shader* Shader::getBoundShader()
{
    return currentShader;
}

//----uniform-----//
Shader::Uniform::Uniform(const Shader::Key& key)
    : m_location	(-1),
    m_type			(0),
    m_index			(0),
    m_shader		(nullptr)
{}

Shader* Shader::Uniform::getShader()
{
    return m_shader;
}

const std::string& Shader::Uniform::getName() const
{
    return m_name;
}

GLenum Shader::Uniform::getType() const
{
    return m_type;
}

//private
Shader* Shader::linkProgram(const std::string& name, GLuint vertShaderID, GLuint fragShaderID, GLint geomShaderID)
{
    GLint successStatus;
    GLint programID;
    glCheck(programID = glCreateProgram());
    glCheck(glAttachShader(programID, vertShaderID));
    glCheck(glAttachShader(programID, fragShaderID));
    if (geomShaderID > -1) glCheck(glAttachShader(programID, geomShaderID));
    glCheck(glLinkProgram(programID));
    glCheck(glGetProgramiv(programID, GL_LINK_STATUS, &successStatus));

    glCheck(glDeleteShader(vertShaderID));
    glCheck(glDeleteShader(fragShaderID));
    if (geomShaderID > -1) glCheck(glDeleteShader(geomShaderID));

    if (successStatus != GL_TRUE)
    {
        std::string errorMessage;

        GLint length;
        glCheck(glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &length));
        if (length == 0) length = 4096;
        if (length > 0)
        {
            std::unique_ptr<char[]> info(new char[length]);
            glCheck(glGetProgramInfoLog(programID, length, NULL, &info[0]));
            info[length - 1] = '\0';
            errorMessage = std::string(&info[0]);
        }

        Logger::Log("Failed to link program with error: " + errorMessage, Logger::Type::Error);
        glCheck(glDeleteProgram(programID));
        return nullptr;
    }

    //stuff our newly created program into a shader object
    Ptr shader = std::make_unique<Shader>(Shader::Key());
    shader->m_programID = programID;
    shader->m_name = name;

    //get shader attribute locations, making sure we don't try picking any vendor reserved ones
    GLint activeAttribs;
    glCheck(glGetProgramiv(programID, GL_ACTIVE_ATTRIBUTES, &activeAttribs));
    if (activeAttribs > 0)
    {
        GLint length;
        glCheck(glGetProgramiv(programID, GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, &length));
        if (length > 0)
        {
            std::unique_ptr<GLchar[]> attribName(new GLchar[length + 1]);
            GLint attribSize;
            GLenum attribType;
            GLint attribLocation;

            for (auto i = 0; i < activeAttribs; ++i)
            {
                glCheck(glGetActiveAttrib(programID, i, length, NULL, &attribSize, &attribType, &attribName[0]));
                attribName[length] = '\0';

                glCheck(attribLocation = glGetAttribLocation(programID, &attribName[0]));

                shader->m_vertexAttributes.insert(std::make_pair(&attribName[0], attribLocation));
            }
        }
    }

    //get all the uniforms in the program
    GLint progUniforms;
    glCheck(glGetProgramiv(programID, GL_ACTIVE_UNIFORMS, &progUniforms));
    if (progUniforms > 0)
    {
        GLint length;
        glCheck(glGetProgramiv(programID, GL_ACTIVE_UNIFORM_MAX_LENGTH, &length));
        if (length > 0)
        {
            std::unique_ptr<GLchar[]> uniformName(new GLchar[length + 1]);
            GLint uniformSize;
            GLenum uniformType;
            GLint uniformLocation;
            UInt32 samplerIndex = 0u;

            for (auto i = 0; i < progUniforms; ++i)
            {
                glCheck(glGetActiveUniform(programID, i, length, NULL, &uniformSize, &uniformType, &uniformName[0]));
                uniformName[length] = '\0';
                if (length > 3)
                {
                    //strip array indices of uniform names to make consistent across hardware
                    //easier to use C string functions here, save converting it to string object and back
                    //(plus simpler to just terminate the string prematurely than create a substr)
                    auto c = strrchr(&uniformName[0], '[');
                    if (c) *c = '\0';
                }

                glCheck(uniformLocation = glGetUniformLocation(programID, &uniformName[0]));
                Uniform::Ptr uniform = std::make_unique<Uniform>(Key());
                uniform->m_shader = shader.get();
                uniform->m_name = &uniformName[0];
                uniform->m_location = uniformLocation;
                uniform->m_type = uniformType;
                //TODO we need to better enumerate sampler types (we'll add shadow samplers later on...)
                if (uniformType == GL_SAMPLER_2D || uniformType == GL_SAMPLER_CUBE || uniformType == GL_SAMPLER_2D_ARRAY)
                {
                    uniform->m_index = samplerIndex;
                    samplerIndex += uniformSize;
                }
                else
                {
                    uniform->m_index = 0u;
                }

                auto hash = shader->m_hashfunc(std::string(uniform->m_name));
                shader->m_uniforms.insert(std::make_pair(hash, std::move(uniform)));
            }
        }
    }

    //cache shader
    return shaderCache.insert(name, shader);
}

bool Shader::compileVertexShader(GLuint vertShaderID, const GLchar** sourceArray)
{
    glCheck(glShaderSource(vertShaderID, sourceArraySize, sourceArray, NULL));
    glCheck(glCompileShader(vertShaderID));
    GLint successStatus;
    glCheck(glGetShaderiv(vertShaderID, GL_COMPILE_STATUS, &successStatus));

    if (successStatus != GL_TRUE)
    {
        std::string errorMessage;

        GLint length;
        glCheck(glGetShaderiv(vertShaderID, GL_INFO_LOG_LENGTH, &length));
        if (length == 0) length = 4096;
        if (length > 0)
        {
            std::unique_ptr<char[]> info(new char[length]);
            glCheck(glGetShaderInfoLog(vertShaderID, length, NULL, &info[0]));
            info[length - 1] = '\0';
            errorMessage = std::string(&info[0]);
        }

        Logger::Log(errorMessage, Logger::Type::Error, Logger::Output::All);
        return false;
    }
    return true;
}

bool Shader::compileFragmentShader(GLuint fragShaderID, const GLchar** sourceArray)
{
    glCheck(glShaderSource(fragShaderID, sourceArraySize, sourceArray, NULL));
    glCheck(glCompileShader(fragShaderID));
    GLint successStatus;
    glCheck(glGetShaderiv(fragShaderID, GL_COMPILE_STATUS, &successStatus));
    if (successStatus != GL_TRUE)
    {
        std::string errorMessage;

        GLint length;
        glCheck(glGetShaderiv(fragShaderID, GL_INFO_LOG_LENGTH, &length));
        if (length == 0)length = 4096;
        if (length > 0)
        {
            std::unique_ptr<char[]> info(new char[length]);
            glCheck(glGetShaderInfoLog(fragShaderID, length, NULL, &info[0]));
            info[length - 1] = '\0';
            errorMessage = std::string(&info[0]);
        }

        Logger::Log(errorMessage, Logger::Type::Error, Logger::Output::All);
        return false;
    }
    return true;
}

bool Shader::compileGeometryShader(GLuint geometryShaderId, const GLchar** sourceArray)
{
    glCheck(glShaderSource(geometryShaderId, sourceArraySize, sourceArray, NULL));
    glCheck(glCompileShader(geometryShaderId));
    GLint successStatus;
    glCheck(glGetShaderiv(geometryShaderId, GL_COMPILE_STATUS, &successStatus));
    if (successStatus != GL_TRUE)
    {
        std::string errorMessage;

        GLint length;
        glCheck(glGetShaderiv(geometryShaderId, GL_INFO_LOG_LENGTH, &length));
        if (length == 0)length = 4096;
        if (length > 0)
        {
            std::unique_ptr<char[]> info(new char[length]);
            glCheck(glGetShaderInfoLog(geometryShaderId, length, NULL, &info[0]));
            info[length - 1] = '\0';
            errorMessage = std::string(&info[0]);
        }

        Logger::Log(errorMessage, Logger::Type::Error, Logger::Output::All);
        return false;
    }
    return true;
}
