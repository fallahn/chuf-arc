/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/Sprite.hpp>
#include <chuf2/SpriteBatch.hpp>
#include <chuf2/Rectangle.hpp>

using namespace chuf;

Sprite::Sprite(const std::string& name, const VertexLayout& vertLayout, Key& key)
    : m_parentBatch	(nullptr),
    m_arrayOffset	(0u),
    m_index			(0u),
    m_vertLayout	(vertLayout),
    m_vertPositions	({}),
    m_colour		(Colour::white),
    m_name			(name),
    m_rotation		(0.f),
    m_ownTransform	(std::make_unique<Transform>()),
    m_transform		(m_ownTransform.get()),
    m_dirtyBits		(0u)
{
    for (auto& p : m_vertPositions)
        p.w = 1.f;

    m_transform->addListener(*this);
}

Sprite::~Sprite()
{
    m_transform->removeListener(*this);
}

//public
void Sprite::setSize(const glm::vec2& size)
{
    m_size = size;
    setDirty(TRANSFORM | VERTS);
}

void Sprite::setOrigin(const glm::vec2& origin)
{
    m_origin = origin;
    setDirty(TRANSFORM | VERTS);
}

void Sprite::move(const glm::vec2& movement)
{
    m_transform->translate(movement.x, movement.y, 0.f);
}

void Sprite::setPosition(const glm::vec2& position)
{
    m_transform->setTranslation(position.x, position.y, 0.f);
}

void Sprite::rotate(float angle)
{
    m_rotation += angle;	
    setRotation(m_rotation);
}

void Sprite::setRotation(float angle)
{
    if (angle > 360.f) angle -= 360.f;
    else if (angle < -360.f) angle += 360.f;
    
    m_transform->setRotation(Transform::Axis::Z, angle);
    m_rotation = angle;
}

void Sprite::scale(float amount)
{
    m_transform->scale(amount);
}

void Sprite::scale(const glm::vec2& amount)
{
    m_transform->scale(amount);
}

void Sprite::setScale(float scale)
{
    m_transform->setScale(scale);
}

void Sprite::setScale(const glm::vec2& scale)
{
    m_transform->setScale(scale);
}

const glm::vec2& Sprite::getSize() const
{
    return m_size;
}

glm::vec2 Sprite::getPosition() const
{
    auto p = m_transform->getTranslation();
    return {p.x, p.y};
}

float Sprite::getRotation() const
{
    return m_rotation;
}

glm::vec2 Sprite::getScale() const
{
    auto s = m_transform->getScale();
    return {s.x, s.y};
}

void Sprite::setSubRect(const FloatRect& rect)
{
    if (m_subRect != rect)
    {
        m_subRect = rect;
        //if (m_size == m_textureSize)
            m_size = { rect.width, rect.height };
        setDirty(UV | VERTS);
    }
}

const FloatRect& Sprite::getSubRect() const
{
    return m_subRect;
}

void Sprite::setColour(const Colour& colour)
{
    m_colour = colour;
    setDirty(COLOUR);
}

const Colour& Sprite::getColour() const
{
    return m_colour;
}

const std::string& Sprite::getName() const
{
    return m_name;
}

const glm::vec2& Sprite::getTextureSize() const
{
    return m_textureSize;
}

void Sprite::setTransform(Transform* t)
{
    m_transform->removeListener(*this);
    
    if (t)
    {
        m_transform = t;
        setDirty(DirtyBits::TRANSFORM);
    }
    else
    {
        m_transform = m_ownTransform.get();
    }

    m_transform->addListener(*this);
}

SpriteBatch& Sprite::getSpriteBatch() const
{
    return *m_parentBatch;
}

const FloatRect& Sprite::getLocalBounds()
{
    update();
    return m_localBounds;
}

const FloatRect& Sprite::getGlobalBounds()
{
    update();
    return m_globalBounds;
}

//private
void Sprite::transformChanged(Transform& t)
{
    setDirty(TRANSFORM);
}

void Sprite::update()
{
    //set initial positions from size / origin
    if (m_dirtyBits & VERTS)
    {
        //LT, LB, RT, RB
        m_vertPositions[0].x = -m_origin.x;
        m_vertPositions[0].y = m_size.y - m_origin.y;

        m_vertPositions[1].x = -m_origin.x;
        m_vertPositions[1].y = -m_origin.y;

        m_vertPositions[2].x = m_size.x - m_origin.x;
        m_vertPositions[2].y = m_size.y - m_origin.y;

        m_vertPositions[3].x = m_size.x - m_origin.x;
        m_vertPositions[3].y = -m_origin.y;

        m_localBounds.width = m_vertPositions[2].x - m_vertPositions[0].x;
        m_localBounds.height = m_vertPositions[1].y - m_vertPositions[0].y;
    }

    auto stride = (m_vertLayout.getVertexSize() / sizeof(float));
    //update positions by multiplying base positions by transform matrix
    if (m_dirtyBits & TRANSFORM)
    {
        m_globalBounds = { FLT_MAX, FLT_MAX, -FLT_MAX, -FLT_MAX };
        
        auto mat = m_transform->getMatrix();
        auto positionOffset = m_arrayOffset + m_vertLayout.getElementOffset(VertexLayout::Type::POSITION);
        assert(positionOffset >= 0);
        
        auto position = 0u;
        auto nv = glm::vec4();
        auto firstPos = glm::vec2();
        for (auto i = 0u; i < m_vertPositions.size(); ++i)
        {
            nv = mat * m_vertPositions[i];
            position = positionOffset + (i * stride);
            
            m_parentBatch->m_vertexArray[position++] = nv.x;
            m_parentBatch->m_vertexArray[position] = nv.y;

            if (i == 0) firstPos = { nv.x, nv.y };

            //update global bounds
            (nv.x < m_globalBounds.left) ? m_globalBounds.left = nv.x : (nv.x > m_globalBounds.width) ? m_globalBounds.width = nv.x : nv.x;
            (nv.y < m_globalBounds.top) ? m_globalBounds.top = nv.y : (nv.y > m_globalBounds.height) ? m_globalBounds.height = nv.y : nv.y;
        }
        m_globalBounds.width -= m_globalBounds.left;
        m_globalBounds.height -= m_globalBounds.top;

        //repeat last vert position for degenerate triangle
        auto lastVert = position + stride - 1;
        m_parentBatch->m_vertexArray[lastVert++] = nv.x;
        m_parentBatch->m_vertexArray[lastVert++] = nv.y;
        
        if (m_index == 0) //first sprite in array
        {
            m_parentBatch->m_vertexArray[lastVert++] = nv.x;
            m_parentBatch->m_vertexArray[lastVert] = nv.y;
        }
        else //repeat first if not at beginning of array
        {
            auto firstVert = positionOffset - stride;
            m_parentBatch->m_vertexArray[firstVert++] = firstPos.x;
            m_parentBatch->m_vertexArray[firstVert] = firstPos.y;
        }
    }

    //update UVs
    if (m_dirtyBits & UV)
    {		
        auto left = m_subRect.left / m_textureSize.x;
        auto top = m_subRect.top / m_textureSize.y;
        auto bottom = (m_subRect.top +/*-*/ m_subRect.height) / m_textureSize.y;
        auto right = (m_subRect.left + m_subRect.width) / m_textureSize.x;
        
        std::array<glm::vec2, 4u> uvs =
        {
            glm::vec2(left, top),
            glm::vec2(left, bottom),
            glm::vec2(right, top),
            glm::vec2(right, bottom)
        };

        auto positionOffset = m_arrayOffset + m_vertLayout.getElementOffset(VertexLayout::Type::UV0);
        assert(positionOffset >= 0);

        for (auto i = 0u; i < uvs.size(); ++i)
        {
            auto position = (i * stride) + positionOffset;
            m_parentBatch->m_vertexArray[position++] = uvs[i].x;
            m_parentBatch->m_vertexArray[position] = uvs[i].y;
        }
    }

    //update colour
    if (m_dirtyBits & COLOUR)
    {
        auto positionOffset = m_arrayOffset + m_vertLayout.getElementOffset(VertexLayout::Type::COLOUR);
        assert(positionOffset >= 0);

        for (auto i = 0u; i < 4u; ++i)
        {
            auto position = (i * stride) + positionOffset;
            m_parentBatch->m_vertexArray[position++] = m_colour.r;
            m_parentBatch->m_vertexArray[position++] = m_colour.g;
            m_parentBatch->m_vertexArray[position++] = m_colour.b;
            m_parentBatch->m_vertexArray[position] = m_colour.a;
        }
    }

    m_dirtyBits = 0u;
}

void Sprite::setDirty(UInt8 dirtyFlag)
{
    assert(m_parentBatch);
    if (!m_dirtyBits) //only want to mark batch dirty once
    {
        m_parentBatch->m_dirtySprites.push_back(this);
    }
    m_dirtyBits |= dirtyFlag;
}