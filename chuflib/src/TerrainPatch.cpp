/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/TerrainPatch.hpp>
#include <chuf2/Terrain.hpp>
#include <chuf2/Mesh.hpp>
#include <chuf2/SubMesh.hpp>
#include <chuf2/Node.hpp>
#include <chuf2/Util.hpp>
#include <chuf2/Scene.hpp>
#include <chuf2/Material.hpp>
#include <chuf2/App.hpp>
#include <chuf2/Reports.hpp>

#include <cfloat>
#include <array>

using namespace chuf;

namespace
{
    Int32 currentPatchIndex = -1;
}

TerrainPatch::TerrainPatch(const Key&)
    : QuadTreeComponent ({}),
    m_terrain           (nullptr),
    m_index             (0),
    m_row               (0),
    m_column            (0),
    m_camera            (nullptr),
    m_currentLevel      (0),
    m_flags             (DirtyBits::ALL),
    m_passedCulling     (false){}


//public
void TerrainPatch::cameraChanged()
{
    m_flags |= DirtyBits::LEVEL;
}

const BoundingBox& TerrainPatch::getBoundingBox(bool worldSpace)
{
    if (!worldSpace) return m_boundingbox;
    if (!(m_flags & DirtyBits::BOUNDS)) return m_boundingboxWorld;

    updateBounds();

    return m_boundingboxWorld;
}

const BoundingSphere& TerrainPatch::getBoundingSphere(bool worldSpace)
{
    if (!worldSpace) return m_boundingSphere;
    if (!(m_flags & DirtyBits::BOUNDS)) return m_boundingSphereWorld;
    
    updateBounds();

    return m_boundingSphereWorld;
}

Material::Ptr TerrainPatch::getMaterial() const
{
    return m_levels[0]->model->getMaterial();
}


//private
TerrainPatch::Ptr TerrainPatch::create(Terrain& terrain, UInt32 index,
                                        UInt32 row, UInt32 column, //epic parameters! (tribute to WinAPI :P)
                                        const std::vector<float>& heights, UInt32 width, UInt32 height,
                                        UInt32 x1, UInt32 z1, UInt32 x2, UInt32 z2,
                                        float xOffset, float zOffset, UInt32 maxStep, float skirtSize)

{
    auto patch = std::make_unique<TerrainPatch>(Key());
    patch->m_terrain = &terrain;
    patch->m_index = index;
    patch->m_row = row;
    patch->m_column = column;

    for (auto i = 1u; i <= maxStep; i *= 2)
    {
        patch->addLOD(heights, width, height, x1, z1, x2, z2, xOffset, zOffset, i, skirtSize);
    }

    patch->m_boundingbox.set(patch->m_levels[0]->model->getMesh()->getBoundingBox());
    patch->updateLocalQuadTreeBounds({ 0.f, 0.f, patch->m_boundingbox.getMaxPoint().x - patch->m_boundingbox.getMinPoint().x,
                                        patch->m_boundingbox.getMaxPoint().z - patch->m_boundingbox.getMinPoint().z });
    patch->updateBounds();
    return std::move(patch);
}

void TerrainPatch::addLOD(const std::vector<float>& heights, UInt32 width, UInt32 height,
                        UInt32 x1, UInt32 z1, UInt32 x2, UInt32 z2,
                        float xOffset, float zOffset, UInt32 step, float skirtSize)
{
    UInt32 patchWidth = 0u;
    UInt32 patchHeight = 0u;

    if (step == 1)
    {
        patchWidth = (x2 - x1) + 1u;
        patchHeight = (z2 - z1) + 1u;
    }
    else
    {
        auto diff = x2 - x1;
        patchWidth = diff / step + ((diff % step == 0) ? 0u : 1u) + 1u;
        diff = z2 - z1;
        patchHeight = diff / step + ((diff % step == 0) ? 0u : 1u) + 1u;
    }

    if (patchWidth < 2u || patchHeight < 2u) return; //too small

    if (skirtSize > 0)
    {
        patchWidth += 2u;
        patchHeight += 2u;
    }

    UInt32 vertCount = patchWidth * patchHeight;
    UInt32 vertElementCount = 14; //postion, normal, tan, bitan, uv
    std::vector<float> verts(vertCount * vertElementCount);
    UInt32 index = 0u;

    glm::vec3 min(FLT_MAX);
    glm::vec3 max(-FLT_MAX);

    float scaledXStep = static_cast<float>(step) * m_terrain->m_scale.x;
    float scaledZStep = static_cast<float>(step) * m_terrain->m_scale.z;

    bool zSkirt = (skirtSize > 0) ? true : false;
    for (auto z = z1;;)
    {
        bool xSkirt = (skirtSize > 0) ? true : false;
        for (auto x = x1;;)
        {
            assert(index < vertCount);
            float* v = verts.data() + (index * vertElementCount);
            index++;

            //calc pos
            v[0] = (x + xOffset) * m_terrain->m_scale.x;
            v[1] = computeHeight(heights, width, x, z);
            if (xSkirt || zSkirt)
            {
                v[1] -= skirtSize * m_terrain->m_scale.y;
            }
            v[2] = (z + zOffset) * m_terrain->m_scale.z;

            //update bounds
            if (!(xSkirt || zSkirt))
            {
                (v[0] < min.x) ? min.x = v[0] : (v[0] > max.x) ? max.x = v[0] : max.x;
                (v[1] < min.y) ? min.y = v[1] : (v[1] > max.y) ? max.y = v[1] : max.y;
                (v[2] < min.z) ? min.z = v[2] : (v[2] > max.z) ? max.z = v[2] : max.z;
            }

            //calc normal
            glm::vec3 position(v[0], computeHeight(heights, width, x, z), v[2]); //calc height because we might be offset by skirt

            glm::vec3 n = position - glm::vec3(v[0],
                computeHeight(heights, width, x, (z < height - step) ? z + step : z),
                (z < height - step) ? v[2] + scaledZStep : v[2]);
            glm::vec3 e = position - glm::vec3((x < width - step) ? v[0] + scaledXStep : v[0],
                computeHeight(heights, width, (x < width - step) ? x + step : x, z),
                v[2]);
            glm::vec3 s = position - glm::vec3(v[0],
                computeHeight(heights, width, x, (z >= step) ? z - step : z),
                (z >= step) ? v[2] - scaledZStep : v[2]);
            glm::vec3 w = position - glm::vec3((x >= step) ? v[0] - scaledXStep : v[0],
                computeHeight(heights, width, (x >= step) ? x - step : x, z),
                v[2]);

            std::array<glm::vec3, 4> normals =
            {
                glm::cross(n, w),
                glm::cross(w, s),
                glm::cross(e, n),
                glm::cross(s, e)
            };

            glm::vec3 normal(normals[0] + normals[1] + normals[2] + normals[3]);
            normal = -glm::normalize(normal);

            v[3] = normal.x;
            v[4] = normal.y;
            v[5] = normal.z;

            //tan - we can just rotate these as we know UVs are square with X/Z
            glm::quat rotation = glm::rotation(glm::vec3(0.f, 1.f, 0.f), normal);
            glm::vec3 tan = glm::rotate(rotation, glm::vec3(1.f, 0.f, 0.f));
            v[6] = tan.x;
            v[7] = tan.y;
            v[8] = tan.z;

            //bitan
            glm::vec3 bitan = glm::rotate(rotation, glm::vec3(0.f, 0.f, 1.f));
            v[9] = bitan.x;
            v[10] = bitan.y;
            v[11] = bitan.z;

            //calc UV
            v[12] = static_cast<float>(x) / width;
            v[13] = 1.f - static_cast<float>(z) / height;
            if (xSkirt)
            {
                const float offset = skirtSize / width;
                (x == x1) ? v[12] -= offset : v[12] += offset;
            }
            else if (zSkirt)
            {
                const float offset = skirtSize / height;
                (z == z1) ? v[13] -= offset : v[13] += offset;
            }

            if (x == x2)
            {
                if (skirtSize == 0 || xSkirt) break;
                xSkirt = true;
            }
            else if (xSkirt)
            {
                xSkirt = false;
            }
            else
            {
                x = std::min(x + step, x2);
            }
        }

        if (z == z2)
        {
            if (skirtSize == 0 || zSkirt) break;
            zSkirt = true;
        }
        else if (zSkirt)
        {
            zSkirt = false;
        }
        else
        {
            z = std::min(z + step, z2);
        }
    }
    assert(index == vertCount);

    //create the mesh
    glm::vec3 centre = min + ((max - min) * 0.5f);

    std::vector<VertexLayout::Element> elements =
    {      
        { VertexLayout::Type::POSITION, 3 },
        { VertexLayout::Type::NORMAL, 3 },
        { VertexLayout::Type::TANGENT, 3 },
        { VertexLayout::Type::BITANGENT, 3 },
        { VertexLayout::Type::UV0, 2 }
    };
    auto mesh = Mesh::createMesh(VertexLayout(elements), vertCount, false);

    mesh->setVertexData(verts.data());
    mesh->setBoundingBox({ min, max });
    mesh->setBoundingSphere({ centre, glm::length(max - centre) });

    //create index array
    UInt32 indexCount =
        (patchWidth * 2u) * //indices per row
        (patchHeight - 1u) + //rows of triangles
        (patchHeight - 2u) * 2u; //and degenerate tris


    assert(indexCount < USHRT_MAX); //YOU NEED TO USE A SMALLER PATCH SIZE

    std::vector<UInt16> indices(indexCount);
    index = 0u;
    //lay out in strips, stitch rows with degenerate triangles
    //zig zagging even and odd rows
    for (auto z = 0u; z < patchHeight - 1; ++z)
    {
        auto i1 = z * patchWidth;
        auto i2 = (z + 1) * patchWidth;

        if (z % 2 == 0) //even row
        {
            if (z > 0)
            {
                //wrap
                indices[index] = indices[index - 1];
                index++; 
                indices[index++] = i1;
            }

            //add strip
            for (auto x = 0u; x < patchWidth; ++x)
            {
                indices[index++] = i1 + x;
                indices[index++] = i2 + x;
            }
        }
        else //odd row
        {
            if (z > 0)
            {
                indices[index] = indices[index - 1];
                index++;
                indices[index++] = i2 + (static_cast<Int32>(patchWidth) - 1);
            }

            for (auto x = static_cast<Int32>(patchWidth)-1; x >= 0; --x)
            {
                indices[index++] = i2 + x;
                indices[index++] = i1 + x;
            }
        }
    }
    assert(index = indexCount);

    auto& subMesh = mesh->addSubMesh(Mesh::PrimitiveType::TriangleStrip, Mesh::IndexFormat::I16, indexCount);
    subMesh.setIndexData(indices.data(), 0, indexCount);

    m_levels.emplace_back(std::make_unique<Level>());
    m_levels.back()->model = Model::create(mesh, nullptr);
}

UInt32 TerrainPatch::draw(UInt32 passflags, bool testFrustum, bool wireframe)
{       
    if (testFrustum)
    {
        Scene* scene = (m_terrain->getNode()) ? m_terrain->getNode()->getScene() : nullptr;
        Camera* camera = (scene) ? scene->getActiveCamera() : nullptr;
        
        if (!camera) return 0;        
        
        auto worldBounds = getBoundingBox(true);
        auto frustum = (passflags & RenderPass::Reflection) ? camera->getReflectionViewFrustum() : camera->getViewFrustum();
        if (!worldBounds.intersects(frustum))
        {
            m_passedCulling = false;
            return 0;
        }
        m_passedCulling = true;
        m_currentLevel = computeLOD(camera, worldBounds);
    }
    else if (!m_passedCulling)
    {
        return 0;
    }
    
    return m_levels[m_currentLevel]->model->draw(passflags, wireframe);
}

void TerrainPatch::updateMaterial()
{
    currentPatchIndex = m_index;

    auto& material = m_terrain->m_material;
    for (auto& l : m_levels)
    {
        l->model->setMaterial(material);
    }
    currentPatchIndex = -1;
}

UInt32 TerrainPatch::computeLOD(Camera* camera, const BoundingBox& worldBounds)
{
    if (m_camera != camera)
    {
        m_camera = camera;
        m_flags |= DirtyBits::LEVEL;
    }

    if (m_levels.empty()) return 0;

    if (!(m_flags & DirtyBits::LEVEL)) return m_currentLevel;

    auto viewport = App::getInstance().getView().viewport;
    glm::vec2 min(FLT_MAX);
    glm::vec2 max(-FLT_MAX);

    auto corners = worldBounds.getCorners();
    for (const auto& c : corners)
    {
        auto pos = camera->project(viewport, { c.x, c.y, c.z });
        (pos.x < min.x) ? min.x = pos.x : (pos.x > max.x) ? max.x = pos.x : max.x;
        (pos.y < min.y) ? min.y = pos.z : (pos.y > max.y) ? max.y = pos.y : max.y;
    }
    float area = (max.x - min.x) * (max.y - min.y);
    float screenArea = viewport.width * viewport.height / 10.f;
    float error = screenArea / area;

    auto maxLevel = m_levels.size() - 1;
    std::size_t lod = static_cast<std::size_t>(error);
    lod = std::min(std::max(lod, static_cast<std::size_t>(0)), maxLevel);


    //alt version. performs slightly worse at near distances, but better at far
    //could be tuned so that lower LODs are brought in sooner (on a curve ratherthan linear)
    /*auto maxDist = m_camera->getFarPlane() * m_camera->getFarPlane();
    auto centre = worldBounds.getCentre();
    auto distance = Util::Vector::lengthSquared(centre - m_camera->getNode()->getTranslationWorld());
    auto distPerLod = maxDist / static_cast<float>(m_levels.size());
    auto lod = static_cast<UInt32>(std::floor(distance / distPerLod));

    lod = std::min(std::max(0u, lod), m_levels.size() - 1);*/

    m_currentLevel = lod;

    return lod;
}

Colour TerrainPatch::getAmbientColour() const
{
    Scene* scene = (m_terrain->getNode()) ? m_terrain->getNode()->getScene() : nullptr;
    return (scene) ? scene->getAmbientColour() : Colour::black;
}

float TerrainPatch::computeHeight(const std::vector<float>& heights, UInt32 width, UInt32 x, UInt32 z)
{
    return heights[(z * width) + x] * m_terrain->m_scale.y;
}

void TerrainPatch::updateNodeBindings()
{
    currentPatchIndex = m_index;
    for (auto& l : m_levels)
    {
        l->model->getMaterial()->bindNode(const_cast<Node*>(m_terrain->getNode()));
        l->model->setNode(const_cast<Node*>(m_terrain->getNode()));
    }
    currentPatchIndex = -1;
    m_flags |= DirtyBits::BOUNDS;
}

void TerrainPatch::updateBounds()
{
    m_flags &= ~DirtyBits::BOUNDS;

    m_boundingboxWorld = m_boundingbox;
    auto quadBox = getLocalQuadTreeBounds();
    quadBox.left += m_boundingbox.getMinPoint().x;
    quadBox.top += m_boundingbox.getMinPoint().z;

    if (m_terrain->getNode())
    {
        m_boundingboxWorld.transform(const_cast<Node*>(m_terrain->getNode())->getWorldMatrix());
        m_boundingSphereWorld.set(m_boundingboxWorld);

        quadBox.width *= m_terrain->m_scale.x;
        quadBox.height *= m_terrain->m_scale.z;
    }
    updateGlobalQuadTreeBounds(quadBox);
}