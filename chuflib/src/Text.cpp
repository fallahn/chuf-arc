/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/Text.hpp>
#include <chuf2/Font.hpp>
#include <chuf2/Sprite.hpp>
#include <chuf2/SpriteBatch.hpp>
#include <chuf2/Shader.hpp>

using namespace chuf;

Text::Text(Font* font, const std::string& text)
    : m_font            (font),
    m_text              (text),
    m_rotation          (0.f),
    m_hSpacing          (4.f),
    m_vSpacing          (3.f),
    m_verticalOffset    (0.f),
    m_dirtyBits         (0u)
{
    assert(font);

    //create spritebatch
    auto shader = Shader::create("res/shaders/Unlit.vert", "res/shaders/Unlit.frag", "#define VERTEX_COLOUR\n#define TEXT\n");
    assert(shader);
    m_spriteBatch = SpriteBatch::create(font->getAtlas(), shader);
    assert(m_spriteBatch);

    if (!text.empty())
    {
        m_dirtyBits |= DirtyBits::ALL;
        Util::String::removeChar(m_text, '\r');
    }
}

//public
void Text::setString(const std::string& text)
{
    if (m_text != text)
    {
        m_text = text;
        Util::String::removeChar(m_text, '\r');
        m_dirtyBits |= DirtyBits::STRING | DirtyBits::SPACING;
    }
}

const std::string& Text::getString() const
{
    return m_text;
}

void Text::setHorizontalSpacing(float spacing)
{
    if (m_hSpacing != spacing)
    {
        m_hSpacing = spacing;
        m_dirtyBits |= DirtyBits::SPACING;
    }
}

float Text::getHorizontalSpacing() const
{
    return m_hSpacing;
}

void Text::setVerticalSpacing(float spacing)
{
    if (m_hSpacing != spacing)
    {
        m_vSpacing = spacing;
        m_dirtyBits |= DirtyBits::SPACING;
    }
}

float Text::getVerticalSpacing() const
{
    return m_vSpacing;
}

void Text::setPosition(const glm::vec2& position)
{
    m_transform.setTranslation(position.x, position.y, 0.f);
    m_dirtyBits |= DirtyBits::TRANSFORM;
}

glm::vec2 Text::getPosition() const
{
    auto p = m_transform.getTranslation();
    return{ p.x, p.y };
}

void Text::move(const glm::vec2& movement)
{
    m_transform.translate(movement.x, movement.y, 0.f);
    m_dirtyBits |= DirtyBits::TRANSFORM;
}

void Text::setRotation(float angle)
{
    if (angle > 360.f) angle -= 360.f;
    else if (angle < -360.f) angle += 360.f;

    m_transform.setRotation(Transform::Axis::Z, angle);
    m_rotation = angle;
    m_dirtyBits |= DirtyBits::TRANSFORM;
}

float Text::getRotation() const
{
    return m_rotation;
}

void Text::rotate(float angle)
{
    m_rotation += angle;
    setRotation(m_rotation);
}

void Text::setScale(float scale)
{
    setScale({ scale, scale });
}

void Text::setScale(const glm::vec2& scale)
{
    m_transform.setScale(scale);
    m_dirtyBits |= DirtyBits::TRANSFORM;
}

glm::vec2 Text::getScale() const
{
    auto s = m_transform.getScale();
    return{ s.x, s.y };
}

void Text::scale(float amount)
{
    scale({ amount, amount });
}

void Text::scale(const glm::vec2& amount)
{
    m_transform.scale(amount);
    m_dirtyBits |= DirtyBits::TRANSFORM;
}

void Text::setOrigin(const glm::vec2& origin)
{
    m_origin = origin;
    m_dirtyBits |= DirtyBits::ORIGIN;
}

const glm::vec2& Text::getOrigin() const
{
    return m_origin;
}

const FloatRect& Text::getLocalBounds()
{
    assert(m_spriteBatch);
    updateText();
    return m_spriteBatch->getLocalBounds();
}

const FloatRect& Text::getGlobalBounds()
{
    assert(m_spriteBatch);
    updateText();
    return m_spriteBatch->getGlobalBounds();
}

void Text::setColour(const chuf::Colour& colour)
{
    m_colour = colour;
    m_dirtyBits |= DirtyBits::COLOUR;
}

const Colour& Text::getColour() const
{
    return m_colour;
}

const Font* Text::getFont() const
{
    return m_font;
}

void Text::draw()
{
    assert(m_spriteBatch);

    //check dirty bits and update
    if (m_dirtyBits) updateText();

    m_spriteBatch->draw();
}

//private
void Text::updateText()
{
    if (m_dirtyBits & DirtyBits::STRING)
    {
        float advance = 0.f;
        float newline = 0.f;

        for (auto i = 0u; i < m_text.length(); ++i)
        {
            auto c = m_text[i]; 
            auto rect = m_font->getChar(c);
            if (c == ' ') rect.width = 12.f; //HAAAAX! This won't work on other sized fonts
            
            //update position and skip newline chars
            if (c == '\n')
            {
                newline -= rect.height; //lines go *below*
                advance = -m_hSpacing;// 0.f;
                rect.width = 0.f;
            }

            //update subrect if we already have a char
            if (i < m_sprites.size())
            {
                m_sprites[i]->setSubRect(rect);
                m_sprites[i]->setPosition({ advance, newline });
            }
            else //add a new char
            {
                auto s = m_spriteBatch->addSprite({ advance, newline });
                s->setSubRect(rect);
                s->setColour(m_colour);
                m_sprites.push_back(s);
            }
            advance += rect.width;
        }

        //make sure the origin is at the bottom of the text
        if (newline != -m_verticalOffset)
        {
            m_verticalOffset = -newline;
            m_dirtyBits |= DirtyBits::OFFSET;
        }


        //remove unused sprites
        while (m_text.length() < m_sprites.size())
        {
            //this is why we need to keep our own copy of used sprites
            m_spriteBatch->removeSprite(m_sprites.back());
            m_sprites.pop_back();
        }

        m_dirtyBits &= ~DirtyBits::STRING; //unset to say we updated string
    }

    
    if (m_dirtyBits) //still dirty, so lets do what needs iterating over
    {
        float hCount = 0.f;
        float vCount = 0.f;
        float vSpace = 0.f;

        for (auto& s : m_sprites)
        {
            if (m_dirtyBits & DirtyBits::SPACING)
            {
                auto pos = s->getPosition();
                if (pos.y < vSpace)
                {
                    vSpace = pos.y;
                    vCount++;
                    hCount = 0.f;
                }
                s->move({ hCount * m_hSpacing, vCount * -m_vSpacing });
                hCount++;
            }

            if (m_dirtyBits & DirtyBits::COLOUR)
                s->setColour(m_colour);

            if (m_dirtyBits & DirtyBits::OFFSET)
                s->move({ 0.f, m_verticalOffset });

            if (m_dirtyBits & DirtyBits::ORIGIN)
                s->move(-m_origin);        
        } 
    }

    if (m_dirtyBits & DirtyBits::TRANSFORM)
    {
        m_spriteBatch->setViewMatrix(m_transform.getMatrix());
    }

    m_dirtyBits = 0u;
}