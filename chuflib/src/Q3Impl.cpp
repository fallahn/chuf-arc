/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/loaders/Q3Impl.hpp>
#include <chuf2/Log.hpp>
#include <chuf2/SubMesh.hpp>
#include <chuf2/Util.hpp>
#include <chuf2/Image.hpp>
#include <chuf2/Texture.hpp>
#include <chuf2/MaterialProperty.hpp>
#include <chuf2/Model.hpp>
#include <chuf2/Scene.hpp>
#include <chuf2/Reports.hpp>

#include <bitset>
#include <fstream>
#include <cassert>

using namespace chuf;

namespace
{
    glm::vec2 toGlm(const Bsp::Q3::Vec2f& vec2)
    {
        return glm::vec2(vec2.x, vec2.y);
    }

    glm::vec3 toGlm(const Bsp::Q3::Vec3f& vec3)
    {
        return glm::vec3(vec3.x, vec3.y, vec3.z);
    }

    template <typename T>
    void parseLump(std::vector<T>& dest, const char* src, Bsp::Q3::Lumps type, const Bsp::Q3::Lump* lumps)
    {
        UInt32 count = lumps[type].length / sizeof(T);
        dest.resize(count);
        std::memcpy(dest.data(), src + lumps[type].offset, lumps[type].length);
    }
}

//ctor / dtor
Q3Impl::Q3Impl(const std::string& path)
{
    loadMap(path);
}

//public
void Q3Impl::draw(UInt32 passFlags)
{
    if ((passFlags & RenderPass::Reflection) && flagDirty(DirtyBits::FRUSTUM_REFLECTION))
    {
        auto camera = getCamera();
        const auto& reflectionFrustum = camera->getReflectionViewFrustum();
        const auto camPos = camera->getNode()->getTranslationWorld();
        updateVisibility(camPos, reflectionFrustum);
    }
    else if (flagDirty(DirtyBits::FRUSTUM))
    {
        auto camera = getCamera();
        const auto& frustum = camera->getViewFrustum();
        const auto camPos = camera->getNode()->getTranslationWorld();
        updateVisibility(camPos, frustum);
    }
    
    if (passFlags & RenderPass::AlphaBlend)
    {
        auto& faces = getTransparentFaces();
        for (auto i : faces)
        {
            drawFace(i, passFlags);
        }
        faces.clear();
        return;
    }

    //TODO update reflect / refract
    
    //else draw normally
    const auto& visibleFaces = getVisibleFaces();
    for (auto i : visibleFaces)
    {
        drawFace(i, passFlags);
    }
}

void Q3Impl::setNode(Node* n)
{
    BspImpl::setNode(n);
    if (n) //make sure we transform bounding boxes into node space
    {
        transformLeafBoundingBoxes();
    }
}

void Q3Impl::transformChanged(Transform&)
{
    transformLeafBoundingBoxes();
}

//private
void Q3Impl::updateVisibility(const glm::vec3& position, const Frustum& frustum)
{
    auto& visibleFaces = getVisibleFaces();
    visibleFaces.clear();

    //find out position in the tree - remember to put our player position in BSP space
    auto leafIndex = findLeaf(glm::vec3(glm::vec4(position, 1.f) * getNode()->getInverseTransposeWorldMatrix()));
    auto clusterIndex = m_leaves[leafIndex].cluster;

    int drawCount = 0;
    int clustersSkipped = 0;
    int leavesCulled = 0;


    std::bitset<10000> bitset; //use this to mark drawn faces
    Int32 i = m_leaves.size();
    while (i--)
    {
        const auto& leaf = m_leaves[i];

        if (!clusterVisible(clusterIndex, leaf.cluster))
        {
            clustersSkipped++;
            continue;
        }

        //check leaf bb is in camera frustum
        const BoundingBox& bb = m_leafBoundingBoxes[i].second;
        if (!frustum.intersects(bb))
        {
            leavesCulled++;
            continue;
        }

        Int32 faceCount = leaf.faceCount;
        //sort the visible  faces by material ID to reduce state switching
        while (faceCount--)
        {
            Int32 faceIndex = m_leafFaces[leaf.firstFace + faceCount];

            if (m_faces[faceIndex].type != Bsp::Q3::POLY) continue; //TODO draw functions for other types

            if (!bitset.test(faceIndex))
            {
                bitset.set(faceIndex);
                visibleFaces.push_back(faceIndex);
                drawCount++;
            }
        }
    }

    std::sort(visibleFaces.begin(), visibleFaces.end(), [&](Int32 left, Int32 right)
    {
        const auto& leftFace = m_faces[left];
        const auto& rightFace = m_faces[right];

        if (leftFace.materialID == rightFace.materialID)
            return (leftFace.lightmapID < rightFace.lightmapID);
        else
            return (leftFace.materialID < rightFace.materialID);
    });

    REPORT("Clusters Dropped", std::to_string(clustersSkipped));
    REPORT("Leaves Culled", std::to_string(leavesCulled));
    REPORT("Faces Drawn", std::to_string(drawCount));
    REPORT("Current Leaf", std::to_string(leafIndex));
}

bool Q3Impl::loadMap(const std::string& map)
{
    std::fstream file;
    file.open(map, std::ios::in | std::ios::binary);

    if (file.fail())
    {
        Logger::Log("Failed to open file " + map, Logger::Type::Error);
        return false;
    }

    file.seekg(0, std::ios::end);
    UInt32 filesize = static_cast<UInt32>(file.tellg());
    file.seekg(0, std::ios::beg);

    if (filesize == 0u)
    {
        Logger::Log("BSP file size 0", Logger::Type::Error);
        return false;
    }

    //copy file into memory
    std::vector<char> bspData(filesize);
    file.read((char*)bspData.data(), filesize);
    file.close();

    //-------------------------------------------------------

    Bsp::Q3::Header header;
    Bsp::Q3::Lump lumps[Bsp::Q3::Lumps::MaxLumps];

    char* dataPtr = bspData.data();
    std::memcpy(&header, dataPtr, sizeof(Bsp::Q3::Header));
    //header validation
    std::string id(header.id, 4);
    if (id != "IBSP")
    {
        Logger::Log("Incorrect header ID found in Q3 BSP: " + id + ". Should be IBSP.", Logger::Type::Error);
        return false;
    }
    
    if (header.version != 46)
    {
        Logger::Log("Incorrect Q3 BSP version: " + std::to_string(header.version) + ". Should be 0x2e.", Logger::Type::Error);
        return false;
    }

    std::memcpy(&lumps, dataPtr + sizeof(Bsp::Q3::Header), Bsp::Q3::Lumps::MaxLumps * sizeof(Bsp::Q3::Lump));

    //-------load the geometry data and create VBOs / IBOs-----//
    std::vector<Bsp::Q3::Vertex> vertices;
    parseLump<Bsp::Q3::Vertex>(vertices, dataPtr, Bsp::Q3::Vertices, lumps);
    UInt32 vertCount = vertices.size();

    //copy faces - keep these around as the data is used for rendering
    parseLump<Bsp::Q3::Face>(m_faces, dataPtr, Bsp::Q3::Faces, lumps);

    //copy indices
    std::vector<Int32> indices;
    parseLump<Int32>(indices, dataPtr, Bsp::Q3::Indices, lumps);

    //load texture data and create materials
    std::vector<Bsp::Q3::Texture> textures;
    parseLump<Bsp::Q3::Texture>(textures, dataPtr, Bsp::Q3::Textures, lumps);
    //TODO surface flags are ignored - should these be included as part of the material definition?
    loadMaterials(textures);

    //load lightmap data
    UInt32 lightmapCount = lumps[Bsp::Q3::Lightmaps].length / sizeof(Bsp::Q3::Lightmap);
    char* lightmapPtr = dataPtr + lumps[Bsp::Q3::Lightmaps].offset;
    buildLightmaps(lightmapPtr, lightmapCount);


    //-------------create mesh data-------------//
    //tangent normal generation for bump mapped geometry
    std::vector<glm::vec3> tangents(vertCount);
    std::vector<glm::vec3> bitangents(vertCount);

    buildNormals(vertices, indices, tangents, bitangents);
    createMesh(vertices, indices, tangents, bitangents);

    //--------parse the visibility and culling data-----//
    parseLump<Bsp::Q3::Node>(m_nodes, dataPtr, Bsp::Q3::Nodes, lumps);
    parseLump<Bsp::Q3::Leaf>(m_leaves, dataPtr, Bsp::Q3::Leaves, lumps);

    //cache the bounding boxes (remember to swap coords!)
    for (const auto& l : m_leaves)
    {
        BoundingBox bb(glm::vec3(l.bbMin.x, l.bbMin.z, -l.bbMin.y), glm::vec3(l.bbMax.x, l.bbMax.z, -l.bbMax.y));
        m_leafBoundingBoxes.emplace_back(std::make_pair(bb, bb));
    }

    parseLump<Int32>(m_leafFaces, dataPtr, Bsp::Q3::LeafFaces, lumps);
    parseLump<Bsp::Q3::Plane>(m_planes, dataPtr, Bsp::Q3::Planes, lumps);
    for (auto& p : m_planes) //do the coord swapping thing
    {
        float temp = p.normal.y;
        p.normal.y = p.normal.z;
        p.normal.z = -temp;
    }

    if (lumps[Bsp::Q3::VisData].length > 0)
    {
        char* visPtr = dataPtr + lumps[Bsp::Q3::VisData].offset;
        std::memcpy(&m_clusters.clusterCount, visPtr, sizeof(Int32));
        visPtr += sizeof(Int32);
        std::memcpy(&m_clusters.bytesPerCluster, visPtr, sizeof(Int32));
        visPtr += sizeof(Int32);

        Int32 visSize = m_clusters.clusterCount * m_clusters.bytesPerCluster;
        m_clusterBitsets.resize(visSize);
        std::memcpy(m_clusterBitsets.data(), visPtr, sizeof(Int8) * visSize);
        m_clusters.bitsetArray = m_clusterBitsets.data(); //we point to the vector to save doing a new/delete on cluster pointer
    }


    //entities data
    std::vector<char> entityList;
    parseLump<char>(entityList, dataPtr, Bsp::Q3::Entities, lumps);
    //std::cout << std::string(entityList.data()) << std::endl;

    return true;
}

void Q3Impl::loadMaterials(const std::vector<Bsp::Q3::Texture>& textures)
{
    auto& materials = getMaterials();    
    for (const auto& t : textures)
    {
        std::string name(t.name);
        if (name != "noshader")
        {
            //if we have a texture with the same name just copy the material pointer
            //instead of creating a new material with the same properties
            //name = name.substr(name.find_last_of('/') + 1);
            //name = "woodceiling1a";
            auto result = std::find_if(materials.begin(), materials.end(),
                [&name](const std::pair<Material::Ptr, std::vector<MaterialProperty*>>& p)
            {
                return (p.first->hasTag(name));
            });

            if (result == materials.end())
            {
                auto material = Material::createFromFile("res/materials/" + name + ".cmf");
                if (!material) //find a fallback
                {
                    material = Material::getDefault();
                }
                material->addTag(name, "null");
                materials.push_back(std::make_pair(material, std::vector<MaterialProperty*>({ material->getProperty("u_lightmapTexture")})));
            }
            else
            {
                materials.push_back(*result);
            }
        }
        else
        {
            //TODO replace with an empty material
            auto material = Material::createFromFile("res/materials/stucco7top.cmf");
            materials.push_back(std::make_pair(material, std::vector<MaterialProperty*>({ material->getProperty("u_lightmapTexture") })));
        }
    }
}

void Q3Impl::buildLightmaps(const char* lightmapPtr, UInt32 lightmapCount)
{
    auto& lightmaps = getLightmaps();
    for (auto i = 0u; i < lightmapCount; ++i)
    {
        auto image = Image::create(128u, 128u, Image::Format::RGB, (UInt8*)(lightmapPtr));
        image->adjustGamma(8.f);
        image->resize(512u, 512u);
        //image->blur(2u);
        //image->save(image->getUid() + ".png");
        auto texture = Texture::create(image);
        lightmaps.emplace_back(Texture::Sampler::create(texture));
        lightmaps.back()->setFilter(Texture::Nearest, Texture::Linear);

        lightmapPtr += sizeof(Bsp::Q3::Lightmap);
    }
    //add a blank lightmap at the end for faces which don't use a map
    //then faces with negative lightmap indices can be given this (because the shader expects map data)
    auto image = Image::create(2u, 2u, Image::Format::RGB);
    auto texture = Texture::create(image);
    lightmaps.emplace_back(Texture::Sampler::create(texture));
}

void Q3Impl::buildNormals(const std::vector<Bsp::Q3::Vertex>& vertices, const std::vector<Int32>& indices,
    std::vector<glm::vec3>& tangents, std::vector<glm::vec3>& bitangents)
{
    for (auto& f : m_faces)
    {
        //calc face normal
        std::vector<glm::vec3> facePositions =
        {
            toGlm(vertices[f.firstVertIndex + indices[f.startIndex]].position),
            toGlm(vertices[f.firstVertIndex + indices[f.startIndex + 1]].position),
            toGlm(vertices[f.firstVertIndex + indices[f.startIndex + 2]].position)
        };

        glm::vec3 deltaPos1 = facePositions[1] - facePositions[0];
        glm::vec3 deltaPos2 = facePositions[2] - facePositions[0];
        //glm::vec3 faceNormal = glm::cross(deltaPos1, deltaPos2); //use the stored normals for now

        //calc tangents
        std::vector<glm::vec2> faceUVs =
        {
            toGlm(vertices[f.firstVertIndex + indices[f.startIndex]].uv0),
            toGlm(vertices[f.firstVertIndex + indices[f.startIndex + 1]].uv0),
            toGlm(vertices[f.firstVertIndex + indices[f.startIndex + 2]].uv0)
        };

        glm::vec2 deltaUV1 = faceUVs[1] - faceUVs[0];
        glm::vec2 deltaUV2 = faceUVs[2] - faceUVs[0];

        const float sign = 1.f / (deltaUV1.x * deltaUV2.y - deltaUV1.y * deltaUV2.x);
        glm::vec3 faceTan = -(deltaPos1 * deltaUV2.y - deltaPos2 *deltaUV1.y) * sign;
        glm::vec3 faceBitan = -(deltaPos2 * deltaUV1.x - deltaPos1 * deltaUV2.x) * sign;

        //make sure to add to ALL verts in a face
        for (auto i = 0; i < f.indexCount; ++i)
        {
            tangents[f.firstVertIndex + indices[f.startIndex + i]] += faceTan;
            bitangents[f.firstVertIndex + indices[f.startIndex + i]] += faceBitan;
        }

        //while we're iterating faces update the lightmap index
        if (f.lightmapID < 0) f.lightmapID = getLightmaps().size() - 1;
    }
    //normalise vectors afterwards as multiple vectorss may be added to a vertex
    for (auto& t : tangents) t = glm::normalize(t);
    for (auto& b : bitangents) b = glm::normalize(b);
}

void Q3Impl::createMesh(const std::vector<Bsp::Q3::Vertex>& vertices, const std::vector<Int32>& indices,
    const std::vector<glm::vec3>& tangents, const std::vector<glm::vec3>& bitangents)
{

    std::vector<VertexLayout::Element> elements =
    {
        VertexLayout::Element(VertexLayout::Type::POSITION, 3u),
        VertexLayout::Element(VertexLayout::Type::UV0, 2u),
        VertexLayout::Element(VertexLayout::Type::UV1, 2u),
        VertexLayout::Element(VertexLayout::Type::NORMAL, 3u),
        VertexLayout::Element(VertexLayout::Type::TANGENT, 3u),
        VertexLayout::Element(VertexLayout::Type::BITANGENT, 3u)
    };
    UInt32 vertCount = vertices.size();
    std::vector<float> vertexData;
    for (auto i = 0u; i < vertCount; ++i)
    {
        vertexData.push_back(vertices[i].position.x);
        vertexData.push_back(vertices[i].position.z); //swap space
        vertexData.push_back(-vertices[i].position.y);
        vertexData.push_back(vertices[i].uv0.x);
        vertexData.push_back(vertices[i].uv0.y);
        vertexData.push_back(vertices[i].uv1.x);
        vertexData.push_back(vertices[i].uv1.y);
        vertexData.push_back(vertices[i].normal.x);
        vertexData.push_back(vertices[i].normal.z);
        vertexData.push_back(-vertices[i].normal.y);
        vertexData.push_back(tangents[i].x);
        vertexData.push_back(tangents[i].z);
        vertexData.push_back(-tangents[i].y);
        vertexData.push_back(bitangents[i].x);
        vertexData.push_back(bitangents[i].z);
        vertexData.push_back(-bitangents[i].y);
    }

    auto mesh = Mesh::createMesh(VertexLayout(elements), vertCount);
    mesh->setVertexData(vertexData.data(), vertCount);

    //we create a submesh for each face (they're quite large in world geometry)
    //so we can pick which faces to draw via the PVS partitioning
    for (const auto& f : m_faces)
    {
        auto& subMesh = mesh->addSubMesh(Mesh::PrimitiveType::Triangles, Mesh::IndexFormat::I32, f.indexCount);
        auto end = f.startIndex + f.indexCount;
        std::vector<Int32> subIndices(&indices[f.startIndex], &indices[(end < indices.size()) ? end : end - 1]);
        std::reverse(subIndices.begin(), subIndices.end()); //reverse face windings
        subMesh.setIndexData(subIndices.data(), 0u, subIndices.size());
    }

    //create vert attrib bindings for mesh/materials
    auto& materials = getMaterials();
    for (auto& m : materials)
    {
        auto tCount = m.first->getTechniqueCount();
        for (auto i = 0u; i < tCount; ++i)
        {
            auto t = m.first->getTechnique(i);
            assert(t);

            auto pCount = t->getPassCount();
            for (auto j = 0u; j < pCount; ++j)
            {
                auto p = t->getPass(j);
                assert(p);

                auto vb = VertexAttribBinding::create(mesh, p->getShader());
                p->setVertexAttribBinding(vb);
            }
        }
    }

    setMesh(mesh);
}

Int32 Q3Impl::findLeaf(const glm::vec3& camPos)
{
    Int32 i = 0;
    float distance = 0.f;

    //use the camera position to find the current leaf by walking the BSP tree
    while (i >= 0)
    {
        const auto& node = m_nodes[i];
        const auto& plane = m_planes[node.planeIndex];

        //check if camera is behind or in front of node plane, and walk accordingly
        distance = plane.normal.x * camPos.x + plane.normal.y * camPos.y + plane.normal.z * camPos.z - plane.distance;
        i = (distance >= 0) ? node.frontIndex : node.rearIndex;
    }

    return ~i; //-(i + 1) is our new index
}

bool Q3Impl::clusterVisible(Int32 currentCluster, Int32 testCluster)
{
    if (!m_clusters.bitsetArray || currentCluster < 0) return true;
    auto index = (currentCluster * m_clusters.bytesPerCluster) + (testCluster >> 3);
    if (index < 0) return true;//for some reason this occasionally evaluates to less than zero
    Int8 visSet = m_clusterBitsets[index];

    Int32 result = visSet & (1 << (testCluster & 7));
    return (result != 0);
}

void Q3Impl::drawFace(UInt32 index, UInt32 passFlags)
{
    const auto& face = m_faces[index];
    Int32 currentMaterial = face.materialID;
    Int32 vertOffset = face.firstVertIndex;
    Int32 lightmapIndex = face.lightmapID;

    //currently this is here as we don't yet handle special cases for geometry
    //not assigned a material
    auto& materials = getMaterials();
    assert(currentMaterial >= 0 && currentMaterial < materials.size());

    auto& material = materials[currentMaterial].first;
    if (!(passFlags & RenderPass::AlphaBlend) && material->hasFlag(RenderPass::AlphaBlend))
    {
        //skip face and store for transparent pass
        addTransparentFace(index);
        return;
    }

    materials[currentMaterial].second[0]->setSampler(getLightmaps()[lightmapIndex]);

    if (material->getType() == Material::Type::Water)
    {
        //TODO how to get water plane distance?
    }

    auto t = material->getTechnique();
    assert(t);

    const auto& subMesh = getMesh()->getSubMesh(index);
    auto passCount = t->getPassCount();
    for (auto i = 0u; i < passCount; ++i)
    {
        auto p = t->getPass(i);
        assert(p);

        if (p->flags() == passFlags)
        {
            p->bind();
            glCheck(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, subMesh.getIndexBuffer()));
            glCheck(glDrawElementsBaseVertex(static_cast<GLenum>(subMesh.getPrimitiveType()), subMesh.getIndexCount(), static_cast<GLenum>(subMesh.getIndexFormat()), 0, vertOffset));
            p->unbind();
        }
    }
}

void Q3Impl::transformLeafBoundingBoxes()
{
    assert(getNode());
    for (auto& p : m_leafBoundingBoxes)
    {
        BoundingBox bb(p.first);
        bb.transform(getNode()->getWorldMatrix());
        p.second = bb;
    }
}
