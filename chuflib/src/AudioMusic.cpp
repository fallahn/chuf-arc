/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/AudioMusic.hpp>
#include <chuf2/AudioFile.hpp>

using namespace chuf;

namespace
{
    using Lock = std::lock_guard<std::mutex>;
}

AudioMusic::AudioMusic()
    : m_file    (std::make_unique<AudioFile>()),
    m_duration  (0.f)
{

}

AudioMusic::~AudioMusic()
{
    stop();
}


//public
bool AudioMusic::open(const std::string& path)
{
    stop();

    if (!m_file->open(path)) return false;

    init();

    return true;
}

float AudioMusic::getDuration() const
{
    return m_duration;
}

//private
bool AudioMusic::onGetData(Chunk& data)
{
    Lock lock(m_mutex);

    data.data = &m_sampleData[0];
    data.sampleCount = m_file->read(&m_sampleData[0], m_sampleData.size());

    return (data.sampleCount == m_sampleData.size());
}

void AudioMusic::onSeek(float offset)
{
    Lock lock(m_mutex);
    m_file->seek(offset);
}

void AudioMusic::init()
{
    m_duration = static_cast<float>(m_file->getSampleCount()) / m_file->getSampleRate() / m_file->getChannelCount();

    m_sampleData.resize(m_file->getSampleRate() * m_file->getChannelCount());

    AudioStream::init(m_file->getChannelCount(), m_file->getSampleRate());
}