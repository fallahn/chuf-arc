/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/AudioSound.hpp>
#include <chuf2/AudioBuffer.hpp>
#include <chuf2/Util.hpp>

using namespace chuf;

AudioSound::AudioSound()
    : m_audioBuffer (nullptr)
{

}

AudioSound::AudioSound(const AudioBuffer& buffer)
    : m_audioBuffer (nullptr)
{
    setBuffer(buffer);
}

AudioSound::AudioSound(const AudioSound& other)
    : AudioSource   (other),
    m_audioBuffer   (nullptr)
{
    if (other.m_audioBuffer)
        setBuffer(*other.m_audioBuffer);

    setLooped(other.looped());
}

AudioSound::~AudioSound()
{
    stop();
    if (m_audioBuffer)
        m_audioBuffer->removeSound(this);
}

AudioSound& AudioSound::operator = (const AudioSound& rhv)
{
    if (m_audioBuffer)
    {
        stop();
        m_audioBuffer->removeSound(this);
        m_audioBuffer = nullptr;
    }

    if (rhv.m_audioBuffer)
        setBuffer(*rhv.m_audioBuffer);

    setLooped(rhv.looped());
    setPitch(rhv.getPitch());
    setAttenuation(rhv.getAttenuation());
    setPosition(rhv.getPosition());
    setVolume(rhv.getVolume());
    setRelativeToListener(rhv.relativeToListener());
    setMinDistance(rhv.getMinDistance());

    return *this;
}

//public
void AudioSound::play()
{
    alCheck(alSourcePlay(m_alId));
}

void AudioSound::pause()
{
    alCheck(alSourcePause(m_alId));
}

void AudioSound::stop()
{
    alCheck(alSourceStop(m_alId));
}

void AudioSound::setLooped(bool looped)
{
    alCheck(alSourcei(m_alId, AL_LOOPING, looped));
}

bool AudioSound::looped() const
{
    ALint looped = 0;
    alCheck(alGetSourcei(m_alId, AL_LOOPING, &looped));
    return (looped != 0);
}

void AudioSound::setBuffer(const AudioBuffer& buffer)
{
    if (m_audioBuffer)
    {
        stop();
        m_audioBuffer->removeSound(this);
    }

    m_audioBuffer = &buffer;
    m_audioBuffer->addSound(this);
    alCheck(alSourcei(m_alId, AL_BUFFER, m_audioBuffer->m_bufferId));
}

const AudioBuffer* AudioSound::getBuffer() const
{
    return m_audioBuffer;
}

void AudioSound::setPlayOffset(float offset)
{
    alCheck(alSourcef(m_alId, AL_SEC_OFFSET, offset));
}

float AudioSound::getPlayOffset() const
{
    ALfloat offset = 0.f;
    alCheck(alGetSourcef(m_alId, AL_SEC_OFFSET, &offset));
    return offset;
}

AudioSource::State AudioSound::getState() const
{
    return AudioSource::getState();
}

void AudioSound::resetBuffer()
{
    stop();

    if (m_audioBuffer)
    {
        alCheck(alSourcei(m_alId, AL_BUFFER, 0));
        m_audioBuffer->removeSound(this);
        m_audioBuffer = nullptr;
    }
}