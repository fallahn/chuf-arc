/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/Camera.hpp>
#include <chuf2/DataTypes.hpp>
#include <chuf2/Util.hpp>
#include <chuf2/Scene.hpp>
#include <chuf2/Model.hpp>
#include <chuf2/Mesh.hpp>
#include <chuf2/Material.hpp>
#include <chuf2/Reports.hpp>
#include <chuf2/Timer.hpp>

#include <chuf2/glm/gtc/matrix_transform.hpp>

#include <cassert>
#include <iostream>

namespace
{
    glm::mat4 identityMat;
}

using namespace chuf;

Camera::Camera(float x, float y, float nearPlane, float farPlane, float aspectRatio, const Key& key)
    : Drawable      (nullptr),
    m_type		    (Orthographic),
    m_fov			(0.f),
    m_orthoSize		(x, y),
    m_aspectRatio	(aspectRatio),
    m_nearPlane		(nearPlane),
    m_farPlane		(farPlane),
    m_active        (false),
    m_dirtyBits		(DirtyBits::ALL),
    m_skyboxModel   (nullptr)/*,
    m_skyboxNode    (nullptr)*/
{}

Camera::Camera(float fov, float nearPlane, float farPlane, float aspectRatio, const Key& key)
    : Drawable      (nullptr),
    m_type		    (Perspective),
    m_fov			(fov),
    m_aspectRatio	(aspectRatio),
    m_nearPlane		(nearPlane),
    m_farPlane		(farPlane),
    m_active        (false),
    m_dirtyBits     (DirtyBits::ALL),
    m_skyboxModel   (nullptr)/*,
    m_skyboxNode    (nullptr)*/
{}

//public
Camera::Ptr Camera::createOrthographic(float x, float y, float nearPlane, float farPlane, float aspectRatio)
{
    return std::make_unique<Camera>(x, y, nearPlane, farPlane, aspectRatio, Key());
}

Camera::Ptr Camera::createPerspective(float fov, float nearPlane, float farPlane, float aspectRatio)
{
    return std::make_unique<Camera>(Util::degToRad(fov), nearPlane, farPlane, aspectRatio, Key());
}

Camera::Type Camera::getType() const
{
    return m_type;
}

float Camera::getFov() const
{
    return m_fov;
}

void Camera::setFov(float fov)
{
    m_fov = fov;
    m_dirtyBits |= (PROJECTION | VIEW_PROJECTION | INV_VIEW_PROJECTION | FRUSTUM | REFL_FRUSTUM | REFLECTION | BOUNDS2D | BOUNDS3D);
    cameraChanged();
}

const glm::vec2& Camera::getOrthoSize() const
{
    return m_orthoSize;
}

void Camera::setOrthoSize(const glm::vec2& size)
{
    m_orthoSize = size;
    m_dirtyBits |= (PROJECTION | VIEW_PROJECTION | INV_VIEW_PROJECTION | FRUSTUM | REFL_FRUSTUM | REFLECTION | BOUNDS2D | BOUNDS3D);
    cameraChanged();
}

float Camera::getAspectRatio() const
{
    return m_aspectRatio;
}

void Camera::setAspectRatio(float ratio)
{
    m_aspectRatio = ratio;
    m_dirtyBits |= (PROJECTION | VIEW_PROJECTION | INV_VIEW_PROJECTION | FRUSTUM | REFL_FRUSTUM | REFLECTION | BOUNDS2D | BOUNDS3D);
    cameraChanged();
}

float Camera::getNearPlane() const
{
    return m_nearPlane;
}

void Camera::setNearPlane(float v)
{
    assert(v < m_farPlane);
    m_nearPlane = v;
    m_dirtyBits |= (PROJECTION | VIEW_PROJECTION | INV_VIEW_PROJECTION | FRUSTUM | REFL_FRUSTUM | REFLECTION | BOUNDS2D | BOUNDS3D);
    cameraChanged();
}

float Camera::getFarPlane() const
{
    return m_farPlane;
}

void Camera::setFarPlane(float v)
{
    assert(v > m_nearPlane);
    m_farPlane = v;
    m_dirtyBits |= (PROJECTION | VIEW_PROJECTION | INV_VIEW_PROJECTION | FRUSTUM | REFL_FRUSTUM | REFLECTION | BOUNDS2D | BOUNDS3D);
    cameraChanged();
}

void Camera::setReflectionPlane(const glm::vec4& p)
{
    m_reflectionPlane.set({{ p.x, p.y, p.z }, p.w});
    m_dirtyBits |= REFLECTION | REFL_FRUSTUM;
}

const glm::vec4& Camera::getReflectionPlane()
{
    return m_reflectionPlane.getVec4();
}

void Camera::invertReflectionPlane()
{
    m_reflectionPlane.invert();
    m_dirtyBits |= REFLECTION | FRUSTUM | REFL_FRUSTUM;
}

Node* Camera::getNode() const
{
    return const_cast<Node*>(Component::getNode());
}

const glm::mat4& Camera::getViewMatrix() const
{
    if (m_dirtyBits & VIEW)
    {
        if (getNode())
        {
            m_viewMat = glm::inverse(getNode()->getWorldMatrix());
        }
        else
        {
            m_viewMat = identityMat;
        }
        m_dirtyBits &= ~VIEW;
    }
    return m_viewMat;
}

const glm::mat4& Camera::getInverseViewMatrix() const
{
    if (m_dirtyBits & INV_VIEW)
    {
        m_inverseViewMat = glm::inverse(getViewMatrix());
        m_dirtyBits &= ~INV_VIEW;
    }
    return m_inverseViewMat;
}

const glm::mat4& Camera::getProjectionMatrix() const
{
    if (m_dirtyBits & PROJECTION)
    {
        if (m_type == Orthographic)
        {
            auto halfSize = m_orthoSize / 2.f;
            m_projectionMat = glm::ortho<float>(-halfSize.x, halfSize.x, -halfSize.y, halfSize.y, m_nearPlane, m_farPlane);
        }
        else
        {
            m_projectionMat = glm::perspective(m_fov, m_aspectRatio, m_nearPlane, m_farPlane);
        }
        m_dirtyBits &= ~PROJECTION;
    }
    return m_projectionMat;
}

const glm::mat4& Camera::getViewProjectionMatrix() const
{
    if (m_dirtyBits & VIEW_PROJECTION)
    {
        m_viewProjectionMat = getProjectionMatrix() * getViewMatrix();
        m_dirtyBits &= ~VIEW_PROJECTION;
    }
    return m_viewProjectionMat;
}

const glm::mat4& Camera::getInverseViewProjectionMatrix() const
{
    if (m_dirtyBits & INV_VIEW_PROJECTION)
    {
        m_inverseViewProjectionMat = glm::inverse(getViewProjectionMatrix());
        m_dirtyBits &= ~INV_VIEW_PROJECTION;
    }
    return m_inverseViewProjectionMat;
}

const glm::mat4& Camera::getReflectionViewMatrix() const
{
    //check flag and rebuild if necessary
    if (m_dirtyBits & (REFLECTION | REFL_FRUSTUM))
    {
        m_reflectionViewMat = getViewMatrix() * getReflectionMatrix();
    }
    return m_reflectionViewMat;
}

const glm::mat4& Camera::getReflectionViewProjectionMatrix() const
{
    //check flags to see if we rebuild matrix
    if (m_dirtyBits & (REFLECTION | REFL_FRUSTUM))
    {
        m_reflectionViewProjectionMat = getProjectionMatrix() * getReflectionViewMatrix();
        //bit is unset by getReflectionViewMatrix();
    }
    return m_reflectionViewProjectionMat;
}

const glm::mat4& Camera::getReflectionMatrix() const
{
    if (m_dirtyBits & (REFLECTION/* | REFL_FRUSTUM*/))
    {
        updateReflectionMatrix();
        m_dirtyBits &= ~REFLECTION;
        //m_dirtyBits &= ~REFL_FRUSTUM;
    }
    return m_reflectionMatrix;
}

const Frustum& Camera::getViewFrustum() const
{
    if (m_dirtyBits & FRUSTUM)
    {
        m_frustum.set(getViewProjectionMatrix());
        m_dirtyBits &= ~FRUSTUM;
    }
    return m_frustum;
}

const Frustum& Camera::getReflectionViewFrustum() const
{
    if (m_dirtyBits & REFL_FRUSTUM)
    {
        m_reflectionFrustum.set(getReflectionViewProjectionMatrix());
        m_dirtyBits &= ~REFL_FRUSTUM;
    }
    return m_reflectionFrustum;
}

const FloatRect& Camera::getFrustumBounds2D() const
{
    if (m_dirtyBits & BOUNDS2D)
    {
        updateBounds2D();
    }
    return m_frustumBounds2D;
}

const BoundingBox& Camera::getFrustumBounds3D() const
{
    if (m_dirtyBits & DirtyBits::BOUNDS3D)
    {
        updateBounds3D();
    }
    return m_frustumBounds3D;
}

glm::vec3 Camera::project(const FloatRect& viewport, const glm::vec3& worldPos) const
{
    auto clipspace = getViewProjectionMatrix() * glm::vec4(worldPos, 1.f);
    assert(clipspace.w != 0);
    float ndcX = clipspace.x / clipspace.w;
    float ndcY = clipspace.y / clipspace.w;
    float ndcZ = clipspace.z / clipspace.w;

    return
    {
        viewport.left + (ndcX + 1.f) * 0.5f * viewport.width,
        viewport.top + (1.f - (ndcY + 1.f) * 0.5f) * viewport.height,
        (ndcZ + 1.f) / 2.f
    };
}

void Camera::addListener(Camera::Listener& l)
{
    m_listeners.push_back(&l);
    cameraChanged();
}

void Camera::removeListener(Camera::Listener& l)
{
    m_listeners.remove_if([&](const Listener* lp){ return lp == &l; });
}

void Camera::setActive(bool active)
{
    m_active = active;
    Node* n = getNode();
    if (active && n)
    {
        Scene* s = n->getScene();
        if (s)
        {
            setRenderer(&s->getRenderer());
            if (m_skyboxNode)
            {
                m_skyboxNode->setScene(s);
            }
        }
    }
    else
    {
        setRenderer(nullptr);
        m_skyboxNode->setScene(nullptr);
    }
}

bool Camera::active() const
{
    return m_active;
}

void Camera::setSkybox(const std::string& path)
{
    auto skyboxModel = Model::create(Mesh::createCube(), nullptr);
    skyboxModel->setMaterial(path);

    if (skyboxModel->getMaterial()->getType() != Material::Type::SkyBox)
    {
        Logger::Log("skybox set with non-skybox material. This won't work", Logger::Type::Warning);
    }

    //skyboxModel->addTag("name", "skybox");
    m_skyboxNode = Node::create("skybox");
    m_skyboxModel = m_skyboxNode->addComponent<Model>(skyboxModel);
}

const BoundingBox& Camera::getBoundingBox() const
{
    return (m_skyboxModel)? m_skyboxModel->getBoundingBox() : getFrustumBounds3D();
}

UInt32 Camera::draw(UInt32 flags, bool) const
{
    //static Timer t;
    if (m_skyboxModel)
    {
        //REPORT("drawing skybox", std::to_string(t.elapsed().asMilliseconds()));
        return m_skyboxModel->draw(flags, false);
    }
    return 0;
}

//private
void Camera::setNode(Node* node)
{   
    if (getNode() != node)
    {
        Component::setNode(node);
        m_dirtyBits |= (PROJECTION | VIEW_PROJECTION | INV_VIEW | INV_VIEW_PROJECTION | FRUSTUM | REFLECTION | BOUNDS2D | BOUNDS3D);
        cameraChanged();
    }
}

void Camera::transformChanged(Transform& t)
{
    m_dirtyBits |= (VIEW | INV_VIEW | INV_VIEW_PROJECTION | VIEW_PROJECTION | FRUSTUM | REFL_FRUSTUM | REFLECTION | BOUNDS2D | BOUNDS3D);
    cameraChanged();
}

void Camera::cameraChanged()
{
    auto n = getNode();
    if (m_skyboxNode && n)
    {
        m_skyboxNode->setTranslation(n->getTranslationWorld());
    }
    
    for (auto& l : m_listeners)
        l->cameraChanged(this);
}

void Camera::updateReflectionMatrix() const
{
    const auto& normal = m_reflectionPlane.getNormal();
    float distance = m_reflectionPlane.getDistance();

    m_reflectionMatrix = glm::mat4 //columns
    (
        glm::vec4(1.f - 2.f * normal.x * normal.x, -2.f * normal.x * normal.y, -2.f * normal.x * normal.z, 0.f),
        glm::vec4(-2.f * normal.x * normal.y, 1.f - 2.f * normal.y * normal.y, -2.f * normal.y * normal.z, 0.f),
        glm::vec4(-2.f * normal.x * normal.z, -2.f * normal.y * normal.z, 1.f - 2.f * normal.z * normal.z, 0.f),
        glm::vec4(-2.f * normal.x * distance, -2.f * normal.y * distance, -2.f * normal.z * distance, 1.f)
    );
}

void Camera::updateBounds2D() const
{    
    if (m_dirtyBits & BOUNDS3D)
    {
        updateBounds3D();
    }

    auto min = m_frustumBounds3D.getMinPoint();
    auto max = m_frustumBounds3D.getMaxPoint();

    m_frustumBounds2D.left = min.x;
    m_frustumBounds2D.top = min.z;
    m_frustumBounds2D.width = max.x - min.x;
    m_frustumBounds2D.height = max.z - min.z;

    m_dirtyBits &= ~BOUNDS2D;
}

void Camera::updateBounds3D() const
{
    auto corners = getViewFrustum().getCorners();
    glm::vec3 min(FLT_MAX), max(-FLT_MAX);
    for (const auto& c : corners)
    {
        if (c.x < min.x) min.x = c.x;
        else if (c.x > max.x) max.x = c.x;
        if (c.y < min.y) min.y = c.y;
        else if (c.y > max.y) max.y = c.y;
        if (c.z < min.z) min.z = c.z;
        else if (c.z > max.z) max.z = c.z;
    }
    m_frustumBounds3D.set({ min, max });

    m_dirtyBits &= ~BOUNDS3D;
}
