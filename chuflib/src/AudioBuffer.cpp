/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/AudioBuffer.hpp>
#include <chuf2/AudioFile.hpp>
#include <chuf2/AudioListener.hpp>
#include <chuf2/AudioSound.hpp>
#include <chuf2/Log.hpp>
#include <chuf2/Util.hpp>

#include <sstream>

using namespace chuf;

AudioBuffer::AudioBuffer()
    : m_bufferId    (0),
    m_duration      (0.f)
{
    alCheck(alGenBuffers(1, &m_bufferId));
}

AudioBuffer::AudioBuffer(const AudioBuffer& other)
    : m_bufferId    (0),
    m_sampleData    (other.m_sampleData),
    m_duration      (other.m_duration)
{
    alCheck(alGenBuffers(1, &m_bufferId));

    update(other.getChannelCount(), other.getSampleRate());
}

AudioBuffer::~AudioBuffer()
{
    for (auto& s : m_audioSounds)
    {
        s->resetBuffer();
    }
     
    if (m_bufferId)
    {
        alCheck(alDeleteBuffers(1, &m_bufferId));
    }
}

//public
bool AudioBuffer::create(const std::string& path)
{
    AudioFile file;
    if (file.open(path))
        return init(file);
    else
        return false;
}

bool AudioBuffer::create(const void* data, UInt32 size)
{
    AudioFile file;
    if (file.open(data, size))
        return init(file);
    else
        return false;
}

const Int16* AudioBuffer::getSamples() const
{
    return m_sampleData.empty() ? nullptr : &m_sampleData[0];
}

UInt32 AudioBuffer::getSampleCount() const
{
    return m_sampleData.size();
}

UInt32 AudioBuffer::getSampleRate() const
{
    ALint sr = 0;
    alCheck(alGetBufferi(m_bufferId, AL_FREQUENCY, &sr));
    return sr;
}

UInt32 AudioBuffer::getChannelCount() const
{
    ALint cc = 0;
    alCheck(alGetBufferi(m_bufferId, AL_CHANNELS, &cc));
    return cc;
}

float AudioBuffer::getDuration() const
{
    return m_duration;
}

//private
bool AudioBuffer::init(AudioFile& file)
{
    UInt32 sampleCount = file.getSampleCount();
    UInt32 channelCount = file.getChannelCount();
    UInt32 sampleRate = file.getSampleRate();

    m_sampleData.resize(sampleCount);

    if (file.read(&m_sampleData[0], sampleCount) == sampleCount)
    {
        return update(channelCount, sampleRate);
    }
    else
    {
        return false;
    }
}

bool AudioBuffer::update(UInt32 channelCount, UInt32 sampleRate)
{
    if (!channelCount || !sampleRate || m_sampleData.empty())
        return false;

    ALenum format = AudioListener::getFormatFromChannelCount(channelCount);
    if (format == 0)
    {
        std::stringstream ss;
        ss << "failed to load audio buffer, " << channelCount << " unsupported channel count" << std::endl;
        Logger::Log(ss.str(), Logger::Type::Error);
        return false;
    }

    SoundList sounds(m_audioSounds);
    for (auto& s : sounds)
        s->resetBuffer();

    ALsizei size = static_cast<ALsizei>(m_sampleData.size()) * sizeof(Int16);
    alCheck(alBufferData(m_bufferId, format, &m_sampleData[0], size, sampleRate));

    m_duration = static_cast<float>(m_sampleData.size()) / sampleRate / channelCount;

    for (auto& s : sounds)
        s->setBuffer(*this);

    return true;
}

void AudioBuffer::addSound(AudioSound* sound) const
{
    m_audioSounds.insert(sound);
}

void AudioBuffer::removeSound(AudioSound* sound) const
{
    m_audioSounds.erase(sound);
}