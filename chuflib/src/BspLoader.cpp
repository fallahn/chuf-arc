/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/loaders/Bsp.hpp>
#include <chuf2/loaders/Q3Impl.hpp>
#include <chuf2/loaders/SourceBspImpl.hpp>

#include <cassert>

using namespace chuf;

BspMap::BspMap(const std::string& path, SceneRenderer* sr, BspMap::Type type, const BspMap::Key& key)
    : Drawable      (sr),
    m_type          (type),
    m_activeCamera  (nullptr),
    m_bb            ({ -1000.f, -1000.f, -1000.f }, {1000.f, 1000.f, 1000.f})
{
    if (type == BspMap::Type::QuakeThree)
    {
        m_impl = std::make_unique<Q3Impl>(path);
        assert(m_impl);
    }
    else if (type == BspMap::Type::Source)
    {
        m_impl = std::make_unique<SourceBspImpl>(path);
        assert(m_impl);
    }
}

//public
BspMap::Ptr BspMap::create(const std::string& path, SceneRenderer* sr, BspMap::Type type)
{
    return std::make_unique<BspMap>(path, sr, type, Key());
}

void BspMap::setActiveCamera(Camera* cam)
{
    if (m_activeCamera)
    {
        m_activeCamera->removeListener(*m_impl.get());
    }
    m_activeCamera = cam;
    if (cam)
    {
        cam->addListener(*m_impl.get());
    }
}

UInt32 BspMap::draw(UInt32 passFlags, bool) const
{
    m_impl->draw(passFlags);
    return 0;
}

void BspMap::setNode(Node* n)
{
    Component::setNode(n);
    m_impl->setNode(n);
}

//private
void BspMap::transformChanged(Transform& t)
{
    m_impl->transformChanged(t);
}