/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/RenderState.hpp>

#include <chuf2/Util.hpp>

using namespace chuf;

RenderState::StateBlock::Ptr RenderState::StateBlock::m_defaultState = std::make_unique<RenderState::StateBlock>(RenderState::StateBlock::Key());


RenderState::StateBlock::StateBlock(const Key& key)
    : m_cullFaceEnabled		(false),
    m_depthTestEnabled		(false),
    m_depthWriteEnabled		(true),
    m_blendEnabled			(false),
    m_clipPlaneEnabled      (false),
    m_depthFunc				(DepthFunc::LESS),
    m_blendSrc				(BlendFunc::ONE),
    m_blendDest				(BlendFunc::ZERO),
    m_cullFace				(CullFace::BACK),
    m_winding				(Winding::COUNTER_CLOCKWISE),
    m_clipDistance          (ClipDistance::Zero),
    m_stencilTestEnabled	(false),
    m_stencilWrite			(StateOverride::ALL_ONE),
    m_stencilFunc			(StencilFunc::ALWAYS),
    m_stencilFuncReference	(0),
    m_stencilFuncMask		(StateOverride::ALL_ONE),
    m_stencilFail			(StencilOp::KEEP),
    m_depthFail				(StencilOp::KEEP),
    m_stencilDepthPass		(StencilOp::KEEP),
    m_overrideBits			(0u)
{}

RenderState::StateBlock::Ptr RenderState::StateBlock::create()
{
    return std::make_unique<StateBlock>(StateBlock::Key());
}

RenderState::StateBlock::Ptr RenderState::StateBlock::clone()
{
    auto newBlock = create();

    newBlock->m_cullFaceEnabled = m_cullFaceEnabled;
    newBlock->m_depthTestEnabled = m_depthTestEnabled;
    newBlock->m_depthWriteEnabled = m_depthWriteEnabled;
    newBlock->m_blendEnabled = m_blendEnabled;
    newBlock->m_clipPlaneEnabled = m_clipPlaneEnabled;
    newBlock->m_depthFunc = m_depthFunc;
    newBlock->m_blendSrc = m_blendSrc;
    newBlock->m_blendDest = m_blendDest;
    newBlock->m_cullFace = m_cullFace;
    newBlock->m_winding = m_winding;
    newBlock->m_clipDistance = m_clipDistance;
    newBlock->m_stencilTestEnabled = m_stencilTestEnabled;
    newBlock->m_stencilWrite = m_stencilWrite;
    newBlock->m_stencilFunc = m_stencilFunc;
    newBlock->m_stencilFuncReference = m_stencilFuncReference;
    newBlock->m_stencilFuncMask = m_stencilFuncMask;
    newBlock->m_stencilFail = m_stencilFail;
    newBlock->m_depthFail = m_depthFail;
    newBlock->m_stencilDepthPass = m_stencilDepthPass;
    newBlock->m_overrideBits = m_overrideBits;

    return std::move(newBlock);
}

//public
void RenderState::StateBlock::setDepthTest(bool b)
{
    m_depthTestEnabled = b;
    if (b)
    {
        m_overrideBits |= StateOverride::DEPTH_TEST;
    }
    else
    {
        m_overrideBits &= ~StateOverride::DEPTH_TEST;
    }
}

void RenderState::StateBlock::setDepthWrite(bool b)
{
    m_depthWriteEnabled = b;
    if (b)
    {
        m_overrideBits |= StateOverride::DEPTH_WRITE;
    }
    else
    {
        m_overrideBits &= ~StateOverride::DEPTH_WRITE;
    }
}

void RenderState::StateBlock::setDepthFunc(RenderState::DepthFunc func)
{
    m_depthFunc = func;
    if (func == DepthFunc::LESS)
    {
        m_overrideBits &= ~StateOverride::DEPTH_FUNC;
    }
    else
    {
        m_overrideBits |= StateOverride::DEPTH_FUNC;
    }
}

void RenderState::StateBlock::setBlend(bool b)
{
    m_blendEnabled = b;
    if (b)
    {
        m_overrideBits |= StateOverride::BLEND;
    }
    else
    {
        m_overrideBits &= ~StateOverride::BLEND;
    }
}

void RenderState::StateBlock::setBlendSrc(BlendFunc func)
{
    m_blendSrc = func;
    if (func == BlendFunc::ONE && m_blendDest == BlendFunc::ZERO)
    {
        m_overrideBits &= ~StateOverride::BLEND_FUNC;
    }
    else
    {
        m_overrideBits |= StateOverride::BLEND_FUNC;
    }
}

void RenderState::StateBlock::setBlendDest(BlendFunc func)
{
    m_blendDest = func;
    if (m_blendSrc == BlendFunc::ONE && func == BlendFunc::ZERO)
    {
        m_overrideBits &= ~StateOverride::BLEND_FUNC;
    }
    else
    {
        m_overrideBits |= StateOverride::BLEND_FUNC;
    }
}

void RenderState::StateBlock::setStencilTest(bool b)
{
    m_stencilTestEnabled = b;
    if (b)
    {
        m_overrideBits |= StateOverride::STENCIL_TEST;
    }
    else
    {
        m_overrideBits &= ~StateOverride::STENCIL_TEST;
    }
}

void RenderState::StateBlock::setStencilWrite(UInt32 mask)
{
    m_stencilWrite = mask;
    if (mask == StateOverride::ALL_ONE)
    {
        m_overrideBits &= ~StateOverride::STENCIL_WRITE;
    }
    else
    {
        m_overrideBits |= StateOverride::STENCIL_WRITE;
    }
}

void RenderState::StateBlock::setStencilFunc(RenderState::StencilFunc func, Int32 reference, UInt32 mask)
{
    m_stencilFunc = func;
    m_stencilFuncReference = reference;
    m_stencilFuncMask = mask;
    if (func == StencilFunc::ALWAYS && reference == 0 && mask == ALL_ONE)
    {
        m_overrideBits &= ~StateOverride::STENCIL_FUNC;
    }
    else
    {
        m_overrideBits |= StateOverride::STENCIL_FUNC;
    }
}

void RenderState::StateBlock::setStencilOp(RenderState::StencilOp stencilFail, RenderState::StencilOp depthFail, RenderState::StencilOp pass)
{
    m_stencilFail = stencilFail;
    m_depthFail = depthFail;
    m_stencilDepthPass = pass;

    if (stencilFail == StencilOp::KEEP &&
        depthFail == StencilOp::KEEP &&
        pass == StencilOp::KEEP)
    {
        m_overrideBits &= ~StateOverride::STENCIL_OP;
    }
    else
    {
        m_overrideBits |= StateOverride::STENCIL_OP;
    }
}

void RenderState::StateBlock::setFrontFace(Winding direction)
{
    m_winding = direction;
    if (direction == CLOCKWISE)
    {
        m_overrideBits |= StateOverride::FRONT_FACE;
    }
    else
    {
        m_overrideBits &= ~StateOverride::FRONT_FACE;
    }
}

void RenderState::StateBlock::setCullFace(bool b)
{
    m_cullFaceEnabled = b;
    if (b)
    {
        m_overrideBits |= StateOverride::CULL_FACE;
    }
    else
    {
        m_overrideBits &= ~StateOverride::CULL_FACE;
    }
}

void RenderState::StateBlock::setCullFaceSide(RenderState::CullFace side)
{
    m_cullFace = side;
    if (side == BACK)
    {
        m_overrideBits &= ~StateOverride::CULL_FACE_SIDE;
    }
    else
    {
        m_overrideBits |= StateOverride::CULL_FACE_SIDE;
    }
}

void RenderState::StateBlock::setClip(bool b)
{
    m_clipPlaneEnabled = b;
    if (b)
    {
        m_overrideBits |= StateOverride::CLIP_PLANE;
    }
    else
    {
        m_overrideBits &= ~StateOverride::CLIP_PLANE;
    }
}

void RenderState::StateBlock::bind()
{
    restore(m_overrideBits);
    bindNoRestore();
}

//private
void RenderState::StateBlock::bindNoRestore()
{
    assert(m_defaultState);

    if ((m_overrideBits & StateOverride::BLEND) 
        && (m_blendEnabled != m_defaultState->m_blendEnabled))
    {
        if (m_blendEnabled)
        {
            glCheck(glEnable(GL_BLEND));
        }
        else
        {
            glCheck(glDisable(GL_BLEND));
        }

        m_defaultState->m_blendEnabled = m_blendEnabled;
    }

    if ((m_overrideBits & StateOverride::BLEND_FUNC) 
        && (m_blendSrc != m_defaultState->m_blendSrc || m_blendDest != m_defaultState->m_blendDest))
    {
        glCheck(glBlendFunc(static_cast<GLenum>(m_blendSrc), static_cast<GLenum>(m_blendDest)));
        m_defaultState->m_blendSrc = m_blendSrc;
        m_defaultState->m_blendDest = m_blendDest;
    }

    if ((m_overrideBits & StateOverride::CULL_FACE)
        && (m_cullFaceEnabled != m_defaultState->m_cullFaceEnabled))
    {
        if (m_cullFaceEnabled)
        {
            glCheck(glEnable(GL_CULL_FACE));
        }
        else
        {
            glCheck(glDisable(GL_CULL_FACE));
        }
        m_defaultState->m_cullFaceEnabled = m_cullFaceEnabled;
    }

    if ((m_overrideBits & StateOverride::CLIP_PLANE)
        && (m_clipPlaneEnabled != m_defaultState->m_clipPlaneEnabled))
    {
        if (m_clipPlaneEnabled)
        {
            glCheck(glEnable(static_cast<GLenum>(m_clipDistance)));
        }
        else
        {
            glCheck(glDisable(static_cast<GLenum>(m_clipDistance)));
        }
        m_defaultState->m_clipPlaneEnabled = m_clipPlaneEnabled;
    }

    if ((m_overrideBits & StateOverride::CULL_FACE_SIDE)
        && (m_defaultState->m_cullFace != m_cullFace))
    {
        glCheck(glCullFace(static_cast<GLenum>(m_cullFace)));
        m_defaultState->m_cullFace = m_cullFace;
    }

    if ((m_overrideBits & StateOverride::FRONT_FACE)
        && (m_winding != m_defaultState->m_winding))
    {
        glCheck(glFrontFace(static_cast<GLenum>(m_winding)));
        m_defaultState->m_winding = m_winding;
    }

    if ((m_overrideBits & StateOverride::DEPTH_TEST)
        && (m_defaultState->m_depthTestEnabled != m_depthTestEnabled))
    {
        if (m_depthTestEnabled)
        {
            glCheck(glEnable(GL_DEPTH_TEST));
        }
        else
        {
            glCheck(glDisable(GL_DEPTH_TEST));
        }
        m_defaultState->m_depthTestEnabled = m_depthTestEnabled;
    }

    if ((m_overrideBits & StateOverride::DEPTH_WRITE)
        && (m_defaultState->m_depthWriteEnabled != m_depthWriteEnabled))
    {
        glCheck(glDepthMask(m_depthWriteEnabled ? GL_TRUE : GL_FALSE));
        m_defaultState->m_depthWriteEnabled = m_depthWriteEnabled;
    }

    if ((m_overrideBits & StateOverride::DEPTH_FUNC)
        && (m_depthFunc != m_defaultState->m_depthFunc))
    {
        glCheck(glDepthFunc(static_cast<GLenum>(m_depthFunc)));
        m_defaultState->m_depthFunc = m_depthFunc;
    }

    if ((m_overrideBits & StateOverride::STENCIL_TEST)
        && (m_stencilTestEnabled != m_defaultState->m_stencilTestEnabled))
    {
        if (m_stencilTestEnabled)
        {
            glCheck(glEnable(GL_STENCIL_TEST));
        }
        else
        {
            glCheck(glDisable(GL_STENCIL_TEST));
        }
        m_defaultState->m_stencilTestEnabled = m_stencilTestEnabled;
    }

    if ((m_overrideBits & StateOverride::STENCIL_WRITE)
        && (m_defaultState->m_stencilWrite != m_stencilWrite))
    {
        glCheck(glStencilMask(m_stencilWrite));
        m_defaultState->m_stencilWrite = m_stencilWrite;
    }

    if ((m_overrideBits & StateOverride::STENCIL_FUNC)
        && (m_stencilFunc != m_defaultState->m_stencilFunc
        || m_stencilFuncMask != m_defaultState->m_stencilFuncMask
        || m_stencilFuncReference != m_defaultState->m_stencilFuncReference))
    {
        glCheck(glStencilFunc(static_cast<GLenum>(m_stencilFunc), m_stencilFuncReference, m_stencilFuncMask));
        m_defaultState->m_stencilFunc = m_stencilFunc;
        m_defaultState->m_stencilFuncMask = m_stencilFuncMask;
        m_defaultState->m_stencilFuncReference = m_stencilFuncReference;
    }

    if ((m_overrideBits & StateOverride::STENCIL_OP)
        && (m_defaultState->m_stencilFail != m_stencilFail
        || m_defaultState->m_depthFail != m_depthFail
        || m_defaultState->m_stencilDepthPass != m_stencilDepthPass))
    {
        glCheck(glStencilOp(static_cast<GLenum>(m_stencilFail), static_cast<GLenum>(m_depthFail), static_cast<GLenum>(m_stencilDepthPass)));
        m_defaultState->m_stencilFail = m_stencilFail;
        m_defaultState->m_stencilDepthPass = m_stencilDepthPass;
        m_defaultState->m_depthFail = m_depthFail;
    }

    m_defaultState->m_overrideBits |= m_overrideBits;
}

void RenderState::StateBlock::restore(UInt64 overrideBits)
{
    assert(m_defaultState);

    if (m_defaultState->m_overrideBits == 0) return;

    if (!(overrideBits & StateOverride::BLEND) 
        && (m_defaultState->m_overrideBits & StateOverride::BLEND))
    {
        glCheck(glDisable(GL_BLEND));
        m_defaultState->m_overrideBits &= ~StateOverride::BLEND;
        m_defaultState->m_blendEnabled = false;
    }

    if (!(overrideBits & StateOverride::BLEND_FUNC) 
        && (m_defaultState->m_overrideBits & StateOverride::BLEND_FUNC))
    {
        glCheck(glBlendFunc(GL_ONE, GL_ZERO));
        m_defaultState->m_overrideBits &= ~StateOverride::BLEND_FUNC;
        m_defaultState->m_blendSrc = BlendFunc::ONE;
        m_defaultState->m_blendDest = BlendFunc::ZERO;
    }

    if (!(overrideBits & StateOverride::CULL_FACE)
        && (m_defaultState->m_overrideBits & StateOverride::CULL_FACE))
    {
        glCheck(glDisable(GL_CULL_FACE));
        m_defaultState->m_overrideBits &= ~StateOverride::CULL_FACE;
        m_defaultState->m_cullFaceEnabled = false;
    }

    if (!(overrideBits & StateOverride::CLIP_PLANE)
        && (m_defaultState->m_overrideBits & StateOverride::CLIP_PLANE))
    {
        //GLint clipCount;
        //glCheck(glGetIntegerv(GL_MAX_CLIP_DISTANCES, &clipCount));
        //for (auto i = 0; i < clipCount; ++i)
            glCheck(glDisable(GL_CLIP_DISTANCE0/* + i*/));

        m_defaultState->m_overrideBits &= ~StateOverride::CLIP_PLANE;
        m_defaultState->m_clipPlaneEnabled = false;
    }

    if (!(overrideBits & StateOverride::CULL_FACE_SIDE)
        && (m_defaultState->m_overrideBits & StateOverride::CULL_FACE_SIDE))
    {
        glCheck(glCullFace(GL_BACK));
        m_defaultState->m_overrideBits &= ~StateOverride::CULL_FACE_SIDE;
        m_defaultState->m_cullFace = CullFace::BACK;
    }

    if (!(overrideBits & StateOverride::FRONT_FACE)
        && (m_defaultState->m_overrideBits & StateOverride::FRONT_FACE))
    {
        glCheck(glFrontFace(GL_CCW));
        m_defaultState->m_overrideBits &= ~StateOverride::FRONT_FACE;
        m_defaultState->m_winding = Winding::COUNTER_CLOCKWISE;
    }

    if (!(overrideBits & StateOverride::DEPTH_TEST)
        && (m_defaultState->m_overrideBits & StateOverride::DEPTH_TEST))
    {
        glCheck(glDisable(GL_DEPTH_TEST));
        m_defaultState->m_overrideBits &= ~StateOverride::DEPTH_TEST;
        m_defaultState->m_depthTestEnabled = false;
    }

    if (!(overrideBits & StateOverride::DEPTH_WRITE)
        && (m_defaultState->m_overrideBits & StateOverride::DEPTH_WRITE))
    {
        glCheck(glDepthMask(GL_TRUE));
        m_defaultState->m_overrideBits &= ~StateOverride::DEPTH_WRITE;
        m_defaultState->m_depthWriteEnabled = true;
    }

    if (!(overrideBits & StateOverride::DEPTH_FUNC)
        && (m_defaultState->m_overrideBits & StateOverride::DEPTH_FUNC))
    {
        glCheck(glDepthFunc(GL_LESS));
        m_defaultState->m_overrideBits &= ~StateOverride::DEPTH_FUNC;
        m_defaultState->m_depthFunc = DepthFunc::LESS;
    }

    if (!(overrideBits & StateOverride::STENCIL_TEST) 
        && (m_defaultState->m_overrideBits & StateOverride::STENCIL_TEST))
    {
        glCheck(glDisable(GL_STENCIL_TEST));
        m_defaultState->m_overrideBits &= ~StateOverride::STENCIL_TEST;
        m_defaultState->m_stencilTestEnabled = false;
    }

    if (!(overrideBits & StateOverride::STENCIL_WRITE)
        && (m_defaultState->m_overrideBits & StateOverride::STENCIL_WRITE))
    {
        glCheck(glStencilMask(StateOverride::ALL_ONE));
        m_defaultState->m_overrideBits &= ~StateOverride::STENCIL_WRITE;
        m_defaultState->m_stencilWrite = StateOverride::ALL_ONE;
    }

    if (!(overrideBits & StateOverride::STENCIL_FUNC)
        && (m_defaultState->m_overrideBits & StateOverride::STENCIL_FUNC))
    {
        glCheck(glStencilFunc(GL_ALWAYS, 0, StateOverride::ALL_ONE));
        m_defaultState->m_overrideBits &= ~StateOverride::STENCIL_FUNC;
        m_defaultState->m_stencilFunc = StencilFunc::ALWAYS;
        m_defaultState->m_stencilFuncReference = 0;
        m_defaultState->m_stencilFuncMask = StateOverride::ALL_ONE;
    }

    if (!(overrideBits & StateOverride::STENCIL_OP) 
        && (m_defaultState->m_overrideBits & StateOverride::STENCIL_OP))
    {
        glCheck(glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP));
        m_defaultState->m_overrideBits &= ~StateOverride::STENCIL_OP;
        m_defaultState->m_stencilFail = StencilOp::KEEP;
        m_defaultState->m_depthFail = StencilOp::KEEP;
        m_defaultState->m_stencilDepthPass = StencilOp::KEEP;
    }
}

void RenderState::StateBlock::enableDepthWrite()
{
    assert(m_defaultState);

    if (!m_defaultState->m_depthWriteEnabled)
    {
        glCheck(glDepthMask(GL_TRUE));
        m_defaultState->m_overrideBits &= ~StateOverride::DEPTH_WRITE;
        m_defaultState->m_depthWriteEnabled = true;
    }
}