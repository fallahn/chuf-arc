/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/MaterialProperty.hpp>
#include <chuf2/Log.hpp>

#include <chuf2/glm/gtc/type_ptr.hpp>

#include <iostream>
#include <functional>

using namespace chuf;

MaterialProperty::MaterialProperty(const std::string& name)
    : m_type	(Type::None),
    m_size		(0),
    m_name		(name),
    m_hash      (0),
    m_uniform	(nullptr)
{
    std::hash<std::string> hash;
    m_hash = hash(m_name);

    clearValue();
}

MaterialProperty::~MaterialProperty()
{
    clearValue();
    //LOG("Successfully Destroyed Material Property " + m_name);
}

//public
const std::string& MaterialProperty::getName() const
{
    return m_name;
}

void MaterialProperty::setValue(float value)
{
    clearValue();
    
    m_value.floatVal = value;
    m_type = Type::Float;
}

void MaterialProperty::setValue(const float* data, UInt32 size)
{
    clearValue();
    
    m_value.floatArray = const_cast<float*>(data);
    m_size = size;
    m_type = Type::FloatArray;
}

void MaterialProperty::setValue(int value)
{
    clearValue();
    
    m_value.intValue = value;
    m_type = Type::Int;
}

void MaterialProperty::setValue(const int* data, UInt32 size)
{
    clearValue();

    m_value.intArray = const_cast<int*>(data);
    m_size = size;
    m_type = Type::IntArray;
}

void MaterialProperty::setValue(const glm::vec2& value)
{
    clearValue();

    //store the data as a dynamic array
    m_dynamicArray.reset(new float[2]{value.x, value.y});
    m_value.floatArray = &m_dynamicArray[0];
    m_size = 1u;
    m_type = Type::Vec2;
}

void MaterialProperty::setValue(const glm::vec2* data, UInt32 size)
{
    clearValue();

    m_dynamicArray.reset(new float[2 * size]);
    UInt32 j = 0u;
    for (auto i = 0u; i < size; ++i)
    {
        m_dynamicArray[j++] = data[i].x;
        m_dynamicArray[j++] = data[i].y;
    }

    m_value.floatArray = &m_dynamicArray[0];
    m_size = size;
    m_type = Type::Vec2;

}

void MaterialProperty::setValue(const glm::vec3& value)
{
    clearValue();

    m_dynamicArray.reset(new float[3]{value.x, value.y, value.z});
    m_value.floatArray = &m_dynamicArray[0];
    m_size = 1u;
    m_type = Type::Vec3;
}

void MaterialProperty::setValue(const glm::vec3* data, UInt32 size)
{
    clearValue();

    m_dynamicArray.reset(new float[3 * size]);
    UInt32 j = 0u;
    for (auto i = 0u; i < size; ++i)
    {
        m_dynamicArray[j++] = data[i].x;
        m_dynamicArray[j++] = data[i].y;
        m_dynamicArray[j++] = data[i].z;
    }

    m_value.floatArray = &m_dynamicArray[0];
    m_size = size;
    m_type = Type::Vec3;
}

void MaterialProperty::setValue(const glm::vec4& value)
{
    clearValue();

    m_dynamicArray.reset(new float[4]{value.x, value.y, value.z, value.w});
    m_value.floatArray = &m_dynamicArray[0];
    m_size = 1u;
    m_type = Type::Vec4;
}

void MaterialProperty::setValue(const glm::vec4* data, UInt32 size)
{
    clearValue();

    m_dynamicArray.reset(new float[4 * size]);
    UInt32 j = 0;
    for (auto i = 0u; i < size; ++i)
    {
        m_dynamicArray[j++] = data[i].x;
        m_dynamicArray[j++] = data[i].y;
        m_dynamicArray[j++] = data[i].z;
        m_dynamicArray[j++] = data[i].w;
    }

    m_value.floatArray = &m_dynamicArray[0];
    m_size = size;
    m_type = Type::Vec4;
}

void MaterialProperty::setValue(const Colour& colour)
{
    setValue(glm::vec4(colour.r, colour.g, colour.b, colour.a));
    m_type = Type::Colour;
}

void MaterialProperty::setValue(const Colour* data, UInt32 size)
{
    clearValue();

    m_dynamicArray.reset(new float[4 * size]);
    UInt32 j = 0;
    for (auto i = 0u; i < size; ++i)
    {
        m_dynamicArray[j++] = data[i].r;
        m_dynamicArray[j++] = data[i].g;
        m_dynamicArray[j++] = data[i].b;
        m_dynamicArray[j++] = data[i].a;
    }

    m_value.floatArray = &m_dynamicArray[0];
    m_size = size;
    m_type = Type::Colour;
}

void MaterialProperty::setValue(const glm::mat3& matrix)
{
    clearValue();

    auto p = glm::value_ptr(matrix);

    m_dynamicArray.reset(new float[9]{p[0], p[1], p[2], p[3], p[4], p[5], p[6], p[7], p[8]});

    m_value.floatArray = &m_dynamicArray[0];
    m_size = 1u;
    m_type = Type::Matrix3;
}

void MaterialProperty::setValue(const glm::mat3* matrices, UInt32 size)
{
    clearValue();

    m_dynamicArray.reset(new float[9 * size]);
    UInt32 j = 0u;
    for (auto i = 0u; i < size; ++i)
    {
        auto p = glm::value_ptr(matrices[i]);
        for (auto k = 0u; k < 9u; ++k)
        {
            m_dynamicArray[j++] = p[k];
        }
    }

    m_size = size;
    m_value.floatArray = &m_dynamicArray[0];
    m_type = Type::Matrix3;
}

void MaterialProperty::setValue(const glm::mat3x4& matrix)
{
    clearValue();

    auto p = glm::value_ptr(matrix);

    m_dynamicArray.reset(new float[12]{p[0], p[1], p[2], p[3], p[4], p[5], p[6], p[7],
                                      p[8], p[9], p[10], p[11]});

    m_value.floatArray = &m_dynamicArray[0];
    m_size = 1u;
    m_type = Type::Matrix3x4;
}

void MaterialProperty::setValue(const glm::mat3x4* matrices, UInt32 size)
{
    clearValue();

    m_dynamicArray.reset(new float[12 * size]);
    UInt32 j = 0u;
    for (auto i = 0u; i < size; ++i)
    {
        auto p = glm::value_ptr(matrices[i]);
        for (auto k = 0u; k < 12u; ++k)
        {
            m_dynamicArray[j++] = p[k];
        }
    }

    m_size = size;
    m_value.floatArray = &m_dynamicArray[0];
    m_type = Type::Matrix3x4;
}

void MaterialProperty::setValue(const glm::mat4& matrix)
{
    clearValue();

    auto p = glm::value_ptr(matrix);
    m_dynamicArray.reset(new float[16]{p[0], p[1], p[2], p[3], p[4], p[5], p[6], p[7],
                                p[8], p[9], p[10], p[11], p[12], p[13], p[14], p[15]});

    m_value.floatArray = &m_dynamicArray[0];
    m_size = 1u;
    m_type = Type::Matrix4;
}

void MaterialProperty::setValue(const glm::mat4* data, UInt32 size)
{
    clearValue();

    m_dynamicArray.reset(new float[16 * size]);
    UInt32 j = 0u;
    for (auto i = 0u; i < size; ++i)
    {
        auto p = glm::value_ptr(data[i]);
        for (auto k = 0u; k < 16u; ++k)
        {
            m_dynamicArray[j++] = p[k];
        }
    }

    m_size = size;
    m_value.floatArray = &m_dynamicArray[0];
    m_type = Type::Matrix4;
}

void MaterialProperty::setValue(const Texture::Sampler::Ptr& sampler)
{
    assert(sampler);
    if (m_textureSampler == sampler) return;
    clearValue();

    m_textureSampler = sampler;
    m_type = Type::Sampler;
}

Texture::Sampler::Ptr MaterialProperty::setValue(const std::string& path, bool createMipmaps)
{
    clearValue();

    m_textureSampler = Texture::Sampler::create(path, createMipmaps);
    if (m_textureSampler) m_type = Type::Sampler;
    return m_textureSampler;
}

Texture::Sampler::Ptr MaterialProperty::getSampler() const
{
    if (m_type == Type::Sampler) return m_textureSampler;
    return nullptr;
}

void MaterialProperty::setSampler(const Texture::Sampler::Ptr& sampler)
{
    setValue(sampler);
}

Texture::Sampler::Ptr MaterialProperty::setSampler(const std::string& path, bool createMipmaps)
{
    return setValue(path, createMipmaps);
}

void MaterialProperty::setValue(std::vector<Texture::Sampler::Ptr>& samplerArray)
{
    clearValue();
    m_samplerArray = std::move(samplerArray);
    m_type = Type::SamplerArray;
}

//private
void MaterialProperty::clearValue()
{
    m_size = 1;
    m_type = Type::None;
    memset(&m_value, 0, sizeof(m_value));
    m_functionBinding.reset();
    m_textureSampler.reset();
    m_dynamicArray.reset();
    m_samplerArray.clear();
}

void MaterialProperty::bind(Shader* shader)
{
    assert(shader);

    if (!m_uniform || m_uniform->getShader() != shader)
    {
        m_uniform = shader->getUniform(m_name, m_hash);
        if (!m_uniform)
        {
            //Logger::Log("Uniform " + m_name + " not found in shader " + shader->getName(), Logger::Type::Error);
            return;		
        }
    }

    switch (m_type)
    {
    default:
        Logger::Log("Uniform " + m_name + " not set in shader " + shader->getName(), Logger::Type::Error);
        return;
    case Type::Float:
        shader->setUniform(m_uniform, m_value.floatVal);
        break;
    case Type::FloatArray:
        shader->setUniform(m_uniform, m_value.floatArray, m_size);
        break;
    case Type::Int:
        shader->setUniform(m_uniform, m_value.intValue);
        break;
    case Type::IntArray:
        shader->setUniform(m_uniform, m_value.intArray, m_size);
        break;
    case Type::Vec2:
        shader->setUniform(m_uniform, m_value.floatArray, m_size, 2u);
        break;
    case Type::Vec3:
        shader->setUniform(m_uniform, m_value.floatArray, m_size, 3u);
        break;
    case Type::Vec4:
        shader->setUniform(m_uniform, m_value.floatArray, m_size, 4u);
        break;
    case Type::Colour:
        shader->setUniform(m_uniform, m_value.floatArray, m_size, 4u);
        break;
    case Type::Matrix3:
        shader->setUniform(m_uniform, m_value.floatArray, m_size, 9u);
        break;
    case Type::Matrix3x4:
        shader->setUniform(m_uniform, m_value.floatArray, m_size, 12u);
        break;
    case Type::Matrix4:
        shader->setUniform(m_uniform, m_value.floatArray, m_size, 16u);
        break;
    case Type::Sampler:
        shader->setUniform(m_uniform, m_textureSampler);
        break;
    case Type::SamplerArray:
        shader->setUniform(m_uniform, m_samplerArray);
        break;
    case Type::Binding:
        if (m_functionBinding)
        {
            m_functionBinding->setValue(shader);
        }
        break;
    }
}