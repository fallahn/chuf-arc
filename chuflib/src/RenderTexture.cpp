/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#include <chuf2/RenderTexture.hpp>
#include <chuf2/Texture.hpp>
#include <chuf2/Image.hpp>
#include <chuf2/Log.hpp>
#include <chuf2/Cache.hpp>

#include <cassert>

using namespace chuf;

namespace
{
    Cache<RenderTexture> textureCache;
}

RenderTexture::RenderTexture(const std::string& name, const RenderTexture::Key& key)
    : m_name(name), m_texture(nullptr){}

//public
RenderTexture* RenderTexture::create(const std::string& name, UInt16 width, UInt16 height)
{
    assert(!name.empty());
    Image::Ptr i = Image::create(width, height, Image::Format::RGBA);
    i->flipVertically(); //makes no odds to rendered output, but sets a flag
    //which correctly invertes the coordinates should this target be used in a sprite batch
    
    auto texture = Texture::create(i, false);
    
    if (!texture)
    {
        Logger::Log("Failed to create texture for render texture " + name, Logger::Type::Error);
        return nullptr;
    }

    return create(name, texture);
}

RenderTexture* RenderTexture::create(const std::string& name, Texture* t)
{
    assert(!name.empty());
    auto exists = textureCache.find(name);
    if (exists) return exists;

    assert(t);

    auto rt = std::make_unique<RenderTexture>(name, RenderTexture::Key());
    rt->m_texture = t;

    return textureCache.insert(name, rt);
}

RenderTexture* RenderTexture::getRenderTexture(const std::string& name)
{
    assert(!name.empty());
    return textureCache.find(name);
}

const std::string& RenderTexture::getName() const
{
    return m_name;
}

Texture* RenderTexture::getTexture() const
{
    return m_texture;
}

UInt16 RenderTexture::getWidth() const
{
    return m_texture->getWidth();
}

UInt16 RenderTexture::getHeight() const
{
    return m_texture->getHeight();
}