/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//node within the quad tree containing quad tree components

#ifndef CHUF_QUADTREE_NODE_HPP_
#define CHUF_QUADTREE_NODE_HPP_

#include <chuf2/QuadTreeComponent.hpp>
#include <chuf2/DataTypes.hpp>

#include <array>
#include <memory>
#include <unordered_set>

namespace chuf
{
    class QuadTree;
    class QuadTreeNode final
    {
    public:
        using Ptr = std::unique_ptr<QuadTreeNode>;
        using Set = std::unordered_set<QuadTreeComponent*>;

        QuadTreeNode();
        QuadTreeNode(const FloatRect& area, Int32 level, QuadTreeNode* parent, QuadTree* quadTree);
        QuadTreeNode(const QuadTreeNode&) = delete;
        QuadTreeNode& operator = (const QuadTreeNode&) = delete;
        ~QuadTreeNode() = default;

        void create(const FloatRect& area, Int32 level, QuadTreeNode* parent, QuadTree* quadTree);
        QuadTree* getTree() const;
        void add(QuadTreeComponent*);
        const FloatRect& getArea() const;

        Int32 getNumComponentsBelow() const;

        void update(QuadTreeComponent*);
        void remove(QuadTreeComponent*);

        const Set& getComponents() const;
        bool hasChildren() const;
        const std::array<Ptr, 4u>& getChildren() const;


    private:
        QuadTreeNode* m_parent;
        QuadTree* m_quadTree;

        bool m_hasChildren;
        std::array<Ptr, 4u> m_children;
        Set m_components;

        FloatRect m_area;
        Int32 m_level;
        Int32 m_numComponentsBelow;

        void getSubComponents();
        glm::uvec2 getPossiblePosition(QuadTreeComponent*);
        void addToThis(QuadTreeComponent*);
        bool addToChildren(QuadTreeComponent*); //true if successful
        void destroyChildren();

        void split();
        void join();
        void clearDestroyed();
    };
}
#endif //CHUF_QUADTREE_NODE_HPP_