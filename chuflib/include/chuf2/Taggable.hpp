/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//allows adding tags to classes which inherit from this, such as nodes or materials
//tag lookups are not particularly fast so not recommended for render critial application

#ifndef CHUF_TAGGABLE_HPP_
#define CHUF_TAGGABLE_HPP_

#include <chuf2/glm.hpp>
#include <chuf2/DataTypes.hpp>

#include <string>
#include <unordered_map>
#include <vector>

namespace chuf
{
    class Taggable
    {
    public:
        virtual ~Taggable() = default;

        void addTag(const std::string& tag, const std::string& value);
        void removeTag(const std::string& tag);
        bool hasTag(const std::string& tag) const;

        const std::string& getTag(const std::string& tag);
        glm::vec2 getTagVec2(const std::string& tag) const;
        glm::vec3 getTagVec3(const std::string& tag) const;
        float getTagFloat(const std::string& tag) const;
        Int32 getTagInt(const std::string& tag) const;

    private:
        std::unordered_map<std::string, std::string> m_tags;

        std::vector<float> toFloatArray(const std::string& value) const;
    };
}

#endif //CHUF_TAGGABLE_HPP_
