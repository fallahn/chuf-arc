/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//materials contain one or more techniques which use one or more passes
//to render a mesh. a technique may use multiple passes at any one time
//but a material only ever uses one technique at a time. multiple
//techniques can be supplied to achieve the same or similar result when
//rendered, by using their own set of passes which are optimised for
//a given task, for example when current hardware may or may not support
//a particular feature request by an alternate technique. One use case
//may be to define techniques for shader model 2.0, shader model 3.0
//and GLES, and set the material's technique accordingly to the current
//hardware

#ifndef CHUF_TECHNIQUE_HPP_
#define CHUF_TECHNIQUE_HPP_

#include <chuf2/RenderState.hpp>
#include <chuf2/Pass.hpp>

namespace chuf
{
    class Pass;
    class Material;
    class Technique final : public RenderState
    {
        friend class Material;
    public:
        using Ptr = std::unique_ptr<Technique>;

        //return this technique's name
        const std::string& getName() const;
        //return the number of render passes
        //in this technique
        UInt32 getPassCount() const;
        //return a pointer to a pass requested
        //by name, or nullptr if pass does not exist
        Pass* getPass(const std::string& name) const;
        //return a pointer to the pass at a given index
        //or nullptr if pass does not exist
        Pass* getPass(UInt32 index);
        //binds the node associate with this technique
        //for correct auto binding of node properties
        void bindNode(Node* node) override;

        Technique(const std::string& name, const std::shared_ptr<Material>& m);
        ~Technique() = default;

    private:

        std::string m_name;
        std::weak_ptr<Material> m_material;
        std::vector<std::unique_ptr<Pass>> m_passes;
    };
}

#endif //CHUF_TECHNIQUE_HPP_