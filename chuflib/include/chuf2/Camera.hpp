/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//represents a virtual camera used in the scene graph
//to calculate any view and projection matrices used when drawing

#ifndef CHUF_CAMERA_HPP_
#define CHUF_CAMERA_HPP_

#include <chuf2/NonCopyable.hpp>
#include <chuf2/ClassKey.hpp>
#include <chuf2/AlignedAllocation.hpp>

#include <chuf2/Drawable.hpp>
#include <chuf2/Frustum.hpp>
#include <chuf2/Rectangle.hpp>
#include <chuf2/BoundingBox.hpp>
#include <chuf2/Node.hpp>

#include <chuf2/glm/glm.hpp>

namespace chuf
{
    class Model;
    class Camera final : public Drawable, private NonCopyable, private Locked, public Aligned<16>
    {
        friend class Node;
    public:
        using Ptr = std::unique_ptr<Camera>;

        enum Type
        {
            Orthographic = 1,
            Perspective
        };

        class Listener
        {
        public:
            virtual ~Listener(){}
            virtual void cameraChanged(Camera* camera) = 0;
        };

        Camera(float x, float y, float nearPlane, float farPlane, float aspectRatio, const Key& key);
        Camera(float fov, float nearPlane, float farPlane, float aspectRatio, const Key& key);
        ~Camera() = default;

        //cameras are heap allocated via a unique_ptr. attach a camera returned by one of the
        //create functions to a node to keep it alive.

        //returns a unique pointer to a camera using an orthographic projection matrix
        static Ptr createOrthographic(float x, float y, float nearPlane, float farPlane, float aspectRatio);
        //returns a unique_ptr using a perspective projection matrix
        static Ptr createPerspective(float fov, float nearPlane, float farPlane, float aspectRatio);

        //returns whether the camera is using an orthographic or perspective projection matrix
        Type getType() const;
        //returns the field of view for perspective cameras, else 0
        float getFov() const;
        //sets the field of view for perspective cameras. reducing the fov effectively zooms the camera
        void setFov(float fov);
        //returns the width and height of an orthographic camera's projection, else 0,0
        const glm::vec2& getOrthoSize() const;
        //sets the orthographic projection size - reducing the size effectively zooms the camera
        void setOrthoSize(const glm::vec2& size);
        //returns the camera's current aspect ratio
        float getAspectRatio() const;
        //sets the camera's current aspect ratio
        void setAspectRatio(float ratio);
        //gets the camera's frustum near plane
        float getNearPlane() const;
        //sets the camera frustum near plane
        void setNearPlane(float near);
        //gets the camera's frustum far plane
        float getFarPlane() const;
        //sets the camera's frustum far plane
        void setFarPlane(float far);
        //sets the reflection plane
        void setReflectionPlane(const glm::vec4& plane);
        //gets the current reflection plane
        const glm::vec4& getReflectionPlane();
        //inverts the cameras reflection plave
        void invertReflectionPlane();

        //returns the camera's parent node, or nullptr if it is not currently attached
        Node* getNode() const;
        //returns the camera's view matrix based on its parent node transform
        const glm::mat4& getViewMatrix() const;
        //returns the inverse view matrix of the camera based on its parent node transform
        const glm::mat4& getInverseViewMatrix() const;
        //returns the camera's projection matrix. the type can be queried with getType()
        const glm::mat4& getProjectionMatrix() const;
        //returns the camera's projection matrix multiplied by its view matrix
        const glm::mat4& getViewProjectionMatrix() const;
        //returns the camera's projection matrix multiplied by its inverse view matrix
        const glm::mat4& getInverseViewProjectionMatrix() const;
        //returns the camera's view matrix reflected about camera's current plane
        const glm::mat4& getReflectionViewMatrix() const;
        //returns the camera's projection maultiplied by the camera's reflection view matrix
        const glm::mat4& getReflectionViewProjectionMatrix() const;
        //returns the reflection matrix based on the camera's current reflection plane
        const glm::mat4& getReflectionMatrix() const;

        //returns the camera's frustum
        const Frustum& getViewFrustum() const;
        //get the frustum of the reflected view
        const Frustum& getReflectionViewFrustum() const;
        //returns a two dimensional AABB around the view frustum
        //in world space coords
        const FloatRect& getFrustumBounds2D() const;
        //returns a bounding box around the camera frustum in world space
        const BoundingBox& getFrustumBounds3D() const; //TODO get reflected bounds


        //projects a world point onto the camera's view and returns it in screen position
        glm::vec3 project(const FloatRect& viewport, const glm::vec3& worldPos) const;

        //allows adding / removing of custom listener callbacks
        void addListener(Listener& listener);
        void removeListener(Listener& listener);

        //sets this camera's active flag
        void setActive(bool);
        //returns true if this is scene's active camera
        bool active() const;

        //takes a path to a skybox material to create a skybox for this camera
        void setSkybox(const std::string& path);

        const BoundingBox& getBoundingBox() const override;
        UInt32 draw(UInt32 flags, bool) const override;

    private:
        enum DirtyBits
        {
            VIEW                = (1 << 0),
            PROJECTION          = (1 << 1),
            VIEW_PROJECTION     = (1 << 3),
            INV_VIEW            = (1 << 4),
            INV_VIEW_PROJECTION = (1 << 5),
            FRUSTUM             = (1 << 6),
            REFL_FRUSTUM        = (1 << 7),
            REFLECTION          = (1 << 8),
            BOUNDS2D            = (1 << 9),
            BOUNDS3D            = (1 << 10),
            CREATE_SKYBOX       = (1 << 11),
            ALL                 = (VIEW | PROJECTION | VIEW_PROJECTION | INV_VIEW | INV_VIEW_PROJECTION | FRUSTUM | REFL_FRUSTUM | REFLECTION | BOUNDS2D | BOUNDS3D | CREATE_SKYBOX)
        };

        Type m_type;
        float m_fov;
        glm::vec2 m_orthoSize;
        float m_aspectRatio;
        float m_nearPlane;
        float m_farPlane;
        bool m_active;
        mutable UInt32 m_dirtyBits;

        Model* m_skyboxModel;
        std::unique_ptr<Node> m_skyboxNode;

        Plane m_reflectionPlane;

        mutable glm::mat4 m_viewMat;
        mutable glm::mat4 m_projectionMat;
        mutable glm::mat4 m_viewProjectionMat;
        mutable glm::mat4 m_inverseViewMat;
        mutable glm::mat4 m_inverseViewProjectionMat;
        mutable glm::mat4 m_reflectionViewMat;
        mutable glm::mat4 m_reflectionViewProjectionMat;
        mutable glm::mat4 m_reflectionMatrix;
        mutable Frustum m_frustum;
        mutable Frustum m_reflectionFrustum;
        mutable FloatRect m_frustumBounds2D;
        mutable BoundingBox m_frustumBounds3D;
        std::list<Listener*> m_listeners;

        void setNode(Node* node) override;
        void transformChanged(Transform& t) override;
        void cameraChanged();
        void updateReflectionMatrix() const;
        void updateBounds2D() const;
        void updateBounds3D() const;
        void createSkybox();
    };
}

#endif //CHUF_CAMERA_HPP_
