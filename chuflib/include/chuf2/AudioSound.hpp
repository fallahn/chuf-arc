/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//represents a sound clip played from an audio buffer

#ifndef CHUF_AUDIO_SOUND_HPP_
#define CHUF_AUDIO_SOUND_HPP_

#include <chuf2/AudioSource.hpp>
#include <cstdlib>

namespace chuf
{
    class AudioBuffer;
    class AudioSound final : public AudioSource
    {
    public:
        AudioSound();
        explicit AudioSound(const AudioBuffer& b);
        AudioSound(const AudioSound& other);
        ~AudioSound();

        AudioSound& operator = (const AudioSound& rhv);

        //play the sound
        void play();
        //pause the sound
        void pause();
        //stop the sound
        void stop();

        //set whether the sound should play looped or not
        void setLooped(bool looped);
        //returns whether or not the sound is set to play looped
        bool looped() const;
        //sets the sounds audio buffer
        void setBuffer(const AudioBuffer& buffer);
        //returns a pointer to the sounds current buffer
        const AudioBuffer* getBuffer() const;
        //offsets the playing point by given number of seconds
        void setPlayOffset(float offset);
        //returns the play offset in seconds
        float getPlayOffset() const;
        //returns the current playback state of the sound
        AudioSource::State getState() const;


        void resetBuffer();
    private:
        const AudioBuffer* m_audioBuffer;
    };
}


#endif //CHUF_AUDIO_SOUND_HPP_