/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//a lightweight text class for displaying text from a heavier-weight font
//text objects only keep a reference to the font passed to them
//so the font must be kept alive somewhere

#ifndef CHUF_TEXT_HPP_
#define CHUF_TEXT_HPP_

#include <chuf2/NonCopyable.hpp>
#include <chuf2/Colour.hpp>
#include <chuf2/Transform.hpp>
#include <chuf2/AlignedAllocation.hpp>
#include <chuf2/Rectangle.hpp>
#include <chuf2/SpriteBatch.hpp>

#include <chuf2/glm/glm.hpp>

#include <string>
#include <memory>

namespace chuf
{
    class Font;
    class Sprite;
    class Text final : private NonCopyable, public Aligned<16>
    {
    public:
        using Ptr = std::unique_ptr<Text>;

        //construct a text object from a given font, with optional text
        Text(Font* font, const std::string& text = "");
        ~Text() = default;

        //sets the text string
        void setString(const std::string& str);
        //returns the text's string
        const std::string& getString() const;

        //TODO fancy formatting functions
        //setFont()
        //setSize()
        //setItalic();

        //sets the spacing between characters
        void setHorizontalSpacing(float spacing);
        //returns the current spacing between characters
        float getHorizontalSpacing() const;
        //sets vertical spacing between lines
        void setVerticalSpacing(float spacing);
        //returns the veritcal spacing between lines
        float getVerticalSpacing() const;

        //set the position of the text
        void setPosition(const glm::vec2& position);
        //get the text position
        glm::vec2 getPosition() const;
        //move the text by given amount
        void move(const glm::vec2& amount);

        //set the text rotation in degrees
        void setRotation(float angle);
        //get the texts rotation
        float getRotation() const;
        //rotate the text by given amount in degrees
        void rotate(float angle);

        //set the text scale
        void setScale(float scale);
        void setScale(const glm::vec2& scale);
        //get the text scale
        glm::vec2 getScale() const;
        //scale text by amount
        void scale(float amount);
        void scale(const glm::vec2& amount);

        //sets the text's transformation origin
        void setOrigin(const glm::vec2& origin);
        //returns the current origin of the text's transform
        const glm::vec2& getOrigin() const;

        //returns the local bounds of the text
        const FloatRect& getLocalBounds();
        //returns the global AABB of the text
        const FloatRect& getGlobalBounds();

        //set the colour of the text
        void setColour(const Colour& colour);
        //return this text's colour
        const Colour& getColour()const;

        //returns a const ref to this text's font
        const Font* getFont() const;

        void draw();

    private:

        enum DirtyBits
        {
            STRING    = (1 << 0),
            SPACING   = (1 << 1),
            COLOUR    = (1 << 2),
            TRANSFORM = (1 << 3),
            ORIGIN    = (1 << 4),
            OFFSET    = (1 << 5),

            ALL = STRING | SPACING | COLOUR | TRANSFORM | ORIGIN | OFFSET
        };

        Font* m_font;
        std::string m_text;
        std::vector<std::shared_ptr<Sprite>> m_sprites;
        std::unique_ptr<SpriteBatch> m_spriteBatch;
        float m_rotation;
        glm::vec2 m_origin;
        float m_hSpacing;
        float m_vSpacing;
        float m_verticalOffset;
        chuf::Colour m_colour;

        UInt8 m_dirtyBits;

        chuf::Transform m_transform;

        void updateText();
    };
}
#endif //CHUF_TEXT_HPP_