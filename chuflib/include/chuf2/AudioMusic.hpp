/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//plays a sound or music file by streaming it from storage rather than loading entirely into memory

#ifndef CHUF_AUDIO_MUSIC_HPP_
#define CHUF_AUDIO_MUSIC_HPP_

#include <chuf2/AudioStream.hpp>

#include <string>
#include <vector>
#include <mutex>
#include <memory>

namespace chuf
{
    class AudioFile;

    class AudioMusic final : public AudioStream
    {
    public:
        AudioMusic();
        ~AudioMusic();

        //attempts to open a music file from given path
        bool open(const std::string& path);
        //returns duration of open file or 0 if no file loaded
        float getDuration() const;

    private:
        std::unique_ptr<AudioFile> m_file;
        float m_duration;
        std::vector<Int16> m_sampleData;
        std::mutex m_mutex;

        bool onGetData(Chunk& data) override;
        void onSeek(float offset) override;
        void init();
    };
}

#endif //CHUF_AUDIO_MUSIC_HPP_