/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//represents the root of a scene to which nodes are attached

#ifndef CHUF_SCENE_HPP_
#define CHUF_SCENE_HPP_

#include <chuf2/ClassKey.hpp>
#include <chuf2/NonCopyable.hpp>
#include <chuf2/Node.hpp>
#include <chuf2/Colour.hpp>
#include <chuf2/Camera.hpp>
#include <chuf2/SceneRenderer.hpp>

#include <memory>
#include <string>
#include <functional>

namespace chuf
{
    class Camera;
    class FrameBuffer;
    class Scene final : private NonCopyable, private Locked
    {
    public:
        enum class FBO
        {
            Reflection,
            Refraction,
            Count
        };

        using Ptr = std::unique_ptr<Scene>;
        using Callback = std::function<bool(Node&, void* userData)>;

        //creates a new scene with the given name
        static Scene::Ptr create(const std::string& name);
        Scene(const std::string& name, const Key& key);
        ~Scene();

        //sets the scene's name
        const std::string& getName() const;
        //returns the scene's name
        void setName(const std::string& name);

        //finds a node with the given name and returns a pointer to it if
        //found, else returns nullptr. Searches all child nodes recursively
        //by default
        Node* findNode(const std::string& name, bool recursive = true);
        //finds all nodes with the given name and inserts a pointer to them into the given
        //vector. Searches child nodes recursively by default, and returns the number of
        //matches found
        UInt32 findNodes(const std::string& name, std::vector<Node*>& dest, bool recursive = true);

        //adds a new node with the given name and returns a reference to it
        Node& addNode(const std::string& name);
        //adds a node to the scene and takes ownership of it. all added nodes
        //are also root nodes in their hierarchy
        void addNode(Node::Ptr& node);
        //removes the given node from the scene and returns its unique_ptr
        //in case ownership needs to be transferred, else removed node is destroyed
        Node::Ptr removeNode(Node* node);
        //removes and destroys all nodes in the scene
        void removeAllNodes();
        //returns the number of nodes in this scene
        UInt32 getNodeCount() const;
        //returns a pointer to the first child node
        //or null pointer if the scene has no child nodes.
        Node* getFirstNode() const;

        //returns the scene's ambient colour, useful for binding to
        //any materials used in the scene
        const Colour& getAmbientColour() const;
        //sets the scene's ambient colour
        void setAmbientColour(const Colour& colour);

        //returns a pointer to the sccene's active camera
        //or nullptr if no nodes in the scene have a camera
        //attached, or setActiveCamera() has been called with
        //a nulltptr parameter
        Camera* getActiveCamera() const;
        //sets the scene's active camera
        void setActiveCamera(Camera* camera);

        //traverses each node in the scene, passing it to the provided function
        //if the function returns false the current node hierarchy is stopped
        //and the traverse function moves onto the next sibling root node
        void traverse(Callback func, void* userData = nullptr);

        //returns a pointer to requested frame buffer
        FrameBuffer* getFrameBuffer(FBO id) const;

        //returns a reference to this scene's renderer
        SceneRenderer& getRenderer();

        //updates all the nodes in a scene
        void update(float);

        //enables scene reflections by creating respective frame buffers
        void enableReflections();

    private:

        void traverseNode(Node& node, Callback func, void* userData);

        std::string m_name;
        std::vector<Node::Ptr> m_nodes;
        Colour m_ambientColour;
        Camera* m_activeCamera;

        std::vector<FrameBuffer*> m_frameBuffers;
        SceneRenderer m_renderer;
    };
}

#endif //CHUF_SCENE_HPP_
