/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//acts as a wrapper around a bullet physics world and factory for physics components

#ifndef CHUF_PHYS_WORLD_HPP_
#define CHUF_PHYS_WORLD_HPP_

#include <chuf2/NonCopyable.hpp>
#include <chuf2/glm.hpp>
#include <chuf2/DataTypes.hpp>
#include <chuf2/physics/PhysCollisionComponent.hpp>
#include <chuf2/Model.hpp>

#include <btBulletCollisionCommon.h>
#include <btBulletDynamicsCommon.h>
#include <BulletCollision/CollisionDispatch/btGhostObject.h>
#include <BulletDynamics/ConstraintSolver/btSequentialImpulseConstraintSolver.h>

#include <memory>
#include <vector>
#include <map>

namespace chuf
{
    class Ray;
    class Node;
    class HeightData;
    class Mesh;

    class PhysicsCollisionShape;
    class PhysicsWorld final : private NonCopyable
    {
        friend class PhysicsCollisionComponent;
    public:
        //observers yo
        class Listener
        {
        public:
            enum class Event
            {
                Activated,
                Deactivated
            };

            virtual void statusEvent(Event) = 0;

        protected:
            virtual ~Listener() = default;
        };

        //result of ray / sweep test
        struct Manifold final
        {
            PhysicsCollisionComponent* component;
            //values are in world space
            glm::vec3 point;
            glm::vec3 normal;
            float distance; //normalised value from collision point
            Manifold() : component(nullptr), distance(0.f){}
        };

        //this can be overriden for custom sweep / ray test filtering
        //the default simply returns the closest intersection
        class ManifoldFilter
        {
        public:
            ManifoldFilter() = default;
            virtual ~ManifoldFilter() = default;

            //called before ray test to determine if given target should be filtered
            virtual bool filter(PhysicsCollisionComponent*);

            //called when ray / sweep test registers collision
            virtual bool hit(const Manifold&);
        };

        PhysicsWorld();
        ~PhysicsWorld() = default;

        //called on startup
        //void start();
        //updates the status of the world
        void update(float dt);
        //called when world is removed
        //void finish();

        //add an observer
        void addListener(Listener*);
        //remove an observer
        void removeListener(Listener*);

        //get the current grative value
        const glm::vec3& getGravity() const;
        //set the world's gravity
        void setGravity(const glm::vec3&);

        //performs a ray test on the world and fills the given
        //manifold with the result based on any given manifold filter
        bool rayTest(const Ray& ray, float distance, Manifold* manifold = nullptr, ManifoldFilter* filter = nullptr);

        //performs a sweep test starting at the world position of the given 
        //collision object and fills the manifold with the result if it is provided
        bool sweepTest(PhysicsCollisionComponent* object, const glm::vec3& end, Manifold* result = nullptr, ManifoldFilter* filter = nullptr);

        //creates a box shape collision object component, sized around centre
        std::unique_ptr<PhysicsCollisionShape> createBox(const glm::vec3& size, const glm::vec3& scale);
        //creates a collision capsule component
        std::unique_ptr<PhysicsCollisionShape> createCapsule(float radius, float height, const glm::vec3& scale);
        //creates a spherical collision object component
        std::unique_ptr<PhysicsCollisionShape> createSphere(float radius, const glm::vec3& scale);
        //creates a collision component from a given height data object
        std::unique_ptr<PhysicsCollisionShape> createHeightData(Node*, HeightData*, const glm::vec3& centreofMassOffset);
        //creates a collision component from given mesh
        std::unique_ptr<PhysicsCollisionShape> createMesh(Mesh*, const glm::vec3& scale);

        //draws debug output using bullet's debug drawer. Requires view projection matrix
        //of the scene camera passed to it.
        void drawDebug(const glm::mat4& viewProjection);

    private:
        //wraps bullet collision callback
        class CollisionCallback : public btCollisionWorld::ContactResultCallback
        {
        public:
            explicit CollisionCallback(PhysicsWorld* pw) : m_physWorld(pw){}

        protected:
            btScalar addSingleResult(btManifoldPoint& mp, const btCollisionObjectWrapper* a, Int32 partIdA, Int32 indexA,
                                    const btCollisionObjectWrapper* b, Int32 partIdB, Int32 indexB) override;
        private:
            PhysicsWorld* m_physWorld;
        }m_collisionCallback;

        //represents the status of a colliding pair of objects
        struct CollisionData final
        {
            CollisionData() : status(0){}

            Int32 status;
            std::vector<PhysicsCollisionComponent::Listener*> listeners;
        };


        bool m_updating;
        std::unique_ptr<btDefaultCollisionConfiguration> m_collisionConfig;
        std::unique_ptr<btCollisionDispatcher> m_collisionDespatcher;
        std::unique_ptr<btBroadphaseInterface> m_overlappingPairCache;
        std::unique_ptr<btSequentialImpulseConstraintSolver> m_constraintSolver;
        std::unique_ptr<btDynamicsWorld> m_btPhysWorld;
        std::unique_ptr<btGhostPairCallback> m_ghostpairCallback;

        std::vector<PhysicsCollisionShape*> m_shapes;
        Listener::Event m_status;
        std::vector<Listener*> m_listeners;
        glm::vec3 m_gravity;
        std::map<PhysicsCollisionComponent::CollisionPair, CollisionData> m_collisionStatus;

        void addListener(PhysicsCollisionComponent::Listener*, PhysicsCollisionComponent* a, PhysicsCollisionComponent* b);
        void removeListener(PhysicsCollisionComponent::Listener*, PhysicsCollisionComponent* a, PhysicsCollisionComponent* b);

        void addCollisionComponent(PhysicsCollisionComponent*);
        void removeCollisionComponent(PhysicsCollisionComponent*, bool removeListeners);

        PhysicsCollisionComponent* getcollisionComponent(const btCollisionObject*) const;

        void destroyShape(PhysicsCollisionShape*);


        //implements bullet's debug drawer
        class DebugDrawer final : public btIDebugDraw
        {
        public:
            DebugDrawer();
            ~DebugDrawer() = default;
            DebugDrawer(const DebugDrawer&) = delete;
            DebugDrawer& operator = (const DebugDrawer&) = delete;

            void draw(const glm::mat4&); //TODO mesh batching probably not optimal, we could do with some view culling

            void drawLine(const btVector3& from, const btVector3& to, const btVector3& fromColour, const btVector3& toColour) override;
            void drawLine(const btVector3& from, const btVector3& to, const btVector3& colour) override;
            void drawContactPoint(const btVector3& pointOnB, const btVector3& normalOnB, btScalar distance, Int32 lifeTime, const btVector3& colour) override;
            void reportErrorWarning(const char* warningString) override;
            void draw3dText(const btVector3& location, const char* textString) override;
            void setDebugMode(Int32 mode) override;
            Int32 getDebugMode() const override;
        private:

            struct Vertex
            {
                Vertex(float x, float y, float z, float r, float g, float b, float a = 1.f)
                    : x(x), y(y), z(z), r(r), g(g), b(b), a(a){}
                
                float x, y, z;
                float r, g, b, a;
            };

            Int32 m_mode;
            Int32 m_lineCount;
            std::shared_ptr<Mesh> m_mesh;
            Model::Ptr m_model;
            std::vector<Vertex> m_vertexData;
            bool m_needsUpdate;

        }m_debugDrawer;

        //implement ray test call back from bullet's class
        class RayTestCallback final : public btCollisionWorld::ClosestRayResultCallback
        {
        public:
            RayTestCallback(const btVector3& rayFromWorld, const btVector3& rayToWorld, ManifoldFilter* filter);

            bool needsCollision(btBroadphaseProxy* proxy) const override;
            btScalar addSingleResult(btCollisionWorld::LocalRayResult& result, bool normalInWorldSpace) override;

        private:
            ManifoldFilter* m_filter;
            Manifold m_manifold;
        };
    };
}

#endif //CHUF_PHYS_WORLD_HPP_