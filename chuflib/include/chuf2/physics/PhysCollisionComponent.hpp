/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#ifndef CHUF_PHYS_COLLISION_COMPONENT_HPP_
#define CHUF_PHYS_COLLISION_COMPONENT_HPP_

#include <chuf2/physics/PhysCollisionShape.hpp>
#include <chuf2/Component.hpp>

#include <memory>

namespace chuf
{
    class Node;
    class PhysicsWorld;
    namespace PhysicsCollisionGroup
    {
        enum
        {
            Default = btBroadphaseProxy::DefaultFilter
        };
    }
    namespace PhysicsCollisionMask
    {
        enum
        {
            Default = btBroadphaseProxy::AllFilter
        };
    }
    class PhysicsCollisionComponent : public Component
    {
        friend class PhysicsWorld;
    public:
        enum class Type
        {
            Body,
            Character,
            Ghost,
            None
        };

        class CollisionPair final
        {
        public:
            CollisionPair(PhysicsCollisionComponent*, PhysicsCollisionComponent*);

            bool operator < (const CollisionPair&) const;

            PhysicsCollisionComponent* objA;
            PhysicsCollisionComponent* objB;
        };

        class Listener
        {
        public:
            enum class Type
            {
                Colliding,
                NotColliding
            };

            Listener() = default;
            virtual ~Listener() = default;
            Listener(const Listener&) = default;
            Listener& operator = (const Listener&) = default;

            //called when two objects in the world collide. Do not attempt to try and disable
            //or remove physics in this callback, rather raise a message or defer deletion to the
            //beginning of the next frame
            virtual void collisionEvent(Type, const CollisionPair&, 
                                        const glm::vec3& intersectPointA = glm::vec3(),
                                        const glm::vec3& intersectPointB = glm::vec3()) = 0;
        };

        //no point locking ctor on an ABC
        PhysicsCollisionComponent(Int32 group = PhysicsCollisionGroup::Default, Int32 mask = PhysicsCollisionMask::Default);
        virtual ~PhysicsCollisionComponent();
        virtual Type getType() const = 0;

        PhysicsCollisionShape* getCollisionShape() const;
        PhysicsCollisionShape::Type getShapeType() const;
        bool kinematicBody() const;
        bool staticBody() const;
        bool dynamicBody() const;
        bool enabled() const;
        void setEnabled(bool);

        //adds a listener to receive collision events from this component
        void addListener(Listener*, PhysicsCollisionComponent* = nullptr);
        //removes a listener currently receiving collision events from this component
        void removeListener(Listener*, PhysicsCollisionComponent* = nullptr);

        //returns true if this component intersects given object
        bool intersects(PhysicsCollisionComponent*) const;

    protected:

        virtual btCollisionObject* getCollisionObject() const = 0;
        void setCollisionShape(PhysicsCollisionShape*);

    private:
        bool m_enabled;
        PhysicsCollisionShape* m_collisionShape;
        Int32 m_collisionGroup;
        Int32 m_collisionMask;
        PhysicsWorld* m_physWorld;

        //syncs chuf transforms with those of bullet
        class MotionState : public btMotionState
        {
        public:
            MotionState(Node*, PhysicsCollisionComponent*, const glm::vec3* centreOfMassOffset = nullptr);
            virtual ~MotionState() = default;

            void getWorldTransform(btTransform&) const override;
            void setWorldTransform(const btTransform&) override;

            void updateTransformFromNode() const;
            void setCentreOfMassOffset(const glm::vec3&);

        private:
            Node* m_node;
            PhysicsCollisionComponent* m_physComponent;
            btTransform m_comOffset;
            mutable btTransform m_worldTransform;
        };
        std::unique_ptr<MotionState> m_motionState;
    };
}

#endif //CHUF_PHYS_COLLISION_COMPONENT_HPP_