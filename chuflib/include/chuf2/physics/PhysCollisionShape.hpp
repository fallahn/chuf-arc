/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#ifndef CHUF_PHYS_COLLISION_SHAPE_HPP_
#define CHUF_PHYS_COLLISION_SHAPE_HPP_

#include <chuf2/NonCopyable.hpp>
#include <chuf2/ClassKey.hpp> //TODO does this need to be locked?
#include <chuf2/glm.hpp>
#include <chuf2/DataTypes.hpp>

#include <vector>
#include <array>

#include <btBulletCollisionCommon.h>

namespace chuf
{
    class HeightData;
    class Mesh;
    class PhysicsCollisionShape : private NonCopyable//, private Locked
    {
    public:
        enum class Type
        {
            None,
            Box,
            Capsule,
            Cylinder,
            Heightfield,
            Mesh,
            Sphere
        };

        PhysicsCollisionShape(Type, btCollisionShape*,/* const Key&, */btStridingMeshInterface* = nullptr);
        virtual ~PhysicsCollisionShape();

        class Definition final
        {
            friend class PhysicsCollisionShape;
        public:
            Definition();
            ~Definition() = default;

            bool empty() const;
            Type getType() const;
            
        private:
            PhysicsCollisionShape::Type m_type;
            struct BoxData final
            {
                std::array<float, 3u> centre;
                std::array<float, 3u> extents;
            };
            struct SphereData final
            {
                std::array<float, 3u> centre;
                float radius;
            };
            struct CapsuleData final
            {
                std::array<float, 3u> centre;
                float radius;
                float height;
            };
            struct CylinderData final
            {
                std::array<float, 3u> centre;
                float radius;
                float height;
            };

            union
            {
                BoxData box;
                SphereData sphere;
                CapsuleData capsule;
                CylinderData cylinder;
                HeightData* heightdata;
                Mesh* mesh;
            }m_data;

            bool m_explicit; //if we created this explicitly or from node bounds
            bool m_centreAbsolute; //whter or not the centre is in world coords, or relative to node
        };

        Type getType() const;
        btCollisionShape* getShape() const; //gets underlying bullet shape. should be ignored in public API :/

        //creates a box from the bounding area of the node this shape's component is attached to
        static Definition box();
        //creates a box from the given paramters, in world coordinates by default, else relative to the node centre
        //if the last paramter given is true
        static Definition box(const glm::vec3& extents, const glm::vec3& centre = glm::vec3(), bool absolute = false);
        //creates a sphere from the node bounding area that his shape's component is attached to
        static Definition Sphere();
        //creates a sphere from the given paramters, in world coords by default, else relative to the node positon
        //if the last paramter given is true
        static Definition Sphere(float radius, const glm::vec3& centre = glm::vec3(), bool absolute = false);
        //creates a capsule based on the bounding data of the node this shape's component is attached to
        static Definition capsule();
        //creates a capsule from the given paramters in world coordinates by default, else relative to node position
        //if the last parameter passed is true
        static Definition capsule(float radius, float length, const glm::vec3& centre = glm::vec3(), bool absolute = false);
        //creates a cylinder based on the bounding data of the node this shape's component is attached to
        static Definition cylinder();
        //creates a cylinder from the given paramters in world coordinates by default, else relative to node position
        //if the last parameter passed is true
        static Definition cylinder(float radius, float length, const glm::vec3& centre = glm::vec3(), bool absolute = false);
        //creates a heightfield from the terrain attached to this shape's component's node
        static Definition heightfield();
        //creates a heightfield from the given height data
        static Definition heightfield(HeightData*);
        //creates a mesh difiniton from the given mesh. This is recommended to be relative low poly
        //and convex. Concave shapes can be easily represented by several smaller meshes or shapes
        static Definition mesh(Mesh*);

    private:
        struct MeshData
        {
            std::vector<float> vertData;
            std::vector<UInt8> indexData;
        };

        struct HeightfieldData
        {
            HeightData* heightdata;
            bool dirtyInverse;
            glm::mat4 inverseMat;
            float minHeight;
            float maxHeight;
        };

        Type m_type;
        btCollisionShape* m_shape;
        btStridingMeshInterface* m_stridingInterface;

        union
        {//TODO need to better manage lifetimes so these don't dangle
            MeshData* meshData;
            HeightfieldData* heightData;
        }m_shapeData;
    };
}

#endif //PHYSIC_COLLISION_SHAPE_HPP_