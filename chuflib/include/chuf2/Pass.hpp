/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//represents one or more render passes in a material's technique
//for example a specific technique may have multiple passes used
//to draw shadow data, before rendering a model with shadows.
//different techniques may choose to use different passes to allow
//for varying hardware support of different rendering effects

#ifndef CHUF_PASS_HPP_
#define CHUF_PASS_HPP_

#include <chuf2/RenderState.hpp>
#include <chuf2/Shader.hpp>
#include <chuf2/VertexAttribBinding.hpp>

namespace chuf
{
    class Technique;
    class Pass final : public RenderState
    {
        friend class Material;
    public:
        using Ptr = std::unique_ptr<Pass>;
        //return the name of the pass
        const std::string&  getName() const;
        //get a pointer to the shader associated with this pass
        Shader* getShader();

        //sets a vertex attribute binding for the pass's shader
        //veretx attribute bindings wrap VAOs if they are available
        //else create a CPU side representation using attribPointers
        void setVertexAttribBinding(VertexAttribBinding* vb);
        //returns a pointer to this pass's atrtribute binding
        VertexAttribBinding* getVertexAttribBinding() const;

        //returns true if one or more queried flags are set
        bool hasFlag(UInt32 flags) const;
        //returns current flag set
        UInt32 flags() const;

        //binds the pass's shader and VAO is it is available
        void bind();
        //unbinds the pass's shader and VAO
        void unbind();

        Pass(const std::string& name, Technique* t);
        ~Pass() = default;

    private:
        std::string m_name;
        Technique* m_technique;
        Shader* m_shader;
        VertexAttribBinding* m_vertAttribBinding;

        UInt32 m_flags;

        bool init(const std::string& vertShader, const std::string& fragShader, const std::string& defines = "");
        bool initFromSource(const std::string& vertShader, const std::string& fragShader, const std::string& defines = "");
    };
}

#endif //CHUF_PASS_HPP_