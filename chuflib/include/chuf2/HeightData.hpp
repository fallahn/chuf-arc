/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//contains floating point height data loaded from a raw array or image

#ifndef CHUF_HEIGHT_DATA_HPP_
#define CHUF_HEIGHT_DATA_HPP_

#include <chuf2/NonCopyable.hpp>
#include <chuf2/ClassKey.hpp>
#include <chuf2/DataTypes.hpp>
#include <chuf2/glm.hpp>

#include <memory>
#include <string>
#include <vector>

namespace chuf
{
    class HeightData final : private NonCopyable, private Locked
    {
    public:
        using Ptr = std::unique_ptr<HeightData>;

        HeightData(UInt32 width, UInt32 height, const Key& key);
        ~HeightData() = default;

        //creates an empty height field with all values at 0
        static Ptr create(Uint32 width, UInt32 height);
        //creates height data from given image on disk (nullptr if image not found)
        static Ptr create(const std::string& path, float scaleMin = 0.f, float scaleMax = 1.f);
        //creates from a raw image as used by height map generating software
        //currently only supports little endian (PC) format images. Raw files have
        //no header data so the width and height must be supplied here
        static Ptr createFromRaw(const std::string& path, UInt32 width, UInt32 height, float scaleMin = 0.f, float scaleMax = 1.f);
        //creates height data from an existing array in memory
        static Ptr create(const float* data, UInt32 width, UInt32 height);

        //returns a reference to the underlying data
        const std::vector<float>& getData() const;
        //gets a value at the specified coordinate. If the value falls outside the
        //height data then it is clamped to the nearest value, values betweeb points
        //are interpolated linearly
        float getValue(const glm::vec2& position) const;
        //returns the dimensions of the height field
        const glm::uvec2& getSize() const;
        //returns row count
        UInt32 rowCount() const;
        //returns column count
        UInt32 columnCount() const;

    private:

        std::vector<float> m_data;
        glm::uvec2 m_dimensions;

    };
}
#endif //CHUF_HEIGHT_DATA_HPP_