/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//wrapper for openAL buffer which stores an audio clip

#ifndef CHUF_AUDIO_BUFFER_HPP_
#define CHUF_AUDIO_BUFFER_HPP_

#include <chuf2/AudioResource.hpp>
#include <chuf2/DataTypes.hpp>

#include <string>
#include <vector>
#include <set>

namespace chuf
{
    class AudioFile;
    class AudioSound;

    class AudioBuffer final : private AudioResource
    {
    public:
        AudioBuffer();
        AudioBuffer(const AudioBuffer& other);
        ~AudioBuffer();

        AudioBuffer& operator = (const AudioBuffer& rhv);

        //loads a file from the given path
        bool create(const std::string& path);
        //creates a buffer from an array of bytes
        bool create(const void* data, UInt32 size);
        //returns the array of samples in the buffer in 16bit format
        const Int16* getSamples() const;
        //returns the number of samples in the buffer
        UInt32 getSampleCount() const;
        //returns the sample rate of the buffer
        UInt32 getSampleRate() const;
        //returns the number of channels
        UInt32 getChannelCount() const;
        //returns total play time of sound in this buffer
        float getDuration() const;

    private:

        friend class AudioSound;

        using SoundList = std::set<AudioSound*>;
        UInt32 m_bufferId;
        std::vector<Int16> m_sampleData;
        float m_duration;
        mutable SoundList m_audioSounds;

        bool init(AudioFile& file);
        bool update(UInt32 channelCount, UInt32 sampleRate);
        void addSound(AudioSound* sound) const;
        void removeSound(AudioSound* sound) const;
    };
}

#endif //CHUF_AUDIO_BUFFER_HPP_
