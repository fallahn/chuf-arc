/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//provides an off screen render buffer with one or more colour targets
//and an optional depth / stencil target by wrapping and OpenGL FBO.
//Useful for post effects or deferred rendering etc.

#ifndef CHUF_FRAMEBUFFER_HPP_
#define CHUF_FRAMEBUFFER_HPP_

#include <chuf2/RenderTexture.hpp>
#include <chuf2/DepthStencilTarget.hpp>
#include <chuf2/Rectangle.hpp>
#include <chuf2/Colour.hpp>
#include <chuf2/View.hpp>

#include <vector>

namespace chuf
{
    class Image;
    class FrameBuffer final : private NonCopyable, public Locked
    {
        friend class App;
    public:
        using Ptr = std::unique_ptr<FrameBuffer>;

        FrameBuffer(const std::string& name, UInt16 width, UInt16 height, FrameBufferID fbo, const Key& key);
        ~FrameBuffer();

        //creates a frame buffer with no attachments, and adds it to the cache
        static FrameBuffer* create(const std::string& name);
        //creates a frame buffer with specified size, and a single render texture. Setting
        //width or height to 0 will create an fbo with no render targets
        static FrameBuffer* create(const std::string& name, UInt16 width, UInt16 height);
        //searchs the cache for frame buffer with given name and returns it, or nullptr if not found
        static FrameBuffer* getFrameBuffer(const std::string& name);

        //get the maximum number of render textures allowable on the current hardware
        static UInt16 getMaxRenderTextures();
        //binds the default frame buffer and returns a pointer to it
        static FrameBuffer* bindDefault();
        //returns a pointer to the currently bound frame buffer
        static FrameBuffer* getCurrent();


        //gets the name of this frame buffer
        const std::string& getName() const;
        //returns the width of the frame buffer
        UInt16 getWidth() const;
        //returns the height of the frame buffer
        UInt16 getHeight() const;
        //set the view of the fram buffer. returns the 
        //previous view for easy restoration
        View setView(const View& view);
        //gets the current view port
        const View& getView() const;

        //sets a render texture at the specified index on the frame buffer's colour attachment
        void setRenderTexture(RenderTexture* rt, UInt16 index = 0);
        //returns the render texture at the requested index or nullptr if there is none
        RenderTexture* getRenderTexture(UInt16 index = 0) const;
        //return the number of render textures attached to this frame buffer
        UInt16 getRenderTextureCount() const;
        //set the frame buffer's depth / stencil target
        void setDepthStencilTarget(DepthStencilTarget* dst);
        //returnd the depth stencil target if it is attached, or nullptr
        DepthStencilTarget* getDepthStencilTarget() const;
        //returns true if this is the default frame buffer
        bool defaultBuffer() const;
        //binds this buffer for drawing and returns a pointer to the previously bound
        //buffer so that it can easily be restored
        FrameBuffer* bind();
        //clears the buffer, including all its attachments (depth / stencil etc)
        void clear(Colour);

    private:

        std::string m_name;
        FrameBufferID m_fbo;
        DepthStencilTarget* m_dst;
        std::vector<RenderTexture*> m_renderTextures;
        View m_view;

        Int32 m_clearFlags;

        static void start();
        static void finish();
        void setRenderTexture(RenderTexture* rt, UInt16 index, GLenum target);

    };
}

#endif //CHUF_FRAMEBUFFER_HPP_