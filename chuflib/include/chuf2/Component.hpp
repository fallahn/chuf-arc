/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//base class for node attachable components

#ifndef CHUF_COMPONENT_HPP_
#define CHUF_COMPONENT_HPP_

#include <chuf2/Taggable.hpp>
#include <chuf2/Transform.hpp>

#include <memory>

namespace chuf
{
    class Node;
    class Component : public Taggable, public Transform::Listener
    {
    public:
        using Ptr = std::unique_ptr<Component>;

        Component() : m_destroyed(false), m_node(nullptr){}
        virtual ~Component() = default;

        virtual void destroy(){ m_destroyed = true; }
        bool destroyed() const { return m_destroyed; }

        const Node* getNode() const { return m_node; }
        virtual void setNode(Node* node){ m_node = node; }

        virtual void transformChanged(Transform&) override {}

    private:
        bool m_destroyed;
        Node* m_node;
    };
}

#endif //CHUF_COMPONENT_HPP_