/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//provides a base class for any transformable objects

#ifndef CHUF_TRANSFORM_HPP_
#define CHUF_TRANSFORM_HPP_

#include <chuf2/glm/glm.hpp>
#include <chuf2/glm/gtc/quaternion.hpp>

#include <chuf2/DataTypes.hpp>
#include <chuf2/AlignedAllocation.hpp>

#include <list>
#include <vector>
#include <memory>

namespace chuf
{
    class Transform : public Aligned<16>
    {
    public:
        enum class Axis
        {
            X,
            Y,
            Z
        };

        //custom listener callback for transform update notification
        class Listener
        {
        public:
            virtual ~Listener() = default;
            virtual void transformChanged(Transform& t) = 0;
        };

        Transform();
        Transform(const glm::vec3& translation, const glm::quat& rotation, const glm::vec3& scale);
        Transform(const glm::vec3& translation, const glm::mat4& rotation, const glm::vec3& scale);
        Transform(const Transform& copy);
        virtual ~Transform() = default;

        //returns a copy of the current transformation
        //because any reference would become out dated
        //once the transform is modified
        glm::mat4 getMatrix();

        //returns the translation of this trnsform in world coordinates
        const glm::vec3& getTranslation() const;
        //returns a quaternion represeting the nodes rotation
        const glm::quat& getRotation() const;
        //fills supplied matric with this transforms rotation
        void getRotation(glm::mat4& dest) const;
        //fills the supplied vector with this transforms rotation as euler angles
        void getRotation(glm::vec3& dest) const;
        //returns a vector 3 containing the scale of each of the transforms axes
        const glm::vec3& getScale() const;
        
        //returns the forward vector of the transform
        glm::vec3 getForwardVector() const;
        //returns the back vector of the transform
        glm::vec3 getBackVector() const;
        //returns the up vector of the transform
        glm::vec3 getUpVector() const;
        //returns the down vector of the transform
        glm::vec3 getDownVector() const;
        //returns the left vector of the transform
        glm::vec3 getLeftVector() const;
        //returns the right vector of the transform
        glm::vec3 getRightVector() const;

        //translates the transform by x, y, z
        void translate(float x, float y, float z);
        //translates the transform by translation
        void translate(const glm::vec3& translation);
        //2D translation in x and y
        void translate(float x, float y);
        //2d translation by given vector in x and y
        void translate(const glm::vec2&);

        //rotates the transform by the given quaternion
        void rotate(const glm::quat& rotation);
        //rotates the transform about the given axis by the amount in degrees
        void rotate(Axis axis, float angle);
        //rotates the transform by the given matrix
        void rotate(const glm::mat4& rotation);

        //scles all axes by the given amount
        void scale(float scale);
        //scales x and y axis by the corresponding component of the given vector
        void scale(const glm::vec2& scale);
        //scales each axis by the corresponding component of the given vector
        void scale(const glm::vec3& scale);

        //sets the transform by the given values
        void set(const glm::vec3& translation, const glm::quat& rotation, const glm::vec3& scale);
        //sets the transform by the given values
        void set(const glm::vec3& translation, const glm::mat4& rotation, const glm::vec3& scale);
        //sets the transform according to the given matrix
        void set(const glm::mat4& matrix);
        //sets the transform according the given transform
        void set(const Transform& transform);

        //resets the transform to its identity values
        void setIdentity();
        //sets the translation to the x, y, z values
        void setTranslation(float x, float y, float z);
        //sets the translation to the x, y, z component values of the given vec3
        void setTranslation(const glm::vec3& translation);
        //sets the translation to the give x and y values
        void setTranslation(float x, float y);
        //sets the translation x and y values to the given vec2
        void setTranslation(const glm::vec2&);

        //sets the transforms rotation to the given quaternion
        void setRotation(const glm::quat& rotation);
        //sets the given axis to the given angle in degrees
        void setRotation(Axis axis, float angle);
        //sets the transforms rotation to that of the rotational part of the given matrix
        void setRotation(const glm::mat4& rotation);

        //sets the scale of all axes to the given value
        void setScale(float scale);
        //sets the scale of each axis to that of the given value
        void setScale(float x, float y, float z);
        //sets the scale of each axes to that of each component in the given vec3
        void setScale(const glm::vec3& scale);
        //sets the scale of the x and y axis to the given values
        void setScale(float x, float y);
        //sets the scale of the x and y values to the given vec2
        void setScale(const glm::vec2&);

        //transforms a given point by the current matrix
        glm::vec3 transformPoint(const glm::vec3& point);
        //transforms the given point by this transforms matrix
        //void transformPoint(glm::vec3& point);
        //transforms 2D point in the x / y axis by this transform's matrix
        glm::vec2 transformPoint(const glm::vec2&);


        void addListener(Listener& l);
        void removeListener(Listener& l);

        //pauses the transform update while any parent transforms
        //are updated
        static void pauseTransformUpdate();
        //resumes the current transform update
        static void resumeTransformUpdate();
        //returns true if transform updates are paused
        static bool transformUpdatePaused();

    protected:
        
        enum DirtyBits
        {
            TRANSLATION = (1<<0),
            ROTATION    = (1<<1),
            SCALE       = (1<<2),
            NOTIFY      = (1<<3)
        };

        void dirty(UInt8 dirtyBits);
        bool isDirty(UInt8 dirtyBits) const;

        static void pauseTransformation(Transform& transform);
        virtual void transformChanged();

    private:

        //TODO add protected accessors if needed
        glm::vec3 m_translation;
        glm::quat m_rotation;
        glm::vec3 m_scale;

        glm::mat4 m_matrix;
        UInt8 m_dirtyBits;

        std::list<Listener*> m_listeners;

    };
}

#endif //CHUF_TRANSFORM_HPP_