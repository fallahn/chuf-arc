/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//defines a mesh / group of sub-meshes which contribute to a model
//attachable to a scene node. the mesh class is a wrapper for an
//opengl vertex buffer object.

#ifndef CHUF_MESH_HPP_
#define CHUF_MESH_HPP_

#include <chuf2/glm/glm.hpp>
#include <chuf2/GL/gl3w.h>

#include <chuf2/NonCopyable.hpp>
#include <chuf2/ClassKey.hpp>

#include <chuf2/VertexLayout.hpp>
#include <chuf2/BoundingBox.hpp>
#include <chuf2/BoundingSphere.hpp>
#include <chuf2/Colour.hpp>

#include <memory>
#include <map>

namespace chuf
{
    class SubMesh;
    class Skin;
    class Mesh : private NonCopyable, protected Locked
    {
    public:
        using Ptr = std::shared_ptr<Mesh>;

        Mesh(const VertexLayout& v, const Key& key);
        virtual ~Mesh();

        enum class IndexFormat
        {
            I8  = GL_UNSIGNED_BYTE,
            I16 = GL_UNSIGNED_SHORT,
            I32 = GL_UNSIGNED_INT
        };

        enum class PrimitiveType
        {
            Points			= GL_POINTS,	
            Lines			= GL_LINES,
            LineStrip		= GL_LINE_STRIP,
            Triangles		= GL_TRIANGLES,
            TriangleStrip	= GL_TRIANGLE_STRIP
        };
        //meshes return a shared_ptr so that they may be used in more than one model

        //creates a single VBO from the specified vertex data layout and vertex count.
        static Ptr createMesh(const VertexLayout& layout, UInt32 count, bool dynamic = false);
        //creates a quad from the given four points in 3d space, with UV coordinates and normals
        //tangents and bitangents allowing basic texturing / lighting
        static Ptr createQuad(const glm::vec3& tl, const glm::vec3& bl, const glm::vec3& tr, const glm::vec3& br);
        //creates a set of lines from the given vector of points
        static Ptr createLines(const std::vector<glm::vec3>& points);
        //creates a 1x1x1 dimension cube which can be scaled via its model's parent node to any size.
        //includes UV, normal, bitangent and tangent vertex data.
        static Ptr createCube();
        //creates a box from the given bounding box with positon and colour data
        static Ptr createFromBoundingBox(const BoundingBox& bb, const Colour& colour = Colour::red);
        //creates a sphere from given bounding sphere with position and colour data
        static Ptr createFromBoundingSphere(const BoundingSphere& bs, const Colour& colour = Colour::red);
        //creates a visualisation mesh from a given frustum
        static Ptr createFromFrustum(const Frustum& f, const Colour& colour = Colour::yellow);
        //creates a visualisation of normal data from a given mesh, if it has any. scale represents line length
        static Ptr createNormalMesh(const Ptr& mesh, float scale = 1.f);
        //creates a visualisation mesh for bones of a given skin
        static Ptr createBoneMesh(const std::shared_ptr<Skin>& skin, Int32 index = -1);
        //creates a grid mesh of given size with given tris per edge
        static Ptr createGridMesh(const glm::vec2& size, const glm::ivec2& triCount = glm::ivec2(1), bool createUVs = false);

        //returns a reference to the object defining the vertex layout
        //of this mesh
        const VertexLayout& getVertexLayout() const;
        //returns the number of vertices used by this mesh
        UInt32 getVertexCount() const;
        //returns the data size of a single vertex in bytes.
        //when using to calulate the stride of floating values for example
        //divide this value by sizeof(float)
        UInt32 getVertexSize() const;
        //the opengl handle id for the mesh's VBO
        VertexBufferID getVertexBuffer() const;
        //returns true if this mesh is hinted with GL_DYNAMIC_DRAW
        bool dynamic() const;
        //returns the mesh's primitive type, TriangleStrip by defualt
        PrimitiveType getPrimitiveType() const;
        //sets the mesh's primitive type
        void setPrimitveType(PrimitiveType type);
        //uploads given vertex data to the VBO
        void setVertexData(const float* data, UInt32 count = 0u, UInt32 start = 0u);
        //adds a submesh which is a wrapper for an opengl element index array
        SubMesh& addSubMesh(PrimitiveType type, IndexFormat format, UInt32 count, bool dynamic = false);
        //returns the number of submeshes making up this mesh
        UInt32 getSubMeshCount() const;
        //returns the subMesh at the given index - TODO this should be a pointer and return nullptr if not found
        SubMesh& getSubMesh(UInt32 index) const;

        //gets the bounding box of the mesh if it has one
        const BoundingBox& getBoundingBox() const;
        //set the mesh's bounding box
        void setBoundingBox(const BoundingBox& bb);
        //gets the bounding sphere is the mesh has one
        const BoundingSphere& getBoundingSphere() const;
        //sets the bounding sphere of this mesh
        void setBoundingSphere(const BoundingSphere& bs);

    private:

        VertexLayout m_vertLayout;
        UInt32 m_vertCount;
        VertexBufferID m_vertBuffer;
        PrimitiveType m_primitiveType;
        std::vector<std::unique_ptr<SubMesh>> m_subMeshes;
        bool m_dynamic;
        BoundingBox m_boundingBox;
        BoundingSphere m_boundingSphere;

        static void updateNormalMeshArray(UInt32 typeIndex, const VertexLayout& vertData, const std::vector<float>& posData, const std::vector<float>& vboData, std::vector<float>& newData, float scale, Colour colour);
    };
}

#endif //CHUF_MESH_HPP_