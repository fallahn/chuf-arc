/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//defines a sub mesh by indexing the vertex array of a mesh object

#ifndef CHUF_SUB_MESH_HPP_
#define CHUF_SUB_MESH_HPP_

#include <chuf2/NonCopyable.hpp>
#include <chuf2/ClassKey.hpp>
#include <chuf2/DataTypes.hpp>

#include <chuf2/Mesh.hpp>

#include <memory>

namespace chuf
{
    //the sub mesh class effectively wraps an index buffer
    //for an associated VBO
    class SubMesh final : private NonCopyable, private Locked
    {
        friend class Mesh;
    public:
        using Ptr = std::unique_ptr<SubMesh>;
        explicit SubMesh(const Key& key);
        ~SubMesh();

        UInt32 getMeshIndex() const;
        //returns the primitive type used to draw the sub mesh
        Mesh::PrimitiveType getPrimitiveType() const;
        //returns the number of indices stored in the index buffer
        UInt32 getIndexCount() const;
        //returns the index format of the buffer data, used
        //to calculate the stride of the array
        Mesh::IndexFormat getIndexFormat() const;
        //returns the opengl buffer handle of the element array
        IndexBufferID getIndexBuffer() const;
        //returns true if this buffer is hinted with GL_DYNAMIC_DRAW
        bool dynamic() const;
        //set all or part of the buffer data with data pointed to by data
        void setIndexData(const void* data, UInt32 start, UInt32 count);

    private:
        static Ptr create(Mesh* mesh, UInt32 meshIndex, Mesh::PrimitiveType type, Mesh::IndexFormat format, UInt32 count, bool dynamic = false);
        
        Mesh* m_mesh;
        UInt32 m_meshIndex;
        Mesh::PrimitiveType m_primitiveType;
        Mesh::IndexFormat m_indexFormat;
        UInt32 m_count;
        IndexBufferID m_indexBuffer;
        bool m_dynamic;
    };
}

#endif //CHUF_SUB_MESH_HPP_