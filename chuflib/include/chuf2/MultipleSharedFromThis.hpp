/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//allows multiple inheritance of std::shared_from_this

#ifndef CHUF_MSFRT_HPP_
#define CHUF_MSFRT_HPP_

#include <memory>

class MultipleSharedFromThis : public std::enable_shared_from_this<MultipleSharedFromThis>
{
public:
    virtual ~MultipleSharedFromThis(){}
};

template <class T>
class inherit_enable_shared_from_this : public virtual MultipleSharedFromThis
{
public:
    std::shared_ptr<T> shared_from_this()
    {
        return std::dynamic_pointer_cast<T>(MultipleSharedFromThis::shared_from_this());
    }
    template <class D>
    std::shared_ptr<D> downcast_shared_from_this()
    {
        return std::dynamic_pointer_cast<D>(MultipleSharedFromThis::shared_from_this());
    }
};


#endif //CHUF_MSFRT_HPP_