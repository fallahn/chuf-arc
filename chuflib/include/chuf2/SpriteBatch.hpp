/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//class to draw multiple sprites from a single texture in a single draw call
//by default textures are flipped when loaded so that sprite Y coordinates
//start at the bottom of the screen and work upwards. This is convenient when
//trying to match coordinates of the scene graph, or external libraries such
//as box2d which have Y zero at the bottom of the screen. if you prefer to have
//Y zero at the top and increment downwards then make sure that 'flipVertically'
//is set to false when creating the texture to be passed to the sprite batch

#ifndef CHUF_SPRITE_BATCH_HPP_
#define CHUF_SPRITE_BATCH_HPP_

#include <chuf2/NonCopyable.hpp>
#include <chuf2/ClassKey.hpp>
#include <chuf2/Rectangle.hpp>
#include <chuf2/VertexLayout.hpp>
#include <chuf2/AlignedAllocation.hpp>
#include <chuf2/Mesh.hpp>

#include <chuf2/glm/glm.hpp>

#include <memory>
#include <string>
#include <vector>

namespace chuf
{
    class Material;
    class Texture;
    class Shader;
    class Sprite;
    class SpriteBatch final : private NonCopyable, private Locked, public Aligned<16>
    {
        friend class Sprite;
    public:
        using Ptr = std::unique_ptr<SpriteBatch>;

        //attempts to create a spritebatch from a path to an image, and an
        //optional custom shader. returns null if loading image fails
        static Ptr create(const std::string& path, Shader* shader = nullptr);
        //creates a spritebatch from a valid pointer to a texture, and an optional
        //shader.
        static Ptr create(Texture* texture, Shader* shader = nullptr);
        //creates a sprite batch from a valid configuration file, else returns nullptr
        static Ptr createFromFile(const std::string& path);

        //adds a sprite to this batch and returns a pointer to it, optionally passing in
        //an initial position and a name.
        std::shared_ptr<Sprite> addSprite(const glm::vec2& position = glm::vec2(), const std::string& name = "");
        //find a sprite in this batch by name and returns a pointer to it if found,
        //else returns nullptr
        std::shared_ptr<Sprite> findSprite(const std::string& name);
        //removes a sprite from this batch and updates the underlying VBO
        void removeSprite(const std::shared_ptr<Sprite>& sprite);

        const std::vector<std::shared_ptr<Sprite>>& getSprites() const;

        //set true to enable texture repeating, or false to clamp textures
        void setTextureRepeat(bool repeat);
        //sprite batches are created with an orthographic projection the
        //dimensions of the current window. use this to set a new projection
        //matrix if the window is resized, or the sprite batch is being rendered
        //to a different target such as a render texture, with different resolution
        void setProjectionMatrix(const glm::mat4& matrix);
        //primarily used for text, set a view matrix to modify the entire sprite batch
        void setViewMatrix(const glm::mat4&  matrix);

        //returns a bounding box encompassing all sprites
        //in local coordinates
        const FloatRect& getLocalBounds() const;
        //returns the global bounding box of the sprite batch taking
        //into account any global transformation
        const FloatRect& getGlobalBounds() const;

        //draws the sprite batch to the currently active buffer
        void draw();

        explicit SpriteBatch(const Key& key);
        ~SpriteBatch() = default;
    private:

        enum DirtyBits
        {
            TRANSFORM     = (1 << 0),
            LOCAL_BOUNDS  = (1 << 1),
            GLOBAL_BOUNDS = (1 << 2),

            ALL = TRANSFORM | LOCAL_BOUNDS | GLOBAL_BOUNDS
        };

        std::vector<float> m_vertexArray;
        UInt32 m_vertCount;
        Mesh::Ptr m_mesh;
        UInt32 m_bufferSize;

        std::shared_ptr<Material> m_material;
        glm::vec2 m_textureSize;
        glm::mat4 m_projectionMatrix;
        glm::mat4 m_viewMatrix;
        mutable glm::mat4 m_viewProjectionMatrix;

        mutable FloatRect m_localBounds;
        mutable FloatRect m_globalBounds;

        mutable UInt8 m_dirtyBits;

        std::vector<std::shared_ptr<Sprite>> m_sprites;
        std::vector<Sprite*> m_dirtySprites;

        const glm::mat4& getViewProjectionMatrix();
        void setMaterial(const std::shared_ptr<Material>& material);
    };
}
#endif //CHUF_SPRITE_BATCH_HPP_