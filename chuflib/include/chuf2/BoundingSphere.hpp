/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#ifndef CHUF_BOUNDING_SPHERE_HPP_
#define CHUF_BOUNDING_SPHERE_HPP_

#include <chuf2/Frustum.hpp>

#include <chuf2/glm/glm.hpp>

#include <vector>

namespace chuf
{
    class Ray;
    class BoundingBox;
    class BoundingSphere final
    {
    public:
        BoundingSphere();
        BoundingSphere(const glm::vec3& centre, float radius);
        BoundingSphere(const BoundingSphere& copy);
        ~BoundingSphere() = default;

        //returns the radius
        float getRadius() const;
        //returns the position of the centre
        const glm::vec3& getCentre() const;
        //set the radius of the sphere
        void setRadius(float rad);
        //set the position of the sphere
        void setCentre(const glm::vec3& pos);

        //returns true if intersects with give sphere
        bool intersects(const BoundingSphere& sphere) const;
        //returns true if intersects given bounding box
        bool intersects(const BoundingBox& bb) const;
        //returns true if sphere intersects given frustum
        bool intersects(const Frustum& f) const;
        //returns whether sphere is on front or rear side of 
        //given plane, or if it intersects
        Plane::Intersection intersects(const Plane& p) const;
        //returns the distane to the origin of the ray if
        //it intersects, else < 0
        float intersects(const Ray&) const;

        //returns true if sphere is empty, ie all zero
        bool empty() const;

        //merges this sphere with given sphere by making it the
        //smallest sphre capable of encompassing both
        void merge(const BoundingSphere& sphere);
        //merges this sphere with the given box by making it the
        //smallest sphere which will encompass both
        void merge(const BoundingBox& bb);

        //set the sphere to given values
        void set(const glm::vec3& centre, float radius);
        //set this sphere to copy the given sphere
        void set(const BoundingSphere& other);
        //set this sphere to contain the given box as tightly
        //as possible
        void set(const BoundingBox& bb);

        //transform the sphere by given matrix
        void transform(const glm::mat4& matrix);

        BoundingSphere& operator *= (const glm::mat4& matrix);


    private:
        glm::vec3 m_centre;
        float m_radius;

        float distanceSquared(const BoundingSphere& bs, const glm::vec3& point);
        bool contains(const BoundingSphere& bs, const std::vector<glm::vec3>& points);
    };

    BoundingSphere operator * (const glm::mat4& matrix, const BoundingSphere& bs);

}

#endif //CHUF_BOUNDING_SPHERE_HPP_