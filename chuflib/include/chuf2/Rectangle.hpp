/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//simple rectangle class

#ifndef CHUF_RECT_HPP_
#define CHUF_RECT_HPP_

#include <chuf2/glm.hpp>

namespace chuf
{
    template <class T>
    class Rectangle final
    {
    public:
        Rectangle()
            : left(static_cast<T>(0)), top(static_cast<T>(0)), width(static_cast<T>(0)), height(static_cast<T>(0)){}
        Rectangle(T l, T t, T w, T h)
            : left(l), top(t), width(w), height(h){}

        T left, top, width, height;

        ~Rectangle() = default;

        bool intersects(const Rectangle<T>& rect) const
        {
            Rectangle<T> i;
            return intersects(rect, i);
        }

        bool intersects(const Rectangle<T>& rectangle, Rectangle<T> intersection) const
        {
            //compute the min and max of the first rectangle on both axes
            T r1MinX = std::min(left, static_cast<T>(left + width));
            T r1MaxX = std::max(left, static_cast<T>(left + width));
            T r1MinY = std::min(top, static_cast<T>(top + height));
            T r1MaxY = std::max(top, static_cast<T>(top + height));

            //compute the min and max of the second rectangle on both axes
            T r2MinX = std::min(rectangle.left, static_cast<T>(rectangle.left + rectangle.width));
            T r2MaxX = std::max(rectangle.left, static_cast<T>(rectangle.left + rectangle.width));
            T r2MinY = std::min(rectangle.top, static_cast<T>(rectangle.top + rectangle.height));
            T r2MaxY = std::max(rectangle.top, static_cast<T>(rectangle.top + rectangle.height));

            //compute the intersection boundaries
            T interLeft = std::max(r1MinX, r2MinX);
            T interTop = std::max(r1MinY, r2MinY);
            T interRight = std::min(r1MaxX, r2MaxX);
            T interBottom = std::min(r1MaxY, r2MaxY);

            //if the intersection is valid (positive non zero area), then there is an intersection
            if ((interLeft < interRight) && (interTop < interBottom))
            {
                intersection = Rectangle<T>(interLeft, interTop, interRight - interLeft, interBottom - interTop);
                return true;
            }
            else
            {
                intersection = Rectangle<T>(0, 0, 0, 0);
                return false;
            }
        }

        glm::vec2 centre() const
        {
            return glm::vec2(left + (width / 2.f), top + (height / 2.f));
        }

        bool contains(const Rectangle<T>& other) const
        {
            if (other.left < left) return false;
            if (other.top < top) return false;
            if (other.left + other.width > left + width) return false;
            if (other.top + other.height > top + height) return false;

            return true;
        }

        bool contains(const glm::vec2& point) const
        {
            return contains(static_cast<T>(point.x), static_cast<T>(point.y));
        }

        bool contains(T x, T y) const
        {
            T minX = std::min(left, static_cast<T>(left + width));
            T maxX = std::max(left, static_cast<T>(left + width));
            T minY = std::min(top, static_cast<T>(top + height));
            T maxY = std::max(top, static_cast<T>(top + height));

            return (x >= minX) && (x < maxX) && (y >= minY) && (y < maxY);
        }

        bool operator == (const Rectangle<T>& rhv)
        {
            if (this == &rhv) return true;
            return ((this->top == rhv.top) && (this->left == rhv.left)
                && (this->height == rhv.height) && (this->width == rhv.width));
        }

        bool operator != (const Rectangle<T>& rhv)
        {
            return !(*this == rhv);
        }
    };

    using FloatRect = Rectangle<float>;
    using IntRect = Rectangle<int>;
    using UIntRect = Rectangle<unsigned>;
}

#endif //CHUF_RECT_HPP_
