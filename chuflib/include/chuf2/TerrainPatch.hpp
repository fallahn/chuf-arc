/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#ifndef CHUF_TERRAIN_PATCH_HPP_
#define CHUF_TERRAIN_PATCH_HPP_

#include <chuf2/Model.hpp>
#include <chuf2/Camera.hpp>
#include <chuf2/Texture.hpp>
#include <chuf2/NonCopyable.hpp>
#include <chuf2/BoundingBox.hpp>
#include <chuf2/BoundingSphere.hpp>
#include <chuf2/Pass.hpp>
#include <chuf2/ClassKey.hpp>
#include <chuf2/QuadTreeComponent.hpp>

#include <vector>
#include <string>

namespace chuf
{
    class Terrain;
    class TerrainPatch final : public NonCopyable, public Locked, public QuadTreeComponent
    {
        friend class Terrain;

    public:
        using Ptr = std::unique_ptr<TerrainPatch>;
        TerrainPatch(const Key&);
        ~TerrainPatch() = default;

        void cameraChanged();
        const BoundingBox& getBoundingBox(bool worldSpace = true);
        const BoundingSphere& getBoundingSphere(bool worldSpace = true);
        std::shared_ptr<Material> getMaterial() const;

    private:


        enum DirtyBits
        {
            BOUNDS   = 0x1,
            LEVEL    = 0x2,
            ALL      = BOUNDS | LEVEL
        };

        struct Level final
        {
            using Ptr = std::unique_ptr<Level>;
            Model::Ptr model;
        };

        Terrain* m_terrain;
        UInt32 m_index;
        UInt32 m_row;
        UInt32 m_column;

        std::vector<Level::Ptr> m_levels;

        BoundingBox m_boundingbox;
        BoundingBox m_boundingboxWorld;
        BoundingSphere m_boundingSphere;
        BoundingSphere m_boundingSphereWorld;
        mutable Camera* m_camera;
        mutable UInt32 m_currentLevel;
        mutable Int32 m_flags;
        mutable bool m_passedCulling;

        static Ptr create(Terrain&, UInt32 index,
                        UInt32 row, UInt32 column,
                        const std::vector<float>& heights, UInt32 width, UInt32 height,
                        UInt32 x1, UInt32 z1, UInt32 x2, UInt32 z2,
                        float xOffset, float zOffset, UInt32 maxStep, float skirtSize);

        void addLOD(const std::vector<float>& heights, UInt32 width, UInt32 height,
                    UInt32 x1, UInt32 z1, UInt32 x2, UInt32 z2,
                    float xOffset, float zOffset, UInt32 step, float skirtSize);

        UInt32 draw(UInt32 passflags, bool testFrustum, bool wireframe = false);
        void updateMaterial();
        UInt32 computeLOD(Camera*, const BoundingBox& worldBounds);
        Colour getAmbientColour() const;
        float computeHeight(const std::vector<float>& heights, UInt32 width, UInt32 x, UInt32 z);
        void updateNodeBindings();
        void updateBounds();
    };
}

#endif //CHUF_TERRAIN_PATCH_HPP_