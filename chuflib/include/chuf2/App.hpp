/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//main application abstract base class. 

#ifndef CHUF_APP_HPP_
#define CHUF_APP_HPP_

#include <chuf2/GL/gl3w.h>
#include <SDL.h>

#include <chuf2/NonCopyable.hpp>
#include <chuf2/Colour.hpp>
#include <chuf2/View.hpp>

#include <iostream>
#include <memory>

namespace chuf
{
    //used to request specific video settings on launch. Pass an instance of this
    //struct to the constructor of the class which is derived from App
    struct VideoContext
    {
        VideoContext()
            : width(1024u), height(768u), depthbits(24u){}
        VideoContext(Uint16 w, UInt16 h, const std::string& title, UInt8 depthBits = 32u)
            : width(w), height(h), windowTitle(title), depthbits(depthBits){}

        Uint16 width;
        UInt16 height;
        std::string windowTitle;
        UInt8 depthbits;
    };
    
    class Image;
    class App : private NonCopyable
    {
    public:
        enum ClearFlags
        {
            COLOUR					= GL_COLOR_BUFFER_BIT,
            DEPTH					= GL_DEPTH_BUFFER_BIT,
            STENCIL					= GL_STENCIL_BUFFER_BIT,
            COLOUR_DEPTH			= COLOUR | DEPTH,
            COLOUR_STENCIL			= COLOUR | STENCIL,
            DEPTH_STENCIL			= DEPTH | STENCIL,
            COLOUR_DEPTH_STENCIL	= COLOUR | DEPTH | STENCIL
        };

        virtual ~App();

        //returns a reference to the currently running instance of
        //the app.
        static App& getInstance();
        void run();
        void close();

        //enables vertical syncronisation
        void setVSyncEnabled(bool enabled = true);
        bool vSyncEnabled();

        //sets the viewport of the active window
        //returns the previous view for easy restoration
        View setView(const View& viewPort);
        const View& getView() const;
        const VideoContext& getVideoContext() const;

        //sets the title of the active window, if applicable
        void setWindowTitle(const std::string& title);
        //returns the current rendering frame rate
        float getFramerate() const;

        //clears the backbuffer in preparation for drawing
        void clear(Colour colour, ClearFlags flags);

        //captures or releases mouse control. set this to true, for example
        //when using the mouse to control the scene camera.
        void setMouseCaptured(bool captured);

        //saves a screen shot of the current frame to a new image
        std::unique_ptr<Image> getScreenshot() const;

        //get system date/time as string
        std::string getDateTimeString() const;

        //gets the mouse position relative to the window
        glm::ivec2 getMousePosition() const;

    protected:
        App(const VideoContext& videoSettings = VideoContext());

        //implement these in your derived class

        //performed on app start up, used for loading resources for example
        virtual void start() = 0;
        //use this to perform any logic updates to the app's state
        virtual void update(float dt) = 0;
        //any drawing should be performed here, whether it be via the scene-graph
        //sprite batch or to an off-screen target
        virtual void draw() = 0;
        //override this and use it to properly release and tidy up and resources
        virtual void finish() = 0;

        //event handlers - override these as necessary.
        virtual void handleMouseEvent(const Event::MouseEvent& evt);
        virtual void handleKeyEvent(const Event::KeyEvent& evt);
        virtual void handleJoyEvent(const Event::JoyEvent& evt);
        virtual void handleControllerEvent(const Event::ControllerEvent& evt);
        virtual void handleWindowEvent(const Event::WindowEvent& evt);

    private:

        SDL_Window* m_window;
        SDL_GLContext m_glContext;

        View m_view;
        VideoContext m_videoContext;

        float m_framerate;

        static App* m_instance;

        void killSDL();

        bool m_running;

        void parseEvents();
    };
}

#endif //CHUF_APP_HPP_
