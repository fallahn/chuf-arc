/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//represents a transformation controlled by one or more bones

#ifndef CHUF_SKIN_HPP_
#define CHUF_SKIN_HPP_

#include <chuf2/DataTypes.hpp>
#include <chuf2/ClassKey.hpp>
#include <chuf2/NonCopyable.hpp>
#include <chuf2/glm.hpp>

#include <vector>
#include <memory>

namespace chuf
{
    using Frame = std::vector<glm::mat4>;
    using Keyframes = std::vector<Frame>;
    class Skin final : private NonCopyable, private Locked
    {
    public:
        struct Animation
        {
        public:
            Animation(UInt32 frameCount, UInt32 startFrame, float frameRate, const std::string& name, bool looped = true);
            ~Animation() = default;

            const glm::mat4* getCurrentFrame() const;
            void setSkin(Skin* skin);
            void update(float dt);

        private:
            UInt32 m_frameCount;
            UInt32 m_startFrame;
            UInt32 m_lastFrame;
            float m_frameRate;
            std::string m_name;
            bool m_looped;

            Frame m_currentFrame;
            Skin* m_parentSkin;
            float m_currentTime;
            float m_frameTime;
            UInt32 m_currentFrameIndex;
            UInt32 m_nextFrameIndex;

            void buildFirstFrame();
            void interpolate(const Frame& a, const Frame& b, float time);
        };
        using Animations = std::vector<Animation>;
        using Ptr = std::shared_ptr<Skin>;
        using BoneHierarchy = std::vector<Int32>;

        //friend class Animation;
        Skin(const BoneHierarchy bones, const Keyframes& keyframes, const Key& key);
        ~Skin() = default;

        static Ptr create(const BoneHierarchy bones, const Keyframes& keyframes);

        const Frame& getFrame(UInt32 index) const;

        void addAnimation(const Animation& anim);
        void setAnimations(const Animations& animations);
        Animation& getAnimation(UInt32 index);


    private:
        Keyframes m_frames;
        Animations m_animations;
        BoneHierarchy m_boneIndices; //list of joint parent IDs
    };
}
#endif //CHUF_SKIN_HPP_