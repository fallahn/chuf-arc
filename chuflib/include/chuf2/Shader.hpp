/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//wraps an OpenGL program as a single shader object

#ifndef CHUF_SHADER_HPP_
#define CHUF_SHADER_HPP_

#include <chuf2/GL/gl3w.h>
#include <chuf2/glm/glm.hpp>

#include <chuf2/ClassKey.hpp>
#include <chuf2/NonCopyable.hpp>
#include <chuf2/DataTypes.hpp>
#include <chuf2/Texture.hpp>

#include <memory>
#include <string>
#include <map>
#include <functional>

namespace chuf
{
    struct Colour;
    class Shader final : private NonCopyable, private Locked
    {
    public:
        enum class Type
        {
            Vertex,
            Tesselation,
            Geometry,
            Fragment
        };

        using SourceList = std::map<Type, std::string>;
        using Ptr = std::unique_ptr<Shader>;

        //wraps a uniform variable so that uniform
        //values may be maintained by render state objects
        //and applied when a shader is bound
        class Uniform final : private NonCopyable
        {
            friend class Shader;
        public:
            using Ptr = std::unique_ptr<Uniform>;
            explicit Uniform(const Shader::Key& key);
            ~Uniform() = default;
            const std::string& getName() const;
            GLenum getType() const;
            Shader* getShader();

        private:
            std::string m_name;
            GLint m_location;
            GLenum m_type;
            UInt32 m_index;
            Shader* m_shader;
        };

        explicit Shader(const Key& key);
        ~Shader();

        //create a shader from a list of strings containing the source code for each type of shader. requires at least
        //vertex and fragment shaders, other shaders such as tesselation and geometry shaders are optional.
        //the defines string may contain a newline (\n) delimited list of glsl defines eg "#define BUMP\n#define CUBE\n"
        static Shader* createFromSource(SourceList& sources, const std::string& defines = "");
        //create a shader from a pair of files located on local storage, where vertShader and fragShader contain the
        //relative paths to those files. the defines string may contain a newline (\n) delimited list of glsl defines
        //used by the shader compiler
        static Shader* create(const std::string& vertShader, const std::string& fragShader, const std::string& defines = "");

        //returns the opengl handle to the requested VAO if it exists, else -1 if it does not
        VertexAttributeID getVertexAttribute(const std::string& name) const;
        //returns a pointer to the requested uniform if it exists, or nullptr if it does not
        Uniform* getUniform(const std::string& name, std::size_t hash) const;
        //returns a pointer to the uniform at the requested index if it exists, or nullptr if it does not
        Uniform* getUniform(UInt32 index) const;
        //returns the number of uniforms in the shader
        UInt32 getUniformCount() const;

        //sets a shader uniform with a given value, where u is a valid uniform
        void setUniform(const Uniform* u, float value);
        void setUniform(const Uniform* u, const float* data, UInt32 size, UInt32 stride = 1u);
        void setUniform(const Uniform* u, int value);
        void setUniform(const Uniform* u, const int* data, UInt32 size);
        void setUniform(const Uniform* u, const glm::vec2& vec2);
        void setUniform(const Uniform* u, const glm::vec2* data, UInt32 size);
        void setUniform(const Uniform* u, const glm::vec3& vec3);
        void setUniform(const Uniform* u, const glm::vec3* data, UInt32 size);
        void setUniform(const Uniform* u, const glm::vec4& vec4);
        void setUniform(const Uniform* u, const glm::vec4* data, UInt32 size);
        void setUniform(const Uniform* u, const Colour& colour);
        void setUniform(const Uniform* u, const Colour* data, UInt32 size);
        void setUniform(const Uniform* u, const glm::mat4& matrix);
        void setUniform(const Uniform* u, const glm::mat4* data, UInt32 size);
        void setUniform(const Uniform* u, const Texture::Sampler::Ptr& sampler);
        void setUniform(const Uniform* u, const std::vector<Texture::Sampler::Ptr>& samplers);

        //returns the name of the shader
        const std::string& getName() const;

        //binds the shader program
        void bind();
        //returns a pointer to the currently bound shader program
        static const Shader* getBoundShader();


    private:

        GLuint m_programID;
        std::string m_name;
        std::unordered_map<std::string, VertexAttributeID> m_vertexAttributes;
        mutable std::unordered_map<std::size_t, Uniform::Ptr> m_uniforms;
        static const Uniform::Ptr emptyUniform; //accessibility of ctor Key prevents this being anonymous

        std::hash<std::string> m_hashfunc;


        static Shader* linkProgram(const std::string& name, GLuint vertShaderID, GLuint fragShaderID, GLint geomShaderID = -1);
        static bool compileVertexShader(GLuint vertexShaderID, const GLchar** shaderSource);
        static bool compileFragmentShader(GLuint fragmentShaderID, const GLchar** shaderSource);
        static bool compileGeometryShader(GLuint geometryShaderID, const GLchar** shaderSource);
    };
}

#endif //CHUF_SHADER_HPP_