/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//responsible for culling, sorting and rendering scene drawables

#ifndef CHUF_SCENE_RENDERER_HPP_
#define CHUF_SCENE_RENDERER_HPP_

#include <chuf2/Colour.hpp>

#include <vector>
#include <functional>
#include <map>

namespace chuf
{
    class Drawable;
    class Scene;
    class App;
    class Camera;

    class SceneRenderer final
    {
    public:
        explicit SceneRenderer(Scene*);
        ~SceneRenderer() = default;

        SceneRenderer(const SceneRenderer&) = delete;
        SceneRenderer& operator= (SceneRenderer&) = delete;

        //adds a drawable to the list to be drawn
        void addDrawable(const Drawable*);
        //removes a drawable from the list to be drawn
        void removeDrawable(const Drawable*);

        //creates a render path of passes from given vector of pass
        //types. alpha passes are automatically included so should not
        //be passed to this function. Existing render paths are replaced
        void buildRenderPath(std::vector<RenderPass>&);

        void draw();

    private:
        std::vector<std::pair<const Drawable*, bool>> m_drawables; //TODO replace with BVH
        Scene* m_scene;
        Colour m_clearColour;

        using ScenePass = std::function<void(Camera*, App&)>;
        void reflectionPass(Camera*, App&);
        void refractionPass(Camera*, App&);
        void standardPass(Camera*, App&);
        void standardPassCached(Camera*, App&); //uses cached frustum results
        void debugPass(Camera*, App&);
        std::vector<ScenePass> m_scenePasses;

        static std::map<RenderPass, ScenePass> passFunctions;
    };
}

#endif //CHUF_SCENE_RENDERER_HPP_