/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//abc for creating drawable components which are attachable to nodes

#ifndef CHUF_DRAWABLE_HPP_
#define CHUF_DRAWABLE_HPP_

#include <chuf2/DataTypes.hpp>
#include <chuf2/Component.hpp>
#include <chuf2/BoundingBox.hpp>

namespace chuf
{
    class SceneRenderer;
    class Drawable : public Component
    {
    public:
        explicit Drawable(SceneRenderer*);
        virtual ~Drawable();

        virtual const BoundingBox& getBoundingBox() const = 0;
        //TODO would sphere be faster for frustum culling? Boxes are needed for BVH
        virtual UInt32 draw(UInt32 passFlags, bool wireframe = false) const = 0;

    protected:
        void setRenderer(SceneRenderer*);
        
    private:
        SceneRenderer* m_renderer;
    };
}

#endif //CHUF_DRAWABLE_HPP_