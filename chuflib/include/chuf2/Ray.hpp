/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#ifndef CHUF_RAY_HPP_
#define CHUF_RAY_HPP_

#include <chuf2/glm.hpp>

namespace chuf
{
    class Frustum;
    class Plane;
    class BoundingSphere;
    class BoundingBox;

    class Ray final
    {
    public:
        //constructs a ray with origin 0, 0, 0, and direction 0, 0, 1
        Ray();
        //constructs a ray with the given origin and direction
        Ray(const glm::vec3& origin, const glm::vec3& direction);
        Ray(const Ray&);
        ~Ray() = default;

        Ray& operator = (const Ray&);
        Ray& operator *= (const glm::mat4&);

        //gets the current origin of the ray
        const glm::vec3& getOrigin() const;
        //sets the origin of the ray
        void setOrigin(const glm::vec3&);

        //gets the current direction of the ray
        const glm::vec3& getDirection() const;
        //sets the direction of the ray
        void setDirection(const glm::vec3&);

        //returns the distance from the ray origin to the
        //intersection if it intersects, else return < 0
        float intersects(const BoundingBox&) const;

        //returns the distance from the ray origin to the
        //intersection if it intersects, else return < 0
        float intersects(const BoundingSphere&) const;

        //returns the distance from the ray origin to the
        //intersection if it intersects, else return < 0
        float intersects(const Plane&) const;

        //returns the distance from the ray origin to the
        //intersection if it intersects, else return < 0
        float intersects(const Frustum&) const;

        //transforms the ray by the given matrix
        void transform(const glm::mat4&);

    private:
        glm::vec3 m_origin;
        glm::vec3 m_direction;

        void normalise();
    };

    const Ray operator * (const glm::mat4&, const Ray&);
}

#endif //CHUF_RAY_HPP_