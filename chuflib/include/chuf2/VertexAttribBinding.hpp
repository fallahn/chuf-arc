/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//allows binding of vertex attributes (position, normal etc) to vertex shader attributes

#ifndef CHUF_VERT_BIND_HPP_
#define CHUF_VERT_BIND_HPP_

#include <chuf2/NonCopyable.hpp>
#include <chuf2/ClassKey.hpp>
#include <chuf2/DataTypes.hpp>

#include <memory>
#include <vector>

namespace chuf
{
    class Mesh;
    class Shader;
    class VertexLayout;

    class VertexAttribBinding final : private NonCopyable, private Locked
    {
    public:
        //vertex attributes are owned by their parent material / renderstate
        using Ptr = std::unique_ptr<VertexAttribBinding>;
        explicit VertexAttribBinding(const Key& k);
        ~VertexAttribBinding();

        static VertexAttribBinding* create(const std::shared_ptr<Mesh>& mesh, Shader* shader);
        static VertexAttribBinding* create(const VertexLayout& layout, void* vertexPtr, Shader* shader);

        //binds the stored vertex attribes to the currently bound shader
        void bind();
        //unbinds the store vertex attributes
        void unbind();

    private:
        //used to represent a CPU side vertex attribute when VAOs are unavaliable
        struct VertexAttrib
        {
            VertexAttrib()
                : enabled(false), size(4), type(GL_FLOAT), normalised(GL_FALSE), stride(0u), ptr(nullptr){}
            bool enabled;
            Int32 size;
            GLenum type;
            GLboolean normalised;
            UInt32 stride;
            void* ptr;
        };

        static VertexAttribBinding* create(const std::shared_ptr<Mesh>& mesh, const VertexLayout& layout, void* ptr, Shader* shader);
        void setVertAttribPointer(GLuint index, GLint size, GLenum type, GLboolean normalise, GLsizei stride, void* ptr);

        GLuint m_handle;
        std::shared_ptr<Mesh> m_mesh;
        Shader* m_shader;
        std::vector<VertexAttrib> m_attributes;
    };
}

#endif //CHUF_VERTEX_BIND_HPP_