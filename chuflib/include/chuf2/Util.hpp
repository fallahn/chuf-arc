/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//utility functions

#ifndef CHUF_UTIL_HPP_
#define CHUF_UTIL_HPP_

#include <al.h>
#include <alc.h>

#include <chuf2/glm/glm.hpp>
#include <chuf2/glm/gtx/quaternion.hpp>

#include <chuf2/GL/gl3w.h>

#include <iostream>
#include <cassert>
#include <string>
#include <sstream>

#include <chuf2/Log.hpp>
#include <chuf2/Rectangle.hpp>

namespace al
{
#ifndef _DEBUG_
#define alCheck(x) x;
#else

#define alCheck(x) x; al::errorCheck(__FILE__, __LINE__);

    static inline void errorCheck(const char* file, int line)
    {
        auto error = alGetError();
        if (error != AL_NO_ERROR)
        {
            std::string message;
            switch (error)
            {
            default: break;
            case AL_INVALID_ENUM:
                message = "AL_INVALID_ENUM: error in enumerated argument";
                break;
            case AL_INVALID_NAME:
                message = "AL_INVALID_NAME: invalid name specified";
                break;
            case AL_INVALID_OPERATION:
                message = "AL_INVALID_OPERATION: operation is not acceptable in current state";
                break;
            case AL_INVALID_VALUE:
                message = "AL_INVALID_VALUE: value is out of range";
                break;
            case AL_OUT_OF_MEMORY:
                message = "AL_OUT_OF_MEMORY: not enough memory left for current operation (too many sounds exist?)";
                break;
            }
            std::stringstream ss;
            ss << "open al error - " << message << " in " << file << " line " << line << std::endl;
            LOG(ss.str());
        }
    }

#endif //_DEBUG_
}

namespace gl
{
#ifndef _DEBUG_
#define glCheck(x) x;
#else
        
#define glCheck(x) x; gl::errorCheck(__FILE__, __LINE__);

    static inline void errorCheck(const char* file, int line)
    {
        auto error = glGetError();
        if (error)
        {
            std::string errorMessage = "unknown gl error";
            switch (error)
            {
            default: 
                errorMessage = "error code: " + std::to_string(error);
                break;
            case GL_INVALID_ENUM:
                errorMessage = "an invalid enum value was supplied";
                break;
            case GL_INVALID_VALUE:
                errorMessage = "numeric argument is out of range";
                break;
            case GL_INVALID_OPERATION:
                errorMessage = "specified operation is not valid in the current state";
                break;
            case GL_STACK_OVERFLOW:
                errorMessage = "detected potential stack overflow";
                break;
            case GL_STACK_UNDERFLOW:
                errorMessage = "detected potential sack underflow";
                break;
            case GL_OUT_OF_MEMORY:
                errorMessage = "not enough memory available for this command";
                break;
            }
            std::stringstream ss;
            ss << "gl Error: " << errorMessage << " in " << file << ", line " << line << std::endl;
            LOG(ss.str());
        }
        assert(error == GL_NO_ERROR);
    }
#endif //_DEBUG_
}

#include <random>
#include <ctime>
namespace
{
    const float PI = 3.14159265359f;
    const float degAsRad = PI / 180.f;
    const glm::mat3 identityMat3 = glm::mat3();
    const glm::mat4 identityMat4 = glm::mat4();

    std::default_random_engine rndEngine(static_cast<unsigned long>(std::time(0)));
}

namespace chuf
{
    namespace Util
    {
        static inline float degToRad(float deg)
        {
            return deg * degAsRad;
        }

        namespace Random
        {
            static inline float value(float begin, float end)
            {
                assert(begin < end);
                std::uniform_real_distribution<float> dist(begin, end);
                return dist(rndEngine);
            }
            static inline int value(int begin, int end)
            {
                assert(begin < end);
                std::uniform_int_distribution<int> dist(begin, end);
                return dist(rndEngine);
            }
        }

        namespace Vector
        {
            static inline float lengthSquared(const glm::vec2& vec)
            {
                return glm::dot(vec, vec);
            }

            static inline float lengthSquared(const glm::vec3& vec)
            {
                return glm::dot(vec, vec);
            }

            //returns true if vector is of zero length
            //ie all components are zero
            static inline bool zero(const glm::vec2& vec)
            {
                return (vec.x == 0.f && vec.y == 0.f);
            }

            //returns true if vector is of zero length
            //ie all components are zero
            static inline bool zero(const glm::vec3& vec)
            {
                return (vec.x == 0.f && vec.y == 0.f && vec.z == 0.f);
            }

            //returns true if all components are one
            static inline bool one(const glm::vec2& vec)
            {
                return (vec.x == 1.f && vec.y == 1.f);
            }

            //returns true if all components are one
            static inline bool one(const glm::vec3& vec)
            {
                return (vec.x == 1.f && vec.y == 1.f && vec.z == 1.f);
            }
        }

        namespace Matrix
        {
            //returns true if matrix is an identity matrix
            static inline bool identity(const glm::mat3& mat)
            {
                return (mat == identityMat3);
            }

            //returns true if matrix is an identity matrix
            static inline bool identity(const glm::mat4& mat)
            {
                return (mat == identityMat4);
            }

            static inline glm::vec3 getForwardVector(const glm::mat4& mat)
            {
                return glm::vec3(-mat[2][0], -mat[2][1], -mat[2][2]);
            }

            static inline glm::vec3 getBackVector(const glm::mat4& mat)
            {
                return glm::vec3(mat[2][0], mat[2][1], mat[2][2]);
            }

            static inline glm::vec3 getUpVector(const glm::mat4& mat)
            {
                return glm::vec3(mat[1][0], mat[1][1], mat[1][2]);
            }

            static inline glm::vec3 getDownVector(const glm::mat4& mat)
            {
                return glm::vec3(-mat[1][0], -mat[1][1], -mat[1][2]);
            }

            static inline glm::vec3 getLeftVector(const glm::mat4& mat)
            {
                return glm::vec3(-mat[0][0], -mat[0][1], -mat[0][2]);
            }

            static inline glm::vec3 getRightVector(const glm::mat4& mat)
            {
                return glm::vec3(mat[0][0], mat[0][1], mat[0][2]);
            }

            static inline glm::vec3 getScale(const glm::mat4& mat)
            {
                return glm::vec3
                    (
                    getRightVector(mat).length(),
                    getUpVector(mat).length(),
                    getBackVector(mat).length()
                    );
            }
        }

        namespace Quaternion
        {
            //returns true if quaternion is an identity quaternion
            static inline bool identity(const glm::quat& q)
            {
                return (q.x == 0.f && q.y == 0.f && q.z == 0.f && q.w == 1.f);
            }
        }
        namespace String
        {
            static inline void removeChar(std::string& line, const char c)
            {
                line.erase(std::remove(line.begin(), line.end(), c), line.end());
            }

            //replaces all instances of c with r
            static inline void replace(std::string& str, const char c, const char r)
            {
                auto start = 0u;
                auto next = str.find_first_of(c, start);
                while (next != std::string::npos && start < str.length())
                {
                    str.replace(next, 1, &r);
                    start = ++next;
                    next = str.find_first_of(c, start);
                    if (next > str.length()) next = std::string::npos;
                }
            }

            static inline std::string toLower(const std::string& str)
            {
                std::string retVal(str);
                std::transform(retVal.begin(), retVal.end(), retVal.begin(), ::tolower);
                return retVal;
            }

            static inline std::string getExtension(const std::string& str)
            {
                std::size_t position;
                if ((position = str.find_last_of('.')) != std::string::npos)
                {
                    std::string ext = str.substr(position);
                    if (ext.find('/') == std::string::npos &&
                        ext.find('\\') == std::string::npos)
                    {
                        return ext;
                    }
                    return "";
                }
                return "";
            }
        }

        namespace Math
        {
            /*template <typename T>
            static T clamp(T val, T min, T max)
            {
                return val < min ? min : (value > max ? max : val);
            }*/
        }

        namespace Rectangle
        {
            static inline FloatRect fromBounds(const glm::vec2& lower, const glm::vec2& upper)
            {
                return FloatRect(lower.x, lower.y, upper.x - lower.x, upper.y - lower.y);
            }
        }
    }
}

#endif //CHUF_UTIL_HPP_