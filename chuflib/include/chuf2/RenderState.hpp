/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//holds a specific set of GL states used when rendering
//and are applied to the gl state machine when bound.
//Material, Technique and Pass all inherit this class

#ifndef CHUF_RENDER_STATE_HPP_
#define CHUF_RENDER_STATE_HPP_

#include <chuf2/ClassKey.hpp>
#include <chuf2/NonCopyable.hpp>
#include <chuf2/DataTypes.hpp>

#include <chuf2/glm/glm.hpp>

#include <chuf2/GL/gl3w.h>

#include <memory>
#include <vector>


namespace chuf
{
    class MaterialProperty;
    class Pass;
    class Node;
    class RenderState : private NonCopyable, protected Locked
    {
        friend class App;
        friend class Pass;
        friend class Technique;
    public:
        enum class BlendFunc
        {
            ZERO					= GL_ZERO,
            ONE						= GL_ONE,
            SRC_COLOUR				= GL_SRC_COLOR,
            ONE_MINUS_SOURCE_COLOUR	= GL_ONE_MINUS_SRC_COLOR,
            DEST_COLOUR				= GL_DST_COLOR,
            ONE_MINUS_DEST_COLOUR	= GL_ONE_MINUS_DST_COLOR,
            SRC_ALPHA				= GL_SRC_ALPHA,
            ONE_MINUS_SRC_ALPHA		= GL_ONE_MINUS_SRC_ALPHA,
            DEST_ALPHA				= GL_DST_ALPHA,
            ONE_MINUS_DEST_ALPHA	= GL_ONE_MINUS_DST_ALPHA,
            CONST_ALPHA				= GL_CONSTANT_ALPHA,
            ONE_MINUS_CONST_ALPHA	= GL_ONE_MINUS_CONSTANT_ALPHA
        };

        enum class DepthFunc
        {
            NEVER		= GL_NEVER,
            LESS		= GL_LESS,
            EQUAL		= GL_EQUAL,
            LEQUAL		= GL_LEQUAL,
            GREATER		= GL_GREATER,
            NOTEQUAL	= GL_NOTEQUAL,
            GEQUAL		= GL_GEQUAL,
            ALWAYS		= GL_ALWAYS
        };

        enum class StencilFunc
        {
            NEVER		= GL_NEVER,
            ALWAYS		= GL_ALWAYS,
            LESS		= GL_LESS,
            LEQUAL		= GL_LEQUAL,
            EQUAL		= GL_EQUAL,
            GREATER		= GL_GREATER,
            GEQUAL		= GL_GEQUAL,
            NOTEQUAL	= GL_NOTEQUAL
        };

        enum class StencilOp
        {
            KEEP	= GL_KEEP,
            ZERO	= GL_ZERO,
            REPLACE = GL_REPLACE,
            INC		= GL_INCR,
            DEC		= GL_DECR,
            INVERT	= GL_INVERT,
            INC_WRAP = GL_INCR_WRAP,
            DEC_WRAP = GL_DECR_WRAP
        };

        /*enum class AutoBind
        {
            None,
            WorldMatrix,
            ViewMatrix,
            ProjectionMatrix,
            WorldViewMatrix,
            ViewProjectionMatrix,
            WorldViewProjectionMatrix,
            InverseTransposeWorldMatrix,
            InverseTransposeWorldViewMatrix,
            CameraWorldPosition,
            CameraViewPosition,
            SceneAmbientColour
        };*/

        enum StateOverride
        {
            BLEND			= (1 << 0),
            BLEND_FUNC		= (1 << 1),
            CULL_FACE		= (1 << 2),
            DEPTH_TEST		= (1 << 3),
            DEPTH_WRITE		= (1 << 4),
            DEPTH_FUNC		= (1 << 5),
            CULL_FACE_SIDE	= (1 << 6),
            STENCIL_TEST	= (1 << 7),
            STENCIL_WRITE	= (1 << 8),
            STENCIL_FUNC	= (1 << 9),
            STENCIL_OP		= (1 << 10),
            FRONT_FACE		= (1 << 11),
            CLIP_PLANE      = (1 << 12),
            ALL_ONE			= 0xffffffff
        };

        enum Winding
        {
            CLOCKWISE			= GL_CW,
            COUNTER_CLOCKWISE	= GL_CCW
        };

        enum CullFace
        {
            FRONT			= GL_FRONT,
            BACK			= GL_BACK,
            FRONT_AND_BACK	= GL_FRONT_AND_BACK
        };

        enum class ClipDistance
        {
            Zero  = GL_CLIP_DISTANCE0,
            One   = GL_CLIP_DISTANCE1,
            Two   = GL_CLIP_DISTANCE2,
            Three = GL_CLIP_DISTANCE4,
            Four  = GL_CLIP_DISTANCE4,
            Five  = GL_CLIP_DISTANCE5,
            Six   = GL_CLIP_DISTANCE6
        };

        //allows a RenderState to maintain its own set of states
        class StateBlock final : private NonCopyable, private Locked
        {
            friend class RenderState;
            friend class App;
            friend class FrameBuffer;
        public:
            using Ptr = std::unique_ptr<StateBlock>;
            StateBlock(const StateBlock::Key& key);
            ~StateBlock() = default;

            static Ptr create();
            Ptr clone();

            //enables or disables depth testing
            void setDepthTest(bool b);
            //enables or disables writing to the depth buffer
            void setDepthWrite(bool b);
            //sets the depth test function
            void setDepthFunc(DepthFunc func);

            //enables or disables blending
            void setBlend(bool b);
            //sets the blend source function
            void setBlendSrc(BlendFunc func);
            //sets the blend destination function
            void setBlendDest(BlendFunc func);

            //enables or disables stencil testing
            void setStencilTest(bool b);
            //enables or disables stencil buffer writing
            void setStencilWrite(UInt32 mask);
            //sts the stencil function
            void setStencilFunc(StencilFunc func, Int32 reference, UInt32 mask);
            //sets the operation to perform when
            void setStencilOp(StencilOp stencilFail, StencilOp depthFail, StencilOp pass);

            //sets the vertex winding direction
            void setFrontFace(Winding direction);

            //enables or diables face culling
            void setCullFace(bool b);
            //set which face side to cull if culling is enabled
            void setCullFaceSide(CullFace side);

            //enables plane clipping
            void setClip(bool b); //TODO enable more than one plane at once

            //binds this state block, applying its state to the gl state
            void bind();

        private:

            static StateBlock::Ptr m_defaultState;

            void bindNoRestore();
            static void restore(UInt64 overrideBits);
            static void enableDepthWrite();

            bool m_cullFaceEnabled;
            bool m_depthTestEnabled;
            bool m_depthWriteEnabled;
            bool m_blendEnabled;
            bool m_clipPlaneEnabled;
            
            DepthFunc m_depthFunc;
            BlendFunc m_blendSrc;
            BlendFunc m_blendDest;
            CullFace m_cullFace;
            Winding m_winding;
            ClipDistance m_clipDistance;

            bool m_stencilTestEnabled;
            UInt32 m_stencilWrite;
            StencilFunc m_stencilFunc;
            Int32 m_stencilFuncReference;
            UInt32 m_stencilFuncMask;
            StencilOp m_stencilFail;
            StencilOp m_depthFail;
            StencilOp m_stencilDepthPass;
            UInt64 m_overrideBits;
        };

        enum class PropertyBinding
        {
            None,
            WorldMatrix,
            ViewMatrix,
            ReflectionViewMatrix,
            ProjectionMatrix,
            WorldViewMatrix,
            ReflectionWorldViewMatrix,
            ViewProjectionMatrix,
            ReflectionViewProjectionMatrix,
            WorldViewProjectionMatrix,
            ReflectionWorldViewProjectionMatrix,
            InverseTransposeWorldMatrix,
            InverseTransposeWorldViewMatrix,
            CameraWorldPosition,
            CameraViewPosition,
            AmbientColour
        };

        explicit RenderState(const Key& key);
        virtual ~RenderState(); //DON'T try using default

        //gets a pointer to the material property with the given name
        //creates the property if it doesn't yet exist
        MaterialProperty* getProperty(const std::string& name) const;
        //gets the property at the given index and returns a pointer
        //to it, or nullptr of property doesn't exist
        MaterialProperty* getProperty(UInt32 index) const;
        //returns the number of material properties current created for this state
        UInt32 getPropertyCount() const;
        //adds an existing property and takes ownership of it
        void addProperty(std::unique_ptr<MaterialProperty>& prop);
        //removes a property with the given name, if it exists
        void removeProperty(const std::string& name);

        //performs an auto binding of one of the default functions to the property
        //with the given name
        void setPropertyBinding(const std::string& name, PropertyBinding binding);
        //performs an auto binding of one of the default functions to the propertty
        //with the given name, via a string representaion of the function as found in
        //a configuration file
        void setPropertyBinding(const std::string& name, const std::string& binding);

        //sets the render states current state block
        void setStateBlock(StateBlock::Ptr& stateblock);
        //returns a pointer to the current stateblock
        StateBlock* getStateBlock() const;
        //binds the material's parent node so that its matrix and vector functions
        //can be conveniently bound to state properties
        virtual void bindNode(Node* node);

    protected:
        RenderState();
        static void start();
        static void finish();
        void bind(Pass& pass);
        RenderState* getTopState(RenderState* below);
        void applyBinding(const std::string& uniformName, const std::string& binding);

    private:
        mutable std::vector<std::unique_ptr<MaterialProperty>> m_properties;
        Node* m_node;
        mutable StateBlock::Ptr m_stateBlock;
        RenderState* m_parent;

        //used for property auto bindings to currently bound node
        const glm::mat4& bindingGetWorldMatrix();
        const glm::mat4& bindingGetViewMatrix();
        const glm::mat4& bindingGetReflectionViewMatrix();
        const glm::mat4& bindingGetProjectionMatrix();
        const glm::mat4& bindingGetWorldViewMatrix();
        const glm::mat4& bindingGetReflectionWorldViewMatrix();
        const glm::mat4& bindingGetViewProjectionMatrix();
        const glm::mat4& bindingGetReflectionViewProjectionMatrix();
        const glm::mat4& bindingGetWorldViewProjectionMatrix();
        const glm::mat4& bindingGetReflectionWorldViewProjectionMatrix();
        const glm::mat4& bindingGetInverseTransposeWorldMatrix();
        const glm::mat4& bindingGetInverseTransposeWorldViewMatrix();
        glm::vec3 bindingGetCameraWorldPosition();
        glm::vec3 bindingGetCameraViewPosition();
        glm::vec3 bindingGetAmbientColour();
        const glm::vec3& bindingGetLightColour();
        glm::vec3 bindingGetLightDirection();
    };
}

#endif //CHUF_RENDER_STATE_HPP_
