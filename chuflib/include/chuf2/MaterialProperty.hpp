/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//represents a material property, usually a shader uniform
//materials carry their own properties so that their values
//can be sent to the material shader when it is bound for the
//current material. material properties only have a single value
//but can be of multiple types, and the current type is automatically
//updated when a new value is set.

#ifndef CHUF_MATERIAL_PROPERTY_HPP_
#define CHUF_MATERIAL_PROPERTY_HPP_

#include <chuf2/NonCopyable.hpp>
#include <chuf2/DataTypes.hpp>
#include <chuf2/Colour.hpp>
#include <chuf2/Shader.hpp>
#include <chuf2/Texture.hpp>

#include <chuf2/glm/glm.hpp>

#include <string>
#include <memory>
#include <functional>

namespace chuf
{
    class MaterialProperty final : private NonCopyable
    {
        friend class RenderState;
    public:
        using Ptr = std::unique_ptr<MaterialProperty>;
        const std::string& getName() const;

        void setValue(float value);
        void setValue(Int32 value);
        void setValue(const float* values, UInt32 size);
        void setValue(const Int32* values, UInt32 size);
        void setValue(const glm::vec2& value);
        void setValue(const glm::vec2* values, UInt32 size);
        void setValue(const glm::vec3& value);
        void setValue(const glm::vec3* values, UInt32 size);
        void setValue(const glm::vec4& value);
        void setValue(const glm::vec4* values, UInt32 size);
        void setValue(const Colour& colour);
        void setValue(const Colour* colours, UInt32 size);
        void setValue(const glm::mat3& matrix);
        void setValue(const glm::mat3* matrices, UInt32 size);
        void setValue(const glm::mat3x4& matrix);
        void setValue(const glm::mat3x4* matrices, UInt32 size);
        void setValue(const glm::mat4& matrix);
        void setValue(const glm::mat4* matrices, UInt32 size);
        
        void setValue(const Texture::Sampler::Ptr& sampler);
        Texture::Sampler::Ptr setValue(const std::string& path, bool createMipmaps = false);
        Texture::Sampler::Ptr getSampler() const;
        void setSampler(const Texture::Sampler::Ptr& sampler);
        Texture::Sampler::Ptr setSampler(const std::string& path, bool createMipmaps = false);
        //warning this takes ownership of the vector!
        void setValue(std::vector<Texture::Sampler::Ptr>& samplerArray);

        //used to bind functions which return constantly changing values, such as matrices
        //or colours to properties in the material.
        template <class T>
        void bindValue(std::function<T()>& function)
        {
            clearValue();
            m_functionBinding.reset(new FunctionValueBinding<T>(this, function));
            m_type = Type::Binding;
        }

        explicit MaterialProperty(const std::string& name);
        ~MaterialProperty();

    private:
        class FunctionBinding : private NonCopyable
        {
        public:
            explicit FunctionBinding(MaterialProperty* matProp)
                : m_materialProperty(matProp){}
            virtual ~FunctionBinding() = default;
            virtual void setValue(Shader* shader) = 0;
        protected:
            const MaterialProperty* materialProperty() const
            {
                return m_materialProperty;
            }
        private:
            MaterialProperty* m_materialProperty;
        };

        template <class T>
        class FunctionValueBinding : public FunctionBinding
        {
        public:
            FunctionValueBinding(MaterialProperty* matProp, const std::function<T()>& function)
                : FunctionBinding(matProp), m_function(function){}
            void setValue(Shader* shader)
            {
                shader->setUniform(materialProperty()->m_uniform, m_function());
            }
        private:
            std::function<T()> m_function;
        };

        enum class Type
        {
            None,
            Float,
            FloatArray,
            Int,
            IntArray,
            Vec2,
            Vec3,
            Vec4,
            Colour,
            Matrix3,
            Matrix3x4,
            Matrix4,
            Sampler,
            SamplerArray,
            Binding //if we bind a function to this property
        } m_type;

        union
        {
            float floatVal;
            float* floatArray;
            int intValue;
            int* intArray;
        } m_value;

        void clearValue();
        void bind(Shader* shader);

        UInt32 m_size;
        std::unique_ptr<float[]> m_dynamicArray; //stores matrices as an array. why not a vector?
        std::string m_name;
        std::size_t m_hash;
        Shader::Uniform* m_uniform;
        std::unique_ptr<FunctionBinding> m_functionBinding;
        Texture::Sampler::Ptr m_textureSampler;
        std::vector<Texture::Sampler::Ptr> m_samplerArray;
    };
}

#endif //CHUF_MATERIAL_PROPERTY_HPP_