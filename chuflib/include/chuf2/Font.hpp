/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//class for loading fonts via freetype, ready to load into a text instance

#ifndef CHUF_FONT_HPP_
#define CHUF_FONT_HPP_

#include <chuf2/NonCopyable.hpp>
#include <chuf2/ClassKey.hpp>
#include <chuf2/Rectangle.hpp>
#include <chuf2/DataTypes.hpp>

#include <string>
#include <vector>
#include <map>
#include <memory>

#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_BITMAP_H

namespace chuf
{
    class Texture;
    class Font final : private NonCopyable, private Locked
    {
    public:
        using Ptr = std::unique_ptr<Font>;

        explicit Font(const Key& key);
        ~Font() = default;

        static Font* create(const std::string& path);
        //returns a pointer to the texture atlas created
        //for this font
        Texture* getAtlas() const;
        //returns the name of this font
        const std::string& getName() const;
        //requests the sub rectangle of the atlas which
        //corresponds to the given char - TODO update this
        //for unicode
        const FloatRect& getChar(char c) const;

    private:

        Texture* m_texture;
        std::map<char, FloatRect> m_subRectangles;
        std::string m_name;

        static std::vector<Ptr> fontCache;

        struct GlyphData
        {
            std::unique_ptr<UInt8[]> data;
            UInt16 width, height;
        };

        //just wrap the freetype bits so we automatically
        //free on error
        struct FT_RAII
        {
            FT_Library flib;
            FT_Face face;
            FT_RAII() : flib(nullptr), face(nullptr){}
            ~FT_RAII()
            {
                if (face)
                    FT_Done_Face(face);
                if (flib)
                    FT_Done_FreeType(flib);
            }
        };
    };
}

#endif //CHUF_FONT_HPP_