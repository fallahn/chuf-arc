/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#ifndef CHUF_AUDIO_SOURCE_HPP_
#define CHUF_AUDIO_SOURCE_HPP_

#include <chuf2/AudioResource.hpp>
#include <chuf2/DataTypes.hpp>

#include <chuf2/glm/vec3.hpp>

namespace chuf
{
    class AudioSource : private AudioResource
    {
    public:
        enum class State
        {
            Playing,
            Paused,
            Stopped
        };

        AudioSource(const AudioSource& other);
        virtual ~AudioSource();

        //set the pitch where 1 is default
        void setPitch(float pitch);
        float getPitch() const;
        //how quickly the sound volume diminishes based
        //on its distance from the listener. 0 will never
        //diminish, 100 will diminish quickly with distance.
        //default value is 1
        void setAttenuation(float att);
        float getAttenuation() const;
        //set the three dimensional position of the sound
        void setPosition(const glm::vec3& position);
        glm::vec3 getPosition() const;
        //set the volume of the sound
        void setVolume(float volume);
        float getVolume() const;
        //sets whether or not the sound position is relative to
        //the world or the listener. Listener relative sounds 
        //will follow the listener position in the world
        void setRelativeToListener(bool relative);
        bool relativeToListener() const;
        //distance at which the sound maintains full volume
        //before the volume starts to attenuate according to
        //the curent attenuation value
        void setMinDistance(float distance);
        float getMinDistance() const;

    protected:
        AudioSource();

        State getState() const;
        UInt32 m_alId;

    private:

    };
}

#endif //CHUF_AUDIO_SOURCE_HPP_