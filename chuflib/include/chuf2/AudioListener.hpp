/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//position in the world at which the audio is heard. generally this
//would be attached to the same node as the active camera.

#ifndef CHUF_LISTENER_HPP_
#define CHUF_LISTENER_HPP_

#include <chuf2/glm/vec3.hpp>
#include <chuf2/Camera.hpp>

namespace chuf
{
    class AudioListener final
    {
    public:
        AudioListener();
        ~AudioListener();

        //sets the listener position
        static void setPosition(const glm::vec3& position);
        //returns the current listener position
        static glm::vec3 getPosition();
        //sets the listener up vector
        static void setUpVector(const glm::vec3& upVec);
        //returns the current listener up vector
        static glm::vec3 getUpVector();
        //sets the direction which the listener is facing
        static void setDirection(const glm::vec3& direction);
        //returns the current direction the listener is facing
        static glm::vec3 getDirection();
        //sets the global volume for all audio form 0 - 100.0
        static void setVolume(float vol);
        //returns the current global audio volume
        static float getVolume();
        //returns the openAL format which matches channel count
        static int getFormatFromChannelCount(unsigned channelCount);

        //add this to the scene's active camera
        //to update the audio listener's position in the world
        class CameraListener final : public Camera::Listener
        {
        public:
            void cameraChanged(Camera* camera) override;
        };

    private:
        static void orientate();
    };
}

#endif //CHUF_LISTENER_HPP_