/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//allows a texture to be attached to a frame buffer as a render target
//TODO look at cubemap support

#ifndef CHUF_RENDER_TEXTURE_HPP_
#define CHUF_RENDER_TEXTURE_HPP_

#include <chuf2/NonCopyable.hpp>
#include <chuf2/ClassKey.hpp>
#include <chuf2/DataTypes.hpp>

#include <memory>
#include <string>

namespace chuf
{
    class Texture;
    class RenderTexture final : private NonCopyable, public Locked
    {
    public:
        using Ptr = std::unique_ptr<RenderTexture>;

        RenderTexture(const std::string& name, const Key& key);
        ~RenderTexture() = default;

        //creates an RGBA render texture and adds it to the cache. NOTE if a render texture
        //with the name already exists then the cached version will be returned, which may
        //not be the intended behaviour (for example it may have different dimensions)
        static RenderTexture* create(const std::string& name, UInt16 width, UInt16 height);
        //tries to create a render texture from the given texture. may fail on some hardware
        //if the given texture type is not supported, in which case nullptr is returned.
        //NOTE a cached texture with the same name will be returned if it exists, which may not
        //be the expected result if the cached texture is a different size or format
        static RenderTexture* create(const std::string& name, Texture* t);
        //returns a render texture from the cache if found, else returns nullptr
        static RenderTexture* getRenderTexture(const std::string& name);

        //returns the render texture's name
        const std::string& getName() const;
        //returns the underlying texture so it can be bound to a texture sampler for example
        Texture* getTexture() const;
        //returns the width of the render texture
        UInt16 getWidth() const;
        //returns the height of the render texture
        UInt16 getHeight() const;


    private:

        std::string m_name;
        Texture* m_texture;
    };
}

#endif //CHUF_RENDER_TEXTURE_HPP_