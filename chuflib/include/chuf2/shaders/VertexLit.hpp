/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#ifndef CHUF_SHADER_VERTEXLIT_HPP_
#define CHUF_SHADER_VERTEXLIT_HPP_


#include <string>

namespace Shaders
{
	namespace VertexLit
	{

		static const std::string vertex =
			"in vec4 a_position;\n" \
			"in vec3 a_normal;\n" \
			"in vec2 a_texCoord0;\n" \
			"\n" \
			"#if defined(LIGHTMAPPED)\n" \
			"in vec2 a_texCoord1;\n" \
			"#endif //LIGHTMAPPED\n" \
			"\n" \
			"#if defined(BUMP)\n" \
			"in vec3 a_tangent;\n" \
			"in vec3 a_bitangent;\n" \
			"#endif //BUMP\n" \
			"\n" \
			"#if defined(SKINNING)\n" \
			"in vec4 a_boneIndices;\n" \
			"in vec4 a_boneWeights;\n" \
			"#endif //SKINNING\n" \
			"\n" \
			"#if defined(CLIP_PLANE)\n" \
			"uniform mat4 u_worldMatrix;\n" \
			"uniform mat4 u_inverseTransposeWorldMatrix;\n" \
			"uniform vec4 u_clipPlane;\n" \
			"#endif //CLIP_PLANE\n" \
			"\n" \
			"#if defined(REFLECTION)\n" \
			"uniform mat4 u_reflectionWorldViewProjectionMatrix;\n" \
			"#else\n" \
			"uniform mat4 u_worldViewProjectionMatrix;\n" \
			"#endif //REFLECTION\n" \
			"uniform mat4 u_inverseTransposeWorldViewMatrix;\n" \
			"\n" \
			"uniform vec3 u_lightDirection;\n" \
			"\n" \
			"#if defined(BUMP)\n" \
			"uniform mat4 u_worldViewMatrix;\n" \
			"uniform mat4 u_viewMatrix;\n" \
			"uniform vec3 u_cameraWorldPosition;\n" \
			"#endif //BUMP\n" \
			"\n" \
			"#if defined(SKINNING)\n" \
			"uniform mat4[80] u_boneMatrices; //TODO set size appropriately?\n" \
			"#endif //SKINNING\n" \
			"\n" \
			"#if !defined(BUMP)\n" \
			"out vec3 v_normalVector;\n" \
			"#endif //BUMP\n" \
			"\n" \
			"out vec2 v_texCoord0;\n" \
			"#if defined(LIGHTMAPPED)\n" \
			"out vec2 v_texCoord1;\n" \
			"#endif //LIGHTMAPPED\n" \
			"out vec3 v_lightDirection;\n" \
			"\n" \
			"#if defined(BUMP)\n" \
			"out vec3 v_cameraPosition;\n" \
			"#endif //BUMP\n" \
			"\n" \
			"void main()\n" \
			"{\n" \
			"	vec4 position = a_position;\n" \
			"\n" \
			"#if defined(SKINNING)\n" \
			"	mat4 skinMatrix = u_boneMatrices[int(a_boneIndices.x)] * a_boneWeights.x;\n" \
			"	skinMatrix += u_boneMatrices[int(a_boneIndices.y)] * a_boneWeights.y;\n" \
			"	skinMatrix += u_boneMatrices[int(a_boneIndices.z)] * a_boneWeights.z;\n" \
			"	skinMatrix += u_boneMatrices[int(a_boneIndices.w)] * a_boneWeights.w;\n" \
			"	position = vec4((skinMatrix * position).xyz, a_position.w);\n" \
			"#endif //SKINNING\n" \
			"\n" \
			"#if defined(REFLECTION)	\n" \
			"	gl_Position = u_reflectionWorldViewProjectionMatrix * position;\n" \
			"#else\n" \
			"	gl_Position = u_worldViewProjectionMatrix * position;\n" \
			"#endif //REFLECTION\n" \
			"	v_texCoord0 = a_texCoord0;\n" \
			"#if defined(LIGHTMAPPED)\n" \
			"	v_texCoord1 = a_texCoord1;\n" \
			"#endif //LIGHTMAPPED\n" \
			"	\n" \
			"	mat3 inverseTransposeWorldViewMatrix = mat3(u_inverseTransposeWorldViewMatrix[0].xyz, u_inverseTransposeWorldViewMatrix[1].xyz, u_inverseTransposeWorldViewMatrix[2].xyz);\n" \
			"	vec3 normalVector = normalize(inverseTransposeWorldViewMatrix * a_normal);\n" \
			"	\n" \
			"	\n" \
			"#if defined(BUMP)\n" \
			"	//TODO all positions need to be pre-multiplied with skinning matrix if needed\n" \
			"	vec3 tangent = normalize(inverseTransposeWorldViewMatrix * a_tangent); \n" \
			"	vec3 bitangent = normalize(inverseTransposeWorldViewMatrix * a_bitangent);\n" \
			"	mat3 tangentSpaceTransformMatrix = mat3(tangent.x, bitangent.x, normalVector.x, tangent.y, bitangent.y, normalVector.y, tangent.z, bitangent.z, normalVector.z);\n" \
			"	\n" \
			"	v_lightDirection = tangentSpaceTransformMatrix * u_lightDirection;//(u_viewMatrix * vec4(u_lightDirection, 1.0)).xyz;\n" \
			"	\n" \
			"	vec4 worldViewPosition = u_worldViewMatrix * a_position;\n" \
			"	v_cameraPosition = tangentSpaceTransformMatrix * (u_worldViewMatrix * vec4(u_cameraWorldPosition, 1.0) - worldViewPosition).xyz;\n" \
			"	\n" \
			"#else\n" \
			"	v_lightDirection = u_lightDirection;\n" \
			"	v_normalVector = normalVector;\n" \
			"#endif //BUMP\n" \
			"	\n" \
			"#if defined(CLIP_PLANE)\n" \
			"	gl_ClipDistance[0] = dot(u_worldMatrix * a_position, u_clipPlane);\n" \
			"#endif //CLIP_PLANE	\n" \
			"	\n" \
			"}\n";


		static const std::string fragment =
			"uniform sampler2D u_diffuseTexture;\n" \
			"\n" \
			"#if defined(BUMP)\n" \
			"uniform sampler2D u_normalTexture;\n" \
			"#if defined(MASK)\n" \
			"uniform sampler2D u_maskTexture;\n" \
			"#endif //MASK\n" \
			"#endif //BUMP\n" \
			"\n" \
			"#if defined(LIGHTMAPPED)\n" \
			"uniform sampler2D u_lightmapTexture;\n" \
			"#endif //LIGHTMAPPED\n" \
			"\n" \
			"uniform vec3 u_lightColour = vec3(1.0, 1.0, 1.0);\n" \
			"uniform vec3 u_ambientColour = vec3(0.2, 0.2, 0.1);\n" \
			"\n" \
			"#if !defined(BUMP)\n" \
			"in vec3 v_normalVector;\n" \
			"#endif //BUMP\n" \
			"\n" \
			"in vec2 v_texCoord0;\n" \
			"#if defined(LIGHTMAPPED)\n" \
			"in vec2 v_texCoord1;\n" \
			"#endif //LIGHTMAPPED\n" \
			"in vec3 v_lightDirection;\n" \
			"\n" \
			"#if defined(BUMP)\n" \
			"in vec3 v_cameraPosition;\n" \
			"#endif //BUMP\n" \
			"\n" \
			"out vec4 outColour;\n" \
			"\n" \
			"void main()\n" \
			"{\n" \
			"	vec4 diffuseColour = texture2D(u_diffuseTexture, v_texCoord0);\n" \
			"	vec3 baseColour = diffuseColour.rgb;\n" \
			"	\n" \
			"#if defined (BUMP)\n" \
			"	vec3 normalVector = normalize(texture2D(u_normalTexture, v_texCoord0).rgb * 2.0 - 1.0);\n" \
			"#else\n" \
			"	vec3 normalVector = v_normalVector;\n" \
			"#endif //BUMP\n" \
			"\n" \
			"#if defined(LIGHTMAPPED)\n" \
			"	\n" \
			"	baseColour *= texture2D(u_lightmapTexture, v_texCoord1).rgb;\n" \
			"#endif //LIGHTMAPPED\n" \
			"\n" \
			"	vec3 lightDirection = normalize(v_lightDirection * 2.0);\n" \
			"	vec3 blendedColour = baseColour * u_ambientColour;\n" \
			"	float diffuseAmount = max(dot(normalVector, -lightDirection), 0.0);\n" \
			"	blendedColour += u_lightColour * baseColour * diffuseAmount;\n" \
			"	\n" \
			"#if defined(BUMP)\n" \
			"	//calc specular\n" \
			"	vec3 eyeVec = normalize(v_cameraPosition);\n" \
			"	vec3 specularAngle = reflect(lightDirection, normalVector);//normalize(normalVector * diffuseAmount * 2.0 - lightDirection);\n" \
			"	\n" \
			"#if defined(MASK)\n" \
			"	vec4 mask = texture2D(u_maskTexture, v_texCoord0);\n" \
			"	float exp = 255.0 * mask.r;\n" \
			"	float specAmount = mask.g;\n" \
			"#else\n" \
			"	const float exp = 255.0;\n" \
			"	const float specAmount = 0.5;\n" \
			"	\n" \
			"#endif //MASK\n" \
			"	vec3 specularColour = vec3(pow(clamp(dot(specularAngle, eyeVec), 0.0, 1.0), exp));\n" \
			"	blendedColour += (specularColour * specAmount);\n" \
			"#endif //BUMP	\n" \
			"	outColour = vec4(blendedColour, diffuseColour.a);\n" \
			"}\n";


	}
}

#endif //CHUF_SHADER_HPP_
