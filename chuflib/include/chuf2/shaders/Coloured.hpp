/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#ifndef CHUF_SHADER_COLOURED_HPP_
#define CHUF_SHADER_COLOURED_HPP_


#include <string>

namespace Shaders
{
	namespace Coloured
	{

		static const std::string vertex =
			"in vec3 a_position;\n" \
			"in vec3 a_normal;\n" \
			"//in vec2 a_texCoord0;\n" \
			"\n" \
			"uniform mat4 u_worldViewProjectionMatrix;\n" \
			"uniform mat4 u_inverseTransposeWorldViewMatrix;\n" \
			"\n" \
			"out vec3 v_normalVector;\n" \
			"//out vec2 v_texCoord;\n" \
			"\n" \
			"void main()\n" \
			"{\n" \
			"	gl_Position = u_worldViewProjectionMatrix * vec4(a_position, 1.0);\n" \
			"	mat3 inverseTransposeWorldViewMatrix = mat3(u_inverseTransposeWorldViewMatrix[0].xyz, u_inverseTransposeWorldViewMatrix[1].xyz, u_inverseTransposeWorldViewMatrix[2].xyz);\n" \
			"	v_normalVector = inverseTransposeWorldViewMatrix * a_normal;\n" \
			"	//v_texCoord = a_texCoord0;\n" \
			"}\n";


		static const std::string fragment =
			"//#version 330\n" \
			"\n" \
			"in vec3 v_normalVector;\n" \
			"//in vec2 v_texCoord;\n" \
			"\n" \
			"//uniform sampler2D u_diffuseTexture;\n" \
			"\n" \
			"uniform vec3 u_lightDirection;// = normalize(vec3(0.8, -1.0, 0.2));\n" \
			"uniform vec3 u_lightColour = vec3(1.0, 1.0, 1.0);\n" \
			"const vec3 baseColour = vec3(1.0, 0.5, 0.0);\n" \
			"\n" \
			"out vec4 outColour;\n" \
			"\n" \
			"void main()\n" \
			"{\n" \
			"	//vec3 baseColour = texture2D(u_diffuseTexture, v_texCoord).rgb;\n" \
			"	vec3 normalVector = normalize(v_normalVector);\n" \
			"	vec3 blendedColour = baseColour * 0.25; //reduce to ambient colour\n" \
			"	float diffuseAmount = max(dot(normalVector, -u_lightDirection), 0.0);\n" \
			"	blendedColour += u_lightColour * baseColour * diffuseAmount;\n" \
			"	outColour = vec4(blendedColour, 1.0);\n" \
			"}\n";


	}
}

#endif //CHUF_SHADER_HPP_
