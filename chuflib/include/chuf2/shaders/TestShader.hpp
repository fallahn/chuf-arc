/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#ifndef CHUF_TEST_SHADER_HPP_
#define CHUF_TEST_SHADER_HPP_

#include <string>

static const std::string vertShader =
"attribute vec3 a_position;"
"attribute vec3 a_normal;"
"uniform mat4 u_worldViewProjectionMatrix;"
"uniform mat4 u_inverseTransposeWorldViewMatrix;"
"varying vec3 v_normalVector;"
"void main(){"
"gl_Position = u_worldViewProjectionMatrix * vec4(a_position, 1.0);"
"mat3 inverseTransposeWorldViewMatrix = mat3(u_inverseTransposeWorldViewMatrix[0].xyz, u_inverseTransposeWorldViewMatrix[1].xyz, u_inverseTransposeWorldViewMatrix[2].xyz);"
"v_normalVector = inverseTransposeWorldViewMatrix * a_normal;"
"}";

static const std::string fragShader =
"varying vec3 v_normalVector;"

"const vec3 lightDirection = normalize(vec3(0.0, -1.0, 0.2));"
"const vec3 lightColour = vec3(1.0, 1.0, 1.0);"
"const vec3 baseColour = vec3(1.0, 0.5, 0.0);"

"void main(){"
"vec3 normalVector = normalize(v_normalVector);"
"vec3 blendedColour = baseColour * 0.4;" //reduce to ambient colour
"float diffuseAmount = max(dot(normalVector, -lightDirection), 0.0);"
"blendedColour += lightColour * baseColour * diffuseAmount;"
"gl_FragColor = vec4(blendedColour, 1.0);"
"}";

#endif //CHUF_TEST_SHADER_HPP_