/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//provides a depth / stencil target which can be attached to a frame buffer

#ifndef CHUF_DEPTH_STENCIL_HPP_
#define CHUF_DEPTH_STENCIL_HPP_

#include <chuf2/NonCopyable.hpp>
#include <chuf2/ClassKey.hpp>
#include <chuf2/DataTypes.hpp>

#include <memory>
#include <string>

namespace chuf
{
    class DepthStencilTarget final : private NonCopyable, public Locked
    {
        friend class FrameBuffer;
    public:
        using Ptr = std::unique_ptr<DepthStencilTarget>;
        enum class Format
        {
            Depth,
            DepthStencil
        };

        DepthStencilTarget(const std::string& name, Format f, UInt16 width, UInt16 height, const Key& key);
        ~DepthStencilTarget();

        //creates a depth / stencil target and adds it to the cache, or returns an existing target if 
        //it has the same name - note it may not necessarily have the same dimensions
        static DepthStencilTarget* create(const std::string& name, Format f, UInt16 width, UInt16 height);
        //searches the cache for target with given name, returns nullptr if not found
        static DepthStencilTarget* getTarget(const std::string& name);


        //returns the name of the dst
        const std::string& getName() const;
        //returns the format of the dst
        Format getFormat() const;
        //returns the width of the target
        UInt16 getWidth() const;
        //returns the height of the target
        UInt16 getHeight() const;
        //returns true if depth and stencil buffers are packed
        bool packed() const;

    private:

        std::string m_name;
        Format m_format;
        RenderBufferID m_depthID;
        RenderBufferID m_stencilID;
        UInt16 m_width;
        UInt16 m_height;
        bool m_packed;
    };
}
#endif //CHUF_DEPTH_STENCIL_HPP_