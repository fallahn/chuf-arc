/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//template class to allow caching opengl objects such as textures and buffers
//allowing efficient reuse of resources, as well as providing ownership
//for resources allocated via std::unique_ptr

#ifndef CHUF_CACHES_HPP_
#define CHUF_CACHES_HPP_

#include <chuf2/NonCopyable.hpp>

#include <memory>
#include <string>
#include <map>
#include <vector>
#include <cassert>

namespace chuf
{
    class BaseCache
    {
    public:
        virtual ~BaseCache() = default;
        //clears all caches, used to force cleanup of cached objects
        static void flushAll();

    protected:
        virtual void clear() = 0;
        static std::vector<BaseCache*> cacheList;
    };
    
    template <class T>
    class Cache final : public BaseCache, private NonCopyable
    {
    public:
        Cache()
        {
            //register this with the cache list so 
            //it is included in a cache-wide clearance
            cacheList.push_back(this);
        }
        ~Cache() = default;

        //insert an object into the cache, and return a raw pointer to it
        T* insert(const std::string& name, std::unique_ptr<T>& item)
        {
            //check if it exists first
            T* result = find(name);
            if (result) return result;

            m_cache.insert(std::make_pair(name, std::move(item)));
            return m_cache[name].get();
        }
        //removes an object from the cache by name
        //WARNING this will invalidate any pointers referencing this item
        void remove(const std::string& name)
        {
            auto result = m_cache.find(name);
            assert(result != m_cache.end());
            m_cache.erase(result);
        }
        //checks the cache for an item with given name and returns a pointer to it
        //or nullptr if item not found
        T* find(const std::string& name) const
        {
            auto result = m_cache.find(name);
            if (result != m_cache.end()) return result->second.get();
            return nullptr;
        }

    private:
        //clears all items from this cache
        void clear() override
        {
            m_cache.clear();
        }

        std::map<std::string, std::unique_ptr<T>> m_cache;      
    };


}
#endif //CHUF_CACHES_HPP_