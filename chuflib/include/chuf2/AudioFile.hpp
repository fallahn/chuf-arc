/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//reads an audio file from disk

#ifndef CHUF_AUDIO_FILE_HPP_
#define CHUF_AUDIO_FILE_HPP_

#include <chuf2/NonCopyable.hpp>
#include <chuf2/DataTypes.hpp>

#include <sndfile.h>

#include <string>

namespace chuf
{
    class AudioFile final : private NonCopyable
    {
    public:
        AudioFile();
        ~AudioFile();

        //open a file for reading from disk;
        bool open(const std::string& path);
        //open a file for reading from an array of data
        bool open(const void* data, UInt32 size);
        //read samples from an open file
        UInt32 read(Int16* data, UInt32 sampleCount);
        //change the current read position
        void seek(float offset);

        //returns the number of samples in the current file
        UInt32 getSampleCount() const;
        //returns the sample rate of the current file
        UInt32 getSampleRate() const;
        //returns the number of channels of the current file
        UInt32 getChannelCount() const;


    private:
        SNDFILE* m_file;
        UInt32 m_sampleCount;
        UInt32 m_sampleRate;
        UInt32 m_channelCount;

        struct Memory
        {
            const char* begin;
            const char* current;
            sf_count_t size;

            static sf_count_t getLength(void* usr);
            static sf_count_t read(void* ptr, sf_count_t count, void* usr);
            static sf_count_t seek(sf_count_t offset, int when, void* usr);
            static sf_count_t tell(void* usr);
        } m_memory;

        void init(SF_INFO fileInfo);
        static Int32 getFormatFromExtension(const std::string& filename);
    };
}

#endif //CHUF_AUDIO_FILE_HPP_