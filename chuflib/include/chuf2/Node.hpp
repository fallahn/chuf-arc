/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//represents a single node in the scene graph hierarchy.
//nodes help employ a component entity approach by allowing
//models, physics, ai, sound and other modules to be attached
//and manipulated within 3D space.

#ifndef CHUF_NODE_HPP_
#define CHUF_NODE_HPP_

#include <chuf2/Transform.hpp>
#include <chuf2/NonCopyable.hpp>
#include <chuf2/ClassKey.hpp>
#include <chuf2/BoundingSphere.hpp>
#include <chuf2/BoundingBox.hpp>
#include <chuf2/Component.hpp>

#include <memory>
#include <unordered_map>
#include <exception>
#include <typeindex>

namespace chuf
{
	class Scene;
	class Light;
	class Camera;
	class Model;
	class Sprite;
    class BspMap;
    class Terrain;
    class Node : public Transform, public Taggable, private NonCopyable, private Locked
	{
		friend class Scene;
	public:
        using Ptr = std::unique_ptr<Node>;

        Node(const std::string& name, const Key& key);
        ~Node();

		//creates a node with the given name. nodes
		//belong to their parent, or the scene if they are a root node
		static Ptr create(const std::string& name);
		//returns the node's name
		const std::string& getName() const;
		//sets the node's name
		void setName(const std::string& name);
		//takes ownership of the given node and adds
		//it to its vector of children
		void addChild(Ptr& child);
		//removes the given child from this nodes children list
		//and returns it so ownership can be passed to a new node
		//if needed
		Ptr removeChild(Node* child);
		//removes and destroys all children owned by this node
		void removeAllChildren();
		//returns a reference to the vector of children owned by
		//this node.
		const std::vector<Ptr>& getChildren() const;
		//returns a reference to this node's parent - TODO this should be a pointer
		//so that nullptr can be return if no parent is found.
		Node& getParent();

		//sets the node active in the hierachy (TODO: applies to physics)
		void setActive(bool active = true);
		bool active() const;
		bool activeInHierarchy() const;

		//returns the number of children belonging to this node
		UInt32 getChildCount() const;

		//finds a node with given name, returns a pointer to it if it is found, else returns nullptr
		//optionally search all its children for a match, true by default
		Node* findNode(const std::string& name, bool recursive = true) const;
		//searches for multiple nodes with the same name and places a pointer to the in the given
		//vector. optionally search hcild nodes recursively, true by default. returns the number
		//of matches found (ie the size of the vector once complete)
		UInt32 findNodes(const std::string& name, std::vector<Node*>& dest, bool recursive = true) const;
		//returns a reference to the root node in this node's hierarchy
		Node& getRootNode();
		//returns a pointer to this node's first child, or nullptr if
		//the node has no children.
		Node* getFirstChild() const;
        //get the bounding sphere which approximately bounds this node, its children,
        //any meshes attached and light range
        const BoundingSphere& getBoundingSphere();
        //get the bounding box which approximately surrounds this node, its children,
        //and any meshes
        const BoundingBox& getBoundingBox();

		//get the node's world matrix
		const glm::mat4& getWorldMatrix();
		//get the node's world view matrix based on the scene's active camera
		const glm::mat4& getWorldViewMatrix();
        //get the node's reflected world view matrix based on the scene's active camera
        const glm::mat4& getReflectionWorldViewMatrix();
		//get the node's inverse transpose world matrix
		const glm::mat4& getInverseTransposeWorldMatrix();
		//get the node's inverse transpose world view matrix based on the scene's active camera
		const glm::mat4& getInverseTransposeWorldViewMatrix();
		//get the node's view matrix based on the scene's active camera
		const glm::mat4& getViewMatrix() const;
		//get the node's inverse view matrix based on the scene's active camera
		const glm::mat4& getInverseViewMatrix() const;
        //get the reflected view matrix based on the scene's active camera
        const glm::mat4& getReflectionViewMatrix() const;
		//get the projection matrix of the scene to which the node belongs
		const glm::mat4& getProjectionMatrix() const;
		//get the view projection matrix of the scene to which the node belongs, based on the scene's active camera
		const glm::mat4& getViewProjectionMatrix() const;
		//get the inverse view projection matrix based on the node's scene's active camera
		const glm::mat4& getInverseViewProjectionMatrix() const;
        //get the reflected view projection matrix based on the scene's active camera
        const glm::mat4& getReflectionViewProjectionMatrix() const;
		//get the world view projection matrix of this node based on the node's scene active camera
		const glm::mat4& getWorldViewProjectionMatrix();
        //get the reflected world view projection matrix based on the scene's active camera
        const glm::mat4& getReflectionWorldViewProjectionMatrix();

		//get the nodes translation in world coordinates
		glm::vec3 getTranslationWorld();
		//get the node's translation in view coordinates
		glm::vec3 getTranslationView();
		//get the node's forward vector in world coordinates
		glm::vec3 getForwardVectorWorld();
		//get the node's forward vector rin view coordinates
		glm::vec3 getForwardVectorView();
		//get the node's up vector in world coordinates
		glm::vec3 getUpVectorWorld();
		//get the node's up vector in view coordinates
		glm::vec3 getUpVectorView();
		//get the node's right vector in world coordinates
		glm::vec3 getRightVectorWorld();
		//get the node's right vector in view coordinates
		glm::vec3 getRightVectorView();

		//get this node's scene's active camera translation in world coordinates
		glm::vec3 getSceneCameraTranslationWorld() const;
		//get this node's scene active camera translation in view coordinates
		glm::vec3 getSceneCameraTranslationView() const;

		//returns a pointer to the node's scene, or nullptr if it
		//is not a member of any scene
		Scene* getScene() const;
		//set this node's scene pointer, can be nullptr
		void setScene(Scene* scene);

		//remove this node from the hierarchy
		void remove(); //TODO is this still right as we no longer use linked lists?

        //forces the node to update its bounds when having modified an attachment
        void setBoundsDirty();

        //adds an existing component to this node, taking ownership of it.
        //returns a pointer to the added component
        template <typename T>
        T* addComponent(std::unique_ptr<T>& component)
        {
            T* retVal = dynamic_cast<T*>(component.get());
            component->setNode(this);
            //remove existing - TODO if we switch to multimap won't need this
            if (auto existing = getComponent<T>()) removeListener(*static_cast<Transform::Listener*>(existing));

            addListener(*static_cast<Transform::Listener*>(retVal));
            m_components[std::type_index(typeid(T))].reset(component.release());

            setBoundsDirty(); //adding models or lights require re-calc

            return retVal;
        }

        template <typename T>
        T* getComponent()
        {
            std::type_index index(typeid(T));
            if (m_components.count(index) != 0)
            {
                return dynamic_cast<T*>(m_components[index].get());
            }
            else
            {
                return nullptr;
            }
        }

        //updates the node and any child nodes by executing any attached script components
        void update(float);

        void destroy();
        bool destroyed() const;

	protected:
	
		virtual void transformChanged() override;
		void hierarchyChanged();


	private:
        enum DirtyBits
        {
            WORLD  = (1 << 0),
            BOUNDS = (1 << 1),
            ALL    = (WORLD | BOUNDS)
        };

		std::string m_name;
		std::vector<Ptr> m_children;
		Node* m_parent;
		Int32 m_parentIndex;

		bool m_active;

        mutable BoundingSphere m_boundingSphere;
        mutable BoundingBox m_boundingBox;

		glm::mat4 m_worldMatrix;

		mutable UInt32 m_dirtybits;
		bool m_hierarchyChanged;

		Scene* m_scene;
        std::unordered_map<std::type_index, Component::Ptr> m_components;

        bool m_destroyed;
	};
}

#endif //CHUF_NODE_HPP_
