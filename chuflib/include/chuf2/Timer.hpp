/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//abstracts the SDL timer function

#ifndef CHUF_TIMER_HPP_
#define CHUF_TIMER_HPP_

#include <SDL_timer.h>
#include <chuf2/NonCopyable.hpp>

namespace chuf
{
    class Time final
    {
    public:
        explicit Time(double time = 0.0) : m_time(time){}

        float asSeconds() const { return static_cast<float>(m_time / 1000.0); }
        double asMilliseconds() const { return m_time; }

    private:
        double m_time;
    };
    
    class Timer final : private NonCopyable
    {
    public:
        Timer() : m_startTime(static_cast<double>(SDL_GetTicks())){	}
        Time restart()
        {
            Time time = elapsed();
            m_startTime = static_cast<double>(SDL_GetTicks());
            return time;
        }
        Time elapsed() const { return Time(static_cast<double>(SDL_GetTicks() - m_startTime)); }
    private:
        double m_startTime;
    };
}

#endif //CHUF_TIMER_HPP_