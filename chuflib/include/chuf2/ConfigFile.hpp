/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//class for parsing config files into managable objects

#ifndef CHUF_CONFIG_FILE_HPP_
#define CHUF_CONFIG_FILE_HPP_

#include <chuf2/DataTypes.hpp>

#include <chuf2/glm/glm.hpp>

#include <string>
#include <vector>
#include <memory>

namespace chuf
{
    //base class for all items loaded from a configuration file
    class ConfigItem
    {
    public:
        ConfigItem(const std::string& name);
        virtual ~ConfigItem() = default;

        const std::string& getName() const;

    protected:
        void setParent(ConfigItem* parent);
        ConfigItem* getParent() const;

    private:
        ConfigItem* m_parent;
        std::string m_name;
    };
    
    //properties are a name / value pair which can only reside in 
    //configuration objects.
    class ConfigProperty final : public ConfigItem
    {
        friend class ConfigObject;
    public:
        using Ptr = std::unique_ptr<ConfigProperty>;
        ConfigProperty(const std::string& name, const std::string& value);

        //returns the property's value as a string representation
        const std::string& valueAsString() const;
        //returns the property's value as an integer if possible, else 0
        Int32 valueAsInt() const;
        //returns the property's value as a floating point value if possible, else 0.f
        float valueAsFloat() const;
        //returns the property's value as a bool, only the string literal "true" 
        //returns as true, all other values are returned as false
        bool valueAsBool() const;
        //attempts to parse value as a 2 component vector. missing components
        //are returned as 0
        glm::vec2 valueAsVec2() const;
        //attempts to parse value as a 3 component vector. missing components
        //are returned as 0
        glm::vec3 valueAsVec3() const;
        //attempts to parse value as a 4 component vector. missing components
        //are returned as 0
        glm::vec4 valueAsVec4() const;

        //sets the property's value
        void setValue(const std::string& v);
        void setValue(Int32 v);
        void setValue(float v);
        void setValue(bool v);
        void setValue(const glm::vec2& v);
        void setValue(const glm::vec3& v);
        void setValue(const glm::vec4& v);
        
    private:
        std::string m_value;
        std::vector<float> valueAsArray() const;
    };

    //a configuration object can hold one or more nested objects
    //or configuration properties. All objects require a name,
    //and may have an optional id as a string value.
    class ConfigObject final : public ConfigItem
    {
    public:
        using Ptr = std::unique_ptr<ConfigObject>;
        using NameValue = std::pair<std::string, std::string>;
        ConfigObject(const std::string& name, const std::string& id);

        static ConfigObject::Ptr create(const std::string& path);

        //get the id of the object
        const std::string& getId() const;
        //returns a pointer to the property if found, else nullptr
        ConfigProperty* findProperty(const std::string& name) const;
        //searches for a child object with given id and returns a pointer
        //to it if found, else retuens nullptr
        ConfigObject* findObjectWithId(const std::string& id) const;
        //searches for a child object with the given name and returns
        //a pointer to it if found, else returns nullptr
        ConfigObject* findObjectWithName(const std::string& name)const;

        //returns a reference to the vector of properties owned by this object
        const std::vector<ConfigProperty::Ptr>& getProperties() const;
        //returns a reference to the vector objects owned by this object
        const std::vector<ConfigObject::Ptr>& getObjects() const;

        //adds a name / value property pair to this object
        void addProperty(const std::string& name, const std::string& value);
        //adds an object with the given name and optional id to this object
        ConfigObject* addObject(const std::string& name, const std::string& id = "");
        //removes a property with the given name from this object if it exists
        void removeProperty(const std::string& name);
        //removes an object (and all of its children) with the given name from
        //this objects if it exists
        ConfigObject::Ptr removeObject(const std::string& name);

        //writes this object and all its children to given path
        void save(const std::string& path);

    private:
        std::string m_id;
        std::vector<ConfigProperty::Ptr> m_properties;
        std::vector<ConfigObject::Ptr> m_objects;

        static NameValue getObjectName(const std::string& line);
        static NameValue getPropertyName(const std::string& line);
        static bool isProperty(const std::string& line);
        static void removeComment(std::string& line);

        void write(std::ofstream& file, Uint16 depth = 0u);
    };

    using ConfigFile = chuf::ConfigObject;
    
}

#endif // CHUF_CONFIG_FILE_HPP_