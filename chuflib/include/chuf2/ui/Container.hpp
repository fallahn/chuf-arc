/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#ifndef CHUF_UI_CONTAINER_HPP_
#define CHUF_UI_CONTAINER_HPP_

#include <chuf2/ui/Control.hpp>

#include <vector>

namespace chuf
{
    namespace ui
    {
        class Container final : public Control
        {
        public:
            using Ptr = std::unique_ptr<Container>;

            explicit Container(Desktop&);
            ~Container() = default;

            void addControl(Control& control);

            bool selectable() const override;
            void update(float) override;
            void handleKeyEvent(const chuf::Event::KeyEvent& evt) override;
            void handleMouseEvent(const chuf::Event::MouseEvent& evt, const glm::vec2& mousePos) override;

            void setAlignment(Alignment) override;

        private:

            std::vector<Control*> m_controls;
            UInt16 m_selectedIndex;

            bool hasSelection() const;
            void select(UInt16 index);
            void selectNext();
            void selectPrevious();
        };
    }
}

#endif //CHUF_UI_CONTAINER_HPP_