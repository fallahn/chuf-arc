/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#ifndef CHUF_UI_DESKTOP_HPP_
#define CHUF_UI_DESKTOP_HPP_

#include <chuf2/Rectangle.hpp>
#include <chuf2/SpriteBatch.hpp>
#include <chuf2/Text.hpp>
#include <chuf2/ui/Container.hpp>
#include <chuf2/View.hpp>

#include <memory>
#include <string>
#include <map>

namespace chuf
{
    class Sprite;
    namespace ui
    {
        class Desktop final
        {
        public:
            Desktop();
            ~Desktop() = default;

            //returns a sprite from the desktop's sprite store
            std::shared_ptr<chuf::Sprite> getSprite(Texture* texture, const FloatRect& subrect = FloatRect());

            //returns a text object which uses specified font
            Text* addText(Font*);
            void removeText(Text*);
            
            //controls created with this function will be part of this desktop
            template <typename T>
            T& addControl()
            {
                m_controls.emplace_back(std::make_unique<T>(*this));
                return *dynamic_cast<T*>(m_controls.back().get());
            }

            //added containers will receive this desktop's events
            void addContainer(Container&);

            void handleKeyEvent(const chuf::Event::KeyEvent&);
            void handleMouseEvent(const chuf::Event::MouseEvent&);

            //sets the view of the desktop when it is drawn, and scales it
            //to the current window. This allows for fixed or higher resolution
            //UI's than the current render window
            void setView(const View&);

            //gets the mouse position scaled to the desktop size
            glm::vec2 getMousePosition() const;

            //draws all the controls belonging to this desktop
            void draw();
        private:

            std::vector<Control::Ptr> m_controls;
            std::vector<Container*> m_containers;
            std::map<TextureID, SpriteBatch::Ptr> m_spriteBatches;
            std::vector<Text::Ptr> m_texts;
            View m_view;
        };
    }
}

#endif //CHUF_UI_DESKTOP_HPP_