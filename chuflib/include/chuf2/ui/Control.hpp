/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#ifndef CHUF_UI_CONTROL_HPP_
#define CHUF_UI_CONTROL_HPP_

#include <chuf2/DataTypes.hpp>
#include <chuf2/glm.hpp>
#include <chuf2/Transform.hpp>

#include <memory>

namespace chuf
{
    namespace ui
    {
        enum class Alignment
        {
            TopLeft,
            BottomLeft,
            Centre,
            TopRight,
            BottomRight
        };

        class Desktop;
        class Control : public Transform
        {
        public:
            using Ptr = std::unique_ptr<Control>;

            explicit Control(Desktop&);
            virtual ~Control() = default;
            Control(const Control&) = delete;
            Control& operator = (const Control&) = delete;

            virtual bool selectable() const = 0;
            bool selected() const;

            virtual void select();
            virtual void deselect();

            virtual bool active() const;
            virtual void activate();
            virtual void deactivate();

            virtual void update(float){}
            virtual void handleKeyEvent(const chuf::Event::KeyEvent& evt) = 0;
            virtual void handleMouseEvent(const chuf::Event::MouseEvent& evt, const glm::vec2& mousePos) = 0;

            virtual void setAlignment(Alignment) = 0;
            virtual bool contains(const glm::vec2& mousePos) const;

            void setVisible(bool);
            bool visible() const;
        
        protected:
            Desktop& getDesktop();

        private:

            bool m_selected;
            bool m_active;
            bool m_visible;
            UInt16 m_index;

            Desktop& m_desktop;
        };
    }
}

#endif