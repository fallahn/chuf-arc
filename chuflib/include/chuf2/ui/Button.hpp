/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/
#ifndef CHUF_UI_BUTTON_HPP_
#define CHUF_UI_BUTTON_HPP_

#include <chuf2/ui/Control.hpp>
#include <chuf2/Rectangle.hpp>

#include <functional>
#include <map>

namespace chuf
{
    class Sprite;
    class Texture;
    class Font;
    class Text;
    namespace ui
    {
        class Button final : public Control
        {
        public:
            using Ptr = std::unique_ptr<Button>;
            using Callback = std::function<void(Control*)>; //TODO this and events should be Control class?

            enum class Event
            {
                MouseEnter,
                MouseExit,
                Activated,
                Deactivated,
                Selected,
                Deselected
            };

            explicit Button(Desktop&);
            ~Button();

            bool selectable() const override;
            void select() override;
            void deselect() override;

            void activate() override;
            void deactivate() override;

            void handleKeyEvent(const chuf::Event::KeyEvent& evt) override;
            void handleMouseEvent(const chuf::Event::MouseEvent& evt, const glm::vec2& mousePos) override;

            void setAlignment(Alignment) override;
            bool contains(const glm::vec2& mousePos) const override;

            void addCallback(Callback, Event);
            void setText(const std::string&, Font*);
            void setTexture(Texture*, const FloatRect& subrect = FloatRect());
            void setTogglable(bool);


        private:

            enum State
            {
                Normal = 0,
                Selected,
                Active,
                Count
            };

            std::map<Event, std::vector<Callback>> m_callbacks;
            //std::vector<FloatRect> m_subrects; //TODO allow changing sprite on mouseover etc
            std::shared_ptr<Sprite> m_sprite;
            Text* m_text;
            bool m_togglable;
        };
    }
}

#endif //CHUF_UI_BUTTON_HPP_