/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//represents an image, loaded on CPU/client side

#ifndef CHUF_IMAGE_HPP_
#define CHUF_IMAGE_HPP_

#include <chuf2/NonCopyable.hpp>
#include <chuf2/ClassKey.hpp>
#include <chuf2/DataTypes.hpp>
#include <chuf2/Rectangle.hpp>
#include <chuf2/Colour.hpp>
#include <chuf2/glm.hpp>

#include <memory>
#include <string>
#include <vector>

namespace chuf
{
    class Image final : private NonCopyable, private Locked
    {
    public:
        enum class Format
        {
            A = 1,
            RGB = 3,
            RGBA,
            Invalid
        };

        using Ptr = std::unique_ptr<Image>;
        explicit Image(const Key& key);
        ~Image() = default;

        static Image::Ptr create(const std::string& path);
        static Image::Ptr create(UInt32 width, UInt32 height, Format format, UInt8* data = nullptr);
        static Image::Ptr create(UInt32 width, UInt32 height, Format format, Colour colour);
        //expects a single cruciform cubemap image, and splits it into 6 faces
        //posX, negX, posY, negY, posZ, negZ
        static std::vector<Image::Ptr> createCubemap(const std::string& path);
        //used for creating fallback or test images which can be loaded as a texture
        //in this case a magenta / black chequer pattern
        static Image::Ptr createChequer();

        //returns a pointer to the data array making up the image
        const UInt8* getData() const;
        //returns the format of the image, can be useful for testing image validity
        Format getFormat() const;
        //returns the width of the image in pixels
        UInt32 getWidth() const;
        //returns the height of the image in pixels
        UInt32 getHeight() const;
        //returns the unique id of the image. usually the path from which the image
        //was loaded, but images created from raw data will attempt to generate their own
        //unique id value. used for identifying cached images and textures.
        const std::string& getUid() const;
        //flips an image vertically. useful for creating textures from images loaded from
        //disk as openGL has an inverted Y axis relative to image data. This is called by
        //default when loading an image into the texture class.
        void flipVertically();
        //returns whether or not the image has been flipped vertically
        bool flippedVertically() const;
        //returns whether or not the image dimensions are power of two values.
        //this is needed on some hardware when creating mip maps for example
        bool isPowerTwo() const;
        //saves image to file at given path. valid extensions are bmp, tga and png
        //path must exist on disk else no file will be written
        void save(const std::string& path) const;
        //copies the given data to the given subrectangle of the image
        void setSubrect(const glm::vec2& position, const Image& src);
        //resizes the image
        void resize(UInt32 width, UInt32 height);
        //adjust the gamma of the image
        void adjustGamma(float amount);
        //apply a basic gaussian blur to the image - FAR from optimal, so don't expect much
        void blur(UInt16 radius);

    private:
        std::vector<UInt8> m_imageData;
        Format m_format;
        UInt32 m_width, m_height;
        std::string m_uid;
        bool m_flippedVertically;

        void calcUid();
        static void copySubrect(UInt8* dst, UInt8* src, const UIntRect& dstRect, const UIntRect& srcRect, Int32 bpp);

        UInt8* getPixel(Int32 x, Int32 y);
        void setPixel(Int32 x, Int32 y, const std::vector<float>& normalisedPixel);
        void blurPass(const glm::vec2& dir, UInt16 radius);
    };
}

#endif //CHUF_IMAGE_HPP_