/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#ifndef CHUF_VIEW_HPP_
#define CHUF_VIEW_HPP_

#include <chuf2/Rectangle.hpp>
#include <chuf2/glm.hpp>

namespace chuf
{
    class View final
    {
    public:
        View();
        View(const FloatRect& vp);
        View(const View&) = default;
        View& operator = (const View&) = default;
        ~View() = default;

        FloatRect viewport;
        //maps a pixel position (such as mouse cursor) to view coordinates
        glm::vec2 mapPixelToView(const glm::ivec2& pixel) const;        
    };
}

#endif //CHUF_VIEW_HPP_