/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//a volume representation of a projection matrix built from six planes

#ifndef CHUF_FRUSTUM_HPP_
#define CHUF_FRUSTUM_HPP_

#include <chuf2/Plane.hpp>

#include <vector>

namespace chuf
{
    class BoundingBox;
    class BoundingSphere;
    class Frustum final
    {
        friend class Camera;
    public:
        Frustum();
        explicit Frustum(const glm::mat4& matrix);
        Frustum(const Frustum& copy);
        ~Frustum() = default;

        const Plane& getNear() const;
        const Plane& getFar() const;
        const Plane& getTop() const;
        const Plane& getBottom() const;
        const Plane& getLeft() const;
        const Plane& getRight() const;
        std::vector<glm::vec3> getCorners() const;
        std::vector<Plane> getPlanes() const;

        const glm::mat4& getProjectionMatrix() const;

        void set(const glm::mat4& matrix);
        void set(const Frustum& frustum);

        bool intersects(const BoundingBox& bb) const;
        bool intersects(const BoundingSphere& sphere) const;
        bool intersects(const glm::vec3& point) const;
        Plane::Intersection intersects(const Plane& plane) const;

    private:

        Plane m_near;
        Plane m_far;
        Plane m_top;
        Plane m_bottom;
        Plane m_left;
        Plane m_right;

        glm::mat4 m_projectionMatrix;

        void updatePlanes();
    };
}

#endif //CHUF_FRUSTUM_HPP_