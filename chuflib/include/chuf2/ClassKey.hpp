/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

/*
To take advantage of shared pointers classes must have a public constructor.
Unfortunately this renders the factory function moot if direct access is provided
via a public ctor. To get around this, classes which inherit the Locked class are
given access to a private member Key which is required as part of the public constructor.
for example:

class MyClass : private Locked
{
public:
    MyClass(const Key& key, float param)
      : m_param(param){}

    static std::shared_ptr<MyClass> create(float someVal)
    {
        return std::make_shared(Key(), someVal);
    }

private:
    float m_param;
};

The factory function create() ensures that the only way to get an instance of
MyClass is via the factory function.
*/
#ifndef CHUF_CLASS_KEY_HPP_
#define CHUF_CLASS_KEY_HPP_

namespace chuf
{
    class Locked
    {
    public:
        virtual ~Locked() = default;
    protected:
        class Key final
        {

        };
    };
}

#endif //CHUF_CLASS_KEY_HPP_