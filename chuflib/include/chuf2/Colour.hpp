/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//represents a colour as RGBA///
#ifndef CHUF_COLOUR_HPP_
#define CHUF_COLOUR_HPP_

#include <chuf2/DataTypes.hpp>

#include <cassert>

namespace chuf
{
    struct Colour final
    {
        Colour() : r(0.f), g(0.f), b(0.f), a(1.f){}
        
        //provides construction from byte (0 - 255) values as convenience
        //all colour values are stored as normalized float values
        Colour(UInt8 red, UInt8 green, UInt8 blue, UInt8 alpha = 255u)
            : r(static_cast<float>(red) / 255.f), g(static_cast<float>(green) / 255.f), b(static_cast<float>(blue) / 255.f), a(static_cast<float>(alpha) / 255.f)
        {
            //do we need assertion when a uchar is never > 255?
            assert(r <= 1.f && g <= 1.f && b <= 1.f && a <= 1.f);
        }
        
        //set a colour via a single 32 bit int, or hex value
        Colour(UInt32 colour) 
            : r(static_cast<float>((colour >> 24) & 0xFF) / 255.f),
            g(static_cast<float>((colour >> 16) & 0xFF) / 255.f),
            b(static_cast<float>((colour >> 8) & 0xFF) / 255.f),
            a(static_cast<float>((colour & 0xFF) / 255.f)){}

        Colour(float red, float green, float blue, float alpha = 1.f)
            : r(red), g(green), b(blue), a(alpha)
        {
            assert(r <= 1.f && g <= 1.f && b <= 1.f && a <= 1.f);
        }
        float r, g, b, a;

        UInt8 redAsByte() const
        {
            return static_cast<UInt8>(255.f * r);
        }

        UInt8 greenAsByte() const
        {
            return static_cast<UInt8>(255.f * g);
        }

        UInt8 blueAsByte() const
        {
            return static_cast<UInt8>(255.f * b);
        }

        UInt8 alphaAsByte() const
        {
            return static_cast<UInt8>(255.f * a);
        }

        UInt32 packed() const
        {
            return (redAsByte() << 24 | greenAsByte() << 16 | blueAsByte() << 8 | alphaAsByte());
        }

        static const Colour red;
        static const Colour green;
        static const Colour blue;
        static const Colour cyan;
        static const Colour magenta;
        static const Colour yellow;
        static const Colour white;
        static const Colour black;
        static const Colour transparent;

    };


    bool operator == (const Colour&, const Colour&);
    bool operator != (const Colour&, const Colour&);
    Colour operator + (const Colour&, const Colour&);
    Colour operator - (const Colour&, const Colour&);
    Colour operator * (const Colour&, const Colour&);

    Colour& operator += (Colour&, const Colour&);
    Colour& operator -= (Colour&, const Colour&);
    Colour& operator *= (Colour&, const Colour&);
}



#endif //CHUF_COLOUR_HPP_