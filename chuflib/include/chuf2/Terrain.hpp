/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#ifndef CHUF_TERRAIN_HPP_
#define CHUF_TERRAIN_HPP_

#include <chuf2/Transform.hpp>
#include <chuf2/ClassKey.hpp>
#include <chuf2/NonCopyable.hpp>
#include <chuf2/HeightData.hpp>
#include <chuf2/BoundingBox.hpp>
#include <chuf2/Drawable.hpp>
#include <chuf2/TerrainPatch.hpp>
#include <chuf2/AlignedAllocation.hpp>
#include <chuf2/Material.hpp>
#include <chuf2/QuadTree.hpp>
#include <chuf2/Component.hpp>

#include <memory>

namespace chuf
{
    class Terrain final : public Aligned<16>, public Drawable, public Camera::Listener, private Locked, private NonCopyable
    {
        friend class TerrainPatch;
    public:

        using Ptr = std::unique_ptr<Terrain>;

        Terrain(SceneRenderer*, const Key&);
        ~Terrain() = default;

        static Ptr create(SceneRenderer*, HeightData::Ptr&, const glm::vec3& scale = glm::vec3(1), UInt32 patchSize = 32u, UInt32 detailLevels = 1u, float skirtSize = 0.f);
        Uint32 getPatchCount() const;
        const TerrainPatch& getPatch(UInt32 index) const;
        const BoundingBox& getBoundingBox() const override;
        float getHeight(float x, float z) const;
        void setNode(Node*) override;
        UInt32 draw(UInt32 passflags, bool = false) const override;
        Material::Ptr getMaterial() const;

        void cameraChanged(Camera*) override;

    private:

        HeightData::Ptr m_heightData;
        glm::vec3 m_scale;
        std::vector<std::unique_ptr<TerrainPatch>> m_patches;

        BoundingBox m_boundingBox;
        Material::Ptr m_material;

        glm::vec2 m_patchSize;
        glm::uvec2 m_patchDims;
        glm::vec2 m_halfSize;

        enum DirtyBits
        {
            TRANSFORM = 0x1,
            CAMERA    = 0x2
        };
        mutable UInt32 m_flags;

        mutable glm::mat4 m_inverseWorldMatrix;

        QuadTree m_quadTree;
        mutable std::vector<QuadTreeComponent*> m_visibilitySet;

        void transformChanged(Transform&) override;
        const glm::mat4& getInverseWorldMatrix() const;
        std::vector<TerrainPatch*> getPatches(const FloatRect& testArea) const;
    };
}

#endif //CHUF_TERRAIN_HPP_