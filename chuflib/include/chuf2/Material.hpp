/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//material class, composed of a technique, in turn composed of multiple passes
//a single technique is normally used, although multiple techniques can be defined
//so that an appropriate technique can be picked for use on particular hardware.
//for example different techniques may use different shader model versions so that
//older hardware may have a fall back. Materials are used in conjunction with
//meshes to create a model which can be attached to a scene node.

#ifndef CHUF_MATERIAL_HPP_
#define CHUF_MATERIAL_HPP_

#include <chuf2/RenderState.hpp>
#include <chuf2/Shader.hpp>
#include <chuf2/Node.hpp>
#include <chuf2/Technique.hpp>
#include <chuf2/Taggable.hpp>

namespace chuf
{
    class ConfigProperty;
    class ConfigObject;
    class Material final : public RenderState, public Taggable
    {
    public : 
        using Ptr = std::shared_ptr<Material>;
        enum class Type
        {
            Unlit, //used for sprite / 2D overlays
            LightMapped, //world geometry which has a pre-rendered lightmap
            VertexLit, //models, props etc
            SkyBox,
            Water,
            Terrain,
            Custom
        };

        //create a material from the given shader
        static Ptr create(Shader* shader, UInt32 defaultPassflags = RenderPass::Standard);
        //attempts to create a material from the given paths to shader files on disk.
        //returns nullptr if shader creation fails, and logs an error
        static Ptr create(const std::string& vertShader, const std::string& fragShader, UInt32 defaultPassflags = RenderPass::Standard);
        //attempts to create a material from a configuration file at the given path
        //returns nullptr if it fails, and logs an error
        static Ptr createFromFile(const std::string& path);
        //returns a default material
        static Ptr getDefault(Type type = Type::Unlit);

        //returns the number of techniques in the material
        Uint32 getTechniqueCount() const;
        //gets a pointer to the technique at the requested index
        //else return nullptr if it doesn't exist
        Technique* getTechnique(UInt32 index);
        //gets a technique by name if it exists, else returns nullptr
        Technique* getTechnique(const std::string& name) const;
        //returns a pointer to the current active technique
        Technique* getTechnique();

        //attempts to set the current technique by name
        void setTechnique(const std::string& name);
        //binds the material's model's parent node to this material
        //so that default shader uniforms such as matrices can be bound
        void bindNode(Node* node) override;

        const Type& getType() const;

        //sets a name string if required
        void setName(const std::string& name);
        //gets the materials name if it has one
        const std::string& getName() const;

        //returns whether or not this material has a particular flag or flags set
        bool hasFlag(UInt32 flags) const;

        explicit Material(const Key& key);
        ~Material();// = default;

    private:

        Type m_type;

        Technique* m_currentTechnique;
        std::vector<std::unique_ptr<Technique>> m_techniques;

        std::string m_name;
        UInt32 m_flags;

        struct TerrainLayer final
        {
            float repeat = 1.f;
            std::string diffusePath;
            std::string normalPath;
            std::string maskPath;
        };

        static void setBlend(Material::Ptr& material, const std::unique_ptr<ConfigProperty>& p, const RenderState::StateBlock::Ptr& sb);
        static void createDebugPass(const Technique::Ptr& technique, const std::string& defines);
        static void createUnlit(Ptr&, Technique::Ptr&, const std::string& path, ConfigObject* matProp);
        static void createVertexLit(Ptr&, Technique::Ptr&, const std::string& path, ConfigObject* matProp);
        static void createLightMapped(Ptr&, Technique::Ptr&, const std::string& path, ConfigObject* matProp);
        static void createSkybox(Ptr&, Technique::Ptr&, const std::string& path, ConfigObject* matProp);
        static void createWater(Ptr&, Technique::Ptr&, const std::string& path, ConfigObject* matProp);
        static void createTerrain(Ptr&, Technique::Ptr&, const std::string& path, ConfigObject* matProp);
        static bool createLayerSamplers(Ptr&, std::string& defines, const std::vector<TerrainLayer>& layers);
    };
}

#endif //CHUF_MATERIAL_HPP_