/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//wraps an opengl texture, and ensures only a single instance of each one

#ifndef CHUF_TEXTURE_HPP_
#define CHUF_TEXTURE_HPP_

#include <chuf2/NonCopyable.hpp>
#include <chuf2/ClassKey.hpp>
#include <chuf2/DataTypes.hpp>
#include <chuf2/Timer.hpp>

#include <unordered_map>
#include <memory>
#include <vector>

#include <chuf2/GL/gl3w.h>

namespace chuf
{
    class Image;
    class Texture final : private NonCopyable, private Locked
    {
    public:
        using Ptr = std::unique_ptr<Texture>;
        enum Format
        {
            Invalid = 0,
            RGB     = GL_RGB,
            RGBA    = GL_RGBA,
            ALPHA   = GL_ALPHA
        };

        enum Filter
        {
            Nearest              = GL_NEAREST,
            Linear               = GL_LINEAR,
            NearestMipmapLinear  = GL_NEAREST_MIPMAP_LINEAR,
            NearestMipmapNearest = GL_NEAREST_MIPMAP_NEAREST,
            LinearMipmapNearest  = GL_LINEAR_MIPMAP_NEAREST,
            LinearMipmapLinear   = GL_LINEAR_MIPMAP_LINEAR
        };

        enum Type
        {
            Texture2D      = GL_TEXTURE_2D,
            Texture2DArray = GL_TEXTURE_2D_ARRAY,
            TextureCube    = GL_TEXTURE_CUBE_MAP
        };

        enum Wrap
        {
            Repeat = GL_REPEAT,
            Clamp  = GL_CLAMP_TO_EDGE
        };

        explicit Texture(const Key& key);
        ~Texture();

        //creates a texture rom the given path, and returns a pointer to it if successful,
        //else returns nullptr. mipmaps are created by default, and textures are flipped
        //so that image coordinates match those of opengl. you may not want to flip the
        //texture when using it in a sprite batch for example, so that the sprite coordinates
        //match those of more traditional systems
        static Texture* create(const std::string& path, bool createMipmaps = true, bool flipVertically = true);
        //create a texture from an existing image in memory
        static Texture* create(const std::unique_ptr<Image>& image, bool createMipmaps = true);
        //create a cubemap from an image in local storage. cubemap images are automatically split
        //when they are loaded.
        static Texture* createCubemap(const std::string& path);
        //vector of images must be posX, negX, posY, negY, posZ, negZ
        static Texture* createCubemap(const std::vector<std::unique_ptr<Image>>& images);
        //attempts to create an animated texture from a *.cat (CHUF Animated Texture) file
        static Texture* createAnimated(const std::string& path);
        //creates a 2D array texture from given images, if they are valid - else returns nullptr
        static Texture* createArray(const std::vector<std::unique_ptr<Image>>& images);

        //returns the path to the original image if loaded from local storage
        const std::string& getPath() const;
        //returns the image format of the texture
        Format getFormat() const;
        //returns whether this texture is a two dimensional or cubemap texture
        Type getType() const;
        //returns the width of the texture if it is not a cubemap
        UInt32 getWidth() const;
        //returns the height of the texture if it is not a cubemap
        UInt32 getHeight() const;
        //returns the underlying opengl handle for this texture
        TextureID getHandle() const;
        //creates mipmaps for this texture if it has power of two dimensions
        //and mipmaps have not yet already been created
        void createMipmaps();
        //returns true if texture contains mipmaps
        bool hasMipmaps() const;
        //returns true if this texture has been flipped vertically
        bool flippedVertically() const;


        //provides a sampler binding so that textures can be bound to
        //material properties, and textures shared between materials
        class Sampler final : private NonCopyable, private Locked
        {
        public:
            using Ptr = std::shared_ptr<Sampler>;
            Sampler(Texture* texture, const Sampler::Key& key);
            ~Sampler() = default;

            //create a sampler from an existing texture and returns a pointer to
            //it if successful, else returns nullptr
            static Ptr create(Texture* texture);
            //attempts to load an image from the given path and creates a sampler with it
            //returns a pointer to the sampler if successful, else returns nullptr
            static Ptr create(const std::string& path, bool generatemipmaps = true, bool flipVertically = true);

            //sts the wrap mode for the associated texture
            void setWrap(Wrap wrapS, Wrap wrapT);
            //sets the filter mode for the associate texture
            void setFilter(Filter min, Filter Mag);
            //returns a pointer to the associated texture
            Texture* getTexture() const;
            //updates the associated texture
            void setTexture(Texture* t);

            void bind();

        private:

            Texture* m_texture;
            Wrap m_wrapS, m_wrapT;
            Filter m_minFilter, m_magFilter;
        };
    private:

        std::string m_path;
        TextureID m_textureID;
        Format m_format;
        Type m_type;
        UInt32 m_width, m_height;
        bool m_mipmaps;
        bool m_flippedVertically;
        Wrap m_wrapS;
        Wrap m_wrapT;
        Wrap m_wrapR;
        Filter m_minFilter;
        Filter m_magFilter;

        enum class Mode
        {
            Animated,
            Static
        }m_mode;
        float m_framerate;
        Timer m_frameTimer;
        mutable UInt32 m_currentFrame;
        std::vector<Texture*> m_frames;
        TextureID getCurrentFrame() const;

        static Texture* create(Format format, UInt32 width, UInt32 height, const UInt8* data, const std::string& uid, bool createMipmaps, bool flippedVertically);
        static Texture* createCubemapFromImages(const std::vector<std::unique_ptr<Image>>& images, const std::string& uid);
        static Texture* createArray(Format format, UInt32 width, UInt32 height, const std::vector<std::unique_ptr<Image>>& images, const std::string& uid, bool flippedVertically);
        static void checkCubemapImages(const std::vector<std::unique_ptr<Image>>& images);
    };
}

#endif //CHUF_TEXTURE_HPP_
