/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//source engine BSP specific structs (up to version 20 or 21)

#ifndef CHUF_SOURCE_BSP_HPP_
#define CHUF_SOURCE_BSP_HPP_

#include <chuf2/DataTypes.hpp>

#include <cassert>

namespace Bsp
{
    namespace Source
    {
        struct Vector
        {
            float x, y, z;
        };

        enum Lumps //lump names in order
        {
            Entities                    = 0,
            Planes                      = 1,
            TextureData                 = 2,
            Vertices                    = 3,
            Visibility                  = 4,
            Nodes                       = 5,
            TextureInfo                 = 6,
            Faces                       = 7,
            Lighting                    = 8,
            Occlusion                   = 9,
            Leaves                      = 10,
            FaceIds                     = 11,
            Edges                       = 12,
            SurfEdges                   = 13,
            Models                      = 14,
            WorldLights                 = 15,
            LeafFaces                   = 16,
            LeafBrushes                 = 17,
            Brushes                     = 18,
            BrushSides                  = 19,
            Areas                       = 20,
            AreaPortals                 = 21,
            Unused0                     = 22,
            Unused1                     = 23,
            Unused2                     = 24,
            Unused3                     = 25,
            DispInfo                    = 26,
            OriginalFaces               = 27,
            PhysDisp                    = 28,
            PhysCollide                 = 29,
            VertexNormals               = 30,
            VertexNormalIndices         = 31,
            DispLightmapAlphas          = 32,
            DispVerts                   = 33,
            DispLightmapSamplePositions = 34,
            GameLump                    = 35,
            LeafWaterData               = 36,
            Primitives                  = 37,
            PrimitiveVertices           = 38,
            PrimitiveIndices            = 39,
            PakFile                     = 40,
            ClipPortalVertices          = 41,
            Cubemaps                    = 42,
            TextureDataStringData       = 43,
            TextureDataStringTable      = 44,
            Overlays                    = 45,
            LeafMinDistanceToWater      = 46,
            FaceMacroTextureInfo        = 47,
            DispTriangles               = 48,
            PhysCollideSurface          = 49,
            WaterOverlays               = 50,
            LeafAmbientIndexHdr         = 51,
            LeafAmbientIndex            = 52,
            LightingHdr                 = 53,
            WorldLightsHdr              = 54,
            LeafAmbientLightingHdr      = 55,
            LeafAmbientLighting         = 56,
            XZipPakFile                 = 57, //legacy from original xbox
            FacesHdr                    = 58,
            MapFlags                    = 59,
            OverlayFades                = 60
        };

        enum class LumpVersion
        {
            Lighting            = 1,
            Faces               = 1,
            Occlusion           = 2,
            Leaves              = 1,
            LeafAmbientLighting = 1
        };

        static const chuf::Int32 MaxLumps = 64;

        struct Lump
        {
            chuf::Int32 fileOffset;
            chuf::Int32 fileLength;
            chuf::Int32 version = 0;
            char fourCC[4];
        };

        struct Header
        {
            chuf::Int32 id;
            chuf::Int32 version;
            Lump lumps[MaxLumps];
            chuf::Int32 mapRevision;
        };

        struct FlagsLump
        {
            chuf::UInt32 mapFlags;
        };

        struct LumpFileHeader
        {
            chuf::Int32 offset;
            chuf::Int32 id;
            chuf::Int32 version;
            chuf::Int32 length;
            chuf::Int32 mapRevision;
        };

        struct GameLumpHeader
        {
            chuf::Int32 lumpCount;
        };

        struct GameLump
        {
            chuf::Int32 id;
            chuf::UInt16 flags;
            chuf::UInt16 version;
            chuf::Int32 fileOffset;
            chuf::Int32 fileLength;
        };

        struct Model
        {
            Vector min;
            Vector max;
            Vector origin;
            chuf::Int32 headNode;
            chuf::Int32 firstFace;
            chuf::Int32 faceCount;
        };

        struct PhysModel
        {
            chuf::Int32 index;
            chuf::Int32 dataSize;
            chuf::Int32 keyDataSize;
            chuf::Int32 solidCount;
        };

        struct PhysDisplacement
        {
            chuf::UInt16 displacementCount;
        };

        struct Vertex
        {
            Vector position;
        };

        struct Plane
        {
            Vector normal;
            float distance;
            chuf::Int32 type;
        };

        struct Node
        {
            chuf::Int32 planeIndex;
            chuf::Int32 children[2];
            chuf::Int16 bbMin[3];
            chuf::Int16 bbMax[3];
            chuf::UInt16 firstFace;
            chuf::UInt16 faceCount;
            chuf::Int16 area; //if all leaves below this node are in the same area this is the area index
        };

        enum TexFlags
        {
            SurfLight     = 0x1,
            SurfSky2D     = 0x2,
            SurfSky       = 0x4,
            SurfWarp      = 0x8,
            SurfTrans     = 0x10,
            SurfNoPortal  = 0x20,
            SurfTrigger   = 0x40,
            SurfNoDraw    = 0x80,
            SurfHint      = 0x100,
            SurfSkip      = 0x200,
            SurfNoLight   = 0x400,
            SurfBumpLight = 0x800,
            SurfNoShadows = 0x1000,
            SurfNoDecals  = 0x2000,
            SurfNoChop    = 0x4000,
            SufHitbox     = 0x8000
        };

        struct TexInfo
        {
            float textureVectorsTexelsPerWorldUnit[2][4];  // [s/t][xyz offset]
            float lightmapVectorsLuxelsPerWorldUnit[2][4]; // [s/t][xyz offset]
            chuf::Int32 mipFlags;
            chuf::Int32 textureData; //ptr to data
        };

        struct TexData
        {
            Vector reflectivity;
            chuf::Int32 nameStringIndex; //id in string table
            chuf::Int32 width, height;
            chuf::Int32 viewWidth, viewHeight;
        };

        //-------------Occlusion structs-------------//

        enum OccluderFlags
        {
            Inactive = 0x1
        };

        struct OccluderData
        {
            chuf::Int32 flags;
            chuf::Int32 firstPoly; //index in occluder poly array
            chuf::Int32 polyCount;
            Vector bbMin;
            Vector bbMax;
            chuf::Int32 area;
        };

        struct OccluderDataV1
        {
            chuf::Int32 flags;
            chuf::Int32 firstPoly; //index in occluder poly array
            chuf::Int32 polyCount;
            Vector bbMin;
            Vector bbMax;
        };

        struct OccluderPolyData
        {
            chuf::Int32 firstVertex; //index in OccluderVertexIndices
            chuf::Int32 vertexCount;
            chuf::Int32 planeIndex;
        };

        struct Edge
        {
            chuf::UInt16 vertices[2]; //vertex indices
        };

        enum PrimitiveType
        {
            Triangles = 0,
            TriangleStrip = 1
        };

        struct Primitive
        {
            chuf::UInt8 type;
            chuf::UInt16 firstIndex;
            chuf::UInt16 indexCount;
            chuf::UInt16 firstVertex;
            chuf::UInt16 vertexCount;
        };

        struct PrimitiveVertex
        {
            Vector position;
        };

        struct Face
        {
            chuf::UInt16 planeNumber;
            chuf::UInt8 side;
            chuf::UInt8 onNode; //1 if on node else 0 if in leaf
            chuf::Int32 firstEdge; //index into surfedge array, not edge array
            chuf::Int16 edgeCount; //count of surfedge array, not edges
            chuf::Int16 textureInfo;
            chuf::Int16 displacementInfo;
            chuf::Int16 surfaceFogVolumeId; //for surfaces which are fog volume boundaries such as water. Other surfaces can check their leaf for fog volume id
            chuf::UInt8 styles[4];
            chuf::Int32 lightOffset; //offset into lightmap array
            float area;
            chuf::Int32 lightmapTextureMinsInLuxels[2]; //uv offset of lightmap
            chuf::Int32 lightmapTextureSizeInLuxels[2]; //size of lightmap for this face
            chuf::Int32 originalFace; //original face this was derived from
            //use accessors for primitive count because top bit also acts as shadow bool
            chuf::Int16 getPrimitiveCount() const { return m_primitiveCount & 0x7FFF; }
            void setPrimitiveCount(chuf::Int16 count)
            {
                assert((count & 0x8000) == 0);
                m_primitiveCount &= ~0x7FFF;
                m_primitiveCount |= (count & 0x7FFF);
            }
            bool dynamicShadowsEnabled() { return (m_primitiveCount & 0x8000) == 0; }
            void setDynamicShadowsEnabled(bool enabled) { (enabled) ? m_primitiveCount &= ~0x8000 : m_primitiveCount |= 0x8000; }

        private:
            chuf::UInt16 m_primitiveCount;
        public:
            chuf::UInt16 firstPrimitive;
            chuf::UInt32 smoothingGroups;
        };

        struct FaceId
        {
            chuf::UInt16 hammerFaceId;
        };

        enum LeafFlags
        {
            //NOTE only 7 bits stored
            Sky = 0x01,
            Radial = 0x02,
            Sky2D = 0x04
        };

        struct ColourRGB32Exp
        {
            chuf::UInt8 r, g, b;
            chuf::Int8 exponent;
        };

        struct CompressedLightCube
        {
            ColourRGB32Exp colour[6];
        };


        struct Leaf
        {
            chuf::Int32 contents; //OR of all brushes
            chuf::Int16 cluster;
            chuf::Int16 area : 9;
            chuf::Int16 flags : 7;
            chuf::Int16 bbMin[3];
            chuf::Int16 bbMax[3];
            chuf::UInt16 firstLeafFace;
            chuf::UInt16 leafFaceCount;
            chuf::UInt16 firstLeafBrush;
            chuf::UInt16 leafBrushCount;
            chuf::Int16 leafWaterId; //-1 for not in water

            //NOTE BSP versions 19 or lower require a CompressedLightCube struct here
            //other versions store lighting in their own chunks
        };

        struct SourceLeafAmbientLighting
        {
            CompressedLightCube cube;
            chuf::UInt8 x, y, z;
            chuf::UInt8 pad;
        };

        struct LeafAmbientIndex
        {
            chuf::UInt16 sampleCount;
            chuf::UInt16 firstSample;
        };

        struct BrushSide
        {
            chuf::UInt16 planeNumber;
            chuf::Int16 textureInfo;
            chuf::Int16 displaceInfo;
            chuf::Int16 bevel;
        };

        struct Brush
        {
            chuf::Int32 firstSide;
            chuf::Int32 sideCount;
            chuf::Int32 contents;
        };

        struct Vis
        {
            chuf::Int32 clusterCount;
            chuf::Int32 bitOffset[8][2];
        };

        //area portals are used to control visibility between areas
        //by opening / closing (such as a model based door) so areas
        //can be marked not visible even is the vis info says it should be
        struct AreaPortal
        {
            chuf::UInt16 portalKey; //entities have an ide which marks the area t owhich they belong
            chuf::UInt16 otherArea; //area into which this portal looks
            chuf::UInt16 firstClipPortalVertex;
            chuf::UInt16 clipPortlVertexCount;
            chuf::Int32 planeNumber;
        };

        struct Area
        {
            chuf::Int32 areaPortalCount;
            chuf::Int32 firstAreaPortal;
        };

        struct LeafWaterInfo
        {
            float surfaceZ;
            float minZ;
            chuf::Int16 surfaceTextureInfoId;
        };

        //lights which were used to light the map
        enum EmitType
        {
            Surface,   //90 degree spot
            Point,     //point light
            Spotlight, //spotlight with falloff
            Skylight,  //directional light
            QuakeLight,//linear falloff
            SkyAmbient //point light with no falloff
        };

        struct WorldLight
        {
            Vector origin;
            Vector intensity;
            Vector normal;
            chuf::Int32 cluster;
            EmitType type;
            chuf::Int32 style;
            float stopDot; //start of falloff for spotlight
            float stopDot2; //end of falloff for spotlight
            float exponent;
            float radius;
            float constAttenuation;
            float linearAttenuation;
            float quadraticAttenuation;
            chuf::Int32 flags;
            chuf::Int32 textureInfo;
            chuf::Int32 owner;
        };

        struct CubemapSample
        {
            chuf::Int32 origin[3];
            chuf::UInt8 size;
        };
    }
}

#endif //CHUF_SOURCE_BSP_HPP_