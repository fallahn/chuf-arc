/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#ifndef CHUF_Q3_BSP_HPP_
#define CHUF_Q3_BSP_HPP_

#include <chuf2/DataTypes.hpp>

//quake 3 bsp format specific structs
namespace Bsp
{
    namespace Q3
    {
        struct Vec2f
        {
            float x, y;
        };

        struct Vec3f
        {
            float x, y, z;
        };

        struct Vec3i
        {
            chuf::Int32 x, y, z;
        };

        struct Header
        {
            //should be IBSP but may be PSBI on little endian
            //will be VBSP with source engine maps
            chuf::Int8 id[4];
            chuf::Int32 version; //46 for Q3
        };


        struct Lump
        {
            chuf::Int32 offset;
            chuf::Int32 length;
        };


        struct Vertex
        {
            Vec3f position;
            Vec2f uv0;
            Vec2f uv1; //shadow map coord
            Vec3f normal;
            chuf::UInt8 colour[4]; //RGBA
        };

        enum FaceType
        {
            POLY = 1,
            PATCH,
            MESH,
            BILLBOARD
        };

        struct Face
        {
            chuf::Int32 materialID;         //index in the material lookup array
            chuf::Int32 effect;             //index for effect, -1 for no effect
            chuf::Int32 type;               //1=poly, 2=patch, 3=mesh, 4=billboard
            chuf::Int32 firstVertIndex;     //index to the first vert in this face
            chuf::Int32 vertCount;          //number of vertices in this face
            chuf::Int32 startIndex;         //start index in the indices array for the face
            chuf::Int32 indexCount;         //number of indices in this face
            chuf::Int32 lightmapID;         //index in the texture array for the light map texture
            chuf::Int32 lightmapCorner[2];  //face's lightmap corner
            chuf::Int32 lightmapSize[2];    //lightmap dimensions
            Vec3f lightmapPosition;       //origin of lightmap
            Vec3f lightmapVectors[2];     //coordinates of lightmap S and T vectors
            Vec3f normal;                 //face normal
            chuf::Int32 size[2];            //bezier patch dimensions
        };

        //referred to as a texture in the docs but we'll actually be loading materials
        struct Texture
        {
            chuf::Int8 name[64];
            chuf::Int32 surfaceFlags;
            chuf::Int32 contentFlags;
        };

        enum Lumps //lump list, specific to BSP type
        {
            Entities,
            Textures,
            Planes,
            Nodes,
            Leaves,
            LeafFaces,
            LeafBrushes,
            Models,
            Brushes,
            BrushSides,
            Vertices,
            Indices,
            Shaders,
            Faces,
            Lightmaps,
            LightVolumes,
            VisData,
            MaxLumps
        };

        struct Lightmap
        {
            char imageBits[128][128][3];   // The RGB data in a 128x128 image
        };

        struct Node
        {
            chuf::Int32 planeIndex; //index in plane array
            chuf::Int32 frontIndex; //child index for front node
            chuf::Int32 rearIndex;  //child index for rear node;
            Vec3i bbMin;            //bounding box minimum position
            Vec3i bbMax;            //bounding box maximum position
        };

        struct Leaf
        {
            chuf::Int32 cluster;    //vis cluster to which the leaf belongs
            chuf::Int32 area;       //area portal to which leaf belongs
            Vec3i bbMin;            //bounding box minimum position
            Vec3i bbMax;            //bounding box maximum position
            chuf::Int32 firstFace;  //index of the first face in the face array
            chuf::Int32 faceCount;  //number of faces in this leaf
            chuf::Int32 firstBrush; //first brush in the brush array
            chuf::Int32 brushCount; //number of brushes in this leaf
        };

        struct Plane
        {
            Vec3f normal;
            float distance;
        };

        struct VisiData
        {
            chuf::Int32 clusterCount = 0;
            chuf::Int32 bytesPerCluster = 0;
            chuf::Int8* bitsetArray = nullptr;
        };
    }
}

#endif //CHUF_Q3_BSP_HPP_