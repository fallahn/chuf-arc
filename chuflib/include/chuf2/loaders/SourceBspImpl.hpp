/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//implement source bsp

#ifndef CHUF_SOURCE_HPP_
#define CHUF_SOURCE_HPP_

#include <chuf2/loaders/BspImpl.hpp>
#include <chuf2/loaders/SourceBsp.hpp>

#include <array>

namespace chuf
{
    class Image;
    class SourceBspImpl final : public BspImpl
    {
    public:
        explicit SourceBspImpl(const std::string& path);
        ~SourceBspImpl() = default;

        
        void draw(UInt32 passFlags) override;
        void transformChanged(Transform&) override;
        void setNode(Node* n) override;

    private:
        std::vector<Bsp::Source::Face> m_faces;
        std::vector<Bsp::Source::TexData> m_textureData;
        std::vector<Bsp::Source::TexInfo> m_textureInfo;
        std::vector<std::pair<glm::vec2, glm::vec2>> m_scaleOffsets;

        std::vector<Bsp::Source::Node> m_nodes;
        std::vector<Bsp::Source::Leaf> m_leaves;
        std::vector<Int16> m_leafFaces;
        std::vector<Bsp::Source::Plane> m_planes;
        std::vector<std::pair<BoundingBox, BoundingBox>> m_boundingBoxes;
        std::vector<std::vector<UInt8>> m_clusterBitsets;

        std::vector<Bsp::Source::LeafWaterInfo> m_waterData;

        std::vector<Texture::Sampler::Ptr> m_radiosityNormalMaps;

        void updateVisibility(const glm::vec3& position, const Frustum& frustum) override;

        bool loadMap(const std::string& path);
        void loadMaterials(const std::vector<Int32>& stringLookup, const std::vector<char>& stringData);
        void loadLightmaps(std::vector<Bsp::Source::ColourRGB32Exp>&);
        void calcTexCoords(const std::vector<glm::vec3>& vertices, std::vector<glm::vec2>&uvCoords0,
            std::vector<glm::vec2>&uvCoords1, const std::vector<Int32>& indices, UInt32 faceIndex);
        void calcNormals(const std::vector<glm::vec3>& vertices, std::vector<glm::vec2>&uvCoords0,
            std::vector<glm::vec3>& normals, std::vector<glm::vec3>& tangents, std::vector<glm::vec3>& bitangents, const std::vector<Int32>& indices);
        std::vector<Int32> getFirstVerts(const std::vector<Bsp::Source::Edge>& edges, const std::vector<Int32>& surfEdges, UInt32 faceIndex);
        void createMesh(const std::vector<Bsp::Source::Vertex>& vertices, const std::vector<Bsp::Source::Edge>& edges, const std::vector<Int32>& surfEdges);

        bool clusterVisible(Int32 current, Int32 test);
        void decompressPVS(UInt32 clusterCount, const std::vector<std::array<UInt32, 2>>& offsets, const char* lumpStart);
        Int32 findLeaf(const glm::vec3& position);
        void transformLeafBoundingBoxes();
        void drawFace(UInt32 index, UInt32 passFlags);

        void storeAtlases(const std::vector<std::unique_ptr<Image>>& atlases);
    };
}

#endif //CHUF_SOUCE_HPP_
