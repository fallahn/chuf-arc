/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//intended to load map data from ID tech BSP files

#ifndef CHUF_BSP_HPP_
#define CHUF_BSP_HPP_

#include <chuf2/ClassKey.hpp>
#include <chuf2/NonCopyable.hpp>
#include <chuf2/Scene.hpp>
#include <chuf2/Drawable.hpp>
#include <chuf2/Component.hpp>
#include <chuf2/loaders/BspImpl.hpp>

#include <memory>

namespace chuf
{
    class Node;
    class FrameBuffer;
    class SceneRenderer;
    class BspMap final : public Drawable, private NonCopyable, public Locked
    {
    public:
        using Ptr = std::unique_ptr<BspMap>;
        enum class Type
        {
            QuakeThree,
            Source
        };

        BspMap(const std::string& path, SceneRenderer*, Type type, const Key& key);
        ~BspMap() = default;

        static Ptr create(const std::string& path, SceneRenderer*, Type = Type::QuakeThree);

        //set the bsp's active camera so it can update its visibilty
        void setActiveCamera(Camera*);

        //draws the current visible set based on Pass flags
        UInt32 draw(UInt32 passFlags, bool = false) const override;

        //maps need a node to get the correct matrices for materials
        void setNode(Node* n) override;

        //gets the bounding volume for this map //TODO implement
        const BoundingBox& getBoundingBox() const override{ return m_bb; }

        //allows binding values to materials used in map
        template <typename T>
        void bindValueToMaterials(const std::string& propertyName, std::function<T()>& function)
        {
            m_impl->bindValueToMaterials(propertyName, function);
        }

        //allows setting framebuffers used in special effects to BDP materials
        void setFrameBuffer(Scene::FBO id, FrameBuffer* fbo)
        {
            m_impl->setFrameBuffer(id, fbo);
        }
    private:
        Type m_type; //so we know to which set of data to refer
        std::unique_ptr<BspImpl> m_impl;
        Camera* m_activeCamera;


        void transformChanged(Transform& t) override;

        BoundingBox m_bb; //TODO replace this with proper implementation
    };
}

#endif //CHUF_BSP_HPP_