/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#ifndef CHUF_BSP_IMPL_HPP_
#define CHUF_BSP_IMPL_HPP_

#include <chuf2/NonCopyable.hpp>
#include <chuf2/DataTypes.hpp>
#include <chuf2/Material.hpp>
#include <chuf2/MaterialProperty.hpp>
#include <chuf2/Mesh.hpp>
#include <chuf2/Scene.hpp>
#include <chuf2/FrameBuffer.hpp>

#include <chuf2/glm/glm.hpp>

#include <functional>

//ABC for implementing different versions of BSP maps

namespace chuf
{
    class Frustum;
    class Node;
    class Transform;
    class BspImpl : private NonCopyable, public Camera::Listener
    {
    public:

        virtual ~BspImpl() = default;

        
        virtual void draw(UInt32) = 0;
        virtual void setNode(Node* n)
        {
            m_sceneNode = n;
            if (n)
            {
                for (auto& m : m_materials)
                {
                    m.first->bindNode(n);
                }
            }
        }
        virtual void transformChanged(Transform&) = 0;
        virtual void cameraChanged(Camera* cam) override
        {
            m_dirtyFlags |= DirtyBits::ALL;
            m_camera = cam;
        }
        template <typename T>
        void bindValueToMaterials(const std::string& propertyName, std::function<T()>& function)
        {
            for (auto& m : m_materials)
            {
                m.first->getProperty(propertyName)->bindValue(function);
            }
        }
        void setFrameBuffer(Scene::FBO id, FrameBuffer* fbo)
        {
            for (auto& m : m_materials)
            {
                if (m.first->getType() == Material::Type::Water)
                {
                    (id == Scene::FBO::Reflection) ? m.first->getProperty("u_reflectionTexture")->setSampler(Texture::Sampler::create(fbo->getRenderTexture()->getTexture()))
                                                    : m.first->getProperty("u_refractionTexture")->setSampler(Texture::Sampler::create(fbo->getRenderTexture()->getTexture()));
                }
            }
        }

    protected:
        enum DirtyBits
        {
            FRUSTUM_REFLECTION = 0x1,
            FRUSTUM = 0x2,
            ALL = FRUSTUM | FRUSTUM_REFLECTION
        };

        std::vector<std::pair<Material::Ptr, std::vector<MaterialProperty*>>>& getMaterials(){ return m_materials; }
        std::vector<Texture::Sampler::Ptr>& getLightmaps(){ return m_lightmaps; }
        Node* getNode(){ return m_sceneNode; }
        void setMesh(const Mesh::Ptr& mesh){ m_mesh = mesh; }
        const Mesh::Ptr& getMesh()const{ return m_mesh; }
        std::vector<Int32>& getVisibleFaces(){ return m_visibleFaces; }
        void addTransparentFace(UInt32 index){ m_transparentFaces.push_back(index); }
        std::vector<UInt32>& getTransparentFaces() {return m_transparentFaces; }
        bool flagDirty(DirtyBits flag, bool reset = true)
        {
            bool retVal = ((m_dirtyFlags & flag) != 0);
            if (reset) m_dirtyFlags &= ~flag;
            return retVal;
        }
        Camera* getCamera(){ return m_camera; }
        virtual void updateVisibility(const glm::vec3&, const Frustum&) = 0;
    private:
        std::vector<std::pair<Material::Ptr, std::vector<MaterialProperty*>>> m_materials;
        std::vector<Texture::Sampler::Ptr> m_lightmaps;
        Node* m_sceneNode = nullptr;
        Mesh::Ptr m_mesh;
        std::vector<Int32> m_visibleFaces;
        std::vector<UInt32> m_transparentFaces;


        UInt32 m_dirtyFlags = DirtyBits::ALL;
        Camera* m_camera = nullptr;
    };
}

#endif //CHUF_BSP_IMPL_HPP_
