/*
Interquake Model Format - see
http://sauerbraten.org/iqm/

Code based on IQM SDK

*/

#ifndef CHUF_IQM_HPP_
#define CHUF_IQM_HPP_

namespace Iqm
{
    struct Header
    {
        char magic[16];
        unsigned int version;
        unsigned int filesize;
        unsigned int flags;
        unsigned int textCount, textOffset;
        unsigned int meshCount, meshOffset;
        unsigned int varrayCount, vertexCount, varrayOffset;
        unsigned int triangleCount, triangleOffset, adjacencyOffset;
        unsigned int jointCount, jointOffset;
        unsigned int poseCount, poseOffset;
        unsigned int animCount, animOffset;
        unsigned int frameCount, frameChannelCount, frameOffset, boundsOffset;
        unsigned int commentCount, commentOffset;
        unsigned int extensionCount, extensionOffset;
    };

    struct Mesh
    {
        unsigned int name;
        unsigned int material;
        unsigned int firstVertex, vertexCount;
        unsigned int firstTriangle, triangleCount;
    };

    enum
    {
        POSITION = 0,
        TEXCOORD = 1,
        NORMAL = 2,
        TANGENT = 3,
        BLENDINDICES = 4,
        BLENDWEIGHTS = 5,
        COLOUR = 6,
        CUSTOM = 0x10
    };

    enum
    {
        BYTE = 0,
        UBYTE = 1,
        SHORT = 2,
        USHORT = 3,
        INT = 4,
        UINT = 5,
        HALF = 6,
        FLOAT = 7,
        DOUBLE = 8
    };

    struct Triangle
    {
        unsigned int vertex[3];
    };

    struct Adjacency
    {
        unsigned int triangle[3];
    };

    struct Jointv1
    {
        unsigned int name;
        int parent;
        float translate[3], rotate[3], scale[3];
    };

    struct Joint
    {
        unsigned int name;
        int parent;
        float translate[3], rotate[4], scale[3];
    };

    struct Posev1
    {
        int parent;
        unsigned int mask;
        float channelOffset[9];
        float channelScale[9];
    };

    struct Pose
    {
        int parent;
        unsigned int mask;
        float channelOffset[10];
        float channelScale[10];
    };

    struct Anim
    {
        unsigned int name;
        unsigned int firstFrame, frameCount;
        float framerate;
        unsigned int flags;
    };

    enum
    {
        IQM_LOOP = 1 << 0
    };

    struct VertexArray
    {
        unsigned int type;
        unsigned int flags;
        unsigned int format;
        unsigned int size;
        unsigned int offset;
    };

    struct Bounds
    {
        float bbmin[3], bbmax[3];
        float xyradius, radius;
    };

    struct Extension
    {
        unsigned int name;
        unsigned int dataCount, dataOffset;
        unsigned int extensionOffset; // pointer to next extension
    };
} //namespace Iqm


#include <chuf2/Mesh.hpp>
#include <chuf2/Model.hpp>
#include <chuf2/Skin.hpp>
#include <chuf2/Cache.hpp>

namespace chuf
{
    class IqmModel final
    {
    public:
        //load an IQM model from disk
        static Model::Ptr load(const std::string& path, SceneRenderer&, bool createTanNormals = false);

    private:
        struct ModelCache
        {
            Mesh::Ptr mesh;
            Skin::Animations animations;
            Skin::BoneHierarchy boneIndices;
            Keyframes keyframes;
            std::vector<std::string> materialNames;
        };
        static Cache<IqmModel::ModelCache> m_cache;
    };

} //namespace chuf

#endif //CHUF_IQM_HPP_

