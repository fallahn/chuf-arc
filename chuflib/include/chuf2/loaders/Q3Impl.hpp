/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//implement Q3 BSP

#ifndef CHUF_Q3_IMPL_HPP_
#define CHUF_Q3_IMPL_HPP_

#include <chuf2/loaders/BspImpl.hpp>
#include <chuf2/loaders/Q3Bsp.hpp>

#include <chuf2/Texture.hpp>
#include <chuf2/Frustum.hpp>
#include <chuf2/Transform.hpp>
#include <chuf2/Mesh.hpp>
#include <chuf2/Material.hpp>

namespace chuf
{
    class Q3Impl final : public BspImpl
    {
    public:
        explicit Q3Impl(const std::string& path);
        ~Q3Impl() = default;

        
        void draw(UInt32 passFlags) override;
        void setNode(Node*) override;
        void transformChanged(Transform&) override;

    private:
        std::vector<Bsp::Q3::Face> m_faces;
        std::vector<Bsp::Q3::Node> m_nodes;
        std::vector<Bsp::Q3::Leaf> m_leaves;
        std::vector<Int32> m_leafFaces;
        std::vector<Bsp::Q3::Plane> m_planes;
        Bsp::Q3::VisiData m_clusters;
        std::vector<Int8> m_clusterBitsets;

        std::vector<std::pair<BoundingBox, BoundingBox>> m_leafBoundingBoxes;

        void updateVisibility(const glm::vec3& position, const Frustum& frustum) override;

        bool loadMap(const std::string& map);
        void loadMaterials(const std::vector<Bsp::Q3::Texture>& textures);
        void buildLightmaps(const char* data, UInt32 lightmapCount);
        void buildNormals(const std::vector<Bsp::Q3::Vertex>& vertices, const std::vector<Int32>& indices,
                            std::vector<glm::vec3>& tangents, std::vector<glm::vec3>& bitangents);

        void createMesh(const std::vector<Bsp::Q3::Vertex>& vertices, const std::vector<Int32>& indices,
            const std::vector<glm::vec3>& tangents, const std::vector<glm::vec3>& bitangents);

        Int32 findLeaf(const glm::vec3& camPos);
        bool clusterVisible(Int32 currentCluster, Int32 testCluster);
        void transformLeafBoundingBoxes();
        void drawFace(chuf::UInt32 index, UInt32 passFlags);
    };
}

#endif //CHUF_Q3_IMPL_HPP_