/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#ifndef CHUF_BONE_HPP_
#define CHUF_BONE_HPP_

#include <chuf2/Node.hpp>

namespace chuf
{
    class Bone;
    using Skeleton = std::vector<Bone>;
    class Bone final// : public Node
    {
    public:
        Bone();
        Bone(const std::string& name, Int32 parentId);
        ~Bone() = default;

        const std::string& getName() const;
        const Int32 getParentId() const;
        void setBindPoseMatrix(const glm::mat4& matrix);
        const glm::mat4& getBindPoseMatrix() const;
        void setInverseBindPose(const glm::mat4& mat);
        const glm::mat4& getInverseBindPoseMatrix() const;

    private:
        std::string m_name;
        Int32 m_parentId;
        glm::mat4 m_bindPose;
        glm::mat4 m_inverseBindpose;

        //void transformChanged() override;
    };
}


#endif //CHUF_BONE_HPP_