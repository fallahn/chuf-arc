/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//represents a 2D sprite created as part of a sprite batch

#ifndef CHUF_SPRITE_HPP_
#define CHUF_SPRITE_HPP_

#include <chuf2/Transform.hpp>
#include <chuf2/Rectangle.hpp>
#include <chuf2/Colour.hpp>
#include <chuf2/NonCopyable.hpp>
#include <chuf2/ClassKey.hpp>
#include <chuf2/VertexLayout.hpp>

#include <chuf2/Util.hpp>

#include <memory>
#include <array>

namespace chuf
{
    class SpriteBatch;
    class Sprite final : public Transform::Listener, private NonCopyable, private Locked, public Aligned<16>
    {
        friend class SpriteBatch;
        friend class Node;
    public:
        using Ptr = std::shared_ptr<Sprite>;

        //by default the textures are flipped vertically
        //before being passed to the sprite batch, so that
        //the Y coordinate system starts at zero at the bottom
        //of the screen. To have sprite coordinates start at 
        //the top make sure that textures are not flipped when
        //created before passing them to the sprite batch

        void setSize(const glm::vec2& size);
        void setOrigin(const glm::vec2& origin);
        void move(const glm::vec2& movement);
        void setPosition(const glm::vec2& position);
        void rotate(float angle);
        void setRotation(float angle);
        void scale(float amount);
        void scale(const glm::vec2& amount);
        void setScale(float scale);
        void setScale(const glm::vec2& scale);

        const glm::vec2& getSize() const;
        const glm::vec2& getOrigin() const;
        glm::vec2 getPosition() const;
        float getRotation() const;
        glm::vec2 getScale() const;

        //allows displaying only part of the sprite batch texture
        //coordinates are in texture pixels, not normalised UV coords
        void setSubRect(const FloatRect& subRect);
        const FloatRect& getSubRect() const;

        //sprite's final colour is multiplied by this
        void setColour(const Colour& colour);
        const Colour& getColour() const;

        const std::string& getName() const;
        const glm::vec2& getTextureSize() const;

        Sprite(const std::string& name, const VertexLayout& vertLayout, Key& key);
        ~Sprite();// = default; //{ LOG("Killed sprite"); };//
    
        //by default sprites have their own transform. This can be overridden
        //with a custom transform from an owning class
        void setTransform(Transform*);

        SpriteBatch& getSpriteBatch() const;

        //returns the local bounds as an AABB of the sprite
        const FloatRect& getLocalBounds();
        //returns the global bounds as an AABB taking into account
        //any transformation of the sprite
        const FloatRect& getGlobalBounds();

    private:

        enum DirtyBits
        {
            TRANSFORM = (1 << 0),
            UV        = (1 << 1),
            COLOUR    = (1 << 2),
            VERTS     = (1 << 3),
            ALL       = TRANSFORM | UV | COLOUR | VERTS
        };

        //use this to tell sprite batch specifically that this sprite is dirty,
        //so we don't have to check every sprite in the batch
        SpriteBatch* m_parentBatch; 
        UInt32 m_arrayOffset; //index in parent batch array at which this sprite's quad starts
        UInt32 m_index; //position in sprite array
        glm::vec2 m_textureSize;
        VertexLayout m_vertLayout;
        
        std::array<glm::vec4, 4u> m_vertPositions; //pre-transform vertex positions. vec4 for easy mat multiplying
        FloatRect m_subRect;
        Colour m_colour;
        std::string m_name;
        glm::vec2 m_size;
        glm::vec2 m_origin;
        float m_rotation;
        
        std::unique_ptr<Transform> m_ownTransform; //sprite has its own transform, although
        Transform* m_transform; //we can point this to a node (as a pointer to base class)

        FloatRect m_localBounds;
        FloatRect m_globalBounds;

        void transformChanged(Transform& t) override;

        UInt8 m_dirtyBits;
        void setDirty(UInt8 dirtyFlag);
        void update(); //called by parent batch to make sprite update its vertices
    };
}

#endif //CHUF_SPRITE_HPP_