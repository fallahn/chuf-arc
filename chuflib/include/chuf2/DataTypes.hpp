/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//definitions and typedefs used throughout chuf

#ifndef CHUF_DATATYPES_HPP_
#define CHUF_DATATYPES_HPP_

#include <SDL_events.h>

#include <chuf2/GL/gl3w.h>

#include <string>



namespace chuf
{
    //TODO this might not hold entirely true on mobile platforms
    using Int8 = char;
    using UInt8 = unsigned char;
    using Int16 = short;
    using UInt16 = unsigned short;
    using Int32 = int;
    using UInt32 = unsigned int;
#if defined(_MSC_VER)
    using Int64 = __int64;
    using UInt64 = unsigned __int64;
#else
    using Int64 = long long;
    using UInt64 = unsigned long long;
#endif

    //GL int defs
    using VertexAttributeID = GLint;
    using TextureID = GLuint;
    using FrameBufferID = GLuint;
    using RenderBufferID = GLuint;
    using VertexBufferID = GLuint;
    using IndexBufferID = GLuint;

    //vertex attribute strings
    const std::string vertexAttribPosition("a_position");
    const std::string vertexAttribColour("a_colour");
    const std::string vertexAttribNormal("a_normal");	
    const std::string vertexAttribTangent("a_tangent");
    const std::string vertexAttribBitangent("a_bitangent");
    const std::string vertexAttribUV0("a_texCoord0");
    const std::string vertexAttribUV1("a_texCoord1");
    const std::string vertexAttribBlendIndices("a_boneIndices");
    const std::string vertexAttribBlendWeights("a_boneWeights");

    namespace Event
    {
        
        //maps event names to SDL events
        //not all SDL events are mapped		
        enum Type
        {
            WINDOW = SDL_WINDOWEVENT,
            
            KEY_PRESSED		= SDL_KEYDOWN,
            KEY_RELEASED	= SDL_KEYUP,
            TEXT_EDITING	= SDL_TEXTEDITING,
            TEXT_INPUT		= SDL_TEXTINPUT,

            MOUSE_MOVE				= SDL_MOUSEMOTION,
            MOUSE_BUTTON_PRESSED	= SDL_MOUSEBUTTONDOWN,
            MOUSE_BUTTON_RELEASED	= SDL_MOUSEBUTTONUP,
            MOUSE_WHEEL_MOVED		= SDL_MOUSEWHEEL,

            JOY_AXIS_MOVED		= SDL_JOYAXISMOTION,
            JOY_BALL_MOVED		= SDL_JOYBALLMOTION,
            JOY_HAT_MOVED		= SDL_JOYHATMOTION,
            JOY_BUTTON_PRESSED	= SDL_JOYBUTTONDOWN,
            JOY_BUTTON_RELEASED = SDL_JOYBUTTONUP,
            JOY_CONNECTED		= SDL_JOYDEVICEADDED,
            JOY_DISCONNECTED	= SDL_JOYDEVICEREMOVED,

            CONTROLLER_AXIS_MOVED		= SDL_CONTROLLERAXISMOTION,
            CONTROLLER_BUTTON_PRESSED	= SDL_CONTROLLERBUTTONDOWN,
            CONTROLLER_BUTTON_RELEASED	= SDL_CONTROLLERBUTTONUP,
            CONTROLLER_CONNECTED		= SDL_CONTROLLERDEVICEADDED,
            CONTROLLER_DISCONNECTED		= SDL_CONTROLLERDEVICEREMOVED,
            CONTROLLER_REMAPPED			= SDL_CONTROLLERDEVICEREMAPPED
        };

        //event aliases
        using MouseEvent = SDL_Event;
        using KeyEvent = SDL_Event;
        using JoyEvent = SDL_Event;
        using ControllerEvent = SDL_Event;
        using WindowEvent = SDL_Event;
    }

    enum RenderPass
    {
        Standard   = 0x1,
        AlphaBlend = 0x2,
        Refraction = 0x4,
        Reflection = 0x8,
        Debug      = 0x10
    };
}

#endif //CHUF_DATATYPES_HPP_