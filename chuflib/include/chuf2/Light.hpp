/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//defines a directional, point or spot light which can be attached to a scene node

#ifndef CHUF_LIGHT_HPP_
#define CHUF_LIGHT_HPP_

#include <chuf2/NonCopyable.hpp>
#include <chuf2/ClassKey.hpp>
#include <chuf2/Colour.hpp>
#include <chuf2/Component.hpp>

#include <chuf2/glm/glm.hpp>

#include <memory>

namespace chuf
{
    class Node;
    class Light final : public Component, private NonCopyable, private Locked
    {
        friend class Node;
    public:
        using Ptr = std::unique_ptr<Light>;

        enum class Type
        {
            Point,
            Spot,
            Directional
        };

        static Ptr createPoint(const Colour& colour, float range);
        static Ptr createSpot(const Colour& colour, float range, float innerAngle, float outerAngle);
        static Ptr createDirectional(const Colour& colour);

        Light(const Colour& colour, float range, const Key& key);
        Light(const Colour& colour, float range, float innerAngle, float outerAngle, const Key& key);
        Light(const Colour& colour, const Key& key);
        ~Light() = default;

        //returns the light's type
        Type getType() const;
        //returns the lights colour. for lights with an evolving
        //or changing colour you would bind this function to a
        //material uniform, so that the changes are reflected in the scene
        const Colour& getColour() const;
        //sets the light's colour
        void setColour(const Colour& colour);
        //returns the light's parent node if it has one, or nullptr
        Node* getNode() const;
        //returns the range of the light's influence, if not a directional light
        float getRange() const;
        //sets the light's range if not a directional light
        void setRange(float range);
        //returns the inverse of the lights range, usually for binding to a shader
        float getRangeInverse() const;
        //returns the light cone inner angle (in radians) of a spot light
        float getInnerAngle() const;
        //sets the inner angle of the light cone of a spot light, in radians
        void setInnerAngle(float angle);
        //returns the light cone outer angle (in radians) of a spot light
        float getOuterAngle() const;
        //sets the outer angle of the light cone of a spot light, in radians
        void setOuterAngle(float angle);
        //returns the cosine of the inner angle of a spot light
        float getInnerAngleCos() const;
        //returns the cosine of the outer angle of a spot light
        float getOuterAngleCos() const;

    private:

        struct BaseLight
        {
            BaseLight(const Colour& colour);
            Colour colour;
        };
        struct Point : public BaseLight
        {
            Point(const Colour& colour, float range);
            float range;
            float rangeInverse;
        };

        struct Spot : public BaseLight
        {
            Spot(const Colour& colour, float range, float innerAngle, float outerAngle);
            float range, rangeInverse;
            float innerAngle, innerAngleCos;
            float outerAngle, outerAngleCos;
        };

        struct Directional : public BaseLight
        {
            explicit Directional(const Colour&  colour);
        };

        Type m_type;
        Node* m_node;

        std::unique_ptr<BaseLight> m_light;
    };
}

#endif //CHUF_LIGHT_HPP_
