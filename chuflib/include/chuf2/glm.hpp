//just to collect together needed glm headers
#include <chuf2/glm/glm.hpp>
#include <chuf2/glm/gtc/matrix_transform.hpp>
#include <chuf2/glm/gtc/type_ptr.hpp>
#include <chuf2/glm/gtc/quaternion.hpp>
#include <chuf2/glm/gtx/transform.hpp>
#include <chuf2/glm/gtx/norm.hpp>
#include <chuf2/glm/gtc/type_precision.hpp>
#include <chuf2/glm/gtx/string_cast.hpp>
#include <chuf2/glm/gtx/io.hpp>
#include <chuf2/glm/gtx/euler_angles.hpp>