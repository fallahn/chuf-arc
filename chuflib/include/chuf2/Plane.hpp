/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//defines a plane as a normal and distance

#ifndef CHUF_PLANE_HPP_
#define CHUF_PLANE_HPP_

#include <chuf2/glm/glm.hpp>

namespace chuf
{
    class BoundingBox;
    class BoundingSphere;
    class Frustum;
    class Plane final
    {
    public:
        enum class Intersection
        {
            Back = -1,
            Intersecting = 0,
            Front = 1
        };

        Plane();
        Plane(const glm::vec3& normal, float distance);
        Plane& operator *= (const glm::mat4& matrix);
        ~Plane() = default;

        //gets the plane's normal vector
        const glm::vec3& getNormal() const;
        //sets the planes normal vector (ie which what it is facing)
        void setNormal(const glm::vec3& normal);

        //return the plane's distance from 0
        float getDistance() const;
        //sets the plane's distance
        void setDistance(float distance);
        //returns the distance from specified point
        float distance(const glm::vec3& point) const;

        //returns as a vec4 representation
        const glm::vec4& getVec4() const { return m_fourComponent; }

        //returns the point of intersection of given planes
        static glm::vec3 intersection(const Plane& p1, const Plane& p2, const Plane& p3);
        //returns true if given plane is parallel to this one, ie the normals are the same or exactly opposite
        bool parallel(const Plane& p) const;
        //sets the plane with the given parameters
        void set(const glm::vec3& normal, float distance);
        //sets the plane from a single vec4
        void set(const glm::vec4& p);
        //sets the plane to a copy of another
        void set(const Plane& p);
        //transforms the plane by a given matrix
        void transform(const glm::mat4& mat);
        //normalises the plane
        void normalise();
        //inverts the direction / distance of the plan
        void invert();

        //returns if plane intersects given bounding box
        //or which side of the plane the box is sitting
        Plane::Intersection intersects(const BoundingBox& box) const;
        //returns if plane instersects given bounding sphere
        //or which side of the plane the sphere is sitting
        Plane::Intersection intersects(const BoundingSphere& sphere) const;
        //returns if plane intersects given plane
        //or which side of the plane the given plane is sitting
        Plane::Intersection intersects(const Plane& plane) const;
        //returns if plane intersects given frustum
        //or which side of the plane the frustum is sitting
        Plane::Intersection intersects(const Frustum& frustum) const;

    private:

        glm::vec3 m_normal;
        float m_distance;
        glm::vec4 m_fourComponent;
    };

    const Plane operator * (const glm::mat4& mat, const Plane& plane);
}

#endif //CHUF_PLANE_HPP_