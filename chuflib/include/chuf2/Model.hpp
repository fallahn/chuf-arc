/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//represents a mesh / material combination which can be attached to a
//scene node

#ifndef CHUF_MODEL_HPP_
#define CHUF_MODEL_HPP_

#include <chuf2/NonCopyable.hpp>
#include <chuf2/ClassKey.hpp>
#include <chuf2/DataTypes.hpp>
#include <chuf2/Drawable.hpp>
#include <chuf2/Component.hpp>

#include <memory>
#include <string>
#include <vector>
#include <map>

namespace chuf
{
    class Mesh;
    class Material;
    class Node;
    class Skin;
    class Shader;
    class VertexAttribBinding;
    class SceneRenderer;
    class Model final : public Drawable, private NonCopyable, private Locked
    {
    public:
        using Ptr = std::unique_ptr<Model>;
        Model(const std::shared_ptr<Mesh>& mesh, SceneRenderer*, const Key& key);
        ~Model() = default;

        //returns a heap allocated unique_ptr to the created model if it 
        //succeeds, else returns nullptr. attach the returned pointer to a 
        //parent node in a scene graph to keep it alive.
        static Ptr create(const std::shared_ptr<Mesh>& mesh, SceneRenderer*);

        //returns a shared_ptr to theis model's mesh
        std::shared_ptr<Mesh> getMesh() const;
        //returns the number of sub meshes used by this model's mesh
        UInt32 getSubMeshCount() const;
        //returns a pointer to the model's material, or material of the given sub mesh
        //index if it is found, else returns nullptr
        std::shared_ptr<Material> getMaterial(Int32 subMeshIndex = -1) const;
        //sets the model's material, or submesh's material if the given index is valid
        void setMaterial(const std::shared_ptr<Material>& material, Int32 subMeshIndex = -1);
        //attempts to create a material from the given shader paths to local storage
        std::shared_ptr<Material> setMaterial(const std::string& vertShader, const std::string& fragShader, Int32 submeshIndex = -1);
        //attempts to create and set a material from the given material config
        std::shared_ptr<Material> setMaterial(const std::string& path, Int32 submeshIndex = -1);
        //returns true if the given sub mesh index is valid, and the sub mesh has a material
        bool hasMaterial(Int32 index);
        //sets a skin for animated meshes
        void setSkin(const std::shared_ptr<Skin>& skin);
        //retrieves a shared pointer to this models skin, or nullptr if no skin exists
        std::shared_ptr<Skin> getSkin() const;
        //returns a pointer to this model's parent node if it has one,
        //else returns nullptr
        Node* getNode() const;
        //sets the model's parent node, and discards any existing one
        void setNode(Node* node) override;
        //returns the bounding box for this model
        const BoundingBox& getBoundingBox() const override;
        //draws the model by binding its mesh's VBO and material's texture/shader
        UInt32 draw(UInt32 passFlags, bool wireframe = false) const override;

        void transformChanged(Transform&) override;

    private:
        std::shared_ptr<Mesh> m_mesh;
        std::shared_ptr<Material> m_material;
        std::vector<std::shared_ptr<Material>> m_subMeshMaterials;
        std::shared_ptr<Skin> m_skin;
        UInt32 m_subMeshCount;
        std::map<Shader*, VertexAttribBinding*> m_vaoBinds;
        
        mutable BoundingBox m_bb;
        mutable bool m_bbDirty;

        void bindNodeToMaterial(const std::shared_ptr<Material>& m);
        void verifySubMeshCount();
    };

}

#endif //CHUF_MODEL_HPP_