/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

#ifndef CHUF_AUDIOSTREAM_HPP_
#define CHUF_AUDIOSTREAM_HPP_

#include <chuf2/AudioSource.hpp>
#include <chuf2/DataTypes.hpp>

#include <thread>
#include <mutex>
#include <array>
#include <memory>

namespace chuf
{
    class AudioStream : public AudioSource
    {
    public:
        struct Chunk
        {
            const Int16* data;
            UInt32 sampleCount;
        };

        virtual ~AudioStream();

        void play();
        void pause();
        void stop();

        void setLooped(bool loop);
        bool looped() const;

        AudioSource::State getState() const;
        UInt32 getSampleRate() const;
        UInt32 getChannelCount() const;

        void setPlayOffset(float offset);
        float getPlayOffset() const;

    protected:

        AudioStream();

        void init(UInt32 channelCount, UInt32 sampleRate);
        virtual bool onGetData(Chunk& data) = 0;
        virtual void onSeek(float offset) = 0;

    private:

        enum
        {
            BufferCount = 3
        };

        std::unique_ptr<std::thread> m_thread;
        mutable std::mutex m_mutex;
        AudioSource::State m_threadStartState;
        bool m_streaming;
        std::array<UInt32, BufferCount> m_buffers;
        UInt32 m_channelCount;
        UInt32 m_sampleRate;
        UInt32 m_bufferFormat;
        bool m_loop;
        UInt64 m_samplesProcessed;
        std::array<bool, BufferCount> m_endBuffers;

        bool fillQueue();
        void clearQueue();
        bool fillAndPushBuffer(UInt32 bufferId);
        void streamData();
    };
}


#endif //CHUF_AUDIOSTREAM_HPP_