/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//a 3d AABB

#ifndef CHUF_AABB_HPP_
#define CHUF_AABB_HPP_

#include <chuf2/Frustum.hpp>

#include <chuf2/glm/glm.hpp>

#include <vector>

namespace chuf
{
    class Ray;
    class BoundingSphere;
    class BoundingBox final
    {
    public:
        BoundingBox() = default;
        BoundingBox(const glm::vec3& minPoint, const glm::vec3& maxPoint);
        BoundingBox(const BoundingBox& other);
        ~BoundingBox() = default;

        //get the centre point of the bounding box
        glm::vec3 getCentre() const;
        //get the minimum point of the bounding box
        const glm::vec3& getMinPoint() const;
        //get the maximum point of the bounding box
        const glm::vec3& getMaxPoint() const;
        //set the min point
        void setMinPoint(const glm::vec3& minPoint);
        //set the max point
        void setMaxPoint(const glm::vec3& maxPoint);

        //returns true if bounding box intersects with given bounding box
        bool intersects(const BoundingBox& bb) const;
        //returns true if this box intersects given sphere
        bool intersects(const BoundingSphere& bs) const;
        //returns true if bounding box intersects with given frustum
        bool intersects(const Frustum& f) const;
        //returns whether the bounding box is in front, behind or intersecting
        //the given plane
        Plane::Intersection intersects(const Plane& p) const;
        //returns the distance from the origin of the ray if it intersects
        //else < 0 if ray does not intersect
        float intersects(const Ray&) const;

        //returns true if bounding box is empty and has no size
        bool empty() const;

        //merges bounding box with given bounding box
        void merge(const BoundingBox& bb);
        //resizes box to encapsulate sphere as close as possible
        void merge(const BoundingSphere& bs);

        //sets the bounding box with the given min and max points
        void set(const glm::vec3& minPoint, const glm::vec3& maxPoint);
        //sets the bounding box to a copy of the given bounding box
        void set(const BoundingBox& other);
        //sets this box to the size of given sphere
        void set(const BoundingSphere& bs);

        //return a vector of vec3 representing the 8 corners
        std::vector<glm::vec4> getCorners() const;

        void transform(const glm::mat4& matrix);

        BoundingBox& operator *= (const glm::mat4& matrix);


    private:
        glm::vec3 m_minPoint, m_maxPoint;
    };

    chuf::BoundingBox operator * (const glm::mat4& matrix, const BoundingBox& bb);

}

#endif //CHUF_AABB_HPP_