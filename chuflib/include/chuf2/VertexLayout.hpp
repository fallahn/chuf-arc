/*********************************************************************
Matt Marchant 2014 - 2015
http://trederia.blogspot.com

CHUF 2.0 Zlib license.

This software is provided 'as-is', without any express or
implied warranty. In no event will the authors be held
liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute
it freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented;
you must not claim that you wrote the original software.
If you use this software in a product, an acknowledgment
in the product documentation would be appreciated but
is not required.

2. Altered source versions must be plainly marked as such,
and must not be misrepresented as being the original software.

3. This notice may not be removed or altered from any
source distribution.
*********************************************************************/

//defines the layout of the vertex properties used by a mesh

#ifndef CHUF_VERTEX_LAYOUT_HPP_
#define CHUF_VERTEX_LAYOUT_HPP_

#include <chuf2/DataTypes.hpp>

#include <vector>

namespace chuf
{
    class VertexLayout final
    {
    public:
        enum class Type
        {
            NONE,
            POSITION = 1,
            COLOUR,
            NORMAL,
            TANGENT,
            BITANGENT,
            BLENDINDICES,
            BLENDWEIGHTS,
            UV0,
            UV1 //second set of UVs for shadow mapping
        };

        struct Element final
        {
            Type type;
            UInt32 size;
            Element();
            Element(Type type, UInt32 size);

            bool operator == (const Element& e) const;
            bool operator != (const Element& e) const;
        };

        explicit VertexLayout(const std::vector<Element>& elements);
        ~VertexLayout() = default;

        const Element& getElement(UInt32 index) const;
        UInt32 getElementCount() const;
        UInt32 getVertexSize() const;
        Int8 getElementIndex(Type elementType) const; //returns -1 if element doesn't exist
        Int32 getElementOffset(Type elementType) const;

        bool operator == (const VertexLayout& v) const;
        bool operator != (const VertexLayout& v) const;



    private:
        std::vector<Element> m_elements;
        UInt32 m_vertSize;
    };
}

#endif //CHUF_VERTEX_LAYOUT_HPP_