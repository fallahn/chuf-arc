project(CHUF2)
cmake_minimum_required(VERSION 2.8)

SET (CMAKE_C_COMPILER             "/usr/bin/clang")
SET (CMAKE_C_FLAGS                "-Wall -std=c99")
SET (CMAKE_C_FLAGS_DEBUG          "-g -_DEBUG_")
SET (CMAKE_C_FLAGS_MINSIZEREL     "-Os -DNDEBUG")
SET (CMAKE_C_FLAGS_RELEASE        "-O4 -DNDEBUG")
SET (CMAKE_C_FLAGS_RELWITHDEBINFO "-O2 -g")

SET (CMAKE_CXX_COMPILER             "/usr/bin/clang++")
SET (CMAKE_CXX_FLAGS                "-Wall")
SET (CMAKE_CXX_FLAGS_DEBUG          "-g -_DEBUG_")
SET (CMAKE_CXX_FLAGS_MINSIZEREL     "-Os -DNDEBUG")
SET (CMAKE_CXX_FLAGS_RELEASE        "-O4 -DNDEBUG")
SET (CMAKE_CXX_FLAGS_RELWITHDEBINFO "-O2 -g")

SET (CMAKE_CXX_FLAGS        "${CMAKE_CXX_FLAGS} -std=c++1y -stdlib=libc++")
SET (CMAKE_EXE_LINKER_FLAGS "${CMAKE_CXX_FLAGS} -lc++abi")

SET (CMAKE_AR      "/usr/bin/llvm-ar")
SET (CMAKE_LINKER  "/usr/bin/llvm-ld")
SET (CMAKE_NM      "/usr/bin/llvm-nm")
SET (CMAKE_OBJDUMP "/usr/bin/llvm-objdump")
SET (CMAKE_RANLIB  "/usr/bin/llvm-ranlib")

SET (CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/Modules/")

#allows building the sample browser app if needed
set(BUILD_SAMPLE FALSE CACHE bool "Enable this to build the CHUF 2.0 Sample Browser Application")

find_package(OpenGL REQUIRED)
find_package(OpenAL REQUIRED)

#TODO this won't work on windows
include (FindPkgConfig)
PKG_SEARCH_MODULE(SDL2 REQUIRED sdl2)
PKG_SEARCH_MODULE(FREETYPE2 REQUIRED freetype2)
PKG_SEARCH_MODULE(SNDFILE REQUIRED sndfile)

#TODO add polyvox as dependency
#TODO add bullet physics as dependency

set(catlib_HDRS
		${CMAKE_SOURCE_DIR}/../utilities/catlib/include/catlib.hpp
		${CMAKE_SOURCE_DIR}/../utilities/catlib/include/stb_image.h)
set(catlib_SRCS
		${CMAKE_SOURCE_DIR}/../utilities/catlib/src/catlib.cpp)
add_library(catlib STATIC ${catlib_HDRS} ${catlib_SRCS})

include_directories(
	${OPENGL_INCLUDE_DIRS}
	${SNDFILE_INCLUDE_DIRS}
	${OPENAL_INCLUDE_DIR}
	${SDL2_INCLUDE_DIRS}
	${FREETYPE2_INCLUDE_DIRS}
	${CMAKE_SOURCE_DIR}/include
	${CMAKE_SOURCE_DIR}/../utilities/catlib/include)

link_libraries(
	${SDL2_LIBRARIES}
	${FREETYPE2_LIBRARIES}
	${OPENAL_LIBRARY}
	${SNDFILE_LIBRARIES}
	${OPENGL_LIBRARIES}
	${CMAKE_DL_LIBS}
	${CMAKE_THREAD_LIBS_INIT}
	catlib)

#if (UNIX)
	link_libraries("${LINK_LIBRARIES}-lpthread")
#endif (UNIX)


add_library(
	${PROJECT_NAME} STATIC 
	src/App.cpp
	src/AudioBuffer.cpp
	src/AudioFile.cpp
	src/AudioListener.cpp
	src/AudioMusic.cpp
	src/AudioResource.cpp
	src/AudioSound.cpp
	src/AudioSource.cpp
	src/AudioStream.cpp
	src/Bone.cpp
	src/BoundingBox.cpp
	src/BoundingSphere.cpp
	src/BspLoader.cpp
	src/Cache.cpp
	src/Camera.cpp
	src/Colour.cpp
	src/ConfigFile.cpp
	src/DepthStencilTarget.cpp
	src/Drawable.cpp
	src/Font.cpp
	src/FrameBuffer.cpp
	src/Frustum.cpp
	src/gl3w.c
	src/HeightData.cpp
	src/Image.cpp
	src/IqmLoader.cpp
	src/Light.cpp
	src/Material.cpp
	src/MaterialProperty.cpp
	src/Mesh.cpp
	src/MeshCreation.cpp
	src/Model.cpp
	src/Node.cpp
	src/Pass.cpp
	src/PhysCollisionComponent.cpp
	src/PhysCollisionShape.cpp
	src/PhysCollisionShapeDefinition.cpp
	src/PhysDebugDraw.cpp
	src/PhysWorld.cpp
	src/Plane.cpp
	src/Q3Impl.cpp
	src/QuadTree.cpp
	src/QuadTreeComponent.cpp
	src/QuadTreeNode.cpp
	src/Ray.cpp
	src/RenderState.cpp
	src/RenderTexture.cpp
	src/Reports.cpp
	src/Scene.cpp
	src/SceneRenderer.cpp
	src/Shader.cpp
	src/Skin.cpp
	src/SkinAnimation.cpp
	src/SourceBspImpl.cpp
	src/Sprite.cpp
	src/SpriteBatch.cpp
	src/StateBlock.cpp
	src/SubMesh.cpp
	src/Taggable.cpp
	src/Technique.cpp
	src/Terrain.cpp
	src/TerrainPatch.cpp
	src/Text.cpp
	src/Texture.cpp
	src/TextureSampler.cpp
	src/Transform.cpp
	src/UIButton.cpp
	src/UIContainer.cpp
	src/UIControl.cpp
	src/UIDesktop.cpp
	src/VertexAttribBinding.cpp
	src/VertexLayout.cpp
	src/View.cpp)

set_target_properties(${PROJECT_NAME} PROPERTIES DEBUG_POSTFIX -d)

#builds the sample browser project if it was selected
if(BUILD_SAMPLE)

link_libraries(${PROJECT_NAME})

include_directories(${CMAKE_SOURCE_DIR}/../sample-browser/include)

#set(BROWSER_SRC ${CMAKE_SOURCE_DIR}/../sample-browser/src)

add_executable(
	SAMPLE_BROWSER
	${CMAKE_SOURCE_DIR}/../sample-browser/src/main.cpp
	${CMAKE_SOURCE_DIR}/../sample-browser/src/SampleAlphaMask.cpp
	${CMAKE_SOURCE_DIR}/../sample-browser/src/SampleBrowser.cpp
	${CMAKE_SOURCE_DIR}/../sample-browser/src/SampleBsp.cpp
	${CMAKE_SOURCE_DIR}/../sample-browser/src/SampleStack.cpp
	${CMAKE_SOURCE_DIR}/../sample-browser/src/SampleTerrain.cpp
	${CMAKE_SOURCE_DIR}/../sample-browser/src/SampleUI.cpp)

install(TARGETS SAMPLE_BROWSER RUNTIME DESTINATION ${CMAKE_SOURCE_DIR}/../sample-browser)

endif()